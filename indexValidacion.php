<?

header('P3P: CP="IDC DSP COR CURa ADMa OUR IND PHY ONL COM STA"');
session_start();

ini_set('display_errors', 0);

include("default.php");
include("librerias/configuracion.php");
include("librerias/funcionEnvioEmail.php");
require("clases/sempat.class.php");

$objSem = new sempat();

$usuarioLogin = mb_convert_encoding(trim($_POST['usuarioNombre']), "ISO-8859-1", "UTF-8");
$usuarioPassword = mb_convert_encoding(trim($_POST['usuarioPassword']), "ISO-8859-1", "UTF-8");
$usuarioPasswordB = mb_convert_encoding(trim($_POST['usuarioPasswordB']), "ISO-8859-1", "UTF-8");
$usuarioPasswordB2 = mb_convert_encoding(trim($_POST['usuarioPasswordB2']), "ISO-8859-1", "UTF-8");
$estado = mb_convert_encoding(trim($_POST['estado']), "ISO-8859-1", "UTF-8");

$mensaje = "";

$usuarioId = "0";
$passEstado = 0;


// <editor-fold>
if ($estado == "login") {

    if (($usuarioLogin != "") && ($usuarioPassword != "")) {

        $resultado = $objSem->logIn($usuarioLogin, $usuarioPassword);
        $usuarioId = $resultado["usuarioId"] ? $resultado["usuarioId"] : "0";

        if ($usuarioId != 0 && $usuarioId != 'A' && $usuarioId != 'B') {

            $_SESSION['SEMPAT_usuarioNombre'] = $usuarioLogin;
            $_SESSION['SEMPAT_usuarioId'] = $usuarioId;
            $_SESSION['SEMPAT_clienteId'] = $resultado["clienteId"];
            $_SESSION['SEMPAT_clienteNombre'] = $resultado["clienteNombre"];
            $_SESSION['SEMPAT_grupoEmisorId'] = $resultado["grupoEmisorId"];

            switch ($resultado["clienteId"]) {
                case 19:
                    $_SESSION['SEMPAT_clienteNombre'] = "MAERSK";
                    $_SESSION['SEMPAT_grupoEmisorId'] = "154";
                    break;
                case 31:
                    $_SESSION['SEMPAT_clienteNombre'] = "KUEHNE & NAGEL";
                    $_SESSION['SEMPAT_grupoEmisorId'] = "1832";
                    break;
                case 24:
                    $_SESSION['SEMPAT_clienteNombre'] = "DELFIN GROUP";
                    $_SESSION['SEMPAT_grupoEmisorId'] = "842";
                    break;
            }


            $_SESSION['SEMPAT_moduloD'] = '';
            $_SESSION['SEMPAT_moduloD2'] = '';

            $passEstado = $resultado["passEstado"];
            $usuarioNombre = $resultado["usuarioNombre"];

            if ($passEstado == 0) {
                $results[] = array('estado' => "pass", 'url' => "");
            } else {
                $ip = getRealIP();
                $objSem->logOn($usuarioId, $ip);
                $results[] = array('estado' => "OK", 'url' => trim($resultado['menuUrl']));
            }
        } else {

            if ($usuarioId == "0" || $usuarioId == "A") {
                $results[] = array('estado' => "A", 'url' => "");   // USUARIO NO EXISTE
            }

            if ($usuarioId == "B") {
                $results[] = array('estado' => "B", 'url' => "");   // PASS INCORRECTA
            }
        }
    } else {
        $results[] = array('estado' => "N", 'url' => "");   // COMPLETE LOS CAMPOS
    }
}
// </editor-fold>
// <editor-fold>
if ($estado == "recuperar") {

    if ($usuarioLogin != "") {

        $mensaje = "";

        $dataUsuario = $objSem->recuperarPassword($usuarioLogin);

        $usuarioIdEdit = 0;
        $usuarioNombre = "";
        $usuarioEmail = "";
        $usuarioPass = "";

        $numFilas = mssql_num_rows($dataUsuario);

        if ($numFilas != 0) {

            if ($dataUsuario) {
                while ($rowdatos = mssql_fetch_array($dataUsuario)) {

                    $usuarioIdEdit = $rowdatos[0];
                    $usuarioNombre = htmlentities($rowdatos[2]);
                    $usuarioEmail = htmlentities($rowdatos[4]);
                    $usuarioPass = htmlentities($rowdatos[6]);
                }
            }

            //$usuarioNombre = "Tomas";
            //$usuarioEmail = "olazo@brains.cl";

            $nombres = split(" ", $usuarioNombre);

            $usuarioNombre = $nombres[0];
            $usuarioNombre = strtolower($usuarioNombre);
            $usuarioNombre = ucwords($usuarioNombre);

            $encabezado = "Le informamos que sus datos de acceso a <b>SEMPAT</b> son los siguientes:<br/><br/>";
            //$footer = "<br><br> Saludos cordiales. <br> <b> SOPORTE SEMPAT</b>";
            $footer = " Para ingresar a <b>SEMPAT</b>, Ud. debe entrar a nuestro portal <a href='http://www.sempat.cl'>www.sempat.cl</a> <br>
                        <br>
                        <label style='color:#3D85FE; font-weight: bold;'>Atentamente <br>
                        Soporte Sempat</label>";

            $correoDe = "sempat@brains.cl";
            $correoDeNombre = "SEMPAT";
            $subject = "Acceso Sempat";


            $mensajeCorreo = "";

            $mensajeCorreo = $mensajeCorreo . "<div style='font-style:\"Lucida Grande\"; font-size:13px'><div style=\"background: url('" . $urlImagenes . "/mail/mailCabecera.png') no-repeat;width: 550px; height: 90px;\"><div style='padding:30px'><img src='" . $urlImagenes . "/mail/logo.png' border='0' width='40%' alt='SEMPAT' title='SEMPAT'/></div></div>";
            $mensajeCorreo = $mensajeCorreo . "<div style=\"background: url('" . $urlImagenes . "/mail/mailBody.png') no-repeat; width: 550px; height: 290px; padding-top:20px; padding-left:20px;\">";

            $mensajeCorreo = $mensajeCorreo . "<label style='color:#3D85FE; font-weight: bold;'>Estimado(a) " . mb_convert_encoding($usuarioNombre, "ISO-8859-1", "UTF-8") . "</label><br><br>";
            $mensajeCorreo = $mensajeCorreo . "<table width='95%' colspan='0' style='border:0'><tr><td style='text-align:justify'>" . stripslashes(mb_convert_encoding($encabezado, "ISO-8859-1", "UTF-8")) . "</td></tr></table>";

            $mensajeCorreo = $mensajeCorreo . "<table width='100%'>";
            $mensajeCorreo = $mensajeCorreo . "<tr><td width='105px'><label style='color:#3D85FE; font-weight: bold;''>Usuario:</label></td><td><b>" . $usuarioLogin . "</b></td></tr>";
            $mensajeCorreo = $mensajeCorreo . "<tr><td><label style='color:#3D85FE; font-weight: bold;''>Contraseña:</label></td><td><b>" . $usuarioPass . "</b></td></tr>";
            $mensajeCorreo = $mensajeCorreo . "</table>";

            $mensajeCorreo = $mensajeCorreo . "<br>" . $footer;

            $mensajeCorreo = $mensajeCorreo . "</div></div>";

            $resultado = enviarCorreo($correoDe, $correoDeNombre, $usuarioEmail, $subject, mb_convert_encoding($mensajeCorreo, "ISO-8859-1", "UTF-8"));

            //echo $mensajeCorreo;
            //echo $resultado;

            if ($resultado == "") {
                $objSem->guardarPassword($usuarioIdEdit, 1, "");
                $results[] = array('estado' => "S", 'url' => "");
            } else {
                $objSem->guardarPassword($usuarioIdEdit, 0, $resultado);
                $results[] = array('estado' => "NS", 'url' => "");
            }
        } else {
            $results[] = array('estado' => "E", 'url' => "");
        }
    } else {
        $results[] = array('estado' => "N", 'url' => "");
    }
}
// </editor-fold>
// <editor-fold>
if ($estado == "pass") {

    $resultado = $objSem->logIn($usuarioLogin, $usuarioPassword);
    $usuarioId = $resultado["usuarioId"] ? $resultado["usuarioId"] : "0";

    if (trim($usuarioPasswordB != "" && $usuarioPasswordB2) != "") {

        if ($usuarioPasswordB == $usuarioPasswordB2) {

            if (strlen($usuarioPasswordB) < 5) {
                $results[] = array('estado' => "5", 'url' => "");   // MIN 5 CARACTERES
            } else {

                $result = $objSem->cambiarPassword($usuarioId, $usuarioPasswordB);
                $row = $base_datos->sql_fetch_assoc($result);
                $numColumnas = mssql_num_fields($result);

                if ($numColumnas > 1) {

                    $_SESSION['SEMPAT_usuarioNombre'] = $usuarioLogin;
                    $_SESSION['SEMPAT_usuarioId'] = $usuarioId;
                    $_SESSION['SEMPAT_clienteId'] = $resultado["clienteId"];
                    $_SESSION['SEMPAT_moduloD'] = '';
                    $_SESSION['SEMPAT_moduloD2'] = '';

                    $query = "Insert into UsuariosLogon Select '" . $usuarioId . "', '" . getRealIP() . "', GETDATE()";
                    $result = $base_datos->sql_query($query);

                    $results[] = array('estado' => "OK", 'url' => trim($row['menuUrl']));
                } else {

                    if ($row["caso"] == "A") {
                        $results[] = array('estado' => "A", 'url' => "");   // NO VALIDA
                    } else if ($row["caso"] == "B") {
                        $results[] = array('estado' => "B", 'url' => "");   // NO DEBE SER IGUAL AL RUT
                    } else {
                        $results[] = array('estado' => "E", 'url' => "");   // ERROR
                    }
                }
            }
        } else {
            $results[] = array('estado' => "NC", 'url' => "");  // NO COINCIDEN
        }
    } else {
        $results[] = array('estado' => "N", 'url' => "");  // COMPLETE LOS CAMPOS
    }
}

// </editor-fold>


echo json_encode($results);

function getRealIP() {
    if (!empty($_SERVER['HTTP_CLIENT_IP']))
        return $_SERVER['HTTP_CLIENT_IP'];

    if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
        return $_SERVER['HTTP_X_FORWARDED_FOR'];

    return $_SERVER['REMOTE_ADDR'];
}

?>