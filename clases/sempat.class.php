<?php

class sempat {

    /** LOGIN **/
    
    function logIn($usuarioNombre, $usuarioPassword){
        $stmt = mssql_init("admLogin");
        mssql_bind($stmt, '@login', $usuarioNombre, SQLVARCHAR);
        mssql_bind($stmt, '@password', $usuarioPassword, SQLVARCHAR);
        $result = sql_db::sql_ejecutar_sp($stmt);
        $row = sql_db::sql_fetch_assoc($result);
        sql_db::sql_close();
        return $row;      
    }

    
    function logOn($usuarioId, $ip){
        $stmt = mssql_init("admLoginIp");
        mssql_bind($stmt, '@login', $usuarioId, SQLINT4);
        mssql_bind($stmt, '@ip', $ip, SQLVARCHAR);
        $result = sql_db::sql_ejecutar_sp($stmt);                
        return $result;      
    }
    
    
    function recuperarPassword($usuarioLogin) {
        $query = " Exec admRecuperarPassword '" . $usuarioLogin . "'";
		//echo $query;
        $result = sql_db::sql_query($query);
        sql_db::sql_close();
        return $result;
    }
    
    
    function guardarPassword($usuarioId, $envioEstado, $envioComentado) {
        $query = "  Update u set    u.envioEstado = '" . $envioEstado . "',
                                    u.envioFecha = GETDATE(),
                                    u.envioComentario = '" . $envioComentado . "'				
                    From Usuarios u
                    Where   u.usuarioId = '" . $usuarioId . "'";
        $result = sql_db::sql_query($query);
        sql_db::sql_close();
        return $result;
    }
    
    
    function cambiarPassword($usuarioId, $pass) {
        $stmt = mssql_init("admCambiarPassword");
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLVARCHAR);
        mssql_bind($stmt, '@usuarioPass', $pass, SQLVARCHAR);
        $result = sql_db::sql_ejecutar_sp($stmt);                
        return $result;  
    }
	
	function listaUsuarios(){
        $query = "Select u.usuarioId, u.usuarioNombre, usuarioEmail From Usuarios u (nolock) Where u.usuarioEstado = 1";
        $result = sql_db::sql_query($query);
        sql_db::sql_close();
        return $result;
    }
    
    
        
    /** MENU **/
    
    function menuRetornarHtml($usuarioId, $menuId) {
	
		$objEst = new sempat();
        $resultado = $objEst->menuSeleccionarModulosMenusUsuarios($usuarioId);        
        $totalModulosUsuario = 0;

        $data = array();
        $modulosArray = array();
        $modulosArrayU = array();
        
        $totalFilas = mssql_num_rows($resultado);
        $i= 0;

        while ($rowdatos = mssql_fetch_array($resultado)) {
            
            $modulosArray[$i][0] = $rowdatos[0];
            $data[] = $rowdatos;                                    
            $i++;            
        }
//                        
//        $modulosArray = array_unique($modulosArray);     
             
                                                       
        $htmlThead = "<thead><tr>";
        $htmlTbody = "<tbody><tr>";

        $numModulos = 0;
        $numModulosV = 0;
        $numMenus = 0;
        $subMenuHtml = "";
        $existe = 0;
        $urlMenu = "";
        $totalModulosCliente = 0;
        $existe = 1;       
        
        $moduloIdS = $_SESSION['SEMPAT_moduloD'];
        
        for($i = 0; $i < $totalFilas; $i++){

            $numMenus = $data[$i][3];
            $moduloId = $data[$i][0];
            $numModulos++;                                                
            $numModulosV++;                        
            
            if($numModulos <= 5 && ($moduloId == $moduloIdS || $moduloIdS == "")){                                
                
                $moduloIdS = "";
                for($j = 0; $j < $numMenus; $j++){

                    if($j == 0){
                        $urlMenu = "../../" . htmlentities($data[$i][6]);
                    }

                    if($data[$i + $j][4] == $menuId){
                        $menuDisponible = 1;    
                        //$existe = 1;
                        $_SESSION['SEMPAT_menuId'] = $menuId;
                        
                        $subMenuHtml = $subMenuHtml . "<a href=\"../../" . htmlentities($data[$i + $j][6]) . "\" class=\"aSelect\">" . htmlentities($data[$i + $j][5]) . "</a> | ";
                    }
                    else{
                        $subMenuHtml = $subMenuHtml . "<a href=\"../../" . htmlentities($data[$i + $j][6]) . "\" >" . htmlentities($data[$i + $j][5]) . "</a> | ";
                    }

                }

                $subMenuHtml = substr($subMenuHtml, 0, (strlen($subMenuHtml) - 2));

                if($numModulos == 1){

                    if($menuDisponible == 1){
                        $htmlThead .= "<th class=\"thLeftSelect\" onClick=\"window.location.href='" . $urlMenu . "'\"><a href=\"" . $urlMenu . "\" class=\"labelSelect\">" . strtoupper(htmlentities($data[$i][1])) . "</a></th>";
                        $htmlTbody .= "<td class=\"tdSubMenuLeft\">" . $subMenuHtml . "</td>";
                    }
                    else{
                        $htmlThead .= "<th class=\"thLeft\" onClick=\"window.location.href='" . $urlMenu . "'\"><a href=\"" . $urlMenu . "\">" . strtoupper(htmlentities($data[$i][1])) . "</a></th>";
                        $htmlTbody .= "<td></td>";
                    }
                }

                if($numModulos > 1 && $numModulos < 5){

                    if($menuDisponible == 1){
                        $htmlThead .= "<th class=\"thCenterSelect\" onClick=\"window.location.href='" . $urlMenu . "'\"><a href=\"" . $urlMenu . "\" class=\"labelSelect\">" . strtoupper(htmlentities($data[$i][1])) . "</a></th>";
                        $htmlTbody .= "<td class=\"tdSubMenuCenter\">" . $subMenuHtml . "</td>";
                    }
                    else{
                        $htmlThead .= "<th class=\"thCenter\" onClick=\"window.location.href='" . $urlMenu . "'\"><a href=\"" . $urlMenu . "\">" . strtoupper(htmlentities($data[$i][1])) . "</a></th>";
                        $htmlTbody .= "<td></td>";
                    }
                }

                if($numModulos >= 5){

                    if($menuDisponible == 1){
                        $htmlThead .= "<th class=\"thRightSelect\" onClick=\"window.location.href='" . $urlMenu . "'\"><a href=\"" . $urlMenu . "\" class=\"labelSelect\">" . strtoupper(htmlentities($data[$i][1])) . "</a></th>";
                        $htmlTbody .= "<td class=\"tdSubMenuRight\">" . $subMenuHtml . "</td>";                                                                                
                    }
                    else{
                        $htmlThead .= "<th class=\"thRight\" onClick=\"window.location.href='" . $urlMenu . "'\"><a href=\"" . $urlMenu . "\">" . strtoupper(htmlentities($data[$i][1])) . "</a></th>";
                        $htmlTbody .= "<td></td>";
                    }                                
                }
                
                $totalModulosCliente++;                
                $modulosArrayU[$numModulosV - 1][1] = "1";
                
            }
            else{                
                $numModulos--;
                $modulosArrayU[$numModulosV - 1][1] = "0";
                
            }
            
            $i = $i + $numMenus - 1;
            $menuDisponible = 0;
            $subMenuHtml = "";
            $modulosArrayU[$numModulosV - 1][0] = $moduloId;
            
            
        }

        /** MENU COMPLETO **/

        for($m = $totalModulosCliente; $m < 4; $m++){
            $htmlThead .= "<th class=\"thCenter\"></th>";
            $htmlTbody .= "<td></td>";
            $totalModulosCliente = $m;
        }

        if($totalModulosCliente < 5){
            $htmlThead .= "<th class=\"thRight\"></th>";
            $htmlTbody .= "<td></td>";
        }

        $htmlThead .= "</tr></thead>";
        $htmlTbody .= "</tr></tbody>";               

        /************/
        
//        echo $modulosArrayU[0][0] . " - " . $modulosArrayU[0][1] . "<br/>";
//        echo $modulosArrayU[1][0] . " - " . $modulosArrayU[1][1] . "<br/>";
//        echo $modulosArrayU[2][0] . " - " . $modulosArrayU[2][1] . "<br/>";
//        echo $modulosArrayU[3][0] . " - " . $modulosArrayU[3][1] . "<br/>";
//        echo $modulosArrayU[4][0] . " - " . $modulosArrayU[4][1] . "<br/>";
//        echo $modulosArrayU[5][0] . " - " . $modulosArrayU[5][1] . "<br/>";
//        echo $modulosArrayU[6][0] . " - " . $modulosArrayU[6][1] . "<br/>";
//        echo $modulosArrayU[7][0] . " - " . $modulosArrayU[7][1] . "<br/>";
//        echo $modulosArrayU[8][0] . " - " . $modulosArrayU[8][1] . "<br/>";                
                
        if($existe == 1){
            
            if(count($modulosArrayU) > 5){
                
                $mod1 = 0;
                
                for($i = 0; $i <= count($modulosArrayU); $i++){
                    
                    if($modulosArrayU[$i][1] == "1"){                        
                        $mod1 = $i;
                        $i = count($modulosArrayU);
                    }                                    
                }                                
                
                if($modulosArrayU[$mod1 - 1][1] != ""){
                    $flechaIzq = "<input type='image' style='position:absolute;left:2px;top:6px; width:11px; height:22px' src='../../imagenes/flecha_01.png' onclick='reloadMenu(" . $modulosArrayU[$mod1 - 1][0] . ")'>";
                }                                
                
                if($modulosArrayU[$mod1 + 4][1] != ""){
                    $flechaDer = "<input type='image' style='position:absolute;left:1145px;top:6px; width:11px; height:22px' src='../../imagenes/flecha_02.png' onclick='reloadMenu(" . $modulosArrayU[$mod1 + 1][0] . ")'>";
                }                     
                
            }
            
            $htmlTabla = "<div id='divMenu'><table class=\"tablaMenu\" cellspadding=\"0\" cellspacing=\"0\">";
            
            return "<div id='divMenuS'>" . $flechaIzq . $htmlTabla . $htmlThead . $htmlTbody . "</table></div>" . $flechaDer . "</div>";
        }
        else{
            return "<script>window.location='../../index.php';</script>";
        }

    }
    
    
    function menuSeleccionarModulosMenusUsuarios($usuarioId){
        $query = "Exec admSeleccionarModulosMenusUsuarios '" . $usuarioId . "'";
        //echo $query;
        $result = sql_db::sql_query($query);
        sql_db::sql_close();
        return $result;                
    }
    
       
    /** CABECERA INDICADORES **/
    
        
    function indicadoresCabeceraDatos($tipo, $clienteId) {
        $query = "Exec cabeceraTablaIndicadoresYTD '" . $tipo . "', '" . $clienteId . "'";        
        //echo $query;
        $result = sql_db::sql_query($query);        
        $row = sql_db::sql_fetch_assoc($result);
        sql_db::sql_close();
        return $row;        
    }
    
    
    function indicadoresCabeceraHtml($tipo, $clienteId, $valorNivel1){
        
        $objSem = new sempat();
        $resultado = $objSem->indicadoresCabeceraDatos($tipo, $clienteId);
        
        $titulo1 = "ENERO";
        $titulo2 = "";
        $titulo3 = "";
        $titulo2b = "";
        $titulo3b = "";
        
        if(strtoupper($resultado["mmActual"]) != "ENERO"){
            $titulo1 = "ENERO - " . strtoupper($resultado["mmActual"]);
        }

        if($resultado["mmAnterior"] != "" && $resultado["mmActual"] != ""){
            $titulo2 = $resultado["mmAnterior"] . " - " . $resultado["mmActual"];
        }
        
        if($resultado["mmActual"] != ""){
            $titulo3 = $resultado["mmActual"];
        }
        
        if($resultado["mmActual"] == "DICIEMBRE"){
            $titulo2b = $resultado["yyyyActual"];
        }
        else{
            if($resultado["yyyyAnterior"] != ""){
                $titulo2b = $resultado["yyyyAnterior"] . " - " . ($resultado["yyyyActual"]);
            }            
        }
        
        if($resultado["mmActual"] == "DICIEMBRE") {
            $titulo3b = $resultado["yyyyAnterior"];            
        }
        else {            
            if($resultado["yyyyAnterior"] != ""){
                $titulo3b = ($resultado["yyyyAnterior"] - 1) . " - " . ($resultado["yyyyAnterior"]);                
            }                        
        }
        
        
        $htmlThead = "<tr>
                        <th class=\"thCenter\" colspan=\"6\">
                            <div class=\"divThLeft\"></div>
                            <div class=\"divThCenter\">" . $titulo1 . "</div>
                            <div class=\"divThRight\"></div>          
                        </th>                        
                        <th style=\"min-width: 10px;\"></th>
                        <th class=\"thCenter\" colspan=\"4\">
                            <div class=\"divThLeft\"></div>          
                            <div class=\"divThCenter\">" . $titulo2 . "</div>
                            <div class=\"divThRight\"></div>          
                        </th> 
                        <th style=\"min-width: 10px;\"></th>
                        <th class=\"thCenter\" colspan=\"4\">
                            <div class=\"divThLeft\"></div>          
                            <div class=\"divThCenter\">" . $titulo3 . "</div>
                            <div class=\"divThRight\"></div>          
                        </th> 
                    </tr>   
                    <tr>
                        <td colspan=\"2\" class=\"left\" style=\"width:185px;\">
                                " . htmlentities($valorNivel1) .  "
                        </td>
                        <td>
                            " . $resultado["yyyyActual"] . "
                        </td>
                        <td>
                            " . $resultado["yyyyAnterior"] . "
                        </td>
                        <td>
                            DIF.
                        </td>
                        <td>
                            Mkt. SHARE
                        </td>
                        <td class=\"clear\">

                        </td>
                        <td class=\"left\">
                            " . $titulo2b .  "
                        </td>
                        <td>
                            " . $titulo3b .  "
                        </td>
                        <td>
                            DIF.
                        </td>
                        <td>
                            Mkt. SHARE
                        </td>
                        <td class=\"clear\">

                        </td>
                        <td class=\"left\">
                            " . $resultado["yyyyActual"] . "
                        </td>
                        <td>
                            " . $resultado["yyyyAnterior"] . "
                        </td>
                        <td>
                            DIF.
                        </td>
                        <td>
                            Mkt. SHARE
                        </td>                        
                    </tr>";

        return $htmlThead;       

    }
    
        
    /** CALCULOS **/
    
        
    function calculoDiferencia($numerador, $denominador){
    
        $resultadoFont = "";

        /*if ($denominador != 0) {
            $resultado = ((($numerador / $denominador) - 1) * 100);
            $resultado = number_format($resultado, 2);
            if ($resultado > 0) {
                $resultadoFont = '<td class="positivo">' . $resultado . ' %</td>';
            } else {
                $resultadoFont = '<td class="negativo">' . $resultado . ' %</td>';
            }
        } else {
            if ($numerador > 0) {
                $resultadoFont = '<td class="positivo">100.00 %</td>';
            } else {
                $resultadoFont = '<td class="negativo">0.00 %</td>';
            }
        }*/
		
		if ($denominador != 0) {
            $resultado = ((($numerador / $denominador) - 1) * 100);
            $resultadoA = number_format($resultado, 2);
            $resultadoB = number_format($resultado, 0, '', '');
                        
            if ($resultado > 0) {
                                
                if(strlen($resultadoB) > 4){
                    $resultadoFont = '<td class="positivo" title="' . $resultadoA . ' %">' . substr($resultadoA, 0, 5) . "..." . ' %</td>';
                }
                else{
                    $resultadoFont = '<td class="positivo">' . $resultadoA . ' %</td>';
                }

            } else {
                
                if(strlen($resultadoB) > 4){
                    $resultadoFont = '<td class="negativo" title="' . $resultadoA . ' %">' . substr($resultadoA, 0, 5) . "..." . ' %</td>';
                }
                else{
                    $resultadoFont = '<td class="negativo">' . $resultadoA . ' %</td>';
                }
                
            }
        } else {
            
            if ($numerador > 0) {
                $resultadoFont = '<td class="positivo">100.00 %</td>';
            } else {
                $resultadoFont = '<td class="negativo">0.00 %</td>';
            }
            
        }

        return $resultadoFont;    
    }


    function calculoMkt($numerador, $denominador){

        $resultadoFont = "";

        if ($denominador != 0) {
            $resultado = (($numerador / $denominador) * 100);
            $resultado = number_format($resultado, 0);
            $resultadoFont = '<td style="text-align:center">' . $resultado . ' %</td>';        
        } else {
            $resultadoFont = '<td style="text-align:center"> 0 %</td>';        
        }

        return $resultadoFont;    
    }
    
        
    /** FILTROS **/

        
    function admBuscarFiltros($clienteId, $moduloId){
        $query = " Exec admBuscarFiltros '" . $clienteId . "','" . $moduloId . "'";
        //echo $query;
        $result = sql_db::sql_query($query);
        sql_db::sql_close();
        return $result;
    }
    
        
    /** FECHAS DEFECTO **/
    
        
    function manifiestoFechaDefecto($mercado, $clienteId){
        $query = "Exec manifiestoFechasDefecto '" . $mercado . "','" . $clienteId . "'";
		//echo $query;
        $result = sql_db::sql_query($query);
        sql_db::sql_close();
        $row = sql_db::sql_fetch_assoc($result);
        return $row;                        
    }
	
	function clientesFechasLimite($clienteId, $fechaDesde, $fechaHasta){
        $query = "Exec clientesFechasLimite '" . $clienteId . "','" . $fechaDesde . "','" . $fechaHasta . "'";
		//echo $query;
        $result = sql_db::sql_query($query);
        sql_db::sql_close();
        $row = sql_db::sql_fetch_assoc($result);
        return $row;                        
    }
	
	
        
    /** EXTRACCIONES **/
    
    
    function extraccionesArchivos($usuarioId, $moduloId) {
        $query = "Select * From Extraccion_Reportes (nolock) Where usuarioId = '" . $usuarioId . "' and modulo = '" . $moduloId . "' Order by fecha_reporte desc";
        //echo $query;
        $result = sql_db::sql_query($query);
        sql_db::sql_close();        
        return $result;
    }
    
        
    function extraccionesEliminarArchivo($usuarioId, $moduloId, $archivoId) {        
        
        $query = "Select nombre_archivo From Extraccion_Reportes (nolock) Where usuarioId = '" . $usuarioId . "' and modulo = '" . $moduloId . "' and id_archivo = '" . $archivoId . "'";                
        //echo $query;
		$result = sql_db::sql_query($query);
        $row = sql_db::sql_fetch_assoc($result);
        sql_db::sql_close();        
		
		$nombreArchivo = $row["nombre_archivo"];
                        
        $query = "Delete e From Extraccion_Reportes e (nolock) Where usuarioId = '" . $usuarioId . "' and modulo = '" . $moduloId . "' and nombre_archivo = '" . $nombreArchivo . "'";        
        sql_db::sql_query($query);        
        sql_db::sql_close(); 
        
        return $nombreArchivo;
        
    }
    
        
    
    /** MERCADOS **/
    
    
    function mercadosDisponibles($mercados, $ancho){
                        
        $tdHtml = "";
                        
        if($mercados == ""){
            $tdHtml = "<td valign='top' width='" . $ancho . "'><label class='niveles'>MERCADO</label><table>";
        }
        else{
            $tdHtml = "<td valign='top' width='" . $ancho . "' style='display:none'><label class='niveles'>MERCADO</label><table>";
        }
        
        switch($mercados){
            
            case "":
                $tdHtml .= "<tr>
                                <td>
                                    <input id='btnChile' type='radio' value='1'>
                                </td> 
                                <td>
                                    Chile
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input id='btnPeru' type='radio' value='2'>
                                </td>                                        
                                <td>
                                    Perú
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input id='btnEcuador' type='radio' value='3'>
                                </td>                                        
                                <td>
                                    Ecuador
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input id='btnColombia' type='radio' value='4'>
                                </td>                                        
                                <td>
                                    Colombia
                                </td>
                            </tr>";
                break;
            
            case "1":
                $tdHtml .= "<tr>
                                <td>
                                    <input id='btnChile' type='radio' value='1' checked='checked'>
                                </td> 
                                <td>
                                    Chile
                                </td>
                            </tr>";
                break;
            
            case "2":
                $tdHtml .= "<tr>
                                <td>
                                    <input id='btnPeru' type='radio' value='2' checked='checked'>
                                </td> 
                                <td>
                                    Perú
                                </td>
                            </tr>";
                break;
            
            case "3":
                $tdHtml .= "<tr>
                                <td>
                                    <input id='btnEcuador' type='radio' value='3' checked='checked'>
                                </td> 
                                <td>
                                    Ecuador
                                </td>
                            </tr>";
                break;
            
            case "4":
                $tdHtml .= "<tr>
                                <td>
                                    <input id='btnColombia' type='radio' value='4' checked='checked'>
                                </td> 
                                <td>
                                    Colombia
                                </td>
                            </tr>";
                break;
            
        }
        
        $tdHtml .= "</table></td>";
        
        return $tdHtml;        
        
    }
    
}

?>


