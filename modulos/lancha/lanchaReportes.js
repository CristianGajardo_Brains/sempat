
var count = 0;


function filtros(){

    var filtros = '';
    var mensaje = '';
    count = 0;

    filtros += filtrosValores($("#agencia"), "Agencia");
    filtros += filtrosValores($("#puerto"), "Puerto");
    filtros += filtrosValores($("#tipoNave"), "Tipo Nave");
    filtros += filtrosValores($("#tipoFaena"), "Tipo Faena");
    filtros += filtrosValores($("#tipoServicio"), "Tipo Servicio");
        
    if(count > 0){
        mensaje = "<font color='#3D85FE' size='2'>Filtros:</font><br>"                
    }
                    
    if(count > 4){
        mensaje += "<div style='height:60px; overflow-y: scroll'>" + filtros + "</div>";
        
    }
    else{
        mensaje += "<div style='height:60px'>" + filtros + "</div>";
    }

    return  mensaje;


}


function buscarNivel1(){

    var nivel1 = $("#selectNivel1").val();
    var nivel2 = $("#selectNivel2").val();
    var nivel3 = $("#selectNivel3").val();        

    var fechaDesde = $("#fechaDesde").val();
    var fechaHasta = $("#fechaHasta").val();


    if(fechaDesde == "" && fechaHasta == ""){

        fechasDefecto('PRACTICAJE'); 
        fechaDesde = $("#fechaDesde").val();
        fechaHasta = $("#fechaHasta").val();

        //fechaDesde = $("#fechaDesdeDef").val();
        //fechaHasta = $("#fechaHastaDef").val();
        //$("#fechaDesde").val(fechaDesde);
        //$("#fechaHasta").val(fechaHasta);

    }

    if(nivel1 != ""){

        $("#divResultado").html("");  
        $("#tablaNivelesDesc").html("");   

        $("#imgCargando").css("display","block");
        $("#divPanelFiltros").css("display","none");
        $("#divOpciones").css("display","none");
        $("#divGraficos").css("display","none");
        $("#divGrafico").css("display","none");
        $("#divGraficoA").css("display","none");
        $("#divGraficoB").css("display","none");
        $("#divMensaje").css("display","none");

        var agencia = $("#agencia").val();
        var puerto = $("#puerto").val();
        var nave = $("#nave").val();
        var tipoNave = $("#tipoNave").val();
        var tipoFaena = $("#tipoFaena").val();
        var tipoServicio = $("#tipoServicio").val();
        
        var nivel1Titulo = $("#selectNivel1 option:selected").text();

        

        $('#selectGraficoReporte').val("line");
        $('#selectselectGrafico').html("Líneas");
        
                        
        var tipoDato = $("input[name=tipoDato]:checked").val();

        switch(tipoDato){
            case 'faenas':
                $('#tituloGrafico').val("Cantidad de Faenas Operativas");
                $('#tituloGraficoEjeY').val("N° de Faenas");                
                $('.tdTipo').css("display", "");
                $('#decimales').val("0");
                break;

            case 'remolcadores':
                $('#tituloGrafico').val("Cantidad de Lanchas");
                $('#tituloGraficoEjeY').val("N° de Lanchas");
                $('.tdTipo').css("display", "none");
                $('#decimales').val("0");
                break;                           
        }
        
        
        $('#tdResumen').html("<center><font color='#3D85FE' size='3'>" + $('#tituloGrafico').val() + "</font></center><br>" + filtros());

        $.ajax({
            type: "POST",
            url: "lanchaReporteNivel1.php",
            data: { fechaDesde: fechaDesde, fechaHasta: fechaHasta, agencia: agencia, puerto: puerto, nave: nave,
                tipoNave: tipoNave, tipoFaena: tipoFaena, tipoServicio: tipoServicio,                
                nivel1: nivel1, nivel2: nivel2, nivel3: nivel3, nivel1Titulo: nivel1Titulo, tipoDato: tipoDato}
            })
            .done(function( msg ) {

                if(msg.trim() != ""){
                                    
                    $("#divResultado").html(msg);                 
                    $("#divOpciones").css("display","block");

                    $("#tBody tr").each(function (index) {

                        if(index < 3){

                            $(this).children("td").each(function (index2, td) {

                                if(index2 == 0){

                                    $(td).children("input").each(function (index3, input) {
                                        input.click();
                                    });

                                    $(td).children("span ").each(function (index3, span) {
                                        $(span).removeClass('checkboxC');
                                        $(span).addClass('checkboxChecked');
                                    });

                                }                                                
                            });
                        }                    
                    });


                    var nivelesDesc = "";                

                    if(nivel1 != "")
                        nivelesDesc = nivelesDesc + "<tr><td><div class='nivel1'></div></td><td class='desc'>Nivel 1: " + nivel1Titulo + "</td></tr>"; 

                    if(nivel2.trim() != ""){
                        var nivel2Titulo = $("#selectNivel2 option:selected").text();
                        nivelesDesc = nivelesDesc + "<tr><td><div class='nivel2'></div></td><td class='desc'>Nivel 2: " + nivel2Titulo + "</td></tr>"; 
                    }

                    if(nivel3 != ""){
                        var nivel3Titulo = $("#selectNivel3 option:selected").text();
                        nivelesDesc = nivelesDesc + "<tr><td><div class='nivel3'></div></td><td class='desc'>Nivel 3: " + nivel3Titulo + "</td></tr>"; 
                    }                                 

                    Custom.initChkbox();
                    
                    $("#divOpciones").css("display","block");
                    $("#tablaNivelesDesc").html(nivelesDesc);
                }
                else{
                    $('#divResultado').html("<div style='margin-left:30px'><label style='color:red'>Búsqueda sin resultados.</label></div>");
                }

                $("#imgCargando").css("display","none");

        }); 

    }
    else{
        $("#divMensaje").css("display","block");
    }                                                                  

}


function buscarNivel2(obj, nivel2, nivel3, campo1, tipoDato, nMeses){

    var tr = obj.parentNode;  
    var className = $(tr).hasClass('OK');                

    if(className == false && nivel2 != ""){

        $.ajax({
            type: "POST",
            url: "lanchaReporteNivel2.php",
            async: false,
            data: {nivel3: nivel3, campo1: campo1, tipoDato: tipoDato, nMeses: nMeses, trId: $(tr).attr('id')}
            })
            .done(function(msg ) {
                $(tr).addClass('OK');            
                $(msg).insertAfter(tr);
        });                
    }
    else{                        
        desplegarNivel($(tr).attr('id'), 2);
    }
}


function buscarNivel3(obj, nivel3, campo1, campo2, tipoDato, nMeses){

    var tr = obj.parentNode;  
    var className = $(tr).hasClass('OK');                

    if(className == false && nivel3 != ""){

        $.ajax({
            type: "POST",
            url: "lanchaReporteNivel3.php",
            async: false,
            data: {campo1: campo1, campo2: campo2, tipoDato: tipoDato, nMeses: nMeses, trId: $(tr).attr('id')}
            })
            .done(function(msg ) {
                $(tr).addClass('OK');            
                $(msg).insertAfter(tr);
        });                
    }
    else{                        
        desplegarNivel($(tr).attr('id'), 3);
    }
}


function validarFormulario(){

    var mensaje = "";

    var nivel1 = $("#selectNivel1").val();
    var fechaDesde = $("#fechaDesde").val();
    var fechaHasta = $("#fechaHasta").val();
            
    if(nivel1 == ""){
        mensaje = "Debe seleccionar el primer nivel.<br>";
    }
    
    if(fechaDesde == ""){
        mensaje += "No ha ingresado la fecha inicial.<br>";
    }
    
    if(fechaHasta == ""){
        mensaje += "No ha ingresado la fecha final.<br>";
    }
    
    if(fechaDesde != "" && fechaHasta != "" && validarFechaMenor(fechaDesde, fechaHasta) == false){
        mensaje += "La fecha incial no puede ser mayor a la fecha final.<br>";
    }
    
    if(fechaDesde != "" && fechaHasta != "" && fechaDiferenciasDias(fechaDesde, fechaHasta) > 365){
        mensaje += "La diferencia entre las fechas no puede ser superior a un año.<br>";
    }
            
    if(mensaje != ""){
        $("#divMensaje").html(mensaje);
        $("#divMensaje").css("display","block");
        return false;
    }
    else{
        $("#divMensaje").html("");
        $("#divMensaje").css("display","none");
        return true;
    }

}
    
    
    