<?php

header('P3P: CP="IDC DSP COR CURa ADMa OUR IND PHY ONL COM STA"');
session_start();

include("../../default.php");

require('lancha.class.php');
$objlancha = new lancha();

$clienteId = $_SESSION['SEMPAT_clienteId'];
$usuarioId = $_SESSION['SEMPAT_usuarioId'];

$grafico = $objlancha->lanchaIndicadoresGrafico(1, $clienteId);
$grafico2 = $objlancha->lanchaIndicadoresGrafico(2, $clienteId);

// <editor-fold>
if ($grafico) {

    $htmlTablaAgencia = "<table id='tablaAgenciaPuerto'><thead><tr><th></th>";
    $htmlTablaGeneral = "<table id='tablaGeneral'><thead><tr><th></th><th>TOTAL GENERAL</th></tr></thead><tbody>";
    $htmlTablaGrafico = "<table id='tablaGrafico'><thead><tr><th></th></tr></thead><tbody>";
            
    $totalColumnas = mssql_num_fields($grafico);
    $subTotal = 0;

    for ($j = 3; $j < $totalColumnas; $j++) {
        $field = mssql_fetch_field($grafico, $j);
        $htmlTablaAgencia = $htmlTablaAgencia . "<th>" . htmlentities($field->name) . "</th>";
    }

    $htmlTablaAgencia = $htmlTablaAgencia . "</thead><tbody>";

    while ($graficoData = mssql_fetch_array($grafico)) {

        $htmlTablaAgencia = $htmlTablaAgencia . "<tr><th>" . $graficoData[2] . "</th>";
        $htmlTablaGeneral = $htmlTablaGeneral . "<tr><th>" . $graficoData[2] . "</th>";
        $htmlTablaGrafico = $htmlTablaGrafico . "<tr><th>" . $graficoData[2] . "</th>";
        $subTotal = 0;

        for ($j = 3; $j < $totalColumnas; $j++) {

            $htmlTablaAgencia = $htmlTablaAgencia . "<td>" . $graficoData[$j] . "</td>";
            $subTotal = $subTotal + $graficoData[$j];
        }

        $htmlTablaAgencia = $htmlTablaAgencia . "</tr>";
        $htmlTablaGeneral = $htmlTablaGeneral . "<td>" . $subTotal . "</td></tr>";
        $htmlTablaGrafico = $htmlTablaGrafico . "</tr>";
    }

    $htmlTablaAgencia = $htmlTablaAgencia . "</tbody></table>";
    $htmlTablaGeneral = $htmlTablaGeneral . "</tbody></table>";
    $htmlTablaGrafico = $htmlTablaGrafico . "</tbody></table>";
}
// </editor-fold>

// <editor-fold>
if ($grafico2) {

    $htmlTablaPuerto = "<table id='tablaPuertoAgencia'><thead><tr><th></th>";

    $totalColumnas = mssql_num_fields($grafico2);

    for ($j = 3; $j < $totalColumnas; $j++) {
        $field = mssql_fetch_field($grafico2, $j);
        $htmlTablaPuerto = $htmlTablaPuerto . '<th>' . htmlentities($field->name) . '</th>';
    }

    $htmlTablaPuerto = $htmlTablaPuerto . "</thead><tbody>";

    while ($graficoData2 = mssql_fetch_array($grafico2)) {

        $htmlTablaPuerto = $htmlTablaPuerto . "<tr><th>" . $graficoData2[2] . "</th>";

        for ($j = 3; $j < $totalColumnas; $j++) {

            $htmlTablaPuerto = $htmlTablaPuerto . "<td>" . $graficoData2[$j] . "</td>";
        }

        $htmlTablaPuerto = $htmlTablaPuerto . "</tr>";
    }

    $htmlTablaPuerto = $htmlTablaPuerto . "</tbody></table>";
}
// </editor-fold>


$htmlTablasGraficos = " <table id=\"mktYtd2\">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>

                        <table id=\"mktYtd1\">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>

                        <table id=\"mkt12M2\">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>

                        <table id=\"mkt12M1\">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>

                        <table id=\"mktM2\">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>

                        <table id=\"mktM1\">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>                    
                        </table>";

echo $htmlTablaGrafico;
echo $htmlTablaGeneral;
echo $htmlTablaAgencia;
echo $htmlTablaPuerto;

echo $htmlTablasGraficos; 


mssql_free_result($grafico);
mssql_free_result($grafico2);



?>
