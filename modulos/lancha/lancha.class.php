<?php

    class lancha {

    //constructor
    
        /******************************************************/
        /********* lancha Tipo I Agencia - Puerto  *****/
        /******************************************************/

        function lanchaIndicadoresGrafico($tipo, $clienteId) {
            $query = "Exec lanchaIndicadoresGrafico " . $tipo . "," . $clienteId;        
            //echo $query;
            $result = sql_db::sql_query($query);
            sql_db::sql_close();
            return $result;
        }

        function lanchaIndicadores($tipoOrden, $tipoBusqueda, $clienteId) {
            $query = "Exec lanchaIndicadores " . $tipoOrden . ", '" . $tipoBusqueda . "'," . $clienteId . ", '', ''";
            //echo $query;
            $result = sql_db::sql_query($query);
            sql_db::sql_close();
            return $result;
        }

        function lanchaListaPuertos($clienteId) {
            $query = "  Select distinct i.puerto, i.yyyy, i.mm
                        From IndicadoreslanchasTipoI i (nolock)
                        Where   i.yyyy = (Select MAX(CAST(yyyy as int)) From IndicadoreslanchasTipoI)
                        and     i.mm = (Select MAX(CAST(mm as int)) From IndicadoreslanchasTipoI i2 Where i2.yyyy = i.yyyy)
                        and     i.puerto <> ''
                        and     i.clienteId = " . $clienteId;
            $result = sql_db::sql_query($query);
            sql_db::sql_close();
            return $result;
        }


        function lanchaComercial2($clienteId, $yyyy, $mm) {

            if($yyyy == "" || $mm == ""){
                $query = "  Select *
                            From IndicadoresComercialesI ic (nolock)
                            Where   ic.yyyy = (Select MAX(CAST(yyyy as int)) From IndicadoreslanchasTipoI)
                            and     ic.mm = (Select MAX(CAST(mm as int)) From IndicadoreslanchasTipoI i2 Where i2.yyyy = ic.yyyy)
                            and     ic.clienteId = " . $clienteId;
            }
            else{
                $query = "  Select *
                            From IndicadoresComercialesI ic (nolock)
                            Where   ic.yyyy = '" . $yyyy . "'
                            and     ic.mm = '" . $mm . "'
                            and     ic.clienteId = " . $clienteId;
            }

            $result = sql_db::sql_query($query);
            $row = sql_db::sql_fetch_assoc($result);
            sql_db::sql_close();
            return $row;
        }

        /******************************************************/
        /************* lancha Tipo II ******************/
        /******************************************************/

        function lanchaTipoIIData($nombreSP, $fechaDesde, $fechaHasta, $campo1, $campo2, $campo3, $periodos1, $periodos2, $clienteId, $agenciaNombre, $puertoNombre, $naveNombre, $tipoNaveNombre, $tipoFaenaNombre, $tipoServicioNombre, $usuarioId) {
            /*$query = "Exec " . $nombreSP . " '" . $fechaDesde . "','" . $fechaHasta . "','" . $campo1 . "','" . $campo2 . "','" . $campo3 . "','" . $periodos1 . "','" . $periodos2 . "'," . $clienteId . ", '" . trim($agenciaNombre) . "', '" . trim($puertoNombre) . "', '" . trim($tipoNaveNombre) . "', '" . trim($tipoFaenaNombre) . "', '" . trim($tipoServicioNombre) . "', " . $usuarioId . "";
            echo $query;*/

            $stmt = mssql_init($nombreSP);

            mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
            mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);
            mssql_bind($stmt, '@var1', $campo1, SQLVARCHAR);
            mssql_bind($stmt, '@var2', $campo2, SQLVARCHAR);
            mssql_bind($stmt, '@var3', $campo3, SQLVARCHAR);        
            mssql_bind($stmt, '@mes1', $periodos1, SQLTEXT);
            mssql_bind($stmt, '@mes2', $periodos2, SQLVARCHAR);
            mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
            mssql_bind($stmt, '@agencia', $agenciaNombre, SQLVARCHAR);
            mssql_bind($stmt, '@puerto', $puertoNombre, SQLVARCHAR);
            mssql_bind($stmt, '@nave', $naveNombre, SQLVARCHAR);
            mssql_bind($stmt, '@tipoNave', $tipoNaveNombre, SQLVARCHAR);
            mssql_bind($stmt, '@tipoFaena', $tipoFaenaNombre, SQLVARCHAR);
            mssql_bind($stmt, '@tipoServicio', $tipoServicioNombre, SQLVARCHAR);            
            mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

            $result = sql_db::sql_ejecutar_sp($stmt);
            sql_db::sql_close();
            return $result;
            
        }

//        function lanchaTipoIIData($nombreSP, $fechaDesde, $fechaHasta, $campo1, $campo2, $campo3, $periodos1, $periodos2, $clienteId, $agenciaNombre, $puertoNombre, $tipoNaveNombre, $tipoFaenaNombre, $tipoServicioNombre, $usuarioId) {
//            $query = "Exec " . $nombreSP . " '" . $fechaDesde . "','" . $fechaHasta . "','" . $campo1 . "','" . $campo2 . "','" . $campo3 . "','" . $periodos1 . "','" . $periodos2 . "'," . $clienteId . ",'" . trim($agenciaNombre) . "','" . trim($puertoNombre) . "','" . trim($tipoNaveNombre) . "','" . trim($tipoFaenaNombre) . "','" . trim($tipoServicioNombre) . "', " . $usuarioId . "";
//            //echo $query;
//            $result = sql_db::sql_query($query);
//            sql_db::sql_close();
//            return $result;
//        }

        function lanchaFechasDefecto($clienteId) {

            $query = "Exec seleccionarFechasDefecto '" . $clienteId . "','P'";
            //echo $query;
            $result = sql_db::sql_query($query);        
            $row = sql_db::sql_fetch_assoc($result);
            sql_db::sql_close();
            return $row;
        }

        function lanchaMostrarNivelII($campo1, $usuarioId) {
            $query = "  Select *
                        From lanchaReporte fr (nolock)
                        Where   fr.nivel2 = 2
                        and     fr.campo1 = '" . $campo1 . "'
                        and     fr.usuarioId = '" . $usuarioId . "'
                        Order by 6 desc, 9 asc";
            $result = sql_db::sql_query($query);
            sql_db::sql_close();
            return $result;
        }

        function lanchaMostrarNivelIII($campo1, $campo2, $usuarioId) {
            $query = "  Select *
                        From lanchaReporte fr (nolock)
                        Where   fr.nivel3 = 3
                        and     fr.campo1 = '" . $campo1 . "'
                        and     fr.campo2 = '" . $campo2 . "'
                        and     fr.usuarioId = '" . $usuarioId . "'
                        Order by 7 desc, 10 asc";
            $result = sql_db::sql_query($query);
            sql_db::sql_close();
            return $result;
        }

    /******************************************************/
    /************** lancha Extracciones ************/
    /******************************************************/
  
    
    function lanchaExtracciones($fechaDesde, $fechaHasta, $tipoDato, $agencia, $puerto, $naveNombre, $tipoNave, $tipoFaena, $tipoServicio, $chkagencia, $chkpuerto, $chknave, $chktipoNave, $chktipoFaena, $chktipoServicio, $nombreArchivo, $usuarioId){
    
        
        /*$query = "Exec lanchaExtracciones '" . $fechaDesde . "','" . $fechaHasta . "','" . $tipoDato . "','" . $agentePortuario . "','" . $puerto . "','" . 
                $tipoNave . "','" . $tipoFaena . "','" . $tipoServicio . "','" . 
                
                $chkagencia . "','" . $chkpuerto . "','" . $chktipoNave . "','" . $chktipoFaena . "','" . $chktipoServicio . "','" . $nombreArchivo . "','" . $usuarioId . "'";        
        echo $query;*/
        
        
        $stmt = mssql_init("lanchaExtracciones");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);        
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        //mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@agencia', $agencia, SQLVARCHAR);
        mssql_bind($stmt, '@puerto', $puerto, SQLVARCHAR);
        mssql_bind($stmt, '@nave', $naveNombre, SQLVARCHAR);
        mssql_bind($stmt, '@tipoNave', $tipoNave, SQLVARCHAR);
        mssql_bind($stmt, '@tipoFaena', $tipoFaena, SQLVARCHAR);
        mssql_bind($stmt, '@tipoServicio', $tipoServicio, SQLVARCHAR);       
        mssql_bind($stmt, '@chkagencia', $chkagencia, SQLVARCHAR);
        mssql_bind($stmt, '@chkpuerto', $chkpuerto, SQLVARCHAR);
        mssql_bind($stmt, '@chknave', $chknave, SQLVARCHAR);
        mssql_bind($stmt, '@chktipoNave', $chktipoNave, SQLVARCHAR);
        mssql_bind($stmt, '@chktipoFaena', $chktipoFaena, SQLVARCHAR);
        mssql_bind($stmt, '@chktipoServicio', $chktipoServicio, SQLVARCHAR);        
        mssql_bind($stmt, '@nombreArchivo', $nombreArchivo, SQLVARCHAR);
        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }           

}

?>


