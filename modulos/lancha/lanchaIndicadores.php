<?
session_start();

setlocale(LC_TIME, "spanish");
$fechaActual = htmlentities(strftime("%A %d de %B del %Y"));
$fechaActual = ucfirst($fechaActual);

$usuarioLog = $_SESSION["SEMPAT_usuarioNombre"];
$usuarioId = $_SESSION["SEMPAT_usuarioId"];

if ($_SESSION['SEMPAT_usuarioId'] == "") {
    echo "<script>window.location='../../index.php';</script>";
}


include("../../default.php");
$plantilla->setPath('../../plantillas/');
$plantilla->setTemplate("header");
$plantilla->setVars(array("USUARIO" => " $usuarioLog ",
    "FECHA" => "$fechaActual"
));
echo $plantilla->show();

require('../../clases/sempat.class.php');
$objSem = new sempat();

$menuId = 7;

$htmlMenu = $objSem->menuRetornarHtml($usuarioId, $menuId);



?>

<script type="text/javascript" src="lanchaIndicadores.js"></script>

<script type="text/javascript">
    


    $(document).ready(function(){                                                    
    
        $("#imgCargando").css("display","");

        lanchaIndicadoresData('AGENCIA', 'AGENCIA');                
                        

    });

   
</script>                        

<? echo $htmlMenu; ?>


<!--        <div id="divProgressBar">Cargando<div id="progressBar"><div></div></div></div>-->

    <div class="divMinHeight">

        <input type="hidden" id="tituloGrafico" value="Cantidad de Faenas Operativas">
        <input type="hidden" id="subTituloGrafico" value="">
        <input type="hidden" id="tituloGraficoEjeY" value="Faenas (n°)">

        <br>

        <div id="divGraficos" style="height: 405px; display:none;">

            <div id="divGrafico" class="divGrafico" style="width:1122px; height:400px; display:none">

            </div>                        

            <div id="divGraficoA" class="divGraficoLeft" style="width:540px; height:400px; display:none;">

            </div>

            <div id="divGraficoB" class="divGraficoRight" style="width:540px; height:400px; display:none;">

            </div>                

        </div>

        <br style="clear:both"/>

        <div id="divOpciones" class="divOpciones" style="display:none">

            <table>
                <tr>
                    <td width="300px" valign="top">
                        <label>ORDEN</label><br>

                        <div class="divSelect">                            
                            <select id="selectOrdenA" name="selectOrdenA" class="styled" style="height:26px;" onChange="graficoOrden1('Agencia', 'Puerto');">
                                <option value="AP">Agencia - puerto</option>
                                <option value="PA">Puerto - agencia</option>
                            </select>                                
                        </div>

                        <div class="divSelect">                            
                            <select id="selectOrdenB" name="selectOrdenB" class="styled" style="height:26px;">
                                <option value="og">Geográfico</option>
                                <option value="mm">Mayor a menor</option>
                            </select>                            
                        </div>

                    </td>
                    <td id="tdResumen" valign="top">

                    </td>
                    <td width="300px" valign="top">
                        <label>GRÁFICO</label>

                        <div class="divSelect">
                            <select id="selectGrafico" name="selectGrafico" class="styled" style="height:26px;">
                                <option value="line">Líneas</option>
                                <option value="column">Columnas</option>
                                <option value="pie">Mkt. Share</option>
                            </select>
                        </div>

                        <div class="divSelect tdMkt" style="display:none">
                            <select id="selectMkt" name="selectMkt" class="styled" style="height:26px;">
                                <option value="mkt1">Acumulado del año</option>
                                <option value="mkt2">Último 12 meses</option>
                                <option value="mkt3">Mes actual</option>
                            </select>
                        </div>
                    </td>
                </tr>
            </table>                                                          

        </div>

        <center id="imgCargando" style="display:none">
            <img src="../../imagenes/cargando.gif" height="42" width="42"><br/><i>Cargando</i>
        </center>

        <table id="tablaValores" class="tablaValores" cellspacing="0" cellspadding="0" style="display:none">                

        </table>

        <div id="divDatosGrafico" style="display:none">

        </div>


    </div>

    <br style="clear:both"/>

<?
$plantilla->setPath('../../plantillas/');
$plantilla->setTemplate("footer");
echo $plantilla->show();
?>