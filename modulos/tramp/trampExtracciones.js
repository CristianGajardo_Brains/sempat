

function buscarExtracciones(){
    
    var nombreArchivo = $("#inputNombreArchivo").val();
    var carpeta = $("#carpeta").val();

    var fechaDesde = $("#fechaDesde").val();
    var fechaHasta = $("#fechaHasta").val();
    
    
    if(fechaDesde == "" && fechaHasta == ""){

        fechasDefecto("TRAMP");
        fechaDesde = $("#fechaDesde").val();
        fechaHasta = $("#fechaHasta").val();
    }
    
    if(validarFormulario()){

        $("#divResultado").html("");  

        $("#imgCargando").css("display","block");
        //$("#divPanelFiltros").css("display","none");            
        //$("#divMensaje").css("display","none");

        var puerto = $("#puerto").val();
        var nave = $("#nave").val();
        var shipper = $("#shipper").val();
        var consignee = $("#consignee").val();
        var tipoNave = $("#tipoNave").val();
        var trafico = $("#trafico").val();
        var sucursal = $("#sucursal").val();
        var paisExtranjero = $("#paisExtranjero").val();
        var puertoExtranjero = $("#puertoExtranjero").val();
        var tipoServicio = $("#tipoServicio").val();
        var tipoCarga = $("#tipoCarga").val();
        var agentePortuario = $("#agentePortuario").val();
        var naviera = $("#naviera").val();
        var granFamilia = $("#granFamilia").val();        
        var familia = $("#familia").val();
        var subFamilia = $("#subFamilia").val();
        var commodity = $("#commodity").val();
        var cargoGroup = $("#cargoGroup").val();


        var chkpuerto = $("#chkpuerto").attr('checked');
        var chknave = $("#chknave").attr('checked');
        var chkshipper = $("#chkshipper").attr('checked');
        var chkconsignee = $("#chkconsignee").attr('checked');
        var chktipoNave = $("#chktipoNave").attr('checked');
        var chktrafico = $("#chktrafico").attr('checked');
        var chksucursal = $("#chksucursal").attr('checked');
        var chkpaisExtranjero = $("#chkpaisExtranjero").attr('checked');
        var chkpuertoExtranjero = $("#chkpuertoExtranjero").attr('checked');
        var chktipoServicio = $("#chktipoServicio").attr('checked');
        var chktipoCarga = $("#chktipoCarga").attr('checked');
        var chkagentePortuario = $("#chkagentePortuario").attr('checked');
        var chknaviera = $("#chknaviera").attr('checked');
        var chkgranFamilia = $("#chkgranFamilia").attr('checked');
        var chkfamilia = $("#chkfamilia").attr('checked');
        var chksubFamilia = $("#chksubFamilia").attr('checked');
        var chkcommodity = $("#chkcommodity").attr('checked');
        var chkcargoGroup = $("#chkcargoGroup").attr('checked');

        var tipoDato = "";
        
        if($("#btnRecaladas").prop('checked')){
            tipoDato = tipoDato + "recaladas," 
        }        
        
        if($("#btnToneladas").prop('checked')){
            tipoDato = tipoDato + "toneladas," 
        }

        var etapa = $("input[name=btnEtapa]:checked").val();
                
        $.ajax({
            type: "POST",
            url: "trampExtraccionesData.php",
            data: { fechaDesde: fechaDesde, fechaHasta: fechaHasta, puerto: puerto, nave: nave, shipper: shipper, consignee: consignee, 
                tipoNave: tipoNave, trafico: trafico, sucursal: sucursal, paisExtranjero: paisExtranjero, puertoExtranjero: puertoExtranjero, tipoServicio: tipoServicio, 
                tipoCarga: tipoCarga, agentePortuario: agentePortuario, naviera: naviera, granFamilia: granFamilia, familia: familia, subFamilia: subFamilia, 
                commodity: commodity, cargoGroup: cargoGroup, 

                chkpuerto: chkpuerto, chknave: chknave, chkshipper: chkshipper, chkconsignee: chkconsignee, 
                chktipoNave: chktipoNave, chktrafico: chktrafico, chksucursal: chksucursal, chkpaisExtranjero: chkpaisExtranjero, chkpuertoExtranjero: chkpuertoExtranjero, 
                chktipoServicio: chktipoServicio, chktipoCarga: chktipoCarga, chkagentePortuario: chkagentePortuario, chknaviera: chknaviera, chkgranFamilia: chkgranFamilia, 
                chkfamilia: chkfamilia, chksubFamilia: chksubFamilia, chkcommodity: chkcommodity, chkcargoGroup: chkcargoGroup, 

                nombreArchivo: nombreArchivo,tipoDato:tipoDato, etapa: etapa, carpeta: carpeta}
            })
            .done(function( msg ) {
                buscarExtraccionesArchivos($("#moduloId").val());
                $("#imgCargando").css("display","none");                                           

        }); 

    }                                                                    

}  


function validarFormulario(){

    var mensaje = "";

    var nombreArchivo = $("#inputNombreArchivo").val();
    var fechaDesde = $("#fechaDesde").val();
    var fechaHasta = $("#fechaHasta").val();                                 
            
    if(nombreArchivo == ""){
        mensaje = "Debe ingresar un nombre para el archivo.<br>";
    }
    
    if(fechaDesde == ""){
        mensaje += "No ha ingresado la fecha inicial.<br>";
    }
    
    if(fechaHasta == ""){
        mensaje += "No ha ingresado la fecha final.<br>";
    }
    
    if(fechaDesde != "" && fechaHasta != "" && validarFechaMenor(fechaDesde, fechaHasta) == false){
        mensaje += "La fecha incial no puede ser mayor a la fecha final.<br>";
    }
    
    if(fechaDesde != "" && fechaHasta != "" && fechaDiferenciasDias(fechaDesde, fechaHasta) > 365){
        mensaje += "La diferencia entre las fechas no puede ser superior a un año.<br>";
    }
            
    if(mensaje != ""){
        $("#divMensaje").html(mensaje);        
        return false;
    }
    else{
        $("#divMensaje").html("");        
        return true;
    }

}
    
    
    