

function buscarExtracciones(){
    
    var nombreArchivo = $("#inputNombreArchivo").val();
    var carpeta = $("#carpeta").val();

    var fechaDesde = $("#fechaDesde").val();
    var fechaHasta = $("#fechaHasta").val();
    
    
    if(fechaDesde == "" && fechaHasta == ""){

        fechasDefecto("LINER");
        fechaDesde = $("#fechaDesde").val();
        fechaHasta = $("#fechaHasta").val();
    }
    
    if(validarFormulario()){

        $("#divResultado").html("");  

        $("#imgCargando").css("display","block");
        //$("#divPanelFiltros").css("display","none");            
        //$("#divMensaje").css("display","none");

        var agentePortuario = $("#agentePortuario").val();
        var naviera = $("#naviera").val();
        var nave = $("#nave").val();
        var tipoNave = $("#tipoNave").val();
        var puerto = $("#puerto").val();
        var sucursal = $("#sucursal").val();
        var trafico = $("#trafico").val();


        var chkagentePortuario = $("#chkagentePortuario").attr('checked');
        var chknaviera = $("#chknaviera").attr('checked');
        var chknave = $("#chknave").attr('checked');
        var chktipoNave = $("#chktipoNave").attr('checked');
        var chkpuerto = $("#chkpuerto").attr('checked');
        var chksucursal = $("#chksucursal").attr('checked');
        var chktrafico = $("#chktrafico").attr('checked');

        var tipoDato = "";
        
        if($("#btnRecaladas").prop('checked')){
            tipoDato = tipoDato + "recaladas," 
        }        
        
        if($("#btnToneladas").prop('checked')){
            tipoDato = tipoDato + "toneladas," 
        } 
        
        if($("#btnTeus").prop('checked')){
            tipoDato = tipoDato + "teus," 
        } 

        var etapa = $("input[name=btnEtapa]:checked").val();
                
        $.ajax({
            type: "POST",
            url: "linerExtraccionesData.php",
            data: { fechaDesde: fechaDesde, fechaHasta: fechaHasta, puerto: puerto, nave: nave,
                tipoNave: tipoNave, trafico: trafico, sucursal: sucursal, agentePortuario: agentePortuario, naviera: naviera, 
                
                chkpuerto: chkpuerto, chknave: chknave, chktipoNave: chktipoNave, chktrafico: chktrafico, chksucursal: chksucursal, 
                chkagentePortuario: chkagentePortuario, chknaviera: chknaviera, 

                nombreArchivo: nombreArchivo, tipoDato:tipoDato, etapa: etapa, carpeta: carpeta}
            })
            .done(function( msg ) {
                buscarExtraccionesArchivos($("#moduloId").val());
                $("#imgCargando").css("display","none");                                           

        }); 

    }                                                                    

}   
    

function validarFormulario(){

    var mensaje = "";

    var nombreArchivo = $("#inputNombreArchivo").val();
    var fechaDesde = $("#fechaDesde").val();
    var fechaHasta = $("#fechaHasta").val();                                 
            
    if(nombreArchivo == ""){
        mensaje = "Debe ingresar un nombre para el archivo.<br>";
    }
    
    if(fechaDesde == ""){
        mensaje += "No ha ingresado la fecha inicial.<br>";
    }
    
    if(fechaHasta == ""){
        mensaje += "No ha ingresado la fecha final.<br>";
    }
    
    if(fechaDesde != "" && fechaHasta != "" && validarFechaMenor(fechaDesde, fechaHasta) == false){
        mensaje += "La fecha incial no puede ser mayor a la fecha final.<br>";
    }
    
    if(fechaDesde != "" && fechaHasta != "" && fechaDiferenciasDias(fechaDesde, fechaHasta) > 365){
        mensaje += "La diferencia entre las fechas no puede ser superior a un año.<br>";
    }
            
    if(mensaje != ""){
        $("#divMensaje").html(mensaje);        
        return false;
    }
    else{
        $("#divMensaje").html("");        
        return true;
    }

}
    
    
    