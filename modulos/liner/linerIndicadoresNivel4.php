<?PHP

session_start();
include ("../../librerias/conexion.php");
require('liner.class.php');
require('../../clases/sempat.class.php');
$objLiner = new liner();
$objSempat= new sempat();


$usuarioId = $_SESSION['SEMPAT_usuarioId'];
$clienteId = $_SESSION['SEMPAT_clienteId'];

$fechaDesde = mb_convert_encoding(trim($_POST['fechaDesde']), "ISO-8859-1", "UTF-8");
$fechaHasta = mb_convert_encoding(trim($_POST['fechaHasta']), "ISO-8859-1", "UTF-8");


$nivel4 = mb_convert_encoding(trim($_POST['nivel4']), "ISO-8859-1", "UTF-8");
$nivel5 = mb_convert_encoding(trim($_POST['nivel5']), "ISO-8859-1", "UTF-8");

$tipoDato = mb_convert_encoding(trim($_POST['tipoDato']), "ISO-8859-1", "UTF-8");

$trId = mb_convert_encoding(trim($_POST['trId']), "ISO-8859-1", "UTF-8");


$nivel1Id = mb_convert_encoding(trim($_POST['nivel1Id']), "ISO-8859-1", "UTF-8");
$nivel2Id = mb_convert_encoding(trim($_POST['nivel2Id']), "ISO-8859-1", "UTF-8");
$nivel3Id = mb_convert_encoding(trim($_POST['nivel3Id']), "ISO-8859-1", "UTF-8");
$otros = mb_convert_encoding(trim($_POST['otros']), "ISO-8859-1", "UTF-8");

$consultaTabla = $objLiner->linerIndicadoresNivel4($fechaDesde, $fechaHasta, $nivel4, $nivel5, $tipoDato, $clienteId, $nivel1Id, $nivel2Id, $nivel3Id, $otros, $usuarioId);


$tbodyHTML = "";

$cursor = ""; 

if($nivel5 != ""){
    $cursor = " style='cursor:pointer'";
}

if ($consultaTabla) {
            
    $resultado = array();
    
    while ($rowdatos = mssql_fetch_array($consultaTabla)) {
        $resultado[] = $rowdatos;
    }
    
    $totales = array();
    
    for($i = 0; $i < count($resultado); $i++){    
        $totales[0] = $totales[0] + $resultado[$i][3];
        $totales[1] = $totales[1] + $resultado[$i][5];
        $totales[2] = $totales[2] + $resultado[$i][7];        
    }        
    
    $fila = 0;
    
    $salidaOtros = "";
    $campo = "";
    $valorCampo = "";
    
    for($i = 0; $i < count($resultado); $i++){
        
        $fila++;
        
        if($resultado[$i][5] != "" || $resultado[$i][6] != ""){
            
            $campo = htmlentities($resultado[$i][1]);

            if(strlen($campo) > 19){
                $valorCampo = substr($campo, 0, 17) . "...";
            }
            else{
                $valorCampo = $campo;
            }
            
            if(htmlentities($resultado[$i][1]) != "OTROS"){
               
                $tbodyHTML .= "<tr id='" . $trId . "-". $fila ."' class=\"nivel4\">";
                $tbodyHTML .= "<td class=\"check\"></td>";
                $tbodyHTML .= "<td class=\"nivel4\" " . $cursor . " onClick='buscarIndicadorNivel5(this, \"" . $fechaDesde . "\", \"" . $fechaHasta . "\", \"" . $nivel5 . "\", \"" . $tipoDato . "\", \"" . $nivel1Id . "\", \"" . $nivel2Id . "\", \"" . $nivel3Id . "\", \"" .  $resultado[$i][2] . "\");' title=\"" . htmlentities($resultado[$i][1]) . "\">" . $valorCampo . "</td>";
                $tbodyHTML .= "<td>" . number_format($resultado[$i][3], 0, '', '.') . "</td>";
                $tbodyHTML .= "<td>" . number_format($resultado[$i][4], 0, '', '.') . "</td>";
                $tbodyHTML .= $objSempat->calculoDiferencia($resultado[$i][3], $resultado[$i][4]);
                $tbodyHTML .= $objSempat->calculoMkt($resultado[$i][3], $totales[0]);

                $tbodyHTML .= "<td class=\"clear\"></td>";
                $tbodyHTML .= "<td class=\"left\">" . number_format($resultado[$i][5], 0, '', '.') . "</td>";
                $tbodyHTML .= "<td>" . number_format($resultado[$i][6], 0, '', '.') . "</td>";
                $tbodyHTML .= $objSempat->calculoDiferencia($resultado[$i][5], $resultado[$i][6]);
                $tbodyHTML .= $objSempat->calculoMkt($resultado[$i][5], $totales[1]);

                $tbodyHTML .= "<td class=\"clear\"></td>";
                $tbodyHTML .= "<td class=\"left\">" . number_format($resultado[$i][7], 0, '', '.') . "</td>";
                $tbodyHTML .= "<td>" . number_format($resultado[$i][8], 0, '', '.') . "</td>";
                $tbodyHTML .= $objSempat->calculoDiferencia($resultado[$i][7], $resultado[$i][8]);
                $tbodyHTML .= $objSempat->calculoMkt($resultado[$i][7], $totales[2]);
                $tbodyHTML .= "</tr>";
            }
            else{
                $salidaOtros .= "<tr id='" . $trId . "-". $fila ."' class=\"nivel4\">";
                $salidaOtros .= "<td class=\"check\"></td>";
                $salidaOtros .= "<td class=\"nivel4\">" . htmlentities($resultado[$i][1]) . "</td>";
                $salidaOtros .= "<td>" . number_format($resultado[$i][3], 0, '', '.') . "</td>";
                $salidaOtros .= "<td>" . number_format($resultado[$i][4], 0, '', '.') . "</td>";
                $salidaOtros .= $objSempat->calculoDiferencia($resultado[$i][3], $resultado[$i][4]);
                $salidaOtros .= $objSempat->calculoMkt($resultado[$i][3], $totales[0]);

                $salidaOtros .= "<td class=\"clear\"></td>";
                $salidaOtros .= "<td class=\"left\">" . number_format($resultado[$i][5], 0, '', '.') . "</td>";
                $salidaOtros .= "<td>" . number_format($resultado[$i][6], 0, '', '.') . "</td>";
                $salidaOtros .= $objSempat->calculoDiferencia($resultado[$i][5], $resultado[$i][6]);
                $salidaOtros .= $objSempat->calculoMkt($resultado[$i][5], $totales[1]);

                $salidaOtros .= "<td class=\"clear\"></td>";
                $salidaOtros .= "<td class=\"left\">" . number_format($resultado[$i][7], 0, '', '.') . "</td>";
                $salidaOtros .= "<td>" . number_format($resultado[$i][8], 0, '', '.') . "</td>";
                $salidaOtros .= $objSempat->calculoDiferencia($resultado[$i][7], $resultado[$i][8]);
                $salidaOtros .= $objSempat->calculoMkt($resultado[$i][7], $totales[2]);
                $salidaOtros .= "</tr>";
            }

            
            
        }
    }                 
    
    echo $tbodyHTML . $salidaOtros;
            
}


//echo $salida;


?>
