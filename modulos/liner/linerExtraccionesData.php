<?PHP

session_start();
include ("../../librerias/conexion.php");
require('liner.class.php');
$objLiner = new liner();


$usuarioId = $_SESSION['SEMPAT_usuarioId'];
$clienteId = $_SESSION['SEMPAT_clienteId'];

$fechaDesde = mb_convert_encoding(trim($_POST['fechaDesde']), "ISO-8859-1", "UTF-8");
$fechaHasta = mb_convert_encoding(trim($_POST['fechaHasta']), "ISO-8859-1", "UTF-8");
$carpeta = mb_convert_encoding(trim($_POST['carpeta']), "ISO-8859-1", "UTF-8");


$nombreArchivo = mb_convert_encoding(trim($_POST['nombreArchivo']), "ISO-8859-1", "UTF-8");

$etapa = mb_convert_encoding(trim($_POST['etapa']), "ISO-8859-1", "UTF-8");

$tipoDato = mb_convert_encoding(trim($_POST['tipoDato']), "ISO-8859-1", "UTF-8");

$puerto = mb_convert_encoding(trim($_POST['puerto']), "ISO-8859-1", "UTF-8");
$nave = mb_convert_encoding(trim($_POST['nave']), "ISO-8859-1", "UTF-8");
$tipoNave = mb_convert_encoding(trim($_POST['tipoNave']), "ISO-8859-1", "UTF-8");
$trafico = mb_convert_encoding(trim($_POST['trafico']), "ISO-8859-1", "UTF-8");
$sucursal = mb_convert_encoding(trim($_POST['sucursal']), "ISO-8859-1", "UTF-8");
$agentePortuario = mb_convert_encoding(trim($_POST['agentePortuario']), "ISO-8859-1", "UTF-8");
$naviera = mb_convert_encoding(trim($_POST['naviera']), "ISO-8859-1", "UTF-8");

$chkpuerto = mb_convert_encoding(trim($_POST['chkpuerto']), "ISO-8859-1", "UTF-8");
$chknave = mb_convert_encoding(trim($_POST['chknave']), "ISO-8859-1", "UTF-8");
$chktipoNave = mb_convert_encoding(trim($_POST['chktipoNave']), "ISO-8859-1", "UTF-8");
$chktrafico = mb_convert_encoding(trim($_POST['chktrafico']), "ISO-8859-1", "UTF-8");
$chksucursal = mb_convert_encoding(trim($_POST['chksucursal']), "ISO-8859-1", "UTF-8");
$chkagentePortuario = mb_convert_encoding(trim($_POST['chkagentePortuario']), "ISO-8859-1", "UTF-8");
$chknaviera = mb_convert_encoding(trim($_POST['chknaviera']), "ISO-8859-1", "UTF-8");


$uploaddir = "../../Reportes/" . $carpeta . "/";
			
if (file_exists($uploaddir)==false) {
    mkdir($uploaddir, 0700, true);
}

$uploaddir = $uploaddir . "/" . $usuarioId . "/";

if (file_exists($uploaddir)==false) {
    mkdir($uploaddir, 0700, true);
}

$objLiner->linerExtracciones($fechaDesde, $fechaHasta, $tipoDato, $clienteId, $etapa, $agentePortuario, $naviera, $nave, $tipoNave, $puerto, $sucursal, $trafico, $chkagentePortuario, $chknaviera, $chknave, $chktipoNave, $chkpuerto, $chksucursal, $chktrafico, $nombreArchivo, $usuarioId);

?>
