<?PHP

session_start();
include ("../../librerias/conexion.php");
require('liner.class.php');
require('../../clases/sempat.class.php');
$objLiner = new liner();
$objSempat= new sempat();


$usuarioId = $_SESSION['SEMPAT_usuarioId'];
$clienteId = $_SESSION['SEMPAT_clienteId'];

$fechaDesde = mb_convert_encoding(trim($_POST['fechaDesde']), "ISO-8859-1", "UTF-8");
$fechaHasta = mb_convert_encoding(trim($_POST['fechaHasta']), "ISO-8859-1", "UTF-8");

$nivel5 = mb_convert_encoding(trim($_POST['nivel5']), "ISO-8859-1", "UTF-8");

$tipoDato = mb_convert_encoding(trim($_POST['tipoDato']), "ISO-8859-1", "UTF-8");

$trId = mb_convert_encoding(trim($_POST['trId']), "ISO-8859-1", "UTF-8");


$nivel1Id = mb_convert_encoding(trim($_POST['nivel1Id']), "ISO-8859-1", "UTF-8");
$nivel2Id = mb_convert_encoding(trim($_POST['nivel2Id']), "ISO-8859-1", "UTF-8");
$nivel3Id = mb_convert_encoding(trim($_POST['nivel3Id']), "ISO-8859-1", "UTF-8");
$nivel4Id = mb_convert_encoding(trim($_POST['nivel4Id']), "ISO-8859-1", "UTF-8");
$otros = mb_convert_encoding(trim($_POST['otros']), "ISO-8859-1", "UTF-8");

$consultaTabla = $objLiner->linerIndicadoresNivel5($fechaDesde, $fechaHasta, $nivel5, $tipoDato, $clienteId, $nivel1Id, $nivel2Id, $nivel3Id, $nivel4Id, $otros, $usuarioId);


$tbodyHTML = "";

if ($consultaTabla) {
            
    $resultado = array();
    
    while ($rowdatos = mssql_fetch_array($consultaTabla)) {
        $resultado[] = $rowdatos;
    }
    
    $totales = array();
    
    for($i = 0; $i < count($resultado); $i++){    
        $totales[0] = $totales[0] + $resultado[$i][3];
        $totales[1] = $totales[1] + $resultado[$i][5];
        $totales[2] = $totales[2] + $resultado[$i][7];        
    }        
    
    $fila = 0;
    
    $salidaOtros = "";
    $campo = "";
    $valorCampo = "";
    
    for($i = 0; $i < count($resultado); $i++){
        
        $fila++;
        
        if($resultado[$i][5] != "" || $resultado[$i][6] != ""){
            
            $campo = htmlentities($resultado[$i][1]);

            if(strlen($campo) > 17){
                $valorCampo = substr($campo, 0, 15) . "...";
            }
            else{
                $valorCampo = $campo;
            }
            
            if(htmlentities($resultado[$i][1]) != "OTROS"){
               
                $tbodyHTML .= "<tr id='" . $trId . "-". $fila ."' class=\"nivel5\">";
                $tbodyHTML .= "<td class=\"check\"></td>";
                $tbodyHTML .= "<td class=\"nivel5\" title=\"" . htmlentities($resultado[$i][1]) . "\">" . $valorCampo . "</td>";
                $tbodyHTML .= "<td>" . number_format($resultado[$i][3], 0, '', '.') . "</td>";
                $tbodyHTML .= "<td>" . number_format($resultado[$i][4], 0, '', '.') . "</td>";
                $tbodyHTML .= $objSempat->calculoDiferencia($resultado[$i][3], $resultado[$i][4]);
                $tbodyHTML .= $objSempat->calculoMkt($resultado[$i][3], $totales[0]);

                $tbodyHTML .= "<td class=\"clear\"></td>";
                $tbodyHTML .= "<td class=\"left\">" . number_format($resultado[$i][5], 0, '', '.') . "</td>";
                $tbodyHTML .= "<td>" . number_format($resultado[$i][6], 0, '', '.') . "</td>";
                $tbodyHTML .= $objSempat->calculoDiferencia($resultado[$i][5], $resultado[$i][6]);
                $tbodyHTML .= $objSempat->calculoMkt($resultado[$i][5], $totales[1]);

                $tbodyHTML .= "<td class=\"clear\"></td>";
                $tbodyHTML .= "<td class=\"left\">" . number_format($resultado[$i][7], 0, '', '.') . "</td>";
                $tbodyHTML .= "<td>" . number_format($resultado[$i][8], 0, '', '.') . "</td>";
                $tbodyHTML .= $objSempat->calculoDiferencia($resultado[$i][7], $resultado[$i][8]);
                $tbodyHTML .= $objSempat->calculoMkt($resultado[$i][7], $totales[2]);
                $tbodyHTML .= "</tr>";
            }
            else{
                $salidaOtros .= "<tr id='" . $trId . "-". $fila ."' class=\"nivel5\">";
                $salidaOtros .= "<td class=\"check\"></td>";
                $salidaOtros .= "<td class=\"nivel5\">" . htmlentities($resultado[$i][1]) . "</td>";
                $salidaOtros .= "<td>" . number_format($resultado[$i][3], 0, '', '.') . "</td>";
                $salidaOtros .= "<td>" . number_format($resultado[$i][4], 0, '', '.') . "</td>";
                $salidaOtros .= $objSempat->calculoDiferencia($resultado[$i][3], $resultado[$i][4]);
                $salidaOtros .= $objSempat->calculoMkt($resultado[$i][3], $totales[0]);

                $salidaOtros .= "<td class=\"clear\"></td>";
                $salidaOtros .= "<td class=\"left\">" . number_format($resultado[$i][5], 0, '', '.') . "</td>";
                $salidaOtros .= "<td>" . number_format($resultado[$i][6], 0, '', '.') . "</td>";
                $salidaOtros .= $objSempat->calculoDiferencia($resultado[$i][5], $resultado[$i][6]);
                $salidaOtros .= $objSempat->calculoMkt($resultado[$i][5], $totales[1]);

                $salidaOtros .= "<td class=\"clear\"></td>";
                $salidaOtros .= "<td class=\"left\">" . number_format($resultado[$i][7], 0, '', '.') . "</td>";
                $salidaOtros .= "<td>" . number_format($resultado[$i][8], 0, '', '.') . "</td>";
                $salidaOtros .= $objSempat->calculoDiferencia($resultado[$i][7], $resultado[$i][8]);
                $salidaOtros .= $objSempat->calculoMkt($resultado[$i][7], $totales[2]);
                $salidaOtros .= "</tr>";
            }

            
            
        }
    }                 
    
    echo $tbodyHTML . $salidaOtros;
            
}


//echo $salida;


?>
