<?PHP

session_start();
include ("../../librerias/conexion.php");
require('flota.class.php');
$objAgencia = new flota();

$usuarioId = $_SESSION['SEMPAT_usuarioId'];
$clienteId = $_SESSION['SEMPAT_clienteId'];

$tipoDato = mb_convert_encoding(trim($_POST['tipoDato']), "ISO-8859-1", "UTF-8");

$fechaDesde = mb_convert_encoding(trim($_POST['fechaDesde']), "ISO-8859-1", "UTF-8");
$fechaHasta = mb_convert_encoding(trim($_POST['fechaHasta']), "ISO-8859-1", "UTF-8");
$carpeta = mb_convert_encoding(trim($_POST['carpeta']), "ISO-8859-1", "UTF-8");


$nombreArchivo = mb_convert_encoding(trim($_POST['nombreArchivo']), "ISO-8859-1", "UTF-8");

$agencia = mb_convert_encoding(trim($_POST['agencia']), "ISO-8859-1", "UTF-8");
$puerto = mb_convert_encoding(trim($_POST['puerto']), "ISO-8859-1", "UTF-8");
$nave = mb_convert_encoding(trim($_POST['nave']), "ISO-8859-1", "UTF-8");
$tipoNave = mb_convert_encoding(trim($_POST['tipoNave']), "ISO-8859-1", "UTF-8");
$tipoFaena = mb_convert_encoding(trim($_POST['tipoFaena']), "ISO-8859-1", "UTF-8");
$tipoServicio = mb_convert_encoding(trim($_POST['tipoServicio']), "ISO-8859-1", "UTF-8");

$chkagencia = mb_convert_encoding(trim($_POST['chkagencia']), "ISO-8859-1", "UTF-8");
$chkpuerto = mb_convert_encoding(trim($_POST['chkpuerto']), "ISO-8859-1", "UTF-8");
$chknave = mb_convert_encoding(trim($_POST['chknave']), "ISO-8859-1", "UTF-8");
$chktipoNave = mb_convert_encoding(trim($_POST['chktipoNave']), "ISO-8859-1", "UTF-8");
$chktipoFaena = mb_convert_encoding(trim($_POST['chktipoFaena']), "ISO-8859-1", "UTF-8");
$chktipoServicio = mb_convert_encoding(trim($_POST['chktipoServicio']), "ISO-8859-1", "UTF-8");


$agencia = str_replace("\'", "'", $agencia);
$puerto = str_replace("\'", "'", $puerto);
$nave = str_replace("\'", "'", $nave);
$tipoNave = str_replace("\'", "'", $tipoNave);
$tipoFaena = str_replace("\'", "'", $tipoFaena);
$tipoServicio = str_replace("\'", "'", $tipoServicio);


$uploaddir = "../../Reportes/" . $carpeta . "/";
			
if (file_exists($uploaddir)==false) {
    mkdir($uploaddir, 0700, true);
}

$uploaddir = $uploaddir . "/" . $usuarioId . "/";

if (file_exists($uploaddir)==false) {
    mkdir($uploaddir, 0700, true);
}

$objAgencia->flotaExtracciones($fechaDesde, $fechaHasta, $tipoDato, $agencia, $puerto, $nave, $tipoNave, $tipoFaena, $tipoServicio, $chkagencia, $chkpuerto, $chknave, $chktipoNave, $chktipoFaena, $chktipoServicio, $nombreArchivo, $usuarioId);

?>
