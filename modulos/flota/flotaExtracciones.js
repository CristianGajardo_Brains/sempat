

function buscarExtracciones(){
    
    var nombreArchivo = $("#inputNombreArchivo").val();
    var carpeta = $("#carpeta").val();

    var fechaDesde = $("#fechaDesde").val();
    var fechaHasta = $("#fechaHasta").val();


    if(fechaDesde == "" && fechaHasta == ""){

        fechasDefecto('PRACTICAJE'); 
        fechaDesde = $("#fechaDesde").val();
        fechaHasta = $("#fechaHasta").val();

        //fechaDesde = $("#fechaDesdeDef").val();
        //fechaHasta = $("#fechaHastaDef").val();
        //$("#fechaDesde").val(fechaDesde);
        //$("#fechaHasta").val(fechaHasta);

    }
    
           
    if(validarFormulario()){

        $("#divResultado").html("");  

        $("#imgCargando").css("display","block");
        //$("#divPanelFiltros").css("display","none");            
        //$("#divMensaje").css("display","none");

        var agencia = $("#agencia").val();
        var puerto = $("#puerto").val();
        var nave = $("#nave").val();
        var tipoNave = $("#tipoNave").val();
        var tipoFaena = $("#tipoFaena").val();
        var tipoServicio = $("#tipoServicio").val();        
        
        var chkagencia = $("#chkagencia").attr('checked');
        var chkpuerto = $("#chkpuerto").attr('checked');
        var chknave = $("#chknave").attr('checked');
        var chktipoNave = $("#chktipoNave").attr('checked');
        var chktipoFaena = $("#chktipoFaena").attr('checked');
        var chktipoServicio = $("#chktipoServicio").attr('checked');
        
                
        var tipoDato = "";
        
        if($("#btnFaenas").prop('checked')){
            tipoDato = tipoDato + "faenas," 
        }                                        
        
        if($("#btnRemolcadores").prop('checked')){
            tipoDato = tipoDato + "remolcadores," 
        }                                        
                                       
        if($("#btnTiempos").prop('checked')){
            tipoDato = tipoDato + "tiempos," 
        }                                        
        
        $.ajax({
            type: "POST",
            url: "flotaExtraccionesData.php",
            data: { fechaDesde: fechaDesde, fechaHasta: fechaHasta, agencia: agencia, puerto: puerto, nave: nave, tipoNave: tipoNave, tipoFaena: tipoFaena, 
                tipoServicio: tipoServicio,
                chkagencia: chkagencia, chkpuerto: chkpuerto, chknave: chknave, chktipoNave: chktipoNave, chktipoFaena: chktipoFaena, 
                chktipoServicio: chktipoServicio, nombreArchivo: nombreArchivo, tipoDato: tipoDato, carpeta: carpeta}
            })
            .done(function( msg ) {
                buscarExtraccionesArchivos($("#moduloId").val());
                $("#imgCargando").css("display","none");                                           

        }); 

    }                                                                    

}   


function validarFormulario(){

    var mensaje = "";

    var nombreArchivo = $("#inputNombreArchivo").val();
    var fechaDesde = $("#fechaDesde").val();
    var fechaHasta = $("#fechaHasta").val();                                 
            
    if(nombreArchivo == ""){
        mensaje = "Debe ingresar un nombre para el archivo.<br>";
    }
    
    if(fechaDesde == ""){
        mensaje += "No ha ingresado la fecha inicial.<br>";
    }
    
    if(fechaHasta == ""){
        mensaje += "No ha ingresado la fecha final.<br>";
    }
    
    if(fechaDesde != "" && fechaHasta != "" && validarFechaMenor(fechaDesde, fechaHasta) == false){
        mensaje += "La fecha incial no puede ser mayor a la fecha final.<br>";
    }
    
    if(fechaDesde != "" && fechaHasta != "" && fechaDiferenciasDias(fechaDesde, fechaHasta) > 365){
        mensaje += "La diferencia entre las fechas no puede ser superior a un año.<br>";
    }
            
    if(mensaje != ""){
        $("#divMensaje").html(mensaje);        
        return false;
    }
    else{
        $("#divMensaje").html("");        
        return true;
    }

}
    
    
    