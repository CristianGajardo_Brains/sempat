<?PHP

session_start();
include ("../../librerias/conexion.php");
require('flota.class.php');
require('../../clases/sempat.class.php');
$objFlota = new flota();
$objSem = new sempat();


$usuarioId = $_SESSION['SEMPAT_usuarioId'];
$clienteId = $_SESSION['SEMPAT_clienteId'];

$fechaDesde = mb_convert_encoding(trim($_POST['fechaDesde']), "ISO-8859-1", "UTF-8");
$fechaHasta = mb_convert_encoding(trim($_POST['fechaHasta']), "ISO-8859-1", "UTF-8");

$tipoDato = mb_convert_encoding(trim($_POST['tipoDato']), "ISO-8859-1", "UTF-8");

$nivel1 = mb_convert_encoding(trim($_POST['nivel1']), "ISO-8859-1", "UTF-8");
$nivel2 = mb_convert_encoding(trim($_POST['nivel2']), "ISO-8859-1", "UTF-8");
$nivel3 = mb_convert_encoding(trim($_POST['nivel3']), "ISO-8859-1", "UTF-8");


$nivel1Titulo = mb_convert_encoding(trim($_POST['nivel1Titulo']), "ISO-8859-1", "UTF-8");

$meses = array();

$agenciaNombre = mb_convert_encoding(trim($_POST['agencia']),"UTF-8", "ISO-8859-1");
$puertoNombre = mb_convert_encoding(trim($_POST['puerto']), "ISO-8859-1", "UTF-8");
$naveNombre = mb_convert_encoding(trim($_POST['nave']), "ISO-8859-1", "UTF-8");
$tipoNaveNombre = mb_convert_encoding(trim($_POST['tipoNave']), "ISO-8859-1", "UTF-8");
$tipoFaenaNombre = mb_convert_encoding(trim($_POST['tipoFaena']), "ISO-8859-1", "UTF-8");
$tipoServicioNombre = mb_convert_encoding(trim($_POST['tipoServicio']), "ISO-8859-1", "UTF-8");

$decimales = 0;
$separador = '';

if($tipoDato == "faenas"){
    $nombreSP = "flotaReportesFaenas";
}

if($tipoDato == "remolcadores"){
    $nombreSP = "flotaReportesRemolcadores";
}

if($tipoDato == "tiempos"){
    $nombreSP = "flotaReportesTiempoFaenas";
    $decimales = 2;
    $separador = ',';
}


$agenciaNombre = str_replace("\'", "'", $agenciaNombre);
$puertoNombre = str_replace("\'", "'", $puertoNombre);
$naveNombre = str_replace("\'", "'", $naveNombre);
$tipoNaveNombre = str_replace("\'", "'", $tipoNaveNombre);
$tipoFaenaNombre = str_replace("\'", "'", $tipoFaenaNombre);
$tipoServicioNombre = str_replace("\'", "'", $tipoServicioNombre);


$mesInicial = substr($fechaDesde, 3, 2);
$añoInicial = substr($fechaDesde, 6, 4);

$mesFinal = substr($fechaHasta, 3, 2);
$añoFinal = substr($fechaHasta, 6, 4);

for ($a = 0; $a < 12; $a++) {

    $mes = "00" . ($mesInicial);
    $mes = substr($mes, strlen($mes) - 2, strlen($mes));

    $meses[$a][0] = $añoInicial . "-" . $mes;

    if ($mes >= $mesFinal && $añoInicial >= $añoFinal) {
        $a = 12;
    }

    if ($mes == 12) {
        $añoInicial++;
        $mesInicial = "01";
    } else {
        $mesInicial++;
    }
}


for ($i = 0; $i < 12; $i++) {
    if ($meses[$i][0] != "") {

        if($tipoDato == "tiempos"){
            $variableSQL = $variableSQL . "ISNULL(sum(ISNULL(CAST([" . $meses[$i][0] . "] as decimal(18,2)),0)),0),";
        }
        else{
            $variableSQL = $variableSQL . "ISNULL(sum(ISNULL(CAST([" . $meses[$i][0] . "] as int),0)),0),";
        }
        $variableSQL2 = $variableSQL2 . "[" . $meses[$i][0] . "],";
    } else {
        $variableSQL = $variableSQL . "ISNULL(sum(ISNULL(CAST([" . $i . "] as int),0)),0),";
        $variableSQL2 = $variableSQL2 . "[" . $i . "],";
    }
}


$variableSQL = substr($variableSQL, 0, strlen($variableSQL) - 1);       // periodos meses
$variableSQL2 = substr($variableSQL2, 0, strlen($variableSQL2) - 1);    // periodos meses - pivote


//echo $variableSQL . "<br>" . $variableSQL2;

$consultaTabla = $objFlota->flotaTipoIIData($nombreSP, $fechaDesde . " 00:00:00", $fechaHasta . " 23:59:59", $nivel1, $nivel2, $nivel3, $variableSQL, $variableSQL2, $clienteId, $agenciaNombre, $puertoNombre, $naveNombre, $tipoNaveNombre, $tipoFaenaNombre, $tipoServicioNombre, $usuarioId);



$cabeceraHTML = "";
$cabeceraHTML2 = "<tr><td colspan='2' class='left' style='width:185px;'>" . htmlentities(strtoupper($nivel1Titulo)) . "</td>";
$colspan=5;

for ($i = 0; $i < count($meses); $i++) {

    $cabeceraHTML2 .= "<td>" . $meses[$i][0] . "</td>";
    $colspan++;
}


$cabeceraHTML = "<table id='tablaReporte' class='tablaValores' cellspacing='0' cellspadding='0'>
                    <thead>
                        <tr>
                            <th class='thCenter' colspan='" . $colspan . "'>
                                <div class='divThLeft'></div>
                                <div class='divThCenter'>" . tituloCabecera($fechaDesde, $fechaHasta, 2) . "</div>
                                <div class='divThRight'></div>
                            </th>
                        </tr>" . $cabeceraHTML2 . "<td>TOTAL</td><td>TOTAL ANT.</td><td>DIF.</td> </thead>";

$campo1 = "";
$valorCampo1 = "";
$salida = "";
$salidaOtros = "";
$total = 0;
$totalAnterior = 0;

$fila = 0;


$cursor = "";

if($nivel2 != ""){
    $cursor = " style='cursor:pointer'";
}


if ($consultaTabla) {

    $numFilas = mssql_num_rows($consultaTabla);

    if($numFilas > 0){

        while ($manifiesto = mssql_fetch_array($consultaTabla)) {
            
            $campo1 = htmlentities($manifiesto[7]);

            if(strlen($campo1) > 24){
                $valorCampo1 = substr($campo1, 0, 19) . "...";
            }
            else{
                $valorCampo1 = $campo1;
            }

            if($campo1 != "OTROS"){
                
                $fila++;

                $salida .= "<tr id='tr-". $fila ."' class='nivel1'><td class='check'><input class='styled' type='checkbox' onClick='graficoCheckboxReporte(this);'></td><td class='nivel1' " . $cursor . " title =\"" . $campo1 . "\" onClick=\"buscarNivel2(this, '" . $nivel2 . "','" . $nivel3 . "', '" . $campo1 . "', '" . $tipoDato . "', '" . count($meses) . "');\">" . $valorCampo1 . "</td>";

                for ($i = 0; $i < count($meses); $i++) {

                    $salida .= "<td>" . number_format($manifiesto[10 + $i], $decimales, $separador, '.') . "</td>";                                                            
                    $meses[$i][$fila] .= $manifiesto[10 + $i];
                }

                if($tipoDato == "remolcadores"){
                    $salida .= "<td><b>" . number_format(($manifiesto[4]/count($meses)), $decimales, $separador, '.') . "</b></td><td><b>" . number_format($manifiesto[22], $decimales, $separador, '.') . "</b></td>";
                    $salida .= $objSem->calculoDiferencia(number_format(($manifiesto[4]/count($meses)), $decimales, $separador, '.'), number_format($manifiesto[22], $decimales, $separador, '.')) . "</tr>";
                    $total = $total + $manifiesto[4]/count($meses);
                }
                else{
                    $salida .= "<td><b>" . number_format($manifiesto[4], $decimales, $separador, '.') . "</b></td><td><b>" . number_format($manifiesto[22], $decimales, $separador, '.') . "</b></td>";
                    $salida .= $objSem->calculoDiferencia($manifiesto[4], $manifiesto[22]) . "</tr>";
                    $total = $total + $manifiesto[4];

                }

                $totalAnterior = $totalAnterior + $manifiesto[22];

            }
            else{

                if($manifiesto[22] != ""){
                    $salidaOtros .= "<tr id='tr-". $numFilas ."' class='nivel1'><td class='check'><input type='checkbox' class='styled' onClick='graficoAgregarReporte(this);'/></td><td class='nivel1' title =\"" . $campo1 . "\">" . $valorCampo1 . "</td>";

                    for ($i = 0; $i < count($meses); $i++) {

                        $salidaOtros .= "<td>" . number_format($manifiesto[10 + $i], $decimales, $separador, '.') . "</td>";
                        $meses[$i][$numFilas] .= $manifiesto[10 + $i];
                    }

                    if($tipoDato == "remolcadores"){
                        $salidaOtros .= "<td><b>" . number_format(($manifiesto[4]/count($meses)), $decimales, $separador, '.') . "</b></td><td><b>" . number_format($manifiesto[22], $decimales, $separador, '.') . "</b></td>";
                        $salidaOtros .= $objSem->calculoDiferencia(number_format(($manifiesto[4]/count($meses)), $decimales, $separador, '.'), number_format($manifiesto[22], $decimales, $separador, '.')) . "</tr>";
                        $total = $total + $manifiesto[4]/count($meses);
                    }
                    else{
                        $salidaOtros .= "<td><b>" . number_format($manifiesto[4], $decimales, $separador, '.') . "</b></td><td><b>" . number_format($manifiesto[22], $decimales, $separador, '.') . "</b></td>";
                        $salidaOtros .= $objSem->calculoDiferencia(number_format($manifiesto[4], $decimales, $separador, '.'), number_format($manifiesto[22], $decimales, $separador, '.')) . "</tr>";
                        $total = $total + $manifiesto[4];
                    }

                    $totalAnterior = $totalAnterior + $manifiesto[22];

                }
            }
        }

        $salidaTotal = "<tr class='nivelTotal'><td class='check'></td><td class='nivelTotal'>TOTAL</td>";
        $salidaTotal2 = "<tr style='display:none'><td></td><td></td>";

        $htmlTablaGrafico = "<table id='tablaGrafico'><thead><tr><th></th></tr></thead><tbody>";
        
        for($x = 0; $x < count($meses); $x++){
                                    
            $tiemposFaenas = 0; //valores distintos a cero

            $htmlTablaGrafico .= "<tr><th>" . retornarPeriodo($meses[$x][0]) . "</th>";

            $valor = "0";

            for ($i = 1; $i <= $numFilas; $i++) {
                $valor = $valor + $meses[$x][$i];

                if($meses[$x][$i] != 0){
                    $tiemposFaenas++;
                }

            }

            if($tipoDato == "faenas"){
                $salidaTotal .= "<td>" .  number_format($valor, $decimales, $separador, '.')   . "</td>";
            }
            else{
                $salidaTotal .= "<td>" .  number_format($valor/$tiemposFaenas, $decimales, $separador, '.')   . "</td>";
                $salidaTotal2 .= "<td>" .  number_format($valor/$tiemposFaenas, $decimales, $separador, '.')   . "</td>";
            }

        }
        
        if($tipoDato == "faenas"){
            $salidaTotal.= "<td>" .  number_format($total, $decimales, $separador, '.')   . "</td><td>" .  number_format($totalAnterior, $decimales, $separador, '.')   . "</td>" . $objSem->calculoDiferencia($total, $totalAnterior) . "</tr>";
        }
        else{
            $salidaTotal .= "<td>" .  number_format($total/$numFilas, $decimales, $separador, '.')   . "</td><td>" .  number_format($totalAnterior/$numFilas, $decimales, $separador, '.')   . "</td>" . $objSem->calculoDiferencia(number_format($total/$numFilas, $decimales, $separador, '.'), number_format($totalAnterior/$numFilas, $decimales, $separador, '.')) . "</tr>";
            $salidaTotal2 .= "<td>" .  number_format($total, $decimales, $separador, '.')   . "</td><td>" .  number_format($totalAnterior, $decimales, $separador, '.')   . "</td><td></td></tr>";
            $salidaTotal .= $salidaTotal2;
        }

        $bodyHTML = "<tbody id='tBody'>" . $salida . $salidaOtros . $salidaTotal ."</tbody>";

        $htmlTablaGrafico .= "</tr></tbody></table>";

        $htmlTablaGrafico .= "<table id=\"tablaActual\">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>

                            <table id=\"tablaAnterior\">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>";





        echo "<div id='divDatosGrafico' style='display:none'>" .  $htmlTablaGrafico . "</div>" . $cabeceraHTML . $bodyHTML . "</table>";

    }
    else{
        echo "";
    }


}
else{
    echo "";
}



function tituloCabecera($fechaDesde, $fechaHasta, $tipo){

    $mesesA = array("", "ENE", "FEB", "MAR", "ABR", "MAY", "JUN", "JUL", "AGO", "SEP", "OCT", "NOV", "DIC");
    $mesesB = array("", "ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE");

    $mDesde = date("m", strtotime($fechaDesde));
    $yDesde = date("Y", strtotime($fechaDesde));

    $mHasta = date("m", strtotime($fechaHasta));
    $yHasta = date("Y", strtotime($fechaHasta));

    $titulo = "";

    if($tipo == 1){
        $titulo = $mesesA[(int)$mDesde] . " " . $yDesde . " - " . $mesesA[(int)$mHasta] . " " . $yHasta;
    }

    if($tipo == 2){
        $titulo = $mesesB[(int)$mDesde] . " " . $yDesde . " - " . $mesesB[(int)$mHasta] . " " . $yHasta;
    }

    return $titulo;

}


function retornarPeriodo($valor){
    $meses = array("", "ENE", "FEB", "MAR", "ABR", "MAY", "JUN", "JUL", "AGO", "SEP", "OCT", "NOV", "DIC");

    $datos = split("-",$valor);

    return $meses[$datos[1]*1] . "-" . $datos[0];

}

//echo $salida;


?>
