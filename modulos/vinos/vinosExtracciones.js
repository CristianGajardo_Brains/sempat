

function buscarExtracciones(){
    
    var nombreArchivo = $("#inputNombreArchivo").val();
    var carpeta = $("#carpeta").val();
    
    var fechaDesde = $("#fechaDesde").val();
    var fechaHasta = $("#fechaHasta").val();
	
	var ffw = $("input[name=btnFww]:checked").val();
		   
    var mercado = "";

    var chile = $("input[id=btnChile]:checked").val();
    var peru = $("input[id=btnPeru]:checked").val();
    var ecuador = $("input[id=btnEcuador]:checked").val();
    var colombia = $("input[id=btnColombia]:checked").val();


    mercado = chile + "," + peru + "," + ecuador + "," + colombia;                                                         
    mercado = replaceAll(mercado, ",undefined",""); 
    mercado = replaceAll(mercado, "undefined",""); 

    if(chile == undefined){
        mercado = mercado.substring(1, mercado.length);
    }

    if(fechaDesde == "" && fechaHasta == ""){

        fechasDefecto("DECLARACIONES"); 
        fechaDesde = $("#fechaDesde").val();
        fechaHasta = $("#fechaHasta").val();    
    }
    
    if(validarFormulario()){

        $("#divResultado").html("");  

        $("#imgCargando").css("display","block");
        //$("#divPanelFiltros").css("display","none");            
        //$("#divMensaje").css("display","none");

        var puertoOrigen = $("#manPuertoOrigen").val();
        var puertoEmbarque = $("#manPuertoEmbarque").val();
        var puertoDescarga = $("#manPuertoDescarga").val();
        var puertoDestino = $("#manPuertoDestino").val();
        var paisOrigen = $("#manPaisOrigen").val();
        var paisEmbarque = $("#manPaisEmbarque").val();
        var paisDescarga = $("#manPaisDescarga").val();
        var paisDestino = $("#manPaisDestino").val();
        var trafico = $("#manTrafico").val();
        var naviera = $("#manNaviera").val();
        var agencia = $("#manAgencia").val();
        var agenteDoc = $("#manAgenteDoc").val();
        var nave = $("#manNave").val();
        var tipoNave = $("#manTipoNave").val();
        var shipper = $("#manExportador").val();
        var consignee = $("#manImportador").val();
        var granFamilia = $("#manGranFamilia").val();
        var familia = $("#manFamilia").val();
        var subFamilia = $("#manSubFamilia").val();
        var commodity = $("#manCommodity").val();
        var tipoCarga = $("#manTipoCarga").val();
        var tipoServicio = $("#manTipoServicio").val();
        var tipoEmbalaje = $("#manTipoEmbalaje").val();
        var statusContainer = $("#manStatusContainer").val();
		var emisor = $("#manEmisor").val();
		var almacen = $("#manAlmacen").val();
		var clausula = $("#manClausula").val();

		var vertical = $("#manVertical").val();		
		var subVertical = $("#manSubVertical").val();		
		var categoria = $("#manCategoria").val();
		

        var chkpuertoOrigen = $("#chkmanPuertoOrigen").attr('checked');
        var chkpuertoEmbarque = $("#chkmanPuertoEmbarque").attr('checked');
        var chkpuertoDescarga = $("#chkmanPuertoDescarga").attr('checked');
        var chkpuertoDestino = $("#chkmanPuertoDestino").attr('checked');
        var chkpaisOrigen = $("#chkmanPaisOrigen").attr('checked');
        var chkpaisEmbarque = $("#chkmanPaisEmbarque").attr('checked');
        var chkpaisDescarga = $("#chkmanPaisDescarga").attr('checked');
        var chkpaisDestino = $("#chkmanPaisDestino").attr('checked');
        var chktrafico = $("#chkmanTrafico").attr('checked');
        var chknaviera = $("#chkmanNaviera").attr('checked');
        var chkagencia = $("#chkmanAgencia").attr('checked');
        var chkagenteDoc = $("#chkmanAgenteDoc").attr('checked');
        var chknave = $("#chkmanNave").attr('checked');
        var chktipoNave = $("#chkmanTipoNave").attr('checked');
        var chkshipper = $("#chkmanExportador").attr('checked');
        var chkconsignee = $("#chkmanImportador").attr('checked');
        var chkgranFamilia = $("#chkmanGranFamilia").attr('checked');
        var chkfamilia = $("#chkmanFamilia").attr('checked');
        var chksubFamilia = $("#chkmanSubFamilia").attr('checked');
        var chkcommodity = $("#chkmanCommodity").attr('checked');
        var chktipoCarga = $("#chkmanTipoCarga").attr('checked');
        var chktipoServicio = $("#chkmanTipoServicio").attr('checked');
        var chktipoEmbalaje = $("#chkmanTipoEmbalaje").attr('checked');
        var chkstatusContainer = $("#chkmanStatusContainer").attr('checked');
		var chkemisor = $("#chkmanEmisor").attr('checked');
		var chkalmacen = $("#chkmanAlmacen").attr('checked');
		var chkclausula = $("#chkmanClausula").attr('checked');
		
		var chkvertical= $("#chkmanVertical").attr('checked');
		var chksubvertical = $("#chkmanSubVertical").attr('checked');
		var chkcategoria = $("#chkmanCategoria").attr('checked');

        var tipoDato = "";
        
        if($("#btnContenedores").prop('checked')){
            tipoDato = tipoDato + "contenedores," 
        }        
        
        if($("#btnTeus").prop('checked')){
            tipoDato = tipoDato + "teus," 
        } 
        
        if($("#btnToneladas").prop('checked')){
            tipoDato = tipoDato + "toneladas," 
        }                                        
		
		if($("#btnFob").prop('checked')){
            tipoDato = tipoDato + "fob," 
        }   
		
		if($("#btnFlete").prop('checked')){
            tipoDato = tipoDato + "flete," 
        }   
		
		if($("#btnDespachos").prop('checked')){
            tipoDato = tipoDato + "despacho," 
        }   
		

        var etapa = $("input[name=btnEtapa]:checked").val();
        var contenedor = $("input[name=btnContenedor]:checked").val();
        var dryReefer = $("input[name=btnSecaRefrigerada]:checked").val();
        

        $.ajax({
            type: "POST",
            url: "vinosExtraccionesData.php",
            data: { fechaDesde: fechaDesde, fechaHasta: fechaHasta, puertoOrigen: puertoOrigen, puertoEmbarque: puertoEmbarque, puertoDescarga: puertoDescarga, puertoDestino: 
                    puertoDestino, paisOrigen: paisOrigen, paisEmbarque: paisEmbarque, paisDescarga: paisDescarga, paisDestino: paisDestino, trafico: trafico, naviera: naviera, 
                agencia: agencia, agenteDoc: agenteDoc, nave: nave, tipoNave: tipoNave, shipper: shipper, consignee: consignee, granFamilia: granFamilia, familia: familia, 
                subFamilia: subFamilia, commodity: commodity, tipoCarga: tipoCarga, tipoServicio: tipoServicio, tipoEmbalaje: tipoEmbalaje, statusContainer: statusContainer, emisor: emisor, almacen: almacen, ffw: ffw, clausula: clausula,

                chkpuertoOrigen: chkpuertoOrigen, chkpuertoEmbarque: chkpuertoEmbarque, chkpuertoDescarga: chkpuertoDescarga, chkpuertoDestino: chkpuertoDestino, chkpaisOrigen: chkpaisOrigen, 
                chkpaisEmbarque: chkpaisEmbarque, chkpaisDescarga: chkpaisDescarga, chkpaisDestino: chkpaisDestino, chktrafico: chktrafico, chknaviera: chknaviera, chkagencia: chkagencia, 
                chkagenteDoc: chkagenteDoc, chknave: chknave, chktipoNave: chktipoNave, chkshipper: chkshipper, chkconsignee: chkconsignee, chkgranFamilia: chkgranFamilia, 
                chkfamilia: chkfamilia, chksubFamilia: chksubFamilia, chkcommodity: chkcommodity, chktipoCarga: chktipoCarga, chktipoServicio: chktipoServicio, 
                chktipoEmbalaje: chktipoEmbalaje, chkstatusContainer: chkstatusContainer, chkemisor: chkemisor, chkalmacen: chkalmacen, chkclausula: chkclausula,

                nombreArchivo: nombreArchivo,tipoDato:tipoDato, mercado: mercado, etapa: etapa, contenedor: contenedor, dryReefer: dryReefer, carpeta:carpeta ,vertical : vertical , subVertical: subVertical , categoria : categoria , chkvertical: chkvertical , chksubvertical : chksubvertical,   chkcategoria : chkcategoria }
            })
            .done(function( msg ) {
                buscarExtraccionesArchivos($("#moduloId").val());
                $("#imgCargando").css("display","none");                                           

        }); 

    }                                                                    

}  


function validarFormulario(){

    var mensaje = "";

    var nombreArchivo = $("#inputNombreArchivo").val();
    var fechaDesde = $("#fechaDesde").val();
    var fechaHasta = $("#fechaHasta").val();                                 
            
    if(nombreArchivo == ""){
        mensaje = "Debe ingresar un nombre para el archivo.<br>";
    }
    
    if(fechaDesde == ""){
        mensaje += "No ha ingresado la fecha inicial.<br>";
    }
    
    if(fechaHasta == ""){
        mensaje += "No ha ingresado la fecha final.<br>";
    }
    
    if(fechaDesde != "" && fechaHasta != "" && validarFechaMenor(fechaDesde, fechaHasta) == false){
        mensaje += "La fecha incial no puede ser mayor a la fecha final.<br>";
    }
    
    if(fechaDesde != "" && fechaHasta != "" && fechaDiferenciasDias(fechaDesde, fechaHasta) > 365){
        mensaje += "La diferencia entre las fechas no puede ser superior a un año.<br>";
    }
            
    if(mensaje != ""){
        $("#divMensaje").html(mensaje);        
        return false;
    }
    else{
        $("#divMensaje").html("");        
        return true;
    }

}
    
    
    