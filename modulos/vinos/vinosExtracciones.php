<?
session_start();

setlocale(LC_TIME, "spanish");
$fechaActual = htmlentities(strftime("%A %d de %B del %Y"));
$fechaActual = ucfirst($fechaActual);


$usuarioLog = $_SESSION["SEMPAT_usuarioNombre"];
$usuarioId = $_SESSION["SEMPAT_usuarioId"];
$clienteId = $_SESSION["SEMPAT_clienteId"];


if ($_SESSION['SEMPAT_usuarioId'] == "") {
    echo "<script>window.location='../../index.php';</script>";
}

include("../../default.php");
$plantilla->setPath('../../plantillas/');
$plantilla->setTemplate("header");
$plantilla->setVars(array("USUARIO" => " $usuarioLog ",
    "FECHA" => "$fechaActual"
));
echo $plantilla->show();

require('../../clases/sempat.class.php');
$objSem = new sempat();

$menuId = 142;
$moduloId = 55;
$mercados = "1";
$carpeta = "Vinos";


$htmlMenu = $objSem->menuRetornarHtml($usuarioId, $menuId);


$moduloFiltros = $objSem->admBuscarFiltros($clienteId, $moduloId);
$scriptAC = "";


//$consultaFechas = $objSem->manifiestoFechasDefecto($clienteId);
//$fechaDesde = "01-10-2012";
//$fechaHasta = "30-09-2013";


// <editor-fold>
if ($moduloFiltros) { 
    
    $filtrosHtml = "<table id='tablaFiltrosCampos' style='margin-left:40px'>";
    $numFiltros = 0;   
    $arreglo = array();
    
    $tablaCamposSalida = "<tr>";

    while ($filtros= mssql_fetch_array($moduloFiltros)) {
                
        if($filtros["filtroVisible"] == "1"){                        
                      
            if(trim($filtros["filtroEtiqueta"]) != ""){                                
                
                //$filtrosHtml .= "<input id=" . $filtros["filtroHtml"] . " type='text' class='inputFiltro' placeHolder='" . htmlentities($filtros["filtroEtiqueta"]) . "'/><input type='text' class='inputFiltroRight' readonly disabled/>";
                //$filtrosHtml .= "<input id=" . $filtros["filtroHtml"] . " type='text'/>";
                
                $arreglo[$filtros["filtroOrden"]][$filtros["filtroColumna"] - 1] = "<td><input id=\""  . "chk" .  $filtros["filtroHtml"] . "\" class=\"styled\" type=\"checkbox\"></td><td width='340px'><input type='text' id=" . $filtros["filtroHtml"] . "></input></td>";                               
                
                
                if(($numFiltros % 6) == 0){
                    $tablaCamposSalida .= "</tr><tr>";
                }
                
                $tablaCamposSalida .= "";
                
                $scriptAC .= "$('#"  . $filtros["filtroHtml"] . "').tokenInput('../../complementos/diccionarios?nivelId=" . htmlentities($filtros["nivelId"]) . "', {
                                            theme: 'facebook',
                                            queryParam: 'filtro',
                                            minChars: 3,
                                            placeHolder: '" . mb_convert_encoding($filtros["filtroEtiqueta"], "UTF-8", "ISO-8859-1")  . "'
                                        });\n\n";                                                            
                
                $numFiltros++;
                
            }
            else{
                $arreglo[$filtros["filtroOrden"]][$filtros["filtroColumna"] - 1] = "<td></td><td></td>";
            }
        }
        else{
            
            if(trim($filtros["filtroHtml"]) != ""){               
                $filtrosHtml .= "<input id=" . $filtros["filtroHtml"] . " type='text' style='display:none'/><input type='text' readonly disabled style='display:none'/>";
            }            
        }
        
//        if(($numFiltros % 5) == 0){
//            $filtrosHtml.= "<br><br>";
//        }
            }
    
    $tablaCamposSalida .= "</tr>";        
    
    for($i = 1; $i <= count($arreglo); $i++){
        $filtrosHtml = $filtrosHtml . "<tr>";
        
        if($arreglo[$i][0] != ""){
            $filtrosHtml = $filtrosHtml . $arreglo[$i][0];
        }
        else{
            $filtrosHtml = $filtrosHtml . "<td></td><td></td>";
        }

        if($arreglo[$i][1] != ""){
            $filtrosHtml = $filtrosHtml . $arreglo[$i][1];
        }
        else{
            $filtrosHtml = $filtrosHtml . "<td></td><td></td>";
        }

        if($arreglo[$i][2] != ""){
            $filtrosHtml = $filtrosHtml . $arreglo[$i][2];
        }
        else{
            $filtrosHtml = $filtrosHtml . "<td></td><td></td>";
        }

        $filtrosHtml = $filtrosHtml . "</tr>";
    }   
    
    $filtrosHtml .= "</table>";
    

}
// </editor-fold>

// <editor-fold>
$tablaMercados = $objSem->mercadosDisponibles($mercados, "210px");
// </editor-fold>






?>

<script type="text/javascript" src="vinosExtracciones.js"></script>
<script type="text/javascript" src="vinosFiltros.js"></script>

<script type="text/javascript">


    $(document).ready(function(){
        
        <? echo $scriptAC; ?>    
                
        <? if ($clienteId == 1111) {?>        
            new Messi('A partir de Octubre de 2013 la información de mercado está basada en las Declaraciones de Aduanas y NO en la información de los Manifiestos Marítimos.', {center: false, align: 'right', titleClass: 'info', title: 'Aviso importante',  width: '320px', viewport: {top: '180px'}});
        <?}?>
                                                               
        $("#imgCargando").css("display","");
        buscarExtraccionesArchivos(<? echo $moduloId; ?>);                                            
        
        

    });


</script>
            
    <? echo $htmlMenu; ?>

    <div class="divMinHeight">


        <div id="divGuardarReporte" class="divPopUp">

            <label>GUARDAR REPORTE</label>

            <br><br>

            <input type="text" class="inputFiltro" placeHolder="Nombre"/>
            <input type="text" class="inputFiltroRight" readonly disabled/>

            <br><br>

            <div style="text-align: left; width:200px; margin-left: 50px; min-height: 160px">

                Nivel 1: Agente Portuario <br>
                Nivel 2: Puerto <br>

            </div>

            <div style="margin-left:10px">
                <input type="button" class="btnPlomo" value="CANCELAR" onclick="cerrarReporte()"/>                            
                <input type="button" class="btnAzul" value="GUARDAR"/>  
            </div>

        </div>

        <div id="divReportesGuardados" class="divPopUp">

            <label>REPORTES GUARDADOS</label>

            <br><br>

            <div style="text-align: left; width:200px; margin-left: 50px; min-height: 210px;">

                <table>
                    <tr>
                        <td>
                            <input type="checkbox" class="styled">
                        </td>
                        <td>
                            Reporte 1
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="checkbox" class="styled">
                        </td>
                        <td>
                            Reporte 2
                        </td>
                    </tr>
                </table>





            </div>

            <div style="margin-left:10px">
                <input type="button" class="btnPlomo" value="CANCELAR" onclick="cerrarReporte2()"/>                            
                <input type="button" class="btnAzul" value="GUARDAR"/>  
            </div>

        </div>

        <div class="divSubContenedor">

            <div class="divPanelBusqueda">
                <img src="#" alt="" title=""><label>PANEL DE BUSQUEDA</label>
            </div>

            <div id="divPanelFiltros" class="divPanelFiltros">
                <div class="divPanelFiltros1" style="height: 200px" >
                    
                    <input id="moduloId" type="hidden" value="<? echo $moduloId; ?>">
                    <input id="carpeta" type="hidden" value="<? echo $carpeta; ?>"/>

                    <table>
                        <tr>
                            <td valign="top" width="400px">
                                <label>FECHAS</label>

                                <br><br>

                                <input id="fechaDesde" type="text" class="inputFecha" placeHolder="Inicio" value=""/><input id="btnFechaDesde" type="button" class="inputFechaBtn" />
                                <input id="fechaDesdeDef" type="hidden"  value="<? echo $fechaDesde; ?>"/>


                                <script type="text/javascript">//<![CDATA[
                                    var myCal1 = Calendar.setup({
                                        checkRange: false,
                                        inputField: "fechaDesde",
                                        trigger: "btnFechaDesde",
                                        //onSelect: function () { this.hide(); validarFechasReportes($('fechaDesde'), 'fechaDesde', 'fechaHasta');},
                                        dateFormat: "%d-%m-%Y"
                                    });
                                    myCal1.setLanguage('es');
                                    //]]></script>

                                <input id="fechaHasta" type="text" class="inputFecha" placeHolder="Fin" value="" style="margin-left:60px"/><input id="btnFechaHasta" type="button" class="inputFechaBtn" />
                                <input id="fechaHastaDef" type="hidden"  value="<? echo $fechaHasta; ?>"/>

                                <script type="text/javascript">//<![CDATA[
                                    var myCal1 = Calendar.setup({
                                        checkRange: false,
                                        inputField: "fechaHasta",
                                        trigger: "btnFechaHasta",
                                        //onSelect: function () { this.hide(); validarFechasReportes($('fechaDesde'), 'fechaDesde', 'fechaHasta');},
                                        dateFormat: "%d-%m-%Y"
                                    });
                                    myCal1.setLanguage('es');
                                    //]]></script>
                                
                                
                                <div id="divMensajeFechas">

                                </div>

                            </td>
                            <? echo $tablaMercados;?>
                            <td valign="top">
                                <label class="niveles">DATOS</label>
                                <table>
                                    <tr>
                                        <td>
                                            <input id="btnContenedores" type="checkbox" class="checkGrafico styled" value="contenedores">
                                        </td>
                                        <td>
                                            N° de Contenedores
                                        </td>
                                        <td width="80px">

                                        </td>
                                        <td style="display:none">
                                            <input id="btnImpo" type="radio" name="btnEtapa" value="2">
                                        </td>
                                        <td style="display:none">
                                            Impo
                                        </td>
                                        <td style="display:none">
                                            <input id="btnExpo" type="radio" name="btnEtapa" value="1" checked>
                                        </td>
                                        <td style="display:none">
                                            Expo
                                        </td>
                                    </tr>                                    
                                    <tr>
                                        <? if($clienteId == "19"){?>                                        
										<td>
                                            <input id="btnFeus" type="checkbox" class="checkGrafico styled" value="feus">
                                        </td>
                                        <td valign="bottom">
                                            FFE
                                        </td>                                        
										
										<?}
										else{?>
										
										<td>
                                            <input id="btnTeus" type="checkbox" class="checkGrafico styled" value="teus">
                                        </td>
                                        <td valign="bottom">
                                            TEUs
                                        </td>                                        																
										<?}?>
                                        <td>

                                        </td>
                                        <td>
                                            <input id="btnContenedor" type="radio" value="C" name="btnContenedor">
                                        </td>
                                        <td>
                                            Contenedor
                                        </td>
                                        <td>
                                            <input id="btnNoContenedor" type="radio" value ="NC" name="btnContenedor">
                                        </td>
                                        <td>
                                            No Contenedor
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input id="btnToneladas" type="checkbox" class="checkGrafico styled" value="toneladas">
                                        </td>
                                        <td valign="bottom">
                                            Toneladas
                                        </td>
                                        <td>

                                        </td>
                                        <td>
                                            <input id="btnSeca" type="radio" value="1" name="btnSecaRefrigerada">
                                        </td>
                                        <td>
                                            Seca
                                        </td>
                                        <td>
                                            <input id="btnRefrigerada" type="radio" value="2" name="btnSecaRefrigerada">
                                        </td>
                                        <td>
                                            Refrigerada
                                        </td>
                                    </tr>
									<?if($clienteId == 34) {?>
									<tr>
										<td>											
											<input id="btnDespachos" type="checkbox" class="checkGrafico styled" value="despacho">																																				
										</td>
										<td>
											N° de despachos
										</td>
									
									</tr>
									<?}?>
									<? if($clienteId == 39) {?>
									<tr>
                                        <td>
                                            <input id="btnFob" type="checkbox" class="checkGrafico styled" value="fob">
                                        </td>
                                        <td>
                                            FOB (Miles USD)
                                        </td>
									</tr>
									<tr>
                                        <td>
                                            <input id="btnFlete" type="checkbox" class="checkGrafico styled" value="flete">
                                        </td>
                                        <td>
                                            Flete (Miles USD)
                                        </td>
										<td></td>
										<? if($clienteId != 19) { ?>										
										<td>
											<input id="btnFFW" type="radio" name="btnFww" value="1">
										</td>
										<td>
											FFWW
										</td>
										
										<?}?>
									</tr>
									<? }?>	
									<? if($clienteId == 9) {?>
									<tr>
                                        <td>
                                            
                                        </td>
                                        <td>
                                            
                                        </td>
									</tr>
									<tr>
                                        <td>
                                            
                                        </td>
                                        <td>
                                            
                                        </td>
										<td></td>
										<td>
											<input id="btnFFW" type="radio" name="btnFww" value="1">
										</td>
										<td>
											FFWW
										</td>
									</tr>
									<? }?>	

                                </table>
                            </td>
                        </tr>
                    </table>                                         

                    <div style="width:100%; margin-top: 0px; margin-bottom: 8px;">                            
                        <label>NOMBRE DEL ARCHIVO</label>                                                    
                    </div>

                    <input id="inputNombreArchivo" type="text" class="inputFiltro">
                    <input type="text" class="inputFiltroRight" disabled readonly>



                    <div style="width:100%; margin-top: 40px; margin-bottom: 8px;">                            
                        <label>FILTROS Y CAMPOS DE SALIDA</label>                                                    
                    </div>

                    <table class="tablaSalidas">
                        <tr>
                            <td>
                                <input id="chkSeleccionarTodo" type="checkbox" class="styled" onClick="seleccionarTodoExtracciones()">
                            </td>                                        
                            <td>
                                Todos
                            </td>
                        </tr>
                    </table>


                    <? echo $filtrosHtml; ?>

                    <div style="clear: both"></div>

                    
                    <div style="float:right;">

                    </div>                                                        

                </div>    

                <div style="clear: both"></div>

                <div class="divPanelFiltros2" style="height:auto">

                    <div style="width:96%;">

<!--                                <label>CAMPOS DE SALIDA</label>  

                        <table class="tablaSalidas">
                            <tr>
                                <td>
                                    <input id="chkSeleccionarTodo" type="checkbox" class="styled" onClick="seleccionarTodo()">
                                </td>                                        
                                <td>
                                    Todos
                                </td>
                            </tr>
                            <tr>
                                <td height="15px">

                                </td>
                            </tr>
                        </table>

                        <table id="tablaSalidas" class="tablaSalidas">
                            <? //echo $tablaCamposSalida; ?>
                        </table>    -->

                        <br>
                        
                        <div id="divMensaje" style="margin-left:0px; display:block; width:450px">
                            
                        </div>

                        <div style="">
                            <input type="button" class="btnLogin" value="GENERAR REPORTE" onClick="buscarExtracciones()"/>
<!--                                    <input type="button" class="btnLogin" value="LIMPIAR"/>  -->
                        </div>   

                        <br>

                    </div>   

                </div> 

                <div class="divPanelFiltros2" style="display:none">

                    <div style="width:100%; margin-bottom: 8px;">                            
                        <label>REPORTES GUARDADOS</label>                            
                    </div>   

                    <input type="text" class="inputFecha" placeHolder="Nombre"/><input type="button" class="inputLupaBtn" onclick="abrirReporte2()"/>

                    <input type="button" class="btnLogin" value="VER"/>

                </div>                                        

            </div>

<!--                    <div class="divPanelBusquedaBottom" onClick="mostrarFiltrosBusqueda()">

            </div>-->

        </div>

        <br style="clear:both"/> 

        <div style="height:70px">

            <center id="imgCargando" style="display:none">
                <img src="../../imagenes/cargando.gif" height="42" width="42"><br/><i>Cargando</i>
            </center>

        </div>




        <div class="divPanelBusqueda" style="margin: 0px auto;">
            <label>EXTRACCIONES</label>
        </div>

        <br>

        <table class="tablaExtracciones" cellspacing="0" cellspadding="0">
            <thead>
                <tr>            
                    <th class="thCenter" style="width:250px;">
                        <div class="divThLeft"></div>          
                        <div class="divThCenter">NOMBRE ARCHIVO</div>                
                    </th>                                                
                    <th class="thCenter" style="width:150px;">                
                        <div class="divThCenter">FECHA</div>                
                    </th>  
                    <th class="thCenter" colspan="2">                
                        <div class="divThCenter"></div>
                        <div class="divThRight"></div>          
                    </th>  
                </tr> 
            </thead>
            <tbody id="tBody">

            </tbody>

        </table>

    </div>

    <br>
            
<?
$plantilla->setPath('../../plantillas/');
$plantilla->setTemplate("footer");
echo $plantilla->show();
?>