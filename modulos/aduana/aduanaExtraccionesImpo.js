

function buscarExtracciones(){
    
    var nombreArchivo = $("#inputNombreArchivo").val();
    var carpeta = $("#carpeta").val();

    var fechaDesde = $("#fechaDesde").val();
    var fechaHasta = $("#fechaHasta").val();
    
    
    if(fechaDesde == "" && fechaHasta == ""){

        fechasDefecto("ADUANA");
        fechaDesde = $("#fechaDesde").val();
        fechaHasta = $("#fechaHasta").val();
    }
    
    if(validarFormulario()){

        $("#divResultado").html("");  

        $("#imgCargando").css("display","block");
        //$("#divPanelFiltros").css("display","none");            
        //$("#divMensaje").css("display","none");

        var aduana = $("#aduana").val();
        var clausula = $("#clausula").val();
        var importador = $("#importador").val();
        var importadorRut = $("#importadorRut").val();
        var formaPago = $("#formaPago").val();
        var naviera = $("#naviera").val();
        var paisOrigen = $("#paisOrigen").val();
        var puertoDescarga = $("#puertoDescarga").val();
        var puertoEmbarque = $("#puertoEmbarque").val();
        var producto = $("#producto").val();
        var marca = $("#marca").val();
        var variedad = $("#variedad").val();
        var tipoBulto = $("#tipoBulto").val();
        var tipoCarga = $("#tipoCarga").val();
        var unidadMedida = $("#unidadMedida").val();
        var viaTransporte = $("#viaTransporte").val();
        
        var chkaduana = $("#chkaduana").attr('checked');
        var chkclausula = $("#chkclausula").attr('checked');
        var chkimportador = $("#chkimportador").attr('checked');
        var chkimportadorRut = $("#chkimportadorRut").attr('checked');
        var chkformaPago = $("#chkformaPago").attr('checked');
        var chknaviera = $("#chknaviera").attr('checked');
        var chkpaisOrigen = $("#chkpaisOrigen").attr('checked');
        var chkpuertoDescarga = $("#chkpuertoDescarga").attr('checked');
        var chkpuertoEmbarque = $("#chkpuertoEmbarque").attr('checked');
        var chktipoBulto = $("#chktipoBulto").attr('checked');
        var chktipoCarga = $("#chktipoCarga").attr('checked');
        var chkproducto = $("#chkproducto").attr('checked');
        var chkmarca = $("#chkmarca").attr('checked');
        var chkvariedad = $("#chkvariedad").attr('checked');
        var chkunidadMedida = $("#chkunidadMedida").attr('checked');
        var chkviaTransporte = $("#chkviaTransporte").attr('checked');


        var tipoDato = "";
        
        if($("#btnFob").prop('checked')){
            tipoDato = tipoDato + "fob," 
        }        
        
        if($("#btnFletes").prop('checked')){
            tipoDato = tipoDato + "fletes," 
        } 
        
        if($("#btnBultos").prop('checked')){
            tipoDato = tipoDato + "bultos," 
        } 
        
        if($("#btnPeso").prop('checked')){
            tipoDato = tipoDato + "peso," 
        } 
        
        if($("#btnPesoNeto").prop('checked')){
            tipoDato = tipoDato + "pesoNeto," 
        } 
        
        if($("#btnSeguro").prop('checked')){
            tipoDato = tipoDato + "seguro," 
        } 
        
        
        var etapa = $("input[name=btnEtapa]:checked").val();
                
        $.ajax({
            type: "POST",
            url: "AduanaExpoExtraccionesDataImpo.php",
            data: { fechaDesde: fechaDesde, fechaHasta: fechaHasta, aduana: aduana, clausula: clausula, importadorRut: importadorRut, importador: importador, producto: producto,
                    marca: marca, variedad: variedad, naviera: naviera, paisOrigen: paisOrigen, puertoDescarga: puertoDescarga, puertoEmbarque: puertoEmbarque,
                    tipoBulto: tipoBulto, tipoCarga: tipoCarga, unidadMedida: unidadMedida, formaPago: formaPago, viaTransporte: viaTransporte, 

                    chkaduana: chkaduana, chkclausula: chkclausula, chkimportadorRut: chkimportadorRut, chkimportador: chkimportador, chkproducto: chkproducto,
                    chkmarca: chkmarca, chavariedad: chkvariedad, chknaviera: chknaviera, chkpaisOrigen: chkpaisOrigen, chkpuertoDescarga: chkpuertoDescarga, chkpuertoEmbarque: chkpuertoEmbarque, 
                    chktipoBulto: chktipoBulto, chktipoCarga: chktipoCarga, chkunidadMedida: chkunidadMedida, 
                    chkformaPago: chkformaPago, chkviaTransporte: chkviaTransporte, 

                nombreArchivo: nombreArchivo,tipoDato:tipoDato, etapa: etapa, carpeta: carpeta}
            })
            .done(function( msg ) {
                buscarExtraccionesArchivos($("#moduloId").val());
                $("#imgCargando").css("display","none");                                           

        }); 

    }                                                                    

}


function validarFormulario(){

    var mensaje = "";

    var nombreArchivo = $("#inputNombreArchivo").val();
    var fechaDesde = $("#fechaDesde").val();
    var fechaHasta = $("#fechaHasta").val();                                 
            
    if(nombreArchivo == ""){
        mensaje = "Debe ingresar un nombre para el archivo.<br>";
    }
    
    if(fechaDesde == ""){
        mensaje += "No ha ingresado la fecha inicial.<br>";
    }
    
    if(fechaHasta == ""){
        mensaje += "No ha ingresado la fecha final.<br>";
    }
    
    if(fechaDesde != "" && fechaHasta != "" && validarFechaMenor(fechaDesde, fechaHasta) == false){
        mensaje += "La fecha incial no puede ser mayor a la fecha final.<br>";
    }
    
    if(fechaDesde != "" && fechaHasta != "" && fechaDiferenciasDias(fechaDesde, fechaHasta) > 365){
        mensaje += "La diferencia entre las fechas no puede ser superior a un año.<br>";
    }
            
    if(mensaje != ""){
        $("#divMensaje").html(mensaje);        
        return false;
    }
    else{
        $("#divMensaje").html("");        
        return true;
    }

}
    
    
    