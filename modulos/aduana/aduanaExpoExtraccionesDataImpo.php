<?PHP

session_start();
include ("../../librerias/conexion.php");
require('aduana.class.php');
$objAduanaExpo = new aduana();


$usuarioId = $_SESSION['SEMPAT_usuarioId'];
$clienteId = $_SESSION['SEMPAT_clienteId'];

$fechaDesde = mb_convert_encoding(trim($_POST['fechaDesde']), "ISO-8859-1", "UTF-8");
$fechaHasta = mb_convert_encoding(trim($_POST['fechaHasta']), "ISO-8859-1", "UTF-8");
$carpeta = mb_convert_encoding(trim($_POST['carpeta']), "ISO-8859-1", "UTF-8");


$nombreArchivo = mb_convert_encoding(trim($_POST['nombreArchivo']), "ISO-8859-1", "UTF-8");

$tipoDato = mb_convert_encoding(trim($_POST['tipoDato']), "ISO-8859-1", "UTF-8");

$aduana = mb_convert_encoding(trim($_POST['aduana']), "ISO-8859-1", "UTF-8");
$clausula = mb_convert_encoding(trim($_POST['clausula']), "ISO-8859-1", "UTF-8");
$importadorRut = mb_convert_encoding(trim($_POST['importadorRut']), "ISO-8859-1", "UTF-8");
$importador = mb_convert_encoding(trim($_POST['importador']), "ISO-8859-1", "UTF-8");
$formaPago = mb_convert_encoding(trim($_POST['formaPago']), "ISO-8859-1", "UTF-8");
$naviera = mb_convert_encoding(trim($_POST['naviera']), "ISO-8859-1", "UTF-8");
$paisOrigen = mb_convert_encoding(trim($_POST['paisOrigen']), "ISO-8859-1", "UTF-8");
$producto = mb_convert_encoding(trim($_POST['producto']), "ISO-8859-1", "UTF-8");
$marca = mb_convert_encoding(trim($_POST['marca']), "ISO-8859-1", "UTF-8");
$variedad = mb_convert_encoding(trim($_POST['variedad']), "ISO-8859-1", "UTF-8");
$puertoDescarga = mb_convert_encoding(trim($_POST['puertoDescarga']), "ISO-8859-1", "UTF-8"); 
$puertoEmbarque = mb_convert_encoding(trim($_POST['puertoEmbarque']), "ISO-8859-1", "UTF-8");
$tipoBulto = mb_convert_encoding(trim($_POST['tipoBulto']), "ISO-8859-1", "UTF-8");
$tipoCarga = mb_convert_encoding(trim($_POST['tipoCarga']), "ISO-8859-1", "UTF-8");
$unidadMedida = mb_convert_encoding(trim($_POST['unidadMedida']), "ISO-8859-1", "UTF-8");
$viaTransporte = mb_convert_encoding(trim($_POST['viaTransporte']), "ISO-8859-1", "UTF-8");


$aduana = str_replace("\'", "'", $aduana);
$clausula = str_replace("\'", "'", $clausula);
$importador = str_replace("\'", "'", $importador);
$importadorRut = str_replace("\'", "'", $importadorRut);
$formaPago = str_replace("\'", "'", $formaPago);
$naviera = str_replace("\'", "'", $naviera);
$paisOrigen = str_replace("\'", "'", $paisOrigen);
$producto = str_replace("\'", "'", $producto);
$marca = str_replace("\'", "'", $marca);
$variedad = str_replace("\'", "'", $variedad);
$puertoDescarga = str_replace("\'", "'", $puertoDescarga);
$puertoEmbarque = str_replace("\'", "'", $puertoEmbarque);
$tipoBulto = str_replace("\'", "'", $tipoBulto);
$tipoCarga = str_replace("\'", "'", $tipoCarga);
$unidadMedida = str_replace("\'", "'", $unidadMedida);
$viaTransporte = str_replace("\'", "'", $viaTransporte);


$chkaduana = mb_convert_encoding(trim($_POST['chkaduana']), "ISO-8859-1", "UTF-8");
$chkclausula = mb_convert_encoding(trim($_POST['chkclausula']), "ISO-8859-1", "UTF-8");
$chkimportadorRut = mb_convert_encoding(trim($_POST['chkimportadorRut']), "ISO-8859-1", "UTF-8");
$chkimportador = mb_convert_encoding(trim($_POST['chkimportador']), "ISO-8859-1", "UTF-8");
$chkformaPago = mb_convert_encoding(trim($_POST['chkformaPago']), "ISO-8859-1", "UTF-8");
$chknaviera = mb_convert_encoding(trim($_POST['chknaviera']), "ISO-8859-1", "UTF-8");
$chkpaisOrigen = mb_convert_encoding(trim($_POST['chkpaisOrigen']), "ISO-8859-1", "UTF-8");
$chkproducto = mb_convert_encoding(trim($_POST['chkproducto']), "ISO-8859-1", "UTF-8");
$chkmarca = mb_convert_encoding(trim($_POST['chkmarca']), "ISO-8859-1", "UTF-8");
$chkvariedad = mb_convert_encoding(trim($_POST['chkvariedad']), "ISO-8859-1", "UTF-8");
$chkpuertoDescarga = mb_convert_encoding(trim($_POST['chkpuertoDescarga']), "ISO-8859-1", "UTF-8"); 
$chkpuertoEmbarque = mb_convert_encoding(trim($_POST['chkpuertoEmbarque']), "ISO-8859-1", "UTF-8");
$chktipoBulto = mb_convert_encoding(trim($_POST['chktipoBulto']), "ISO-8859-1", "UTF-8");
$chktipoCarga = mb_convert_encoding(trim($_POST['chktipoCarga']), "ISO-8859-1", "UTF-8");
$chkunidadMedida = mb_convert_encoding(trim($_POST['chkunidadMedida']), "ISO-8859-1", "UTF-8");
$chkviaTransporte = mb_convert_encoding(trim($_POST['chkviaTransporte']), "ISO-8859-1", "UTF-8");


$uploaddir = "../../Reportes/" . $carpeta . "/";
			
if (file_exists($uploaddir)==false) {
    mkdir($uploaddir, 0700, true);
}

$uploaddir = $uploaddir . "/" . $usuarioId . "/";

if (file_exists($uploaddir)==false) {
    mkdir($uploaddir, 0700, true);
    
}

$objAduanaExpo->aduanaImpoExtracciones($fechaDesde, $fechaHasta, $tipoDato, $clienteId, $aduana, $clausula, $importadorRut, $importador, $formaPago, $naviera, $paisOrigen, $producto, $marca, $variedad, $puertoDescarga, $puertoEmbarque, $tipoBulto, $tipoCarga, $unidadMedida, $viaTransporte, $chkaduana, $chkclausula, $chkimportadorRut, $chkimportador, $chkformaPago, $chknaviera, $chkpaisOrigen, $chkproducto, $chkmarca, $chkvariedad, $chkpuertoDescarga, $chkpuertoEmbarque, $chktipoBulto, $chktipoCarga, $chkunidadMedida, $chkviaTransporte, $nombreArchivo, $usuarioId);

?>
