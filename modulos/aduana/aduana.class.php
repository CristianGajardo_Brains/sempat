<?php

class aduana {
    
    
    function ultimaFechaManifiesto($clienteId) {
        $query = "Exec seleccionarFechasLimites '" . $clienteId . "','M'";
        $result = sql_db::sql_query($query);
        sql_db::sql_close();
        $row = sql_db::sql_fetch_assoc($result);
        return $row;
    }
    
    
    function manifiestoFechasDefecto($clienteId) {
        $query = "Exec seleccionarFechasDefecto '" . $clienteId . "','M'";
        $result = sql_db::sql_query($query);
        sql_db::sql_close();
        $row = sql_db::sql_fetch_assoc($result);
        return $row;
    }    
       
    
    //EXTRACCIONES
    
    function aduanaExpoExtracciones($fechaDesde, $fechaHasta, $tipoDato, $clienteId, $aduana, $clausula, $exportador, $formaPago, 
            $naviera, $paisDestino, $producto, $puertoDescarga, $puertoEmbarque, $tipoBulto, $tipoCarga, $unidadMedida, $viaTransporte, 
            $chkaduana, $chkclausula, $chkexportador, $chkformaPago, $chknaviera, $chkpaisDestino, $chkproducto, $chkpuertoDescarga, 
            $chkpuertoEmbarque, $chktipoBulto, $chktipoCarga, $chkunidadMedida, $chkviaTransporte, $nombreArchivo, $usuarioId){
        
        
        //echo 
        
        /*$query = "Exec aduanaExtracciones '" . $fechaDesde . "','" . $fechaHasta . "','" .$tipoDato . "','" . $aduana . "','" . $clausula . "','" . $exportador . "','" . $mercaderia . "','" . 
                $moneda . "','" . $nave . "','" . $naviera . "','" . $paisDestino . "','" .  $puertoDescarga . "','" . $puertoEmbarque . "','" . $tipoBulto . "','" . $tipoCarga . "','" .  
                $tipoOperacion . "','" . $trafico . "','" . $viaTransporte . "','" .                  
                $chkaduana . "','" . $chkclausula . "','" . $chkexportador . "','" . $chkmercaderia . "','" . $chkmoneda . "','" . $chknave . "','" . $chknaviera . "','" . 
                $chkpaisDestino . "','" . $chkpuertoDescarga . "','" . $chkpuertoEmbarque . "','" . $chktipoBulto . "','" . $chktipoCarga . "','" . $chktipoOperacion . "','" . $chktrafico . "','" . $chkviaTransporte . "','" . 
                $chkpaisDestino . "','" . $nombreArchivo . "','" . $usuarioId . "'";        
        echo $query;*/
                        
        $stmt = mssql_init("aduanaExpoExtracciones");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);        
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        //mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);        
        mssql_bind($stmt, '@aduana', $aduana, SQLVARCHAR);
        mssql_bind($stmt, '@clausula', $clausula, SQLVARCHAR);
        mssql_bind($stmt, '@exportador', $exportador, SQLVARCHAR);
        mssql_bind($stmt, '@formaPago', $formaPago, SQLVARCHAR);
        mssql_bind($stmt, '@naviera', $naviera, SQLVARCHAR);
        mssql_bind($stmt, '@paisDestino', $paisDestino, SQLVARCHAR);
        mssql_bind($stmt, '@producto', $producto, SQLVARCHAR);
        mssql_bind($stmt, '@puertoDescarga', $puertoDescarga, SQLVARCHAR);
        mssql_bind($stmt, '@puertoEmbarque', $puertoEmbarque, SQLVARCHAR);
        mssql_bind($stmt, '@tipoBulto', $tipoBulto, SQLVARCHAR);
        mssql_bind($stmt, '@tipoCarga', $tipoCarga, SQLVARCHAR);
        mssql_bind($stmt, '@unidadMedida', $unidadMedida, SQLVARCHAR);
        mssql_bind($stmt, '@viaTransporte', $viaTransporte, SQLVARCHAR);      
        
        mssql_bind($stmt, '@chkaduana', $chkaduana, SQLVARCHAR);
        mssql_bind($stmt, '@chkclausula', $chkclausula, SQLVARCHAR);
        mssql_bind($stmt, '@chkexportador', $chkexportador, SQLVARCHAR);
        mssql_bind($stmt, '@chkformaPago', $chkformaPago, SQLVARCHAR);
        mssql_bind($stmt, '@chknaviera', $chknaviera, SQLVARCHAR);
        mssql_bind($stmt, '@chkpaisDestino', $chkpaisDestino, SQLVARCHAR);
        mssql_bind($stmt, '@chkproducto', $chkproducto, SQLVARCHAR);
        mssql_bind($stmt, '@chkpuertoDescarga', $chkpuertoDescarga, SQLVARCHAR);
        mssql_bind($stmt, '@chkpuertoEmbarque', $chkpuertoEmbarque, SQLVARCHAR);
        mssql_bind($stmt, '@chktipoBulto', $chktipoBulto, SQLVARCHAR);
        mssql_bind($stmt, '@chktipoCarga', $chktipoCarga, SQLVARCHAR);
        mssql_bind($stmt, '@chkunidadMedida', $chkunidadMedida, SQLVARCHAR);
        mssql_bind($stmt, '@chkviaTransporte', $chkviaTransporte, SQLVARCHAR);                                      
        
        mssql_bind($stmt, '@nombreArchivo', $nombreArchivo, SQLVARCHAR);                                    
        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    function aduanaImpoExtracciones($fechaDesde, $fechaHasta, $tipoDato, $clienteId, $aduana, $clausula, $importadorRut, $importador, $formaPago, 
            $naviera, $paisOrigen, $producto, $marca, $variedad, $puertoDescarga, $puertoEmbarque, $tipoBulto, $tipoCarga, $unidadMedida, $viaTransporte, 
            $chkaduana, $chkclausula, $chkimportadorRut, $chkimportador, $chkformaPago, $chknaviera, $chkpaisOrigen, $chkproducto, 
            $chkmarca, $chkvariedad, $chkpuertoDescarga, $chkpuertoEmbarque, $chktipoBulto, $chktipoCarga, $chkunidadMedida, $chkviaTransporte, 
            $nombreArchivo, $usuarioId){
        
        
        //echo 
        
        /*$query = "Exec aduanaExtracciones '" . $fechaDesde . "','" . $fechaHasta . "','" .$tipoDato . "','" . $aduana . "','" . $clausula . "','" . $exportador . "','" . $mercaderia . "','" . 
                $moneda . "','" . $nave . "','" . $naviera . "','" . $paisDestino . "','" .  $puertoDescarga . "','" . $puertoEmbarque . "','" . $tipoBulto . "','" . $tipoCarga . "','" .  
                $tipoOperacion . "','" . $trafico . "','" . $viaTransporte . "','" .                  
                $chkaduana . "','" . $chkclausula . "','" . $chkexportador . "','" . $chkmercaderia . "','" . $chkmoneda . "','" . $chknave . "','" . $chknaviera . "','" . 
                $chkpaisDestino . "','" . $chkpuertoDescarga . "','" . $chkpuertoEmbarque . "','" . $chktipoBulto . "','" . $chktipoCarga . "','" . $chktipoOperacion . "','" . $chktrafico . "','" . $chkviaTransporte . "','" . 
                $chkpaisDestino . "','" . $nombreArchivo . "','" . $usuarioId . "'";        
        echo $query;*/
                        
        $stmt = mssql_init("aduanaImpoExtracciones");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);        
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        //mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);        
        mssql_bind($stmt, '@aduana', $aduana, SQLVARCHAR);
        mssql_bind($stmt, '@clausula', $clausula, SQLVARCHAR);
        mssql_bind($stmt, '@importadorRut', $importadorRut, SQLVARCHAR);
        mssql_bind($stmt, '@importador', $importador, SQLVARCHAR);
        mssql_bind($stmt, '@formaPago', $formaPago, SQLVARCHAR);
        mssql_bind($stmt, '@naviera', $naviera, SQLVARCHAR);
        mssql_bind($stmt, '@paisOrigen', $paisOrigen, SQLVARCHAR);
        mssql_bind($stmt, '@producto', $producto, SQLVARCHAR);
        mssql_bind($stmt, '@marca', $marca, SQLVARCHAR);
        mssql_bind($stmt, '@variedad', $variedad, SQLVARCHAR);
        mssql_bind($stmt, '@puertoDescarga', $puertoDescarga, SQLVARCHAR);
        mssql_bind($stmt, '@puertoEmbarque', $puertoEmbarque, SQLVARCHAR);
        mssql_bind($stmt, '@tipoBulto', $tipoBulto, SQLVARCHAR);
        mssql_bind($stmt, '@tipoCarga', $tipoCarga, SQLVARCHAR);
        mssql_bind($stmt, '@unidadMedida', $unidadMedida, SQLVARCHAR);
        mssql_bind($stmt, '@viaTransporte', $viaTransporte, SQLVARCHAR);      
        
        mssql_bind($stmt, '@chkaduana', $chkaduana, SQLVARCHAR);
        mssql_bind($stmt, '@chkclausula', $chkclausula, SQLVARCHAR);
        mssql_bind($stmt, '@chkimportadorRut', $chkimportadorRut, SQLVARCHAR);
        mssql_bind($stmt, '@chkimportador', $chkimportador, SQLVARCHAR);
        mssql_bind($stmt, '@chkformaPago', $chkformaPago, SQLVARCHAR);
        mssql_bind($stmt, '@chknaviera', $chknaviera, SQLVARCHAR);
        mssql_bind($stmt, '@chkpaisOrigen', $chkpaisOrigen, SQLVARCHAR);
        mssql_bind($stmt, '@chkproducto', $chkproducto, SQLVARCHAR);
        mssql_bind($stmt, '@chkmarca', $chkmarca, SQLVARCHAR);
        mssql_bind($stmt, '@chkvariedad', $chkvariedad, SQLVARCHAR);
        mssql_bind($stmt, '@chkpuertoDescarga', $chkpuertoDescarga, SQLVARCHAR);
        mssql_bind($stmt, '@chkpuertoEmbarque', $chkpuertoEmbarque, SQLVARCHAR);
        mssql_bind($stmt, '@chktipoBulto', $chktipoBulto, SQLVARCHAR);
        mssql_bind($stmt, '@chktipoCarga', $chktipoCarga, SQLVARCHAR);
        mssql_bind($stmt, '@chkunidadMedida', $chkunidadMedida, SQLVARCHAR);
        mssql_bind($stmt, '@chkviaTransporte', $chkviaTransporte, SQLVARCHAR);                                      
        
        mssql_bind($stmt, '@nombreArchivo', $nombreArchivo, SQLVARCHAR);                                    
        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    
    function aduanaExtraccionesArchivos($usuarioId, $moduloId) {
        $query = "Select * From Extraccion_Reportes (nolock) Where usuarioId = '" . $usuarioId . "' and modulo = '" . $moduloId . "' Order by fecha_reporte desc";
        //echo $query;
        $result = sql_db::sql_query($query);
        sql_db::sql_close();        
        return $result;
    }
    
    function aduanaExtraccionesSeleccionarArchivo($usuarioId, $moduloId, $arhivoId) {
        $query = "Select nombre_archivo From Extraccion_Reportes (nolock) Where usuarioId = '" . $usuarioId . "' and modulo = '" . $moduloId . "' and id_archivo = '" . $arhivoId . "'";                
        $result = sql_db::sql_query($query);
        $row = sql_db::sql_fetch_assoc($result);
        sql_db::sql_close();        
        return $row;
    }
    
    function aduanaExtraccionesEliminarArchivo($usuarioId, $moduloId, $arhivoId) {        
        $query = "Delete e From Extraccion_Reportes e Where usuarioId = '" . $usuarioId . "' and modulo = '" . $moduloId . "' and id_archivo = '" . $arhivoId . "'";        
        sql_db::sql_query($query);        
        sql_db::sql_close();        
        return true;
    }
    
    
}

?>


