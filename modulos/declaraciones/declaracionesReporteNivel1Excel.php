<?PHP

session_start();
include ("../../librerias/conexion.php");
require('declaraciones.class.php');
require('../../clases/sempat.class.php');
$objManifiesto = new declaracion();
$objSempat= new sempat();

$usuarioId = $_SESSION['SEMPAT_usuarioId'];
$clienteId = $_SESSION['SEMPAT_clienteId'];

include("declaracionesIdioma.php");

$fechaDesde = mb_convert_encoding(trim($_POST['fechaDesde']), "ISO-8859-1", "UTF-8");
$fechaHasta = mb_convert_encoding(trim($_POST['fechaHasta']), "ISO-8859-1", "UTF-8");

$nivel1 = mb_convert_encoding(trim($_POST['nivel1']), "ISO-8859-1", "UTF-8");
$nivel2 = mb_convert_encoding(trim($_POST['nivel2']), "ISO-8859-1", "UTF-8");
$nivel3 = mb_convert_encoding(trim($_POST['nivel3']), "ISO-8859-1", "UTF-8");
$nivel4 = mb_convert_encoding(trim($_POST['nivel4']), "ISO-8859-1", "UTF-8");
$nivel5 = mb_convert_encoding(trim($_POST['nivel5']), "ISO-8859-1", "UTF-8");

$nivel1Titulo = mb_convert_encoding(trim($_POST['nivel1Titulo']), "ISO-8859-1", "UTF-8");

$etapa = mb_convert_encoding(trim($_POST['etapa']), "ISO-8859-1", "UTF-8");
$mercado = mb_convert_encoding(trim($_POST['mercado']), "ISO-8859-1", "UTF-8");
$contenedor = mb_convert_encoding(trim($_POST['contenedor']), "ISO-8859-1", "UTF-8");
$dryReefer = mb_convert_encoding(trim($_POST['dryReefer']), "ISO-8859-1", "UTF-8");

$tipoEmbalaje = mb_convert_encoding(trim($_POST['tipoEmbalaje']), "ISO-8859-1", "UTF-8");
$statusContainer = mb_convert_encoding(trim($_POST['statusContainer']), "ISO-8859-1", "UTF-8");
$tipoDato = mb_convert_encoding(trim($_POST['tipoDato']), "ISO-8859-1", "UTF-8");

$puertoOrigen = mb_convert_encoding(trim($_POST['puertoOrigen']), "ISO-8859-1", "UTF-8");
$puertoEmbarque = mb_convert_encoding(trim($_POST['puertoEmbarque']), "ISO-8859-1", "UTF-8");
$puertoDescarga = mb_convert_encoding(trim($_POST['puertoDescarga']), "ISO-8859-1", "UTF-8");
$puertoDestino = mb_convert_encoding(trim($_POST['puertoDestino']), "ISO-8859-1", "UTF-8");

$paisOrigen = mb_convert_encoding(trim($_POST['paisOrigen']), "ISO-8859-1", "UTF-8");
$paisEmbarque = mb_convert_encoding(trim($_POST['paisEmbarque']), "ISO-8859-1", "UTF-8");
$paisDescarga = mb_convert_encoding(trim($_POST['paisDescarga']), "ISO-8859-1", "UTF-8");
$paisDestino = mb_convert_encoding(trim($_POST['paisDestino']), "ISO-8859-1", "UTF-8");

$trafico = mb_convert_encoding(trim($_POST['trafico']), "ISO-8859-1", "UTF-8");
$naviera = mb_convert_encoding(trim($_POST['naviera']), "ISO-8859-1", "UTF-8");
$agencia = mb_convert_encoding(trim($_POST['agencia']), "ISO-8859-1", "UTF-8");
$agenteDoc = mb_convert_encoding(trim($_POST['agenteDoc']), "ISO-8859-1", "UTF-8");
$nave = mb_convert_encoding(trim($_POST['nave']), "ISO-8859-1", "UTF-8");
$tipoNave = mb_convert_encoding(trim($_POST['tipoNave']), "ISO-8859-1", "UTF-8");
$shipper = mb_convert_encoding(trim($_POST['shipper']), "ISO-8859-1", "UTF-8");
$consignee = mb_convert_encoding(trim($_POST['consignee']), "ISO-8859-1", "UTF-8");

$granFamilia = mb_convert_encoding(trim($_POST['granFamilia']), "ISO-8859-1", "UTF-8");
$familia = mb_convert_encoding(trim($_POST['familia']), "ISO-8859-1", "UTF-8");
$subFamilia = mb_convert_encoding(trim($_POST['subFamilia']), "ISO-8859-1", "UTF-8");
$commodity = mb_convert_encoding(trim($_POST['commodity']), "ISO-8859-1", "UTF-8");

$tipoCarga = mb_convert_encoding(trim($_POST['tipoCarga']), "ISO-8859-1", "UTF-8");
$tipoServicio = mb_convert_encoding(trim($_POST['tipoServicio']), "ISO-8859-1", "UTF-8");

/** 07-02-2016 **/


$emisor = mb_convert_encoding(trim($_POST['emisor']), "ISO-8859-1", "UTF-8");
$almacen = mb_convert_encoding(trim($_POST['almacen']), "ISO-8859-1", "UTF-8");
$ffw = mb_convert_encoding(trim($_POST['ffw']), "ISO-8859-1", "UTF-8");

/** 04-09-2017 **/

$clausula = mb_convert_encoding(trim($_POST['clausula']), "ISO-8859-1", "UTF-8");

/** 04-03-2015 **/

$consultaTablaFechas = $objSempat->clientesFechasLimite($clienteId, $fechaDesde, $fechaHasta);
$fechaDesde = date('d-m-Y', strtotime($consultaTablaFechas["fechaDesde"])); 
$fechaHasta = date('d-m-Y', strtotime($consultaTablaFechas["fechaHasta"]));

$vertical  = mb_convert_encoding(trim($_POST['vertical']), "ISO-8859-1", "UTF-8");
$subVertical  = mb_convert_encoding(trim($_POST['subVertical']), "ISO-8859-1", "UTF-8");
$categoria  = mb_convert_encoding(trim($_POST['categoria']), "ISO-8859-1", "UTF-8");
$conVert  = mb_convert_encoding(trim($_POST['vertCon']), "ISO-8859-1", "UTF-8");
$sinVert  = mb_convert_encoding(trim($_POST['vertSin']), "ISO-8859-1", "UTF-8");
/****************/

$consultaTabla = $objManifiesto->declaracionReporteNivel1($fechaDesde, $fechaHasta, $nivel1, $nivel2, $nivel3, $nivel4, $nivel5, $tipoDato, $clienteId, $mercado, $etapa, $naviera, $agencia, $trafico, $puertoOrigen, $puertoEmbarque, $nave, $puertoDestino, $puertoDescarga, $tipoNave, $tipoServicio, $tipoCarga, $tipoEmbalaje, $paisOrigen, $paisEmbarque, $agenteDoc, $paisDestino, $paisDescarga, $statusContainer, $shipper, $consignee, $commodity, $granFamilia, $familia, $subFamilia, $dryReefer, $contenedor, $emisor, $almacen, $ffw, $clausula, $vertical, $subVertical, $categoria, $conVert, $sinVert, $usuarioId);


var_dump(consultaTabla);


?>
