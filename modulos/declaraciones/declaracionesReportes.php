<?
session_start();

setlocale(LC_TIME, "spanish");
$fechaActual = htmlentities(strftime("%A %d de %B del %Y"));
$fechaActual = ucfirst($fechaActual);

$usuarioLog = $_SESSION["SEMPAT_usuarioNombre"];
$usuarioId = $_SESSION["SEMPAT_usuarioId"];
$clienteId = $_SESSION["SEMPAT_clienteId"];

/** 28-02-2018 * */
include("declaracionesIdioma.php");

/* * ************* */

if ($_SESSION['SEMPAT_usuarioId'] == "") {
    echo "<script>window.location='../../index.php';</script>";
}

include("../../default.php");
$plantilla->setPath('../../plantillas/');
$plantilla->setTemplate("header");
$plantilla->setVars(array("USUARIO" => " $usuarioLog ",
    "FECHA" => "$fechaActual"
));
echo $plantilla->show();

require('../../clases/sempat.class.php');
$objSem = new sempat();

$moduloId = 35;
$menuId = 81;
$mercados = "1";


$htmlMenu = $objSem->menuRetornarHtml($usuarioId, $menuId);


$moduloFiltros = $objSem->admBuscarFiltros($clienteId, $moduloId);
$scriptAC = "";


//$consultaFechas = $objSem->manifiestoFechasDefecto($clienteId);
// <editor-fold>
if ($moduloFiltros) {

    if ($mercados == "") {
        $filttrosNivel = "<option value='1'>" . $labEtapa . "</option><option value='28'>Mercado</option>";
    } else {
        $filttrosNivel = "<option value='1'>" . $labEtapa . "</option>";
    }

    $filtrosHtml = "<table style='margin-left:40px'>";
    $numFiltros = 0;
    $arreglo = array();

    while ($filtros = mssql_fetch_array($moduloFiltros)) {

        if ($filtros["filtroVisible"] == "1") {

            $numFiltros++;

            if (trim($filtros["filtroEtiqueta"]) != "") {

                $filttrosNivel .= "<option value='" . $filtros["nivelId"] . "'>" . htmlentities($filtros["filtroEtiqueta"]) . "</option>";

                //$filtrosHtml .= "<input id=" . $filtros["filtroHtml"] . " type='text' class='inputFiltro' placeHolder='" . htmlentities($filtros["filtroEtiqueta"]) . "'/><input type='text' class='inputFiltroRight' readonly disabled/>";
                //$filtrosHtml .= "<input id=" . $filtros["filtroHtml"] . " type='text'/>";

                $arreglo[$filtros["filtroOrden"]][$filtros["filtroColumna"] - 1] = "<td width='340px'><input type='text' id=" . $filtros["filtroHtml"] . "></input></td>";

                $scriptAC .= "$('#" . $filtros["filtroHtml"] . "').tokenInput('../../complementos/diccionarios?nivelId=" . htmlentities($filtros["nivelId"]) . "', {
                                            theme: 'facebook',
                                            queryParam: 'filtro',
                                            minChars: 3,
                                            placeHolder: '" . mb_convert_encoding($filtros["filtroEtiqueta"], "UTF-8", "ISO-8859-1") . "'
                                        });\n\n";
            } else {
                $arreglo[$filtros["filtroOrden"]][$filtros["filtroColumna"] - 1] = "<td></td>";
            }
        } else {

            if (trim($filtros["filtroHtml"]) != "") {
                $filtrosHtml .= "<input id=" . $filtros["filtroHtml"] . " type='text' style='display:none'/><input type='text' readonly disabled style='display:none'/>";
            }
        }

//        if(($numFiltros % 5) == 0){
//            $filtrosHtml.= "<br><br>";
//        }
    }

    $filttrosNivel .= "<option value='18'>" . $labSeca . "/" . $labRefrigerada . "</option>";

    for ($i = 1; $i <= count($arreglo); $i++) {
        $filtrosHtml = $filtrosHtml . "<tr>";

        if ($arreglo[$i][0] != "") {
            $filtrosHtml = $filtrosHtml . $arreglo[$i][0];
        } else {
            $filtrosHtml = $filtrosHtml . "<td></td>";
        }

        if ($arreglo[$i][1] != "") {
            $filtrosHtml = $filtrosHtml . $arreglo[$i][1];
        } else {
            $filtrosHtml = $filtrosHtml . "<td></td>";
        }

        if ($arreglo[$i][2] != "") {
            $filtrosHtml = $filtrosHtml . $arreglo[$i][2];
        } else {
            $filtrosHtml = $filtrosHtml . "<td></td>";
        }

        $filtrosHtml = $filtrosHtml . "</tr>";
    }

    $filtrosHtml .= "</table>";
}
// </editor-fold>
// <editor-fold>
$tablaMercados = $objSem->mercadosDisponibles($mercados, "210px");
// </editor-fold>
?>

<? if ($idioma == "EN") { ?>
    <script type="text/javascript" src="declaracionesReportesEN.js"></script>
    <input id="idioma" type="text" value="EN" style="display:none"/>
<? } else {
    ?>
    <script type="text/javascript" src="declaracionesReportes.js"></script>
<? } ?>
<script type="text/javascript" src="declaracionesFiltros.js"></script>

<script type="text/javascript">

    $(document).ready(function () {

<? if ($usuarioId == "61") { ?>


            $('#btnTeus').click();
            $('#btnImpo').click();


<? } ?>

<? if ($clienteId == "37") { ?>

            $('#btnImpo').click();

<? } ?>


<? echo $scriptAC; ?>


<? if ($clienteId == 1111) { ?>
            new Messi('A partir de Octubre de 2013 la información de mercado está basada en las Declaraciones de Aduanas y NO en la información de los Manifiestos Marítimos.', {center: false, align: 'right', titleClass: 'info', title: 'Aviso importante', width: '320px', viewport: {top: '180px'}});
<? } ?>

    });

</script>

<? echo $htmlMenu; ?>

<div class="divMinHeight">


    <div id="divGuardarReporte" class="divPopUp">

        <label>GUARDAR REPORTE</label>

        <br><br>

        <input type="text" class="inputFiltro" placeHolder="Nombre"/>
        <input type="text" class="inputFiltroRight" readonly disabled/>

        <br><br>

        <div style="text-align: left; width:200px; margin-left: 50px; min-height: 160px">

            Nivel 1: Agente Portuario <br>
            Nivel 2: Puerto <br>

        </div>

        <div style="margin-left:10px">
            <input type="button" class="btnPlomo" value="CANCELAR" onclick="cerrarReporte()"/>                            
            <input type="button" class="btnAzul" value="GUARDAR"/>  
        </div>

    </div>

    <div id="divReportesGuardados" class="divPopUp">

        <label>REPORTES GUARDADOS</label>

        <br><br>

        <div style="text-align: left; width:200px; margin-left: 50px; min-height: 210px;">

            <table>
                <tr>
                    <td>
                        <input type="checkbox" class="styled">
                    </td>
                    <td>
                        Reporte 1
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="checkbox" class="styled">
                    </td>
                    <td>
                        Reporte 2
                    </td>
                </tr>
            </table>

        </div>

        <div style="margin-left:10px">
            <input type="button" class="btnPlomo" value="CANCELAR" onclick="cerrarReporte2()"/>                            
            <input type="button" class="btnAzul" value="GUARDAR"/>  
        </div>

    </div>



    <div class="divSubContenedor">
	


        <div class="divPanelBusqueda" onClick="mostrarFiltrosBusqueda()">
            <img src="../../imagenes/lupa.png" alt="" title=""><label><? echo $labPanelBusqueda; ?></label>
        </div>

        <div id="divPanelFiltros" class="divPanelFiltros">
            <div class="divPanelFiltros1" style="height: 200px" >

<?php if ($clienteId == 19 || $clienteId == 25 || $clienteId == 31 || $clienteId == 38 || $clienteId == 18 || $clienteId == 24 || $usuarioId == 204 || $usuarioId == 231 || $usuarioId == 275 || $usuarioId == 241 || $usuarioId == 314 || $usuarioId == 206 || $usuarioId == 215 || $usuarioId == 239 || $usuarioId == 278 || $usuarioId == 279 || $usuarioId == 242 || $usuarioId == 255 || $usuarioId == 257) { ?>
                    <div style="position: absolute; top: 20px; right: 0px;">
                        <input type="button" class="btnLogin" value="Share of Wallet" onclick="window.location = 'declaracionesShareOfWallet.php'"/><br>
                        <input type="button" class="btnLogin" value="Missing Account" onclick="window.location = 'declaracionesMissingAccount.php'"/>
                    </div>
<?php } ?>
                <table>
                    <tr>
                        <td valign="top" width="400px">
                            <label><? echo $labFechas; ?></label>

                            <br><br>

                            <input id="fechaDesde" type="text" class="inputFecha" placeHolder="<? echo $labFechaInicio; ?>" value=""/><input id="btnFechaDesde" type="button" class="inputFechaBtn" />
<!--                            <input id="fechaDesdeDef" type="hidden"  value="<? //echo $fechaDesde;  ?>"/>-->


                            <script type="text/javascript">//<![CDATA[
                                var myCal1 = Calendar.setup({
                                    checkRange: false,
                                    inputField: "fechaDesde",
                                    trigger: "btnFechaDesde",
                                    //onSelect: function () { this.hide(); validarFechasReportes($('fechaDesde'), 'fechaDesde', 'fechaHasta');},
                                    dateFormat: "%d-%m-%Y"
                                });
                                myCal1.setLanguage('es');
                                //]]></script>

                            <input id="fechaHasta" type="text" class="inputFecha" placeHolder="<? echo $labFechaFin; ?>" value="" style="margin-left:60px"/><input id="btnFechaHasta" type="button" class="inputFechaBtn" />
<!--                            <input id="fechaHastaDef" type="hidden"  value="<? //echo $fechaHasta;  ?>"/>-->


                            <script type="text/javascript">//<![CDATA[
                                var myCal1 = Calendar.setup({
                                    checkRange: false,
                                    inputField: "fechaHasta",
                                    trigger: "btnFechaHasta",
                                    //onSelect: function () { this.hide(); validarFechasReportes($('fechaDesde'), 'fechaDesde', 'fechaHasta');},
                                    dateFormat: "%d-%m-%Y"
                                });
                                myCal1.setLanguage('es');
                                //]]></script>


                            <div id="divMensajeFechas">

                            </div>

                        </td> 
<? echo $tablaMercados; ?>
                        <td valign="top">
                            <label class="niveles"><? echo $labDatos; ?></label>
                            <table>
                                <tr>
                                    <td>
                                        <input id="btnContenedores" type="radio" name="tipoDato" value="contenedores" checked>
                                    </td>
                                    <td>
<? echo $labNcontenedores; ?>
                                    </td>
                                    <td width="80px">

                                    </td>
                                    <td>
                                        <input id="btnImpo" type="radio" name="btnEtapa" value="2">
                                    </td>
                                    <td>
<? echo $labImpo; ?>
                                    </td>
                                    <td>
                                        <input id="btnExpo" type="radio" name="btnEtapa" value="1">
                                    </td>
                                    <td>
<? echo $labExpo; ?>
                                    </td>
                                </tr>                                    
                                <tr>
<? if ($clienteId == "19") { ?>

                                        <td>
                                            <input id="btnFeus" type="radio" name="tipoDato" value="feus">
                                        </td>
                                        <td valign="bottom">
                                            FFE
                                        </td>                                    									
<? } else {
    ?>

                                        <td>
                                            <input id="btnTeus" type="radio" name="tipoDato" value="teus">
                                        </td>
                                        <td valign="bottom">
                                            TEUs
                                        </td>

<? } ?>
                                    <td>

                                    </td>
                                    <td>
                                        <input id="btnContenedor" type="radio" value="C" name="btnContenedor" checked>
                                    </td>
                                    <td>
                                    <? echo $labContenedor; ?>
                                    </td>
                                    <td>
                                        <input id="btnNoContenedor" type="radio" value ="NC" name="btnContenedor" disabled>
                                    </td>
                                    <td>
                                        No <? echo $labContenedor; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input id="btnToneladas" type="radio" name="tipoDato" value="toneladas">
                                    </td>
                                    <td valign="bottom">
<? echo $labToneladas; ?>
                                    </td>
                                    <td>

                                    </td>
                                    <td>
                                        <input id="btnSeca" type="radio" value="1" name="btnSecaRefrigerada">
                                    </td>
                                    <td>
<? echo $labSeca; ?>
                                    </td>
                                    <td>
                                        <input id="btnRefrigerada" type="radio" value="2" name="btnSecaRefrigerada">
                                    </td>
                                    <td>
<? echo $labRefrigerada; ?>
                                    </td>
                                </tr>
<? if ($clienteId == 34) { ?>
                                    <tr>
                                        <td>
                                            <input id="btnDespachos" type="radio" name="tipoDato" value="despacho">																																				
                                        </td>
                                        <td>
                                            N° de despachos
                                        </td>

                                    </tr>
<? } ?>
<? if ($clienteId == 40) { ?>
                                    <tr>
                                        <td>
                                            <input id="btnBls" type="radio" name="tipoDato" value="bls">																																				
                                        </td>
                                        <td>
                                            N° de bls
                                        </td>

                                    </tr>
<? } ?>
<? if ($clienteId == 25 || $clienteId == 1 || $clienteId == 16 || $clienteId == 19 || $clienteId == 21 || $clienteId == 22 || $clienteId == 23 || $clienteId == 24 || $clienteId == 25 || $clienteId == 26 || $clienteId == 29 || $clienteId == 30 || $clienteId == 31 || $clienteId == 32 || $clienteId == 33 || $clienteId == 34 || $clienteId == 36 || $clienteId == 37 || $clienteId == 38 || $clienteId == 18 || $clienteId == 40 || $clienteId == 41 || $clienteId == 42 || $clienteId == 3) { ?>
                                    <tr>
                                        <td>
                                            <input id="btnFob" type="radio" name="tipoDato" value="fob">
                                        </td>
                                        <td>
                                    <? echo $labFob; ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input id="btnFlete" type="radio" name="tipoDato" value="flete">
                                        </td>
                                        <td>
    <? echo $labFlete; ?>
                                        </td>
                                        <td></td>
                                        <td>
                                            <input id="btnFFW" type="radio" name="btnFww" value="1">
                                        </td>
                                        <td>
    <? echo $labFFWW; ?>
                                        </td>
                                    </tr>
<? }
if ($clienteId == 9) {
    ?>
                                    <tr>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>

                                        </td>
                                        <td>

                                        </td>
                                        <td></td>
                                        <td>
                                            <input id="btnFFW" type="radio" name="btnFww" value="1">
                                        </td>
                                        <td>
                                    <? echo $labFFWW; ?>
                                        </td>
                                    </tr>
<? } ?>


                            </table>
                        </td>
                    </tr>
                </table>

                <div style="width:100%; margin-top: 35px; margin-bottom: 8px;">                            
                    <label><? echo $labNiveles; ?></label>                                                    
                </div>

                <select id="selectNivel1" name="selectNivel1" class="styled filtro">
                    <option value="" class="placeHolder"><? echo $labNivel; ?> 1</option>
<? echo $filttrosNivel; ?>
                </select>

                <select id="selectNivel2" name="selectNivel2" class="styled filtro">
                    <option value=""  class="placeHolder"><? echo $labNivel; ?> 2</option>
<? echo $filttrosNivel; ?>
                </select>

                <select id="selectNivel3" name="selectNivel3" class="styled filtro">
                    <option value=""  class="placeHolder"><? echo $labNivel; ?> 3</option>
<? echo $filttrosNivel; ?>
                </select>

                <select id="selectNivel4" name="selectNivel4" class="styled filtro">
                    <option value=""  class="placeHolder"><? echo $labNivel; ?> 4</option>
                    <? echo $filttrosNivel; ?>
                </select>

                <select id="selectNivel5" name="selectNivel5" class="styled filtro">
                    <option value="" class="placeHolder"><? echo $labNivel; ?> 5</option>
                    <? echo $filttrosNivel; ?>
                </select>


                <div style="width:100%; margin-top: 40px; margin-bottom: 8px;">                            
                    <label><? echo $labFiltros; ?></label>                                                    
                </div>

<? echo $filtrosHtml; ?>

                <div style="clear: both"></div>

                <div id="divMensaje">
<? echo $mensajeNivel1; ?>
                </div>

                <div style="float:right; margin-right: 90px; margin-top: 15px; margin-bottom: 15px;">
                    <input type="button" class="btnLogin" value="<? echo $labBuscar; ?>" onclick="buscarNivel1()"/>                            
<!--                                <input type="button" class="btnLogin" value="GUARDAR" onclick="abrirReporte()"/>  -->
                </div>                                                        

            </div>    

            <div style="clear: both"></div>

            <div class="divPanelFiltros2" style="display:none">

                <div style="width:100%; margin-bottom: 8px;">                            
                    <label>REPORTES GUARDADOS</label>                            
                </div>   

                <input type="text" class="inputFecha" placeHolder="Nombre"/><input type="button" class="inputLupaBtn" onclick="abrirReporte2()"/>

                <input type="button" class="btnLogin" value="VER"/>

                <br/><br/>

            </div>                                        

        </div>

        <div class="divPanelBusquedaBottom" onClick="mostrarFiltrosBusqueda()">

        </div>

    </div>

    <div id="divGraficos" style="height: 405px; display:none;">

        <div id="divGrafico" class="divGrafico" style="width:1122px; height:400px; display:none;">

        </div>                        

        <div id="divGraficoA" class="divGraficoLeft" style="width:540px; height:390px; display:none;">

        </div>

        <div id="divGraficoB" class="divGraficoRight" style="width:540px; height:390px; display:none;">

        </div> 

    </div>

    <br style="clear:both"/>   

    <div id="divOpciones" class="divOpciones" style="display:none">  

        <table>
            <tr>
                <td width="300px" valign="top">
                    <label><? echo $labNiveles; ?></label>
                    <table id="tablaNivelesDesc" class="tablaNiveles">

                    </table>
                </td>
                <td id="tdResumen" width="475px" valign="top">

                </td>
                <td width="300px" rowspan="2" valign="top">
                    <table>
                        <tr style="height:30px;">
                            <td width="100px" valign="middle">
                                <label><? echo $labGraficos; ?></label>
                            </td>
                            <td width="230px" valign="middle">
                                <select id="selectGraficoReporte" name="selectGrafico" class="styled" style="height:26px;">
                                    <option value="line"><? echo $grLineas; ?></option>
                                    <option value="column"><? echo $grBarras; ?></option>
                                    <option value="pie"><? echo $grPorcentaje; ?></option>
                                </select>

                                <input id="tituloGrafico" type="hidden" value="">
                                <input id="tituloGrafico2" type="hidden" value="">
                                <input id="tituloGraficoEjeY" type="hidden" value="">
                            </td>
                        </tr>
                        <tr style="height:30px;">
                            <td valign="middle" class="tdTipo">
                                <label><? echo $labTipoCnt; ?></label>
                            </td>
                            <td class="tdTipo">
                                <select id="selectTipoReporte" name="selectTipoReporte" class="styled" style="height:26px;">
                                    <option value=""><? echo $labAmbos; ?></option>
                                    <option value="C20">C20</option>
                                    <option value="C40">C40</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

    </div>

    <center id="imgCargando" style="display:none">
        <img src="../../imagenes/cargando.gif" height="42" width="42"><br/><i><? echo $labCargando; ?></i>
    </center>

    <div id="divResultado">


    </div>	

</div>

<br>

<?
$plantilla->setPath('../../plantillas/');
$plantilla->setTemplate("footer");
echo $plantilla->show();
?>