<?PHP

session_start();
include ("../../librerias/conexion.php");
require('declaraciones.class.php');
require('../../clases/sempat.class.php');
$objManifiesto = new declaracion();
$objSempat = new sempat();



$usuarioId = $_SESSION['SEMPAT_usuarioId'];
$clienteId = $_SESSION['SEMPAT_clienteId'];
$clienteNombre = $_SESSION["SEMPAT_clienteNombre"];
$grupoEmisorId = $_SESSION["SEMPAT_grupoEmisorId"];

$fechaDesde = mb_convert_encoding(trim($_POST['fechaDesde']), "ISO-8859-1", "UTF-8");
$fechaHasta = mb_convert_encoding(trim($_POST['fechaHasta']), "ISO-8859-1", "UTF-8");

$nivel2 = mb_convert_encoding(trim($_POST['nivel2']), "ISO-8859-1", "UTF-8");
$nivel3 = mb_convert_encoding(trim($_POST['nivel3']), "ISO-8859-1", "UTF-8");
$nivel4 = mb_convert_encoding(trim($_POST['nivel4']), "ISO-8859-1", "UTF-8");
$nivel5 = mb_convert_encoding(trim($_POST['nivel5']), "ISO-8859-1", "UTF-8");

$tipoDato = mb_convert_encoding(trim($_POST['tipoDato']), "ISO-8859-1", "UTF-8");

$trId = mb_convert_encoding(trim($_POST['trId']), "ISO-8859-1", "UTF-8");

$nivel1Id = mb_convert_encoding(trim($_POST['nivel1Id']), "ISO-8859-1", "UTF-8");
$otros = mb_convert_encoding(trim($_POST['otros']), "ISO-8859-1", "UTF-8");

$totalGroup = mb_convert_encoding(trim($_POST['total']), "ISO-8859-1", "UTF-8");

$consultaTabla = $objManifiesto->declaracionReporteNivel2($fechaDesde, $fechaHasta, $nivel2, $nivel3, $nivel4, $nivel5, $tipoDato, $clienteId, $nivel1Id, $otros, $usuarioId);




$meses = array();

$mesInicial = substr($fechaDesde, 3, 2);
$añoInicial = substr($fechaDesde, 6, 4);

$mesFinal = substr($fechaHasta, 3, 2);
$añoFinal = substr($fechaHasta, 6, 4);

for ($a = 0; $a < 12; $a++) {

    $mes = "00" . ($mesInicial);
    $mes = substr($mes, strlen($mes) - 2, strlen($mes));

    $meses[$a][0] = $añoInicial . "-" . $mes;

    if ($mes >= $mesFinal && $añoInicial >= $añoFinal) {
        $a = 12;
    }

    if ($mes == 12) {
        $añoInicial++;
        $mesInicial = "01";
    } else {
        $mesInicial++;
    }
}


$campo1 = "";
$valorCampo1 = "";
$salida = "";
$salidaOtros = "";
$total = 0;
$totalAnterior = 0;

$fila = 0;

$cursor = "";

if ($nivel3 != "") {
    $cursor = " style='cursor:pointer'";
}

if ($consultaTabla) {

    $numFilas = mssql_num_rows($consultaTabla);
    $cont2 = 3;

    while ($manifiesto = mssql_fetch_array($consultaTabla)) {

        $fila++;

        $campo1 = htmlentities($manifiesto[1]);

        if (strlen($campo1) > 22) {
            $valorCampo1 = substr($campo1, 0, 20) . "...";
        } else {
            $valorCampo1 = $campo1;
        }


        if ($campo1 != "OTROS") {

            $salida .= "<tr id='" . $trId . "-" . $fila . "' ";

            if (strcmp($clienteNombre, $campo1) === 0) {
                $salida .= " class='nivel2 pinta'";
            } else {
                $salida .= " class='nivel2'";
            }

            $img = 'off';
            $iclass = 'c-off';
            if ($cont2 > 0) {
                $img = 'on';
                $iclass = 'c-on';
                $cont2--;
            }

            $salida .= "><td class='check'><img src='../../imagenes/$img.png' class='$iclass' onclick='rotator(this)'></td><td class='nivel2' " . $cursor . " title =\"" . $campo1 . "\" onClick='buscarNivel3(this, \"" . $fechaDesde . "\", \"" . $fechaHasta . "\", \"" . $nivel3 . "\", \"" . $nivel4 . "\", \"" . $nivel5 . "\", \"" . $tipoDato . "\", \"" . $nivel1Id . "\", \"" . $manifiesto[2] . "\");'>" . $valorCampo1 . "</td>";



            for ($i = 0; $i < count($meses); $i++) {

                $salida .= "<td>" . number_format($manifiesto[3 + $i], 0, '', '.') . "</td>";
                //$meses[$i][$fila] .= number_format($manifiesto[3 + $i], 0, '', '');
            }

            if ($totalGroup == 0) {
                $porc = 0;
            } else {
                $porc = round((100 * round($manifiesto[16])) / $totalGroup, 2);
            }

            //$salida .= "<td><b>" . number_format($manifiesto[16], 0, '', '.') . "</b></td><td><b>" . number_format($manifiesto[15], 0, '', '.') . "a</b></td>";                                    
            $salida .= "<td><b>" . number_format($manifiesto[16], 0, '', '.') . "</b></td><td><b>" . $porc . "%</b></td>";
            //$salida .= $objSempat->calculoDiferencia($manifiesto[16], $manifiesto[15]) . "</tr>";
            $salida .= "</tr>";

            $total = $total + number_format($manifiesto[16], 0, '', '');
            $totalAnterior = $totalAnterior + number_format($manifiesto[15], 0, '', '');
        } else {

            if ($manifiesto[16] != "0" && $manifiesto[16] != "") {
                $salidaOtros .= "<tr id='" . $trId . "-" . ($numFilas + 1) . "' ";



                $salidaOtros .= " class='nivel2'><td class='check'><input type='checkbox' class='styled'/></td><td class='nivel2' title =\"" . $campo1 . "\">" . $valorCampo1 . "</td>";

                for ($i = 0; $i < count($meses); $i++) {

                    $salidaOtros .= "<td>" . number_format($manifiesto[3 + $i], 0, '', '.') . "</td>";
                    //$meses[$i][$numFilas + 1] .= number_format($manifiesto[3 + $i], 0, '', '');
                }

                $porc = round((100 * round($manifiesto[16])) / $totalGroup, 2);

                //$salidaOtros .= "<td><b>" . number_format($manifiesto[16], 0, '', '.') . "</b></td><td><b>" . number_format($manifiesto[15], 0, '', '.') . "</b></td>";
                $salidaOtros .= "<td><b>" . number_format($manifiesto[16], 0, '', '.') . "</b></td><td><b>" . $porc . "%</b></td>";
                //$salidaOtros .= $objSempat->calculoDiferencia($manifiesto[16], $manifiesto[15]) . "</tr>";
                $salidaOtros .= "</tr>";

                $total = $total + number_format($manifiesto[16], 0, '', '');
                $totalAnterior = $totalAnterior + number_format($manifiesto[15], 0, '', '');
            }
        }
    }

    echo $salida . $salidaOtros;
} else {
    echo "";
}


//echo $salida;
?>
