<?php

class declaracion {
    
	
	function traerTop20Nivel1($nivel1,$usuarioId, $mesDesde, $mesHasta, $añoDesde, $añoHasta){
		
		$queryNivelCampo = " select  nivelCampo from NivelesManifiesto where nivelId = ".$nivel1." ";
        $result = sql_db::sql_query($queryNivelCampo);
        sql_db::sql_close();
        $row = sql_db::sql_fetch_assoc($result);
        $nivelCampo = $row['nivelCampo'];
		
		$queryNivelNombre = " select  nivelNombre from NivelesManifiesto where nivelId = ".$nivel1." ";
        $result = sql_db::sql_query($queryNivelNombre);
        sql_db::sql_close();
        $row = sql_db::sql_fetch_assoc($result);
        $nivelNombre = $row['nivelNombre'];
		
		$queryNivelConsulta = " select  replace(nivelConsulta , '@nivel', 'd.nivel1') as 'nivelConsulta' from NivelesManifiesto where nivelId = ".$nivel1." ";
        $result = sql_db::sql_query($queryNivelConsulta);
        sql_db::sql_close();
        $row = sql_db::sql_fetch_assoc($result);
        $nivelConsulta = $row['nivelConsulta'];
		
		
		
		
		
		$where = "";
		
		if($nivel1 == 2 || $nivel1 == 3){
			
			if($nivelCampo == "consigneeId"){
				$where = " AND nivel1 <> 1032162 ";
			}
			if($nivelCampo == "shipperId"){
				$where = " AND nivel1 <> 1032162 ";
			}
			
		}
		
		
		if($nivelCampo == "navieraId"){
			$where = " AND nivel1 <> 2939 ";
		}
		//echo $where;

		$Fdesde = $añoDesde.$mesDesde;
		$Fhasta = $añoHasta.$mesHasta; 
        $query = "SELECT TOP 20  nivel1 
					FROM DeclaracionesReporte d
					". $nivelConsulta ."
					WHERE usuarioId = ". $usuarioId ."
					AND fecha BETWEEN ". $Fdesde. " 
					AND ". $Fhasta ." 
					".$where." 
					AND ". $nivelNombre." not in ('UNKNOWN','TO THE ORDER','OTHERS','OCASIONALES','OTROS')
					GROUP BY nivel1 , ". $nivelNombre."
					ORDER  BY  SUM(total)  DESC ";        
		//echo $query;
		//echo $nivelConsulta;
		//echo $nivelNombre;
		//echo $nivelCampo;
		$result = sql_db::sql_query($query);
        sql_db::sql_close();        
        //return $row;
        //sql_db::sql_close();      
		//echo "query nivel 1: " . $query;
        return $result;
	}
	
	
	
	
	function traerTopNivel2($usuarioId, $mesDesde, $mesHasta, $añoDesde, $añoHasta,$nivel1,$valn1,$nivel2){
		
		$queryNivelNombre = " select  nivelNombre from NivelesManifiesto where nivelId = ".$nivel2." ";
        $result = sql_db::sql_query($queryNivelNombre);
        sql_db::sql_close();
        $row = sql_db::sql_fetch_assoc($result);
        $nivelNombre = $row['nivelNombre'];
		
		$queryNivelConsulta = " select  replace(nivelConsulta , '@nivel', 'd.nivel2') as 'nivelConsulta' from NivelesManifiesto where nivelId = ".$nivel2." ";
        $result = sql_db::sql_query($queryNivelConsulta);
        sql_db::sql_close();
        $row = sql_db::sql_fetch_assoc($result);
        $nivelConsulta = $row['nivelConsulta'];
		
		$queryNivelCampo = " select  nivelCampo from NivelesManifiesto where nivelId = ".$valn1." ";
        $result = sql_db::sql_query($queryNivelCampo);
        sql_db::sql_close();
        $row = sql_db::sql_fetch_assoc($result);
        $nivelCampo = $row['nivelCampo'];
		
		$queryNivelCampo2 = " select  nivelCampo from NivelesManifiesto where nivelId = ".$nivel2." ";
        $result2 = sql_db::sql_query($queryNivelCampo2);
        sql_db::sql_close();
        $row2 = sql_db::sql_fetch_assoc($result2);
        $nivelCampo2 = $row2['nivelCampo'];
		
		$where = "";
		
		if($valn1 == 2 || $valn1 == 3){
			
			if($nivelCampo == "consigneeId"){
				$where = " AND nivel1 <> 1032162 ";
			}
			if($nivelCampo == "shipperId"){
				$where = " AND nivel1 <> 1032162 ";
			}
			
		}
		
		if($nivelCampo == "navieraId"){
			$where = " AND nivel1 <> 2939 ";
		}
		
		$where2 = "";
		
		if($nivel2 == 2 || $nivel2 == 3){
			
			if($nivelCampo2 == "consigneeId"){
				$where2 = " AND nivel2 <> 1032162 ";
			}
			if($nivelCampo2 == "shipperId"){
				$where2 = " AND nivel2 <> 1032162 ";
			}
			
		}
		
		if($nivelCampo2 == "navieraId"){
			$where2 = " AND nivel2 <> 2939 ";
		}
		
		
		$Fdesde = $añoDesde.$mesDesde;
		$Fhasta = $añoHasta.$mesHasta; 
		
		$query = "SELECT  TOP 20 nivel2  ,".$nivelNombre."
					FROM DeclaracionesReporte d
					". $nivelConsulta ."
					WHERE usuarioId = ". $usuarioId ." 
					AND fecha BETWEEN ". $Fdesde. " AND ". $Fhasta ."
					". $where ."
					". $where2 ."
					AND nivel1 =  ". $nivel1 ."
					AND ".$nivelNombre." not in ('UNKNOWN','TO THE ORDER','OTHERS','OCASIONALES','OTROS','')
					GROUP BY nivel2, ".$nivelNombre."
					ORDER  BY  SUM(total)   DESC ";
					
		//echo "query nivel 2" . $query;	
	   // echo "Nivel Nombre " . $nivelNombre;
		//echo "Nivel Consulta " . $nivelConsulta;
		$result = sql_db::sql_query($query);
        sql_db::sql_close();               
        return $result;
		
		
	}
	
	function generarConsultaExcel($nivel1,$usuarioId,$arregloNivel1,$arregloPrueba,$valn1,$nivel2 ){
		
		$queryNivelCampo = " select  nivelCampo from NivelesManifiesto where nivelId = ".$nivel1." ";
        $result = sql_db::sql_query($queryNivelCampo);
        sql_db::sql_close();
        $row = sql_db::sql_fetch_assoc($result);
        $nivelCampo = $row['nivelCampo'];
		
		$where = "";
		
		if($valn1 == 2 || $valn1 == 3){
			
			if($nivelCampo == "consigneeId"){
				$where = " AND nivel1 <> 1032162 ";
			}
			if($nivelCampo == "shipperId"){
				$where = " AND nivel1 <> 1032162 ";
			}
			
		}
		
		
		if($nivelCampo == "navieraId"){
			$where = " AND nivel1 <> 2939 ";
		}
		
		
		
        $cabezeraInsert = "INSERT INTO [dbo].[sempatReportesExcel]
							([usuarioId]
						   ,[nivel1]
						   ,[nivel2]
						   ,[total1]
						   ,[total2]
						   ,[campo1]
						   ,[campo2] ";
		$cuerpoInsert = "";
		
		for ($a = 0; $a < count($arregloPrueba) ; $a++) {
			
		   $cuerpoInsert .= "  ,[mes".($a+1)."] ";
			
			
		}	
		
		$pieInsert = ",[totalAnterior]
					   ,[orden]
					   ,[orden2]) "; 
					   
		$consultaInsert = $cabezeraInsert."".$cuerpoInsert."".$pieInsert;			   
		
	
		$queryNivelNombre = " select  nivelNombre from NivelesManifiesto where nivelId = ".$nivel1." ";
        $result = sql_db::sql_query($queryNivelNombre);
        sql_db::sql_close();
        $row = sql_db::sql_fetch_assoc($result);
        $nivelNombre = $row['nivelNombre'];
		
		$queryNivelConsulta = " select  replace(nivelConsulta , '@nivel', 'd.nivel1') as 'nivelConsulta' from NivelesManifiesto where nivelId = ".$nivel1." ";
        $result = sql_db::sql_query($queryNivelConsulta);
        sql_db::sql_close();
        $row = sql_db::sql_fetch_assoc($result);
        $nivelConsulta = $row['nivelConsulta'];
		
		$cabezeraSql = " ".$consultaInsert ."
						SELECT  
						d.usuarioId, 
						1, 
						0  as 'nivel2', 
						SUM(total) as 'total1',
						0  as 'total2',  
						".$nivelNombre."  as 'campo1',
						'' as 'campo2', ";
						
		$posFin = count($arregloPrueba)-1;

		$cuerpoSql = '';
		for ($i = 0; $i < count($arregloPrueba) ; $i++) {
			
		   $cuerpoSql .= " ISNULL((SELECT  SUM(total) AS 'total_anterior' FROM DeclaracionesReporte WHERE usuarioId = ".$usuarioId."  AND fecha = ".$arregloPrueba[$i]."  AND nivel1 = ".$arregloNivel1." ".$where." GROUP BY  nivel1 ),0) as 'mes".($i+1)."', ";
			
			
		}				
		
		$pieSql = " ISNULL(( SELECT  SUM(total) as 'total_anterior' from DeclaracionesReporte WHERE usuarioId = ".$usuarioId." AND fecha = 0  AND nivel1 = ".$arregloNivel1." ".$where." GROUP BY nivel1 ),0) as 'total_anterior' ,
					2  as 'orden',
					1 as 'orden2' ";
		
		
		$sqlFrom = " from DeclaracionesReporte d 
					".$nivelConsulta."
					WHERE d.usuarioId = ".$usuarioId."
					AND d.fecha between ".$arregloPrueba[0]." and ".$arregloPrueba[$posFin]." 
					". $where ." 
					 and nivel1 = ".$arregloNivel1."
					GROUP BY d.usuarioId, ".$nivelNombre." , d.nivel1 
					ORDER  BY  sum(total)   DESC  ";
					
		$consultaSql = $cabezeraSql."".$cuerpoSql."".$pieSql."".$sqlFrom;
		
		//echo $consultaSql;
		//echo "consulta 1". $consultaSql;
        sql_db::sql_query($consultaSql);        
        sql_db::sql_close();   
        return true;

				
	}
	
	function generarConsultaExcelNivel2($nivel1,$nivel2,$usuarioId,$arregloNivel1,$arregloNivel2,$arregloPrueba){
		
		$queryNivelNombre = " select  nivelNombre from NivelesManifiesto where nivelId = ".$nivel1." ";
        $result = sql_db::sql_query($queryNivelNombre);
        sql_db::sql_close();
        $row = sql_db::sql_fetch_assoc($result);
        $nivelNombre = $row['nivelNombre'];
		
		$queryNivelConsulta = " select  replace(nivelConsulta , '@nivel', 'd.nivel1') as 'nivelConsulta' from NivelesManifiesto where nivelId = ".$nivel1." ";
        $result = sql_db::sql_query($queryNivelConsulta);
        sql_db::sql_close();
        $row = sql_db::sql_fetch_assoc($result);
        $nivelConsulta = $row['nivelConsulta'];
		
		$posFin = count($arregloPrueba)-1;
		
		
		//consulta de datos nivel 2
		$queryNivelNombre2 = " select  nivelNombre from NivelesManifiesto where nivelId = ".$nivel2." ";
        $result0 = sql_db::sql_query($queryNivelNombre2);
        sql_db::sql_close();
        $row0 = sql_db::sql_fetch_assoc($result0);
        $nivelNombre2 = $row0['nivelNombre'];
		
		$queryNivelConsulta2 = " select  replace(nivelConsulta , '@nivel', 'd.nivel2') as 'nivelConsulta' from NivelesManifiesto where nivelId = ".$nivel2." ";
        $result01 = sql_db::sql_query($queryNivelConsulta2);
        sql_db::sql_close();
        $row01 = sql_db::sql_fetch_assoc($result01);
        $nivelConsulta2 = $row01['nivelConsulta'];

		$queryNivelCampo = " select  nivelCampo from NivelesManifiesto where nivelId = ".$nivel1." ";
        $result = sql_db::sql_query($queryNivelCampo);
        sql_db::sql_close();
        $row = sql_db::sql_fetch_assoc($result);
        $nivelCampo = $row['nivelCampo'];
		
		$queryNivelCampo2 = " select  nivelCampo from NivelesManifiesto where nivelId = ".$nivel2." ";
        $result2 = sql_db::sql_query($queryNivelCampo2);
        sql_db::sql_close();
        $row2 = sql_db::sql_fetch_assoc($result2);
        $nivelCampo2 = $row2['nivelCampo'];
		
		$where = "";
		
		if($nivel1 == 2 || $nivel1 == 3){
			
			if($nivelCampo == "consigneeId"){
				$where = " AND nivel1 <> 1032162 ";
			}
			if($nivelCampo == "shipperId"){
				$where = " AND nivel1 <> 1032162 ";
			}
			
		}
		
		if($nivelCampo == "navieraId"){
			$where = " AND nivel1 <> 2939 ";
		}
		
		$where2 = "";
		
		if($nivel2 == 2 || $nivel2 == 3){
			
			if($nivelCampo2 == "consigneeId"){
				$where2 = " AND nivel2 <> 1032162 ";
			}
			if($nivelCampo2 == "shipperId"){
				$where2 = " AND nivel2 <> 1032162 ";
			}
			
		}
		
		if($nivelCampo2 == "navieraId"){
			$where2 = " AND nivel2 <> 2939 ";
		}


		$queryNivel2 = "select top 20 nivel2  from DeclaracionesReporte
						WHERE usuarioId =  ".$usuarioId."
						AND fecha between ".$arregloPrueba[0]." and ".$arregloPrueba[$posFin]." 
						".$where."
						".$where2."
						AND nivel1 = ".$arregloNivel1."
						GROUP BY nivel2
						ORDER  BY  sum(total)   DESC ";
		$result02 = sql_db::sql_query($queryNivelNombre);
        sql_db::sql_close();
		
		
		
		$cabezeraInsert = "INSERT INTO [dbo].[sempatReportesExcel]
							([usuarioId]
						   ,[nivel1]
						   ,[nivel2]
						   ,[total1]
						   ,[total2]
						   ,[campo1]
						   ,[campo2] ";
		$cuerpoInsert = "";
		
		for ($b = 0; $b < count($arregloPrueba) ; $b++) {
			
		   $cuerpoInsert .= "  ,[mes".($b+1)."] ";
			
			
		}	
		
		$pieInsert = ",[totalAnterior]
					   ,[orden]
					   ,[orden2]) "; 
					   
		$consultaInsert = $cabezeraInsert."".$cuerpoInsert."".$pieInsert;	
		
		
		//$cantidad = count($arregloNivel2);
		
		$cabezeraSql2 = " ".$consultaInsert."
						SELECT d.usuarioId,
						0 as 'nivel1', 
						2 as 'nivel2', 
						(SELECT SUM(total) from DeclaracionesReporte d WHERE d.usuarioId = ".$usuarioId." AND d.fecha between ".$arregloPrueba[0]." and ".$arregloPrueba[$posFin]." " .$where. "  and nivel1 = ".$arregloNivel1." ) as 'total1',
						 SUM(total) as 'total2',
						 ".$nivelNombre." as 'campo1',
						 ".$nivelNombre2." as 'campo2', ";
		
		
		
	
		
		$cuerpoSql2  = '';
		
		for ($p = 0; $p <  count($arregloPrueba); $p++) {
			

			 $cuerpoSql2 .= " ISNULL((select  sum(total)  from DeclaracionesReporte WHERE usuarioId = ".$usuarioId." AND fecha = ".$arregloPrueba[$p]."  and nivel1 = ".$arregloNivel1." ".$where." and nivel2 = ".$arregloNivel2."  group by nivel1 ),0) as 'mes".($p+1)."', ";
				
		}
		
		$pieSql2 = "ISNULL(( select  sum(total)  from DeclaracionesReporte WHERE usuarioId = ".$usuarioId." AND fecha = 0  and nivel1 = ".$arregloNivel1." ".$where." and nivel2 = ".$arregloNivel2." group by nivel1 ),0) as 'total_anterior' ,
					0  as 'orden',
					2 as 'orden2' ";
					
					
		$sqlFrom2 = " from DeclaracionesReporte d 
					".$nivelConsulta."
					".$nivelConsulta2."
					WHERE d.usuarioId = ".$usuarioId."
					AND d.fecha between ".$arregloPrueba[0]." and ".$arregloPrueba[$posFin]."  
					".$where."
					".$where2."
					and nivel1 = ".$arregloNivel1."
					and nivel2 = ".$arregloNivel2."
					GROUP BY d.usuarioId,  ".$nivelNombre.", ".$nivelNombre2.", d.nivel2 
					ORDER  BY  sum(total)   DESC  ";
					
		$consultaSql2 = $cabezeraSql2."".$cuerpoSql2."".$pieSql2."".$sqlFrom2;
		
		//echo $consultaSql;
		//echo "consulta 2: ". $consultaSql2;
		//echo "consulta 2". $consultaSql2;
        sql_db::sql_query($consultaSql2);        
        sql_db::sql_close();   
        return true;
		
					
	}
	
	function insertarOtrosNivel2($nivel1,$usuarioId,$arregloNivel1,$arregloPrueba, $nivel2){
		
			
		$queryNivelCampo = " select  nivelCampo from NivelesManifiesto where nivelId = ".$nivel1." ";
        $result = sql_db::sql_query($queryNivelCampo);
        sql_db::sql_close();
        $row = sql_db::sql_fetch_assoc($result);
        $nivelCampo = $row['nivelCampo'];
		
		$queryNivelCampo2 = " select  nivelCampo from NivelesManifiesto where nivelId = ".$nivel2." ";
        $result2 = sql_db::sql_query($queryNivelCampo2);
        sql_db::sql_close();
        $row2 = sql_db::sql_fetch_assoc($result2);
        $nivelCampo2 = $row2['nivelCampo'];
		
		
		$where = "";
		
		if($nivel1 == 2 || $nivel1 == 3){
			
			if($nivelCampo == "consigneeId"){
				$where = " AND nivel1 <> 1032162 ";
			}
			if($nivelCampo == "shipperId"){
				$where = " AND nivel1 <> 1032162 ";
			}
			
		}
		
		if($nivelCampo == "navieraId"){
			$where = " AND nivel1 <> 2939 ";
		}
		
		$where2 = "";
		
		if($nivel2 == 2 || $nivel2 == 3){
			
			if($nivelCampo2 == "consigneeId"){
				$where2 = " AND nivel2 <> 1032162 ";
			}
			if($nivelCampo2 == "shipperId"){
				$where2 = " AND nivel2 <> 1032162 ";
			}
			
		}
		
		if($nivelCampo2 == "navieraId"){
			$where2 = " AND nivel2 <> 2939 ";
		}
	
		
		
		$queryNivelNombre = " select  nivelNombre from NivelesManifiesto where nivelId = ".$nivel1." ";
        $result = sql_db::sql_query($queryNivelNombre);
        sql_db::sql_close();
        $row = sql_db::sql_fetch_assoc($result);
        $nivelNombre = $row['nivelNombre'];
		
		$queryNivelConsulta = " select  replace(nivelConsulta , '@nivel', 'd.nivel1') as 'nivelConsulta' from NivelesManifiesto where nivelId = ".$nivel1." ";
        $result = sql_db::sql_query($queryNivelConsulta);
        sql_db::sql_close();
        $row = sql_db::sql_fetch_assoc($result);
        $nivelConsulta = $row['nivelConsulta'];
		
		$posFin = count($arregloPrueba)-1;
		
		
		
		$cabezeraInsert = "INSERT INTO [dbo].[sempatReportesExcel]
							([usuarioId]
						   ,[nivel1]
						   ,[nivel2]
						   ,[total1]
						   ,[total2]
						   ,[campo1]
						   ,[campo2] ";
		$cuerpoInsert = "";
		
		for ($a = 0; $a < count($arregloPrueba) ; $a++) {
			
		   $cuerpoInsert .= "  ,[mes".($a+1)."] ";
			
			
		}	
		
		$pieInsert = ",[totalAnterior]
					   ,[orden]
					   ,[orden2]) "; 
					   
		$consultaInsert = $cabezeraInsert."".$cuerpoInsert."".$pieInsert;	
				
		$cabezeraSqlOtrosNivel2 = " ".$consultaInsert."
								   SELECT  d.usuarioId,
								   0 as 'nivel1', 
								   2 as 'nivel2', 
								   0 as 'total1',
								   (SELECT sum(total1)  from sempatReportesExcel WHERE usuarioId = ".$usuarioId." and campo1 =  ".$nivelNombre." and nivel1 = 1) -  (SELECT sum(total2)  from sempatReportesExcel WHERE usuarioId = ".$usuarioId." and campo1 =  ".$nivelNombre." and nivel1 = 0)  as 'total2',
								   
								   " .$nivelNombre. " as 'campo1',
								  'OTROS' as 'campo2', ";
								  
		$cuerpoSqlOtrosNivel2  = '';
		
		for ($m = 0; $m <  count($arregloPrueba); $m++) {
			

			 //$cuerpoSqlOtrosNivel2 .= " ISNULL((select  sum(total)  from DeclaracionesReporte WHERE usuarioId = ".$usuarioId." AND fecha = ".$arregloPrueba[$p]."  and nivel1 = ".$arregloNivel1." and nivel1 <> 2939 and nivel2 = ".$arregloNivel2."  group by nivel1 ),0) as 'mes".($p+1)."', ";
				$cuerpoSqlOtrosNivel2 .= " ISNULL((SELECT sum(mes".($m+1).")  from sempatReportesExcel WHERE usuarioId = ".$usuarioId." and campo1 =  ".$nivelNombre." and nivel1 = 1) - (SELECT sum(mes".($m+1).")  from sempatReportesExcel WHERE usuarioId = ".$usuarioId." and campo1 =  ".$nivelNombre." and nivel1 = 0),0) as 'mes".($m+1)."', ";
		}
		
		$pieSqlSqlOtrosNivel2 = " ISNULL((SELECT sum(totalAnterior)  from sempatReportesExcel WHERE usuarioId = ".$usuarioId." and campo1 =  ".$nivelNombre." and nivel1 = 1) - (SELECT sum(totalAnterior)  from sempatReportesExcel WHERE usuarioId = ".$usuarioId." and campo1 =  ".$nivelNombre." and nivel1 = 0),0) as 'total_anterior' ,
									2 as 'orden',
									0 as 'orden2' ";
									
		$sqlFromOtrosNivel2 = " from DeclaracionesReporte d
									".$nivelConsulta."
									WHERE usuarioId = ".$usuarioId."
									AND fecha between ".$arregloPrueba[0]." and ".$arregloPrueba[$posFin]." 
									 ". $where ."
								     ". $where2 ."
									 AND nivel1 = ".$arregloNivel1." 
									GROUP BY  d.usuarioId, ".$nivelNombre."
									ORDER  BY  sum(total)   DESC  ";
		
		$consultaSqlOtrosNivel2 = $cabezeraSqlOtrosNivel2."".$cuerpoSqlOtrosNivel2."".$pieSqlSqlOtrosNivel2."".$sqlFromOtrosNivel2;
		
		//echo $consultaSqlOtrosNivel2;
	   // echo "consulta 3". $consultaSqlOtrosNivel2;
        sql_db::sql_query($consultaSqlOtrosNivel2);        
        sql_db::sql_close();  
		return true;		
		
		
		
	}
	
	function insertarOtrosExcel($usuarioId, $mesDesde, $mesHasta, $añoDesde, $añoHasta,$arregloPrueba){
		
		$Fdesde = $añoDesde.$mesDesde;
		$Fhasta = $añoHasta.$mesHasta; 
		
		$cabezeraInsert = "INSERT INTO [dbo].[sempatReportesExcel]
							([usuarioId]
						   ,[nivel1]
						   ,[nivel2]
						   ,[total1]
						   ,[total2]
						   ,[campo1]
						   ,[campo2] ";
		$cuerpoInsert = "";
		
		for ($a = 0; $a < count($arregloPrueba) ; $a++) {
			
		   $cuerpoInsert .= "  ,[mes".($a+1)."] ";
			
			
		}	
		
		$pieInsert = ",[totalAnterior]
					   ,[orden]
					   ,[orden2]) "; 
					   
		$consultaInsert = $cabezeraInsert."".$cuerpoInsert."".$pieInsert;
		
		$cabezeraSql = " ".$consultaInsert."
					    SELECT  
					    d.usuarioId, 
					    1 as 'nivel1', 
					    0  as 'nivel2', 
					   (select  sum(total)  from DeclaracionesReporte WHERE usuarioId = ".$usuarioId." AND fecha between ". $Fdesde . " and ". $Fhasta ." )  - (SELECT ISNULL(sum(mes1),0)+ISNULL(sum(mes2),0)+ISNULL(sum(mes3),0)+ISNULL(sum(mes4),0)+ISNULL(sum(mes5),0)+ISNULL(sum(mes6),0)+ISNULL(sum(mes7),0)+ISNULL(sum(mes8),0)+ISNULL(sum(mes9),0)+ISNULL(sum(mes10),0)+ISNULL(sum(mes11),0)+ISNULL(sum(mes12),0)  from sempatReportesExcel WHERE usuarioId = ".$usuarioId." and nivel1 = 1 ) 'total1',
					  	0  as 'total2',  
					   'OTROS'  as 'campo1',
				      	'' as 'campo2',  ";
						
		$cuerpoSql  = '';
		
		for ($p = 0; $p <  count($arregloPrueba); $p++) {
			

			 $cuerpoSql .= " (select  sum(total)  from DeclaracionesReporte WHERE usuarioId = ". $usuarioId ." AND fecha = ".$arregloPrueba[$p]."  )  - (SELECT ISNULL(sum(mes".($p+1)."),0)   from sempatReportesExcel WHERE usuarioId = ". $usuarioId ." and nivel1 = 1 ) as 'mes".($p+1)."', ";
				
		}
		
		
		$pieSql = " ISNULL(( select  sum(total)  from DeclaracionesReporte WHERE usuarioId = ". $usuarioId ." AND fecha = 0 ) - (SELECT sum(totalAnterior)   from sempatReportesExcel WHERE usuarioId = ". $usuarioId ." and nivel1 = 1 ),0) as 'total_anterior',
					1  as 'orden',
					1 as 'orden2'
					from DeclaracionesReporte d 
					WHERE d.usuarioId = ". $usuarioId ." 
					GROUP BY d.usuarioId
					ORDER  BY  sum(total)   DESC  
					 ";
					 
		$consultaSql = $cabezeraSql."".$cuerpoSql."".$pieSql;
		
		//echo $consultaSql;
		//echo $consultaSql2;
		//echo "consulta 4". $consultaSql;
        sql_db::sql_query($consultaSql);        
        sql_db::sql_close();   
        return true;			 
					 
	}
	
	function insertarTotalExcel($usuarioId, $mesDesde, $mesHasta, $añoDesde, $añoHasta,$arregloPrueba){
		
		$Fdesde = $añoDesde.$mesDesde;
		$Fhasta = $añoHasta.$mesHasta; 
		
		$cabezeraInsert = "INSERT INTO [dbo].[sempatReportesExcel]
							([usuarioId]
						   ,[nivel1]
						   ,[nivel2]
						   ,[total1]
						   ,[total2]
						   ,[campo1]
						   ,[campo2] ";
		$cuerpoInsert = "";
		
		for ($a = 0; $a < count($arregloPrueba) ; $a++) {
			
		   $cuerpoInsert .= "  ,[mes".($a+1)."] ";
			
			
		}	
		
		$pieInsert = ",[totalAnterior]
					   ,[orden]
					   ,[orden2]) "; 
					   
		$consultaInsert = $cabezeraInsert."".$cuerpoInsert."".$pieInsert;
		
		$cabezeraSql = " ".$consultaInsert."
					    SELECT  
						d.usuarioId, 
						1 as 'nivel1', 
						0  as 'nivel2', 
						(SELECT sum(total1)  from sempatReportesExcel WHERE usuarioId = ". $usuarioId ." and nivel1 = 1)  'total1',
						0  as 'total2',  
						'TOTAL'  as 'campo1',
						'' as 'campo2',   ";
						
		$cuerpoSql  = '';
		
		for ($p = 0; $p <  count($arregloPrueba); $p++) {
			

			 $cuerpoSql .= " (SELECT sum(mes".($p+1).")  from sempatReportesExcel WHERE usuarioId = ". $usuarioId ." and nivel1 = 1 ) as 'mes".($p+1)."', ";
				
		}
		
		
		$pieSql = " (SELECT ISNULL(sum(totalAnterior),0)   from sempatReportesExcel WHERE usuarioId = ". $usuarioId ." and nivel1 = 1) as 'total_anterior',
					0  as 'orden',
					1 as 'orden2'
					from DeclaracionesReporte d 
					WHERE d.usuarioId = ". $usuarioId ."  
					GROUP BY d.usuarioId
					ORDER  BY  sum(total)   DESC    
					 ";
					 
		$consultaSql = $cabezeraSql."".$cuerpoSql."".$pieSql;
		
		//echo $consultaSql;
		//echo $consultaSql2;
	   //echo "consulta 5". $consultaSql;
        sql_db::sql_query($consultaSql);        
        sql_db::sql_close();   
        return true;			 
					 
	}
	
	function EliminarConsultaUsuario($usuarioId){
		$query = "DELETE  from sempatReportesExcel WHERE usuarioId = ".$usuarioId." ";
		$result = sql_db::sql_query($query);
        sql_db::sql_close();
		return true;
	}
	
	function EliminarRegistrosCon0($usuarioId){
		$query = "delete
				  FROM sempatReportesExcel WHERE usuarioId = ".$usuarioId."
				  and mes1 = 0
				  and mes2 = 0
					and mes3 = 0
					and mes4 = 0
					and mes5 = 0
					and mes6 = 0
					and mes7 = 0
					and mes8 = 0
					and mes9 = 0
					and mes10 = 0
					and mes11 = 0
					and mes12 = 0";
		$result = sql_db::sql_query($query);
        sql_db::sql_close();
		return true;
	}
	
	function obtenerConsultaUsuario($usuarioId){
		$query = " SELECT * from sempatReportesExcel WHERE usuarioId = ".$usuarioId." ";
		//echo $query;
		$result = sql_db::sql_query($query);
		
        sql_db::sql_close();
		//var_dump($result);
		return $result;
	}
    
    function ultimaFechaDeclaracion($clienteId) {
        $query = "Exec seleccionarFechasLimites '" . $clienteId . "','M'";
        $result = sql_db::sql_query($query);
        sql_db::sql_close();
        $row = sql_db::sql_fetch_assoc($result);
        return $row;
    }
    
    
    function declaracionFechasDefecto($clienteId) {
        $query = "Exec seleccionarFechasDefecto '" . $clienteId . "','M'";
        $result = sql_db::sql_query($query);
        sql_db::sql_close();
        $row = sql_db::sql_fetch_assoc($result);
        return $row;
    }
    
    
    //INDICADORES
    
    function indicadoresCabeceraDatos($fecha) {
        $query = "Exec declaracionesIndicadoresCabecera '" . $fecha . "'";        
        //echo $query;
        $result = sql_db::sql_query($query);
        $row = sql_db::sql_fetch_assoc($result);
        sql_db::sql_close();
        return $row;
    }
    
    
    function indicadoresCabeceraHtml($valorNivel1, $fecha){
        
        $objSem = new declaracion();
        $resultado = $objSem->indicadoresCabeceraDatos($fecha);
        
        $titulo1 = "ENERO";
        $titulo2 = "";
        $titulo3 = "";
        $titulo2b = "";
        $titulo3b = "";
        
        if(strtoupper($resultado["mmActual"]) != "ENERO"){
            $titulo1 = "ENERO - " . strtoupper($resultado["mmActual"]);
        }

        if($resultado["mmAnterior"] != "" && $resultado["mmActual"] != ""){
            $titulo2 = $resultado["mmAnterior"] . " - " . $resultado["mmActual"];
        }
        
        if($resultado["mmActual"] != ""){
            $titulo3 = $resultado["mmActual"];
        }
        
        if($resultado["mmActual"] == "DICIEMBRE"){
            $titulo2b = $resultado["yyyyActual"];
        }
        else{
            if($resultado["yyyyAnterior"] != ""){
                $titulo2b = $resultado["yyyyAnterior"] . " - " . ($resultado["yyyyActual"]);
            }            
        }
        
        if($resultado["mmActual"] == "DICIEMBRE") {
            $titulo3b = $resultado["yyyyAnterior"];            
        }
        else {            
            if($resultado["yyyyAnterior"] != ""){
                $titulo3b = ($resultado["yyyyAnterior"] - 1) . " - " . ($resultado["yyyyAnterior"]);                
            }                        
        }
        
        
        $htmlThead = "<tr>
                        <th class=\"thCenter\" colspan=\"6\">
                            <div class=\"divThLeft\"></div>
                            <div class=\"divThCenter\">" . $titulo1 . "</div>
                            <div class=\"divThRight\"></div>          
                        </th>                        
                        <th style=\"min-width: 10px;\"></th>
                        <th class=\"thCenter\" colspan=\"4\">
                            <div class=\"divThLeft\"></div>          
                            <div class=\"divThCenter\">" . $titulo2 . "</div>
                            <div class=\"divThRight\"></div>          
                        </th> 
                        <th style=\"min-width: 10px;\"></th>
                        <th class=\"thCenter\" colspan=\"4\">
                            <div class=\"divThLeft\"></div>          
                            <div class=\"divThCenter\">" . $titulo3 . "</div>
                            <div class=\"divThRight\"></div>          
                        </th> 
                    </tr>   
                    <tr>
                        <td colspan=\"2\" class=\"left\" style=\"width:185px;\">
                                " . mb_convert_encoding(trim($valorNivel1), "UTF-8", "ISO-8859-1") .  "
                        </td>
                        <td>
                            " . $resultado["yyyyActual"] . "
                        </td>
                        <td>
                            " . $resultado["yyyyAnterior"] . "
                        </td>
                        <td>
                            DIF.
                        </td>
                        <td>
                            Mkt. SHARE
                        </td>
                        <td class=\"clear\">

                        </td>
                        <td class=\"left\">
                            " . $titulo2b .  "
                        </td>
                        <td>
                            " . $titulo3b .  "
                        </td>
                        <td>
                            DIF.
                        </td>
                        <td>
                            Mkt. SHARE
                        </td>
                        <td class=\"clear\">

                        </td>
                        <td class=\"left\">
                            " . $resultado["yyyyActual"] . "
                        </td>
                        <td>
                            " . $resultado["yyyyAnterior"] . "
                        </td>
                        <td>
                            DIF.
                        </td>
                        <td>
                            Mkt. SHARE
                        </td>                        
                    </tr>";

        return $htmlThead;       

    }
    
    
    function declaracionIndicadoresNivel1($fechaDesde, $fechaHasta, $nivel1, $nivel2, $nivel3, $nivel4, $nivel5, $tipoDato, $clienteId, $mercado, $etapa, $naviera, $agencia, $trafico, 
            $puertoOrigen, $puertoEmbarque, $nave, $puertoDestino, $puertoDescarga, $tipoNave, $tipoServicio, $tipoCarga, $tipoEmbalaje, $paisOrigen, $paisEmbarque,
            $agenteDoc, $paisDestino, $paisDescarga, $statusContainer, $shipper, $consignee, $commodity, $granFamilia, $familia, $subFamilia, $dryReefer, $contenedor, $emisor, $almacen, $mercadoFFW, $clausula, $vertical, $subVertical, $categoria, $conVert, $sinVert, $usuarioId){
    
        
        /*$query = "Exec declaracionIndicadoresNivel1 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel1 . "','" . $nivel2 . "','" . $nivel3 . "','" . $nivel4 . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $mercado . "','" . $etapa . "','" . $naviera . "','" . $agencia . "','" . $trafico . "','" . $puertoOrigen . "','" . $puertoEmbarque . "','" . $nave . "','" . $puertoDestino . "','" . 
                $puertoDescarga . "','" . $tipoNave . "','" . $tipoServicio . "','" . $tipoCarga . "','" . $tipoEmbalaje . "','" . $paisOrigen . "','" . $paisEmbarque . "','" . $agenteDoc . "','" . 
                $paisDestino . "','" . $paisDescarga . "','" . $statusContainer . "','" . $shipper . "','" . $consignee . "','" . $commodity . "','" . $granFamilia . "','" . $familia . "','" . 
                $subFamilia . "','" . $dryReefer . "','" .$contenedor . "','" . $usuarioId . "'";        
        echo $query;*/
		
					
			$stmt = mssql_init("declaracionesIndicadoresNivel1");
			
			mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
			mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);
			mssql_bind($stmt, '@var1', $nivel1, SQLVARCHAR);
			mssql_bind($stmt, '@var2', $nivel2, SQLVARCHAR);
			mssql_bind($stmt, '@var3', $nivel3, SQLVARCHAR);
			mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
			mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
			mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
			mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
			mssql_bind($stmt, '@mercado', $mercado, SQLVARCHAR);
			mssql_bind($stmt, '@etapa', $etapa, SQLVARCHAR);
			mssql_bind($stmt, '@naviera', $naviera, SQLVARCHAR);
			mssql_bind($stmt, '@agencia', $agencia, SQLVARCHAR);
			mssql_bind($stmt, '@trafico', $trafico, SQLVARCHAR);
			mssql_bind($stmt, '@puertoOrigen', $puertoOrigen, SQLVARCHAR);
			mssql_bind($stmt, '@puertoEmbarque', $puertoEmbarque, SQLVARCHAR);
			mssql_bind($stmt, '@nave', $nave, SQLVARCHAR);
			mssql_bind($stmt, '@puertoDestino', $puertoDestino, SQLVARCHAR);
			mssql_bind($stmt, '@puertoDescarga', $puertoDescarga, SQLVARCHAR);
			mssql_bind($stmt, '@tipoNave', $tipoNave, SQLVARCHAR);
			mssql_bind($stmt, '@tipoServicio', $tipoServicio, SQLVARCHAR);
			mssql_bind($stmt, '@tipoCarga', $tipoCarga, SQLVARCHAR);
			mssql_bind($stmt, '@tipoEmbalaje', $tipoEmbalaje, SQLVARCHAR);
			mssql_bind($stmt, '@paisOrigen', $paisOrigen, SQLVARCHAR);
			mssql_bind($stmt, '@paisEmbarque', $paisEmbarque, SQLVARCHAR);
			mssql_bind($stmt, '@agenteDoc', $agenteDoc, SQLVARCHAR);
			mssql_bind($stmt, '@paisDestino', $paisDestino, SQLVARCHAR);
			mssql_bind($stmt, '@paisDescarga', $paisDescarga, SQLVARCHAR);
			mssql_bind($stmt, '@statusContainer', $statusContainer, SQLVARCHAR);
			mssql_bind($stmt, '@shipper', $shipper, SQLVARCHAR);
			mssql_bind($stmt, '@consignee', $consignee, SQLVARCHAR);
			mssql_bind($stmt, '@commodity', $commodity, SQLVARCHAR);
			mssql_bind($stmt, '@granFamilia', $granFamilia, SQLVARCHAR);
			mssql_bind($stmt, '@familia', $familia, SQLVARCHAR);
			mssql_bind($stmt, '@subFamilia', $subFamilia, SQLVARCHAR);
			mssql_bind($stmt, '@dryReefer', $dryReefer, SQLVARCHAR);        
			mssql_bind($stmt, '@containerVacios', $contenedor, SQLVARCHAR);
			mssql_bind($stmt, '@emisor', $emisor, SQLVARCHAR);
			mssql_bind($stmt, '@almacen', $almacen, SQLVARCHAR);
			mssql_bind($stmt, '@mercadoFFW', $mercadoFFW, SQLVARCHAR);
			mssql_bind($stmt, '@clausula', $clausula, SQLVARCHAR);
			mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);
		

        

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
	
	
	function declaracionIndicadoresNivel1Resumen($fechaDesde, $fechaHasta, $nivel1, $nivel2, $nivel3, $nivel4, $nivel5, $tipoDato, $clienteId, $mercado, $etapa, $naviera, $agencia, $trafico, 
            $puertoOrigen, $puertoEmbarque, $nave, $puertoDestino, $puertoDescarga, $tipoNave, $tipoServicio, $tipoCarga, $tipoEmbalaje, $paisOrigen, $paisEmbarque,
            $agenteDoc, $paisDestino, $paisDescarga, $statusContainer, $shipper, $consignee, $commodity, $granFamilia, $familia, $subFamilia, $dryReefer, $contenedor, $emisor, $almacen, $mercadoFFW, $usuarioId){
    
        
        /*$query = "Exec declaracionIndicadoresNivel1 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel1 . "','" . $nivel2 . "','" . $nivel3 . "','" . $nivel4 . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $mercado . "','" . $etapa . "','" . $naviera . "','" . $agencia . "','" . $trafico . "','" . $puertoOrigen . "','" . $puertoEmbarque . "','" . $nave . "','" . $puertoDestino . "','" . 
                $puertoDescarga . "','" . $tipoNave . "','" . $tipoServicio . "','" . $tipoCarga . "','" . $tipoEmbalaje . "','" . $paisOrigen . "','" . $paisEmbarque . "','" . $agenteDoc . "','" . 
                $paisDestino . "','" . $paisDescarga . "','" . $statusContainer . "','" . $shipper . "','" . $consignee . "','" . $commodity . "','" . $granFamilia . "','" . $familia . "','" . 
                $subFamilia . "','" . $dryReefer . "','" .$contenedor . "','" . $usuarioId . "'";        
        echo $query;*/
        		      	
					  
        $stmt = mssql_init("declaracionesIndicadoresNivel1Resumen");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);
        mssql_bind($stmt, '@var1', $nivel1, SQLVARCHAR);
        mssql_bind($stmt, '@var2', $nivel2, SQLVARCHAR);
        mssql_bind($stmt, '@var3', $nivel3, SQLVARCHAR);
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@mercado', $mercado, SQLVARCHAR);
        mssql_bind($stmt, '@etapa', $etapa, SQLVARCHAR);
        mssql_bind($stmt, '@naviera', $naviera, SQLVARCHAR);
        mssql_bind($stmt, '@agencia', $agencia, SQLVARCHAR);
        mssql_bind($stmt, '@trafico', $trafico, SQLVARCHAR);
        mssql_bind($stmt, '@puertoOrigen', $puertoOrigen, SQLVARCHAR);
        mssql_bind($stmt, '@puertoEmbarque', $puertoEmbarque, SQLVARCHAR);
        mssql_bind($stmt, '@nave', $nave, SQLVARCHAR);
        mssql_bind($stmt, '@puertoDestino', $puertoDestino, SQLVARCHAR);
        mssql_bind($stmt, '@puertoDescarga', $puertoDescarga, SQLVARCHAR);
        mssql_bind($stmt, '@tipoNave', $tipoNave, SQLVARCHAR);
        mssql_bind($stmt, '@tipoServicio', $tipoServicio, SQLVARCHAR);
        mssql_bind($stmt, '@tipoCarga', $tipoCarga, SQLVARCHAR);
        mssql_bind($stmt, '@tipoEmbalaje', $tipoEmbalaje, SQLVARCHAR);
        mssql_bind($stmt, '@paisOrigen', $paisOrigen, SQLVARCHAR);
        mssql_bind($stmt, '@paisEmbarque', $paisEmbarque, SQLVARCHAR);
        mssql_bind($stmt, '@agenteDoc', $agenteDoc, SQLVARCHAR);
        mssql_bind($stmt, '@paisDestino', $paisDestino, SQLVARCHAR);
        mssql_bind($stmt, '@paisDescarga', $paisDescarga, SQLVARCHAR);
        mssql_bind($stmt, '@statusContainer', $statusContainer, SQLVARCHAR);
        mssql_bind($stmt, '@shipper', $shipper, SQLVARCHAR);
        mssql_bind($stmt, '@consignee', $consignee, SQLVARCHAR);
        mssql_bind($stmt, '@commodity', $commodity, SQLVARCHAR);
        mssql_bind($stmt, '@granFamilia', $granFamilia, SQLVARCHAR);
        mssql_bind($stmt, '@familia', $familia, SQLVARCHAR);
        mssql_bind($stmt, '@subFamilia', $subFamilia, SQLVARCHAR);
        mssql_bind($stmt, '@dryReefer', $dryReefer, SQLVARCHAR);        
        mssql_bind($stmt, '@containerVacios', $contenedor, SQLVARCHAR);
		mssql_bind($stmt, '@emisor', $emisor, SQLVARCHAR);
		mssql_bind($stmt, '@almacen', $almacen, SQLVARCHAR);
		mssql_bind($stmt, '@mercadoFFW', $mercadoFFW, SQLVARCHAR);
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
	
    
    
    function declaracionIndicadoresNivel1Grafico($fechaDesde, $fechaHasta, $nivel1, $usuarioId){
            
        /*$query = "Exec declaracionIndicadoresNivel1Grafico '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel1 . "','" . $usuarioId . "'";        
        echo $query;*/
               
        $stmt = mssql_init("declaracionesIndicadoresNivel1Grafico");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);
        mssql_bind($stmt, '@var1', $nivel1, SQLVARCHAR);                                            
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    
    function declaracionIndicadoresNivel1Contenedores($fechaDesde, $fechaHasta, $nivel1, $tipoDato, $usuarioId){
    
        
        /*$query = "Exec declaracionIndicadoresNivel1Contenedores '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel1 . "','" . $usuarioId . "','" . $tipoDato . "'";        
        echo $query;*/
                
        $stmt = mssql_init("declaracionesIndicadoresNivel1Contenedores");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);                
        mssql_bind($stmt, '@var1', $nivel1, SQLVARCHAR);        
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    function declaracionIndicadoresNivel2($fechaDesde, $fechaHasta, $nivel2, $nivel3, $nivel4, $nivel5, $tipoDato, $clienteId, $nivel1Id, $otros, $usuarioId){
    
        
        /*$query = "Exec declaracionIndicadoresNivel2 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel2 . "','" . $nivel3 . "','" . $nivel4 . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $nivel1Id . "','" . $otros . "','" . $usuarioId . "'";                
        echo $query;*/
                
        $stmt = mssql_init("declaracionesIndicadoresNivel2");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);        
        mssql_bind($stmt, '@var2', $nivel2, SQLVARCHAR);
        mssql_bind($stmt, '@var3', $nivel3, SQLVARCHAR);
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@nivel1Id', $nivel1Id, SQLVARCHAR);
        mssql_bind($stmt, '@otros', $otros, SQLVARCHAR);        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    function declaracionIndicadoresNivel3($fechaDesde, $fechaHasta, $nivel3, $nivel4, $nivel5, $tipoDato, $clienteId, $nivel1Id, $nivel2Id, $otros, $usuarioId){
    
        
        /*$query = "Exec declaracionIndicadoresNivel3 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel3 . "','" . $nivel4 . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $nivel1Id . "','" . $nivel2Id . "','" . $otros . "','" . $usuarioId . "'";        
        echo $query;*/
                
        $stmt = mssql_init("declaracionesIndicadoresNivel3");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);                
        mssql_bind($stmt, '@var3', $nivel3, SQLVARCHAR);
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@nivel1Id', $nivel1Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel2Id', $nivel2Id, SQLVARCHAR);
        mssql_bind($stmt, '@otros', $otros, SQLVARCHAR);        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    function declaracionIndicadoresNivel4($fechaDesde, $fechaHasta, $nivel4, $nivel5, $tipoDato, $clienteId, $nivel1Id, $nivel2Id, $nivel3Id, $otros, $usuarioId){
            
        /*$query = "Exec declaracionIndicadoresNivel4 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel4 . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $nivel1Id . "','" . $nivel2Id . "','" . $nivel3Id . "','" . $otros . "','" . $usuarioId . "'";      
        echo $query;*/
                
        $stmt = mssql_init("declaracionesIndicadoresNivel4");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);                        
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@nivel1Id', $nivel1Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel2Id', $nivel2Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel3Id', $nivel3Id, SQLVARCHAR);
        mssql_bind($stmt, '@otros', $otros, SQLVARCHAR);        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    function declaracionIndicadoresNivel5($fechaDesde, $fechaHasta, $nivel5, $tipoDato, $clienteId, $nivel1Id, $nivel2Id, $nivel3Id, $nivel4Id, $otros, $usuarioId){
            
        /*$query = "Exec declaracionIndicadoresNivel5 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $nivel1Id . "','" . $nivel2Id . "','" . $nivel3Id . "','" . $nivel4Id . "','" . $otros . "','" . $usuarioId . "'";        
        echo $query;*/
                
        $stmt = mssql_init("declaracionesIndicadoresNivel5");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);                                
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@nivel1Id', $nivel1Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel2Id', $nivel2Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel3Id', $nivel3Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel4Id', $nivel4Id, SQLVARCHAR);
        mssql_bind($stmt, '@otros', $otros, SQLVARCHAR);        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    
    //REPORTES
    
    function declaracionReporteNivel1($fechaDesde, $fechaHasta, $nivel1, $nivel2, $nivel3, $nivel4, $nivel5, $tipoDato, $clienteId, $mercado, $etapa, $naviera, $agencia, $trafico, 
            $puertoOrigen, $puertoEmbarque, $nave, $puertoDestino, $puertoDescarga, $tipoNave, $tipoServicio, $tipoCarga, $tipoEmbalaje, $paisOrigen, $paisEmbarque,
            $agenteDoc, $paisDestino, $paisDescarga, $statusContainer, $shipper, $consignee, $commodity, $granFamilia, $familia, $subFamilia, $dryReefer, $contenedor, $emisor, $almacen, $mercadoFFW, $clausula, $vertical, $subVertical, $categoria, $conVert, $sinVert, $usuarioId){
    
        
        /*$query = "Exec declaracionReporteNivel1 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel1 . "','" . $nivel2 . "','" . $nivel3 . "','" . $nivel4 . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $mercado . "','" . $etapa . "','" . $naviera . "','" . $agencia . "','" . $trafico . "','" . $puertoOrigen . "','" . $puertoEmbarque . "','" . $nave . "','" . $puertoDestino . "','" . 
                $puertoDescarga . "','" . $tipoNave . "','" . $tipoServicio . "','" . $tipoCarga . "','" . $tipoEmbalaje . "','" . $paisOrigen . "','" . $paisEmbarque . "','" . $agenteDoc . "','" . 
                $paisDestino . "','" . $paisDescarga . "','" . $statusContainer . "','" . $shipper . "','" . $consignee . "','" . $commodity . "','" . $granFamilia . "','" . $familia . "','" . 
                $subFamilia . "','" . $dryReefer . "','" .$contenedor . "','" .$emisor . "','" .$almacen . "','" .$mercadoFFW . "','" . $usuarioId . "'";        
        echo $query;*/
        
        		
			        $stmt = mssql_init("declaracionesReporteNivel1");

        mssql_bind($stmt, '@fechaDesdeA', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHastaA', $fechaHasta, SQLVARCHAR);
        mssql_bind($stmt, '@var1', $nivel1, SQLVARCHAR);
        mssql_bind($stmt, '@var2', $nivel2, SQLVARCHAR);
        mssql_bind($stmt, '@var3', $nivel3, SQLVARCHAR);
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@mercado', $mercado, SQLVARCHAR);
        mssql_bind($stmt, '@etapa', $etapa, SQLVARCHAR);
        mssql_bind($stmt, '@naviera', $naviera, SQLVARCHAR);
        mssql_bind($stmt, '@agencia', $agencia, SQLVARCHAR);
        mssql_bind($stmt, '@trafico', $trafico, SQLVARCHAR);
        mssql_bind($stmt, '@puertoOrigen', $puertoOrigen, SQLVARCHAR);
        mssql_bind($stmt, '@puertoEmbarque', $puertoEmbarque, SQLVARCHAR);
        mssql_bind($stmt, '@nave', $nave, SQLVARCHAR);
        mssql_bind($stmt, '@puertoDestino', $puertoDestino, SQLVARCHAR);
        mssql_bind($stmt, '@puertoDescarga', $puertoDescarga, SQLVARCHAR);
        mssql_bind($stmt, '@tipoNave', $tipoNave, SQLVARCHAR);
        mssql_bind($stmt, '@tipoServicio', $tipoServicio, SQLVARCHAR);
        mssql_bind($stmt, '@tipoCarga', $tipoCarga, SQLVARCHAR);
        mssql_bind($stmt, '@tipoEmbalaje', $tipoEmbalaje, SQLVARCHAR);
        mssql_bind($stmt, '@paisOrigen', $paisOrigen, SQLVARCHAR);
        mssql_bind($stmt, '@paisEmbarque', $paisEmbarque, SQLVARCHAR);
        mssql_bind($stmt, '@agenteDoc', $agenteDoc, SQLVARCHAR);
        mssql_bind($stmt, '@paisDestino', $paisDestino, SQLVARCHAR);
        mssql_bind($stmt, '@paisDescarga', $paisDescarga, SQLVARCHAR);
        mssql_bind($stmt, '@statusContainer', $statusContainer, SQLVARCHAR);
        mssql_bind($stmt, '@shipper', $shipper, SQLVARCHAR);
        mssql_bind($stmt, '@consignee', $consignee, SQLVARCHAR);
        mssql_bind($stmt, '@commodity', $commodity, SQLVARCHAR);
        mssql_bind($stmt, '@granFamilia', $granFamilia, SQLVARCHAR);
        mssql_bind($stmt, '@familia', $familia, SQLVARCHAR);
        mssql_bind($stmt, '@subFamilia', $subFamilia, SQLVARCHAR);
        mssql_bind($stmt, '@dryReefer', $dryReefer, SQLVARCHAR);        
        mssql_bind($stmt, '@containerVacios', $contenedor, SQLVARCHAR);         
		mssql_bind($stmt, '@emisor', $emisor, SQLVARCHAR);
		mssql_bind($stmt, '@almacen', $almacen, SQLVARCHAR);
		mssql_bind($stmt, '@mercadoFFW', $mercadoFFW, SQLVARCHAR);		  
		mssql_bind($stmt, '@clausula', $clausula, SQLVARCHAR);      	
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);
		


        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    
     function declaracionSoWReporteNivel1($fechaDesde, $fechaHasta, $nivel1, $nivel2, $nivel3, $nivel4, $nivel5, $tipoDato, $clienteId, $mercado, $etapa, $naviera, $agencia, $trafico, 
            $puertoOrigen, $puertoEmbarque, $nave, $puertoDestino, $puertoDescarga, $tipoNave, $tipoServicio, $tipoCarga, $tipoEmbalaje, $paisOrigen, $paisEmbarque,
            $agenteDoc, $paisDestino, $paisDescarga, $statusContainer, $shipper, $consignee, $commodity, $granFamilia, $familia, $subFamilia, $dryReefer, $contenedor, $emisor, $almacen,
            $mercadoFFW, $clausula, $usuarioId, $participacionTOP, $participacionBTM, $top){
    
        
        /*$query = "Exec declaracionReporteNivel1 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel1 . "','" . $nivel2 . "','" . $nivel3 . "','" . $nivel4 . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $mercado . "','" . $etapa . "','" . $naviera . "','" . $agencia . "','" . $trafico . "','" . $puertoOrigen . "','" . $puertoEmbarque . "','" . $nave . "','" . $puertoDestino . "','" . 
                $puertoDescarga . "','" . $tipoNave . "','" . $tipoServicio . "','" . $tipoCarga . "','" . $tipoEmbalaje . "','" . $paisOrigen . "','" . $paisEmbarque . "','" . $agenteDoc . "','" . 
                $paisDestino . "','" . $paisDescarga . "','" . $statusContainer . "','" . $shipper . "','" . $consignee . "','" . $commodity . "','" . $granFamilia . "','" . $familia . "','" . 
                $subFamilia . "','" . $dryReefer . "','" .$contenedor . "','" .$emisor . "','" .$almacen . "','" .$mercadoFFW . "','" . $usuarioId . "'";        
        echo $query;*/
         /*
        echo "declaracionesReporteNivel1ShareOfWallet '$fechaDesde', '$fechaHasta', '$nivel1', '$nivel2', '$nivel3', '$nivel4', '$nivel5', '$tipoDato', '$clienteId', '$mercado', '$etapa',
                '$naviera', '$agencia', '$trafico', '$puertoOrigen', '$puertoEmbarque', '$nave', '$puertoDestino', '$puertoDescarga', '$tipoNave', '$tipoServicio', '$tipoCarga', '$tipoEmbalaje',
                '$paisOrigen', '$paisEmbarque', '$agenteDoc', '$paisDestino', '$paisDescarga', '$statusContainer', '$shipper', '$consignee', '$commodity', '$granFamilia', '$familia', '$subFamilia',
                '$dryReefer',  '$contenedor', '$emisor', '$almacen', '$mercadoFFW', '$clausula', '$participacionTOP', '$participacionBTM', '$top', '$usuarioId'";
        */
        
        $stmt = mssql_init("declaracionesReporteNivel1ShareOfWallet");

        mssql_bind($stmt, '@fechaDesdeA', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHastaA', $fechaHasta, SQLVARCHAR);
        mssql_bind($stmt, '@var1', $nivel1, SQLVARCHAR);
        mssql_bind($stmt, '@var2', $nivel2, SQLVARCHAR);
        mssql_bind($stmt, '@var3', $nivel3, SQLVARCHAR);
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@mercado', $mercado, SQLVARCHAR);
        mssql_bind($stmt, '@etapa', $etapa, SQLVARCHAR);
        mssql_bind($stmt, '@naviera', $naviera, SQLVARCHAR);
        mssql_bind($stmt, '@agencia', $agencia, SQLVARCHAR);
        mssql_bind($stmt, '@trafico', $trafico, SQLVARCHAR);
        mssql_bind($stmt, '@puertoOrigen', $puertoOrigen, SQLVARCHAR);
        mssql_bind($stmt, '@puertoEmbarque', $puertoEmbarque, SQLVARCHAR);
        mssql_bind($stmt, '@nave', $nave, SQLVARCHAR);
        mssql_bind($stmt, '@puertoDestino', $puertoDestino, SQLVARCHAR);
        mssql_bind($stmt, '@puertoDescarga', $puertoDescarga, SQLVARCHAR);
        mssql_bind($stmt, '@tipoNave', $tipoNave, SQLVARCHAR);
        mssql_bind($stmt, '@tipoServicio', $tipoServicio, SQLVARCHAR);
        mssql_bind($stmt, '@tipoCarga', $tipoCarga, SQLVARCHAR);
        mssql_bind($stmt, '@tipoEmbalaje', $tipoEmbalaje, SQLVARCHAR);
        mssql_bind($stmt, '@paisOrigen', $paisOrigen, SQLVARCHAR);
        mssql_bind($stmt, '@paisEmbarque', $paisEmbarque, SQLVARCHAR);
        mssql_bind($stmt, '@agenteDoc', $agenteDoc, SQLVARCHAR);
        mssql_bind($stmt, '@paisDestino', $paisDestino, SQLVARCHAR);
        mssql_bind($stmt, '@paisDescarga', $paisDescarga, SQLVARCHAR);
        mssql_bind($stmt, '@statusContainer', $statusContainer, SQLVARCHAR);
        mssql_bind($stmt, '@shipper', $shipper, SQLVARCHAR);
        mssql_bind($stmt, '@consignee', $consignee, SQLVARCHAR);
        mssql_bind($stmt, '@commodity', $commodity, SQLVARCHAR);
        mssql_bind($stmt, '@granFamilia', $granFamilia, SQLVARCHAR);
        mssql_bind($stmt, '@familia', $familia, SQLVARCHAR);
        mssql_bind($stmt, '@subFamilia', $subFamilia, SQLVARCHAR);
        mssql_bind($stmt, '@dryReefer', $dryReefer, SQLVARCHAR);        
        mssql_bind($stmt, '@containerVacios', $contenedor, SQLVARCHAR);         
        mssql_bind($stmt, '@emisor', $emisor, SQLVARCHAR);
        mssql_bind($stmt, '@almacen', $almacen, SQLVARCHAR);
        mssql_bind($stmt, '@mercadoFFW', $mercadoFFW, SQLVARCHAR);		        	
        mssql_bind($stmt, '@clausula', $clausula, SQLVARCHAR);

        mssql_bind($stmt, '@participacionTOP', $participacionTOP, SQLVARCHAR);
        mssql_bind($stmt, '@participacionBTM', $participacionBTM, SQLVARCHAR);
        mssql_bind($stmt, '@top', $top, SQLVARCHAR);
                
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);
        
        
        $result = sql_db::sql_ejecutar_sp($stmt);
        
        //echo '[' . mssql_get_last_message() . ']';
        
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    
    
        
    function declaracionMAReporteNivel1($fechaDesde, $fechaHasta, $nivel1, $nivel2, $nivel3, $nivel4, $nivel5, $tipoDato, $clienteId, $mercado, $etapa, $naviera, $agencia, $trafico, 
            $puertoOrigen, $puertoEmbarque, $nave, $puertoDestino, $puertoDescarga, $tipoNave, $tipoServicio, $tipoCarga, $tipoEmbalaje, $paisOrigen, $paisEmbarque,
            $agenteDoc, $paisDestino, $paisDescarga, $statusContainer, $shipper, $consignee, $commodity, $granFamilia, $familia, $subFamilia, $dryReefer, $contenedor, $emisor, $almacen, $mercadoFFW, $clausula, $usuarioId,
            $tipoFiltro, $baremoTOP, $baremoBTM, $participacionMIN, $participacionMAX, $top){
    //tipoFiltro: tipoFiltro, baremoTOP: baremoTOP, baremoBTM: baremoBTM, participacion: participacion, top: top
        
        /*$query = "Exec declaracionReporteNivel1 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel1 . "','" . $nivel2 . "','" . $nivel3 . "','" . $nivel4 . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $mercado . "','" . $etapa . "','" . $naviera . "','" . $agencia . "','" . $trafico . "','" . $puertoOrigen . "','" . $puertoEmbarque . "','" . $nave . "','" . $puertoDestino . "','" . 
                $puertoDescarga . "','" . $tipoNave . "','" . $tipoServicio . "','" . $tipoCarga . "','" . $tipoEmbalaje . "','" . $paisOrigen . "','" . $paisEmbarque . "','" . $agenteDoc . "','" . 
                $paisDestino . "','" . $paisDescarga . "','" . $statusContainer . "','" . $shipper . "','" . $consignee . "','" . $commodity . "','" . $granFamilia . "','" . $familia . "','" . 
                $subFamilia . "','" . $dryReefer . "','" .$contenedor . "','" .$emisor . "','" .$almacen . "','" .$mercadoFFW . "','" . $usuarioId . "'";        
        echo $query;*/
        /*echo "declaracionesReporteNivel1MissingAccount '$fechaDesde', '$fechaHasta', '$nivel1', '$nivel2', '$nivel3', '$nivel4', '$nivel5', '$tipoDato', '$clienteId', '$mercado', '$etapa',
                '$naviera', '$agencia', '$trafico', '$puertoOrigen', '$puertoEmbarque', '$nave', '$puertoDestino', '$puertoDescarga', '$tipoNave', '$tipoServicio', '$tipoCarga', '$tipoEmbalaje',
                '$paisOrigen', '$paisEmbarque', '$agenteDoc', '$paisDestino', '$paisDescarga', '$statusContainer', '$shipper', '$consignee', '$commodity', '$granFamilia', '$familia', '$subFamilia',
                '$dryReefer',  '$contenedor', '$emisor', '$almacen', '$mercadoFFW', '$usuarioId','$tipoFiltro', '$baremoTOP', '$baremoBTM', '$participacionMIN', '$participacionMAX', '$top'";
        */
        
        $stmt = mssql_init("declaracionesReporteNivel1MissingAccount");

        mssql_bind($stmt, '@fechaDesdeA', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHastaA', $fechaHasta, SQLVARCHAR);
        mssql_bind($stmt, '@var1', $nivel1, SQLVARCHAR);
        mssql_bind($stmt, '@var2', $nivel2, SQLVARCHAR);
        mssql_bind($stmt, '@var3', $nivel3, SQLVARCHAR);
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@mercado', $mercado, SQLVARCHAR);
        mssql_bind($stmt, '@etapa', $etapa, SQLVARCHAR);
        mssql_bind($stmt, '@naviera', $naviera, SQLVARCHAR);
        mssql_bind($stmt, '@agencia', $agencia, SQLVARCHAR);
        mssql_bind($stmt, '@trafico', $trafico, SQLVARCHAR);
        mssql_bind($stmt, '@puertoOrigen', $puertoOrigen, SQLVARCHAR);
        mssql_bind($stmt, '@puertoEmbarque', $puertoEmbarque, SQLVARCHAR);
        mssql_bind($stmt, '@nave', $nave, SQLVARCHAR);
        mssql_bind($stmt, '@puertoDestino', $puertoDestino, SQLVARCHAR);
        mssql_bind($stmt, '@puertoDescarga', $puertoDescarga, SQLVARCHAR);
        mssql_bind($stmt, '@tipoNave', $tipoNave, SQLVARCHAR);
        mssql_bind($stmt, '@tipoServicio', $tipoServicio, SQLVARCHAR);
        mssql_bind($stmt, '@tipoCarga', $tipoCarga, SQLVARCHAR);
        mssql_bind($stmt, '@tipoEmbalaje', $tipoEmbalaje, SQLVARCHAR);
        mssql_bind($stmt, '@paisOrigen', $paisOrigen, SQLVARCHAR);
        mssql_bind($stmt, '@paisEmbarque', $paisEmbarque, SQLVARCHAR);
        mssql_bind($stmt, '@agenteDoc', $agenteDoc, SQLVARCHAR);
        mssql_bind($stmt, '@paisDestino', $paisDestino, SQLVARCHAR);
        mssql_bind($stmt, '@paisDescarga', $paisDescarga, SQLVARCHAR);
        mssql_bind($stmt, '@statusContainer', $statusContainer, SQLVARCHAR);
        mssql_bind($stmt, '@shipper', $shipper, SQLVARCHAR);
        mssql_bind($stmt, '@consignee', $consignee, SQLVARCHAR);
        mssql_bind($stmt, '@commodity', $commodity, SQLVARCHAR);
        mssql_bind($stmt, '@granFamilia', $granFamilia, SQLVARCHAR);
        mssql_bind($stmt, '@familia', $familia, SQLVARCHAR);
        mssql_bind($stmt, '@subFamilia', $subFamilia, SQLVARCHAR);
        mssql_bind($stmt, '@dryReefer', $dryReefer, SQLVARCHAR);        
        mssql_bind($stmt, '@containerVacios', $contenedor, SQLVARCHAR);         
		mssql_bind($stmt, '@emisor', $emisor, SQLVARCHAR);
		mssql_bind($stmt, '@almacen', $almacen, SQLVARCHAR);
		mssql_bind($stmt, '@mercadoFFW', $mercadoFFW, SQLVARCHAR);	
		mssql_bind($stmt, '@clausula', $clausula, SQLVARCHAR);
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);
        
        mssql_bind($stmt, '@tipoIndicador', $tipoFiltro, SQLCHAR);		        	
        mssql_bind($stmt, '@baremoBTM', $baremoBTM, SQLVARCHAR);		        	
        mssql_bind($stmt, '@baremoTOP', $baremoTOP, SQLVARCHAR);		        	
        mssql_bind($stmt, '@participacionMIN', $participacionMIN, SQLVARCHAR);		        	
        mssql_bind($stmt, '@participacionMAX', $participacionMAX, SQLVARCHAR);		        	
        mssql_bind($stmt, '@topval', $top, SQLVARCHAR);		        	
        
        /*
        DECLARE @runQuery as NVARCHAR(MAX)
			DECLARE @tipoIndicador as char(1)
			DECLARE @baremoBTM as VARCHAR(MAX)
			DECLARE @baremoTOP as VARCHAR(MAX)
			DECLARE @participacion as VARCHAR(MAX)
			DECLARE @topval as VARCHAR(MAX)
        */
        $result = sql_db::sql_ejecutar_sp($stmt);
        
        //echo '[' . mssql_get_last_message() . ']';
        
        sql_db::sql_close();
        return $result;         
    
    }
    
   
    
    
    
    
    
    
    function declaracionReporteNivel1Contenedores($fechaDesde, $fechaHasta, $nivel1, $tipoDato, $usuarioId){
    
        
        /*$query = "Exec declaracionesReporteNivel1Contenedores '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel1 . "','" . $usuarioId . "','" . $tipoDato . "'";        
        echo $query;*/
                
        $stmt = mssql_init("declaracionesReporteNivel1Contenedores");

        mssql_bind($stmt, '@fechaDesdeA', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHastaA', $fechaHasta, SQLVARCHAR);                
        mssql_bind($stmt, '@var1', $nivel1, SQLVARCHAR);        
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    function declaracionReporteNivel2($fechaDesde, $fechaHasta, $nivel2, $nivel3, $nivel4, $nivel5, $tipoDato, $clienteId, $nivel1Id, $otros, $usuarioId){
    
        
        $query = "Exec declaracionReporteNivel2 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel2 . "','" . $nivel3 . "','" . $nivel4 . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $nivel1Id . "','" . $otros . "','" . $usuarioId . "'";        
        echo $query;
                
        $stmt = mssql_init("declaracionesReporteNivel2");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);        
        mssql_bind($stmt, '@var2', $nivel2, SQLVARCHAR);
        mssql_bind($stmt, '@var3', $nivel3, SQLVARCHAR);
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@nivel1Id', $nivel1Id, SQLVARCHAR);
        mssql_bind($stmt, '@otros', $otros, SQLVARCHAR);        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    function declaracionReporteNivel3($fechaDesde, $fechaHasta, $nivel3, $nivel4, $nivel5, $tipoDato, $clienteId, $nivel1Id, $nivel2Id, $otros, $usuarioId){
    
        
        /*$query = "Exec declaracionReporteNivel3 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel3 . "','" . $nivel4 . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $nivel1Id . "','" . $nivel2Id . "','" . $otros . "','" . $usuarioId . "'";        
        echo $query;*/
                
        $stmt = mssql_init("declaracionesReporteNivel3");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);                
        mssql_bind($stmt, '@var3', $nivel3, SQLVARCHAR);
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@nivel1Id', $nivel1Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel2Id', $nivel2Id, SQLVARCHAR);
        mssql_bind($stmt, '@otros', $otros, SQLVARCHAR);        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    function declaracionReporteNivel4($fechaDesde, $fechaHasta, $nivel4, $nivel5, $tipoDato, $clienteId, $nivel1Id, $nivel2Id, $nivel3Id, $otros, $usuarioId){
            
        /*$query = "Exec declaracionReporteNivel4 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel4 . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $nivel1Id . "','" . $nivel2Id . "','" . $nivel3Id . "','" . $otros . "','" . $usuarioId . "'";        
        echo $query;*/
                
        $stmt = mssql_init("declaracionesReporteNivel4");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);                        
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@nivel1Id', $nivel1Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel2Id', $nivel2Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel3Id', $nivel3Id, SQLVARCHAR);
        mssql_bind($stmt, '@otros', $otros, SQLVARCHAR);        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    function declaracionReporteNivel5($fechaDesde, $fechaHasta, $nivel5, $tipoDato, $clienteId, $nivel1Id, $nivel2Id, $nivel3Id, $nivel4Id, $otros, $usuarioId){
            
        /*$query = "Exec declaracionReporteNivel5 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $nivel1Id . "','" . $nivel2Id . "','" . $nivel3Id . "','" . $nivel4Id . "','" . $otros . "','" . $usuarioId . "'";        
        echo $query;*/
                
        $stmt = mssql_init("declaracionesReporteNivel5");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);                                
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@nivel1Id', $nivel1Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel2Id', $nivel2Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel3Id', $nivel3Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel4Id', $nivel4Id, SQLVARCHAR);
        mssql_bind($stmt, '@otros', $otros, SQLVARCHAR);        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    //EXTRACCIONES
    
    function declaracionExtracciones($fechaDesde, $fechaHasta, $tipoDato, $clienteId, $mercado, $etapa, 
            $naviera, $agencia, $trafico, $puertoOrigen, $puertoEmbarque, $nave, $puertoDestino, $puertoDescarga, $tipoNave, $tipoServicio, $tipoCarga, $tipoEmbalaje, $paisOrigen, 
            $paisEmbarque, $agenteDoc, $paisDestino, $paisDescarga, $statusContainer, $shipper, $consignee, $commodity, $granFamilia, $familia, $subFamilia, $dryReefer, $contenedor, $emisor, $almacen, $mercadoFFW, $clausula,
            $chknaviera, $chkagencia, $chktrafico, $chkpuertoOrigen, $chkpuertoEmbarque, $chknave, $chkpuertoDestino, $chkpuertoDescarga, $chktipoNave, $chktipoServicio, $chktipoCarga, $chktipoEmbalaje, $chkpaisOrigen, 
            $chkpaisEmbarque, $chkagenteDoc, $chkpaisDestino, $chkpaisDescarga, $chkstatusContainer, $chkshipper, $chkconsignee, $chkcommodity, $chkgranFamilia, $chkfamilia, $chksubFamilia, $chkdryReefer, $chkemisor, $chkalmacen, $chkclausula, $carpeta, $nombreArchivo,
            $usuarioId,$vertical , $subVertical , $categoria , $chkvertical, $chksubvertical, $chkcategoria){
    
        
        /*$query = "Exec declaracionesExtracciones '" . $fechaDesde . "','" . $fechaHasta . "','" . $tipoDato . "','" . $mercado . "','" . $etapa . "','" . 
                $naviera . "','" . $agencia . "','" . $trafico . "','" . $puertoOrigen . "','" . $puertoEmbarque . "','" . $nave . "','" . $puertoDestino . "','" . 
                $puertoDescarga . "','" . $tipoNave . "','" . $tipoServicio . "','" . $tipoCarga . "','" . $tipoEmbalaje . "','" . $paisOrigen . "','" . $paisEmbarque . "','" . $agenteDoc . "','" . 
                $paisDestino . "','" . $paisDescarga . "','" . $statusContainer . "','" . $shipper . "','" . $consignee . "','" . $commodity . "','" . $granFamilia . "','" . $familia . "','" . 
                $subFamilia . "','" . $dryReefer . "','" .$contenedor . "','" .$emisor . "','" .$almacen . "','" .$mercadoFFW . "','" . 
                
                $chknaviera . "','" . $chkagencia . "','" . $chktrafico . "','" . $chkpuertoOrigen . "','" . $chkpuertoEmbarque . "','" . $chknave . "','" . $chkpuertoDestino . "','" . 
                $chkpuertoDescarga . "','" . $chktipoNave . "','" . $chktipoServicio . "','" . $chktipoCarga . "','" . $chktipoEmbalaje . "','" . $chkpaisOrigen . "','" . $chkpaisEmbarque . "','" . $chkagenteDoc . "','" . 
                $chkpaisDestino . "','" . $chkpaisDescarga . "','" . $chkstatusContainer . "','" . $chkshipper . "','" . $chkconsignee . "','" . $chkcommodity . "','" . $chkgranFamilia . "','" . $chkfamilia . "','" . 
                $chksubFamilia . "','" . $chkdryReefer . "','" . $chkemisor . "','" . $chkalmacen . "','" .$chkcontenedor . "','" . $nombreArchivo . "','" . $usuarioId . "'";        
        echo $query;*/
        
		/*f($usuarioId == "103"){
			$stmt = mssql_init("declaracionesExtracciones3");
		}
        else{
			$stmt = mssql_init("declaracionesExtracciones");
		}*/
		
		if($usuarioId == 61){
			$stmt = mssql_init("declaracionesExtracciones_VERTICALES");
			
		 mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);        
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        //mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@mercado', $mercado, SQLVARCHAR);
        mssql_bind($stmt, '@etapa', $etapa, SQLVARCHAR);
        mssql_bind($stmt, '@naviera', $naviera, SQLVARCHAR);
        mssql_bind($stmt, '@agencia', $agencia, SQLVARCHAR);
        mssql_bind($stmt, '@trafico', $trafico, SQLVARCHAR);
        mssql_bind($stmt, '@puertoOrigen', $puertoOrigen, SQLVARCHAR);
        mssql_bind($stmt, '@puertoEmbarque', $puertoEmbarque, SQLVARCHAR);
        mssql_bind($stmt, '@nave', $nave, SQLVARCHAR);
        mssql_bind($stmt, '@puertoDestino', $puertoDestino, SQLVARCHAR);
        mssql_bind($stmt, '@puertoDescarga', $puertoDescarga, SQLVARCHAR);
        mssql_bind($stmt, '@tipoNave', $tipoNave, SQLVARCHAR);
        mssql_bind($stmt, '@tipoServicio', $tipoServicio, SQLVARCHAR);
        mssql_bind($stmt, '@tipoCarga', $tipoCarga, SQLVARCHAR);
        mssql_bind($stmt, '@tipoEmbalaje', $tipoEmbalaje, SQLVARCHAR);
        mssql_bind($stmt, '@paisOrigen', $paisOrigen, SQLVARCHAR);
        mssql_bind($stmt, '@paisEmbarque', $paisEmbarque, SQLVARCHAR);
        mssql_bind($stmt, '@agenteDoc', $agenteDoc, SQLVARCHAR);
        mssql_bind($stmt, '@paisDestino', $paisDestino, SQLVARCHAR);
        mssql_bind($stmt, '@paisDescarga', $paisDescarga, SQLVARCHAR);
        mssql_bind($stmt, '@statusContainer', $statusContainer, SQLVARCHAR);
        mssql_bind($stmt, '@shipper', $shipper, SQLVARCHAR);
        mssql_bind($stmt, '@consignee', $consignee, SQLVARCHAR);
        mssql_bind($stmt, '@commodity', $commodity, SQLVARCHAR);
        mssql_bind($stmt, '@granFamilia', $granFamilia, SQLVARCHAR);
        mssql_bind($stmt, '@familia', $familia, SQLVARCHAR);
        mssql_bind($stmt, '@subFamilia', $subFamilia, SQLVARCHAR);
        mssql_bind($stmt, '@dryReefer', $dryReefer, SQLVARCHAR);        
        mssql_bind($stmt, '@containerVacios', $contenedor, SQLVARCHAR);
		mssql_bind($stmt, '@emisor', $emisor, SQLVARCHAR);
		mssql_bind($stmt, '@almacen', $almacen, SQLVARCHAR);
		mssql_bind($stmt, '@mercadoFFW', $mercadoFFW, SQLVARCHAR);
        mssql_bind($stmt, '@clausula', $clausula, SQLVARCHAR);
		
		mssql_bind($stmt, '@chknaviera', $chknaviera, SQLVARCHAR);
        mssql_bind($stmt, '@chkagencia', $chkagencia, SQLVARCHAR);
        mssql_bind($stmt, '@chktrafico', $chktrafico, SQLVARCHAR);
        mssql_bind($stmt, '@chkpuertoOrigen', $chkpuertoOrigen, SQLVARCHAR);
        mssql_bind($stmt, '@chkpuertoEmbarque', $chkpuertoEmbarque, SQLVARCHAR);
        mssql_bind($stmt, '@chknave', $chknave, SQLVARCHAR);
        mssql_bind($stmt, '@chkpuertoDestino', $chkpuertoDestino, SQLVARCHAR);
        mssql_bind($stmt, '@chkpuertoDescarga', $chkpuertoDescarga, SQLVARCHAR);
        mssql_bind($stmt, '@chktipoNave', $chktipoNave, SQLVARCHAR);
        mssql_bind($stmt, '@chktipoServicio', $chktipoServicio, SQLVARCHAR);
        mssql_bind($stmt, '@chktipoCarga', $chktipoCarga, SQLVARCHAR);
        mssql_bind($stmt, '@chktipoEmbalaje', $chktipoEmbalaje, SQLVARCHAR);
        mssql_bind($stmt, '@chkpaisOrigen', $chkpaisOrigen, SQLVARCHAR);
        mssql_bind($stmt, '@chkpaisEmbarque', $chkpaisEmbarque, SQLVARCHAR);
        mssql_bind($stmt, '@chkagenteDoc', $chkagenteDoc, SQLVARCHAR);
        mssql_bind($stmt, '@chkpaisDestino', $chkpaisDestino, SQLVARCHAR);
        mssql_bind($stmt, '@chkpaisDescarga', $chkpaisDescarga, SQLVARCHAR);
        mssql_bind($stmt, '@chkstatusContainer', $chkstatusContainer, SQLVARCHAR);
        mssql_bind($stmt, '@chkshipper', $chkshipper, SQLVARCHAR);
        mssql_bind($stmt, '@chkconsignee', $chkconsignee, SQLVARCHAR);
        mssql_bind($stmt, '@chkcommodity', $chkcommodity, SQLVARCHAR);
        mssql_bind($stmt, '@chkgranFamilia', $chkgranFamilia, SQLVARCHAR);
        mssql_bind($stmt, '@chkfamilia', $chkfamilia, SQLVARCHAR);
        mssql_bind($stmt, '@chksubFamilia', $chksubFamilia, SQLVARCHAR);
        mssql_bind($stmt, '@chkdryReefer', $chkdryReefer, SQLVARCHAR);        
        mssql_bind($stmt, '@chkemisor', $chkemisor, SQLVARCHAR);
        mssql_bind($stmt, '@chkalmacen', $chkalmacen, SQLVARCHAR);        		
		mssql_bind($stmt, '@chkclausula', $chkclausula, SQLVARCHAR);
		
		mssql_bind($stmt, '@vertical', $vertical, SQLVARCHAR);
        mssql_bind($stmt, '@subVertical', $subVertical, SQLVARCHAR);
        mssql_bind($stmt, '@categoria', $categoria, SQLVARCHAR);        
        mssql_bind($stmt, '@chkvertical', $chkvertical, SQLVARCHAR);
        mssql_bind($stmt, '@chksubvertical', $chksubvertical, SQLVARCHAR);        		
		mssql_bind($stmt, '@chkcategoria', $chkcategoria, SQLVARCHAR);
		
		
		
		mssql_bind($stmt, '@carpeta', $carpeta, SQLVARCHAR);
        mssql_bind($stmt, '@nombreArchivo', $nombreArchivo, SQLVARCHAR);
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);
		


		}else{
				$stmt = mssql_init("declaracionesExtracciones");
        

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);        
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        //mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@mercado', $mercado, SQLVARCHAR);
        mssql_bind($stmt, '@etapa', $etapa, SQLVARCHAR);
        mssql_bind($stmt, '@naviera', $naviera, SQLVARCHAR);
        mssql_bind($stmt, '@agencia', $agencia, SQLVARCHAR);
        mssql_bind($stmt, '@trafico', $trafico, SQLVARCHAR);
        mssql_bind($stmt, '@puertoOrigen', $puertoOrigen, SQLVARCHAR);
        mssql_bind($stmt, '@puertoEmbarque', $puertoEmbarque, SQLVARCHAR);
        mssql_bind($stmt, '@nave', $nave, SQLVARCHAR);
        mssql_bind($stmt, '@puertoDestino', $puertoDestino, SQLVARCHAR);
        mssql_bind($stmt, '@puertoDescarga', $puertoDescarga, SQLVARCHAR);
        mssql_bind($stmt, '@tipoNave', $tipoNave, SQLVARCHAR);
        mssql_bind($stmt, '@tipoServicio', $tipoServicio, SQLVARCHAR);
        mssql_bind($stmt, '@tipoCarga', $tipoCarga, SQLVARCHAR);
        mssql_bind($stmt, '@tipoEmbalaje', $tipoEmbalaje, SQLVARCHAR);
        mssql_bind($stmt, '@paisOrigen', $paisOrigen, SQLVARCHAR);
        mssql_bind($stmt, '@paisEmbarque', $paisEmbarque, SQLVARCHAR);
        mssql_bind($stmt, '@agenteDoc', $agenteDoc, SQLVARCHAR);
        mssql_bind($stmt, '@paisDestino', $paisDestino, SQLVARCHAR);
        mssql_bind($stmt, '@paisDescarga', $paisDescarga, SQLVARCHAR);
        mssql_bind($stmt, '@statusContainer', $statusContainer, SQLVARCHAR);
        mssql_bind($stmt, '@shipper', $shipper, SQLVARCHAR);
        mssql_bind($stmt, '@consignee', $consignee, SQLVARCHAR);
        mssql_bind($stmt, '@commodity', $commodity, SQLVARCHAR);
        mssql_bind($stmt, '@granFamilia', $granFamilia, SQLVARCHAR);
        mssql_bind($stmt, '@familia', $familia, SQLVARCHAR);
        mssql_bind($stmt, '@subFamilia', $subFamilia, SQLVARCHAR);
        mssql_bind($stmt, '@dryReefer', $dryReefer, SQLVARCHAR);        
        mssql_bind($stmt, '@containerVacios', $contenedor, SQLVARCHAR);
		mssql_bind($stmt, '@emisor', $emisor, SQLVARCHAR);
		mssql_bind($stmt, '@almacen', $almacen, SQLVARCHAR);
		mssql_bind($stmt, '@mercadoFFW', $mercadoFFW, SQLVARCHAR);
        mssql_bind($stmt, '@clausula', $clausula, SQLVARCHAR);
		
		mssql_bind($stmt, '@chknaviera', $chknaviera, SQLVARCHAR);
        mssql_bind($stmt, '@chkagencia', $chkagencia, SQLVARCHAR);
        mssql_bind($stmt, '@chktrafico', $chktrafico, SQLVARCHAR);
        mssql_bind($stmt, '@chkpuertoOrigen', $chkpuertoOrigen, SQLVARCHAR);
        mssql_bind($stmt, '@chkpuertoEmbarque', $chkpuertoEmbarque, SQLVARCHAR);
        mssql_bind($stmt, '@chknave', $chknave, SQLVARCHAR);
        mssql_bind($stmt, '@chkpuertoDestino', $chkpuertoDestino, SQLVARCHAR);
        mssql_bind($stmt, '@chkpuertoDescarga', $chkpuertoDescarga, SQLVARCHAR);
        mssql_bind($stmt, '@chktipoNave', $chktipoNave, SQLVARCHAR);
        mssql_bind($stmt, '@chktipoServicio', $chktipoServicio, SQLVARCHAR);
        mssql_bind($stmt, '@chktipoCarga', $chktipoCarga, SQLVARCHAR);
        mssql_bind($stmt, '@chktipoEmbalaje', $chktipoEmbalaje, SQLVARCHAR);
        mssql_bind($stmt, '@chkpaisOrigen', $chkpaisOrigen, SQLVARCHAR);
        mssql_bind($stmt, '@chkpaisEmbarque', $chkpaisEmbarque, SQLVARCHAR);
        mssql_bind($stmt, '@chkagenteDoc', $chkagenteDoc, SQLVARCHAR);
        mssql_bind($stmt, '@chkpaisDestino', $chkpaisDestino, SQLVARCHAR);
        mssql_bind($stmt, '@chkpaisDescarga', $chkpaisDescarga, SQLVARCHAR);
        mssql_bind($stmt, '@chkstatusContainer', $chkstatusContainer, SQLVARCHAR);
        mssql_bind($stmt, '@chkshipper', $chkshipper, SQLVARCHAR);
        mssql_bind($stmt, '@chkconsignee', $chkconsignee, SQLVARCHAR);
        mssql_bind($stmt, '@chkcommodity', $chkcommodity, SQLVARCHAR);
        mssql_bind($stmt, '@chkgranFamilia', $chkgranFamilia, SQLVARCHAR);
        mssql_bind($stmt, '@chkfamilia', $chkfamilia, SQLVARCHAR);
        mssql_bind($stmt, '@chksubFamilia', $chksubFamilia, SQLVARCHAR);
        mssql_bind($stmt, '@chkdryReefer', $chkdryReefer, SQLVARCHAR);        
        mssql_bind($stmt, '@chkemisor', $chkemisor, SQLVARCHAR);
        mssql_bind($stmt, '@chkalmacen', $chkalmacen, SQLVARCHAR);        		
		mssql_bind($stmt, '@chkclausula', $chkclausula, SQLVARCHAR);
		
		mssql_bind($stmt, '@carpeta', $carpeta, SQLVARCHAR);
        mssql_bind($stmt, '@nombreArchivo', $nombreArchivo, SQLVARCHAR);
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);
			
		}
		
	

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    function declaracionExtraccionesArchivos($usuarioId, $moduloId) {
        $query = "Select * From Extraccion_Reportes (nolock) Where usuarioId = '" . $usuarioId . "' and modulo = '" . $moduloId . "' Order by fecha_reporte desc";
        //echo $query;
        $result = sql_db::sql_query($query);
        sql_db::sql_close();        
        return $result;
    }
    
    function declaracionExtraccionesSeleccionarArchivo($usuarioId, $moduloId, $arhivoId) {
        $query = "Select nombre_archivo From Extraccion_Reportes (nolock) Where usuarioId = '" . $usuarioId . "' and modulo = '" . $moduloId . "' and id_archivo = '" . $arhivoId . "'";                
        $result = sql_db::sql_query($query);
        $row = sql_db::sql_fetch_assoc($result);
        sql_db::sql_close();        
        return $row;
    }
    
    function declaracionExtraccionesEliminarArchivo($usuarioId, $moduloId, $arhivoId) {        
        $query = "Delete e From Extraccion_Reportes e Where usuarioId = '" . $usuarioId . "' and modulo = '" . $moduloId . "' and id_archivo = '" . $arhivoId . "'";        
        sql_db::sql_query($query);        
        sql_db::sql_close();        
        return true;
    }
	
	function declaracionesIndicadoresNivel1Preview($usuarioId) {
        $query = "";                    
		$query = "	Delete d
					From DeclaracionesIndicadores d
					Where usuarioId = " . $usuarioId . "
					
					Insert into DeclaracionesIndicadores
					Select " . $usuarioId . ", consultaId, nivel1, nivel2, nivel3, nivel4, nivel5, fecha, total, totalC20, totalC40
					From DeclaracionesIndicadoresPreview (nolock)
					
					Select * From DeclaracionesMaritimoPreview (nolock)";        
		
		$result = sql_db::sql_query($query);
        sql_db::sql_close(); 
					
		return $result;
    }
	
	function declaracionesIndicadoresNivel1PreviewGrafico() {
        $query = "Select * From DeclaracionesMaritimoPreviewGrafico (nolock)";        
        $result = sql_db::sql_query($query);
        sql_db::sql_close();        
        return $result;
    }
	
	function declaracionesIndicadoresNivel1Preview2($usuarioId) {
        $query = "";                    
		$query = "	Delete d
					From DeclaracionesIndicadores d
					Where usuarioId = " . $usuarioId . "
					
					Insert into DeclaracionesIndicadores
					Select " . $usuarioId . ", consultaId, nivel1, nivel2, nivel3, nivel4, nivel5, fecha, total, totalC20, totalC40
					From DeclaracionesIndicadoresPreview2 (nolock)

					Select * From DeclaracionesMaritimoPreview2 (nolock)";
		
		$result = sql_db::sql_query($query);
        sql_db::sql_close(); 
					
		return $result;
    }
	
	function declaracionesIndicadoresNivel1PreviewGrafico2() {
        $query = "Select * From DeclaracionesMaritimoPreviewGrafico2 (nolock)";        
        $result = sql_db::sql_query($query);
        sql_db::sql_close();        
        return $result;
    }
	
	
	function declaracionesIndicadoresNivel1PreviewDelfin($usuarioId) {
        $query = "";                    
		$query = "	Delete d
					From DeclaracionesIndicadores d
					Where usuarioId = " . $usuarioId . "
					
					Insert into DeclaracionesIndicadores
					Select " . $usuarioId . ", consultaId, nivel1, nivel2, nivel3, nivel4, nivel5, fecha, total, totalC20, totalC40
					From DeclaracionesIndicadoresPreviewDelfin (nolock)
					
					Select * From DeclaracionesMaritimoPreviewDelfin (nolock)";        
		
		$result = sql_db::sql_query($query);
        sql_db::sql_close(); 
					
		return $result;
    }
	
	function declaracionesIndicadoresNivel1PreviewGraficoDelfin() {
        $query = "Select * From DeclaracionesMaritimoPreviewGraficoDelfin (nolock)";        
        $result = sql_db::sql_query($query);
        sql_db::sql_close();        
        return $result;
    }
	
	
	function declaracionesIndicadoresNivel1PreviewExtraPort($usuarioId) {
        $query = "";                    
		$query = "	Delete d
					From DeclaracionesIndicadores d
					Where usuarioId = " . $usuarioId . "
					
					Insert into DeclaracionesIndicadores
					Select " . $usuarioId . ", consultaId, nivel1, nivel2, nivel3, nivel4, nivel5, fecha, total, totalC20, totalC40
					From DeclaracionesIndicadoresPreviewExtraPort (nolock)
					
					Select * From DeclaracionesMaritimoPreviewExtraPort (nolock)";        
		
		$result = sql_db::sql_query($query);
        sql_db::sql_close(); 
					
		return $result;
    }
	
	function declaracionesIndicadoresNivel1PreviewGraficoExtraPort() {
        $query = "Select * From DeclaracionesMaritimoPreviewGraficoExtraPort (nolock)";        
        $result = sql_db::sql_query($query);
        sql_db::sql_close();        
        return $result;
    }
    
    
}

?>


