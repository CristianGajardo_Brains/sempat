<?php
session_start();

setlocale(LC_TIME, "spanish");
$fechaActual = htmlentities(strftime("%A %d de %B del %Y"));
$fechaActual = ucfirst($fechaActual);

$usuarioLog = $_SESSION["SEMPAT_usuarioNombre"];
$usuarioId = $_SESSION["SEMPAT_usuarioId"];
$clienteId = $_SESSION["SEMPAT_clienteId"];

include ("../../librerias/conexion.php");
require('declaraciones.class.php');
include ('../../librerias/PHPExcel/Classes/PHPExcel.php');

$objDeclaracion = new declaracion();

$mesesTitulo = array('','ENERO','FEBRERO','MARZO','ABRIL','MAYO','JUNIO','JULIO','AGOSTO','SEPTIEMBRE','OCTUBRE','NOVIEMBRE','DICIEMBRE');
$letras = array("","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");

$mesesCabezera = array('','Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic');
$mesesDato = array('mes1','mes2','mes3','mes4','mes5','mes6','mes7','mes8','mes9','mes10','mes11','mes12');


$nivel1 =  $_POST['nivel1'];
$nivel2 =  $_POST['nivel2']; 
$nivel3 = $_POST['nivel3'];
$nivel4 =  $_POST['nivel4'];
$nivel5 = $_POST['nivel5'];
$nivelTituloN1 = $_POST['nivelTituloN1'];
$nivelTituloN2 = $_POST['nivelTituloN2'];
$nivelTituloN3 = $_POST['nivelTituloN3'];
$nivelTituloN4 = $_POST['nivelTituloN4'];
$nivelTituloN5 = $_POST['nivelTituloN5'];
$mercado =  $_POST['mercado'];
$etapa = $_POST['etapa'];
$fechaDesde = $_POST['fechaDesde'];
$fechaHasta = $_POST['fechaHasta'];
$tipoDato = $_POST['tipoDato'];

$mesDesde = "";
$mesHasta = "";

$añoDesde = "";
$añoHasta = "";
//01-12-2016 date("Y");
//30-11-2017

//ECHO $fechaDesde."<BR>";
//ECHO $fechaHasta."<BR>";

$añoDesde = substr($fechaDesde ,6,4);
$añoHasta = substr($fechaHasta ,6,4);

//echo $añoDesde;
//echo $añoHasta;


$mesDesde = substr($fechaDesde ,3,2);
$mesHasta = substr($fechaHasta ,3,2);

//echo $mesDesde;
//echo $mesHasta;

$mesesTitulo[(int)$mesDesde];
$mesesTitulo[(int)$mesHasta];

$nuevoMesDeste = (int)$mesDesde;
$nuevoMesHasta = (int)$mesHasta;


//ECHO $mesesTitulo[date("m", $fechaDesde)]."<BR>";
//ECHO $mesesTitulo[date("m", $fechaHasta)]."<BR>";

$tipoTitulo = "";
$mercadoTitulo = "";


switch ($etapa) {

    case 1:

       $tipoTitulo = 'EXPO';

        break;

    case 2:

        $tipoTitulo = 'IMPO';

        break;

}



switch ($mercado) {

    case 1:

       $mercadoTitulo = 'CHILE';

        break;

    case 2:

        $mercadoTitulo = 'PERU';

        break;
	case 3:

        $mercadoTitulo = 'ECUADOR';

        break;
	case 4:

        $mercadoTitulo = 'COLOMBIA';

        break;

}



$rangoTitulo = $mesesTitulo[(int)$mesDesde]." ".$añoDesde. " - " . $mesesTitulo[(int)$mesHasta]." ". $añoHasta;
$valoresTitulo = 'Valores En: '. $tipoDato;



// Crea un nuevo objeto PHPExcel
$objPHPExcel = new PHPExcel();




function retonarFecha($fechaDesde,$fechaHasta){
	   
	   
        $f1 = $fechaDesde;
        $f2 = $fechaHasta;
        $h1 = explode('-', $f1);
        $h2 = explode('-', $f2);
        $m1 = $h1[1];
        $y1 = $h1[2];
        $m2 = $h2[1];
        $y2 = $h2[2];
       $arregloMeses = array();
        //cho $y1 . '-' . $m1 . ' ' . $y2 . '-' . $m2;
        while (!($m1 == $m2 && $y1 == $y2)){
            if ($m1 * 1 < 10){
                array_push($arregloMeses, $y1 . '-0' . $m1 * 1);
            } else {
                array_push($arregloMeses, $y1 . '-' . $m1);
            }
            
            $m1++;
            if ($m1 == 13){
                $m1 = 1;
                $y1++;
            }
        }
        array_push($arregloMeses, $y2 . '-' . $m2);
      //  print_r($l);
		
	return $arregloMeses;
}

$arregloPrueba = array();

$arregloPrueba = retonarFecha($fechaDesde,$fechaHasta);
//print_r($arregloPrueba);

function cellColor($cells,$color){
        global $objPHPExcel;
        $objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()
        ->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array('rgb' => $color)
        ));
}

// Elimina las gridlines de la hoja de calculo
$objPHPExcel->getActiveSheet()->setShowGridlines(false); 
$objPHPExcel->getActiveSheet()->setPrintGridlines(false); 

//Preparando encabezado de la hoja de calculo
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20); 

$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(5); 
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(5); 

// Agrega valores y da formato a los titulos de la hoja de calculo
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B2', $tipoTitulo);
$objPHPExcel->getActiveSheet()->getStyle('B2')->getFont()->setSize(18);
$objPHPExcel->getActiveSheet()->getStyle('B2')->getFont()->setName('CALIBRI');
$objPHPExcel->getActiveSheet()->getStyle('B2')->getFont()->getColor()->setRGB('000000');

$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B3', $mercadoTitulo);
$objPHPExcel->getActiveSheet()->getStyle('B3')->getFont()->setSize(16);
$objPHPExcel->getActiveSheet()->getStyle('B3')->getFont()->setName('CALIBRI');
$objPHPExcel->getActiveSheet()->getStyle('B3')->getFont()->getColor()->setRGB('FF6600');

$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B4', $rangoTitulo);
$objPHPExcel->getActiveSheet()->getStyle('B4')->getFont()->setSize(13);
$objPHPExcel->getActiveSheet()->getStyle('B4')->getFont()->setName('CALIBRI');
$objPHPExcel->getActiveSheet()->getStyle('B4')->getFont()->getColor()->setRGB('808080');

$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B5', $valoresTitulo);
$objPHPExcel->getActiveSheet()->getStyle('B5')->getFont()->setSize(10);
$objPHPExcel->getActiveSheet()->getStyle('B5')->getFont()->setName('CALIBRI');
$objPHPExcel->getActiveSheet()->getStyle('B5')->getFont()->getColor()->setRGB('000000');






//Agrupa y da formato a las celdas de los niveles
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B7:B8');
$borde = array(
    'borders' => array(
        'top' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => '72A8ED'), 
            ),  
        'right' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => 'FFFFFF'), 
            ), 
        'bottom' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => '72A8ED'), 
            ), 
        'left' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => '72A8ED'), 
            ), 
        ), 
    ); 
$objPHPExcel->getActiveSheet()->getStyle('B7:B8')->applyFromArray($borde);
// cellColor('B7','72A8ED');



//Verifica cuantos niveles fueron consultados para llenar la cabezera
if($nivelTituloN2 != ''){
    $objPHPExcel->setActiveSheetIndex(0)->getCell('B7')->setValue($nivelTituloN1 . "\n        " . $nivelTituloN2);  
}else{
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B7', $nivelTituloN1);
}




//resto 1 semana
$totalMeses = "";
//echo strtotime($fechaDesde)-strtotime($fechaHasta);

$arregloAños = array();
array_push($arregloAños, $añoDesde);
array_push($arregloAños, $añoHasta);
$totalMeses =  count($arregloAños);
$totalAños = array('');



//cabezera primera tabla 
if('ENERO' == $mesesTitulo[$mesHasta]){
	  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C7', 'ENERO');
}else{
  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C7', 'ENERO' . ' - ' . $mesesTitulo[$mesHasta]);	
}



// Mezcla y da formato a la celda que indica el valor de los años en la consulta
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('C7:'.$letras[$totalMeses+2].'7');
$borde = array(
    'borders' => array(
        'top' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => '72A8ED'), 
            ),  
        'right' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => 'FFFFFF'), 
            ), 
        'bottom' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => 'FFFFFF'), 
            ), 
        'left' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => 'FFFFFF'), 
            ), 
        ), 
    ); 
$objPHPExcel->getActiveSheet()->getStyle('C7:'.$letras[$totalMeses+2].'7')->applyFromArray($borde);


// Rellena la cabezera con los valores TOTAL, TOTAL ANT. y DIF.
$objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras[$totalMeses+3].'7', 'DIF.'); 
$objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras[$totalMeses+4].'7', 'Mkt. SHARE'); 

$objPHPExcel->setActiveSheetIndex(0)->mergeCells($letras[$totalMeses+4].'7:'.$letras[$totalMeses+4].'8');

// Centra la celda TOTAL para dejarlas bajo el formato
$objPHPExcel->setActiveSheetIndex(0)->mergeCells($letras[$totalMeses+3].'7:'.$letras[$totalMeses+3].'8');
$borde = array(
    'borders' => array(
        'top' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => '72A8ED'), 
            ),  
        'right' => array(
            'style' => PHPExcel_Style_Border::BORDER_HAIR, 
            'color' => array('rgb' => '72A8ED'), 
            ), 
        'bottom' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => '72A8ED'), 
            ), 
        'left' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => 'FFFFFF'), 
            ), 
        ), 
    ); 
$objPHPExcel->getActiveSheet()->getStyle($letras[$totalMeses+3].'7:'.$letras[$totalMeses+3].'8')->applyFromArray($borde);

// Centra la celda TOTAL ANT. para dejarlas bajo el formato
$objPHPExcel->setActiveSheetIndex(0)->mergeCells($letras[$totalMeses+4].'7:'.$letras[$totalMeses+4].'8');
$borde = array(
    'borders' => array(
        'top' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => '72A8ED'), 
            ),  
        'right' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => 'FFFFFF'), 
            ), 
        'bottom' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => '72A8ED'), 
            ), 
        'left' => array(
            'style' => PHPExcel_Style_Border::BORDER_HAIR, 
            'color' => array('rgb' => '72A8ED'), 
            ), 
        ), 
    ); 
$objPHPExcel->getActiveSheet()->getStyle($letras[$totalMeses+4].'7:'.$letras[$totalMeses+4].'8')->applyFromArray($borde);
$borde = array(
    'borders' => array(
        'top' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => '72A8ED'), 
            ),  
        'right' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => '72A8ED'), 
            ), 
        'bottom' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => '72A8ED'), 
            ), 
        'left' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => 'FFFFFF'), 
            ), 
        ), 
    ); 
$objPHPExcel->getActiveSheet()->getStyle($letras[$totalMeses+4].'7:'.$letras[$totalMeses+4].'8')->applyFromArray($borde);


// Se da formato a la cabecera
$borde = array(
    'borders' => array(
        'vertical' => array(
            'style' => PHPExcel_Style_Border::BORDER_HAIR, 
            'color' => array('rgb' => '72A8ED'), 
            ),  
        ), 
    ); 
$objPHPExcel->getActiveSheet()->getStyle('C8:'.$letras[$totalMeses+2].'8')->applyFromArray($borde);
$objPHPExcel->getActiveSheet()->getStyle('B7:'.$letras[$totalMeses+4].'8')->getAlignment()->applyFromArray(array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('C7:'.$letras[$totalMeses+4].'8')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('B7:'.$letras[$totalMeses+4].'8')->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('B7:'.$letras[$totalMeses+4].'8')->getFont()->setSize(9);
$objPHPExcel->getActiveSheet()->getStyle('B7:'.$letras[$totalMeses+4].'8')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('B7:'.$letras[$totalMeses+4].'8')->getFont()->setName('CALIBRI');
$objPHPExcel->getActiveSheet()->getStyle('B7:'.$letras[$totalMeses+4].'8')->getFont()->getColor()->setRGB('FFFFFF');
cellColor('B7:'.$letras[$totalMeses+4].'8','72A8ED');



// Centra la celda TOTAL para dejarlas bajo el formato
$objPHPExcel->setActiveSheetIndex(0)->mergeCells($letras[$totalMeses+3].'7:'.$letras[$totalMeses+3].'8');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells($letras[$totalMeses+4].'7:'.$letras[$totalMeses+4].'8');


$objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras[3].'8', $añoHasta);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras[4].'8', $añoDesde);





//echo $mesesTitulo[$nuevoMesDeste];
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Preparando encabezado de la hoja de calculo 
$objPHPExcel->getActiveSheet()->getStyle('H7:I7')->getFont()->setSize(9);

$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H7', $mesesTitulo[$nuevoMesDeste]. ' - ' . $mesesTitulo[$nuevoMesHasta]);	


// Mezcla y da formato a la celda que indica el valor de los años en la consulta
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('H7:I7');
$borde = array(
    'borders' => array(
        'top' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => '72A8ED'), 
            ),  
        'right' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => 'FFFFFF'), 
            ), 
        'bottom' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => 'FFFFFF'), 
            ), 
        'left' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => '72A8ED'), 
            ), 
        ), 
    ); 
$objPHPExcel->getActiveSheet()->getStyle('H7:I7')->applyFromArray($borde);


$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J7', 'DIF.'); 
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('K7', 'Mkt. SHARE'); 
$objPHPExcel->getActiveSheet()->getStyle('J7:K7')->getFont()->setSize(9);

$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H8', ($añoHasta-1).'-'.$añoHasta);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I8', ($añoDesde-1).'-'.$añoDesde);
$objPHPExcel->getActiveSheet()->getStyle('H8:I8')->getFont()->setSize(9);

$borde = array(
    'borders' => array(
        'top' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => '72A8ED'), 
            ),  
        'right' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => '72A8ED'), 
            ), 
        'bottom' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => '72A8ED'), 
            ), 
        'left' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => 'FFFFFF'), 
            ), 
        ), 
    ); 
$objPHPExcel->getActiveSheet()->getStyle('J7:K8')->applyFromArray($borde);

$borde = array(
    'borders' => array(
        'top' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => 'FFFFFF'), 
            ),  
        'right' => array(
            'style' => PHPExcel_Style_Border::BORDER_HAIR, 
            'color' => array('rgb' => 'FFFFFF'), 
            ), 
        'bottom' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => '72A8ED'), 
            ), 
        'left' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => '72A8ED'), 
            ), 
        ), 
    ); 
$objPHPExcel->getActiveSheet()->getStyle('H8:I8')->applyFromArray($borde);

// Centra la celda TOTAL para dejarlas bajo el formato
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('J7:J8');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('K7:K8');


// Se da formato a la cabecera
$borde = array(
    'borders' => array(
        'vertical' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => 'FFFFFF'), 
            ),  
        ), 
    ); 
$objPHPExcel->getActiveSheet()->getStyle('H8:I8')->applyFromArray($borde);
$objPHPExcel->getActiveSheet()->getStyle('J7:K7')->applyFromArray($borde);
$objPHPExcel->getActiveSheet()->getStyle('J8:K8')->applyFromArray($borde);
$objPHPExcel->getActiveSheet()->getStyle('H7:K8')->getAlignment()->applyFromArray(array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('H7:K8')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('H7:K8')->getFont()->setSize(9);
$objPHPExcel->getActiveSheet()->getStyle('H7:K8')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('H7:K8')->getFont()->setName('CALIBRI');
$objPHPExcel->getActiveSheet()->getStyle('H7:K8')->getFont()->getColor()->setRGB('FFFFFF');
cellColor('H7:K8','72A8ED');

///////////////////////////////////////////////////////////////////////////////////////////////////

//Preparando encabezado de la hoja de calculo 
$objPHPExcel->getActiveSheet()->getStyle('M7:N7')->getFont()->setSize(9);


$objPHPExcel->setActiveSheetIndex(0)->setCellValue('M7', $mesesTitulo[$mesHasta]);	

// Mezcla y da formato a la celda que indica el valor de los años en la consulta
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('M7:N7');
$borde = array(
    'borders' => array(
        'top' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => '72A8ED'), 
            ),  
        'right' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => 'FFFFFF'), 
            ), 
        'bottom' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => 'FFFFFF'), 
            ), 
        'left' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => '72A8ED'), 
            ), 
        ), 
    ); 
$objPHPExcel->getActiveSheet()->getStyle('M7:N7')->applyFromArray($borde);

$objPHPExcel->setActiveSheetIndex(0)->setCellValue('O7', 'DIF.'); 
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('P7', 'Mkt. SHARE'); 
$objPHPExcel->getActiveSheet()->getStyle('O7:P7')->getFont()->setSize(9);

$objPHPExcel->setActiveSheetIndex(0)->setCellValue('M8', $añoHasta);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('N8', $añoDesde);
$objPHPExcel->getActiveSheet()->getStyle('M8:N8')->getFont()->setSize(9);


$borde = array(
    'borders' => array(
        'top' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => '72A8ED'), 
            ),  
        'right' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => '72A8ED'), 
            ), 
        'bottom' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => '72A8ED'), 
            ), 
        'left' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => 'FFFFFF'), 
            ), 
        ), 
    ); 
$objPHPExcel->getActiveSheet()->getStyle('O7:P8')->applyFromArray($borde);


$borde = array(
    'borders' => array(
        'top' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => 'FFFFFF'), 
            ),  
        'right' => array(
            'style' => PHPExcel_Style_Border::BORDER_HAIR, 
            'color' => array('rgb' => 'FFFFFF'), 
            ), 
        'bottom' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => '72A8ED'), 
            ), 
        'left' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => '72A8ED'), 
            ), 
        ), 
    ); 
$objPHPExcel->getActiveSheet()->getStyle('M8:N8')->applyFromArray($borde);


// Centra la celda TOTAL para dejarlas bajo el formato
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('O7:O8');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('P7:P8');


// Se da formato a la cabecera
$borde = array(
    'borders' => array(
        'vertical' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => 'FFFFFF'), 
            ),  
        ), 
    ); 
$objPHPExcel->getActiveSheet()->getStyle('M8:N8')->applyFromArray($borde);
$objPHPExcel->getActiveSheet()->getStyle('O7:P7')->applyFromArray($borde);
$objPHPExcel->getActiveSheet()->getStyle('O8:P8')->applyFromArray($borde);
$objPHPExcel->getActiveSheet()->getStyle('M7:P8')->getAlignment()->applyFromArray(array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('M7:P8')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('M7:P8')->getFont()->setSize(9);
$objPHPExcel->getActiveSheet()->getStyle('M7:P8')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('M7:P8')->getFont()->setName('CALIBRI');
$objPHPExcel->getActiveSheet()->getStyle('M7:P8')->getFont()->getColor()->setRGB('FFFFFF');
cellColor('M7:P8','72A8ED');



// consulta que limpia la tabla antes de insertar los datos
$objDeclaracion->EliminarConsultaUsuarioIndicadores($usuarioId);

//consulta que obtine el top 15
$resultado = $objDeclaracion->traerTop20Nivel1Indicadores($nivel1,$usuarioId, $mesDesde, $mesHasta, $añoDesde, $añoHasta);
$arregloNivel1 = array();
$arregloNivel2 = array();
while($row = sql_db::sql_fetch_assoc($resultado)){
		array_push($arregloNivel1,$row['nivel1']);
}

$cantidad = count($arregloNivel1);
//print_r($arregloNivel1);


for ($p = 0; $p < $cantidad ; $p++) {
	
	// $objDeclaracion->generarConsultaExcel($nivel1,$usuarioId,$arregloNivel1[$p],str_replace("-", "", $arregloPrueba),$nivel1);
	 $objDeclaracion->generarConsultaExcelIndicadores($nivel1,$arregloNivel1[$p],$usuarioId, $mesDesde, $mesHasta, $añoDesde, $añoHasta);
	 //$objDeclaracion->generarConsultaExcel($nivel1,$usuarioId,4330,str_replace("-", "", $arregloPrueba));
	 
	 if(!$nivel2 == ""){
		 
	
		 
			  $result02 = $objDeclaracion->traerTopNivel2Indicadores($usuarioId, $mesDesde, $mesHasta, $añoDesde, $añoHasta,$arregloNivel1[$p],$nivel1,$nivel2);
				 //$result02 = $objDeclaracion->traerTopNivel2($usuarioId, $mesDesde, $mesHasta, $añoDesde, $añoHasta,4330);
				//consulta que obtine el top 20 nivel2  4330
				
				while($row03 = sql_db::sql_fetch_assoc($result02)){
						
					array_push($arregloNivel2,$row03['nivel2']);
							
				}
				
				
				
				$cantidad2 = count($arregloNivel2);
				
				if($cantidad2 == 0){
					
				}else{
					
					for($h=0; $h < $cantidad2; $h++){
						
						$objDeclaracion->generarConsultaExcelNivel2Indicadores($nivel1,$nivel2,$usuarioId,$arregloNivel1[$p],$arregloNivel2[$h], $mesDesde, $mesHasta, $añoDesde, $añoHasta);
						//$objDeclaracion->generarConsultaExcelNivel2($nivel1,$nivel2,$usuarioId,4330,$arregloNivel2[$h],str_replace("-", "", $arregloPrueba));
					}
					$objDeclaracion->insertarOtrosNivel2Indicadores($nivel1,$usuarioId,$arregloNivel1[$p],$nivel2);
					//$objDeclaracion->insertarOtrosNivel2($nivel1,$usuarioId,4330,str_replace("-", "", $arregloPrueba));
					$arregloNivel2 = array();
					
				}
				
				
				
		 
	 }
	
	
		
}


$objDeclaracion->insertarOtrosIndicadoresNivel1($nivel1,$arregloNivel1,$usuarioId, $mesDesde, $mesHasta, $añoDesde, $añoHasta);


	
	$resultadoOtros = $objDeclaracion->validarOtrosIndicadores($usuarioId);


			while($rowOtros = sql_db::sql_fetch_assoc($resultadoOtros)){

				//echo $rowOtros['total1'];
				if($rowOtros['total1'] == 0 && $rowOtros['total2'] == 0 && $rowOtros['total3'] == 0 && $rowOtros['total4'] == 0 && $rowOtros['total5'] == 0 && $rowOtros['total6'] == 0){
					//echo "pal que lee 8==========D";
					$objDeclaracion->eliminarIndicadoresEnNull($usuarioId);
					if($cantidad > 14 ){
					
							if($clienteId == 16 || $clienteId == 21 || $clienteId == 25 || $clienteId == 29 || $clienteId == 31 || $clienteId == 32 || $clienteId == 34 || $clienteId == 38 || $clienteId == 18 || $clienteId == 40){
								$objDeclaracion->ingresarOtrosPreviewIndicadores($usuarioId);
							}else if($clienteId == 36 || $clienteId == 23){
								$objDeclaracion->ingresarOtrosPreviewIndicadoresP2($usuarioId);
							}else if($clienteId == 24){
								$objDeclaracion->ingresarOtrosPreviewIndicadoresP3($usuarioId);
							}else{
								
							}
					}
					
					
					
				}else{
					//echo "no funciono";
				}
				///$objDeclaracion->actualizarMarketShareIndicadores($row9['campo1'],$row9['MKSHARE'],$usuarioId,$row9['IdIndicadorSempat']);
				


				
				
			}
	







$objDeclaracion->insetarTotalExcelIndicadores($usuarioId);

$resultado = $objDeclaracion->calcularMarketShareIndicadores($usuarioId);
//$resultado2 = $objDeclaracion->calcularMarketShareIndicadores2($usuarioId);
//$resultado3 = $objDeclaracion->calcularMarketShareIndicadores3($usuarioId);




while($row9 = sql_db::sql_fetch_assoc($resultado)){
	//array_push($arregloMarketShare, );
	//$arregloMarketShare[$row9['campo1']] = $row9['MKSHARE'];
	$objDeclaracion->actualizarMarketShareIndicadores($row9['campo1'],$row9['MKSHARE'],$usuarioId,$row9['IdIndicadorSempat']);
	
	$marketShare = $objDeclaracion->calcularMarketShareIndicadoresNivel2($usuarioId,$row9['campo1'],$row9['IdIndicadorSempat']);
	//print_r($marketShare);
	
	if($row9['campo2'] == ""){
		
	}else{
		while($rowMk = sql_db::sql_fetch_assoc($marketShare)){
		$objDeclaracion->actualizarMarketShareNivel2($usuarioId,$rowMk['IdIndicadorSempat'],$rowMk['MKT1'],$rowMk['MKT2'],$rowMk['MKT3']);
		}
	}
	

	
	
}

$resultado2 = $objDeclaracion->calcularMarketShareIndicadores2($usuarioId);


while($row99 = sql_db::sql_fetch_assoc($resultado2)){
	$objDeclaracion->actualizarMarketShareIndicadores2($row99['campo1'],$row99['MKSHARE'],$usuarioId,$row99['IdIndicadorSempat']);
	
}

$resultado3 = $objDeclaracion->calcularMarketShareIndicadores3($usuarioId);


while($row999 = sql_db::sql_fetch_assoc($resultado3)){
	$objDeclaracion->actualizarMarketShareIndicadores3($row999['campo1'],$row999['MKSHARE'],$usuarioId,$row999['IdIndicadorSempat']);
	
}

$totalFilas = $cantidad;
$fila = 9; //Fila donde se comienza a llenar los datos

$bandera = true;    //Indica que se esta en nivel 1
$n2Inicio = '';     //Inicio de celdas de 2do nivel
$n2Fin = '';        //Fin de celdas de 2do nivel
$mod3 = 1;          //Contador que salta cada 3 valores.
//obtenerConsultaIndicadoresUsuario

$tablaDatos = $objDeclaracion->obtenerConsultaIndicadoresUsuario($usuarioId);

while($row2 = sql_db::sql_fetch_assoc($tablaDatos)){
	
	
		    if($row2['nivel1'] == '1'){
			
		// Si bandera es false, significa que antes de eso se termina el nivel 2, por lo tanto agrupa y formatea 
        // el nivel dos de la fila anterior.
				if(!$bandera){
					
					$n2Fin = $fila - 1;
					 //$n2Inicio = 9;
					$objPHPExcel->getActiveSheet()->getStyle('B' . $n2Inicio . ':B' . $n2Fin)->getAlignment()->setIndent(1);
					//$objPHPExcel->getActiveSheet()->getStyle($letras[$totalMeses+5] . $n2Inicio . ':' . 'F'  . $n2Fin)->getNumberFormat()->setFormatCode('0.00%');
					$objPHPExcel->getActiveSheet()->getStyle($letras[$totalMeses+3] . $n2Inicio . ':' . 'F'  . $n2Fin)->getFont()->setBold(true);
					$objPHPExcel->getActiveSheet()->getStyle('C' . $n2Inicio . ':' . 'D' . $n2Fin)->getNumberFormat()->setFormatCode('#,##0');
					$objPHPExcel->getActiveSheet()->getStyle('B' . $n2Inicio . ':' .  'F' . $n2Fin)->getFont()->setItalic(true);
					$objPHPExcel->getActiveSheet()->getStyle('B' . $n2Inicio . ':' . 'F' . $n2Fin)->getFont()->setName('CALIBRI');
					$objPHPExcel->getActiveSheet()->getStyle('B' . $n2Inicio . ':' .  'F' . $n2Fin)->getFont()->setSize(9);
					$borde = array(
						'borders' => array(
							'top' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN, 
								'color' => array('rgb' => '72A8ED'), 
								),
							'right' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN, 
								'color' => array('rgb' => '72A8ED'), 
								),
							'bottom' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN, 
								'color' => array('rgb' => '72A8ED'), 
								), 
							'left' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN, 
								'color' => array('rgb' => '72A8ED'), 
								), 
							), 
						);
					$objPHPExcel->getActiveSheet()->getStyle('B' . $n2Inicio . ':' . 'F' . $n2Fin)->applyFromArray($borde);
					cellColor('B' . $n2Inicio . ':' . 'F' . $n2Fin,'F5F5F5');
					
					//TABLA NUMERO 2
					$objPHPExcel->getActiveSheet()->getStyle('H' . $n2Inicio . ':H' . $n2Fin)->getAlignment()->setIndent(1);
					//$objPHPExcel->getActiveSheet()->getStyle('H' . $n2Inicio . ':' . 'K'  . $n2Fin)->getNumberFormat()->setFormatCode('0.00%');
					$objPHPExcel->getActiveSheet()->getStyle('H' . $n2Inicio . ':' . 'K'  . $n2Fin)->getFont()->setBold(true);
					$objPHPExcel->getActiveSheet()->getStyle('H' . $n2Inicio . ':' . 'I' . $n2Fin)->getNumberFormat()->setFormatCode('#,##0');
					$objPHPExcel->getActiveSheet()->getStyle('H' . $n2Inicio . ':' .  'K' . $n2Fin)->getFont()->setItalic(true);
					$objPHPExcel->getActiveSheet()->getStyle('H' . $n2Inicio . ':' . 'K' . $n2Fin)->getFont()->setName('CALIBRI');
					$objPHPExcel->getActiveSheet()->getStyle('H' . $n2Inicio . ':' .  'K' . $n2Fin)->getFont()->setSize(9);
					$borde = array(
						'borders' => array(
							'top' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN, 
								'color' => array('rgb' => '72A8ED'), 
								),
							'right' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN, 
								'color' => array('rgb' => '72A8ED'), 
								),
							'bottom' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN, 
								'color' => array('rgb' => '72A8ED'), 
								), 
							'left' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN, 
								'color' => array('rgb' => '72A8ED'), 
								), 
							), 
						);
					$objPHPExcel->getActiveSheet()->getStyle('H' . $n2Inicio . ':' . 'K' . $n2Fin)->applyFromArray($borde);
					cellColor('H' . $n2Inicio . ':' . 'K' . $n2Fin,'F5F5F5');
					
					//TABLA NUMERO 3
					$objPHPExcel->getActiveSheet()->getStyle('M' . $n2Inicio . ':M' . $n2Fin)->getAlignment()->setIndent(1);
					//$objPHPExcel->getActiveSheet()->getStyle('M' . $n2Inicio . ':' . 'P'  . $n2Fin)->getNumberFormat()->setFormatCode('0.00%');
					$objPHPExcel->getActiveSheet()->getStyle('H' . $n2Inicio . ':' . 'K'  . $n2Fin)->getFont()->setBold(true);
					$objPHPExcel->getActiveSheet()->getStyle('M' . $n2Inicio . ':' . 'N' . $n2Fin)->getNumberFormat()->setFormatCode('#,##0');
					$objPHPExcel->getActiveSheet()->getStyle('M' . $n2Inicio . ':' .  'P' . $n2Fin)->getFont()->setItalic(true);
					$objPHPExcel->getActiveSheet()->getStyle('M' . $n2Inicio . ':' . 'P' . $n2Fin)->getFont()->setName('CALIBRI');
					$objPHPExcel->getActiveSheet()->getStyle('M' . $n2Inicio . ':' . 'P' . $n2Fin)->getFont()->setSize(9);
					$borde = array(
						'borders' => array(
							'top' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN, 
								'color' => array('rgb' => '72A8ED'), 
								),
							'right' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN, 
								'color' => array('rgb' => '72A8ED'), 
								),
							'bottom' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN, 
								'color' => array('rgb' => '72A8ED'), 
								), 
							'left' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN, 
								'color' => array('rgb' => '72A8ED'), 
								), 
							), 
						);
					$objPHPExcel->getActiveSheet()->getStyle('M' . $n2Inicio . ':' . 'P' . $n2Fin)->applyFromArray($borde);
					cellColor('M' . $n2Inicio . ':' . 'P' . $n2Fin,'F5F5F5');
					$bandera = true;
				
				
				}
				        // celda que contiene el nombre del nivel 1
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B' . $fila, $row2['campo1']);
				$columna = 3; // Columna donde se comienza a llenar los datos
				for($x = 0 ; $x < $totalMeses ; $x++) {

					// valores por meses en el nivel 1.
					if($row2[$mesesDato[$x]]){
						$objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras[$columna] . $fila, $row2[$mesesDato[$x]]);
						
						$columna++; 

					}elseif($x < $totalMeses){
						$objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras[$columna] . $fila, 0);

						$columna++;

					}else{
						break;
					}
				}
				// celda que contiene el total del nivel 1
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C' . $fila, $row2['total1']);
			    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D' . $fila, $row2['total2']);
				
				if($row2['MKSHARE1'] == 0000){
						//echo "entro aca"."<br>";
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F' . $fila, (0));
				}else{
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F' . $fila, ($row2['MKSHARE1']/100));
				}
				
				if($row2['MKSHARE2'] == 0000){
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('K' . $fila, (0));
				}else{
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('K' . $fila, ($row2['MKSHARE2']/100));
				}
				
				if($row2['MKSHARE3'] == 0000){
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('P' . $fila, (0));
				}else{
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('P' . $fila, ($row2['MKSHARE3']/100));
				}
				
				
				//$objPHPExcel->setActiveSheetIndex(0)->setCellValue('K' . $fila, ($row2['MKSHARE2']/100));
				//$objPHPExcel->setActiveSheetIndex(0)->setCellValue('P' . $fila, ($row2['MKSHARE3']/100));
				
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H' . $fila, $row2['total3']);
			    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I' . $fila, $row2['total4']);

				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('M' . $fila, $row2['total5']);
			    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('N' . $fila, $row2['total6']);


				// Calcula diferencia porcentual entre TOTAL y TOTAL ANT en el nivel 1
				if($row2['total2'] != 0){
					$diferencia = ($row2['total1'] / $row2['total2']) - 1;    
				}else{
					$diferencia = 1;
				}
				
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E' . $fila, $diferencia);
				if($diferencia >= 0){ // Verifica si es positivo para dar cierto formato
					$objPHPExcel->getActiveSheet()->getStyle('E' . $fila)->getFont()->getColor()->setRGB('0000FF');
				}else{ // En caso de ser negativo, entrega otro formato
					$objPHPExcel->getActiveSheet()->getStyle('E' . $fila)->getFont()->getColor()->setRGB('FF0000');
				}
				
				// Calcula diferencia porcentual entre TOTAL y TOTAL ANT en el nivel 1
				if($row2['total4'] != 0){
					$diferencia2 = ($row2['total3'] / $row2['total4']) - 1;    
				}else{
					$diferencia2 = 1;
				}
				
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J' . $fila, $diferencia2);
				if($diferencia2 >= 0){ // Verifica si es positivo para dar cierto formato
					$objPHPExcel->getActiveSheet()->getStyle('J' . $fila)->getFont()->getColor()->setRGB('0000FF');
				}else{ // En caso de ser negativo, entrega otro formato
					$objPHPExcel->getActiveSheet()->getStyle('J' . $fila)->getFont()->getColor()->setRGB('FF0000');
				}
				
				// Calcula diferencia porcentual entre TOTAL y TOTAL ANT en el nivel 1
				if($row2['total6'] != 0){
					$diferencia3 = ($row2['total5'] / $row2['total6']) - 1;    
				}else{
					$diferencia3 = 1;
				}
				
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('O' . $fila, $diferencia3);
				if($diferencia3 >= 0){ // Verifica si es positivo para dar cierto formato
					$objPHPExcel->getActiveSheet()->getStyle('O' . $fila)->getFont()->getColor()->setRGB('0000FF');
				}else{ // En caso de ser negativo, entrega otro formato
					$objPHPExcel->getActiveSheet()->getStyle('O' . $fila)->getFont()->getColor()->setRGB('FF0000');
				}
				
				 // Da formato al nivel 1 actual
				$objPHPExcel->getActiveSheet()->getStyle('B' . $fila . ':' . $letras[$columna+2] . $fila)->getFont()->setSize(9);
				$objPHPExcel->getActiveSheet()->getStyle('B' . $fila . ':' . $letras[$columna+2] . $fila)->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->getStyle('B' . $fila . ':' . $letras[$columna+2] . $fila)->getFont()->setName('CALIBRI');
				$objPHPExcel->getActiveSheet()->getStyle('B' . $fila . ':' . 'D'. $fila)->getFont()->getColor()->setRGB('000000');
				$objPHPExcel->getActiveSheet()->getStyle('C' . $fila . ':' .  'D' . $fila)->getNumberFormat()->setFormatCode('#,##0');
				
				$objPHPExcel->getActiveSheet()->getStyle('H' . $fila . ':' . 'K' . $fila)->getFont()->setSize(9);
				$objPHPExcel->getActiveSheet()->getStyle('H' . $fila . ':' . 'K' . $fila)->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->getStyle('H' . $fila . ':' . 'K' . $fila)->getFont()->setName('CALIBRI');
				$objPHPExcel->getActiveSheet()->getStyle('H' . $fila . ':' . 'I' . $fila)->getFont()->getColor()->setRGB('000000');
				$objPHPExcel->getActiveSheet()->getStyle('H' . $fila . ':' . 'I' . $fila)->getNumberFormat()->setFormatCode('#,##0');
				
				$objPHPExcel->getActiveSheet()->getStyle('M' . $fila . ':' . 'P' . $fila)->getFont()->setSize(9);
				$objPHPExcel->getActiveSheet()->getStyle('M' . $fila . ':' . 'P' . $fila)->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->getStyle('M' . $fila . ':' . 'P' . $fila)->getFont()->setName('CALIBRI');
				$objPHPExcel->getActiveSheet()->getStyle('M' . $fila . ':' . 'N' . $fila)->getFont()->getColor()->setRGB('000000');
				$objPHPExcel->getActiveSheet()->getStyle('M' . $fila . ':' . 'N' . $fila)->getNumberFormat()->setFormatCode('#,##0');
				
				
				$objPHPExcel->getActiveSheet()->getStyle('E' . $fila)->getNumberFormat()->setFormatCode('0.00%');
				$objPHPExcel->getActiveSheet()->getStyle('J' . $fila)->getNumberFormat()->setFormatCode('0.00%');
				$objPHPExcel->getActiveSheet()->getStyle('O' . $fila)->getNumberFormat()->setFormatCode('0.00%');
			    $objPHPExcel->getActiveSheet()->getStyle('F' . $fila . ':' . 'F' . $fila)->getNumberFormat()->setFormatCode('#,##%');
				//$objPHPExcel->getActiveSheet()->getStyle('F' . $fila)->getNumberFormat()->setFormatCode('0.00%');
				$objPHPExcel->getActiveSheet()->getStyle('K' . $fila . ':' . 'K' . $fila)->getNumberFormat()->setFormatCode('#,##%');
				//$objPHPExcel->getActiveSheet()->getStyle('K' . $fila)->getNumberFormat()->setFormatCode('0.00%');
				$objPHPExcel->getActiveSheet()->getStyle('P' . $fila . ':' . 'P' . $fila)->getNumberFormat()->setFormatCode('#,##%');
				//$objPHPExcel->getActiveSheet()->getStyle('P' . $fila)->getNumberFormat()->setFormatCode('0.00%');
				
				$borde = array(
					'borders' => array(
						'top' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN, 
							'color' => array('rgb' => '72A8ED'), 
							),
						'right' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN, 
							'color' => array('rgb' => '72A8ED'), 
							),
						'bottom' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN, 
							'color' => array('rgb' => '72A8ED'), 
							), 
						'left' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN, 
							'color' => array('rgb' => '72A8ED'), 
							), 
						), 
					); 
				$objPHPExcel->getActiveSheet()->getStyle('B' . $fila . ':' . $letras[$columna+1] . $fila)->applyFromArray($borde);
				if($row2['orden'] == 0){ //Debe ser coloreado de otra forma
					cellColor('B' . $fila . ':' . 'F' . $fila ,'B8CFEB');
				}
				$borde = array(
					'borders' => array(
						'top' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN, 
							'color' => array('rgb' => '72A8ED'), 
							),
						'right' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN, 
							'color' => array('rgb' => '72A8ED'), 
							),
						'bottom' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN, 
							'color' => array('rgb' => '72A8ED'), 
							), 
						'left' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN, 
							'color' => array('rgb' => '72A8ED'), 
							), 
						), 
					); 
				$objPHPExcel->getActiveSheet()->getStyle('H' . $fila . ':' . 'K' . $fila)->applyFromArray($borde);
				if($row2['orden'] == 0){ //Debe ser coloreado de otra forma
					cellColor('H' . $fila . ':' . 'K' . $fila ,'B8CFEB');
				}
				$borde = array(
					'borders' => array(
						'top' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN, 
							'color' => array('rgb' => '72A8ED'), 
							),
						'right' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN, 
							'color' => array('rgb' => '72A8ED'), 
							),
						'bottom' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN, 
							'color' => array('rgb' => '72A8ED'), 
							), 
						'left' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN, 
							'color' => array('rgb' => '72A8ED'), 
							), 
						), 
					); 
				$objPHPExcel->getActiveSheet()->getStyle('M' . $fila . ':' . 'P' . $fila)->applyFromArray($borde);
				if($row2['orden'] == 0){ //Debe ser coloreado de otra forma
					cellColor('M' . $fila . ':' . 'P' . $fila ,'B8CFEB');
				}
				$fila++;
		

				
		
		
		}else{
			
			
			
			
			
			if($bandera){
            $n2Inicio = $fila;
            $bandera = false;
            $mod3 = 1;
			}
			// Da valor y formatea la celda que contiene el nombre del nivel 2
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B' . $fila, $row2['campo2']);
			
			$columna = 3; // Columna donde se comienza a llenar los datos
			for($i = 0 ; $i < $totalMeses ; $i++) {

				// Rellena y da formato a los valores por meses en el nivel 2.
				if($row2[$mesesDato[$i]]){
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras[$columna] . $fila, $row2[$mesesDato[$i]]);

					$columna++; 

				}elseif($i < $totalMeses){
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras[$columna] . $fila, 0);

					$columna++;

				}else{
					break;
				}
			}
			
			// Rellena y da formato a la celda que contiene el total del nivel 2
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C' . $fila, $row2['total1']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D' . $fila, $row2['total2']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H' . $fila, $row2['total3']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I' . $fila, $row2['total4']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('M' . $fila, $row2['total5']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('N' . $fila, $row2['total6']);
			
		//	echo $row2['MKSHARE1'];
			
				if($row2['MKSHARE1'] == 0000){
					echo "entro aca"."<br>";
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F' . $fila, (0));
				}else{
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F' . $fila, ($row2['MKSHARE1']/100));
				}
				
				if($row2['MKSHARE2'] == 0000){
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('K' . $fila, (0));
				}else{
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('K' . $fila, ($row2['MKSHARE2']/100));
				}
				
				if($row2['MKSHARE3'] == 0000){
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('P' . $fila, (0));
				}else{
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('P' . $fila, ($row2['MKSHARE3']/100));
				}
				
			
		//   $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F' . $fila, ($row2['MKSHARE1']/100));
			//$objPHPExcel->setActiveSheetIndex(0)->setCellValue('K' . $fila, ($row2['MKSHARE2']/100));
		 	//$objPHPExcel->setActiveSheetIndex(0)->setCellValue('P' . $fila, ($row2['MKSHARE3']/100));
				
				
			// Calcula y formatea diferencia porcentual entre TOTAL y TOTAL ANT en el nivel 1
			
				// Calcula diferencia porcentual entre TOTAL y TOTAL ANT en el nivel 1
				
				if($row2['total2'] != 0){
					$diferencia01 = ($row2['total1'] / $row2['total2']) - 1;    
				}else{
					$diferencia01 = 1;
				}
				
				if($row2['total4'] != 0){
					$diferencia02 = ($row2['total3'] / $row2['total4']) - 1;    
				}else{
					$diferencia02 = 1;
				}
				
			
				if($diferencia02 >= 0){ // Verifica si es positivo para dar cierto formato
					$objPHPExcel->getActiveSheet()->getStyle('J' . $fila)->getFont()->getColor()->setRGB('0000FF');
				}else{ // En caso de ser negativo, entrega otro formato
					$objPHPExcel->getActiveSheet()->getStyle('J' . $fila)->getFont()->getColor()->setRGB('FF0000');
				}
				

		   
			if($diferencia01 >= 0){ // Verifica si es positivo para dar cierto formato
				$objPHPExcel->getActiveSheet()->getStyle('E' . $fila)->getFont()->getColor()->setRGB('0000FF');
			}else{ // En caso de ser negativo, entrega otro formato
				$objPHPExcel->getActiveSheet()->getStyle('E' . $fila)->getFont()->getColor()->setRGB('FF0000');
			}
			
			// Calcula diferencia porcentual entre TOTAL y TOTAL ANT en el nivel 1
				if($row2['total6'] != 0){
					$diferencia03 = ($row2['total5'] / $row2['total6']) - 1;    
				}else{
					$diferencia03 = 1;
				}
				
				//$objPHPExcel->setActiveSheetIndex(0)->setCellValue('O' . $fila, $diferencia03);
				if($diferencia03 >= 0){ // Verifica si es positivo para dar cierto formato
					$objPHPExcel->getActiveSheet()->getStyle('O' . $fila)->getFont()->getColor()->setRGB('0000FF');
				}else{ // En caso de ser negativo, entrega otro formato
					$objPHPExcel->getActiveSheet()->getStyle('O' . $fila)->getFont()->getColor()->setRGB('FF0000');
				}
			
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E' . $fila, $diferencia01);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J' . $fila, $diferencia02);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('O' . $fila, $diferencia03);
			$objPHPExcel->getActiveSheet()->getStyle('E' . $fila)->getNumberFormat()->setFormatCode('0.00%');
			$objPHPExcel->getActiveSheet()->getStyle('J' . $fila)->getNumberFormat()->setFormatCode('0.00%');
			$objPHPExcel->getActiveSheet()->getStyle('O' . $fila)->getNumberFormat()->setFormatCode('0.00%');
			
			//objPHPExcel->getActiveSheet()->getStyle('F' . $fila)->getNumberFormat()->setFormatCode('0%');
			$objPHPExcel->getActiveSheet()->getStyle('F' . $fila . ':' . 'F' . $fila)->getNumberFormat()->setFormatCode('#,##%');
			$objPHPExcel->getActiveSheet()->getStyle('K' . $fila . ':' . 'K' . $fila)->getNumberFormat()->setFormatCode('#,##%');
			$objPHPExcel->getActiveSheet()->getStyle('P' . $fila . ':' . 'P' . $fila)->getNumberFormat()->setFormatCode('#,##%');
			
			$objPHPExcel->getActiveSheet()->getStyle('F' . $fila . ':' . 'F' . $fila)->getNumberFormat()->applyFromArray( 
        array( 
            'code' => PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE
        )
		);
			$objPHPExcel->getActiveSheet()->getStyle('K' . $fila . ':' . 'K' . $fila)->getNumberFormat()->applyFromArray( 
        array( 
            'code' => PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE
        )
		);
		
			$objPHPExcel->getActiveSheet()->getStyle('P' . $fila . ':' . 'P' . $fila)->getNumberFormat()->applyFromArray( 
        array( 
            'code' => PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE
        )
		);
			
			
			//<$objPHPExcel->getActiveSheet()->getStyle('F' . $fila . ':' . 'F' . $fila)->getNumberFormat()->setFormatCode('#,##%');
		   // $objPHPExcel->getActiveSheet()->getStyle('F' . $fila)->getNumberFormat()->setFormatCode('0.00%');
			
			$objPHPExcel->getActiveSheet()->getStyle('K' . $fila . ':' . 'K' . $fila)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('P' . $fila . ':' . 'P' . $fila)->getFont()->setBold(true);

		
			  // Cada 3 filas, se agrega una linea divisoria
			if($mod3 == 3){

				$borde = array(
					'borders' => array(
						'bottom' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN, 
							'color' => array('rgb' => '72A8ED'), 
							), 
						), 
					); 
				$objPHPExcel->getActiveSheet()->getStyle('B' . $fila . ':' . 'F' . $fila)->applyFromArray($borde);
				$objPHPExcel->getActiveSheet()->getStyle('H' . $fila . ':' . 'K' . $fila)->applyFromArray($borde);
				$objPHPExcel->getActiveSheet()->getStyle('M' . $fila . ':' . 'P' . $fila)->applyFromArray($borde);
				$mod3 = 0;
			}

			$objPHPExcel->getActiveSheet()->getRowDimension($fila)->setOutlineLevel(1);
			$objPHPExcel->getActiveSheet()->getRowDimension($fila)->setVisible(false);

			$mod3++;
			$fila++;

			
			
			
			
			
		}
}

$objPHPExcel->getActiveSheet()->setShowSummaryBelow(false);

// Se le agrega el formato final de las columnas
$borde = array(
    'borders' => array(
        'left' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => '72A8ED'), 
            ), 
        'right' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => '72A8ED'), 
            ), 
        ), 
    ); 
$objPHPExcel->getActiveSheet()->getStyle('C9:' . $letras[$totalMeses+4] . ($fila-1))->applyFromArray($borde);
$borde = array(
    'borders' => array(
        'vertical' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => '72A8ED'), 
            ), 
        ), 
    ); 
$objPHPExcel->getActiveSheet()->getStyle('C9:' . $letras[$totalMeses+2] . ($fila-1))->applyFromArray($borde);
$borde = array(
    'borders' => array(
        'left' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => '72A8ED'), 
            ), 
        'right' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => '72A8ED'), 
            ), 
        ), 
    ); 
$objPHPExcel->getActiveSheet()->getStyle($letras[$totalMeses+3] . '9:' . $letras[$totalMeses+3] . ($fila-1))->applyFromArray($borde);

	


// Se le agrega el formato final de las columnas
$borde = array(
    'borders' => array(
        'left' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => '72A8ED'), 
            ), 
        'right' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => '72A8ED'), 
            ), 
        ), 
    ); 
$objPHPExcel->getActiveSheet()->getStyle('H9:' . 'K' . ($fila-1))->applyFromArray($borde);
$borde = array(
    'borders' => array(
        'vertical' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => '72A8ED'), 
            ), 
        ), 
    ); 
$objPHPExcel->getActiveSheet()->getStyle('H9:' . 'K' . ($fila-1))->applyFromArray($borde);
$borde = array(
    'borders' => array(
        'left' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => '72A8ED'), 
            ), 
        'right' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => '72A8ED'), 
            ), 
        ), 
    ); 
$objPHPExcel->getActiveSheet()->getStyle('H' . '9:' . 'K'. ($fila-1))->applyFromArray($borde);



// Se le agrega el formato final de las columnas
$borde = array(
    'borders' => array(
        'left' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => '72A8ED'), 
            ), 
        'right' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => '72A8ED'), 
            ), 
        ), 
    ); 
$objPHPExcel->getActiveSheet()->getStyle('M9:' . 'P' . ($fila-1))->applyFromArray($borde);
$borde = array(
    'borders' => array(
        'vertical' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => '72A8ED'), 
            ), 
        ), 
    ); 
$objPHPExcel->getActiveSheet()->getStyle('M9:' . 'P' . ($fila-1))->applyFromArray($borde);
$borde = array(
    'borders' => array(
        'left' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => '72A8ED'), 
            ), 
        'right' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('rgb' => '72A8ED'), 
            ), 
        ), 
    ); 
$objPHPExcel->getActiveSheet()->getStyle('M' . '9:' . 'P'. ($fila-1))->applyFromArray($borde);
	



// Renombrar Hoja
$objPHPExcel->getActiveSheet()->setTitle('REPORTE SEMPAT');
 
// Establecer la hoja activa, para que cuando se abra el documento se muestre primero.
$objPHPExcel->setActiveSheetIndex(0);
 




// Verifica los directorios para guardar los archivos existen y los crea en caso de no existir

$uploaddir = "../../ReportesExcel/declaracionesIndicadores";
            
if (file_exists($uploaddir)==false) {
    $mask = umask(0);
    mkdir($uploaddir, 0777);
    umask($mask);
}

$uploaddir = $uploaddir . "/" . $usuarioId . "/";

if (file_exists($uploaddir)==false) {
    $mask = umask(0);
    mkdir($uploaddir, 0777);
    umask($mask);
}

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save($uploaddir . 'ReporteSempat.xls');
//$objWriter->save('php://output');
exit;

?>
