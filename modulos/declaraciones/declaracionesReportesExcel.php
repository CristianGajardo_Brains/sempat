<?php
session_start();

setlocale(LC_TIME, "spanish");
$fechaActual = htmlentities(strftime("%A %d de %B del %Y"));
$fechaActual = ucfirst($fechaActual);

$usuarioLog = $_SESSION["SEMPAT_usuarioNombre"];
$usuarioId = $_SESSION["SEMPAT_usuarioId"];
$clienteId = $_SESSION["SEMPAT_clienteId"];

include ("../../librerias/conexion.php");
require('declaraciones.class.php');
include ('../../librerias/PHPExcel/Classes/PHPExcel.php');

$objDeclaracion = new declaracion();

$mesesTitulo = array('','ENERO','FEBRERO','MARZO','ABRIL','MAYO','JUNIO','JULIO','AGOSTO','SEPTIEMBRE','OCTUBRE','NOVIEMBRE','DICIEMBRE');
$letras = array("","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");
$mesesCabezera = array('','Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic');
$mesesDato = array('mes1','mes2','mes3','mes4','mes5','mes6','mes7','mes8','mes9','mes10','mes11','mes12');


$nivel1 =  $_POST['nivel1'];
$nivel2 =  $_POST['nivel2']; 
$nivel3 = $_POST['nivel3'];
$nivel4 =  $_POST['nivel4'];
$nivel5 = $_POST['nivel5'];
$nivelTituloN1 = $_POST['nivelTituloN1'];
$nivelTituloN2 = $_POST['nivelTituloN2'];
$nivelTituloN3 = $_POST['nivelTituloN3'];
$nivelTituloN4 = $_POST['nivelTituloN4'];
$nivelTituloN5 = $_POST['nivelTituloN5'];
$mercado =  $_POST['mercado'];
$etapa = $_POST['etapa'];
$fechaDesde = $_POST['fechaDesde'];
$fechaHasta = $_POST['fechaHasta'];
$tipoDato = $_POST['tipoDato'];

$mesDesde = "";
$mesHasta = "";

$añoDesde = "";
$añoHasta = "";
//01-12-2016 date("Y");
//30-11-2017

//ECHO $fechaDesde."<BR>";
//ECHO $fechaHasta."<BR>";

$añoDesde = substr($fechaDesde ,6,4);
$añoHasta = substr($fechaHasta ,6,4);

//echo $añoDesde;
//echo $añoHasta;


$mesDesde = substr($fechaDesde ,3,2);
$mesHasta = substr($fechaHasta ,3,2);

$mesesTitulo[(int)$mesDesde];
$mesesTitulo[(int)$mesHasta];


//ECHO $mesesTitulo[date("m", $fechaDesde)]."<BR>";
//ECHO $mesesTitulo[date("m", $fechaHasta)]."<BR>";

$tipoTitulo = "";
$mercadoTitulo = "";


switch ($etapa) {

    case 1:

       $tipoTitulo = 'EXPO';

        break;

    case 2:

        $tipoTitulo = 'IMPO';

        break;

}

switch ($mercado) {

    case 1:

       $mercadoTitulo = 'CHILE';

        break;

    case 2:

        $mercadoTitulo = 'PERU';

        break;
	case 3:

        $mercadoTitulo = 'ECUADOR';

        break;
	case 4:

        $mercadoTitulo = 'COLOMBIA';

        break;

}



$rangoTitulo = $mesesTitulo[(int)$mesDesde]." ".$añoDesde. " - " . $mesesTitulo[(int)$mesHasta]." ". $añoHasta;
$valoresTitulo = 'Valores En: '. $tipoDato;



// Crea un nuevo objeto PHPExcel
$objPHPExcel = new PHPExcel();


function retonarFecha($añoDesde, $añoHasta, $mesDesde, $mesHasta){
	
	$arregloMeses = array();
	$arregloM = array("","01","02","03","04","05","06","07","08","09","10","11","12");
	//echo $mesHasta;
	$fechaFN = $añoDesde.$mesDesde;
	$fechaFNTotal = $añoDesde."12";
	//$fechaFN = "201611";
	//$diferenciaMesDesde  = 0;
	$diferenciaMesDesde =  ($fechaFNTotal - (int)$fechaFN ) ;
	
	$total = ((int)$diferenciaMesDesde + 1) + (int)$mesDesde;
	//echo $total;
	//echo date('m',(date('Y-m', strtotime($fechaDesde)) - date('Y-m', strtotime($fechaDesdeFn))));

	//echo $mesDesde;
	
	//$diferenciaMesDesde =  date('m',date('Y-m', strtotime($fechaDesde))-date('Y-m', strtotime($fechaDesdeFn)));
	//echo $diferenciaMesDesde;
	for ($i = $mesDesde; $i < $total; $i++) {

		array_push($arregloMeses,$añoDesde."-".$arregloM[$i]);
		

	}
	for ($j = 1; $j <= (int)$mesHasta; $j++) {
		
		$j+1;
		array_push($arregloMeses,$añoHasta."-".$arregloM[$j]);
		
	}
	return $arregloMeses;
}

$arregloPrueba = array();

$arregloPrueba = retonarFecha($añoDesde, $añoHasta, $mesDesde, $mesHasta);
//print_r($arregloPrueba);

function cellColor($cells,$color){
        global $objPHPExcel;
        $objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()
        ->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array('rgb' => $color)
        ));
}

// Elimina las gridlines de la hoja de calculo
$objPHPExcel->getActiveSheet()->setShowGridlines(false); 
$objPHPExcel->getActiveSheet()->setPrintGridlines(false); 

//Preparando encabezado de la hoja de calculo
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20); 


// Agrega valores y da formato a los titulos de la hoja de calculo
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B2', $tipoTitulo);
$objPHPExcel->getActiveSheet()->getStyle('B2')->getFont()->setSize(18);
$objPHPExcel->getActiveSheet()->getStyle('B2')->getFont()->setName('CALIBRI');
$objPHPExcel->getActiveSheet()->getStyle('B2')->getFont()->getColor()->setRGB('000000');

$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B3', $mercadoTitulo);
$objPHPExcel->getActiveSheet()->getStyle('B3')->getFont()->setSize(16);
$objPHPExcel->getActiveSheet()->getStyle('B3')->getFont()->setName('CALIBRI');
$objPHPExcel->getActiveSheet()->getStyle('B3')->getFont()->getColor()->setRGB('FF6600');

$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B4', $rangoTitulo);
$objPHPExcel->getActiveSheet()->getStyle('B4')->getFont()->setSize(13);
$objPHPExcel->getActiveSheet()->getStyle('B4')->getFont()->setName('CALIBRI');
$objPHPExcel->getActiveSheet()->getStyle('B4')->getFont()->getColor()->setRGB('808080');

$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B5', $valoresTitulo);
$objPHPExcel->getActiveSheet()->getStyle('B5')->getFont()->setSize(10);
$objPHPExcel->getActiveSheet()->getStyle('B5')->getFont()->setName('CALIBRI');
$objPHPExcel->getActiveSheet()->getStyle('B5')->getFont()->getColor()->setRGB('000000');

//Agrupa y da formato a las celdas de los niveles
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B7:B8');
$borde = array(
    'borders' => array(
        'top' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('argb' => '72A8ED'), 
            ),  
        'right' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('argb' => 'FFFFFF'), 
            ), 
        'bottom' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('argb' => '72A8ED'), 
            ), 
        'left' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('argb' => '72A8ED'), 
            ), 
        ), 
    ); 
$objPHPExcel->getActiveSheet()->getStyle('B7:B8')->applyFromArray($borde);
// cellColor('B7','72A8ED');


//Verifica cuantos niveles fueron consultados para llenar la cabezera
if($nivelTituloN2 != ''){
    $objPHPExcel->setActiveSheetIndex(0)->getCell('B7')->setValue($nivelTituloN1 . "\n        " . $nivelTituloN2);  
}else{
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B7', $nivelTituloN1);
}




//resto 1 semana
$totalMeses = "";
//echo strtotime($fechaDesde)-strtotime($fechaHasta);
$totalMeses =  count($arregloPrueba);

//$totalMeses = count($filtroMeses);
//$yDesde = substr($fDesde,6,9);
//$yHasta = substr($fHasta,6,9);




// Verifica si la consulta se hizo dentro de un mismo año o si abarca dos años. Esto para llenar la cabezera
if($añoDesde == $añoHasta){
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C7', $añoDesde);
}else{
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C7', $añoDesde . ' - ' . $añoHasta);    
}


// Mezcla y da formato a la celda que indica el valor de los años en la consulta
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('C7:'.$letras[$totalMeses+2].'7');
$borde = array(
    'borders' => array(
        'top' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('argb' => '72A8ED'), 
            ),  
        'right' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('argb' => 'FFFFFF'), 
            ), 
        'bottom' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('argb' => 'FFFFFF'), 
            ), 
        'left' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('argb' => 'FFFFFF'), 
            ), 
        ), 
    ); 
$objPHPExcel->getActiveSheet()->getStyle('C7:'.$letras[$totalMeses+2].'7')->applyFromArray($borde);


// Rellena la cabezera con los valores TOTAL, TOTAL ANT. y DIF.
$objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras[$totalMeses+3].'7', 'TOTAL'); 
$objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras[$totalMeses+4].'7', 'TOTAL ANT.'); 
$objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras[$totalMeses+5].'7', 'DIF.'); 

$objPHPExcel->setActiveSheetIndex(0)->mergeCells($letras[$totalMeses+5].'7:'.$letras[$totalMeses+5].'8');

// Centra la celda TOTAL para dejarlas bajo el formato
$objPHPExcel->setActiveSheetIndex(0)->mergeCells($letras[$totalMeses+3].'7:'.$letras[$totalMeses+3].'8');
$borde = array(
    'borders' => array(
        'top' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('argb' => '72A8ED'), 
            ),  
        'right' => array(
            'style' => PHPExcel_Style_Border::BORDER_HAIR, 
            'color' => array('argb' => '72A8ED'), 
            ), 
        'bottom' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('argb' => '72A8ED'), 
            ), 
        'left' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('argb' => 'FFFFFF'), 
            ), 
        ), 
    ); 
$objPHPExcel->getActiveSheet()->getStyle($letras[$totalMeses+3].'7:'.$letras[$totalMeses+3].'8')->applyFromArray($borde);


// Centra la celda TOTAL ANT. para dejarlas bajo el formato
$objPHPExcel->setActiveSheetIndex(0)->mergeCells($letras[$totalMeses+4].'7:'.$letras[$totalMeses+4].'8');
$borde = array(
    'borders' => array(
        'top' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('argb' => '72A8ED'), 
            ),  
        'right' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('argb' => 'FFFFFF'), 
            ), 
        'bottom' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('argb' => '72A8ED'), 
            ), 
        'left' => array(
            'style' => PHPExcel_Style_Border::BORDER_HAIR, 
            'color' => array('argb' => '72A8ED'), 
            ), 
        ), 
    ); 
$objPHPExcel->getActiveSheet()->getStyle($letras[$totalMeses+4].'7:'.$letras[$totalMeses+4].'8')->applyFromArray($borde);
$borde = array(
    'borders' => array(
        'top' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('argb' => '72A8ED'), 
            ),  
        'right' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('argb' => '72A8ED'), 
            ), 
        'bottom' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('argb' => '72A8ED'), 
            ), 
        'left' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('argb' => 'FFFFFF'), 
            ), 
        ), 
    ); 
$objPHPExcel->getActiveSheet()->getStyle($letras[$totalMeses+5].'7:'.$letras[$totalMeses+5].'8')->applyFromArray($borde);

//$meses = "2013-12/2014-01/2014-02/2014-03/2014-04/2014-05/2014-06/2014-07/2014-08/2014-09/2014-10/2014-11";
//$filtroMeses = explode("/", $meses);

$arregloPrueba = array();

$arregloPrueba = retonarFecha($añoDesde, $añoHasta, $mesDesde, $mesHasta);

//print_r($filtroMeses);

// Centra y da valor a las celdas de los meses
for($i = 0 ; $i < $totalMeses ; $i++){
    $mesActual = $mesesCabezera[(int)substr($arregloPrueba[$i],5,2)];
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras[$i+3].'8', $mesActual);
}




// Se da formato a la cabecera
$borde = array(
    'borders' => array(
        'vertical' => array(
            'style' => PHPExcel_Style_Border::BORDER_HAIR, 
            'color' => array('argb' => '72A8ED'), 
            ),  
        ), 
    ); 
$objPHPExcel->getActiveSheet()->getStyle('C8:'.$letras[$totalMeses+2].'8')->applyFromArray($borde);
$objPHPExcel->getActiveSheet()->getStyle('B7:'.$letras[$totalMeses+5].'8')->getAlignment()->applyFromArray(array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('C7:'.$letras[$totalMeses+5].'8')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('B7:'.$letras[$totalMeses+5].'8')->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('B7:'.$letras[$totalMeses+5].'8')->getFont()->setSize(9);
$objPHPExcel->getActiveSheet()->getStyle('B7:'.$letras[$totalMeses+5].'8')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('B7:'.$letras[$totalMeses+5].'8')->getFont()->setName('CALIBRI');
$objPHPExcel->getActiveSheet()->getStyle('B7:'.$letras[$totalMeses+5].'8')->getFont()->getColor()->setRGB('FFFFFF');
cellColor('B7:'.$letras[$totalMeses+5].'8','72A8ED');



// consulta que limpia la tabla antes de insertar los datos
$objDeclaracion->EliminarConsultaUsuario($usuarioId);


//consulta que obtine el top 20
$resultado = $objDeclaracion->traerTop20Nivel1($nivel1,$usuarioId, $mesDesde, $mesHasta, $añoDesde, $añoHasta);
$arregloNivel1 = array();
$arregloNivel2 = array();
while($row = sql_db::sql_fetch_assoc($resultado)){
		array_push($arregloNivel1,$row['nivel1']);
}



$cantidad = count($arregloNivel1);


for ($p = 0; $p < $cantidad ; $p++) {
	
	 $objDeclaracion->generarConsultaExcel($nivel1,$usuarioId,$arregloNivel1[$p],str_replace("-", "", $arregloPrueba),$nivel1,$nivel2);
	 //$objDeclaracion->generarConsultaExcel($nivel1,$usuarioId,4330,str_replace("-", "", $arregloPrueba));
	 $result02 = $objDeclaracion->traerTopNivel2($usuarioId, $mesDesde, $mesHasta, $añoDesde, $añoHasta,$arregloNivel1[$p],$nivel1,$nivel2);
	 //$result02 = $objDeclaracion->traerTopNivel2($usuarioId, $mesDesde, $mesHasta, $añoDesde, $añoHasta,4330);
	//consulta que obtine el top 20 nivel2  4330
	while($row03 = sql_db::sql_fetch_assoc($result02)){
			
		array_push($arregloNivel2,$row03['nivel2']);
				
	}
	$cantidad2 = count($arregloNivel2);
	
	for($h=0; $h < $cantidad2; $h++){
		$objDeclaracion->generarConsultaExcelNivel2($nivel1,$nivel2,$usuarioId,$arregloNivel1[$p],$arregloNivel2[$h],str_replace("-", "", $arregloPrueba));
		//$objDeclaracion->generarConsultaExcelNivel2($nivel1,$nivel2,$usuarioId,4330,$arregloNivel2[$h],str_replace("-", "", $arregloPrueba));
	}
	$objDeclaracion->insertarOtrosNivel2($nivel1,$usuarioId,$arregloNivel1[$p],str_replace("-", "", $arregloPrueba),$nivel2);
	//$objDeclaracion->insertarOtrosNivel2($nivel1,$usuarioId,4330,str_replace("-", "", $arregloPrueba));
	$arregloNivel2 = array();
	
		
}

$objDeclaracion->insertarOtrosExcel($usuarioId, $mesDesde, $mesHasta, $añoDesde, $añoHasta,str_replace("-", "", $arregloPrueba));
$objDeclaracion->insertarTotalExcel($usuarioId, $mesDesde, $mesHasta, $añoDesde, $añoHasta,str_replace("-", "", $arregloPrueba));
$objDeclaracion->EliminarRegistrosCon0($usuarioId);

//consulta que trae los datos para el excel



$totalFilas = $cantidad;
$fila = 9; //Fila donde se comienza a llenar los datos

$bandera = true;    //Indica que se esta en nivel 1
$n2Inicio = '';     //Inicio de celdas de 2do nivel
$n2Fin = '';        //Fin de celdas de 2do nivel
$mod3 = 1;          //Contador que salta cada 3 valores.

$tablaDatos = $objDeclaracion->obtenerConsultaUsuario($usuarioId);



while($row2 = sql_db::sql_fetch_assoc($tablaDatos)){
	
	
	
	    if($row2['nivel1'] == '1'){
			
		// Si bandera es false, significa que antes de eso se termina el nivel 2, por lo tanto agrupa y formatea 
        // el nivel dos de la fila anterior.
				if(!$bandera){
					
					$n2Fin = $fila - 1;
					$objPHPExcel->getActiveSheet()->getStyle('B' . $n2Inicio . ':B' . $n2Fin)->getAlignment()->setIndent(1);
					$objPHPExcel->getActiveSheet()->getStyle($letras[$totalMeses+5] . $n2Inicio . ':' . $letras[$totalMeses+5] . $n2Fin)->getNumberFormat()->setFormatCode('0.00%');
					$objPHPExcel->getActiveSheet()->getStyle($letras[$totalMeses+3] . $n2Inicio . ':' . $letras[$totalMeses+5] . $n2Fin)->getFont()->setBold(true);
					$objPHPExcel->getActiveSheet()->getStyle('C' . $n2Inicio . ':' . $letras[$totalMeses+4] . $n2Fin)->getNumberFormat()->setFormatCode('#,##0');
					$objPHPExcel->getActiveSheet()->getStyle('B' . $n2Inicio . ':' . $letras[$totalMeses+5] . $n2Fin)->getFont()->setItalic(true);
					$objPHPExcel->getActiveSheet()->getStyle('B' . $n2Inicio . ':' . $letras[$totalMeses+5] . $n2Fin)->getFont()->setName('CALIBRI');
					$objPHPExcel->getActiveSheet()->getStyle('B' . $n2Inicio . ':' . $letras[$totalMeses+5] . $n2Fin)->getFont()->setSize(9);
					$borde = array(
						'borders' => array(
							'top' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN, 
								'color' => array('argb' => '72A8ED'), 
								),
							'right' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN, 
								'color' => array('argb' => '72A8ED'), 
								),
							'bottom' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN, 
								'color' => array('argb' => '72A8ED'), 
								), 
							'left' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN, 
								'color' => array('argb' => '72A8ED'), 
								), 
							), 
						);
					$objPHPExcel->getActiveSheet()->getStyle('B' . $n2Inicio . ':' . $letras[$totalMeses+5] . $n2Fin)->applyFromArray($borde);
					cellColor('B' . $n2Inicio . ':' . $letras[$totalMeses+5] . $n2Fin,'F5F5F5');

					$bandera = true;
				
				
				}
				        // celda que contiene el nombre del nivel 1
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B' . $fila, $row2['campo1']);
				$columna = 3; // Columna donde se comienza a llenar los datos
				for($x = 0 ; $x < $totalMeses ; $x++) {

					// valores por meses en el nivel 1.
					if($row2[$mesesDato[$x]]){
						$objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras[$columna] . $fila, $row2[$mesesDato[$x]]);
						
						$columna++; 

					}elseif($x < $totalMeses){
						$objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras[$columna] . $fila, 0);

						$columna++;

					}else{
						break;
					}
				}
				// celda que contiene el total del nivel 1
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras[$columna] . $fila, $row2['total1']);

				// celda que contiene el total anterior del nivel 1
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras[$columna+1] . $fila, $row2['totalAnterior']);

				// Calcula diferencia porcentual entre TOTAL y TOTAL ANT en el nivel 1
				if($row2['totalAnterior'] != 0){
					$diferencia = ($row2['total1'] / $row2['totalAnterior']) - 1;    
				}else{
					$diferencia = 1;
				}
				
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras[$columna+2] . $fila, $diferencia);
				if($diferencia >= 0){ // Verifica si es positivo para dar cierto formato
					$objPHPExcel->getActiveSheet()->getStyle($letras[$columna+2] . $fila)->getFont()->getColor()->setRGB('0000FF');
				}else{ // En caso de ser negativo, entrega otro formato
					$objPHPExcel->getActiveSheet()->getStyle($letras[$columna+2] . $fila)->getFont()->getColor()->setRGB('FF0000');
				}
				 // Da formato al nivel 1 actual
				$objPHPExcel->getActiveSheet()->getStyle('B' . $fila . ':' . $letras[$columna+2] . $fila)->getFont()->setSize(9);
				$objPHPExcel->getActiveSheet()->getStyle('B' . $fila . ':' . $letras[$columna+2] . $fila)->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->getStyle('B' . $fila . ':' . $letras[$columna+2] . $fila)->getFont()->setName('CALIBRI');
				$objPHPExcel->getActiveSheet()->getStyle('B' . $fila . ':' . $letras[$columna+1] . $fila)->getFont()->getColor()->setRGB('000000');
				$objPHPExcel->getActiveSheet()->getStyle('C' . $fila . ':' . $letras[$columna+1] . $fila)->getNumberFormat()->setFormatCode('#,##0');
				$objPHPExcel->getActiveSheet()->getStyle($letras[$columna+2] . $fila)->getNumberFormat()->setFormatCode('0.00%');
				$borde = array(
					'borders' => array(
						'top' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN, 
							'color' => array('argb' => '72A8ED'), 
							),
						'right' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN, 
							'color' => array('argb' => '72A8ED'), 
							),
						'bottom' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN, 
							'color' => array('argb' => '72A8ED'), 
							), 
						'left' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN, 
							'color' => array('argb' => '72A8ED'), 
							), 
						), 
					); 
				$objPHPExcel->getActiveSheet()->getStyle('B' . $fila . ':' . $letras[$columna+2] . $fila)->applyFromArray($borde);
				if($row2['orden'] == 0){ //Debe ser coloreado de otra forma
					cellColor('B' . $fila . ':' . $letras[$columna+2] . $fila ,'B8CFEB');
				}
				
				$fila++;
		

				
		
		
		}else{
			
			if($bandera){
            $n2Inicio = $fila;
            $bandera = false;
            $mod3 = 1;
			}
			// Da valor y formatea la celda que contiene el nombre del nivel 2
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B' . $fila, $row2['campo2']);
			
			$columna = 3; // Columna donde se comienza a llenar los datos
			for($i = 0 ; $i < $totalMeses ; $i++) {

				// Rellena y da formato a los valores por meses en el nivel 2.
				if($row2[$mesesDato[$i]]){
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras[$columna] . $fila, $row2[$mesesDato[$i]]);

					$columna++; 

				}elseif($i < $totalMeses){
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras[$columna] . $fila, 0);

					$columna++;

				}else{
					break;
				}
			}
			
			// Rellena y da formato a la celda que contiene el total del nivel 2
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras[$columna] . $fila, $row2['total2']);

			// Rellena y da formato a la celda que contiene el total anterior del nivel 1
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras[$columna+1] . $fila, $row2['totalAnterior']);

			// Calcula y formatea diferencia porcentual entre TOTAL y TOTAL ANT en el nivel 1
			if($row2['totalAnterior'] != 0){
				$diferencia = ($row2['total2'] / $row2['totalAnterior']) - 1;    
			}else{
				$diferencia = 1;
			}
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras[$columna+2] . $fila, $diferencia);
		   
			if($diferencia >= 0){ // Verifica si es positivo para dar cierto formato
				$objPHPExcel->getActiveSheet()->getStyle($letras[$columna+2] . $fila)->getFont()->getColor()->setRGB('0000FF');
			}else{ // En caso de ser negativo, entrega otro formato
				$objPHPExcel->getActiveSheet()->getStyle($letras[$columna+2] . $fila)->getFont()->getColor()->setRGB('FF0000');
			}
			  // Cada 3 filas, se agrega una linea divisoria
			if($mod3 == 3){

				$borde = array(
					'borders' => array(
						'bottom' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN, 
							'color' => array('argb' => '72A8ED'), 
							), 
						), 
					); 
				$objPHPExcel->getActiveSheet()->getStyle('B' . $fila . ':' . $letras[$totalMeses+5] . $fila)->applyFromArray($borde);

				$mod3 = 0;
			}

			$objPHPExcel->getActiveSheet()->getRowDimension($fila)->setOutlineLevel(1);
			$objPHPExcel->getActiveSheet()->getRowDimension($fila)->setVisible(false);

			$mod3++;
			$fila++;

			
			
			
		}


}
	
$objPHPExcel->getActiveSheet()->setShowSummaryBelow(false);

// Se le agrega el formato final de las columnas
$borde = array(
    'borders' => array(
        'left' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('argb' => '72A8ED'), 
            ), 
        'right' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('argb' => '72A8ED'), 
            ), 
        ), 
    ); 
$objPHPExcel->getActiveSheet()->getStyle('C9:' . $letras[$totalMeses+4] . ($fila-1))->applyFromArray($borde);
$borde = array(
    'borders' => array(
        'vertical' => array(
            'style' => PHPExcel_Style_Border::BORDER_HAIR, 
            'color' => array('argb' => '72A8ED'), 
            ), 
        ), 
    ); 
$objPHPExcel->getActiveSheet()->getStyle('C9:' . $letras[$totalMeses+2] . ($fila-1))->applyFromArray($borde);
$borde = array(
    'borders' => array(
        'left' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN, 
            'color' => array('argb' => '72A8ED'), 
            ), 
        'right' => array(
            'style' => PHPExcel_Style_Border::BORDER_HAIR, 
            'color' => array('argb' => '72A8ED'), 
            ), 
        ), 
    ); 
$objPHPExcel->getActiveSheet()->getStyle($letras[$totalMeses+3] . '9:' . $letras[$totalMeses+3] . ($fila-1))->applyFromArray($borde);
	
	





// Renombrar Hoja
$objPHPExcel->getActiveSheet()->setTitle('REPORTE SEMPAT');
 
// Establecer la hoja activa, para que cuando se abra el documento se muestre primero.
$objPHPExcel->setActiveSheetIndex(0);
 
// redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="01simple.xls"');
header('Cache-Control: max-age=0');




// Verifica los directorios para guardar los archivos existen y los crea en caso de no existir

$uploaddir = "../../ReportesExcel/declaraciones";
            
if (file_exists($uploaddir)==false) {
    $mask = umask(0);
    mkdir($uploaddir, 0777);
    umask($mask);
}

$uploaddir = $uploaddir . "/" . $usuarioId . "/";

if (file_exists($uploaddir)==false) {
    $mask = umask(0);
    mkdir($uploaddir, 0777);
    umask($mask);
}

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save($uploaddir . 'ReporteSempat.xls');
//$objWriter->save('php://output');
exit;

?>