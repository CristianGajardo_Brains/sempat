<?
session_start();


$idioma = "";
$labPanelBusqueda = "PANEL DE BUSQUEDA";
$labFechas = "FECHAS";
$labFechaInicio = "Inicio";
$labFechaFin = "Fin";
$labDatos = "DATOS";
$labNcontenedores = "N° de Contenedores";
$labToneladas = "Toneladas";
$labImpo = "Impo";
$labExpo = "Expo";
$labContenedor = "Contenedor";
$labSeca = "Seca";
$labRefrigerada = "Refrigerada";
$labFob = "FOB (Miles USD)";
$labFlete = "Flete (Miles USD)";
$labNiveles = "NIVELES";
$labNivel = "Nivel";
$labEtapa = "Etapa";
$labFiltros = "FILTROS";
$labBuscar = "BUSCAR";
$mensajeNivel1 = "Debe seleccionar el primer nivel.";
$labGraficos = "GRÁFICO";
$grLineas = "Líneas";
$grBarras = "Barras";
$grPorcentaje = "Porcentaje";
$labTipoCnt = "TIPO CNT.";
$labAmbos = "Ambos";
$labCargando = "Cargando";
$labFFWW = "Sólo carga forwarder";
$labNombreArchivo = "NOMBRE DEL ARCHIVO";
$labFiltrosCampos = "FILTROS Y CAMPOS DE SALIDA";
$labTodos = "Todos";
$labExtracciones = "EXTRACCIONES";
$labNombreArchivo2 = "NOMBRE ARCHIVO";
$labFecha = "FECHA";
$btnGenerarReporte = "GENERAR REPORTE";

$acumulado = "Acumulado del año";
$meses = "Últimos 12 meses";
$mes = "Mes actual";

$OTROS = "OTROS";


if($clienteId == "41")
{
	$idioma = "EN";
}

if($idioma == "EN"){
	$labPanelBusqueda = "SEARCH PANEL";
	$labFechas = "DATES";
	$labFechaInicio = "Start";
	$labFechaFin = "End";
	$labDatos = "DATA";
	$labNcontenedores = "N° Containers";
	$labToneladas = "Tons";
	$labImpo = "Import";
	$labExpo = "Export";
	$labContenedor = "Container";
	$labSeca = "Dry";
	$labRefrigerada = "Reefer";
	$labFob = "FOB (USD)";
	$labFlete = "Freight (USD)";
	$labNiveles = "LEVELS";
	$labNivel = "Level";
	$labEtapa = "Bound";
	$labFiltros = "FILTERS";
	$labBuscar = "SEARCH";
	$mensajeNivel1 = "You must select the first level.";
	$labGraficos = "CHARTS";
	$grLineas = "Lines";
	$grBarras = "Bars";
	$grPorcentaje = "Percentage";
	$labTipoCnt = "CNT. TYPE";
	$labAmbos = "All";
	$labCargando = "Loading";
	$labFFWW = "Only forwarder cargoes";
	$labNombreArchivo = "FILE NAME";
	$labFiltrosCampos = "FILTERS AND FIELDS OF DEPARTURE";
	$labTodos = "All";
	$labExtracciones = "REPORTS";
	$labNombreArchivo2 = "FILE NAME";
	$labFecha = "DATE";
	$btnGenerarReporte = "CREATE REPORT";
	$acumulado = "Year to date";
	$meses = "Moving annual total";
	$mes = "Current month";
	$OTROS = "OTHERS";

}









?>