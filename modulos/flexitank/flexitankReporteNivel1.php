<?PHP

session_start();
include ("../../librerias/conexion.php");
require('flexitank.class.php');
require('../../clases/sempat.class.php');
$objFlexitank = new flexitank();
$objSempat= new sempat();


$usuarioId = $_SESSION['SEMPAT_usuarioId'];
$clienteId = $_SESSION['SEMPAT_clienteId'];

$fechaDesde = mb_convert_encoding(trim($_POST['fechaDesde']), "ISO-8859-1", "UTF-8");
$fechaHasta = mb_convert_encoding(trim($_POST['fechaHasta']), "ISO-8859-1", "UTF-8");

$nivel1 = mb_convert_encoding(trim($_POST['nivel1']), "ISO-8859-1", "UTF-8");
$nivel2 = mb_convert_encoding(trim($_POST['nivel2']), "ISO-8859-1", "UTF-8");
$nivel3 = mb_convert_encoding(trim($_POST['nivel3']), "ISO-8859-1", "UTF-8");
$nivel4 = mb_convert_encoding(trim($_POST['nivel4']), "ISO-8859-1", "UTF-8");
$nivel5 = mb_convert_encoding(trim($_POST['nivel5']), "ISO-8859-1", "UTF-8");

$nivel1Titulo = mb_convert_encoding(trim($_POST['nivel1Titulo']), "ISO-8859-1", "UTF-8");

$tipoDato = mb_convert_encoding(trim($_POST['tipoDato']), "ISO-8859-1", "UTF-8");

$aduana = mb_convert_encoding(trim($_POST['aduana']), "ISO-8859-1", "UTF-8");
$clausula = mb_convert_encoding(trim($_POST['clausula']), "ISO-8859-1", "UTF-8");
$exportador = mb_convert_encoding(trim($_POST['exportador']), "ISO-8859-1", "UTF-8");
$mercaderia = mb_convert_encoding(trim($_POST['mercaderia']), "ISO-8859-1", "UTF-8");
$moneda = mb_convert_encoding(trim($_POST['moneda']), "ISO-8859-1", "UTF-8");
$nave = mb_convert_encoding(trim($_POST['nave']), "ISO-8859-1", "UTF-8");
$naviera = mb_convert_encoding(trim($_POST['naviera']), "ISO-8859-1", "UTF-8");
$paisDestino = mb_convert_encoding(trim($_POST['paisDestino']), "ISO-8859-1", "UTF-8");
$puertoDescarga = mb_convert_encoding(trim($_POST['puertoDescarga']), "ISO-8859-1", "UTF-8"); 
$puertoEmbarque = mb_convert_encoding(trim($_POST['puertoEmbarque']), "ISO-8859-1", "UTF-8");
$tipoBulto = mb_convert_encoding(trim($_POST['tipoBulto']), "ISO-8859-1", "UTF-8");
$tipoCarga = mb_convert_encoding(trim($_POST['tipoCarga']), "ISO-8859-1", "UTF-8");
$tipoOperacion = mb_convert_encoding(trim($_POST['tipoOperacion']), "ISO-8859-1", "UTF-8");
$trafico = mb_convert_encoding(trim($_POST['trafico']), "ISO-8859-1", "UTF-8");
$viaTransporte = mb_convert_encoding(trim($_POST['viaTransporte']), "ISO-8859-1", "UTF-8");


$aduana = str_replace("\'", "'", $aduana);
$clausula = str_replace("\'", "'", $clausula);
$exportador = str_replace("\'", "'", $exportador);
$mercaderia = str_replace("\'", "'", $mercaderia);
$moneda = str_replace("\'", "'", $moneda);
$nave = str_replace("\'", "'", $nave);
$naviera = str_replace("\'", "'", $naviera);
$paisDestino = str_replace("\'", "'", $paisDestino);
$puertoDescarga = str_replace("\'", "'", $puertoDescarga);
$puertoEmbarque = str_replace("\'", "'", $puertoEmbarque);
$tipoBulto = str_replace("\'", "'", $tipoBulto);
$tipoCarga = str_replace("\'", "'", $tipoCarga);
$tipoOperacion = str_replace("\'", "'", $tipoOperacion);
$trafico = str_replace("\'", "'", $trafico);
$viaTransporte = str_replace("\'", "'", $viaTransporte);

$consultaTabla = $objFlexitank->flexitankReporteNivel1($fechaDesde, $fechaHasta, $nivel1, $nivel2, $nivel3, $nivel4, $nivel5, $tipoDato, $clienteId, $aduana, $clausula, $exportador, $mercaderia, $moneda, $nave, $naviera, $paisDestino, $puertoDescarga, $puertoEmbarque, $tipoBulto, $tipoCarga, $tipoOperacion, $trafico, $viaTransporte, $usuarioId);


$meses = array();

$mesInicial = substr($fechaDesde, 3, 2);
$añoInicial = substr($fechaDesde, 6, 4);

$mesFinal = substr($fechaHasta, 3, 2);
$añoFinal = substr($fechaHasta, 6, 4);

for ($a = 0; $a < 12; $a++) {

    $mes = "00" . ($mesInicial);
    $mes = substr($mes, strlen($mes) - 2, strlen($mes));

    $meses[$a][0] = $añoInicial . "-" . $mes;

    if ($mes >= $mesFinal && $añoInicial >= $añoFinal) {                        
        $a = 12;                        
    }

    if ($mes == 12) {
        $añoInicial++;
        $mesInicial = "01";
    } else {
        $mesInicial++;
    }        
    
}


$cabeceraHTML = "";
$cabeceraHTML2 = "<tr><td colspan='2' class='left' style='width:185px;'>" . htmlentities(strtoupper($nivel1Titulo)) . "</td>";
$colspan=5;

for ($i = 0; $i < count($meses); $i++) {
    $cabeceraHTML2 .= "<td>" . $meses[$i][0] . "</td>";
    $colspan++;
}


$cabeceraHTML = "<table id='tablaReporte' class='tablaValores' cellspacing='0' cellspadding='0'>
                    <thead>
                        <tr>            
                            <th class='thCenter' colspan='" . $colspan . "'>
                                <div class='divThLeft'></div>          
                                <div class='divThCenter'>" . tituloCabecera($fechaDesde, $fechaHasta, 2) . "</div>
                                <div class='divThRight'></div>          
                            </th>                                                
                        </tr>" . $cabeceraHTML2 . "<td>TOTAL</td><td>TOTAL ANT.</td><td>DIF.</td> </thead>";

$campo1 = "";
$valorCampo1 = "";
$salida = "";
$salidaOtros = "";
$total = 0;
$totalAnterior = 0;

$fila = 0;

$cursor = ""; 

if($nivel2 != ""){
    $cursor = " style='cursor:pointer'";
}


if ($consultaTabla) {
    
    if(mssql_num_fields($consultaTabla) > 1){
    
        $numFilas = mssql_num_rows($consultaTabla);

        while ($manifiesto = mssql_fetch_array($consultaTabla)) {

            $fila++;

            $campo1 = trim(htmlentities($manifiesto[1]));

            if(strlen($campo1) > 24){
                $valorCampo1 = substr($campo1, 0, 19) . "...";
            }
            else{
                $valorCampo1 = $campo1;
            }

            if($campo1 != "OTROS"){

                $salida .= "<tr id='tr-". $fila ."' class='nivel1'><td class='check'><input class='styled' type='checkbox' onClick='graficoCheckboxReporte(this);'></td><td class='nivel1' " . $cursor . " title =\"" . $campo1 . "\" onClick='buscarNivel2(this, \"" . $fechaDesde . "\", \"" . $fechaHasta . "\", \"" . $nivel2 . "\", \"" . $nivel3 . "\", \"" . $nivel4 . "\", \"" . $nivel5 . "\", \"" . $tipoDato . "\", \"" . $manifiesto[2] . "\");'>" . $valorCampo1 . "</td>";        

                for ($i = 0; $i < count($meses); $i++) {

                    $salida .= "<td>" . number_format($manifiesto[3 + $i], 0, '', '.') . "</td>";
                    $meses[$i][$fila] .= number_format($manifiesto[3 + $i], 0, '', '');
                }

                $salida .= "<td><b>" . number_format($manifiesto[16], 0, '', '.') . "</b></td><td><b>" . number_format($manifiesto[15], 0, '', '.') . "</b></td>";                                    
                $salida .= $objSempat->calculoDiferencia($manifiesto[16], $manifiesto[15]) . "</tr>";

                $total = $total + number_format($manifiesto[16], 0, '', '');
                $totalAnterior = $totalAnterior + number_format($manifiesto[15], 0, '', '');

            }
            else{

                if($manifiesto[16] != "0" && $manifiesto[16] != ""){
                    $salidaOtros .= "<tr id='tr-". ($numFilas + 1) ."' class='nivel1'><td class='check'><input type='checkbox' class='styled' onClick='graficoCheckboxReporte(this);'/></td><td class='nivel1' title =\"" . $campo1 . "\">" . $valorCampo1 . "</td>";        

                    for ($i = 0; $i < count($meses); $i++) {

                        $salidaOtros .= "<td>" . number_format($manifiesto[3 + $i], 0, '', '.') . "</td>";                                                            
                        $meses[$i][$numFilas + 1] .= number_format($manifiesto[3 + $i], 0, '', '');
                    }

                    $salidaOtros .= "<td><b>" . number_format($manifiesto[16], 0, '', '.') . "</b></td><td><b>" . number_format($manifiesto[15], 0, '', '.') . "</b></td>";
                    $salidaOtros .= $objSempat->calculoDiferencia($manifiesto[16], $manifiesto[15]) . "</tr>";

                    $total = $total + number_format($manifiesto[16], 0, '', '');
                    $totalAnterior = $totalAnterior + number_format($manifiesto[15], 0, '', '');

                }
            }
        }

        $salidaTotal = "<tr class='nivelTotal'><td class='check'></td><td class='nivelTotal'>TOTAL</td>";

        $htmlTablaGrafico = "<table id='tablaGrafico'><thead><tr><th></th></tr></thead><tbody>";


        for($x = 0; $x < count($meses); $x++){      

            $htmlTablaGrafico .= "<tr><th>" . retornarPeriodo($meses[$x][0]) . "</th>";

            $valor = "0";

            for ($i = 1; $i <= ($numFilas + 1); $i++) {            
                $valor = $valor + $meses[$x][$i];
            }

            $salidaTotal .= "<td>" .  number_format($valor, 0, '', '.')   . "</td>";
        }

        $salidaTotal.= "<td>" .  number_format($total, 0, '', '.')   . "</td><td>" .  number_format($totalAnterior, 0, '', '.')   . "</td>" . $objSempat->calculoDiferencia($total, $totalAnterior) . "</tr>";

        $bodyHTML = "<tbody id='tBody'>" . $salida . $salidaOtros . $salidaTotal ."</tbody>";


        $htmlTablaGrafico .= "</tr></tbody></table>";

        $htmlTablaGrafico .= "<table id=\"tablaActual\">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>

                            <table id=\"tablaAnterior\">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>";

                
        echo "<div id='divDatosGrafico' style='display:none'>" .  $htmlTablaGrafico . "</div>" . $cabeceraHTML . $bodyHTML . "</table>";

    }
        
}
else{
    echo "Sin resultado";
}




function tituloCabecera($fechaDesde, $fechaHasta, $tipo){
    
    $mesesA = array("", "ENE", "FEB", "MAR", "ABR", "MAY", "JUN", "JUL", "AGO", "SEP", "OCT", "NOV", "DIC");
    $mesesB = array("", "ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE");
    
    $mDesde = date("m", strtotime($fechaDesde));
    $yDesde = date("Y", strtotime($fechaDesde));
    
    $mHasta = date("m", strtotime($fechaHasta));
    $yHasta = date("Y", strtotime($fechaHasta));
    
    $titulo = "";        
    
    if($tipo == 1){        
        
        if($mDesde == $mHasta && $yDesde == $yHasta){
            $titulo = $mesesA[(int)$mDesde] . " " . $yDesde;        
        }
        else{
            $titulo = $mesesA[(int)$mDesde] . " " . $yDesde . " - " . $mesesA[(int)$mHasta] . " " . $yHasta;        
        }
        
    }
    
    if($tipo == 2){
        if($mDesde == $mHasta && $yDesde == $yHasta){
            $titulo = $mesesB[(int)$mDesde] . " " . $yDesde;  
        }
        else{
            $titulo = $mesesB[(int)$mDesde] . " " . $yDesde . " - " . $mesesB[(int)$mHasta] . " " . $yHasta;        
        }
        
    }
    
    return $titulo;
    
}


function retornarPeriodo($valor){    
    $meses = array("", "ENE", "FEB", "MAR", "ABR", "MAY", "JUN", "JUL", "AGO", "SEP", "OCT", "NOV", "DIC");
    
    $datos = split("-",$valor);
    
    return $meses[$datos[1]*1] . "-" . $datos[0];            
    
}

//echo $salida;


?>
