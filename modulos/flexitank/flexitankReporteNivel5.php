<?PHP

session_start();
include ("../../librerias/conexion.php");
require('flexitank.class.php');
require('../../clases/sempat.class.php');
$objFlexitank = new flexitank();
$objSempat= new sempat();



$usuarioId = $_SESSION['SEMPAT_usuarioId'];
$clienteId = $_SESSION['SEMPAT_clienteId'];

$fechaDesde = mb_convert_encoding(trim($_POST['fechaDesde']), "ISO-8859-1", "UTF-8");
$fechaHasta = mb_convert_encoding(trim($_POST['fechaHasta']), "ISO-8859-1", "UTF-8");

$nivel5 = mb_convert_encoding(trim($_POST['nivel5']), "ISO-8859-1", "UTF-8");

$tipoDato = mb_convert_encoding(trim($_POST['tipoDato']), "ISO-8859-1", "UTF-8");

$trId = mb_convert_encoding(trim($_POST['trId']), "ISO-8859-1", "UTF-8");


$nivel1Id = mb_convert_encoding(trim($_POST['nivel1Id']), "ISO-8859-1", "UTF-8");
$nivel2Id = mb_convert_encoding(trim($_POST['nivel2Id']), "ISO-8859-1", "UTF-8");
$nivel3Id = mb_convert_encoding(trim($_POST['nivel3Id']), "ISO-8859-1", "UTF-8");
$nivel4Id = mb_convert_encoding(trim($_POST['nivel4Id']), "ISO-8859-1", "UTF-8");
$otros = mb_convert_encoding(trim($_POST['otros']), "ISO-8859-1", "UTF-8");

$consultaTabla = $objFlexitank->flexitankReporteNivel5($fechaDesde, $fechaHasta, $nivel5, $tipoDato, $clienteId, $nivel1Id, $nivel2Id, $nivel3Id, $nivel4Id, $otros, $usuarioId);

$meses = array();

$mesInicial = substr($fechaDesde, 3, 2);
$añoInicial = substr($fechaDesde, 6, 4);

$mesFinal = substr($fechaHasta, 3, 2);
$añoFinal = substr($fechaHasta, 6, 4);

for ($a = 0; $a < 12; $a++) {

    $mes = "00" . ($mesInicial);
    $mes = substr($mes, strlen($mes) - 2, strlen($mes));

    $meses[$a][0] = $añoInicial . "-" . $mes;

    if ($mes >= $mesFinal && $añoInicial >= $añoFinal) {                        
        $a = 12;                        
    }

    if ($mes == 12) {
        $añoInicial++;
        $mesInicial = "01";
    } else {
        $mesInicial++;
    }        
    
}


$campo1 = "";
$valorCampo1 = "";
$salida = "";
$salidaOtros = "";
$total = 0;
$totalAnterior = 0;

$fila = 0;

if ($consultaTabla) {
    
    $numFilas = mssql_num_rows($consultaTabla);

    while ($manifiesto = mssql_fetch_array($consultaTabla)) {
        
        $fila++;

        $campo1 = trim(htmlentities($manifiesto[1]));

        if(strlen($campo1) > 16){
            $valorCampo1 = substr($campo1, 0, 14) . "...";
        }
        else{
            $valorCampo1 = $campo1;
        }

        if($campo1 != "OTROS"){

            $salida .= "<tr id='" . $trId . "-". $fila ."' class='nivel5'><td class='check'></td><td class='nivel5' title =\"" . $campo1 . "\" >" . $valorCampo1 . "</td>";        

            for ($i = 0; $i < count($meses); $i++) {

                $salida .= "<td>" . number_format($manifiesto[3 + $i], 0, '', '.') . "</td>";
                //$meses[$i][$fila] .= number_format($manifiesto[3 + $i], 0, '', '');
            }

            $salida .= "<td><b>" . number_format($manifiesto[16], 0, '', '.') . "</b></td><td><b>" . number_format($manifiesto[15], 0, '', '.') . "</b></td>";                                    
            $salida .= $objSempat->calculoDiferencia($manifiesto[16], $manifiesto[15]) . "</tr>";
            
            $total = $total + number_format($manifiesto[16], 0, '', '');
            $totalAnterior = $totalAnterior + number_format($manifiesto[15], 0, '', '');
                        
        }
        else{
            
            if($manifiesto[16] != "0" && $manifiesto[16] != ""){
                $salidaOtros .= "<tr id='" . $trId . "-". ($numFilas + 1) ."' class='nivel5'><td class='check'><input type='checkbox' class='styled'/></td><td class='nivel5' title =\"" . $campo1 . "\">" . $valorCampo1 . "</td>";        

                for ($i = 0; $i < count($meses); $i++) {

                    $salidaOtros .= "<td>" . number_format($manifiesto[3 + $i], 0, '', '.') . "</td>";
                    //$meses[$i][$numFilas + 1] .= number_format($manifiesto[3 + $i], 0, '', '');
                }

                $salidaOtros .= "<td><b>" . number_format($manifiesto[16], 0, '', '.') . "</b></td><td><b>" . number_format($manifiesto[15], 0, '', '.') . "</b></td>";
                $salidaOtros .= $objSempat->calculoDiferencia($manifiesto[16], $manifiesto[15]) . "</tr>";
                
                $total = $total + number_format($manifiesto[16], 0, '', '');
                $totalAnterior = $totalAnterior + number_format($manifiesto[15], 0, '', '');
            
            }
        }
    }               
        
    echo $salida . $salidaOtros;
    
}
else{
    echo "";
}



//echo $salida;


?>
