<?PHP

session_start();
include ("../../librerias/conexion.php");
require('flexitank.class.php');
$objFlexitank = new flexitank();


$usuarioId = $_SESSION['SEMPAT_usuarioId'];
$clienteId = $_SESSION['SEMPAT_clienteId'];

$fechaDesde = mb_convert_encoding(trim($_POST['fechaDesde']), "ISO-8859-1", "UTF-8");
$fechaHasta = mb_convert_encoding(trim($_POST['fechaHasta']), "ISO-8859-1", "UTF-8");
$carpeta = mb_convert_encoding(trim($_POST['carpeta']), "ISO-8859-1", "UTF-8");


$nombreArchivo = mb_convert_encoding(trim($_POST['nombreArchivo']), "ISO-8859-1", "UTF-8");

$tipoDato = mb_convert_encoding(trim($_POST['tipoDato']), "ISO-8859-1", "UTF-8");

$aduana = mb_convert_encoding(trim($_POST['aduana']), "ISO-8859-1", "UTF-8");
$clausula = mb_convert_encoding(trim($_POST['clausula']), "ISO-8859-1", "UTF-8");
$exportador = mb_convert_encoding(trim($_POST['exportador']), "ISO-8859-1", "UTF-8");
$mercaderia = mb_convert_encoding(trim($_POST['mercaderia']), "ISO-8859-1", "UTF-8");
$moneda = mb_convert_encoding(trim($_POST['moneda']), "ISO-8859-1", "UTF-8");
$nave = mb_convert_encoding(trim($_POST['nave']), "ISO-8859-1", "UTF-8");
$naviera = mb_convert_encoding(trim($_POST['naviera']), "ISO-8859-1", "UTF-8");
$paisDestino = mb_convert_encoding(trim($_POST['paisDestino']), "ISO-8859-1", "UTF-8");
$puertoDescarga = mb_convert_encoding(trim($_POST['puertoDescarga']), "ISO-8859-1", "UTF-8"); 
$puertoEmbarque = mb_convert_encoding(trim($_POST['puertoEmbarque']), "ISO-8859-1", "UTF-8");
$tipoBulto = mb_convert_encoding(trim($_POST['tipoBulto']), "ISO-8859-1", "UTF-8");
$tipoCarga = mb_convert_encoding(trim($_POST['tipoCarga']), "ISO-8859-1", "UTF-8");
$tipoOperacion = mb_convert_encoding(trim($_POST['tipoOperacion']), "ISO-8859-1", "UTF-8");
$trafico = mb_convert_encoding(trim($_POST['trafico']), "ISO-8859-1", "UTF-8");
$viaTransporte = mb_convert_encoding(trim($_POST['viaTransporte']), "ISO-8859-1", "UTF-8");


$aduana = str_replace("\'", "'", $aduana);
$clausula = str_replace("\'", "'", $clausula);
$exportador = str_replace("\'", "'", $exportador);
$mercaderia = str_replace("\'", "'", $mercaderia);
$moneda = str_replace("\'", "'", $moneda);
$nave = str_replace("\'", "'", $nave);
$naviera = str_replace("\'", "'", $naviera);
$paisDestino = str_replace("\'", "'", $paisDestino);
$puertoDescarga = str_replace("\'", "'", $puertoDescarga);
$puertoEmbarque = str_replace("\'", "'", $puertoEmbarque);
$tipoBulto = str_replace("\'", "'", $tipoBulto);
$tipoCarga = str_replace("\'", "'", $tipoCarga);
$tipoOperacion = str_replace("\'", "'", $tipoOperacion);
$trafico = str_replace("\'", "'", $trafico);
$viaTransporte = str_replace("\'", "'", $viaTransporte);




$chkaduana = mb_convert_encoding(trim($_POST['chkaduana']), "ISO-8859-1", "UTF-8");
$chkclausula = mb_convert_encoding(trim($_POST['chkclausula']), "ISO-8859-1", "UTF-8");
$chkexportador = mb_convert_encoding(trim($_POST['chkexportador']), "ISO-8859-1", "UTF-8");
$chkmercaderia = mb_convert_encoding(trim($_POST['chkmercaderia']), "ISO-8859-1", "UTF-8");
$chkmoneda = mb_convert_encoding(trim($_POST['chkmoneda']), "ISO-8859-1", "UTF-8");
$chknave = mb_convert_encoding(trim($_POST['chknave']), "ISO-8859-1", "UTF-8");
$chknaviera = mb_convert_encoding(trim($_POST['chknaviera']), "ISO-8859-1", "UTF-8");
$chkpaisDestino = mb_convert_encoding(trim($_POST['chkpaisDestino']), "ISO-8859-1", "UTF-8");
$chkpuertoDescarga = mb_convert_encoding(trim($_POST['chkpuertoDescarga']), "ISO-8859-1", "UTF-8"); 
$chkpuertoEmbarque = mb_convert_encoding(trim($_POST['chkpuertoEmbarque']), "ISO-8859-1", "UTF-8");
$chktipoBulto = mb_convert_encoding(trim($_POST['chktipoBulto']), "ISO-8859-1", "UTF-8");
$chktipoCarga = mb_convert_encoding(trim($_POST['chktipoCarga']), "ISO-8859-1", "UTF-8");
$chktipoOperacion = mb_convert_encoding(trim($_POST['chktipoOperacion']), "ISO-8859-1", "UTF-8");
$chktrafico = mb_convert_encoding(trim($_POST['chktrafico']), "ISO-8859-1", "UTF-8");
$chkviaTransporte = mb_convert_encoding(trim($_POST['chkviaTransporte']), "ISO-8859-1", "UTF-8");


$uploaddir = "../../Reportes/" . $carpeta . "/";
			
if (file_exists($uploaddir)==false) {
    mkdir($uploaddir, 0700, true);
}

$uploaddir = $uploaddir . "/" . $usuarioId . "/";

if (file_exists($uploaddir)==false) {
    mkdir($uploaddir, 0700, true);
}

$objFlexitank->flexitankExtracciones($fechaDesde, $fechaHasta, $tipoDato, $clienteId, $aduana, $clausula, $exportador, $mercaderia, $moneda, $nave, $naviera, $paisDestino, $puertoDescarga, $puertoEmbarque, $tipoBulto, $tipoCarga, $tipoOperacion, $trafico, $viaTransporte, $chkaduana, $chkclausula, $chkexportador, $chkmercaderia, $chkmoneda, $chknave, $chknaviera, $chkpaisDestino, $chkpuertoDescarga, $chkpuertoEmbarque, $chktipoBulto, $chktipoCarga, $chktipoOperacion, $chktrafico, $chkviaTransporte, $nombreArchivo, $usuarioId);

?>
