

function buscarExtracciones(){
    
    var nombreArchivo = $("#inputNombreArchivo").val();
    var carpeta = $("#carpeta").val();

    var fechaDesde = $("#fechaDesde").val();
    var fechaHasta = $("#fechaHasta").val();
    
    var mercado = "";

    var chile = $("input[id=btnChile]:checked").val();
    var peru = $("input[id=btnPeru]:checked").val();
    var ecuador = $("input[id=btnEcuador]:checked").val();
    var colombia = $("input[id=btnColombia]:checked").val();


    mercado = chile + "," + peru + "," + ecuador + "," + colombia;                                                         
    mercado = replaceAll(mercado, ",undefined",""); 
    mercado = replaceAll(mercado, "undefined",""); 

    if(chile == undefined){
        mercado = mercado.substring(1, mercado.length);
    }

    if(fechaDesde == "" && fechaHasta == ""){

        fechasDefecto(mercado); 
        fechaDesde = $("#fechaDesde").val();
        fechaHasta = $("#fechaHasta").val();    
    }
    
    if(validarFormulario()){

        $("#divResultado").html("");  

        $("#imgCargando").css("display","block");
        //$("#divPanelFiltros").css("display","none");            
        //$("#divMensaje").css("display","none");

        var puertoOrigen = $("#puertoOrigen").val();
        var puertoEmbarque = $("#puertoEmbarque").val();
        var puertoDescarga = $("#puertoDescarga").val();
        var puertoDestino = $("#puertoDestino").val();
        var paisOrigen = $("#paisOrigen").val();
        var paisEmbarque = $("#paisEmbarque").val();
        var paisDescarga = $("#paisDescarga").val();
        var paisDestino = $("#paisDestino").val();
        var trafico = $("#trafico").val();
        var naviera = $("#naviera").val();
        var agencia = $("#agencia").val();
        var agenteDoc = $("#agenteDoc").val();
        var nave = $("#nave").val();
        var tipoNave = $("#tipoNave").val();
        var shipper = $("#shipper").val();
        var consignee = $("#consignee").val();
        var granFamilia = $("#granFamilia").val();
        var familia = $("#familia").val();
        var subFamilia = $("#subFamilia").val();
        var commodity = $("#commodity").val();
        var tipoCarga = $("#tipoCarga").val();
        var tipoServicio = $("#tipoServicio").val();
        var tipoEmbalaje = $("#tipoEmbalaje").val();
        var statusContainer = $("#statusContainer").val();


        var chkpuertoOrigen = $("#chkpuertoOrigen").attr('checked');
        var chkpuertoEmbarque = $("#chkpuertoEmbarque").attr('checked');
        var chkpuertoDescarga = $("#chkpuertoDescarga").attr('checked');
        var chkpuertoDestino = $("#chkpuertoDestino").attr('checked');
        var chkpaisOrigen = $("#chkpaisOrigen").attr('checked');
        var chkpaisEmbarque = $("#chkpaisEmbarque").attr('checked');
        var chkpaisDescarga = $("#chkpaisDescarga").attr('checked');
        var chkpaisDestino = $("#chkpaisDestino").attr('checked');
        var chktrafico = $("#chktrafico").attr('checked');
        var chknaviera = $("#chknaviera").attr('checked');
        var chkagencia = $("#chkagencia").attr('checked');
        var chkagenteDoc = $("#chkagenteDoc").attr('checked');
        var chknave = $("#chknave").attr('checked');
        var chktipoNave = $("#chktipoNave").attr('checked');
        var chkshipper = $("#chkshipper").attr('checked');
        var chkconsignee = $("#chkconsignee").attr('checked');
        var chkgranFamilia = $("#chkgranFamilia").attr('checked');
        var chkfamilia = $("#chkfamilia").attr('checked');
        var chksubFamilia = $("#chksubFamilia").attr('checked');
        var chkcommodity = $("#chkcommodity").attr('checked');
        var chktipoCarga = $("#chktipoCarga").attr('checked');
        var chktipoServicio = $("#chktipoServicio").attr('checked');
        var chktipoEmbalaje = $("#chktipoEmbalaje").attr('checked');
        var chkstatusContainer = $("#chkstatusContainer").attr('checked');

        var tipoDato = $("input[name=tipoDato]:checked").val();

        var etapa = $("input[name=btnEtapa]:checked").val();
        var contenedor = $("input[name=btnContenedor]:checked").val();
        var dryReefer = $("input[name=btnSecaRefrigerada]:checked").val();
        

        $.ajax({
            type: "POST",
            url: "granelesExtraccionesData.php",
            data: { fechaDesde: fechaDesde, fechaHasta: fechaHasta, puertoOrigen: puertoOrigen, puertoEmbarque: puertoEmbarque, puertoDescarga: puertoDescarga, puertoDestino: 
                    puertoDestino, paisOrigen: paisOrigen, paisEmbarque: paisEmbarque, paisDescarga: paisDescarga, paisDestino: paisDestino, trafico: trafico, naviera: naviera, 
                agencia: agencia, agenteDoc: agenteDoc, nave: nave, tipoNave: tipoNave, shipper: shipper, consignee: consignee, granFamilia: granFamilia, familia: familia, 
                subFamilia: subFamilia, commodity: commodity, tipoCarga: tipoCarga, tipoServicio: tipoServicio, tipoEmbalaje: tipoEmbalaje, statusContainer: statusContainer, 

                chkpuertoOrigen: chkpuertoOrigen, chkpuertoEmbarque: chkpuertoEmbarque, chkpuertoDescarga: chkpuertoDescarga, chkpuertoDestino: chkpuertoDestino, chkpaisOrigen: chkpaisOrigen, 
                chkpaisEmbarque: chkpaisEmbarque, chkpaisDescarga: chkpaisDescarga, chkpaisDestino: chkpaisDestino, chktrafico: chktrafico, chknaviera: chknaviera, chkagencia: chkagencia, 
                chkagenteDoc: chkagenteDoc, chknave: chknave, chktipoNave: chktipoNave, chkshipper: chkshipper, chkconsignee: chkconsignee, chkgranFamilia: chkgranFamilia, 
                chkfamilia: chkfamilia, chksubFamilia: chksubFamilia, chkcommodity: chkcommodity, chktipoCarga: chktipoCarga, chktipoServicio: chktipoServicio, 
                chktipoEmbalaje: chktipoEmbalaje, chkstatusContainer: chkstatusContainer, 

                nombreArchivo: nombreArchivo,tipoDato:tipoDato, mercado: mercado, etapa: etapa, contenedor: contenedor, dryReefer: dryReefer, carpeta: carpeta}
            })
            .done(function( msg ) {
                buscarExtraccionesArchivos($("#moduloId").val());
                $("#imgCargando").css("display","none");                                           

        }); 

    }                                                                    

}  


function validarFormulario(){

    var mensaje = "";

    var nombreArchivo = $("#inputNombreArchivo").val();
    var fechaDesde = $("#fechaDesde").val();
    var fechaHasta = $("#fechaHasta").val();                                 
            
    if(nombreArchivo == ""){
        mensaje = "Debe ingresar un nombre para el archivo.<br>";
    }
    
    if(fechaDesde == ""){
        mensaje += "No ha ingresado la fecha inicial.<br>";
    }
    
    if(fechaHasta == ""){
        mensaje += "No ha ingresado la fecha final.<br>";
    }
    
    if(fechaDesde != "" && fechaHasta != "" && validarFechaMenor(fechaDesde, fechaHasta) == false){
        mensaje += "La fecha incial no puede ser mayor a la fecha final.<br>";
    }
    
    if(fechaDesde != "" && fechaHasta != "" && fechaDiferenciasDias(fechaDesde, fechaHasta) > 365){
        mensaje += "La diferencia entre las fechas no puede ser superior a un año.<br>";
    }
            
    if(mensaje != ""){
        $("#divMensaje").html(mensaje);        
        return false;
    }
    else{
        $("#divMensaje").html("");        
        return true;
    }

}
    
    
    