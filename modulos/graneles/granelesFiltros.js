
$(document).ready(function(){
    
    $("#fechaDesde").mask("99-99-9999");
    $("#fechaHasta").mask("99-99-9999");

    $('#fechaDesde').bind('blur', function() {
        validarFecha(this);
    });

    $('#fechaHasta').bind('blur', function() {
        validarFecha(this);
    });


    $('#btnChile').bind('click', function() {
        deschequear(this);
    });

    $('#btnPeru').bind('click', function() {
        deschequear(this);
    });

    $('#btnEcuador').bind('click', function() {
        deschequear(this);
    });

    $('#btnColombia').bind('click', function() {
        deschequear(this);
    });

    $('#btnImpo').bind('click', function() {
        deschequear(this);
    });

    $('#btnExpo').bind('click', function() {
        deschequear(this);
    });

    $('#btnContenedor').bind('click', function() {
        deschequear(this);
    });

    $('#btnNoContenedor').bind('click', function() {
        deschequear(this);
    });

    $('#btnSeca').bind('click', function() {
        deschequear(this);
    });

    $('#btnRefrigerada').bind('click', function() {
        deschequear(this);
    });

    $('#btnContenedor').bind('mouseup', function() {
        $('#btnSeca').attr('disabled', false);
        $('#btnRefrigerada').attr('disabled', false);
        $('#btnContenedores').attr('disabled', false);
        $('#btnTeus').attr('disabled', false);
    });

    $('#btnNoContenedor').bind('mouseup', function() {
        if(this.checked){
            $('#btnSeca').attr('disabled', false);
            $('#btnRefrigerada').attr('disabled', false);
            $('#btnContenedores').attr('disabled', false);
            $('#btnTeus').attr('disabled', false);
        }
        else {
            $('#btnSeca').attr('disabled', true);
            $('#btnRefrigerada').attr('disabled', true);
            $('#btnContenedores').attr('disabled', true);
            $('#btnTeus').attr('disabled', true);
        }
    });

    $('#btnSeca').bind('click', function() {

        if($('#btnSeca').attr('checked')){
            $('#btnContenedor').attr('checked', true);
            $('#btnContenedor').attr('disabled', true);
            $('#btnNoContenedor').attr('disabled', true);
        }
        else {
            $('#btnContenedor').attr('checked', false);
            $('#btnContenedor').attr('disabled', false);
            $('#btnNoContenedor').attr('disabled', false);
        }
    });

    $('#btnRefrigerada').bind('click', function() {

        if($('#btnRefrigerada').attr('checked')){
            $('#btnContenedor').attr('checked', true);
            $('#btnContenedor').attr('disabled', true);
            $('#btnNoContenedor').attr('disabled', true);
        }
        else {
            $('#btnContenedor').attr('checked', false);
            $('#btnContenedor').attr('disabled', false);
            $('#btnNoContenedor').attr('disabled', false);
        }
    });

});







