<?PHP

session_start();
include ("../../librerias/conexion.php");
require('manifiesto.class.php');
require('../../clases/sempat.class.php');
$objManifiesto = new manifiesto();
$objSempat= new sempat();


$usuarioId = $_SESSION['SEMPAT_usuarioId'];
$clienteId = $_SESSION['SEMPAT_clienteId'];

$fechaDesde = mb_convert_encoding(trim($_POST['fechaDesde']), "ISO-8859-1", "UTF-8");
$fechaHasta = mb_convert_encoding(trim($_POST['fechaHasta']), "ISO-8859-1", "UTF-8");

$nivel1 = mb_convert_encoding(trim($_POST['nivel1']), "ISO-8859-1", "UTF-8");
$nivel2 = mb_convert_encoding(trim($_POST['nivel2']), "ISO-8859-1", "UTF-8");
$nivel3 = mb_convert_encoding(trim($_POST['nivel3']), "ISO-8859-1", "UTF-8");
$nivel4 = mb_convert_encoding(trim($_POST['nivel4']), "ISO-8859-1", "UTF-8");
$nivel5 = mb_convert_encoding(trim($_POST['nivel5']), "ISO-8859-1", "UTF-8");


$nivel1Titulo = mb_convert_encoding(trim($_POST['nivel1Titulo']), "ISO-8859-1", "UTF-8");

$etapa = mb_convert_encoding(trim($_POST['etapa']), "ISO-8859-1", "UTF-8");
$mercado = mb_convert_encoding(trim($_POST['mercado']), "ISO-8859-1", "UTF-8");
$contenedor = mb_convert_encoding(trim($_POST['contenedor']), "ISO-8859-1", "UTF-8");
$dryReefer = mb_convert_encoding(trim($_POST['dryReefer']), "ISO-8859-1", "UTF-8");

$tipoEmbalaje = mb_convert_encoding(trim($_POST['tipoEmbalaje']), "ISO-8859-1", "UTF-8");
$statusContainer = mb_convert_encoding(trim($_POST['statusContainer']), "ISO-8859-1", "UTF-8");
$tipoDato = mb_convert_encoding(trim($_POST['tipoDato']), "ISO-8859-1", "UTF-8");

$puertoOrigen = mb_convert_encoding(trim($_POST['puertoOrigen']), "ISO-8859-1", "UTF-8");
$puertoEmbarque = mb_convert_encoding(trim($_POST['puertoEmbarque']), "ISO-8859-1", "UTF-8");
$puertoDescarga = mb_convert_encoding(trim($_POST['puertoDescarga']), "ISO-8859-1", "UTF-8");
$puertoDestino = mb_convert_encoding(trim($_POST['puertoDestino']), "ISO-8859-1", "UTF-8");

$paisOrigen = mb_convert_encoding(trim($_POST['paisOrigen']), "ISO-8859-1", "UTF-8");
$paisEmbarque = mb_convert_encoding(trim($_POST['paisEmbarque']), "ISO-8859-1", "UTF-8");
$paisDescarga = mb_convert_encoding(trim($_POST['paisDescarga']), "ISO-8859-1", "UTF-8");
$paisDestino = mb_convert_encoding(trim($_POST['paisDestino']), "ISO-8859-1", "UTF-8");

$trafico = mb_convert_encoding(trim($_POST['trafico']), "ISO-8859-1", "UTF-8");
$naviera = mb_convert_encoding(trim($_POST['naviera']), "ISO-8859-1", "UTF-8");
$agencia = mb_convert_encoding(trim($_POST['agencia']), "ISO-8859-1", "UTF-8");
$agenteDoc = mb_convert_encoding(trim($_POST['agenteDoc']), "ISO-8859-1", "UTF-8");
$nave = mb_convert_encoding(trim($_POST['nave']), "ISO-8859-1", "UTF-8");
$tipoNave = mb_convert_encoding(trim($_POST['tipoNave']), "ISO-8859-1", "UTF-8");
$shipper = mb_convert_encoding(trim($_POST['shipper']), "ISO-8859-1", "UTF-8");
$consignee = mb_convert_encoding(trim($_POST['consignee']), "ISO-8859-1", "UTF-8");

$granFamilia = mb_convert_encoding(trim($_POST['granFamilia']), "ISO-8859-1", "UTF-8");
$familia = mb_convert_encoding(trim($_POST['familia']), "ISO-8859-1", "UTF-8");
$subFamilia = mb_convert_encoding(trim($_POST['subFamilia']), "ISO-8859-1", "UTF-8");
$commodity = mb_convert_encoding(trim($_POST['commodity']), "ISO-8859-1", "UTF-8");

$tipoCarga = mb_convert_encoding(trim($_POST['tipoCarga']), "ISO-8859-1", "UTF-8");
$tipoServicio = mb_convert_encoding(trim($_POST['tipoServicio']), "ISO-8859-1", "UTF-8");

$consultaTabla = $objManifiesto->manifiestoIndicadoresNivel1($fechaDesde, $fechaHasta, $nivel1, $nivel2, $nivel3, $nivel4, $nivel5, $tipoDato, $clienteId, $mercado, $etapa, $naviera, $agencia, $trafico, $puertoOrigen, $puertoEmbarque, $nave, $puertoDestino, $puertoDescarga, $tipoNave, $tipoServicio, $tipoCarga, $tipoEmbalaje, $paisOrigen, $paisEmbarque, $agenteDoc, $paisDestino, $paisDescarga, $statusContainer, $shipper, $consignee, $commodity, $granFamilia, $familia, $subFamilia, $dryReefer, $contenedor, $usuarioId);

$consultaTablaGrafico = $objManifiesto->manifiestoIndicadoresNivel1Grafico($fechaDesde, $fechaHasta, $nivel1, $usuarioId);

$meses = array();

$mesInicial = substr($fechaDesde, 3, 2);
$añoInicial = substr($fechaDesde, 6, 4);

$mesFinal = substr($fechaHasta, 3, 2);
$añoFinal = substr($fechaHasta, 6, 4);

for ($a = 0; $a < 12; $a++) {

    $mes = "00" . ($mesInicial);
    $mes = substr($mes, strlen($mes) - 2, strlen($mes));

    $meses[$a][0] = $añoInicial . "-" . $mes;

    if ($mes >= $mesFinal && $añoInicial >= $añoFinal) {                        
        $a = 12;                        
    }

    if ($mes == 12) {
        $añoInicial++;
        $mesInicial = "01";
    } else {
        $mesInicial++;
    }        
    
}

//<editor-fold>
if ($consultaTablaGrafico) {
        
    $fila = 0;    
    
    $htmlTablaGrafico = "<table id='tablaGrafico'><thead><th></th></thead>";
    $htmlTablaGeneral = "<table id='tablaGeneral'>";
    $htmlTablaGeneralC20 = "<table id='tablaGeneralC20'>";
    $htmlTablaGeneralC40 = "<table id='tablaGeneralC40'>";
    $htmlTablaGeneralHead = "<thead><tr><th></th>";
    $htmlTablaGeneralHeadC20 = "<thead><tr><th></th>";
    $htmlTablaGeneralHeadC40 = "<thead><tr><th></th>";
    $htmlTablaGeneralBody = "<tbody>";
    $htmlTablaGeneralBodyC20 = "<tbody>";
    $htmlTablaGeneralBodyC40 = "<tbody>";
    
    
    $resultadoGrafico = array();    
    
    while ($rowdatos = mssql_fetch_array($consultaTablaGrafico)) {
        $resultadoGrafico[] = $rowdatos;
    }        
    
    
    for($x = 0; $x < count($resultadoGrafico); $x++){        
        if(trim($resultadoGrafico[$x][17]) == ""){
            $htmlTablaGeneralHead .= "<th>" . htmlentities($resultadoGrafico[$x][1]) . "</th>";                        
        }
        if(trim($resultadoGrafico[$x][17]) == "C20"){
            $htmlTablaGeneralHeadC20 .= "<th>" . htmlentities($resultadoGrafico[$x][1]) . "</th>";
        }
        if(trim($resultadoGrafico[$x][17]) == "C40"){
            $htmlTablaGeneralHeadC40 .= "<th>" . htmlentities($resultadoGrafico[$x][1]) . "</th>";
        }
    }
            
    for($i = 0; $i < count($meses); $i++){     
        
        $htmlTablaGrafico .= "<tr><th>" . retornarPeriodo($meses[$i][0]) . "</th></tr>";                
        $htmlTablaGeneralBody .= "<tr><th>" . retornarPeriodo($meses[$i][0]) . "</th>";
        $htmlTablaGeneralBodyC20 .= "<tr><th>" . retornarPeriodo($meses[$i][0]) . "</th>";
        $htmlTablaGeneralBodyC40 .= "<tr><th>" . retornarPeriodo($meses[$i][0]) . "</th>";
        
        for($x = 0; $x < count($resultadoGrafico); $x++){                                    
            if(trim($resultadoGrafico[$x][17]) == ""){
                $htmlTablaGeneralBody .= "<th>" . number_format($resultadoGrafico[$x][$i + 3], 0, '', '') . "</th>";
            }
            if(trim($resultadoGrafico[$x][17]) == "C20"){
                $htmlTablaGeneralBodyC20 .= "<th>" . number_format($resultadoGrafico[$x][$i + 3], 0, '', '') . "</th>";
            }
            if(trim($resultadoGrafico[$x][17]) == "C40"){
                $htmlTablaGeneralBodyC40 .= "<th>" . number_format($resultadoGrafico[$x][$i + 3], 0, '', '') . "</th>";
            }
        }
        $htmlTablaGeneralBody .= "</tr>";
        $htmlTablaGeneralBodyC20 .= "</tr>";
        $htmlTablaGeneralBodyC40 .= "</tr>";

    }
    
    
    $htmlTablaGeneral .= $htmlTablaGeneralHead . "</tr></thead>" . $htmlTablaGeneralBody . "</tbody></table>";
    $htmlTablaGeneral .= $htmlTablaGeneralC20 . $htmlTablaGeneralHeadC20 . "</tr></thead>" . $htmlTablaGeneralBodyC20 . "</tbody></table>";
    $htmlTablaGeneral .= $htmlTablaGeneralC40 . $htmlTablaGeneralHeadC40 . "</tr></thead>" . $htmlTablaGeneralBodyC40 . "</tbody></table>";
    
    
                                                                                         
    $htmlTablaGrafico .= "</tbody></table>
                        <table id=\"mktYtd2\">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>

                        <table id=\"mktYtd1\">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>

                        <table id=\"mkt12M2\">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>

                        <table id=\"mkt12M1\">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>

                        <table id=\"mktM2\">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>

                        <table id=\"mktM1\">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>                    
                        </table>" . $htmlTablaGeneral;
    
}
//</editor-fold>

$htmlTablaCabecera = $objManifiesto->indicadoresCabeceraHtml(strtoupper($nivel1Titulo), $fechaHasta);


$theadHTML = "<thead id=\"tablaValoresTHead\" >" . $htmlTablaCabecera ."</thead>";
$tbodyHTML = "<tbody id=\"tBody\" class=\"indicadores\">";

$cursor = ""; 

if($nivel2 != ""){
    $cursor = " style='cursor:pointer'";
}


//<editor-fold>
if ($consultaTabla) {
                    
    if(mssql_num_fields($consultaTabla) > 1){
        
        $resultado = array();
    
        while ($rowdatos = mssql_fetch_array($consultaTabla)) {
            $resultado[] = $rowdatos;
        }

        $totales = array();
               
        for($i = 0; $i < count($resultado); $i++){    
            $totales[0] = $totales[0] + $resultado[$i][3];
            $totales[1] = $totales[1] + $resultado[$i][5];
            $totales[2] = $totales[2] + $resultado[$i][7];        
        }        

        $col_1 = 0;
        $col_2 = 0;
        $col_3 = 0;
        $col_4 = 0;
        $col_5 = 0;
        $col_6 = 0;
        $fila = 0;

        $salidaOtros = "";
        $campo = "";
        $valorCampo = "";

        for($i = 0; $i < count($resultado); $i++){

            $fila++;

            if($resultado[$i][5] != "" || $resultado[$i][6] != ""){

                $campo = htmlentities($resultado[$i][1]);

                if(strlen($campo) > 24){
                    $valorCampo = substr($campo, 0, 19) . "...";
                }
                else{
                    $valorCampo = $campo;
                }

                if(htmlentities($resultado[$i][1]) != "OTROS"){
                    $tbodyHTML .= "<tr id=\"tr-" . $fila . "\" class=\"nivel1\">";
                    $tbodyHTML .= "<td class=\"check\"><input class='styled' type='checkbox' onClick='graficoCheckboxIndicador(this);'/></td>";
                    $tbodyHTML .= "<td class=\"nivel1\" " . $cursor . " onClick='buscarIndicadorNivel2(this, \"" . $fechaDesde . "\", \"" . $fechaHasta . "\", \"" . $nivel2 . "\", \"" . $nivel3 . "\", \"" . $nivel4 . "\", \"" . $nivel5 . "\", \"" . $tipoDato . "\", \"" . $resultado[$i][2] . "\");' title=\"" . htmlentities($resultado[$i][1]) . "\">" . $valorCampo . "</td>";
                    $tbodyHTML .= "<td>" . number_format($resultado[$i][3], 0, '', '.') . "</td>";
                    $tbodyHTML .= "<td>" . number_format($resultado[$i][4], 0, '', '.') . "</td>";
                    $tbodyHTML .= $objSempat->calculoDiferencia($resultado[$i][3], $resultado[$i][4]);
                    $tbodyHTML .= $objSempat->calculoMkt($resultado[$i][3], $totales[0]);

                    $tbodyHTML .= "<td class=\"clear\"></td>";
                    $tbodyHTML .= "<td class=\"left\">" . number_format($resultado[$i][5], 0, '', '.') . "</td>";
                    $tbodyHTML .= "<td>" . number_format($resultado[$i][6], 0, '', '.') . "</td>";
                    $tbodyHTML .= $objSempat->calculoDiferencia($resultado[$i][5], $resultado[$i][6]);
                    $tbodyHTML .= $objSempat->calculoMkt($resultado[$i][5], $totales[1]);

                    $tbodyHTML .= "<td class=\"clear\"></td>";
                    $tbodyHTML .= "<td class=\"left\">" . number_format($resultado[$i][7], 0, '', '.') . "</td>";
                    $tbodyHTML .= "<td>" . number_format($resultado[$i][8], 0, '', '.') . "</td>";
                    $tbodyHTML .= $objSempat->calculoDiferencia($resultado[$i][7], $resultado[$i][8]);
                    $tbodyHTML .= $objSempat->calculoMkt($resultado[$i][7], $totales[2]);
                    $tbodyHTML .= "</tr>";               
                }
                else{

                    $salidaOtros .= "<tr id=\"tr-" . $fila . "\" class=\"nivel1\">";
                    $salidaOtros .= "<td class=\"check\"><input class='styled' type='checkbox' onClick='graficoCheckboxIndicador(this);'/></td>";
                    $salidaOtros .= "<td class=\"nivel1\">" . htmlentities($resultado[$i][1]) . "</td>";
                    $salidaOtros .= "<td>" . number_format($resultado[$i][3], 0, '', '.') . "</td>";
                    $salidaOtros .= "<td>" . number_format($resultado[$i][4], 0, '', '.') . "</td>";
                    $salidaOtros .= $objSempat->calculoDiferencia($resultado[$i][3], $resultado[$i][4]);
                    $salidaOtros .= $objSempat->calculoMkt($resultado[$i][3], $totales[0]);

                    $salidaOtros .= "<td class=\"clear\"></td>";
                    $salidaOtros .= "<td class=\"left\">" . number_format($resultado[$i][5], 0, '', '.') . "</td>";
                    $salidaOtros .= "<td>" . number_format($resultado[$i][6], 0, '', '.') . "</td>";
                    $salidaOtros .= $objSempat->calculoDiferencia($resultado[$i][5], $resultado[$i][6]);
                    $salidaOtros .= $objSempat->calculoMkt($resultado[$i][5], $totales[1]);

                    $salidaOtros .= "<td class=\"clear\"></td>";
                    $salidaOtros .= "<td class=\"left\">" . number_format($resultado[$i][7], 0, '', '.') . "</td>";
                    $salidaOtros .= "<td>" . number_format($resultado[$i][8], 0, '', '.') . "</td>";
                    $salidaOtros .= $objSempat->calculoDiferencia($resultado[$i][7], $resultado[$i][8]);
                    $salidaOtros .= $objSempat->calculoMkt($resultado[$i][7], $totales[2]);
                    $salidaOtros .= "</tr>";
                }  

                $col_1 = $col_1 + number_format($resultado[$i][3], 0, '', '');
                $col_2 = $col_2 + number_format($resultado[$i][4], 0, '', '');
                $col_3 = $col_3 + number_format($resultado[$i][5], 0, '', '');
                $col_4 = $col_4 + number_format($resultado[$i][6], 0, '', '');
                $col_5 = $col_5 + number_format($resultado[$i][7], 0, '', '');
                $col_6 = $col_6 + number_format($resultado[$i][8], 0, '', '');
                
            }
        }

        $tbodyHTML .= $salidaOtros;

        $tbodyHTML .= "<tr class=\"nivelTotal\">";
        $tbodyHTML .= "<td class=\"check\"></td>";
        $tbodyHTML .= "<td class=\"nivelTotal\">Total</td>";
        $tbodyHTML .= "<td>" . number_format($col_1, 0, '', '.') . "</td>";
        $tbodyHTML .= "<td>" . number_format($col_2, 0, '', '.') . "</td>";
        $tbodyHTML .= $objSempat->calculoDiferencia($col_1, $col_2);
        $tbodyHTML .= "<td style=\"text-align:center\">100%</td>";

        $tbodyHTML .= "<td class=\"clear\"></td>";
        $tbodyHTML .= "<td class=\"left\">" . number_format($col_3, 0, '', '.') . "</td>";
        $tbodyHTML .= "<td>" . number_format($col_4, 0, '', '.') . "</td>";
        $tbodyHTML .= $objSempat->calculoDiferencia($col_3, $col_4);
        $tbodyHTML .= "<td style=\"text-align:center\">100%</td>";

        $tbodyHTML .= "<td class=\"clear\"></td>";
        $tbodyHTML .= "<td class=\"left\">" . number_format($col_5, 0, '', '.') . "</td>";
        $tbodyHTML .= "<td>" . number_format($col_6, 0, '', '.') . "</td>";
        $tbodyHTML .= $objSempat->calculoDiferencia($col_5, $col_6);
        $tbodyHTML .= "<td style=\"text-align:center\">100%</td>";
        $tbodyHTML .= "</tr>";


        $tbodyHTML .= "</tbody>";


        $bodyHTMLC20 = "";
        $bodyHTMLC40 = "";

        //<editor-fold>
        if($tipoDato == "contenedores"){  

            $consultaTabla = $objManifiesto->manifiestoIndicadoresNivel1Contenedores($fechaDesde, $fechaHasta, $nivel1, "totalC20", $usuarioId);

            $bodyHTMLC20 = "<tbody id=\"tBodyC20\" class=\"indicadores\" style='display:none'>";

            if ($consultaTabla) {

                $tipoDato = "contenedoresC20";

                $resultado = array();

                while ($rowdatos = mssql_fetch_array($consultaTabla)) {
                    $resultado[] = $rowdatos;
                }

                $totales = array();

                for($i = 0; $i < count($resultado); $i++){    
                    $totales[0] = $totales[0] + $resultado[$i][3];
                    $totales[1] = $totales[1] + $resultado[$i][5];
                    $totales[2] = $totales[2] + $resultado[$i][7];        
                }        

                $col_1 = 0;
                $col_2 = 0;
                $col_3 = 0;
                $col_4 = 0;
                $col_5 = 0;
                $col_6 = 0;
                $fila = 0;

                $salidaOtros = "";


                for($i = 0; $i < count($resultado); $i++){

                    $fila++;

                    if($resultado[$i][5] != "" || $resultado[$i][6] != ""){

                        $campo = htmlentities($resultado[$i][1]);

                        if(strlen($campo) > 24){
                            $valorCampo = substr($campo, 0, 19) . "...";
                        }
                        else{
                            $valorCampo = $campo;
                        }

                        if(htmlentities($resultado[$i][1]) != "OTROS"){
                            $bodyHTMLC20 .= "<tr id=\"trC20-" . $fila . "\" class=\"nivel1\">";
                            $bodyHTMLC20 .= "<td class=\"check\"><input class=\"styled\" type=\"checkbox\" onClick=\"graficoCheckboxIndicador(this);\"/></td>";
                            $bodyHTMLC20 .= "<td class=\"nivel1\" " . $cursor . " onClick='buscarIndicadorNivel2(this, \"" . $fechaDesde . "\", \"" . $fechaHasta . "\", \"" . $nivel2 . "\", \"" . $nivel3 . "\", \"" . $nivel4 . "\", \"" . $nivel5 . "\", \"" . $tipoDato . "\", \"" . $resultado[$i][2] . "\");' title=\"" . htmlentities($resultado[$i][1]) . "\">" . $valorCampo . "</td>";                        
                            $bodyHTMLC20 .= "<td>" . number_format($resultado[$i][3], 0, '', '.') . "</td>";
                            $bodyHTMLC20 .= "<td>" . number_format($resultado[$i][4], 0, '', '.') . "</td>";
                            $bodyHTMLC20 .= $objSempat->calculoDiferencia($resultado[$i][3], $resultado[$i][4]);
                            $bodyHTMLC20 .= $objSempat->calculoMkt($resultado[$i][3], $totales[0]);

                            $bodyHTMLC20 .= "<td class=\"clear\"></td>";
                            $bodyHTMLC20 .= "<td class=\"left\">" . number_format($resultado[$i][5], 0, '', '.') . "</td>";
                            $bodyHTMLC20 .= "<td>" . number_format($resultado[$i][6], 0, '', '.') . "</td>";
                            $bodyHTMLC20 .= $objSempat->calculoDiferencia($resultado[$i][5], $resultado[$i][6]);
                            $bodyHTMLC20 .= $objSempat->calculoMkt($resultado[$i][5], $totales[1]);

                            $bodyHTMLC20 .= "<td class=\"clear\"></td>";
                            $bodyHTMLC20 .= "<td class=\"left\">" . number_format($resultado[$i][7], 0, '', '.') . "</td>";
                            $bodyHTMLC20 .= "<td>" . number_format($resultado[$i][8], 0, '', '.') . "</td>";
                            $bodyHTMLC20 .= $objSempat->calculoDiferencia($resultado[$i][7], $resultado[$i][8]);
                            $bodyHTMLC20 .= $objSempat->calculoMkt($resultado[$i][7], $totales[2]);
                            $bodyHTMLC20 .= "</tr>";               
                        }
                        else{

                            $salidaOtros .= "<tr id=\"trC20-" . $fila . "\" class=\"nivel1\">";
                            $salidaOtros .= "<td class=\"check\"><input class='styled' type='checkbox' onClick='graficoCheckboxIndicador(this);'/></td>";
                            $salidaOtros .= "<td class=\"nivel1\">" . htmlentities($resultado[$i][1]) . "</td>";
                            $salidaOtros .= "<td>" . number_format($resultado[$i][3], 0, '', '.') . "</td>";
                            $salidaOtros .= "<td>" . number_format($resultado[$i][4], 0, '', '.') . "</td>";
                            $salidaOtros .= $objSempat->calculoDiferencia($resultado[$i][3], $resultado[$i][4]);
                            $salidaOtros .= $objSempat->calculoMkt($resultado[$i][3], $totales[0]);

                            $salidaOtros .= "<td class=\"clear\"></td>";
                            $salidaOtros .= "<td class=\"left\">" . number_format($resultado[$i][5], 0, '', '.') . "</td>";
                            $salidaOtros .= "<td>" . number_format($resultado[$i][6], 0, '', '.') . "</td>";
                            $salidaOtros .= $objSempat->calculoDiferencia($resultado[$i][5], $resultado[$i][6]);
                            $salidaOtros .= $objSempat->calculoMkt($resultado[$i][5], $totales[1]);

                            $salidaOtros .= "<td class=\"clear\"></td>";
                            $salidaOtros .= "<td class=\"left\">" . number_format($resultado[$i][7], 0, '', '.') . "</td>";
                            $salidaOtros .= "<td>" . number_format($resultado[$i][8], 0, '', '.') . "</td>";
                            $salidaOtros .= $objSempat->calculoDiferencia($resultado[$i][7], $resultado[$i][8]);
                            $salidaOtros .= $objSempat->calculoMkt($resultado[$i][7], $totales[2]);
                            $salidaOtros .= "</tr>";
                        }  

                        $col_1 = $col_1 + number_format($resultado[$i][3], 0, '', '');
                        $col_2 = $col_2 + number_format($resultado[$i][4], 0, '', '');
                        $col_3 = $col_3 + number_format($resultado[$i][5], 0, '', '');
                        $col_4 = $col_4 + number_format($resultado[$i][6], 0, '', '');
                        $col_5 = $col_5 + number_format($resultado[$i][7], 0, '', '');
                        $col_6 = $col_6 + number_format($resultado[$i][8], 0, '', '');
                    }
                }

                $bodyHTMLC20 .= $salidaOtros;

                $bodyHTMLC20 .= "<tr class=\"nivelTotal\">";
                $bodyHTMLC20 .= "<td class=\"check\"></td>";
                $bodyHTMLC20 .= "<td class=\"nivelTotal\">Total</td>";
                $bodyHTMLC20 .= "<td>" . number_format($col_1, 0, '', '.') . "</td>";
                $bodyHTMLC20 .= "<td>" . number_format($col_2, 0, '', '.') . "</td>";
                $bodyHTMLC20 .= $objSempat->calculoDiferencia($col_1, $col_2);
                $bodyHTMLC20 .= "<td style=\"text-align:center\">100%</td>";

                $bodyHTMLC20 .= "<td class=\"clear\"></td>";
                $bodyHTMLC20 .= "<td class=\"left\">" . number_format($col_3, 0, '', '.') . "</td>";
                $bodyHTMLC20 .= "<td>" . number_format($col_4, 0, '', '.') . "</td>";
                $bodyHTMLC20 .= $objSempat->calculoDiferencia($col_3, $col_4);
                $bodyHTMLC20 .= "<td style=\"text-align:center\">100%</td>";

                $bodyHTMLC20 .= "<td class=\"clear\"></td>";
                $bodyHTMLC20 .= "<td class=\"left\">" . number_format($col_5, 0, '', '.') . "</td>";
                $bodyHTMLC20 .= "<td>" . number_format($col_6, 0, '', '.') . "</td>";
                $bodyHTMLC20 .= $objSempat->calculoDiferencia($col_5, $col_6);
                $bodyHTMLC20 .= "<td style=\"text-align:center\">100%</td>";
                $bodyHTMLC20 .= "</tr>";

                $bodyHTMLC20 .= "</tbody>";

            }


            $consultaTabla = $objManifiesto->manifiestoIndicadoresNivel1Contenedores($fechaDesde, $fechaHasta, $nivel1, "totalC40", $usuarioId);

            $bodyHTMLC40 = "<tbody id=\"tBodyC40\" class=\"indicadores\"  style='display:none'>";

            if ($consultaTabla) {

                $tipoDato = "contenedoresC40";

                $resultado = array();

                while ($rowdatos = mssql_fetch_array($consultaTabla)) {
                    $resultado[] = $rowdatos;
                }

                $totales = array();

                for($i = 0; $i < count($resultado); $i++){    
                    $totales[0] = $totales[0] + $resultado[$i][3];
                    $totales[1] = $totales[1] + $resultado[$i][5];
                    $totales[2] = $totales[2] + $resultado[$i][7];        
                }        

                $col_1 = 0;
                $col_2 = 0;
                $col_3 = 0;
                $col_4 = 0;
                $col_5 = 0;
                $col_6 = 0;
                $fila = 0;

                $salidaOtros = "";


                for($i = 0; $i < count($resultado); $i++){

                    $fila++;

                    if($resultado[$i][5] != "" || $resultado[$i][6] != ""){

                        $campo = htmlentities($resultado[$i][1]);

                        if(strlen($campo) > 24){
                            $valorCampo = substr($campo, 0, 19) . "...";
                        }
                        else{
                            $valorCampo = $campo;
                        }

                        if(htmlentities($resultado[$i][1]) != "OTROS"){
                            $bodyHTMLC40 .= "<tr id=\"trC20-" . $fila . "\" class=\"nivel1\">";
                            $bodyHTMLC40 .= "<td class=\"check\"><input class='styled' type='checkbox' onClick='graficoCheckboxIndicador(this);'/></td>";
                            $bodyHTMLC40 .= "<td class=\"nivel1\" " . $cursor . " onClick='buscarIndicadorNivel2(this, \"" . $fechaDesde . "\", \"" . $fechaHasta . "\", \"" . $nivel2 . "\", \"" . $nivel3 . "\", \"" . $nivel4 . "\", \"" . $nivel5 . "\", \"" . $tipoDato . "\", \"" . $resultado[$i][2] . "\");' title=\"" . htmlentities($resultado[$i][1]) . "\">" . $valorCampo . "</td>";                        
                            $bodyHTMLC40 .= "<td>" . number_format($resultado[$i][3], 0, '', '.') . "</td>";
                            $bodyHTMLC40 .= "<td>" . number_format($resultado[$i][4], 0, '', '.') . "</td>";
                            $bodyHTMLC40 .= $objSempat->calculoDiferencia($resultado[$i][3], $resultado[$i][4]);
                            $bodyHTMLC40 .= $objSempat->calculoMkt($resultado[$i][3], $totales[0]);

                            $bodyHTMLC40 .= "<td class=\"clear\"></td>";
                            $bodyHTMLC40 .= "<td class=\"left\">" . number_format($resultado[$i][5], 0, '', '.') . "</td>";
                            $bodyHTMLC40 .= "<td>" . number_format($resultado[$i][6], 0, '', '.') . "</td>";
                            $bodyHTMLC40 .= $objSempat->calculoDiferencia($resultado[$i][5], $resultado[$i][6]);
                            $bodyHTMLC40 .= $objSempat->calculoMkt($resultado[$i][5], $totales[1]);

                            $bodyHTMLC40 .= "<td class=\"clear\"></td>";
                            $bodyHTMLC40 .= "<td class=\"left\">" . number_format($resultado[$i][7], 0, '', '.') . "</td>";
                            $bodyHTMLC40 .= "<td>" . number_format($resultado[$i][8], 0, '', '.') . "</td>";
                            $bodyHTMLC40 .= $objSempat->calculoDiferencia($resultado[$i][7], $resultado[$i][8]);
                            $bodyHTMLC40 .= $objSempat->calculoMkt($resultado[$i][7], $totales[2]);
                            $bodyHTMLC40 .= "</tr>";               
                        }
                        else{

                            $salidaOtros .= "<tr id=\"trC20-" . $fila . "\" class=\"nivel1\">";
                            $salidaOtros .= "<td class=\"check\"><input class='styled' type='checkbox' onClick='graficoCheckbox(this);'/></td>";
                            $salidaOtros .= "<td class=\"nivel1\">" . htmlentities($resultado[$i][1]) . "</td>";
                            $salidaOtros .= "<td>" . number_format($resultado[$i][3], 0, '', '.') . "</td>";
                            $salidaOtros .= "<td>" . number_format($resultado[$i][4], 0, '', '.') . "</td>";
                            $salidaOtros .= $objSempat->calculoDiferencia($resultado[$i][3], $resultado[$i][4]);
                            $salidaOtros .= $objSempat->calculoMkt($resultado[$i][3], $totales[0]);

                            $salidaOtros .= "<td class=\"clear\"></td>";
                            $salidaOtros .= "<td class=\"left\">" . number_format($resultado[$i][5], 0, '', '.') . "</td>";
                            $salidaOtros .= "<td>" . number_format($resultado[$i][6], 0, '', '.') . "</td>";
                            $salidaOtros .= $objSempat->calculoDiferencia($resultado[$i][5], $resultado[$i][6]);
                            $salidaOtros .= $objSempat->calculoMkt($resultado[$i][5], $totales[1]);

                            $salidaOtros .= "<td class=\"clear\"></td>";
                            $salidaOtros .= "<td class=\"left\">" . number_format($resultado[$i][7], 0, '', '.') . "</td>";
                            $salidaOtros .= "<td>" . number_format($resultado[$i][8], 0, '', '.') . "</td>";
                            $salidaOtros .= $objSempat->calculoDiferencia($resultado[$i][7], $resultado[$i][8]);
                            $salidaOtros .= $objSempat->calculoMkt($resultado[$i][7], $totales[2]);
                            $salidaOtros .= "</tr>";
                        }  

                        $col_1 = $col_1 + number_format($resultado[$i][3], 0, '', '');
                        $col_2 = $col_2 + number_format($resultado[$i][4], 0, '', '');
                        $col_3 = $col_3 + number_format($resultado[$i][5], 0, '', '');
                        $col_4 = $col_4 + number_format($resultado[$i][6], 0, '', '');
                        $col_5 = $col_5 + number_format($resultado[$i][7], 0, '', '');
                        $col_6 = $col_6 + number_format($resultado[$i][8], 0, '', '');
                    }
                }

                $bodyHTMLC40 .= $salidaOtros;

                $bodyHTMLC40 .= "<tr class=\"nivelTotal\">";
                $bodyHTMLC40 .= "<td class=\"check\"></td>";
                $bodyHTMLC40 .= "<td class=\"nivelTotal\">Total</td>";
                $bodyHTMLC40 .= "<td>" . number_format($col_1, 0, '', '.') . "</td>";
                $bodyHTMLC40 .= "<td>" . number_format($col_2, 0, '', '.') . "</td>";
                $bodyHTMLC40 .= $objSempat->calculoDiferencia($col_1, $col_2);
                $bodyHTMLC40 .= "<td style=\"text-align:center\">100%</td>";

                $bodyHTMLC40 .= "<td class=\"clear\"></td>";
                $bodyHTMLC40 .= "<td class=\"left\">" . number_format($col_3, 0, '', '.') . "</td>";
                $bodyHTMLC40 .= "<td>" . number_format($col_4, 0, '', '.') . "</td>";
                $bodyHTMLC40 .= $objSempat->calculoDiferencia($col_3, $col_4);
                $bodyHTMLC40 .= "<td style=\"text-align:center\">100%</td>";

                $bodyHTMLC40 .= "<td class=\"clear\"></td>";
                $bodyHTMLC40 .= "<td class=\"left\">" . number_format($col_5, 0, '', '.') . "</td>";
                $bodyHTMLC40 .= "<td>" . number_format($col_6, 0, '', '.') . "</td>";
                $bodyHTMLC40 .= $objSempat->calculoDiferencia($col_5, $col_6);
                $bodyHTMLC40 .= "<td style=\"text-align:center\">100%</td>";
                $bodyHTMLC40 .= "</tr>";

                $bodyHTMLC40 .= "</tbody>";

            }                        
        }
        //</editor-fold>   

        echo  "<div id='divDatosGrafico' style='display:none'>" .  $htmlTablaGrafico . "</div><table id=\"tablaValores\" class=\"tablaValores\" cellspacing=\"0\" cellspadding=\"0\">". $theadHTML . $tbodyHTML . $bodyHTMLC20 . $bodyHTMLC40 . "</table>";
    }
    else{
        echo "";
    }
            
}
//</editor-fold>


function retornarPeriodo($valor){    
    $meses = array("", "ENE", "FEB", "MAR", "ABR", "MAY", "JUN", "JUL", "AGO", "SEP", "OCT", "NOV", "DIC");
    
    $datos = split("-",$valor);
    
    return $meses[$datos[1]*1] . "-" . $datos[0];            
    
}




//echo $salida;


?>
