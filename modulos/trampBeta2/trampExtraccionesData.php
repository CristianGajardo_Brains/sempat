<?PHP

session_start();
include ("../../librerias/conexion.php");
require('tramp.class.php');
$objTramp = new tramp();


$usuarioId = $_SESSION['SEMPAT_usuarioId'];
$clienteId = $_SESSION['SEMPAT_clienteId'];

$fechaDesde = mb_convert_encoding(trim($_POST['fechaDesde']), "ISO-8859-1", "UTF-8");
$fechaHasta = mb_convert_encoding(trim($_POST['fechaHasta']), "ISO-8859-1", "UTF-8");
$carpeta = mb_convert_encoding(trim($_POST['carpeta']), "ISO-8859-1", "UTF-8");


$nombreArchivo = mb_convert_encoding(trim($_POST['nombreArchivo']), "ISO-8859-1", "UTF-8");

$etapa = mb_convert_encoding(trim($_POST['etapa']), "ISO-8859-1", "UTF-8");

$tipoDato = mb_convert_encoding(trim($_POST['tipoDato']), "ISO-8859-1", "UTF-8");

$puerto = mb_convert_encoding(trim($_POST['puerto']), "ISO-8859-1", "UTF-8");
$nave = mb_convert_encoding(trim($_POST['nave']), "ISO-8859-1", "UTF-8");
$shipper = mb_convert_encoding(trim($_POST['shipper']), "ISO-8859-1", "UTF-8");
$consignee = mb_convert_encoding(trim($_POST['consignee']), "ISO-8859-1", "UTF-8");
$tipoNave = mb_convert_encoding(trim($_POST['tipoNave']), "ISO-8859-1", "UTF-8");
$trafico = mb_convert_encoding(trim($_POST['trafico']), "ISO-8859-1", "UTF-8");
$sucursal = mb_convert_encoding(trim($_POST['sucursal']), "ISO-8859-1", "UTF-8");
$paisExtranjero = mb_convert_encoding(trim($_POST['paisExtranjero']), "ISO-8859-1", "UTF-8");
$puertoExtranjero = mb_convert_encoding(trim($_POST['puertoExtranjero']), "ISO-8859-1", "UTF-8");
$tipoServicio = mb_convert_encoding(trim($_POST['tipoServicio']), "ISO-8859-1", "UTF-8");
$tipoCarga = mb_convert_encoding(trim($_POST['tipoCarga']), "ISO-8859-1", "UTF-8");
$agentePortuario = mb_convert_encoding(trim($_POST['agentePortuario']), "ISO-8859-1", "UTF-8");
$naviera = mb_convert_encoding(trim($_POST['naviera']), "ISO-8859-1", "UTF-8");
$granFamilia = mb_convert_encoding(trim($_POST['granFamilia']), "ISO-8859-1", "UTF-8");
$familia = mb_convert_encoding(trim($_POST['familia']), "ISO-8859-1", "UTF-8");
$subFamilia = mb_convert_encoding(trim($_POST['subFamilia']), "ISO-8859-1", "UTF-8");
$commodity = mb_convert_encoding(trim($_POST['commodity']), "ISO-8859-1", "UTF-8");
$cargoGroup = mb_convert_encoding(trim($_POST['cargoGroup']), "ISO-8859-1", "UTF-8");

$chkpuerto = mb_convert_encoding(trim($_POST['chkpuerto']), "ISO-8859-1", "UTF-8");
$chknave = mb_convert_encoding(trim($_POST['chknave']), "ISO-8859-1", "UTF-8");
$chkshipper = mb_convert_encoding(trim($_POST['chkshipper']), "ISO-8859-1", "UTF-8");
$chkconsignee = mb_convert_encoding(trim($_POST['chkconsignee']), "ISO-8859-1", "UTF-8");
$chktipoNave = mb_convert_encoding(trim($_POST['chktipoNave']), "ISO-8859-1", "UTF-8");
$chktrafico = mb_convert_encoding(trim($_POST['chktrafico']), "ISO-8859-1", "UTF-8");
$chksucursal = mb_convert_encoding(trim($_POST['chksucursal']), "ISO-8859-1", "UTF-8");
$chkpaisExtranjero = mb_convert_encoding(trim($_POST['chkpaisExtranjero']), "ISO-8859-1", "UTF-8");
$chkpuertoExtranjero = mb_convert_encoding(trim($_POST['chkpuertoExtranjero']), "ISO-8859-1", "UTF-8");
$chktipoServicio = mb_convert_encoding(trim($_POST['chktipoServicio']), "ISO-8859-1", "UTF-8");
$chktipoCarga = mb_convert_encoding(trim($_POST['chktipoCarga']), "ISO-8859-1", "UTF-8");
$chkagentePortuario = mb_convert_encoding(trim($_POST['chkagentePortuario']), "ISO-8859-1", "UTF-8");
$chknaviera = mb_convert_encoding(trim($_POST['chknaviera']), "ISO-8859-1", "UTF-8");
$chkgranFamilia = mb_convert_encoding(trim($_POST['chkgranFamilia']), "ISO-8859-1", "UTF-8");
$chkfamilia = mb_convert_encoding(trim($_POST['chkfamilia']), "ISO-8859-1", "UTF-8");
$chksubFamilia = mb_convert_encoding(trim($_POST['chksubFamilia']), "ISO-8859-1", "UTF-8");
$chkcommodity = mb_convert_encoding(trim($_POST['chkcommodity']), "ISO-8859-1", "UTF-8");
$chkcargoGroup = mb_convert_encoding(trim($_POST['chkcargoGroup']), "ISO-8859-1", "UTF-8");


$uploaddir = "../../Reportes/" . $carpeta . "/";
			
if (file_exists($uploaddir)==false) {
    mkdir($uploaddir, 0700, true);
}

$uploaddir = $uploaddir . "/" . $usuarioId . "/";

if (file_exists($uploaddir)==false) {
    mkdir($uploaddir, 0700, true);
}

$objTramp->trampExtracciones($fechaDesde, $fechaHasta, $tipoDato, $clienteId, $etapa, $agentePortuario, $naviera, $nave, $tipoNave, $puerto, $paisExtranjero, $puertoExtranjero, $sucursal, $trafico, $shipper, $consignee, $tipoCarga, $tipoServicio, $cargoGroup, $granFamilia, $familia, $subFamilia, $commodity, $chkagentePortuario, $chknaviera, $chknave, $chktipoNave, $chkpuerto, $chkpaisExtranjero, $chkpuertoExtranjero, $chksucursal, $chktrafico, $chkshipper, $chkconsignee, $chktipoCarga, $chktipoServicio, $chkcargoGroup, $chkgranFamilia, $chkfamilia, $chksubFamilia, $chkcommodity, $nombreArchivo, $usuarioId);

?>
