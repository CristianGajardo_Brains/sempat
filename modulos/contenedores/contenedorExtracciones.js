

function buscarExtracciones(){
    
    var nombreArchivo = $("#inputNombreArchivo").val();
    var carpeta = $("#carpeta").val();
    
    var fechaDesde = $("#fechaDesde").val();
    var fechaHasta = $("#fechaHasta").val();
    
    var mercado = "";

    var chile = $("input[id=btnChile]:checked").val();
    var peru = $("input[id=btnPeru]:checked").val();
    var ecuador = $("input[id=btnEcuador]:checked").val();
    var colombia = $("input[id=btnColombia]:checked").val();


    mercado = chile + "," + peru + "," + ecuador + "," + colombia;                                                         
    mercado = replaceAll(mercado, ",undefined",""); 
    mercado = replaceAll(mercado, "undefined",""); 

    if(chile == undefined){
        mercado = mercado.substring(1, mercado.length);
    }

    if(fechaDesde == "" && fechaHasta == ""){

        fechasDefecto(mercado); 
        fechaDesde = $("#fechaDesde").val();
        fechaHasta = $("#fechaHasta").val();    
    }
    
    if(validarFormulario()){

        $("#divResultado").html("");  

        $("#imgCargando").css("display","block");
        //$("#divPanelFiltros").css("display","none");            
        //$("#divMensaje").css("display","none");

        var puertoOrigen = $("#manPuertoOrigen").val();
        var puertoEmbarque = $("#manPuertoEmbarque").val();
        var puertoDescarga = $("#manPuertoDescarga").val();
        var puertoDestino = $("#manPuertoDestino").val();
        var paisOrigen = $("#manPaisOrigen").val();
        var paisEmbarque = $("#manPaisEmbarque").val();
        var paisDescarga = $("#manPaisDescarga").val();
        var paisDestino = $("#manPaisDestino").val();
        var trafico = $("#manTrafico").val();
        var naviera = $("#manNaviera").val();
        var agencia = $("#manAgencia").val();
        var agenteDoc = $("#manAgenteDoc").val();
        var nave = $("#manNave").val();
        var tipoNave = $("#manTipoNave").val();
        var shipper = $("#manShipper").val();
        var consignee = $("#manConsignee").val();
        var granFamilia = $("#manGranFamilia").val();
        var familia = $("#manFamilia").val();
        var subFamilia = $("#manSubFamilia").val();
        var commodity = $("#manCommodity").val();
        var tipoCarga = $("#manTipoCarga").val();
        var tipoServicio = $("#manTipoServicio").val();
        var tipoEmbalaje = $("#manTipoEmbalaje").val();
        var statusContainer = $("#manStatusContainer").val();


        var chkpuertoOrigen = $("#chkmanPuertoOrigen").attr('checked');
        var chkpuertoEmbarque = $("#chkmanPuertoEmbarque").attr('checked');
        var chkpuertoDescarga = $("#chkmanPuertoDescarga").attr('checked');
        var chkpuertoDestino = $("#chkmanPuertoDestino").attr('checked');
        var chkpaisOrigen = $("#chkmanPaisOrigen").attr('checked');
        var chkpaisEmbarque = $("#chkmanPaisEmbarque").attr('checked');
        var chkpaisDescarga = $("#chkmanPaisDescarga").attr('checked');
        var chkpaisDestino = $("#chkmanPaisDestino").attr('checked');
        var chktrafico = $("#chkmanTrafico").attr('checked');
        var chknaviera = $("#chkmanNaviera").attr('checked');
        var chkagencia = $("#chkmanAgencia").attr('checked');
        var chkagenteDoc = $("#chkmanAgenteDoc").attr('checked');
        var chknave = $("#chkmanNave").attr('checked');
        var chktipoNave = $("#chkmanTipoNave").attr('checked');
        var chkshipper = $("#chkmanShipper").attr('checked');
        var chkconsignee = $("#chkmanConsignee").attr('checked');
        var chkgranFamilia = $("#chkmanGranFamilia").attr('checked');
        var chkfamilia = $("#chkmanFamilia").attr('checked');
        var chksubFamilia = $("#chkmanSubFamilia").attr('checked');
        var chkcommodity = $("#chkmanCommodity").attr('checked');
        var chktipoCarga = $("#chkmanTipoCarga").attr('checked');
        var chktipoServicio = $("#chkmanTipoServicio").attr('checked');
        var chktipoEmbalaje = $("#chkmanTipoEmbalaje").attr('checked');
        var chkstatusContainer = $("#chkmanStatusContainer").attr('checked');

        var tipoDato = "";
        
        if($("#btnContenedores").prop('checked')){
            tipoDato = tipoDato + "contenedores," 
        }        
        
        if($("#btnTeus").prop('checked')){
            tipoDato = tipoDato + "teus," 
        } 
        
        if($("#btnToneladas").prop('checked')){
            tipoDato = tipoDato + "toneladas," 
        }                                        
		
		if($("#btnFob").prop('checked')){
            tipoDato = tipoDato + "fob," 
        } 
		
		if($("#btnCif").prop('checked')){
            tipoDato = tipoDato + "cif," 
        } 

        var etapa = $("input[name=btnEtapa]:checked").val();
        var contenedor = "C";
        var dryReefer = $("input[name=btnSecaRefrigerada]:checked").val();
        

        $.ajax({
            type: "POST",
            url: "manifiestoExtraccionesData.php",
            data: { fechaDesde: fechaDesde, fechaHasta: fechaHasta, puertoOrigen: puertoOrigen, puertoEmbarque: puertoEmbarque, puertoDescarga: puertoDescarga, puertoDestino: 
                    puertoDestino, paisOrigen: paisOrigen, paisEmbarque: paisEmbarque, paisDescarga: paisDescarga, paisDestino: paisDestino, trafico: trafico, naviera: naviera, 
                agencia: agencia, agenteDoc: agenteDoc, nave: nave, tipoNave: tipoNave, shipper: shipper, consignee: consignee, granFamilia: granFamilia, familia: familia, 
                subFamilia: subFamilia, commodity: commodity, tipoCarga: tipoCarga, tipoServicio: tipoServicio, tipoEmbalaje: tipoEmbalaje, statusContainer: statusContainer, 

                chkpuertoOrigen: chkpuertoOrigen, chkpuertoEmbarque: chkpuertoEmbarque, chkpuertoDescarga: chkpuertoDescarga, chkpuertoDestino: chkpuertoDestino, chkpaisOrigen: chkpaisOrigen, 
                chkpaisEmbarque: chkpaisEmbarque, chkpaisDescarga: chkpaisDescarga, chkpaisDestino: chkpaisDestino, chktrafico: chktrafico, chknaviera: chknaviera, chkagencia: chkagencia, 
                chkagenteDoc: chkagenteDoc, chknave: chknave, chktipoNave: chktipoNave, chkshipper: chkshipper, chkconsignee: chkconsignee, chkgranFamilia: chkgranFamilia, 
                chkfamilia: chkfamilia, chksubFamilia: chksubFamilia, chkcommodity: chkcommodity, chktipoCarga: chktipoCarga, chktipoServicio: chktipoServicio, 
                chktipoEmbalaje: chktipoEmbalaje, chkstatusContainer: chkstatusContainer, 

                nombreArchivo: nombreArchivo,tipoDato:tipoDato, mercado: mercado, etapa: etapa, contenedor: contenedor, dryReefer: dryReefer, carpeta:carpeta}
            })
            .done(function( msg ) {
                buscarExtraccionesArchivos($("#moduloId").val());
                $("#imgCargando").css("display","none");                                           

        }); 

    }                                                                    

}  


function validarFormulario(){

    var mensaje = "";

    var nombreArchivo = $("#inputNombreArchivo").val();
    var fechaDesde = $("#fechaDesde").val();
    var fechaHasta = $("#fechaHasta").val();                                 
            
    if(nombreArchivo == ""){
        mensaje = "Debe ingresar un nombre para el archivo.<br>";
    }
    
    if(fechaDesde == ""){
        mensaje += "No ha ingresado la fecha inicial.<br>";
    }
    
    if(fechaHasta == ""){
        mensaje += "No ha ingresado la fecha final.<br>";
    }
    
    if(fechaDesde != "" && fechaHasta != "" && validarFechaMenor(fechaDesde, fechaHasta) == false){
        mensaje += "La fecha incial no puede ser mayor a la fecha final.<br>";
    }
    
    if(fechaDesde != "" && fechaHasta != "" && fechaDiferenciasDias(fechaDesde, fechaHasta) > 365){
        mensaje += "La diferencia entre las fechas no puede ser superior a un año.<br>";
    }
            
    if(mensaje != ""){
        $("#divMensaje").html(mensaje);        
        return false;
    }
    else{
        $("#divMensaje").html("");        
        return true;
    }

}
    
    
    