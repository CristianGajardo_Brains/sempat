

function buscarExtracciones(){
    
    var nombreArchivo = $("#inputNombreArchivo").val();
    var carpeta = $("#carpeta").val();

    var fechaDesde = $("#fechaDesde").val();
    var fechaHasta = $("#fechaHasta").val();
    
    
    if(fechaDesde == "" && fechaHasta == ""){

        fechasDefecto("BUNKERING");
        fechaDesde = $("#fechaDesde").val();
        fechaHasta = $("#fechaHasta").val();
    }
    
    if(validarFormulario()){

        $("#divResultado").html("");  

        $("#imgCargando").css("display","block");
        //$("#divPanelFiltros").css("display","none");            
        //$("#divMensaje").css("display","none");

        var embarcador = $("#embarcador").val();
        var naviera = $("#naviera").val();
        var nave = $("#nave").val();
        var tipoNave = $("#tipoNave").val();
        var puertoOrigen = $("#puertoOrigen").val();
        var proveedor = $("#proveedor").val();
        var producto = $("#producto").val();
        var detalle = $("#detalle").val();


        var chkembarcador = $("#chkembarcador").attr('checked');
        var chknaviera = $("#chknaviera").attr('checked');
        var chknave = $("#chknave").attr('checked');
        var chktipoNave = $("#chktipoNave").attr('checked');
        var chkpuertoOrigen = $("#chkpuertoOrigen").attr('checked');
        var chkproveedor = $("#chkproveedor").attr('checked');
        var chkproducto = $("#chkproducto").attr('checked');
        var chkdetalle = $("#chkdetalle").attr('checked');

        var tipoDato = "";
        
        if($("#btnToneladas").prop('checked')){
            tipoDato = tipoDato + "toneladas," 
        }        
        
        if($("#btnFobus").prop('checked')){
            tipoDato = tipoDato + "fobus," 
        } 

        var etapa = $("input[name=btnEtapa]:checked").val();
                
        $.ajax({
            type: "POST",
            url: "bunkeringExtraccionesData.php",
            data: { fechaDesde: fechaDesde, fechaHasta: fechaHasta, embarcador: embarcador, naviera: naviera, nave: nave, tipoNave: tipoNave, 
                puertoOrigen: puertoOrigen, proveedor: proveedor, producto: producto, detalle: detalle, 

                chkembarcador: chkembarcador, chknaviera: chknaviera, chknave: chknave, chktipoNave: chktipoNave, 
                chkpuertoOrigen: chkpuertoOrigen, chkproveedor: chkproveedor, chkproducto: chkproducto, chkdetalle: chkdetalle, 

                nombreArchivo: nombreArchivo,tipoDato:tipoDato, etapa: etapa, carpeta: carpeta}
            })
            .done(function( msg ) {
                buscarExtraccionesArchivos($("#moduloId").val());
                $("#imgCargando").css("display","none");                                           

        }); 

    }                                                                    

}


function validarFormulario(){

    var mensaje = "";

    var nombreArchivo = $("#inputNombreArchivo").val();
    var fechaDesde = $("#fechaDesde").val();
    var fechaHasta = $("#fechaHasta").val();                                 
            
    if(nombreArchivo == ""){
        mensaje = "Debe ingresar un nombre para el archivo.<br>";
    }
    
    if(fechaDesde == ""){
        mensaje += "No ha ingresado la fecha inicial.<br>";
    }
    
    if(fechaHasta == ""){
        mensaje += "No ha ingresado la fecha final.<br>";
    }
    
    if(fechaDesde != "" && fechaHasta != "" && validarFechaMenor(fechaDesde, fechaHasta) == false){
        mensaje += "La fecha incial no puede ser mayor a la fecha final.<br>";
    }
    
    if(fechaDesde != "" && fechaHasta != "" && fechaDiferenciasDias(fechaDesde, fechaHasta) > 365){
        mensaje += "La diferencia entre las fechas no puede ser superior a un año.<br>";
    }
            
    if(mensaje != ""){
        $("#divMensaje").html(mensaje);        
        return false;
    }
    else{
        $("#divMensaje").html("");        
        return true;
    }

}
    
    
    