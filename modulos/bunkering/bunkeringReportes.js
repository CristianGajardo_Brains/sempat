
var count = 0;


function filtros(){

    var filtros = '';
    var mensaje = '';
    count = 0;

    filtros += filtrosValores($("#embarcador"), "Embarcador");
    filtros += filtrosValores($("#naviera"), "Naviera");
    filtros += filtrosValores($("#nave"), "Nave");
    filtros += filtrosValores($("#tipoNave"), "Tipo de Nave");
    filtros += filtrosValores($("#puertoOrigen"), "Puerto Origen");
    filtros += filtrosValores($("#proveedor"), "Proveedor");
    filtros += filtrosValores($("#producto"), "Producto");
    filtros += filtrosValores($("#detalle"), "Detalle"); 

    if(count > 0){
        mensaje = "<font color='#3D85FE' size='2'>Filtros:</font><br>"                
    }
                    
    if(count > 4){
        mensaje += "<div style='height:60px; overflow-y: scroll'>" + filtros + "</div>";
        
    }
    else{
        mensaje += "<div style='height:60px'>" + filtros + "</div>";
    }

    return  mensaje;


}


function buscarNivel1(){

    var nivel1 = $("#selectNivel1").val();
    var nivel2 = $("#selectNivel2").val();
    var nivel3 = $("#selectNivel3").val();
    var nivel4 = $("#selectNivel4").val();
    var nivel5 = $("#selectNivel5").val();

    var fechaDesde = $("#fechaDesde").val();
    var fechaHasta = $("#fechaHasta").val();


    if(fechaDesde == "" && fechaHasta == ""){

        fechasDefecto("BUNKERING"); 
        fechaDesde = $("#fechaDesde").val();
        fechaHasta = $("#fechaHasta").val();

    }


    if(validarFormulario()){

        $("#divResultado").html("");  
        $("#tablaNivelesDesc").html("");   

        $("#imgCargando").css("display","block");
        $("#divPanelFiltros").css("display","none");
        $("#divOpciones").css("display","none");
        $("#divGraficos").css("display","none");
        $("#divGrafico").css("display","none");
        $("#divGraficoA").css("display","none");
        $("#divGraficoB").css("display","none");
        $("#divMensaje").css("display","none");

        var embarcador = $("#embarcador").val();
        var naviera = $("#naviera").val();
        var nave = $("#nave").val();
        var tipoNave = $("#tipoNave").val();
        var puertoOrigen = $("#puertoOrigen").val();
        var proveedor = $("#proveedor").val();
        var producto = $("#producto").val();
        var detalle = $("#detalle").val();

        var nivel1Titulo = $("#selectNivel1 option:selected").text();
                
        $('#selectGraficoReporte').val("line");
        $('#selectselectGrafico').html("Líneas");

        var tipoDato = $("input[name=tipoDato]:checked").val();

        switch(tipoDato){
            case 'fobus':
                $('#tituloGrafico').val("USD");
                $('#tituloGraficoEjeY').val("USD");                
                $('.tdTipo').css("display", "");
                break;
            
            case 'toneladas':
                $('#tituloGrafico').val("N° de Toneladas");
                $('#tituloGraficoEjeY').val("N° de Toneladas");
                $('.tdTipo').css("display", "none");
                break;
        }

        $('#tdResumen').html("<center><font color='#3D85FE' size='3'>" + $('#tituloGrafico').val() + "</font><br></center>" + filtros());


        $.ajax({
            type: "POST",
            url: "bunkeringReporteNivel1.php",
            //async: false,
            data: {fechaDesde: fechaDesde, fechaHasta: fechaHasta, embarcador: embarcador, naviera: naviera, nave: nave, tipoNave: tipoNave, 
                puertoOrigen: puertoOrigen, proveedor: proveedor, producto: producto, detalle: detalle, 
                tipoDato:tipoDato, nivel1: nivel1, nivel2: nivel2, nivel3: nivel3, nivel4: nivel4, nivel5: nivel5, nivel1Titulo: nivel1Titulo}
            })
            .done(function( msg ) {
                
                if(msg.trim() != ""){
                                    
                    $("#divResultado").html(msg);                 
                    $("#divOpciones").css("display","block");

                    $("#tBody tr").each(function (index) {

                        if(index < 3){

                            $(this).children("td").each(function (index2, td) {

                                if(index2 == 0){

                                    $(td).children("input").each(function (index3, input) {
                                        input.click();
                                    });

                                    $(td).children("span ").each(function (index3, span) {
                                        $(span).removeClass('checkboxC');
                                        $(span).addClass('checkboxChecked');
                                    });

                                }                                                
                            });
                        }                    
                    });


                    var nivelesDesc = "";                

                    if(nivel1 != "")
                        nivelesDesc = nivelesDesc + "<tr><td><div class='nivel1'></div></td><td class='desc'>Nivel 1: " + nivel1Titulo + "</td></tr>"; 

                    if(nivel2.trim() != ""){
                        var nivel2Titulo = $("#selectNivel2 option:selected").text();
                        nivelesDesc = nivelesDesc + "<tr><td><div class='nivel2'></div></td><td class='desc'>Nivel 2: " + nivel2Titulo + "</td></tr>"; 
                    }

                    if(nivel3 != ""){
                        var nivel3Titulo = $("#selectNivel3 option:selected").text();
                        nivelesDesc = nivelesDesc + "<tr><td><div class='nivel3'></div></td><td class='desc'>Nivel 3: " + nivel3Titulo + "</td></tr>"; 
                    }

                    if(nivel4 != ""){
                        var nivel4Titulo = $("#selectNivel4 option:selected").text();
                        nivelesDesc = nivelesDesc + "<tr><td><div class='nivel4'></div></td><td class='desc'>Nivel 4: " + nivel4Titulo + "</td></tr>"; 
                    }

                    if(nivel5 != ""){
                        var nivel5Titulo = $("#selectNivel5 option:selected").text();
                        nivelesDesc = nivelesDesc + "<tr><td><div class='nivel5'></div></td><td class='desc'>Nivel 5: " + nivel5Titulo + "</td></tr>"; 
                    }                   

                    Custom.initChkbox();
                    
                    $("#divOpciones").css("display","block");
                    $("#tablaNivelesDesc").html(nivelesDesc);
                }
                else{
                    $('#divResultado').html("<div style='margin-left:30px'><label style='color:red'>Búsqueda sin resultados.</label></div>");
                }

                $("#imgCargando").css("display","none");

        }); 

    }    

}


function buscarNivel2(obj, fechaDesde, fechaHasta, nivel2, nivel3, nivel4, nivel5, tipoDato, nivel1Id, otros){

    var tr = obj.parentNode;  
    var className = $(tr).hasClass('OK');                

    if(className == false && nivel2 != ""){

        $.ajax({                
            type: "POST",
            url: "bunkeringReporteNivel2.php",
            async: false,
            data: {fechaDesde: fechaDesde, fechaHasta: fechaHasta, tipoDato:tipoDato, nivel2: nivel2, nivel3: nivel3, nivel4: nivel4, nivel5: nivel5, trId: $(tr).attr('id'), nivel1Id: nivel1Id, otros: otros}
            })
            .done(function(msg ) {
                $(tr).addClass('OK');            
                $(msg).insertAfter(tr);
        });                
    }
    else{                        
        desplegarNivel($(tr).attr('id'), 2);
    }
}


function buscarNivel3(obj, fechaDesde, fechaHasta, nivel3, nivel4, nivel5, tipoDato, nivel1Id, nivel2Id, otros){

    var tr = obj.parentNode;  
    var className = $(tr).hasClass('OK');                

    if(className == false && nivel3 != ""){

        $.ajax({
            type: "POST",
            url: "bunkeringReporteNivel3.php",
            async: false,
            data: {fechaDesde: fechaDesde, fechaHasta: fechaHasta, tipoDato:tipoDato, nivel3: nivel3, nivel4: nivel4, nivel5: nivel5, trId: $(tr).attr('id'), nivel1Id: nivel1Id, nivel2Id: nivel2Id, otros: otros}
            })
            .done(function(msg ) {
                $(tr).addClass('OK');            
                $(msg).insertAfter(tr);
        });                
    }
    else{                        
        desplegarNivel($(tr).attr('id'), 3);
    }
}


function buscarNivel4(obj, fechaDesde, fechaHasta, nivel4, nivel5, tipoDato, nivel1Id, nivel2Id, nivel3Id, otros){

    var tr = obj.parentNode;  
    var className = $(tr).hasClass('OK');                

    if(className == false && nivel4 != ""){

        $.ajax({
            type: "POST",
            url: "bunkeringReporteNivel4.php",
            async: false,
            data: {fechaDesde: fechaDesde, fechaHasta: fechaHasta, tipoDato:tipoDato, nivel4: nivel4, nivel5: nivel5, trId: $(tr).attr('id'), nivel1Id: nivel1Id, nivel2Id: nivel2Id, nivel3Id: nivel3Id, otros: otros}
            })
            .done(function(msg ) {
                $(tr).addClass('OK');            
                $(msg).insertAfter(tr);
        });                
    }
    else{                        
        desplegarNivel($(tr).attr('id'), 4);
    }
}


function buscarNivel5(obj, fechaDesde, fechaHasta, nivel5, tipoDato, nivel1Id, nivel2Id, nivel3Id, nivel4Id, otros){

    var tr = obj.parentNode;  
    var className = $(tr).hasClass('OK');                

    if(className == false && nivel5 != ""){

        $.ajax({
            type: "POST",
            url: "bunkeringReporteNivel5.php",
            async: false,
            data: {fechaDesde: fechaDesde, fechaHasta: fechaHasta, tipoDato:tipoDato, nivel5: nivel5, trId: $(tr).attr('id'), nivel1Id: nivel1Id, nivel2Id: nivel2Id, nivel3Id: nivel3Id, nivel4Id: nivel4Id, otros: otros}
            })
            .done(function(msg ) {
                $(tr).addClass('OK');            
                $(msg).insertAfter(tr);                                                            
        });                
    }
    else{                        
        desplegarNivel($(tr).attr('id'), 5);
    }
}

    
function validarFormulario(){

    var mensaje = "";

    var nivel1 = $("#selectNivel1").val();
    var fechaDesde = $("#fechaDesde").val();
    var fechaHasta = $("#fechaHasta").val();
            
    if(nivel1 == ""){
        mensaje = "Debe seleccionar el primer nivel.<br>";
    }
    
    if(fechaDesde == ""){
        mensaje += "No ha ingresado la fecha inicial.<br>";
    }
    
    if(fechaHasta == ""){
        mensaje += "No ha ingresado la fecha final.<br>";
    }
    
    if(fechaDesde != "" && fechaHasta != "" && validarFechaMenor(fechaDesde, fechaHasta) == false){
        mensaje += "La fecha incial no puede ser mayor a la fecha final.<br>";
    }
    
    if(fechaDesde != "" && fechaHasta != "" && fechaDiferenciasDias(fechaDesde, fechaHasta) > 365){
        mensaje += "La diferencia entre las fechas no puede ser superior a un año.<br>";
    }
            
    if(mensaje != ""){
        $("#divMensaje").html(mensaje);
        $("#divMensaje").css("display","block");
        return false;
    }
    else{
        $("#divMensaje").html("");
        $("#divMensaje").css("display","none");
        return true;
    }

}
    
    
    