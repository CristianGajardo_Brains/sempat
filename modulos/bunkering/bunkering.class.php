<?php

class bunkering {
    
    
    function ultimaFechaManifiesto($clienteId) {
        $query = "Exec seleccionarFechasLimites '" . $clienteId . "','M'";
        $result = sql_db::sql_query($query);
        sql_db::sql_close();
        $row = sql_db::sql_fetch_assoc($result);
        return $row;
    }
    
    
    function manifiestoFechasDefecto($clienteId) {
        $query = "Exec seleccionarFechasDefecto '" . $clienteId . "','M'";
        $result = sql_db::sql_query($query);
        sql_db::sql_close();
        $row = sql_db::sql_fetch_assoc($result);
        return $row;
    }
    
    
    //INDICADORES
    
    function indicadoresCabeceraDatos($fecha) {
        $query = "Exec manifiestoIndicadoresCabecera '" . $fecha . "'";        
        //echo $query;
        $result = sql_db::sql_query($query);
        $row = sql_db::sql_fetch_assoc($result);
        sql_db::sql_close();
        return $row;
    }
    
    
    function indicadoresCabeceraHtml($valorNivel1, $fecha){
        
        $objSem = new bunkering();
        $resultado = $objSem->indicadoresCabeceraDatos($fecha);
        
        $titulo1 = "ENERO";
        $titulo2 = "";
        $titulo3 = "";
        $titulo2b = "";
        $titulo3b = "";
        
        if(strtoupper($resultado["mmActual"]) != "ENERO"){
            $titulo1 = "ENERO - " . strtoupper($resultado["mmActual"]);
        }

        if($resultado["mmAnterior"] != "" && $resultado["mmActual"] != ""){
            $titulo2 = $resultado["mmAnterior"] . " - " . $resultado["mmActual"];
        }
        
        if($resultado["mmActual"] != ""){
            $titulo3 = $resultado["mmActual"];
        }
        
        if($resultado["mmActual"] == "DICIEMBRE"){
            $titulo2b = $resultado["yyyyActual"];
        }
        else{
            if($resultado["yyyyAnterior"] != ""){
                $titulo2b = $resultado["yyyyAnterior"] . " - " . ($resultado["yyyyActual"]);
            }            
        }
        
        if($resultado["mmActual"] == "DICIEMBRE") {
            $titulo3b = $resultado["yyyyAnterior"];            
        }
        else {            
            if($resultado["yyyyAnterior"] != ""){
                $titulo3b = ($resultado["yyyyAnterior"] - 1) . " - " . ($resultado["yyyyAnterior"]);                
            }                        
        }
        
        
        $htmlThead = "<tr>
                        <th class=\"thCenter\" colspan=\"6\">
                            <div class=\"divThLeft\"></div>
                            <div class=\"divThCenter\">" . $titulo1 . "</div>
                            <div class=\"divThRight\"></div>          
                        </th>                        
                        <th style=\"min-width: 10px;\"></th>
                        <th class=\"thCenter\" colspan=\"4\">
                            <div class=\"divThLeft\"></div>          
                            <div class=\"divThCenter\">" . $titulo2 . "</div>
                            <div class=\"divThRight\"></div>          
                        </th> 
                        <th style=\"min-width: 10px;\"></th>
                        <th class=\"thCenter\" colspan=\"4\">
                            <div class=\"divThLeft\"></div>          
                            <div class=\"divThCenter\">" . $titulo3 . "</div>
                            <div class=\"divThRight\"></div>          
                        </th> 
                    </tr>   
                    <tr>
                        <td colspan=\"2\" class=\"left\" style=\"width:185px;\">
                                " . mb_convert_encoding(trim($valorNivel1), "UTF-8", "ISO-8859-1") .  "
                        </td>
                        <td>
                            " . $resultado["yyyyActual"] . "
                        </td>
                        <td>
                            " . $resultado["yyyyAnterior"] . "
                        </td>
                        <td>
                            DIF.
                        </td>
                        <td>
                            Mkt. SHARE
                        </td>
                        <td class=\"clear\">

                        </td>
                        <td class=\"left\">
                            " . $titulo2b .  "
                        </td>
                        <td>
                            " . $titulo3b .  "
                        </td>
                        <td>
                            DIF.
                        </td>
                        <td>
                            Mkt. SHARE
                        </td>
                        <td class=\"clear\">

                        </td>
                        <td class=\"left\">
                            " . $resultado["yyyyActual"] . "
                        </td>
                        <td>
                            " . $resultado["yyyyAnterior"] . "
                        </td>
                        <td>
                            DIF.
                        </td>
                        <td>
                            Mkt. SHARE
                        </td>                        
                    </tr>";

        return $htmlThead;       

    }
    
    
    function bunkeringIndicadoresNivel1($fechaDesde, $fechaHasta, $nivel1, $nivel2, $nivel3, $nivel4, $nivel5, $tipoDato, $clienteId, $embarcador, $naviera, $nave, $tipoNave, 
            $puertoOrigen, $proveedor, $producto, $detalle, $usuarioId){
    
        
        /*$query = "Exec bunkeringIndicadoresNivel1 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel1 . "','" . $nivel2 . "','" . $nivel3 . "','" . $nivel4 . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $embarcador . "','" . $naviera . "','" . $nave . "','" . $tipoNave . "','" . $puertoOrigen . "','" . $proveedor . "','" . 
                $producto . "','" . $detalle . "','" . $usuarioId . "'";        
        echo $query;*/
        
        
        $stmt = mssql_init("bunkeringIndicadoresNivel1");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);
        mssql_bind($stmt, '@var1', $nivel1, SQLVARCHAR);
        mssql_bind($stmt, '@var2', $nivel2, SQLVARCHAR);
        mssql_bind($stmt, '@var3', $nivel3, SQLVARCHAR);
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);        
        mssql_bind($stmt, '@embarcador', $embarcador, SQLVARCHAR);
        mssql_bind($stmt, '@naviera', $naviera, SQLVARCHAR);
        mssql_bind($stmt, '@nave', $nave, SQLVARCHAR);
        mssql_bind($stmt, '@tipoNave', $tipoNave, SQLVARCHAR);
        mssql_bind($stmt, '@puertoOrigen', $puertoOrigen, SQLVARCHAR);
        mssql_bind($stmt, '@proveedor', $proveedor, SQLVARCHAR);
        mssql_bind($stmt, '@producto', $producto, SQLVARCHAR);
        mssql_bind($stmt, '@detalle', $detalle, SQLVARCHAR);
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    function bunkeringIndicadoresNivel1Grafico($fechaDesde, $fechaHasta, $nivel1, $usuarioId){
            
        /*$query = "Exec bunkeringIndicadoresNivel1Grafico '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel1 . "','" . $usuarioId . "'";        
        echo $query;*/
               
        $stmt = mssql_init("bunkeringIndicadoresNivel1Grafico");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);
        mssql_bind($stmt, '@var1', $nivel1, SQLVARCHAR);                                            
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
               
    
    function bunkeringIndicadoresNivel2($fechaDesde, $fechaHasta, $nivel2, $nivel3, $nivel4, $nivel5, $tipoDato, $clienteId, $nivel1Id, $otros, $usuarioId){
    
        
        /*$query = "Exec bunkeringIndicadoresNivel2 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel2 . "','" . $nivel3 . "','" . $nivel4 . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $nivel1Id . "','" . $otros . "','" . $usuarioId . "'";                
        echo $query;*/
                
        $stmt = mssql_init("bunkeringIndicadoresNivel2");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);        
        mssql_bind($stmt, '@var2', $nivel2, SQLVARCHAR);
        mssql_bind($stmt, '@var3', $nivel3, SQLVARCHAR);
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@nivel1Id', $nivel1Id, SQLVARCHAR);
        mssql_bind($stmt, '@otros', $otros, SQLVARCHAR);        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    function bunkeringIndicadoresNivel3($fechaDesde, $fechaHasta, $nivel3, $nivel4, $nivel5, $tipoDato, $clienteId, $nivel1Id, $nivel2Id, $otros, $usuarioId){
    
        
        /*$query = "Exec bunkeringIndicadoresNivel3 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel3 . "','" . $nivel4 . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $nivel1Id . "','" . $nivel2Id . "','" . $otros . "','" . $usuarioId . "'";        
        echo $query;*/
                
        $stmt = mssql_init("bunkeringIndicadoresNivel3");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);                
        mssql_bind($stmt, '@var3', $nivel3, SQLVARCHAR);
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@nivel1Id', $nivel1Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel2Id', $nivel2Id, SQLVARCHAR);
        mssql_bind($stmt, '@otros', $otros, SQLVARCHAR);        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    function bunkeringIndicadoresNivel4($fechaDesde, $fechaHasta, $nivel4, $nivel5, $tipoDato, $clienteId, $nivel1Id, $nivel2Id, $nivel3Id, $otros, $usuarioId){
            
        /*$query = "Exec bunkeringIndicadoresNivel4 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel4 . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $nivel1Id . "','" . $nivel2Id . "','" . $nivel3Id . "','" . $otros . "','" . $usuarioId . "'";      
        echo $query;*/
                
        $stmt = mssql_init("bunkeringIndicadoresNivel4");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);                        
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@nivel1Id', $nivel1Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel2Id', $nivel2Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel3Id', $nivel3Id, SQLVARCHAR);
        mssql_bind($stmt, '@otros', $otros, SQLVARCHAR);        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    function bunkeringIndicadoresNivel5($fechaDesde, $fechaHasta, $nivel5, $tipoDato, $clienteId, $nivel1Id, $nivel2Id, $nivel3Id, $nivel4Id, $otros, $usuarioId){
            
        /*$query = "Exec bunkeringIndicadoresNivel5 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $nivel1Id . "','" . $nivel2Id . "','" . $nivel3Id . "','" . $nivel4Id . "','" . $otros . "','" . $usuarioId . "'";        
        echo $query;*/
                
        $stmt = mssql_init("bunkeringIndicadoresNivel5");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);                                
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@nivel1Id', $nivel1Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel2Id', $nivel2Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel3Id', $nivel3Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel4Id', $nivel4Id, SQLVARCHAR);
        mssql_bind($stmt, '@otros', $otros, SQLVARCHAR);        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    
    //REPORTES
    
    function bunkeringReporteNivel1($fechaDesde, $fechaHasta, $nivel1, $nivel2, $nivel3, $nivel4, $nivel5, $tipoDato, $clienteId, $embarcador, $naviera, $nave, $tipoNave, 
            $puertoOrigen, $proveedor, $producto, $detalle, $usuarioId){
    
        /*$query = "Exec bunkeringReporteNivel1 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel1 . "','" . $nivel2 . "','" . $nivel3 . "','" . $nivel4 . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $embarcador . "','" . $naviera . "','" . $nave . "','" . $tipoNave . "','" . $puertoOrigen . "','" . $proveedor . "','" . 
                $producto . "','" . $detalle . "','" . $usuarioId . "'";        
        echo $query;*/
                        
        
        $stmt = mssql_init("bunkeringReporteNivel1");

        mssql_bind($stmt, '@fechaDesdeA', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHastaA', $fechaHasta, SQLVARCHAR);
        mssql_bind($stmt, '@var1', $nivel1, SQLVARCHAR);
        mssql_bind($stmt, '@var2', $nivel2, SQLVARCHAR);
        mssql_bind($stmt, '@var3', $nivel3, SQLVARCHAR);
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);        
        mssql_bind($stmt, '@embarcador', $embarcador, SQLVARCHAR);
        mssql_bind($stmt, '@naviera', $naviera, SQLVARCHAR);
        mssql_bind($stmt, '@nave', $nave, SQLVARCHAR);
        mssql_bind($stmt, '@tipoNave', $tipoNave, SQLVARCHAR);
        mssql_bind($stmt, '@puertoOrigen', $puertoOrigen, SQLVARCHAR);
        mssql_bind($stmt, '@proveedor', $proveedor, SQLVARCHAR);
        mssql_bind($stmt, '@producto', $producto, SQLVARCHAR);
        mssql_bind($stmt, '@detalle', $detalle, SQLVARCHAR);                                    
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
          
    
    function bunkeringReporteNivel2($fechaDesde, $fechaHasta, $nivel2, $nivel3, $nivel4, $nivel5, $tipoDato, $clienteId, $nivel1Id, $otros, $usuarioId){
    
        
        $query = "Exec bunkeringReporteNivel2 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel2 . "','" . $nivel3 . "','" . $nivel4 . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $nivel1Id . "','" . $otros . "','" . $usuarioId . "'";        
        echo $query;
                
        $stmt = mssql_init("bunkeringReporteNivel2");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);        
        mssql_bind($stmt, '@var2', $nivel2, SQLVARCHAR);
        mssql_bind($stmt, '@var3', $nivel3, SQLVARCHAR);
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@nivel1Id', $nivel1Id, SQLVARCHAR);
        mssql_bind($stmt, '@otros', $otros, SQLVARCHAR);        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    function bunkeringReporteNivel3($fechaDesde, $fechaHasta, $nivel3, $nivel4, $nivel5, $tipoDato, $clienteId, $nivel1Id, $nivel2Id, $otros, $usuarioId){
    
        
        /*$query = "Exec bunkeringReporteNivel3 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel3 . "','" . $nivel4 . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $nivel1Id . "','" . $nivel2Id . "','" . $otros . "','" . $usuarioId . "'";        
        echo $query;*/
                
        $stmt = mssql_init("bunkeringReporteNivel3");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);                
        mssql_bind($stmt, '@var3', $nivel3, SQLVARCHAR);
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@nivel1Id', $nivel1Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel2Id', $nivel2Id, SQLVARCHAR);
        mssql_bind($stmt, '@otros', $otros, SQLVARCHAR);        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    function bunkeringReporteNivel4($fechaDesde, $fechaHasta, $nivel4, $nivel5, $tipoDato, $clienteId, $nivel1Id, $nivel2Id, $nivel3Id, $otros, $usuarioId){
            
        /*$query = "Exec bunkeringReporteNivel4 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel4 . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $nivel1Id . "','" . $nivel2Id . "','" . $nivel3Id . "','" . $otros . "','" . $usuarioId . "'";        
        echo $query;*/
                
        $stmt = mssql_init("bunkeringReporteNivel4");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);                        
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@nivel1Id', $nivel1Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel2Id', $nivel2Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel3Id', $nivel3Id, SQLVARCHAR);
        mssql_bind($stmt, '@otros', $otros, SQLVARCHAR);        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    function bunkeringReporteNivel5($fechaDesde, $fechaHasta, $nivel5, $tipoDato, $clienteId, $nivel1Id, $nivel2Id, $nivel3Id, $nivel4Id, $otros, $usuarioId){
            
        /*$query = "Exec bunkeringReporteNivel5 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $nivel1Id . "','" . $nivel2Id . "','" . $nivel3Id . "','" . $nivel4Id . "','" . $otros . "','" . $usuarioId . "'";        
        echo $query;*/
                
        $stmt = mssql_init("bunkeringReporteNivel5");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);                                
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@nivel1Id', $nivel1Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel2Id', $nivel2Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel3Id', $nivel3Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel4Id', $nivel4Id, SQLVARCHAR);
        mssql_bind($stmt, '@otros', $otros, SQLVARCHAR);        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    //EXTRACCIONES
    
    function bunkeringExtracciones($fechaDesde, $fechaHasta, $tipoDato, $clienteId, $embarcador, $naviera, $nave, $tipoNave, $puertoOrigen, $proveedor, $producto, $detalle,            
            $chkembarcador, $chknaviera, $chknave, $chktipoNave, $chkpuertoOrigen, $chkproveedor, $chkproducto, $chkdetalle, $nombreArchivo, $usuarioId){
    
        
        /*$query = "Exec bunkeringExtracciones '" . $fechaDesde . "','" . $fechaHasta . "','" .$tipoDato . "','" . $embarcador . "','" . $naviera . "','" . $nave . "','" . $tipoNave . "','" . 
                $puertoOrigen . "','" . $proveedor . "','" . $producto . "','" . $detalle . "','" .                 
                
                $chkembarcador . "','" . $chknaviera . "','" . $chknave . "','" . $chktipoNave . "','" . $chkpuertoOrigen . "','" . $chkproveedor . "','" . $chkproducto . "','" . 
                $chkdetalle . "','" . $nombreArchivo . "','" . $usuarioId . "'";        
        echo $query;*/
                        
        $stmt = mssql_init("bunkeringExtracciones");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);        
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        //mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);        
        mssql_bind($stmt, '@embarcador', $embarcador, SQLVARCHAR);
        mssql_bind($stmt, '@naviera', $naviera, SQLVARCHAR);
        mssql_bind($stmt, '@nave', $nave, SQLVARCHAR);
        mssql_bind($stmt, '@tipoNave', $tipoNave, SQLVARCHAR);
        mssql_bind($stmt, '@puertoOrigen', $puertoOrigen, SQLVARCHAR);
        mssql_bind($stmt, '@proveedor', $proveedor, SQLVARCHAR);
        mssql_bind($stmt, '@producto', $producto, SQLVARCHAR);
        mssql_bind($stmt, '@detalle', $detalle, SQLVARCHAR);
                
        mssql_bind($stmt, '@chkembarcador', $chkembarcador, SQLVARCHAR);
        mssql_bind($stmt, '@chknaviera', $chknaviera, SQLVARCHAR);
        mssql_bind($stmt, '@chknave', $chknave, SQLVARCHAR);
        mssql_bind($stmt, '@chktipoNave', $chktipoNave, SQLVARCHAR);
        mssql_bind($stmt, '@chkpuertoOrigen', $chkpuertoOrigen, SQLVARCHAR);
        mssql_bind($stmt, '@chkproveedor', $chkproveedor, SQLVARCHAR);
        mssql_bind($stmt, '@chkproducto', $chkproducto, SQLVARCHAR);
        mssql_bind($stmt, '@chkdetalle', $chkdetalle, SQLVARCHAR);                                  
        
        mssql_bind($stmt, '@nombreArchivo', $nombreArchivo, SQLVARCHAR);                                    
        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    function bunkeringExtraccionesArchivos($usuarioId, $moduloId) {
        $query = "Select * From Extraccion_Reportes (nolock) Where usuarioId = '" . $usuarioId . "' and modulo = '" . $moduloId . "' Order by fecha_reporte desc";
        //echo $query;
        $result = sql_db::sql_query($query);
        sql_db::sql_close();        
        return $result;
    }
    
    function bunkeringExtraccionesSeleccionarArchivo($usuarioId, $moduloId, $arhivoId) {
        $query = "Select nombre_archivo From Extraccion_Reportes (nolock) Where usuarioId = '" . $usuarioId . "' and modulo = '" . $moduloId . "' and id_archivo = '" . $arhivoId . "'";                
        $result = sql_db::sql_query($query);
        $row = sql_db::sql_fetch_assoc($result);
        sql_db::sql_close();        
        return $row;
    }
    
    function bunkeringExtraccionesEliminarArchivo($usuarioId, $moduloId, $arhivoId) {        
        $query = "Delete e From Extraccion_Reportes e Where usuarioId = '" . $usuarioId . "' and modulo = '" . $moduloId . "' and id_archivo = '" . $arhivoId . "'";        
        sql_db::sql_query($query);        
        sql_db::sql_close();        
        return true;
    }
    
    
}

?>


