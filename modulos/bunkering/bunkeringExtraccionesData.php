<?PHP

session_start();
include ("../../librerias/conexion.php");
require('bunkering.class.php');
$objBunkering = new bunkering();


$usuarioId = $_SESSION['SEMPAT_usuarioId'];
$clienteId = $_SESSION['SEMPAT_clienteId'];

$fechaDesde = mb_convert_encoding(trim($_POST['fechaDesde']), "ISO-8859-1", "UTF-8");
$fechaHasta = mb_convert_encoding(trim($_POST['fechaHasta']), "ISO-8859-1", "UTF-8");
$carpeta = mb_convert_encoding(trim($_POST['carpeta']), "ISO-8859-1", "UTF-8");


$nombreArchivo = mb_convert_encoding(trim($_POST['nombreArchivo']), "ISO-8859-1", "UTF-8");

$etapa = mb_convert_encoding(trim($_POST['etapa']), "ISO-8859-1", "UTF-8");

$tipoDato = mb_convert_encoding(trim($_POST['tipoDato']), "ISO-8859-1", "UTF-8");

$embarcador = mb_convert_encoding(trim($_POST['embarcador']), "ISO-8859-1", "UTF-8");
$naviera = mb_convert_encoding(trim($_POST['naviera']), "ISO-8859-1", "UTF-8");
$nave = mb_convert_encoding(trim($_POST['nave']), "ISO-8859-1", "UTF-8");
$tipoNave = mb_convert_encoding(trim($_POST['tipoNave']), "ISO-8859-1", "UTF-8");
$puertoOrigen = mb_convert_encoding(trim($_POST['puertoOrigen']), "ISO-8859-1", "UTF-8");
$proveedor = mb_convert_encoding(trim($_POST['proveedor']), "ISO-8859-1", "UTF-8");
$producto = mb_convert_encoding(trim($_POST['producto']), "ISO-8859-1", "UTF-8");
$detalle = mb_convert_encoding(trim($_POST['detalle']), "ISO-8859-1", "UTF-8");

$chkembarcador = mb_convert_encoding(trim($_POST['chkembarcador']), "ISO-8859-1", "UTF-8");
$chknaviera = mb_convert_encoding(trim($_POST['chknaviera']), "ISO-8859-1", "UTF-8");
$chknave = mb_convert_encoding(trim($_POST['chknave']), "ISO-8859-1", "UTF-8");
$chktipoNave = mb_convert_encoding(trim($_POST['chktipoNave']), "ISO-8859-1", "UTF-8");
$chkpuertoOrigen = mb_convert_encoding(trim($_POST['chkpuertoOrigen']), "ISO-8859-1", "UTF-8");
$chkproveedor = mb_convert_encoding(trim($_POST['chkproveedor']), "ISO-8859-1", "UTF-8");
$chkproducto = mb_convert_encoding(trim($_POST['chkproducto']), "ISO-8859-1", "UTF-8");
$chkdetalle = mb_convert_encoding(trim($_POST['chkdetalle']), "ISO-8859-1", "UTF-8");


$embarcador = str_replace("\'", "'", $embarcador);
$naviera = str_replace("\'", "'", $naviera);
$nave = str_replace("\'", "'", $nave);
$tipoNave = str_replace("\'", "'", $tipoNave);
$puertoOrigen = str_replace("\'", "'", $puertoOrigen);
$proveedor = str_replace("\'", "'", $proveedor);
$producto = str_replace("\'", "'", $producto);
$detalle = str_replace("\'", "'", $detalle);

$uploaddir = "../../Reportes/" . $carpeta . "/";
			
if (file_exists($uploaddir)==false) {
    mkdir($uploaddir, 0700, true);
}

$uploaddir = $uploaddir . "/" . $usuarioId . "/";

if (file_exists($uploaddir)==false) {
    mkdir($uploaddir, 0700, true);
}

$objBunkering->bunkeringExtracciones($fechaDesde, $fechaHasta, $tipoDato, $clienteId, $embarcador, $naviera, $nave, $tipoNave, $puertoOrigen, $proveedor, $producto, $detalle, $chkembarcador, $chknaviera, $chknave, $chktipoNave, $chkpuertoOrigen, $chkproveedor, $chkproducto, $chkdetalle, $nombreArchivo, $usuarioId);

?>
