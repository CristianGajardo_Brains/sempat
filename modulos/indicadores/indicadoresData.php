<?PHP
session_start();
include ("../../librerias/conexion.php");
require('indicadores.class.php');
require('../../clases/sempat.class.php');
$objIndicadores = new indicadores();
$objSempat = new sempat();


$usuarioId = $_SESSION['SEMPAT_usuarioId'];
$clienteId = $_SESSION['SEMPAT_clienteId'];

$fechaDesde = mb_convert_encoding(trim($_POST['fechaDesde']), "ISO-8859-1", "UTF-8");
$fechaHasta = mb_convert_encoding(trim($_POST['fechaHasta']), "ISO-8859-1", "UTF-8");

$resultado = $objIndicadores->indicadoresData($fechaDesde, $fechaHasta, $clienteId);


$titulos = retornarTitulo($fechaDesde, $fechaHasta);

if ($resultado) {

    if (mssql_num_rows($resultado) == 48) {                
        
        if(retornarPeriodo($fechaDesde) != retornarPeriodo($fechaHasta)){
            $periodo = retornarPeriodo($fechaDesde) . " - " .  retornarPeriodo($fechaHasta);                        
        }
        else{
            $periodo = retornarPeriodo($fechaDesde);
        }                        

        $snAgDoCmMerAc = mssql_result($resultado, 0, 'total');
        $snAgDoCmSaaAc = mssql_result($resultado, 1, 'total');
        $snAgDoCmMerAn = mssql_result($resultado, 2, 'total');
        $snAgDoCmSaaAn = mssql_result($resultado, 3, 'total');
        
        $snAgReRnMerAc = mssql_result($resultado, 4, 'total');
        $snAgReRnSaaAc = mssql_result($resultado, 5, 'total');
        $snAgReRnMerAn = mssql_result($resultado, 6, 'total');
        $snAgReRnSaaAn = mssql_result($resultado, 7, 'total');
        
        $snFlFaFoMerAc = mssql_result($resultado, 8, 'total');
        $snFlFaFoSaaAc = mssql_result($resultado, 9, 'total');
        $snFlFaFoMerAn = mssql_result($resultado, 10, 'total');
        $snFlFaFoSaaAn = mssql_result($resultado, 11, 'total');
        
        $snFlFaFcMerAc = mssql_result($resultado, 12, 'total');
        $snFlFaFcSaaAc = mssql_result($resultado, 13, 'total');
        $snFlFaFcMerAn = mssql_result($resultado, 14, 'total');
        $snFlFaFcSaaAn = mssql_result($resultado, 15, 'total');
        
        $snDmDeBmMerAc = mssql_result($resultado, 16, 'total');
        $snDmDeBmSaaAc = mssql_result($resultado, 17, 'total');
        $snDmDeBmMerAn = mssql_result($resultado, 18, 'total');
        $snDmDeBmSaaAn = mssql_result($resultado, 19, 'total');
        
        $snOpEfTtMerAc = mssql_result($resultado, 20, 'total');
        $snOpEfTtSaaAc = mssql_result($resultado, 21, 'total');
        $snOpEfTtMerAn = mssql_result($resultado, 22, 'total');
        $snOpEfTtSaaAn = mssql_result($resultado, 23, 'total');
        
        $scFrAlTaMerAc = mssql_result($resultado, 24, 'total');
        $scFrAlTaSaaAc = mssql_result($resultado, 25, 'total');
        $scFrAlTaMerAn = mssql_result($resultado, 26, 'total');
        $scFrAlTaSaaAn = mssql_result($resultado, 27, 'total');
        
        $scGrAlTaMerAc = mssql_result($resultado, 28, 'total');
        $scGrAlTaSaaAc = mssql_result($resultado, 29, 'total');
        $scGrAlTaMerAn = mssql_result($resultado, 30, 'total');
        $scGrAlTaSaaAn = mssql_result($resultado, 31, 'total');
        
        $scIeViCeMerAc = mssql_result($resultado, 32, 'total');
        $scIeViCeSaaAc = mssql_result($resultado, 33, 'total');
        $scIeViCeMerAn = mssql_result($resultado, 34, 'total');
        $scIeViCeSaaAn = mssql_result($resultado, 35, 'total');
        
        $scIeFrCeMerAc = mssql_result($resultado, 36, 'total');
        $scIeFrCeSaaAc = mssql_result($resultado, 37, 'total');
        $scIeFrCeMerAn = mssql_result($resultado, 38, 'total');
        $scIeFrCeSaaAn = mssql_result($resultado, 39, 'total');
        
        $scAepSanCiMerAc = mssql_result($resultado, 40, 'total');
        $scAepSanCiSaaAc = mssql_result($resultado, 41, 'total');
        $scAepSanCiMerAn = mssql_result($resultado, 42, 'total');
        $scAepSanCiSaaAn = mssql_result($resultado, 43, 'total');
        
        $scAepValCiMerAc = mssql_result($resultado, 44, 'total');
        $scAepValCiSaaAc = mssql_result($resultado, 45, 'total');
        $scAepValCiMerAn = mssql_result($resultado, 46, 'total');
        $scAepValCiSaaAn = mssql_result($resultado, 47, 'total');
        
        
        $html = "<div style='margin-left:35px'>
                    <label>SERVICIOS A LAS NAVES</label>
                </div>

                <table id='tablaIndicadores' class='tablaIndicadores' cellspacing='0' cellspadding='0'>
                    <thead>
                        <tr>
                            <th width='190px' rowspan='2' class='left'>
                                ACTIVIDAD
                            </th>
                            <th width='170px' rowspan='2'>
                                SERVICIO
                            </th>
                            <th width='300px' rowspan='2' colspan='2'>
                                INDICADOR
                            </th>
                            <th width='360px' colspan='4'>
                                " . $periodo . "
                            </th>
                        </tr>
                        <tr>
                            " . $titulos . "
                            <th width='70px' class='s_top'>
                                DIF.
                            </th>
                            <th width='90px' class='s_top'>
                                MKT. SHARE
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td rowspan='4' class='left'>
                                AGENCIAMIENTO
                            </td>
                            <td rowspan='2'>
                                DOCUMENTAL
                            </td>
                            <td rowspan='2'>
                                CONTENEDORES MOVILIZADOS
                            </td>
                            <td>
                                SAAM
                            </td>
                            <td align='right'>
                                " . retornarValor($snAgDoCmSaaAc) . "
                            </td>
                            <td align='right'>
                                " . retornarValorAnterior($snAgDoCmSaaAc, $snAgDoCmSaaAn) . "
                            </td>
                                " . $objIndicadores->calculoDiferenciaIndicadores($snAgDoCmSaaAc, $snAgDoCmSaaAn) . "
                                " . $objIndicadores->calculoMktIndicadores($snAgDoCmSaaAc, $snAgDoCmMerAc) . "
                        </tr>
                        <tr class='s_top n'>                            
                            <td>
                                MERCADO
                            </td>
                            <td align='right'>
                                " . retornarValor($snAgDoCmMerAc) . "
                            </td>
                            <td align='right'>
                                " . retornarValorAnterior($snAgDoCmMerAc, $snAgDoCmMerAn) . "
                            </td>
                                " . $objIndicadores->calculoDiferenciaIndicadores($snAgDoCmMerAc, $snAgDoCmMerAn) . "
                                " . $objIndicadores->calculoMktIndicadores($snAgDoCmMerAc, $snAgDoCmMerAc) . "
                        </tr>
                        <tr class='s_top'>
                            <td rowspan='2'>
                                RECALADAS
                            </td>
                            <td  rowspan='2'>
                                RECALADAS NAVES
                            </td>
                            <td>
                                SAAM
                            </td>
                            <td align='right'>
                                " . retornarValor($snAgReRnSaaAc) . "
                            </td>
                            <td align='right'>
                                " . retornarValorAnterior($snAgReRnSaaAc, $snAgReRnSaaAn) . "
                            </td>
                                " . $objIndicadores->calculoDiferenciaIndicadores($snAgReRnSaaAc, $snAgReRnSaaAn) . "
                                " . $objIndicadores->calculoMktIndicadores($snAgReRnSaaAc, $snAgReRnMerAc) . "
                        </tr>
                        <tr class='s_top n' rowspan='2'>                        
                            <td>
                                MERCADO
                            </td>
                            <td align='right'>
                                " . retornarValor($snAgReRnMerAc) . "
                            </td>
                            <td align='right'>
                                " . retornarValorAnterior($snAgReRnMerAc, $snAgReRnMerAn) . "
                            </td>
                                " . $objIndicadores->calculoDiferenciaIndicadores($snAgReRnMerAc, $snAgReRnMerAn) . "
                                " . $objIndicadores->calculoMktIndicadores($snAgReRnMerAc, $snAgReRnMerAc) . "
                        </tr>                        
                        <tr>
                            <td class='espacio' colspan='8'>

                            </td>                    
                        </tr>                        
                        <tr>
                            <td rowspan='4' class='left'>
                                FLOTA
                            </td>
                            <td rowspan='4'>
                                FAENAS
                            </td>
                            <td rowspan='2'>
                                FAENAS OPERATIVAS
                            </td>
                            <td>
                                SAAM
                            </td>
                            <td align='right'>
                                " . retornarValor($snFlFaFoSaaAc) . "
                            </td>
                            <td align='right'>
                                " . retornarValorAnterior($snFlFaFoSaaAc, $snFlFaFoSaaAn) . "
                            </td>
                                " . $objIndicadores->calculoDiferenciaIndicadores($snFlFaFoSaaAc, $snFlFaFoSaaAn) . "
                                " . $objIndicadores->calculoMktIndicadores($snFlFaFoSaaAc, $snFlFaFoMerAc) . "
                        </tr>
                        <tr class='s_top n'>                            
                            <td>
                                MERCADO
                            </td>
                            <td align='right'>
                                " . retornarValor($snFlFaFoMerAc) . "
                            </td>
                            <td align='right'>
                                " . retornarValorAnterior($snFlFaFoMerAc, $snFlFaFoMerAn) . "
                            </td>
                                " . $objIndicadores->calculoDiferenciaIndicadores($snFlFaFoMerAc, $snFlFaFoMerAn) . "
                                " . $objIndicadores->calculoMktIndicadores($snFlFaFoMerAc, $snFlFaFoMerAc) . "
                        </tr>
                        <tr class='s_top'>                            
                            <td rowspan='2'>
                                FAENAS COMERCIALES
                            </td>
                            <td>
                                SAAM
                            </td>
                            <td align='right'>
                                " . retornarValor($snFlFaFcSaaAc) . "
                            </td>
                            <td align='right'>
                                " . retornarValorAnterior($snFlFaFcSaaAc, $snFlFaFcSaaAn) . "
                            </td>
                                " . $objIndicadores->calculoDiferenciaIndicadores($snFlFaFcSaaAc, $snFlFaFcSaaAn) . "
                                " . $objIndicadores->calculoMktIndicadores($snFlFaFcSaaAc, $snFlFaFcMerAc) . "
                        </tr>
                        <tr class='s_top n'>                            
                            <td>
                                MERCADO
                            </td>
                            <td align='right'>
                                " . retornarValor($snFlFaFcMerAc) . "
                            </td>
                            <td align='right'>
                                " . retornarValorAnterior($snFlFaFcMerAc, $snFlFaFcMerAn) . "
                            </td>
                                " . $objIndicadores->calculoDiferenciaIndicadores($snFlFaFcMerAc, $snFlFaFcMerAn) . "
                                " . $objIndicadores->calculoMktIndicadores($snFlFaFcMerAc, $snFlFaFcMerAc) . "
                        </tr>
                        <tr>
                            <td class='espacio' colspan='8'>

                            </td> 
                        </tr>
                        <tr>
                            <td rowspan='2' class='left'>
                                DEPÓSITO Y MAESTRANZA
                            </td>
                            <td rowspan='2'>
                                DEPÓSITO
                            </td>
                            <td rowspan='2'>
                                BOX MOVILIZADOS (RYD)
                            </td>
                            <td>
                                SAAM
                            </td>
                            <td align='right'>
                                " . retornarValor($snDmDeBmSaaAc) . "
                            </td>
                            <td align='right'>
                                " . retornarValorAnterior($snDmDeBmSaaAc, $snDmDeBmSaaAn) . "
                            </td>
                                " . $objIndicadores->calculoDiferenciaIndicadores($snDmDeBmSaaAc, $snDmDeBmSaaAn) . "
                                " . $objIndicadores->calculoMktIndicadores($snDmDeBmSaaAc, $snDmDeBmMerAc) . "
                        </tr>
                        <tr class='s_top n'>                
                            <td>
                                MERCADO
                            </td>
                            <td align='right'>
                                " . retornarValor($snDmDeBmMerAc) . "
                            </td>
                            <td align='right'>
                                " . retornarValorAnterior($snDmDeBmMerAc, $snDmDeBmMerAn) . "
                            </td>
                                " . $objIndicadores->calculoDiferenciaIndicadores($snDmDeBmMerAc, $snDmDeBmMerAn) . "
                                " . $objIndicadores->calculoMktIndicadores($snDmDeBmMerAc, $snDmDeBmMerAc) . "
                        </tr>
                        <tr>
                            <td class='espacio' colspan='8'>

                            </td> 
                        </tr>
                        <tr>
                            <td rowspan='2' class='left'>
                                OPERACIONES PORTUARIAS
                            </td>
                            <td rowspan='2'>
                                ESTIBA FRUTA
                            </td>
                            <td rowspan='2'>
                                TONS. TRANSFERIDAS
                            </td>
                            <td>
                                SAAM
                            </td>
                            <td align='right'>
                                " . retornarValor($snOpEfTtSaaAc) . "
                            </td>
                            <td align='right'>
                                " . retornarValorAnterior($snOpEfTtSaaAc, $snOpEfTtSaaAn) . "
                            </td>
                                " . $objIndicadores->calculoDiferenciaIndicadores($snOpEfTtSaaAc, $snOpEfTtSaaAn) . "
                                " . $objIndicadores->calculoMktIndicadores($snOpEfTtSaaAc, $snOpEfTtMerAc) . "
                        </tr>
                        <tr class='s_top n'>                
                            <td>
                                MERCADO
                            </td>
                            <td align='right'>
                                " . retornarValor($snOpEfTtMerAc) . "
                            </td>
                            <td align='right'>
                                " . retornarValorAnterior($snOpEfTtMerAc, $snOpEfTtMerAn) . "
                            </td>
                                " . $objIndicadores->calculoDiferenciaIndicadores($snOpEfTtMerAc, $snOpEfTtMerAn) . "
                                " . $objIndicadores->calculoMktIndicadores($snOpEfTtMerAc, $snOpEfTtMerAc) . "
                        </tr>
                    </tbody>
                </table>
                
                <br>
                
                <div style='margin-left:35px'>
                    <label>SERVICIOS A LA CARGA</label>
                </div>

                <table id='tablaIndicadores' class='tablaIndicadores' cellspacing='0' cellspadding='0'>
                    <thead>                
                        <tr>            
                            <th width='190px' rowspan='2' class='left'>
                                ACTIVIDAD
                            </th>
                            <th width='170px' rowspan='2'>
                                SERVICIO
                            </th>  
                            <th width='300px' rowspan='2' colspan='2'>
                                INDICADOR
                            </th>  
                            <th width='360px' colspan='4'>
                                " . $periodo . "
                            </th>                          
                        </tr>
                        <tr>
                            " . $titulos . " 
                            <th width='70px' class='s_top'>
                                DIF.
                            </th>  
                            <th width='90px' class='s_top'>
                                MKT. SHARE
                            </th>  

                        </tr>
                    </thead>
                    <tbody>                                        
                        <tr>
                            <td rowspan='2' class='left'>
                                FRIGORÍFICO
                            </td>
                            <td rowspan='2'>
                                ALMACENAJE
                            </td>
                            <td rowspan='2'>
                                TONS. ALMANCENADAS (CÁMARAS)
                            </td>
                            <td>
                                SAAM
                            </td>
                            <td align='right'>
                                " . retornarValor($scFrAlTaSaaAc) . "
                            </td>
                            <td align='right'>
                                " . retornarValorAnterior($scFrAlTaSaaAc, $scFrAlTaSaaAn) . "
                            </td>
                                " . $objIndicadores->calculoDiferenciaIndicadores($scFrAlTaSaaAc, $scFrAlTaSaaAn) . "
                                " . $objIndicadores->calculoMktIndicadores($scFrAlTaSaaAc, $scFrAlTaMerAc) . "
                        </tr>
                        <tr class='s_top n'>                
                            <td>
                                MERCADO
                            </td>
                            <td align='right'>
                                " . retornarValor($scFrAlTaMerAc) . "
                            </td>
                            <td align='right'>
                                " . retornarValorAnterior($scFrAlTaMerAc, $scFrAlTaMerAn) . "
                            </td>
                                " . $objIndicadores->calculoDiferenciaIndicadores($scFrAlTaMerAc, $scFrAlTaMerAn) . "
                                " . $objIndicadores->calculoMktIndicadores($scFrAlTaMerAc, $scFrAlTaMerAc) . "
                        </tr>
                        <tr>
                            <td class='espacio' colspan='8'>

                            </td> 
                        </tr>
                        
                        <tr>
                            <td rowspan='4' class='left'>
                                AEP
                            </td>
                            <td rowspan='2'>
                                SAN ANTONIO
                            </td>
                            <td rowspan='2'>
                                CONTENEDORES INGRESADOS
                            </td>
                            <td>
                                SAAM
                            </td>
                            <td align='right'>
                                " . retornarValor($scAepSanCiSaaAc) . "
                            </td>
                            <td align='right'>
                                " . retornarValorAnterior($scAepSanCiSaaAc, $scAepSanCiSaaAn) . "
                            </td>
                                " . $objIndicadores->calculoDiferenciaIndicadores($scAepSanCiSaaAc, $scAepSanCiSaaAn) . "
                                " . $objIndicadores->calculoMktIndicadores($scAepSanCiSaaAc, $scAepSanCiMerAc) . "
                        </tr>
                        <tr class='s_top n'>                            
                            <td>
                                MERCADO
                            </td>
                            <td align='right'>
                                " . retornarValor($scAepSanCiMerAc) . "
                            </td>
                            <td align='right'>
                                " . retornarValorAnterior($scAepSanCiMerAc, $scAepSanCiMerAn) . "
                            </td>
                                " . $objIndicadores->calculoDiferenciaIndicadores($scAepSanCiMerAc, $scAepSanCiMerAn) . "
                                " . $objIndicadores->calculoMktIndicadores($scAepSanCiMerAc, $scAepSanCiMerAc) . "

                            </td>
                        </tr>
                        <tr class='s_top'>
                            <td rowspan='2'>
                                VALPARAÍSO
                            </td>
                            <td rowspan='2'>
                                CONTENEDORES INGRESADOS 
                            </td>
                            <td>
                                SAAM
                            </td>
                            <td align='right'>
                                " . retornarValor($scAepValCiSaaAc) . "
                            </td>
                            <td align='right'>
                                " . retornarValorAnterior($scAepValCiSaaAc, $scAepValCiSaaAn) . "
                            </td>
                                " . $objIndicadores->calculoDiferenciaIndicadores($scAepValCiSaaAc, $scAepValCiSaaAn) . "
                                " . $objIndicadores->calculoMktIndicadores($scAepValCiSaaAc, $scAepValCiMerAc) . "

                            </td>
                        </tr>
                        <tr class='s_top n'>                            
                            <td>
                                MERCADO
                            </td>
                            <td align='right'>
                                " . retornarValor($scAepValCiMerAc) . "
                            </td>
                            <td align='right'>
                                " . retornarValorAnterior($scAepValCiMerAc, $scAepValCiMerAn) . "
                            </td>
                                " . $objIndicadores->calculoDiferenciaIndicadores($scAepValCiMerAc, $scAepValCiMerAn) . "
                                " . $objIndicadores->calculoMktIndicadores($scAepValCiMerAc, $scAepValCiMerAc) . "
                            </td>
                        </tr>
                        <tr>
                            <td class='espacio' colspan='8'>

                            </td> 
                        </tr>
                        
                        <tr>
                            <td rowspan='2' class='left'>
                                GRANELES
                            </td>
                            <td rowspan='2'>
                                ALMACENAJE
                            </td>
                            <td rowspan='2'>
                                TONS. ALMACENADAS
                            </td>
                            <td>
                                SAAM
                            </td>
                            <td align='right'>
                                " . retornarValor($scGrAlTaSaaAc) . "
                            </td>
                            <td align='right'>
                                " . retornarValorAnterior($scGrAlTaSaaAc, $scGrAlTaSaaAn) . "
                            </td>
                                " . $objIndicadores->calculoDiferenciaIndicadores($scGrAlTaSaaAc, $scGrAlTaSaaAn) . "
                                " . $objIndicadores->calculoMktIndicadores($scGrAlTaSaaAc, $scGrAlTaMerAc) . "
                        </tr>
                        <tr class='s_top n'>                
                            <td>
                                MERCADO
                            </td>
                            <td align='right'>
                                " . retornarValor($scGrAlTaMerAc) . "
                            </td>
                            <td align='right'>
                                " . retornarValorAnterior($scGrAlTaMerAc, $scGrAlTaMerAn) . "
                            </td>
                                " . $objIndicadores->calculoDiferenciaIndicadores($scGrAlTaMerAc, $scGrAlTaMerAn) . "
                                " . $objIndicadores->calculoMktIndicadores($scGrAlTaMerAc, $scGrAlTaMerAc) . "

                            </td>
                        </tr>
                        <tr>
                            <td class='espacio' colspan='8'>

                            </td> 
                        </tr>
                        <tr>
                            <td rowspan='4' class='left'>
                                SERVICIO EXPO/IMPO
                            </td>
                            <td rowspan='2'>
                                VINO
                            </td>
                            <td rowspan='2'>
                                CAJAS 9 LTS. EQUIVALENTES
                            </td>
                            <td>
                                SAAM
                            </td>
                            <td align='right'>
                                " . retornarValor($scIeViCeSaaAc) . "
                            </td>
                            <td align='right'>
                                " . retornarValorAnterior($scIeViCeSaaAc, $scIeViCeSaaAn) . "
                            </td>
                                " . $objIndicadores->calculoDiferenciaIndicadores($scIeViCeSaaAc, $scIeViCeSaaAn) . "
                                " . $objIndicadores->calculoMktIndicadores($scIeViCeSaaAc, $scIeViCeMerAc) . "
                        </tr>
                        <tr class='s_top n'>                            
                            <td>
                                MERCADO
                            </td>
                            <td align='right'>
                                " . retornarValor($scIeViCeMerAc) . "
                            </td>
                            <td align='right'>
                                " . retornarValorAnterior($scIeViCeMerAc, $scIeViCeMerAn) . "
                            </td>
                                " . $objIndicadores->calculoDiferenciaIndicadores($scIeViCeMerAc, $scIeViCeMerAn) . "
                                " . $objIndicadores->calculoMktIndicadores($scIeViCeMerAc, $scIeViCeMerAc) . "

                            </td>
                        </tr>
                        <tr class='s_top'>
                            <td rowspan='2'>
                                FRUTA
                            </td>
                            <td rowspan='2'>
                                CAJAS EMBARCADAS
                            </td>
                            <td>
                                SAAM
                            </td>
                            <td align='right'>
                                " . retornarValor($scIeFrCeSaaAc) . "
                            </td>
                            <td align='right'>
                                " . retornarValorAnterior($scIeFrCeSaaAc, $scIeFrCeSaaAn) . "
                            </td>
                                " . $objIndicadores->calculoDiferenciaIndicadores($scIeFrCeSaaAc, $scIeFrCeSaaAn) . "
                                " . $objIndicadores->calculoMktIndicadores($scIeFrCeSaaAc, $scIeFrCeMerAc) . "

                            </td>
                        </tr>
                        <tr class='s_top n'>                            
                            <td>
                                MERCADO
                            </td>
                            <td align='right'>
                                " . retornarValor($scIeFrCeMerAc) . "
                            </td>
                            <td align='right'>
                                " . retornarValorAnterior($scIeFrCeMerAc, $scIeFrCeMerAn) . "
                            </td>
                                " . $objIndicadores->calculoDiferenciaIndicadores($scIeFrCeMerAc, $scIeFrCeMerAn) . "
                                " . $objIndicadores->calculoMktIndicadores($scIeFrCeMerAc, $scIeFrCeMerAc) . "
                            </td>
                        </tr>
                    </tbody>
                </table>";
        
        echo $html;


    }
}




function retornarPeriodo($valor){    
    //$meses = array("", "ENE", "FEB", "MAR", "ABR", "MAY", "JUN", "JUL", "AGO", "SEP", "OCT", "NOV", "DIC");
    $meses = array("", "ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE");
    
    $datos = split("-",$valor);
    
    //return $meses[$datos[1]*1] . " " . $datos[2];
    return $meses[$datos[1]*1];
    
}


function retornarTitulo($fechaDesde, $fechaHasta){    
    
    $yyyyDesde = substr($fechaDesde, 6, 4);
    $yyyyHasta = substr($fechaHasta, 6, 4);

    $celda = "";
    $celda1 = "";
    $celda2 = "";

    if ($yyyyDesde != "" && $yyyyHasta != "") {

        if ($yyyyDesde == $yyyyHasta) {
            $celda1 = $yyyyDesde;
            $celda2 = ($yyyyDesde - 1);
        } else {
            $celda1 = $yyyyDesde . " - " . $yyyyHasta;
            $celda2 = ($yyyyDesde - 1) . " - " . ($yyyyHasta - 1);
        }

        $celda = "<th width='100px' class='s_top'>" . $celda1 . "</th><th width='100px' class='s_top'>" . $celda2 . "</th>";
        
    }
    
    return $celda;   

}


function retornarValor($valor){
    
    if(trim($valor) != ""){
        return number_format($valor, 0, '', '.');
    }
    else{
        return "";
    }
    
}


function retornarValorAnterior($valorActual, $valorAnterior){
    
    if(trim($valorActual) != ""){
        
        if(trim($valorAnterior) != ""){
            return number_format($valorAnterior, 0, '', '.');
        }
        else{
            return "";
        }
        
    }
    else{
        return "";
    }
    
    
}


?>




