<?php

class indicadores {
    
   
    function indicadoresData($fechaDesde, $fechaHasta, $clienteId) {
        $query = "EXEC IndicadoresReporteListar '" . $fechaDesde . "','" . $fechaHasta . "'," . $clienteId . "";
        //echo $query;
        $result = sql_db::sql_query($query);
        sql_db::sql_close();
        return $result;
    }
    
    
    function calculoDiferenciaIndicadores($numerador, $denominador){
    
        $resultadoFont = "";

        if ($denominador != 0 && trim($denominador) != "" && trim($numerador) != "") {
            $resultado = ((($numerador / $denominador) - 1) * 100);
            $resultadoA = number_format($resultado, 2);
            $resultadoB = number_format($resultado, 0, '', '');
                        
            if ($resultado > 0) {
                                
                if(strlen($resultadoB) > 4){
                    $resultadoFont = '<td class="positivo" title="' . $resultadoA . ' %">' . substr($resultadoA, 0, 5) . "..." . ' %</td>';
                }
                else{
                    $resultadoFont = '<td class="positivo">' . $resultadoA . ' %</td>';
                }

            } else {
                
                if(strlen($resultadoB) > 4){
                    $resultadoFont = '<td class="negativo" title="' . $resultadoA . ' %">' . substr($resultadoA, 0, 5) . "..." . ' %</td>';
                }
                else{
                    $resultadoFont = '<td class="negativo">' . $resultadoA . ' %</td>';
                }
                
            }
        } else {
            
            if(trim($numerador) != "" && trim($denominador) != ""){
                if ($numerador > 0) {
                    $resultadoFont = '<td class="positivo">100.00 %</td>';
                } else {
                    $resultadoFont = '<td class="negativo">0.00 %</td>';
                }
            }
            else{
                $resultadoFont = '<td></td>';            
            }            

        }

        return $resultadoFont;    
    }
    
    
    function calculoMktIndicadores($numerador, $denominador){

        $resultadoFont = "";

        if ($denominador != 0 && trim($numerador) != "" && trim($denominador) != "") {
            $resultado = (($numerador / $denominador) * 100);
            $resultado = number_format($resultado, 0);
            $resultadoFont = '<td style="text-align:center">' . $resultado . ' %</td>';
        } else {

            if($numerador == 0 && trim($numerador) != ""){
                $resultadoFont = '<td style="text-align:center"> 0 %</td>';
            }
            else{
                $resultadoFont = '<td> </td>';
            }

        }

        return $resultadoFont;
    }


}

?>


