<?
session_start();

setlocale(LC_TIME, "spanish");
$fechaActual = htmlentities(strftime("%A %d de %B del %Y"));
$fechaActual = ucfirst($fechaActual);


$usuarioLog = $_SESSION["SEMPAT_usuarioNombre"];
$usuarioId = $_SESSION["SEMPAT_usuarioId"];
$clienteId = $_SESSION["SEMPAT_clienteId"];


if ($_SESSION['SEMPAT_usuarioId'] == "") {
    echo "<script>window.location='../../index.php';</script>";
}

include("../../default.php");
$plantilla->setPath('../../plantillas/');
$plantilla->setTemplate("header");
$plantilla->setVars(array("USUARIO" => " $usuarioLog ",
    "FECHA" => "$fechaActual"
));
echo $plantilla->show();

require('../../clases/sempat.class.php');
$objSem = new sempat();

$menuId = 20;

$htmlMenu = $objSem->menuRetornarHtml($usuarioId, $menuId);


$htmlListaArchivos2013 = "";
$htmlListaArchivos2014 = "";
$htmlListaArchivos2015 = "";
$htmlListaArchivos2016 = "";


$htmlListaArchivos2013 = leerArchivos("2013");
$htmlListaArchivos2014 = leerArchivos("2014");
$htmlListaArchivos2015 = leerArchivos("2015");
$htmlListaArchivos2016 = leerArchivos("2016");

function leerArchivos($carpeta){
    
    $htmlListaArchivos = "";
    
    $dir = "../administracion/reportesGerencia/" . $carpeta;
    $entradas = array();
    $i = 0;

    $directorio = opendir($dir);
    while ($archivo = readdir($directorio)) {
        if ($archivo != "." && $archivo != "..") {
            $entradas[$archivo] = filemtime($dir . "/" . $archivo);
        }
    }

    asort($entradas); // arsort o asort
    closedir($directorio);

    foreach ($entradas as $archivo => $valor) {
        if ($i <= 10) {
            $archivoNombre = basename($archivo, ".pdf");
            $htmlListaArchivos = $htmlListaArchivos . "<tr><td>" . $archivoNombre . "</td><td><a href='" . $dir . "/" . $archivo . "' title='Descargar' target='_blank'><img src='../../imagenes/pdf.png'/></a></td></tr>";
            $i = 0;
        }
    }
    
    return $htmlListaArchivos;
}


?>

<? echo $htmlMenu; ?>

<script type="text/javascript">




    $(document).ready(function () {

		$("#tHead2015").click(function () {

            var display = document.getElementById("tBody2015").style.display;

            if (display === "none") {
                $("#tBody2015").css("display", "");
            } else {
                $("#tBody2015").css("display", "none");
            }

        });
	
	
        $("#tHead2014").click(function () {

            var display = document.getElementById("tBody2014").style.display;

            if (display === "none") {
                $("#tBody2014").css("display", "");
            } else {
                $("#tBody2014").css("display", "none");
            }

        });
        
        $("#tHead2013").click(function () {

            var display = document.getElementById("tBody2013").style.display;

            if (display === "none") {
                $("#tBody2013").css("display", "");
            } else {
                $("#tBody2013").css("display", "none");
            }

        });

    });


</script>

<div class="divMinHeight">

    <div class="divSubContenedor">

        <div class="divPanelBusquedaB">
            <img src="#" alt="" title=""><label>REPORTES DE GERENCIA</label>
        </div>

    </div>
	
	<table id="tabla2016" class="tablaExtracciones" cellspacing="0" cellspadding="0">
        <thead id="tHead2016" style="cursor: pointer">
            <tr>            
                <th class="thCenter" style="width:332px;">
                    <div class="divThLeft"></div>          
                    <div class="divThCenter">ARCHIVOS 2016</div>                
                </th>                                                
                <th class="thCenter" style="width:50px;">                
                    <div class="divThCenter"></div>                
                </th>                                          
            </tr> 
        </thead>
        <tbody id="tBody2016" style="display:">
            <? echo $htmlListaArchivos2016; ?>
        </tbody>

    </table>
	
	
    <table id="tabla2015" class="tablaExtracciones" cellspacing="0" cellspadding="0">
        <thead id="tHead2015" style="cursor: pointer">
            <tr>            
                <th class="thCenter" style="width:332px;">
                    <div class="divThLeft"></div>          
                    <div class="divThCenter">ARCHIVOS 2015</div>                
                </th>                                                
                <th class="thCenter" style="width:50px;">                
                    <div class="divThCenter"></div>                
                </th>                                          
            </tr> 
        </thead>
        <tbody id="tBody2015" style="display:">
            <? echo $htmlListaArchivos2015; ?>
        </tbody>

    </table>

    <table id="tabla2014" class="tablaExtracciones" cellspacing="0" cellspadding="0">
        <thead id="tHead2014" style="cursor: pointer">
            <tr>
                <th class="thCenter" style="width:332px;">
                    <div class="divThLeft"></div>          
                    <div class="divThCenter">ARCHIVOS 2014</div>                
                </th>                                                
                <th class="thCenter" style="width:50px;">                
                    <div class="divThCenter"></div>                
                </th>                                          
            </tr> 
        </thead>
        <tbody id="tBody2014" style="display:none">
            <? echo $htmlListaArchivos2014; ?>
        </tbody>

    </table>

    <table class="tablaExtracciones" cellspacing="0" cellspadding="0">
        <thead id="tHead2013" style="cursor: pointer">
            <tr>            
                <th class="thCenter" style="width:332px;">
                    <div class="divThLeft"></div>          
                    <div class="divThCenter">ARCHIVOS 2013</div>                
                </th>                                                
                <th class="thCenter" style="width:50px;">                
                    <div class="divThCenter"></div>                
                </th>                                          
            </tr> 
        </thead>
        <tbody id="tBody2013" style="display:none">
            <? echo $htmlListaArchivos2013; ?>
        </tbody>

    </table>             

</div>

<br>

<?
$plantilla->setPath('../../plantillas/');
$plantilla->setTemplate("footer");
echo $plantilla->show();
?>