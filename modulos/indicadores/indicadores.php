<?
session_start();

setlocale(LC_TIME, "spanish");
$fechaActual = htmlentities(strftime("%A %d de %B del %Y"));
$fechaActual = ucfirst($fechaActual);


$usuarioLog = $_SESSION["SEMPAT_usuarioNombre"];
$usuarioId = $_SESSION["SEMPAT_usuarioId"];
$clienteId = $_SESSION["SEMPAT_clienteId"];


if ($_SESSION['SEMPAT_usuarioId'] == "") {
    echo "<script>window.location='../../index.php';</script>";
}

include("../../default.php");
$plantilla->setPath('../../plantillas/');
$plantilla->setTemplate("header");
$plantilla->setVars(array("USUARIO" => " $usuarioLog ",
    "FECHA" => "$fechaActual"
));
echo $plantilla->show();

require('../../clases/sempat.class.php');
$objSem = new sempat();

$menuId = 14;

$htmlMenu = $objSem->menuRetornarHtml($usuarioId, $menuId);


?>

<script type="text/javascript" src="indicadores.js"></script>

<script type="text/javascript">


    $(document).ready(function(){
        
        $("#fechaDesde").mask("99-99-9999");
        $("#fechaHasta").mask("99-99-9999");

        $('#fechaDesde').bind('blur', function() {
            validarFecha(this);
        });

        $('#fechaHasta').bind('blur', function() {
            validarFecha(this);
        });
        
        
        <? if ($clienteId == 1) {?>                    
            //new Messi('Ya se encuentra disponible la información de los Manifiestos Marítimos desde Octubre de 2013.', {center: false, align: 'right', titleClass: 'info', title: 'Aviso importante',  width: '320px', viewport: {top: '180px'}});
        <?}?>
        
        buscarIndicadores();           

    });

</script>
            
    <? echo $htmlMenu; ?>

    <div class="divMinHeight">
        
        <div class="divSubContenedor">

            <div class="divPanelBusqueda" onClick="mostrarFiltrosBusqueda()">
                <img src="../../imagenes/lupa.png" alt="" title=""><label>PANEL DE BUSQUEDA</label>
            </div>

            <div id="divPanelFiltros" class="divPanelFiltros" style="display:none">
                <div class="divPanelFiltros1" style="height: auto" >                                        
                    <table>
                        <tr>
                            <td valign="top" width="400px">
                                <label>FECHAS</label>

                                <br><br>

                                <input id="fechaDesde" type="text" class="inputFecha" placeHolder="Inicio" value=""/><input id="btnFechaDesde" type="button" class="inputFechaBtn" />
                                <input id="fechaDesdeDef" type="hidden"  value="<? echo $fechaDesde; ?>"/>


                                <script type="text/javascript">//<![CDATA[
                                    var myCal1 = Calendar.setup({
                                        checkRange: false,
                                        inputField: "fechaDesde",
                                        trigger: "btnFechaDesde",
                                        //onSelect: function () { this.hide(); validarFechasReportes($('fechaDesde'), 'fechaDesde', 'fechaHasta');},
                                        dateFormat: "%d-%m-%Y"
                                    });
                                    myCal1.setLanguage('es');
                                    //]]></script>

                                <input id="fechaHasta" type="text" class="inputFecha" placeHolder="Fin" value="" style="margin-left:60px"/><input id="btnFechaHasta" type="button" class="inputFechaBtn" />
                                <input id="fechaHastaDef" type="hidden"  value="<? echo $fechaHasta; ?>"/>

                                <script type="text/javascript">//<![CDATA[
                                    var myCal1 = Calendar.setup({
                                        checkRange: false,
                                        inputField: "fechaHasta",
                                        trigger: "btnFechaHasta",
                                        //onSelect: function () { this.hide(); validarFechasReportes($('fechaDesde'), 'fechaDesde', 'fechaHasta');},
                                        dateFormat: "%d-%m-%Y"
                                    });
                                    myCal1.setLanguage('es');
                                    //]]></script>
                                
                                
                                <div id="divMensajeFechas">

                                </div>

                            </td>         
                        </tr>
                    </table>
                    <input type="button" class="btnLogin" value="BUSCAR" onClick="buscarIndicadores()"/>
                    <br/><br>
                </div>
                                        
            </div>
            

            <div id="divMensaje">
                
            </div>            


        </div>
        
        <div style="height:70px">

            <center id="imgCargando" style="display:none">
                <img src="../../imagenes/cargando.gif" height="42" width="42"><br/><i>Cargando</i>
            </center>

        </div>

        <div id="divResultado">
                    
            
        
        </div>

    </div>

    <br>
            
<?
$plantilla->setPath('../../plantillas/');
$plantilla->setTemplate("footer");
echo $plantilla->show();
?>