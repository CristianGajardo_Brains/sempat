var count = 0;



function buscarIndicadores(){        
    
    var fechaDesde = $("#fechaDesde").val();
    var fechaHasta = $("#fechaHasta").val();


    if(fechaDesde == "" && fechaHasta == ""){

        fechasDefecto('INDICADORES'); 
        fechaDesde = $("#fechaDesde").val();
        fechaHasta = $("#fechaHasta").val();

    }


    if(validarFormulario()){

        $("#divResultado").html("");  
        
        $("#imgCargando").css("display","block");        
        $("#divMensaje").css("display","none");
       
        $.ajax({
            type: "POST",
            url: "indicadoresData.php",
            //async: false,
            data: { fechaDesde: fechaDesde, fechaHasta: fechaHasta}
            })
            .done(function( msg ) {

                if(msg.trim() != ""){                        
                    $("#divResultado").html(msg);                                                            
                }
                else{
                    $('#divResultado').html("<div style='margin-left:30px'><label style='color:red'>Búsqueda sin resultados.</label></div>");
                }

                $("#imgCargando").css("display","none");

        }); 

    }    

}

function validarFormulario(){

    var mensaje = "";

    var fechaDesde = $("#fechaDesde").val();
    var fechaHasta = $("#fechaHasta").val();
            
    if(fechaDesde == ""){
        mensaje += "No ha ingresado la fecha inicial.<br>";
    }
    
    if(fechaHasta == ""){
        mensaje += "No ha ingresado la fecha final.<br>";
    }
    
    if(fechaDesde != "" && fechaHasta != "" && validarFechaMenor(fechaDesde, fechaHasta) == false){
        mensaje += "La fecha incial no puede ser mayor a la fecha final.<br>";
    }
    
    if(fechaDesde != "" && fechaHasta != "" && fechaDiferenciasDias(fechaDesde, fechaHasta) > 365){
        mensaje += "La diferencia entre las fechas no puede ser superior a un año.<br>";
    }
            
    if(mensaje != ""){
        $("#divMensaje").html(mensaje);
        $("#divMensaje").css("display","block");
        return false;
    }
    else{
        $("#divMensaje").html("");
        $("#divMensaje").css("display","none");
        return true;
    }

}
    
    
    