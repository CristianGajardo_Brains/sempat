<?PHP

session_start();
include ("../../librerias/conexion.php");
require('vehiculos.class.php');
require('../../clases/sempat.class.php');
$objVehiculos = new vehiculos();
$objSempat= new sempat();


$usuarioId = $_SESSION['SEMPAT_usuarioId'];
$clienteId = $_SESSION['SEMPAT_clienteId'];

$fechaDesde = mb_convert_encoding(trim($_POST['fechaDesde']), "ISO-8859-1", "UTF-8");
$fechaHasta = mb_convert_encoding(trim($_POST['fechaHasta']), "ISO-8859-1", "UTF-8");

$nivel1 = mb_convert_encoding(trim($_POST['nivel1']), "ISO-8859-1", "UTF-8");
$nivel2 = mb_convert_encoding(trim($_POST['nivel2']), "ISO-8859-1", "UTF-8");
$nivel3 = mb_convert_encoding(trim($_POST['nivel3']), "ISO-8859-1", "UTF-8");
$nivel4 = mb_convert_encoding(trim($_POST['nivel4']), "ISO-8859-1", "UTF-8");
$nivel5 = mb_convert_encoding(trim($_POST['nivel5']), "ISO-8859-1", "UTF-8");

$nivel1Titulo = mb_convert_encoding(trim($_POST['nivel1Titulo']), "ISO-8859-1", "UTF-8");

$tipoDato = mb_convert_encoding(trim($_POST['tipoDato']), "ISO-8859-1", "UTF-8");

$mercadoId = mb_convert_encoding(trim($_POST['mercadoId']), "ISO-8859-1", "UTF-8");

$rut = mb_convert_encoding(trim($_POST['rut']), "ISO-8859-1", "UTF-8");
$importador = mb_convert_encoding(trim($_POST['importador']), "ISO-8859-1", "UTF-8");
$naviera = mb_convert_encoding(trim($_POST['naviera']), "ISO-8859-1", "UTF-8");
$nave = mb_convert_encoding(trim($_POST['nave']), "ISO-8859-1", "UTF-8");
$tipoNave = mb_convert_encoding(trim($_POST['tipoNave']), "ISO-8859-1", "UTF-8");

$tipo = mb_convert_encoding(trim($_POST['tipo']), "ISO-8859-1", "UTF-8");
$producto = mb_convert_encoding(trim($_POST['producto']), "ISO-8859-1", "UTF-8");
$marca = mb_convert_encoding(trim($_POST['marca']), "ISO-8859-1", "UTF-8");
$pago = mb_convert_encoding(trim($_POST['pago']), "ISO-8859-1", "UTF-8");
$via = mb_convert_encoding(trim($_POST['via']), "ISO-8859-1", "UTF-8");

$trafico = mb_convert_encoding(trim($_POST['trafico']), "ISO-8859-1", "UTF-8");
$paisOrigen = mb_convert_encoding(trim($_POST['paisOrigen']), "ISO-8859-1", "UTF-8");
$puertoEmbarque = mb_convert_encoding(trim($_POST['puertoEmbarque']), "ISO-8859-1", "UTF-8");
$puertoDescarga = mb_convert_encoding(trim($_POST['puertoDescarga']), "ISO-8859-1", "UTF-8");
$aduana = mb_convert_encoding(trim($_POST['aduana']), "ISO-8859-1", "UTF-8");
$estado = mb_convert_encoding(trim($_POST['estado']), "ISO-8859-1", "UTF-8");

$rut = str_replace("\'", "'", $rut);
$importador = str_replace("\'", "'", $importador);
$naviera = str_replace("\'", "'", $naviera);
$nave = str_replace("\'", "'", $nave);
$tipoNave = str_replace("\'", "'", $tipoNave);

$tipo = str_replace("\'", "'", $tipo);
$producto = str_replace("\'", "'", $producto);
$marca = str_replace("\'", "'", $marca);
$pago = str_replace("\'", "'", $pago);
$via = str_replace("\'", "'", $via);

$trafico = str_replace("\'", "'", $trafico);
$paisOrigen = str_replace("\'", "'", $paisOrigen);
$puertoEmbarque = str_replace("\'", "'", $puertoEmbarque);
$puertoDescarga = str_replace("\'", "'", $puertoDescarga);
$aduana = str_replace("\'", "'", $aduana);
$estado = str_replace("\'", "'", $estado);

$consultaTabla = $objVehiculos->vehiculosReporteNivel1($mercadoId, $fechaDesde, $fechaHasta, $nivel1, $nivel2, $nivel3, $nivel4, $nivel5, $tipoDato, $Rut, $importador, $naviera, $nave, $tipoNave, $tipo, $producto, $marca, $pago, $via, $trafico, $paisOrigen, $puertoEmbarque, $puertoDescarga, $aduana, $estado, $usuarioId);

$meses = array();

$mesInicial = substr($fechaDesde, 3, 2);
$añoInicial = substr($fechaDesde, 6, 4);

$mesFinal = substr($fechaHasta, 3, 2);
$añoFinal = substr($fechaHasta, 6, 4);

for ($a = 0; $a < 12; $a++) {

    $mes = "00" . ($mesInicial);
    $mes = substr($mes, strlen($mes) - 2, strlen($mes));

    $meses[$a][0] = $añoInicial . "-" . $mes;

    if ($mes >= $mesFinal && $añoInicial >= $añoFinal) {                        
        $a = 12;                        
    }

    if ($mes == 12) {
        $añoInicial++;
        $mesInicial = "01";
    } else {
        $mesInicial++;
    }        
    
}


$cabeceraHTML = "";
$cabeceraHTML2 = "<tr><td colspan='2' class='left' style='width:185px;'>" . htmlentities(strtoupper($nivel1Titulo)) . "</td>";
$colspan=5;

for ($i = 0; $i < count($meses); $i++) {
    $cabeceraHTML2 .= "<td>" . $meses[$i][0] . "</td>";
    $colspan++;
    
    $columnasMeses[$i] = $meses[$i][0] ;
}

$cabeceraHTML = "<table id='tablaReporte' class='tablaValores' cellspacing='0' cellspadding='0'>
                    <thead>
                        <tr>            
                            <th class='thCenter' colspan='" . $colspan . "'>
                                <div class='divThLeft'></div>          
                                <div class='divThCenter'>" . tituloCabecera($fechaDesde, $fechaHasta, 2) . "</div>
                                <div class='divThRight'></div>          
                            </th>                                                
                        </tr>" . $cabeceraHTML2 . "<td>TOTAL</td><td>TOTAL ANT.</td><td>DIF.</td> </thead>";

$campo1 = "";
$valorCampo1 = "";
$salida = "";
$salidaOtros = "";
$total = 0;
$totalAnterior = 0;

$fila = 0;

$cursor = ""; 

if($nivel2 != ""){
    $cursor = " style='cursor:pointer'";
}


if ($consultaTabla) {
    
    if(mssql_num_fields($consultaTabla) > 1){
    
        $numFilas = mssql_num_rows($consultaTabla);

        while ($manifiesto = mssql_fetch_array($consultaTabla)) {

            $fila++;

            $campo1 = htmlentities($manifiesto[1]);

            if(strlen($campo1) > 24){
                $valorCampo1 = substr($campo1, 0, 19) . "...";
            }
            else{
                $valorCampo1 = $campo1;
            }

            if($campo1 != "OTHERS"){

                $salida .= "<tr id='tr-". $fila ."' class='nivel1'><td class='check'><input class='styled' type='checkbox' onClick='graficoCheckboxReporte(this);'></td><td class='nivel1' " . $cursor . " title =\"" . $campo1 . "\" onClick='buscarNivel2(this, \"" . $fechaDesde . "\", \"" . $fechaHasta . "\", \"" . $nivel2 . "\", \"" . $nivel3 . "\", \"" . $nivel4 . "\", \"" . $nivel5 . "\", \"" . $tipoDato . "\", \"" . $manifiesto[2] . "\");'>" . $valorCampo1 . "</td>";        

                for ($i = 0; $i < count($meses); $i++) {

                    $salida .= "<td>" . number_format($manifiesto[3 + $i], 0, '', '.') . "</td>";
                    $meses[$i][$fila] .= number_format($manifiesto[3 + $i], 0, '', '');
                }

                $salida .= "<td><b>" . number_format($manifiesto[16], 0, '', '.') . "</b></td><td><b>" . number_format($manifiesto[15], 0, '', '.') . "</b></td>";                                    
                $salida .= $objSempat->calculoDiferencia($manifiesto[16], $manifiesto[15]) . "</tr>";

                $total = $total + number_format($manifiesto[16], 0, '', '');
                $totalAnterior = $totalAnterior + number_format($manifiesto[15], 0, '', '');

            }
            else{

                if($manifiesto[16] != "0" && $manifiesto[16] != ""){
                    $salidaOtros .= "<tr id='tr-". ($numFilas + 1) ."' class='nivel1'><td class='check'><input type='checkbox' class='styled' onClick='graficoCheckboxReporte(this);'/></td><td class='nivel1' title =\"" . $campo1 . "\">" . $valorCampo1 . "</td>";        

                    for ($i = 0; $i < count($meses); $i++) {

                        $salidaOtros .= "<td>" . number_format($manifiesto[3 + $i], 0, '', '.') . "</td>";                                                            
                        $meses[$i][$numFilas + 1] .= number_format($manifiesto[3 + $i], 0, '', '');
                    }

                    $salidaOtros .= "<td><b>" . number_format($manifiesto[16], 0, '', '.') . "</b></td><td><b>" . number_format($manifiesto[15], 0, '', '.') . "</b></td>";
                    $salidaOtros .= $objSempat->calculoDiferencia($manifiesto[16], $manifiesto[15]) . "</tr>";

                    $total = $total + number_format($manifiesto[16], 0, '', '');
                    $totalAnterior = $totalAnterior + number_format($manifiesto[15], 0, '', '');

                }
            }
        }

        $salidaTotal = "<tr class='nivelTotal'><td class='check'></td><td class='nivelTotal'>TOTAL</td>";

        $htmlTablaGrafico = "<table id='tablaGrafico'><thead><tr><th></th></tr></thead><tbody>";


        for($x = 0; $x < count($meses); $x++){      

            $htmlTablaGrafico .= "<tr><th>" . retornarPeriodo($meses[$x][0]) . "</th>";

            $valor = "0";

            for ($i = 1; $i <= ($numFilas + 1); $i++) {            
                $valor = $valor + $meses[$x][$i];
            }

            $salidaTotal .= "<td>" .  number_format($valor, 0, '', '.')   . "</td>";
        }

        $salidaTotal.= "<td>" .  number_format($total, 0, '', '.')   . "</td><td>" .  number_format($totalAnterior, 0, '', '.')   . "</td>" . $objSempat->calculoDiferencia($total, $totalAnterior) . "</tr>";

        $bodyHTML = "<tbody id='tBody'>" . $salida . $salidaOtros . $salidaTotal ."</tbody>";


        $htmlTablaGrafico .= "</tr></tbody></table>";

        $htmlTablaGrafico .= "<table id=\"tablaActual\">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>

                            <table id=\"tablaAnterior\">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>";
        
        $datos[0] = $usuarioId;
        $datos[1] = tituloCabecera($fechaDesde, $fechaHasta, 2); 
        $datos[2] = $nivel1;
        $datos[3] = $nivel2;
        $datos[4] = $tipoDato;
        $datos[5] = $fechaDesde;
        $datos[6] = $fechaHasta;
        $datos[7] = $mercado;
        $arreglo = implode("/", $datos);
        $meses = implode("/", $columnasMeses);

        $excel = "  <div style='text-align:right; padding-right:20px'><form action='vehiculosExcel.php' method='post' target='_blank' id='FormularioExportacion' name='FormularioExportacion'>
                <p>  <img src='../../imagenes/excel.png' class='botonExcel' style='width:30px' title='Exportar a Excel' /></p>
                    <input type='hidden' id='datos' name='datos' value='" . $arreglo . "'/>
                    <input type='hidden' id='meses' name='meses' value='" . $meses . "'/>  
                    <input type='hidden' id='tituloNombre' name='tituloNombre' value=''/>
                    <input type='hidden' id='nivel1Nombre' name='nivel1Nombre' value=''/>
                    <input type='hidden' id='nivel2Nombre' name='nivel2Nombre' value=''/>                    
            </form></div>";

        $excel = "";
        
        echo "<div id='divDatosGrafico' style='display:none'>" .  $htmlTablaGrafico . "</div>" . $excel . $cabeceraHTML . $bodyHTML . "</table>";

    }
        
}
else{
    echo "Sin resultado";
}




function tituloCabecera($fechaDesde, $fechaHasta, $tipo){
    
    $mesesA = array("", "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC");
    $mesesB = array("", "JANUARY", "FEBRUARY", "MARCH", "APRIL", "MAY", "JUNE", "JULY", "AUGUST", "SEPTEMBER", "OCTOBER", "NOVEMBER", "DECEMBER");
    
    $mDesde = date("m", strtotime($fechaDesde));
    $yDesde = date("Y", strtotime($fechaDesde));
    
    $mHasta = date("m", strtotime($fechaHasta));
    $yHasta = date("Y", strtotime($fechaHasta));
    
    $titulo = "";        
    
    if($tipo == 1){        
        
        if($mDesde == $mHasta && $yDesde == $yHasta){
            $titulo = $mesesA[(int)$mDesde] . " " . $yDesde;        
        }
        else{
            $titulo = $mesesA[(int)$mDesde] . " " . $yDesde . " - " . $mesesA[(int)$mHasta] . " " . $yHasta;        
        }
        
    }
    
    if($tipo == 2){
        if($mDesde == $mHasta && $yDesde == $yHasta){
            $titulo = $mesesB[(int)$mDesde] . " " . $yDesde;  
        }
        else{
            $titulo = $mesesB[(int)$mDesde] . " " . $yDesde . " - " . $mesesB[(int)$mHasta] . " " . $yHasta;        
        }
        
    }
    
    return $titulo;
    
}


function retornarPeriodo($valor){    
    $meses = array("", "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC");
    
    $datos = split("-",$valor);
    
    return $meses[$datos[1]*1] . "-" . $datos[0];            
    
}

//echo $salida;


echo '  <html>
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
                    <title>JQuery Excel</title>
                    <style type="text/css">
                        .botonExcel{cursor:pointer;}
                    </style>
            </head>
        </html>';

?>

<script language="javascript">
$(document).ready(function() {
	$(".botonExcel").click(function(event) {
                               
                var etapa = $("input[name=btnEtapa]:checked").val();                
                var tipoDato = $("input[name=tipoDato]:checked").val();

                var nivel1Nombre = $("#selectNivel1 option:selected").text();
                var nivel2Nombre = $("#selectNivel2 option:selected").text();
                
                var titulo = "";
                                                
                switch(etapa){
                    case undefined:
                        titulo = 'Exportaciones e Importaciones'
                        break;

                    case '1':
                        titulo = 'Exportaciones'
                        break;

                    case '2':
                        titulo = 'Importaciones'
                        break;                
                }                                
                
                switch(tipoDato){
                    case 'recaladas':
                         titulo = titulo + "     N° de Recaladas";                        
                        break;

                    case 'fobus':
                        titulo = titulo + "     USD";                                   
                        break;
                }

                $("#tituloNombre").val(titulo);
                $("#nivel1Nombre").val(nivel1Nombre);
                $("#nivel2Nombre").val(nivel2Nombre);                                          
                
		$("#FormularioExportacion").submit();
});
});
</script>

