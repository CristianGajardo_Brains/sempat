
var count = 0;


function filtros(){

    var filtros = '';
    var mensaje = '';
    count = 0;

    filtros += filtrosValores($("#rut"), "Import Id");
    filtros += filtrosValores($("#importador"), "Importer");
    filtros += filtrosValores($("#naviera"), "Shipping Commpany");
    filtros += filtrosValores($("#nave"), "Vessel");
    filtros += filtrosValores($("#tipoNave"), "Vessel Type");
    
    filtros += filtrosValores($("#tipo"), "Vehicle Type");
    filtros += filtrosValores($("#producto"), "Product");
    filtros += filtrosValores($("#marca"), "Brand");
    filtros += filtrosValores($("#pago"), "Pay");
    filtros += filtrosValores($("#via"), "Mode of Transport");
    
    filtros += filtrosValores($("#trafico"), "Traffic");
    filtros += filtrosValores($("#paisOrigen"), "Country Of Origin");
    filtros += filtrosValores($("#puertoEmbarque"), "Port Of Loading");
    filtros += filtrosValores($("#puertoDescarga"), "Port Of Discharge");
    filtros += filtrosValores($("#aduana"), "Customs");
	filtros += filtrosValores($("#estado"), "Status");

    if(count > 0){
        mensaje = "<font color='#3D85FE' size='2'>Filters:</font><br>"                
    }
                    
    if(count > 4){
        mensaje += "<div style='height:60px; overflow-y: scroll'>" + filtros + "</div>";
        
    }
    else{
        mensaje += "<div style='height:60px'>" + filtros + "</div>";
    }

    return  mensaje;


}


function buscarNivel1(){

    var nivel1 = $("#selectNivel1").val();
    var nivel2 = $("#selectNivel2").val();
    var nivel3 = $("#selectNivel3").val();
    var nivel4 = $("#selectNivel4").val();
    var nivel5 = $("#selectNivel5").val();

    var fechaDesde = $("#fechaDesde").val();
    var fechaHasta = $("#fechaHasta").val();


    if(fechaDesde == "" && fechaHasta == ""){
	
	
		var mercadoId = $("input[name=mercado]:checked").val();
	
		if(mercadoId != "1"){
			fechasDefecto("VEHICULOS_PE"); 
		}
		else{
			fechasDefecto("VEHICULOS"); 
		}
	    
        fechaDesde = $("#fechaDesde").val();
        fechaHasta = $("#fechaHasta").val();

    }


    if(validarFormulario()){

        $("#divResultado").html("");  
        $("#tablaNivelesDesc").html("");   

        $("#imgCargando").css("display","block");
        $("#divPanelFiltros").css("display","none");
        $("#divOpciones").css("display","none");
        $("#divGraficos").css("display","none");
        $("#divGrafico").css("display","none");
        $("#divGraficoA").css("display","none");
        $("#divGraficoB").css("display","none");
        $("#divMensaje").css("display","none");

        var rut = $("#rut").val();
        var importador = $("#importador").val();        
        var naviera = $("#naviera").val();
        var nave = $("#nave").val();
        var tipoNave = $("#tipoNave").val();        
        
        var tipo = $("#tipo").val();
        var producto = $("#producto").val();
        var marca = $("#marca").val();
        var pago = $("#pago").val();
        var via = $("#via").val();
        
        var trafico = $("#trafico").val();
        var paisOrigen = $("#paisOrigen").val();
        var puertoEmbarque = $("#puertoEmbarque").val();
        var puertoDescarga = $("#puertoDescarga").val();
        var aduana = $("#aduana").val();
		var estado = $("#estado").val();

        var nivel1Titulo = $("#selectNivel1 option:selected").text();
                
        $('#selectGraficoReporte').val("line");
        $('#selectselectGrafico').html("Lines");

        var tipoDato = $("input[name=tipoDato]:checked").val();
		var mercadoId = $("input[name=mercado]:checked").val();
        
        var subtitulo = "";
		
		
		if(mercadoId == "1"){

			switch(tipoDato){
				case 'cantidad':
					$('#tituloGrafico').val("Chile Imports in Units");
					$('#tituloGraficoEjeY').val("Units");                
					$('.tdTipo').css("display", "");
					subtitulo = "in Units";
					break;
				
				case 'toneladas':
					$('#tituloGrafico').val("Chile Imports in Tons");
					$('#tituloGraficoEjeY').val("Tons");
					$('.tdTipo').css("display", "none");
					subtitulo = "in Tons";
					break;
				
				case 'fletes':
					$('#tituloGrafico').val("Chile Imports in Freight USD");
					$('#tituloGraficoEjeY').val("Freight USD");
					$('.tdTipo').css("display", "none");
					subtitulo = "in Freight USD";
					break;
			}
			$('#tdResumen').html("<center><font color='#3D85FE' size='3'>Chile Imports</font><br>" + subtitulo + "</center>" + filtros());
			
		}
		else if(mercadoId == "2"){
			
			switch(tipoDato){
				case 'cantidad':
					$('#tituloGrafico').val("Peru Imports in Units");
					$('#tituloGraficoEjeY').val("Units");                
					$('.tdTipo').css("display", "");
					subtitulo = "in Units";
					break;
				
				case 'toneladas':
					$('#tituloGrafico').val("Peru Imports in Tons");
					$('#tituloGraficoEjeY').val("Tons");
					$('.tdTipo').css("display", "none");
					subtitulo = "in Tons";
					break;
				
				case 'fletes':
					$('#tituloGrafico').val("Peru Imports in Freight USD");
					$('#tituloGraficoEjeY').val("Freight USD");
					$('.tdTipo').css("display", "none");
					subtitulo = "in Freight USD";
					break;
			}
			$('#tdResumen').html("<center><font color='#3D85FE' size='3'>Peru Imports</font><br>" + subtitulo + "</center>" + filtros());			
		
		}
		else{
		
			switch(tipoDato){
				case 'cantidad':
					$('#tituloGrafico').val("Chile - Peru Imports in Units");
					$('#tituloGraficoEjeY').val("Units");                
					$('.tdTipo').css("display", "");
					subtitulo = "in Units";
					break;
				
				case 'toneladas':
					$('#tituloGrafico').val("Chile - Peru Imports in Tons");
					$('#tituloGraficoEjeY').val("Tons");
					$('.tdTipo').css("display", "none");
					subtitulo = "in Tons";
					break;
				
			}
			$('#tdResumen').html("<center><font color='#3D85FE' size='3'>Chile - Peru Imports</font><br>" + subtitulo + "</center>" + filtros());
		
        }
        

        $.ajax({
            type: "POST",
            url: "vehiculosReporteNivel1.php",
            //async: false,
            data: { mercadoId:mercadoId, fechaDesde: fechaDesde, fechaHasta: fechaHasta, 
                    rut: rut, importador: importador, nave: nave, tipoNave: tipoNave, naviera: naviera, 
                    tipo: tipo, producto: producto, marca: marca, pago: pago, via: via, estado: estado,
                    trafico: trafico, paisOrigen: paisOrigen, puertoEmbarque: puertoEmbarque, puertoDescarga: puertoDescarga, aduana: aduana,
                    tipoDato:tipoDato, nivel1: nivel1, nivel2: nivel2, nivel3: nivel3, nivel4: nivel4, nivel5: nivel5, nivel1Titulo: nivel1Titulo}
            })
            .done(function( msg ) {
                
                if(msg.trim() != ""){
                                    
                    $("#divResultado").html(msg);                 
                    $("#divOpciones").css("display","block");

                    $("#tBody tr").each(function (index) {

                        if(index < 3){

                            $(this).children("td").each(function (index2, td) {

                                if(index2 == 0){

                                    $(td).children("input").each(function (index3, input) {
                                        input.click();
                                    });

                                    $(td).children("span ").each(function (index3, span) {
                                        $(span).removeClass('checkboxC');
                                        $(span).addClass('checkboxChecked');
                                    });

                                }                                                
                            });
                        }                    
                    });


                    var nivelesDesc = "";                

                    if(nivel1 != "")
                        nivelesDesc = nivelesDesc + "<tr><td><div class='nivel1'></div></td><td class='desc'>Level 1: " + nivel1Titulo + "</td></tr>"; 

                    if(nivel2.trim() != ""){
                        var nivel2Titulo = $("#selectNivel2 option:selected").text();
                        nivelesDesc = nivelesDesc + "<tr><td><div class='nivel2'></div></td><td class='desc'>Level 2: " + nivel2Titulo + "</td></tr>"; 
                    }

                    if(nivel3 != ""){
                        var nivel3Titulo = $("#selectNivel3 option:selected").text();
                        nivelesDesc = nivelesDesc + "<tr><td><div class='nivel3'></div></td><td class='desc'>Level 3: " + nivel3Titulo + "</td></tr>"; 
                    }

                    if(nivel4 != ""){
                        var nivel4Titulo = $("#selectNivel4 option:selected").text();
                        nivelesDesc = nivelesDesc + "<tr><td><div class='nivel4'></div></td><td class='desc'>Level 4: " + nivel4Titulo + "</td></tr>"; 
                    }

                    if(nivel5 != ""){
                        var nivel5Titulo = $("#selectNivel5 option:selected").text();
                        nivelesDesc = nivelesDesc + "<tr><td><div class='nivel5'></div></td><td class='desc'>Level 5: " + nivel5Titulo + "</td></tr>"; 
                    }                   

                    Custom.initChkbox();
					
					 
                    
                    $("#divOpciones").css("display","block");
                    $("#tablaNivelesDesc").html(nivelesDesc);
					
					
					
                    
                    
                }
                else{
                    $('#divResultado').html("<div style='margin-left:30px'><label style='color:red'>Your search returns no results.</label></div>");
                }

                $("#imgCargando").css("display","none");

        }); 

    }    

}


function buscarNivel2(obj, fechaDesde, fechaHasta, nivel2, nivel3, nivel4, nivel5, tipoDato, nivel1Id, otros){

    var tr = obj.parentNode;  
    var className = $(tr).hasClass('OK');                

    if(className == false && nivel2 != ""){

        $.ajax({                
            type: "POST",
            url: "vehiculosReporteNivel2.php",
            async: false,
            data: {fechaDesde: fechaDesde, fechaHasta: fechaHasta, tipoDato:tipoDato, nivel2: nivel2, nivel3: nivel3, nivel4: nivel4, nivel5: nivel5, trId: $(tr).attr('id'), nivel1Id: nivel1Id, otros: otros}
            })
            .done(function(msg ) {
                $(tr).addClass('OK');            
                $(msg).insertAfter(tr);
        });                
    }
    else{                        
        desplegarNivel($(tr).attr('id'), 2);
    }
}


function buscarNivel3(obj, fechaDesde, fechaHasta, nivel3, nivel4, nivel5, tipoDato, nivel1Id, nivel2Id, otros){

    var tr = obj.parentNode;  
    var className = $(tr).hasClass('OK');                

    if(className == false && nivel3 != ""){

        $.ajax({
            type: "POST",
            url: "vehiculosReporteNivel3.php",
            async: false,
            data: {fechaDesde: fechaDesde, fechaHasta: fechaHasta, tipoDato:tipoDato, nivel3: nivel3, nivel4: nivel4, nivel5: nivel5, trId: $(tr).attr('id'), nivel1Id: nivel1Id, nivel2Id: nivel2Id, otros: otros}
            })
            .done(function(msg ) {
                $(tr).addClass('OK');            
                $(msg).insertAfter(tr);
        });                
    }
    else{                        
        desplegarNivel($(tr).attr('id'), 3);
    }
}


function buscarNivel4(obj, fechaDesde, fechaHasta, nivel4, nivel5, tipoDato, nivel1Id, nivel2Id, nivel3Id, otros){

    var tr = obj.parentNode;  
    var className = $(tr).hasClass('OK');                

    if(className == false && nivel4 != ""){

        $.ajax({
            type: "POST",
            url: "vehiculosReporteNivel4.php",
            async: false,
            data: {fechaDesde: fechaDesde, fechaHasta: fechaHasta, tipoDato:tipoDato, nivel4: nivel4, nivel5: nivel5, trId: $(tr).attr('id'), nivel1Id: nivel1Id, nivel2Id: nivel2Id, nivel3Id: nivel3Id, otros: otros}
            })
            .done(function(msg ) {
                $(tr).addClass('OK');            
                $(msg).insertAfter(tr);
        });                
    }
    else{                        
        desplegarNivel($(tr).attr('id'), 4);
    }
}


function buscarNivel5(obj, fechaDesde, fechaHasta, nivel5, tipoDato, nivel1Id, nivel2Id, nivel3Id, nivel4Id, otros){

    var tr = obj.parentNode;  
    var className = $(tr).hasClass('OK');                

    if(className == false && nivel5 != ""){

        $.ajax({
            type: "POST",
            url: "vehiculosReporteNivel5.php",
            async: false,
            data: {fechaDesde: fechaDesde, fechaHasta: fechaHasta, tipoDato:tipoDato, nivel5: nivel5, trId: $(tr).attr('id'), nivel1Id: nivel1Id, nivel2Id: nivel2Id, nivel3Id: nivel3Id, nivel4Id: nivel4Id, otros: otros}
            })
            .done(function(msg ) {
                $(tr).addClass('OK');            
                $(msg).insertAfter(tr);                                                            
        });                
    }
    else{                        
        desplegarNivel($(tr).attr('id'), 5);
    }
}

    
function validarFormulario(){

    var mensaje = "";

    var nivel1 = $("#selectNivel1").val();
    var fechaDesde = $("#fechaDesde").val();
    var fechaHasta = $("#fechaHasta").val();
            
    if(nivel1 == ""){
        mensaje = "You must select the first level.<br>";
    }
    
    if(fechaDesde == ""){
        mensaje += "You Haven't entered the start date.<br>";
    }
    
    if(fechaHasta == ""){
        mensaje += "You haven't entered the end date.<br>";
    }
    
    if(fechaDesde != "" && fechaHasta != "" && validarFechaMenor(fechaDesde, fechaHasta) == false){
        mensaje += "The initial date may not exceed the final date.<br>";
    }
    
    if(fechaDesde != "" && fechaHasta != "" && fechaDiferenciasDias(fechaDesde, fechaHasta) > 365){
        mensaje += "The difference between the dates can not be longer than one year.<br>";
    }
            
    if(mensaje != ""){
        $("#divMensaje").html(mensaje);
        $("#divMensaje").css("display","block");
        return false;
    }
    else{
        $("#divMensaje").html("");
        $("#divMensaje").css("display","none");
        return true;
    }

}
    
    
    