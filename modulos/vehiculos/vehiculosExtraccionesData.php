<?PHP

session_start();
include ("../../librerias/conexion.php");
require('vehiculos.class.php');
$objVehiculos = new vehiculos();


$usuarioId = $_SESSION['SEMPAT_usuarioId'];
$clienteId = $_SESSION['SEMPAT_clienteId'];

$fechaDesde = mb_convert_encoding(trim($_POST['fechaDesde']), "ISO-8859-1", "UTF-8");
$fechaHasta = mb_convert_encoding(trim($_POST['fechaHasta']), "ISO-8859-1", "UTF-8");
$carpeta = mb_convert_encoding(trim($_POST['carpeta']), "ISO-8859-1", "UTF-8");


$nombreArchivo = mb_convert_encoding(trim($_POST['nombreArchivo']), "ISO-8859-1", "UTF-8");

$etapa = mb_convert_encoding(trim($_POST['etapa']), "ISO-8859-1", "UTF-8");

$tipoDato = mb_convert_encoding(trim($_POST['tipoDato']), "ISO-8859-1", "UTF-8");

$mercadoId = mb_convert_encoding(trim($_POST['mercadoId']), "ISO-8859-1", "UTF-8");

$importadorRut = mb_convert_encoding(trim($_POST['rut']), "ISO-8859-1", "UTF-8");
$importadorNombre= mb_convert_encoding(trim($_POST['importador']), "ISO-8859-1", "UTF-8");
$naviera = mb_convert_encoding(trim($_POST['naviera']), "ISO-8859-1", "UTF-8");
$nave = mb_convert_encoding(trim($_POST['nave']), "ISO-8859-1", "UTF-8");
$tipoNave = mb_convert_encoding(trim($_POST['tipoNave']), "ISO-8859-1", "UTF-8");

$tipo = mb_convert_encoding(trim($_POST['tipo']), "ISO-8859-1", "UTF-8");
$producto = mb_convert_encoding(trim($_POST['producto']), "ISO-8859-1", "UTF-8");
$marca = mb_convert_encoding(trim($_POST['marca']), "ISO-8859-1", "UTF-8");
$pago = mb_convert_encoding(trim($_POST['pago']), "ISO-8859-1", "UTF-8");
$via = mb_convert_encoding(trim($_POST['via']), "ISO-8859-1", "UTF-8");

$trafico = mb_convert_encoding(trim($_POST['trafico']), "ISO-8859-1", "UTF-8");
$paisOrigen = mb_convert_encoding(trim($_POST['paisOrigen']), "ISO-8859-1", "UTF-8");
$puertoEmbarque = mb_convert_encoding(trim($_POST['puertoEmbarque']), "ISO-8859-1", "UTF-8");
$puertoDescarga = mb_convert_encoding(trim($_POST['puertoDescarga']), "ISO-8859-1", "UTF-8");
$aduana = mb_convert_encoding(trim($_POST['aduana']), "ISO-8859-1", "UTF-8");
$estado = mb_convert_encoding(trim($_POST['estado']), "ISO-8859-1", "UTF-8");

$rut = str_replace("\'", "'", $rut);
$importador = str_replace("\'", "'", $importador);
$naviera = str_replace("\'", "'", $naviera);
$nave = str_replace("\'", "'", $nave);
$tipoNave = str_replace("\'", "'", $tipoNave);

$tipo = str_replace("\'", "'", $tipo);
$producto = str_replace("\'", "'", $producto);
$marca = str_replace("\'", "'", $marca);
$pago = str_replace("\'", "'", $pago);
$via = str_replace("\'", "'", $via);

$trafico = str_replace("\'", "'", $trafico);
$paisOrigen = str_replace("\'", "'", $paisOrigen);
$puertoEmbarque = str_replace("\'", "'", $puertoEmbarque);
$puertoDescarga = str_replace("\'", "'", $puertoDescarga);
$aduana = str_replace("\'", "'", $aduana);
$estado = str_replace("\'", "'", $estado);


$chkrut = mb_convert_encoding(trim($_POST['chkrut']), "ISO-8859-1", "UTF-8");
$chkimportador = mb_convert_encoding(trim($_POST['chkimportador']), "ISO-8859-1", "UTF-8");
$chknaviera = mb_convert_encoding(trim($_POST['chknaviera']), "ISO-8859-1", "UTF-8");
$chknave = mb_convert_encoding(trim($_POST['chknave']), "ISO-8859-1", "UTF-8");
$chktipoNave = mb_convert_encoding(trim($_POST['chktipoNave']), "ISO-8859-1", "UTF-8");
$chktipo = mb_convert_encoding(trim($_POST['chktipo']), "ISO-8859-1", "UTF-8");
$chkproducto = mb_convert_encoding(trim($_POST['chkproducto']), "ISO-8859-1", "UTF-8");
$chkmarca = mb_convert_encoding(trim($_POST['chkmarca']), "ISO-8859-1", "UTF-8");
$chkpago = mb_convert_encoding(trim($_POST['chkpago']), "ISO-8859-1", "UTF-8");
$chkvia = mb_convert_encoding(trim($_POST['chkvia']), "ISO-8859-1", "UTF-8");
$chktrafico = mb_convert_encoding(trim($_POST['chktrafico']), "ISO-8859-1", "UTF-8");
$chkpaisOrigen = mb_convert_encoding(trim($_POST['chkpaisOrigen']), "ISO-8859-1", "UTF-8");
$chkpuertoEmbarque = mb_convert_encoding(trim($_POST['chkpuertoEmbarque']), "ISO-8859-1", "UTF-8");
$chkpuertoDescarga = mb_convert_encoding(trim($_POST['chkpuertoDescarga']), "ISO-8859-1", "UTF-8");
$chkaduana = mb_convert_encoding(trim($_POST['chkaduana']), "ISO-8859-1", "UTF-8");
$chkestado = mb_convert_encoding(trim($_POST['chkestado']), "ISO-8859-1", "UTF-8");


$uploaddir = "../../Reportes/" . $carpeta . "/";
			
if (file_exists($uploaddir)==false) {
    mkdir($uploaddir, 0700, true);
}

$uploaddir = $uploaddir . "/" . $usuarioId . "/";

if (file_exists($uploaddir)==false) {
    mkdir($uploaddir, 0700, true);
}

$objVehiculos->vehiculosExtracciones($mercadoId, $fechaDesde, $fechaHasta, $tipoDato, $importadorRut, $importadorNombre, $naviera, $nave, $tipoNave, $tipo, $producto, $marca, $pago, $via, $trafico, $paisOrigen, $puertoEmbarque, $puertoDescarga, $aduana, $estado, $chkrut, $chkimportador, $chknaviera, $chknave, $chktipoNave, $chktipo, $chkproducto, $chkmarca, $chkpago, $chkvia, $chktrafico, $chkpaisOrigen, $chkpuertoEmbarque, $chkpuertoDescarga, $chkaduana, $chkestado, $nombreArchivo, $usuarioId);

?>
