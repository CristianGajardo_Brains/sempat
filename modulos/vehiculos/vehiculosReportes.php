<?
session_start();

setlocale(LC_TIME, "spanish");
$fechaActual = htmlentities(strftime("%A %d de %B del %Y"));
$fechaActual = ucfirst($fechaActual);


$usuarioLog = $_SESSION["SEMPAT_usuarioNombre"];
$usuarioId = $_SESSION["SEMPAT_usuarioId"];
$clienteId = $_SESSION["SEMPAT_clienteId"];


if ($_SESSION['SEMPAT_usuarioId'] == "") {
    echo "<script>window.location='../../index.php';</script>";
}

include("../../default.php");
$plantilla->setPath('../../plantillas/');
$plantilla->setTemplate("header_vehiculo");
$plantilla->setVars(array("USUARIO" => " $usuarioLog ",
    "FECHA" => "$fechaActual"
));
echo $plantilla->show();

require('../../clases/sempat.class.php');
$objSem = new sempat();

require('vehiculos.class.php');
$objVeh = new vehiculos();

$moduloId = 37;
$menuId = 87;


$htmlMenu = $objSem->menuRetornarHtml($usuarioId, $menuId);


$moduloFiltros = $objSem->admBuscarFiltros($clienteId, $moduloId);
$scriptAC = "";


//$consultaFechas = $objSem->manifiestoFechasDefecto($clienteId);


// <editor-fold>
if ($moduloFiltros) {

    $filttrosNivel = "";
    $filtrosHtml = "<table style='margin-left:40px'>";
    $numFiltros = 0;
    $arreglo = array();
	
	if($clienteId == "4" || $usuarioId == "103"){
		$filttrosNivel = "<option value='28'>Market</option>";	
	}

    while ($filtros= mssql_fetch_array($moduloFiltros)) {
                
        if($filtros["filtroVisible"] == "1"){
            
            $numFiltros++;
                      
            if(trim($filtros["filtroEtiqueta"]) != ""){
                
                $filttrosNivel .= "<option value='" . $filtros["nivelId"] . "'>" . htmlentities($filtros["filtroEtiquetaIngles"]) . "</option>";            
                                                                                                                                      
                //$filtrosHtml .= "<input id=" . $filtros["filtroHtml"] . " type='text' class='inputFiltro' placeHolder='" . htmlentities($filtros["filtroEtiqueta"]) . "'/><input type='text' class='inputFiltroRight' readonly disabled/>";
                //$filtrosHtml .= "<input id=" . $filtros["filtroHtml"] . " type='text'/>";
                
                $arreglo[$filtros["filtroOrden"]][$filtros["filtroColumna"] - 1] = "<td width='340px'><input type='text' id=" . $filtros["filtroHtml"] . "></input></td>";                               
                
                $scriptAC .= "$('#"  . $filtros["filtroHtml"] . "').tokenInput('../../complementos/diccionarios?nivelId=" . htmlentities($filtros["nivelId"]) . "', {
                                            theme: 'facebook',
                                            queryParam: 'filtro',
                                            minChars: 3,
                                            placeHolder: '" . mb_convert_encoding($filtros["filtroEtiquetaIngles"], "UTF-8", "ISO-8859-1")  . "'
                                        });\n\n";                                                            
            }
            else{
                $arreglo[$filtros["filtroOrden"]][$filtros["filtroColumna"] - 1] = "<td></td>";
            }
        }
        else{
            
            if(trim($filtros["filtroHtml"]) != ""){               
                $filtrosHtml .= "<input id=" . $filtros["filtroHtml"] . " type='text' style='display:none'/><input type='text' readonly disabled style='display:none'/>";
            }            
        }
        
//        if(($numFiltros % 5) == 0){
//            $filtrosHtml.= "<br><br>";
//        }
        
    }        
    
    for($i = 1; $i <= count($arreglo); $i++){
        $filtrosHtml = $filtrosHtml . "<tr>";
        
        if($arreglo[$i][0] != ""){
            $filtrosHtml = $filtrosHtml . $arreglo[$i][0];
        }
        else{
            $filtrosHtml = $filtrosHtml . "<td></td>";
        }

        if($arreglo[$i][1] != ""){
            $filtrosHtml = $filtrosHtml . $arreglo[$i][1];
        }
        else{
            $filtrosHtml = $filtrosHtml . "<td></td>";
        }

        if($arreglo[$i][2] != ""){
            $filtrosHtml = $filtrosHtml . $arreglo[$i][2];
        }
        else{
            $filtrosHtml = $filtrosHtml . "<td></td>";
        }

        $filtrosHtml = $filtrosHtml . "</tr>";
    }   
    
    $filtrosHtml .= "</table>";
    

}


if($clienteId == "18"){
	$listaArchivos = $objVeh->leerArchivosNyk();	
}
else{
	$listaArchivos = $objVeh->leerArchivos();	
}

// </editor-fold>

?>

<script type="text/javascript" src="vehiculosReportes.js"></script>

<script type="text/javascript">
                            
    $(document).ready(function(){
        
        <? echo $scriptAC; ?>    
                
        $("#fechaDesde").mask("99-99-9999");
        $("#fechaHasta").mask("99-99-9999");
		
		$('#btnChile').bind('click', function() {
            deschequear(this);
        });
        
        $('#btnPeru').bind('click', function() {
            deschequear(this);
        });  
        
        $('#fechaDesde').bind('blur', function() {
            validarFecha(this);
        });
        
        $('#fechaHasta').bind('blur', function() {
            validarFecha(this);
        });   

		
        var url = '../../complementos/diccionarios?nivelId=122';
        var series = [];
        series[0] = {id: 1, name: 'MARITIME'};

        $('#token-input-via').remove();
        $('#via').tokenInput(url, { prePopulate: series,theme: 'facebook', queryParam: 'filtro', minChars: 3, placeHolder: 'Mode of Transport'});

        $('#token-input-via').css({                
        color: "#3D85FE"
        });                    
        $('#token-input-via').val("Mode of Transport: 1 selected");

         
                        

    });


</script>
            
<? echo $htmlMenu; ?>

<div class="divMinHeight">


    <div id="divGuardarReporte" class="divPopUp">

        <label>SAVED REPORT</label>

        <br><br>

        <input type="text" class="inputFiltro" placeHolder="Nombre"/>
        <input type="text" class="inputFiltroRight" readonly disabled/>

        <br><br>

        <div style="text-align: left; width:200px; margin-left: 50px; min-height: 160px">

            Level 1: Port agent <br>
            Level 2: Port <br>

        </div>

        <div style="margin-left:10px">
            <input type="button" class="btnPlomo" value="CANCEL" onclick="cerrarReporte()"/>                            
            <input type="button" class="btnAzul" value="SAVE"/>  
        </div>

    </div>

<? if($clienteId == "4" || $clienteId == "9" || $usuarioId == "157" || $usuarioId == "156"){ ?>
    <div id="divReportesGuardados" class="divPopUp">

        <label>REPORTS</label>

        <br><br>

        <div style="text-align: left; width:370px; margin-left: 20px; height: 220px; overflow: scroll">
		
			<br/>

            <table>
				<?php echo $listaArchivos; ?>			
				
            </table>

        </div>

        <div style="margin-left:10px">
            <input type="button" class="btnPlomo" value="CLOSE" onclick="cerrarReporte2()"/>                                        
        </div>

    </div>
<? } ?>



    <div class="divSubContenedor">

        <div class="divPanelBusqueda" onClick="mostrarFiltrosBusqueda()">                        
            <img src="../../imagenes/lupa.png" alt="" title=""><label>SEARCH PANEL</label>
        </div>

        <div id="divPanelFiltros" class="divPanelFiltros">
            <div class="divPanelFiltros1" style="height: 200px" >

                <table>
                    <tr>
                        <td valign="top" width="400px">
                            <label>DATES</label>

                            <br><br>

                            <input id="fechaDesde" type="text" class="inputFecha" placeHolder="Start" value=""/><input id="btnFechaDesde" type="button" class="inputFechaBtn" />
<!--                            <input id="fechaDesdeDef" type="hidden"  value="<? //echo $fechaDesde; ?>"/>-->


                            <script type="text/javascript">//<![CDATA[
                                var myCal1 = Calendar.setup({
                                    checkRange: false,
                                    inputField: "fechaDesde",
                                    trigger: "btnFechaDesde",
                                    //onSelect: function () { this.hide(); validarFechasReportes($('fechaDesde'), 'fechaDesde', 'fechaHasta');},
                                    dateFormat: "%d-%m-%Y"
                                });
                                myCal1.setLanguage('es');
                                //]]></script>

                            <input id="fechaHasta" type="text" class="inputFecha" placeHolder="End" value="" style="margin-left:60px"/><input id="btnFechaHasta" type="button" class="inputFechaBtn" />
<!--                            <input id="fechaHastaDef" type="hidden"  value="<? //echo $fechaHasta; ?>"/>-->


                            <script type="text/javascript">//<![CDATA[
                                var myCal1 = Calendar.setup({
                                    checkRange: false,
                                    inputField: "fechaHasta",
                                    trigger: "btnFechaHasta",
                                    //onSelect: function () { this.hide(); validarFechasReportes($('fechaDesde'), 'fechaDesde', 'fechaHasta');},
                                    dateFormat: "%d-%m-%Y"
                                });
                                myCal1.setLanguage('es');
                                //]]></script>


                            <div id="divMensajeFechas">

                            </div>

                        </td>                         
                        <td valign="top">
                            <label class="niveles">DATA</label>
                            <table>
                                <tr>
                                    <td>
                                        <input id="btnCantidad" type="radio" name="tipoDato" value="cantidad" checked>
                                    </td>
                                    <td valign="bottom">
                                        Units
                                    </td>                                    
                                </tr>
                                <tr>
                                    <td>
                                        <input id="btnToneladas" type="radio" name="tipoDato" value="toneladas">
                                    </td>
                                    <td>
                                        Tons
                                    </td>                                    
                                </tr>
								<? if($clienteId == 4){ ?>								
								<tr>
									<td>
                                        <input id="btnFletes" type="radio" name="tipoDato" value="fletes">
                                    </td>
                                    <td>
                                        Freight USD
                                    </td>
								</tr>								
								<? }?>
							
                            </table>
                        </td>
						
						<? if($clienteId == "4" || $usuarioId == "103"){ ?>
							 <td valign="top" style='padding-left: 120px'>
								<label class="niveles">MARKET</label>
								<table>
									<tr>
										<td>
											<input id="btnChile" type="radio" name="mercado" value="1" checked>
										</td>
										<td valign="bottom">
											Chile
										</td>                                    
									</tr>
									<tr>
										<td>
											<input id="btnPeru" type="radio" name="mercado" value="2">
										</td>
										<td>
											Peru
										</td>                                    
									</tr>   		
								</table>
							</td>
							<? if($clienteId == "4" || $clienteId == "9"){ ?>
							<? //if($clienteId == "4" && $usuarioId == "103"){ ?>
							
							
							<td valign="top" style='padding-left: 350px'>
								<img src="imagenes/pdf.png" alt="" style="width: 25px;height: 25px;cursor: pointer" onclick="abrirReporte2()" title="Reports">							
							</td>
							
							<? } ?>
											
						<?}
						else{ ?>
							<td>
								<input id="mercadoChile" type="radio" name="mercado" value="1" checked style='display:none'/>
							</td>
						<?}
						if($usuarioId == "157" || $usuarioId == "156"){ ?>
							<? //if($clienteId == "4" && $usuarioId == "103"){ ?>
							
							
							<td valign="top" style='padding-left: 540px'>
								<img src="imagenes/pdf.png" alt="" style="width: 25px;height: 25px;cursor: pointer" onclick="abrirReporte2()" title="Reports">
							</td>
							
							<? } ?>
						
                    </tr>
                </table>

                <div style="width:100%; margin-top: 35px; margin-bottom: 8px;">                            
                    <label>LEVELS</label>                                                    
                </div>

                <select id="selectNivel1" name="selectNivel1" class="styled filtro">
                    <option value="" class="placeHolder">Level 1</option>
                    <? echo $filttrosNivel; ?>
                </select>

                <select id="selectNivel2" name="selectNivel2" class="styled filtro">
                    <option value=""  class="placeHolder">Level 2</option>
                    <? echo $filttrosNivel; ?>
                </select>

                <select id="selectNivel3" name="selectNivel3" class="styled filtro">
                    <option value=""  class="placeHolder">Level 3</option>
                    <? echo $filttrosNivel; ?>
                </select>

                <select id="selectNivel4" name="selectNivel4" class="styled filtro">
                    <option value=""  class="placeHolder">Level 4</option>
                    <? echo $filttrosNivel; ?>
                </select>

                <select id="selectNivel5" name="selectNivel5" class="styled filtro">
                    <option value="" class="placeHolder">Level 5</option>
                    <? echo $filttrosNivel; ?>
                </select>


                <div style="width:100%; margin-top: 40px; margin-bottom: 8px;">                            
                    <label>FILTERS</label>                                                    
                </div>

                <? echo $filtrosHtml; ?>

                <div style="clear: both"></div>

                <div id="divMensaje">
                    You must select the first level.
                </div>

                <div style="float:right; margin-right: 90px; margin-top: 15px; margin-bottom: 15px;">
                    <input type="button" class="btnLogin" value="SEARCH" onclick="buscarNivel1()"/>                            
<!--                                <input type="button" class="btnLogin" value="GUARDAR" onclick="abrirReporte()"/>  -->
                </div>                                                        

            </div>    

            <div style="clear: both"></div>

            <div class="divPanelFiltros2" style="display:none">

                <div style="width:100%; margin-bottom: 8px;">                            
                    <label>STORED REPORTS</label>                            
                </div>   

                <input type="text" class="inputFecha" placeHolder="Nombre"/><input type="button" class="inputLupaBtn" onclick="abrirReporte2()"/>

                <input type="button" class="btnLogin" value="VER"/>

                <br/><br/>

            </div>                                        

        </div>

        <div class="divPanelBusquedaBottom" onClick="mostrarFiltrosBusqueda()">

        </div>

    </div>

    <div id="divGraficos" style="height: 405px; display:none;">

        <div id="divGrafico" class="divGrafico" style="width:1122px; height:400px; display:none;">

        </div>                        

        <div id="divGraficoA" class="divGraficoLeft" style="width:540px; height:390px; display:none;">

        </div>

        <div id="divGraficoB" class="divGraficoRight" style="width:540px; height:390px; display:none;">

        </div> 

    </div>

    <br style="clear:both"/>   

    <div id="divOpciones" class="divOpciones" style="display:none">  

        <table>
            <tr>
                <td width="300px" valign="top">
                    <label>LEVELS</label>
                    <table id="tablaNivelesDesc" class="tablaNiveles">

                    </table>
                </td>
                <td id="tdResumen" valign="top">

                </td>
                <td width="300px" rowspan="2" valign="top">
                    <table>
                        <tr style="height:30px;">
                            <td width="100px" valign="middle">
                                <label>CHARTS</label>
                            </td>
                            <td width="230px" valign="middle">
                                <select id="selectGraficoReporte" name="selectGrafico" class="styled" style="height:26px;">
                                    <option value="line">Lines</option>
                                    <option value="column">Bars</option>
                                    <option value="pie">Percentage</option>
                                </select>

                                <input id="tituloGrafico" type="hidden" value="">
                                <input id="tituloGrafico2" type="hidden" value="">
                                <input id="tituloGraficoEjeY" type="hidden" value="">
                            </td>
                        </tr>                        
                    </table>
                </td>
            </tr>
        </table>

    </div>

    <center id="imgCargando" style="display:none">
        <img src="../../imagenes/cargando.gif" height="42" width="42"><br/><i>Cargando</i>
    </center>

    <div id="divResultado">


    </div>

</div>

<br>
            
<?
$plantilla->setPath('../../plantillas/');
$plantilla->setTemplate("footer");
echo $plantilla->show();
?>