<?php

class vehiculos {
    
    
	
	function leerArchivos(){
    
		$htmlListaArchivos = "";
		
		$dir = "reportes";
		$entradas = array();
		$i = 0;

		$directorio = opendir($dir);
		while ($archivo = readdir($directorio)) {
			if ($archivo != "." && $archivo != "..") {
				$entradas[$archivo] = filemtime($dir . "/" . $archivo);
			}
		}

		arsort($entradas); // arsort o asort
		closedir($directorio);

		foreach ($entradas as $archivo => $valor) {
			if ($i <= 10) {
				$archivoNombre = basename($archivo, ".pdf");
				$htmlListaArchivos = $htmlListaArchivos . "<tr><td>" . $archivoNombre . "</td><td><a href='" . $dir . "/" . $archivo . "' title='Download' target='_blank'><img src='../../imagenes/pdf.png'/></a></td></tr>";
				$i = 0;
			}
		}
		
		return $htmlListaArchivos;
	}
	
	
	function leerArchivosNyk(){
    
		$htmlListaArchivos = "";
		
		$dir = "reportes";
		$entradas = array();
		$i = 0;

		/*$directorio = opendir($dir);
		while ($archivo = readdir($directorio)) {
			if ($archivo != "." && $archivo != "..") {
				$entradas[$archivo] = filemtime($dir . "/" . $archivo);
			}
		}

		arsort($entradas); // arsort o asort
		closedir($directorio);

		foreach ($entradas as $archivo => $valor) {
			if ($i <= 10) {
				$archivoNombre = basename($archivo, ".pdf");
				
				$i = 0;
			}
		}*/
		
		$htmlListaArchivos = $htmlListaArchivos . "<tr><td>Report (PCC - RORO) February 2017 - January 2018</td><td><a href='reportesNyk/Report (PCC - RORO) February 2017 - January 2018.pdf' title='Download' target='_blank'><img src='../../imagenes/pdf.png'/></a></td>    <td><a href='reportesNyk/Pivot Vechicles (PCC - RORO) February 2017 - January 2018.xlsx' title='Download' target='_blank'><img src='../../imagenes/xls.png'/></a></td>  <td><a href='reportesNyk/Report (PCC - RORO) February 2017 - January 2018.pptx' title='Download' target='_blank'><img src='../../imagenes/ppt.png'/></a></td>  </tr>";
		$htmlListaArchivos = $htmlListaArchivos . "<tr><td>Report (PCC - RORO) January - December 2017</td><td><a href='reportesNyk/Report (PCC - RORO) January - December 2017.pdf' title='Download' target='_blank'><img src='../../imagenes/pdf.png'/></a></td>    <td><a href='reportesNyk/Pivot Vechicles (PCC - RORO) January - December 2017.xlsx' title='Download' target='_blank'><img src='../../imagenes/xls.png'/></a></td>  <td><a href='reportesNyk/Report (PCC - RORO) January - December 2017.pptx' title='Download' target='_blank'><img src='../../imagenes/ppt.png'/></a></td>  </tr>";
		$htmlListaArchivos = $htmlListaArchivos . "<tr><td>Report (PCC - RORO) Dec 2016 - Nov 2017</td><td><a href='reportesNyk/Report (PCC - RORO) December 2016 - November 2017.pdf' title='Download' target='_blank'><img src='../../imagenes/pdf.png'/></a></td>    <td><a href='reportesNyk/Pivot Vechicles (PCC - RORO) December 2016 - November 2017.xlsx' title='Download' target='_blank'><img src='../../imagenes/xls.png'/></a></td>  <td><a href='reportesNyk/Report (PCC - RORO) December 2016 - November 2017.pptx' title='Download' target='_blank'><img src='../../imagenes/ppt.png'/></a></td>  </tr>";
		$htmlListaArchivos = $htmlListaArchivos . "<tr><td>Report (PCC - RORO) October 2015 - Sept 2016</td><td><a href='reportesNyk/Report (PCC - RORO) October 2015 - September 2016.pdf' title='Download' target='_blank'><img src='../../imagenes/pdf.png'/></a></td>    <td><a href='reportesNyk/Pivot Vechicles (PCC - RORO) October 2015 - September 2016.xlsx' title='Download' target='_blank'><img src='../../imagenes/xls.png'/></a></td>  <td><a href='reportesNyk/Report (PCC - RORO) October 2015 - September 2016.pptx' title='Download' target='_blank'><img src='../../imagenes/ppt.png'/></a></td>  </tr>";
		$htmlListaArchivos = $htmlListaArchivos . "<tr><td>Report (PCC - RORO) Sept 2015 - August 2016</td><td><a href='reportesNyk/Report (PCC - RORO) September 2015 - August 2016.pdf' title='Download' target='_blank'><img src='../../imagenes/pdf.png'/></a></td>    <td><a href='reportesNyk/Pivot Vechicles (PCC - RORO) September 2015 - August 2016.xlsx' title='Download' target='_blank'><img src='../../imagenes/xls.png'/></a></td>  <td><a href='reportesNyk/Report (PCC - RORO) September 2015 - August 2016.pptx' title='Download' target='_blank'><img src='../../imagenes/ppt.png'/></a></td>  </tr>";
		$htmlListaArchivos = $htmlListaArchivos . "<tr><td>Report (PCC - RORO) August 2015 - July 2016</td><td><a href='reportesNyk/Report (PCC - RORO) August 2015 - July 2016.pdf' title='Download' target='_blank'><img src='../../imagenes/pdf.png'/></a></td>    <td><a href='reportesNyk/Pivot Vechicles (PCC - RORO) August 2015 - July 2016.xlsx' title='Download' target='_blank'><img src='../../imagenes/xls.png'/></a></td>  <td><a href='reportesNyk/Report (PCC - RORO) August 2015 - July 2016.pptx' title='Download' target='_blank'><img src='../../imagenes/ppt.png'/></a></td>  </tr>";
		$htmlListaArchivos = $htmlListaArchivos . "<tr><td>Report (PCC - RORO) July 2015 - June 2016</td><td><a href='reportesNyk/Report (PCC - RORO) July 2015 - June 2016.pdf' title='Download' target='_blank'><img src='../../imagenes/pdf.png'/></a></td>    <td><a href='reportesNyk/Pivot Vechicles (PCC - RORO) July 2015 - June 2016.xlsx' title='Download' target='_blank'><img src='../../imagenes/xls.png'/></a></td>  <td><a href='reportesNyk/Report (PCC - RORO) July 2015 - June 2016.pptx' title='Download' target='_blank'><img src='../../imagenes/ppt.png'/></a></td>  </tr>";
		$htmlListaArchivos = $htmlListaArchivos . "<tr><td>Report (PCC - RORO) June 2015 - May 2016</td><td><a href='reportesNyk/Report (PCC - RORO) June 2015 - May 2016.pdf' title='Download' target='_blank'><img src='../../imagenes/pdf.png'/></a></td>    <td><a href='reportesNyk/Pivot Vechicles (PCC - RORO) June 2015 - May 2016.xlsx' title='Download' target='_blank'><img src='../../imagenes/xls.png'/></a></td>  <td><a href='reportesNyk/Report (PCC - RORO) June 2015 - May 2016.pptx' title='Download' target='_blank'><img src='../../imagenes/ppt.png'/></a></td>  </tr>";
		$htmlListaArchivos = $htmlListaArchivos . "<tr><td>Report (PCC - RORO) May 2015 - April 2016</td><td><a href='reportesNyk/Report (PCC - RORO) May 2015 - April 2016.pdf' title='Download' target='_blank'><img src='../../imagenes/pdf.png'/></a></td>    <td><a href='reportesNyk/Pivot Vechicles (PCC - RORO) May 2015 - April 2016.xlsx' title='Download' target='_blank'><img src='../../imagenes/xls.png'/></a></td>  <td><a href='reportesNyk/Report (PCC - RORO) May 2015 - April 2016.pptx' title='Download' target='_blank'><img src='../../imagenes/ppt.png'/></a></td>  </tr>";
		$htmlListaArchivos = $htmlListaArchivos . "<tr><td>Report (PCC - RORO) April 2015 - March 2016</td><td><a href='reportesNyk/Report (PCC - RORO) April 2015 - March 2016.pdf' title='Download' target='_blank'><img src='../../imagenes/pdf.png'/></a></td>    <td><a href='reportesNyk/Pivot Vechicles (PCC - RORO) April 2015 - March 2016.xlsx' title='Download' target='_blank'><img src='../../imagenes/xls.png'/></a></td>  <td><a href='reportesNyk/Report (PCC - RORO) April 2015 - March 2016.pptx' title='Download' target='_blank'><img src='../../imagenes/ppt.png'/></a></td>  </tr>";
		$htmlListaArchivos = $htmlListaArchivos . "<tr><td>Report (PCC - RORO) March 2015 - February 2016</td><td><a href='reportesNyk/Report (PCC - RORO) March 2015 - February 2016.pdf' title='Download' target='_blank'><img src='../../imagenes/pdf.png'/></a></td>    <td><a href='reportesNyk/Pivot Vechicles (PCC - RORO) March 2015 - February 2016.xlsx' title='Download' target='_blank'><img src='../../imagenes/xls.png'/></a></td>  <td><a href='reportesNyk/Report (PCC - RORO) March 2015 - February 2016.pptx' title='Download' target='_blank'><img src='../../imagenes/ppt.png'/></a></td>  </tr>";
		$htmlListaArchivos = $htmlListaArchivos . "<tr><td>Report (PCC - RORO) February 2015 - January 2016</td><td><a href='reportesNyk/Report (PCC - RORO) February 2015 - January 2016.pdf' title='Download' target='_blank'><img src='../../imagenes/pdf.png'/></a></td>    <td><a href='reportesNyk/Pivot Vechicles (PCC - RORO) February 2015 - January 2016.xlsx' title='Download' target='_blank'><img src='../../imagenes/xls.png'/></a></td>  <td><a href='reportesNyk/Report (PCC - RORO) February 2015 - January 2016.pptx' title='Download' target='_blank'><img src='../../imagenes/ppt.png'/></a></td>  </tr>";
		
		return $htmlListaArchivos;
	}
	
	
    function ultimaFechaManifiesto($clienteId) {
        $query = "Exec seleccionarFechasLimites '" . $clienteId . "','M'";
        $result = sql_db::sql_query($query);
        sql_db::sql_close();
        $row = sql_db::sql_fetch_assoc($result);
        return $row;
    }
    
    
    function manifiestoFechasDefecto($clienteId) {
        $query = "Exec seleccionarFechasDefecto '" . $clienteId . "','M'";
        $result = sql_db::sql_query($query);
        sql_db::sql_close();
        $row = sql_db::sql_fetch_assoc($result);
        return $row;
    }
    
    
    //INDICADORES
    
    function indicadoresCabeceraDatos($fecha) {
        $query = "Exec manifiestoIndicadoresCabecera '" . $fecha . "'";        
        //echo $query;
        $result = sql_db::sql_query($query);
        $row = sql_db::sql_fetch_assoc($result);
        sql_db::sql_close();
        return $row;
    }
    
    
    function indicadoresCabeceraHtml($valorNivel1, $fecha){
        
        $objSem = new vehiculos();
        $resultado = $objSem->indicadoresCabeceraDatos($fecha);
        
        $titulo1 = "JANUARY";
        $titulo2 = "";
        $titulo3 = "";
        $titulo2b = "";
        $titulo3b = "";
        
        if(strtoupper($resultado["mmActual"]) != "ENERO"){
            $titulo1 = "JANUARY - " . strtoupper($resultado["mmActualIngles"]);
        }

        if($resultado["mmAnterior"] != "" && $resultado["mmActual"] != ""){
            $titulo2 = $resultado["mmAnteriorIngles"] . " - " . $resultado["mmActualIngles"];
        }
        
        if($resultado["mmActual"] != ""){
            $titulo3 = $resultado["mmActualIngles"];
        }
        
        if($resultado["mmActual"] == "DICIEMBRE"){
            $titulo2b = $resultado["yyyyActual"];
        }
        else{
            if($resultado["yyyyAnterior"] != ""){
                $titulo2b = $resultado["yyyyAnterior"] . " - " . ($resultado["yyyyActual"]);
            }            
        }
        
        if($resultado["mmActual"] == "DICIEMBRE") {
            $titulo3b = $resultado["yyyyAnterior"];            
        }
        else {            
            if($resultado["yyyyAnterior"] != ""){
                $titulo3b = ($resultado["yyyyAnterior"] - 1) . " - " . ($resultado["yyyyAnterior"]);                
            }                        
        }
        
        
        $htmlThead = "<tr>
                        <th class=\"thCenter\" colspan=\"6\">
                            <div class=\"divThLeft\"></div>
                            <div class=\"divThCenter\">" . $titulo1 . "</div>
                            <div class=\"divThRight\"></div>          
                        </th>                        
                        <th style=\"min-width: 10px;\"></th>
                        <th class=\"thCenter\" colspan=\"4\">
                            <div class=\"divThLeft\"></div>          
                            <div class=\"divThCenter\">" . $titulo2 . "</div>
                            <div class=\"divThRight\"></div>          
                        </th> 
                        <th style=\"min-width: 10px;\"></th>
                        <th class=\"thCenter\" colspan=\"4\">
                            <div class=\"divThLeft\"></div>          
                            <div class=\"divThCenter\">" . $titulo3 . "</div>
                            <div class=\"divThRight\"></div>          
                        </th> 
                    </tr>   
                    <tr>
                        <td colspan=\"2\" class=\"left\" style=\"width:185px;\">
                                " . mb_convert_encoding(trim($valorNivel1), "UTF-8", "ISO-8859-1") .  "
                        </td>
                        <td>
                            " . $resultado["yyyyActual"] . "
                        </td>
                        <td>
                            " . $resultado["yyyyAnterior"] . "
                        </td>
                        <td>
                            DIF.
                        </td>
                        <td>
                            Mkt. SHARE
                        </td>
                        <td class=\"clear\">

                        </td>
                        <td class=\"left\">
                            " . $titulo2b .  "
                        </td>
                        <td>
                            " . $titulo3b .  "
                        </td>
                        <td>
                            DIF.
                        </td>
                        <td>
                            Mkt. SHARE
                        </td>
                        <td class=\"clear\">

                        </td>
                        <td class=\"left\">
                            " . $resultado["yyyyActual"] . "
                        </td>
                        <td>
                            " . $resultado["yyyyAnterior"] . "
                        </td>
                        <td>
                            DIF.
                        </td>
                        <td>
                            Mkt. SHARE
                        </td>                        
                    </tr>";

        return $htmlThead;       

    }
    
    
    function vehiculosIndicadoresNivel1($mercadoId, $fechaDesde, $fechaHasta, $nivel1, $nivel2, $nivel3, $nivel4, $nivel5, $tipoDato, $importadorRut, $importadorNombre, 
            $naviera, $nave, $tipoNave, $tipoVehiculo, $producto, $marca, $pago, $viaTransporte, $trafico, $paisOrigen, $puertoEmbarque, $puertoDescarga, $aduana, $estado, $usuarioId){
    
        //echo "aquiii";
        /*$query = "Exec vehiculosIndicadoresNivel1 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel1 . "','" . $nivel2 . "','" . $nivel3 . "','" . $nivel4 . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $embarcador . "','" . $naviera . "','" . $nave . "','" . $tipoNave . "','" . $puertoOrigen . "','" . $proveedor . "','" . 
                $producto . "','" . $detalle . "','" . $usuarioId . "'";        
        echo $query;*/
                
        $stmt = mssql_init("vehiculosIndicadoresNivel1");
		
		mssql_bind($stmt, '@mercadoId', $mercadoId, SQLVARCHAR);
        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);
        mssql_bind($stmt, '@var1', $nivel1, SQLVARCHAR);
        mssql_bind($stmt, '@var2', $nivel2, SQLVARCHAR);
        mssql_bind($stmt, '@var3', $nivel3, SQLVARCHAR);
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);            
        mssql_bind($stmt, '@importadorRut', $importadorRut, SQLVARCHAR);
        mssql_bind($stmt, '@importadorNombre', $importadorNombre, SQLVARCHAR);
        mssql_bind($stmt, '@naviera', $naviera, SQLVARCHAR);
        mssql_bind($stmt, '@nave', $nave, SQLVARCHAR);
        mssql_bind($stmt, '@tipoNave', $tipoNave, SQLVARCHAR);
        mssql_bind($stmt, '@tipoVehiculo', $tipoVehiculo, SQLVARCHAR);
        mssql_bind($stmt, '@producto', $producto, SQLVARCHAR);
        mssql_bind($stmt, '@marca', $marca, SQLVARCHAR);
        mssql_bind($stmt, '@pago', $pago, SQLVARCHAR);
        mssql_bind($stmt, '@viaTransporte', $viaTransporte, SQLVARCHAR);        
        mssql_bind($stmt, '@trafico', $trafico, SQLVARCHAR);
        mssql_bind($stmt, '@paisOrigen', $paisOrigen, SQLVARCHAR);
        mssql_bind($stmt, '@puertoEmbarque', $puertoEmbarque, SQLVARCHAR);
        mssql_bind($stmt, '@puertoDescarga', $puertoDescarga, SQLVARCHAR);
        mssql_bind($stmt, '@aduana', $aduana, SQLVARCHAR);
		mssql_bind($stmt, '@estado', $estado, SQLVARCHAR);
        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    function vehiculosIndicadoresNivel1Grafico($fechaDesde, $fechaHasta, $nivel1, $usuarioId){
            
        /*$query = "Exec vehiculosIndicadoresNivel1Grafico '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel1 . "','" . $usuarioId . "'";        
        echo $query;*/
               
        $stmt = mssql_init("vehiculosIndicadoresNivel1Grafico");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);
        mssql_bind($stmt, '@var1', $nivel1, SQLVARCHAR);                                            
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
               
    
    function vehiculosIndicadoresNivel2($fechaDesde, $fechaHasta, $nivel2, $nivel3, $nivel4, $nivel5, $tipoDato, $clienteId, $nivel1Id, $otros, $usuarioId){
    
        
        /*$query = "Exec vehiculosIndicadoresNivel2 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel2 . "','" . $nivel3 . "','" . $nivel4 . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $nivel1Id . "','" . $otros . "','" . $usuarioId . "'";                
        echo $query;*/
                
        $stmt = mssql_init("vehiculosIndicadoresNivel2");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);        
        mssql_bind($stmt, '@var2', $nivel2, SQLVARCHAR);
        mssql_bind($stmt, '@var3', $nivel3, SQLVARCHAR);
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@nivel1Id', $nivel1Id, SQLVARCHAR);
        mssql_bind($stmt, '@otros', $otros, SQLVARCHAR);        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    function vehiculosIndicadoresNivel3($fechaDesde, $fechaHasta, $nivel3, $nivel4, $nivel5, $tipoDato, $clienteId, $nivel1Id, $nivel2Id, $otros, $usuarioId){
    
        
        /*$query = "Exec vehiculosIndicadoresNivel3 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel3 . "','" . $nivel4 . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $nivel1Id . "','" . $nivel2Id . "','" . $otros . "','" . $usuarioId . "'";        
        echo $query;*/
                
        $stmt = mssql_init("vehiculosIndicadoresNivel3");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);                
        mssql_bind($stmt, '@var3', $nivel3, SQLVARCHAR);
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@nivel1Id', $nivel1Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel2Id', $nivel2Id, SQLVARCHAR);
        mssql_bind($stmt, '@otros', $otros, SQLVARCHAR);        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    function vehiculosIndicadoresNivel4($fechaDesde, $fechaHasta, $nivel4, $nivel5, $tipoDato, $clienteId, $nivel1Id, $nivel2Id, $nivel3Id, $otros, $usuarioId){
            
        /*$query = "Exec vehiculosIndicadoresNivel4 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel4 . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $nivel1Id . "','" . $nivel2Id . "','" . $nivel3Id . "','" . $otros . "','" . $usuarioId . "'";      
        echo $query;*/
                
        $stmt = mssql_init("vehiculosIndicadoresNivel4");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);                        
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@nivel1Id', $nivel1Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel2Id', $nivel2Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel3Id', $nivel3Id, SQLVARCHAR);
        mssql_bind($stmt, '@otros', $otros, SQLVARCHAR);        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    function vehiculosIndicadoresNivel5($fechaDesde, $fechaHasta, $nivel5, $tipoDato, $clienteId, $nivel1Id, $nivel2Id, $nivel3Id, $nivel4Id, $otros, $usuarioId){
            
        /*$query = "Exec vehiculosIndicadoresNivel5 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $nivel1Id . "','" . $nivel2Id . "','" . $nivel3Id . "','" . $nivel4Id . "','" . $otros . "','" . $usuarioId . "'";        
        echo $query;*/
                
        $stmt = mssql_init("vehiculosIndicadoresNivel5");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);                                
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@nivel1Id', $nivel1Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel2Id', $nivel2Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel3Id', $nivel3Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel4Id', $nivel4Id, SQLVARCHAR);
        mssql_bind($stmt, '@otros', $otros, SQLVARCHAR);        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    
    //REPORTES
    
    function vehiculosReporteNivel1($mercadoId, $fechaDesde, $fechaHasta, $nivel1, $nivel2, $nivel3, $nivel4, $nivel5, $tipoDato, $importadorRut, $importadorNombre, 
            $naviera, $nave, $tipoNave, $tipoVehiculo, $producto, $marca, $pago, $viaTransporte, $trafico, $paisOrigen, $puertoEmbarque, $puertoDescarga, $aduana, $estado, $usuarioId){
    
        /*$query = "Exec vehiculosReporteNivel1 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel1 . "','" . $nivel2 . "','" . $nivel3 . "','" . $nivel4 . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $embarcador . "','" . $naviera . "','" . $nave . "','" . $tipoNave . "','" . $puertoOrigen . "','" . $proveedor . "','" . 
                $producto . "','" . $detalle . "','" . $usuarioId . "'";        
        echo $query;*/
                        
        
        $stmt = mssql_init("vehiculosReporteNivel1");

		mssql_bind($stmt, '@mercadoId', $mercadoId, SQLVARCHAR);
        mssql_bind($stmt, '@fechaDesdeA', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHastaA', $fechaHasta, SQLVARCHAR);
        mssql_bind($stmt, '@var1', $nivel1, SQLVARCHAR);
        mssql_bind($stmt, '@var2', $nivel2, SQLVARCHAR);
        mssql_bind($stmt, '@var3', $nivel3, SQLVARCHAR);
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@importadorRut', $importadorRut, SQLVARCHAR);
        mssql_bind($stmt, '@importadorNombre', $importadorNombre, SQLVARCHAR);
        mssql_bind($stmt, '@naviera', $naviera, SQLVARCHAR);
        mssql_bind($stmt, '@nave', $nave, SQLVARCHAR);
        mssql_bind($stmt, '@tipoNave', $tipoNave, SQLVARCHAR);
        mssql_bind($stmt, '@tipoVehiculo', $tipoVehiculo, SQLVARCHAR);
        mssql_bind($stmt, '@producto', $producto, SQLVARCHAR);
        mssql_bind($stmt, '@marca', $marca, SQLVARCHAR);
        mssql_bind($stmt, '@pago', $pago, SQLVARCHAR);
        mssql_bind($stmt, '@viaTransporte', $viaTransporte, SQLVARCHAR);        
        mssql_bind($stmt, '@trafico', $trafico, SQLVARCHAR);
        mssql_bind($stmt, '@paisOrigen', $paisOrigen, SQLVARCHAR);
        mssql_bind($stmt, '@puertoEmbarque', $puertoEmbarque, SQLVARCHAR);
        mssql_bind($stmt, '@puertoDescarga', $puertoDescarga, SQLVARCHAR);
        mssql_bind($stmt, '@aduana', $aduana, SQLVARCHAR);                                   
		mssql_bind($stmt, '@estado', $estado, SQLVARCHAR);   
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
          
    
    function vehiculosReporteNivel2($fechaDesde, $fechaHasta, $nivel2, $nivel3, $nivel4, $nivel5, $tipoDato, $clienteId, $nivel1Id, $otros, $usuarioId){
    
        
        $query = "Exec vehiculosReporteNivel2 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel2 . "','" . $nivel3 . "','" . $nivel4 . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $nivel1Id . "','" . $otros . "','" . $usuarioId . "'";        
        echo $query;
                
        $stmt = mssql_init("vehiculosReporteNivel2");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);        
        mssql_bind($stmt, '@var2', $nivel2, SQLVARCHAR);
        mssql_bind($stmt, '@var3', $nivel3, SQLVARCHAR);
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@nivel1Id', $nivel1Id, SQLVARCHAR);
        mssql_bind($stmt, '@otros', $otros, SQLVARCHAR);        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    function vehiculosReporteNivel3($fechaDesde, $fechaHasta, $nivel3, $nivel4, $nivel5, $tipoDato, $clienteId, $nivel1Id, $nivel2Id, $otros, $usuarioId){
    
        
        /*$query = "Exec vehiculosReporteNivel3 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel3 . "','" . $nivel4 . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $nivel1Id . "','" . $nivel2Id . "','" . $otros . "','" . $usuarioId . "'";        
        echo $query;*/
                
        $stmt = mssql_init("vehiculosReporteNivel3");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);                
        mssql_bind($stmt, '@var3', $nivel3, SQLVARCHAR);
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@nivel1Id', $nivel1Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel2Id', $nivel2Id, SQLVARCHAR);
        mssql_bind($stmt, '@otros', $otros, SQLVARCHAR);        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    function vehiculosReporteNivel4($fechaDesde, $fechaHasta, $nivel4, $nivel5, $tipoDato, $clienteId, $nivel1Id, $nivel2Id, $nivel3Id, $otros, $usuarioId){
            
        /*$query = "Exec vehiculosReporteNivel4 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel4 . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $nivel1Id . "','" . $nivel2Id . "','" . $nivel3Id . "','" . $otros . "','" . $usuarioId . "'";        
        echo $query;*/
                
        $stmt = mssql_init("vehiculosReporteNivel4");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);                        
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@nivel1Id', $nivel1Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel2Id', $nivel2Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel3Id', $nivel3Id, SQLVARCHAR);
        mssql_bind($stmt, '@otros', $otros, SQLVARCHAR);        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    function vehiculosReporteNivel5($fechaDesde, $fechaHasta, $nivel5, $tipoDato, $clienteId, $nivel1Id, $nivel2Id, $nivel3Id, $nivel4Id, $otros, $usuarioId){
            
        /*$query = "Exec vehiculosReporteNivel5 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $nivel1Id . "','" . $nivel2Id . "','" . $nivel3Id . "','" . $nivel4Id . "','" . $otros . "','" . $usuarioId . "'";        
        echo $query;*/
                
        $stmt = mssql_init("vehiculosReporteNivel5");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);                                
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@nivel1Id', $nivel1Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel2Id', $nivel2Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel3Id', $nivel3Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel4Id', $nivel4Id, SQLVARCHAR);
        mssql_bind($stmt, '@otros', $otros, SQLVARCHAR);        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    //EXTRACCIONES
    
    function vehiculosExtracciones($mercadoId, $fechaDesde, $fechaHasta, $tipoDato, $importadorRut, $importadorNombre, 
            $naviera, $nave, $tipoNave, $tipoVehiculo, $producto, $marca, $pago, $viaTransporte, $trafico, $paisOrigen, $puertoEmbarque, $puertoDescarga, $aduana, $estado,
            $chkimportadorRut, $chkimportadorNombre, $chknaviera, $chknave, $chktipoNave, $chktipoVehiculo, $chkproducto, $chkmarca, $chkpago, $chkviaTransporte, 
            $chktrafico, $chkpaisOrigen, $chkpuertoEmbarque, $chkpuertoDescarga, $chkaduana, $chkestado, $nombreArchivo, $usuarioId){
    
        
        $query = "Exec vehiculosExtracciones '" . $fechaDesde . "'
		,'" . $fechaHasta . "'
		,'" .$tipoDato . "'
		,'" . $importadorRut . "'
		,'" . $importadorNombre . "'
		,'" . $naviera . "'
		,'" . $nave . "'
		,'" . $tipoNave . "'
		,'" . $tipoVehiculo . "'
		,'" . $producto . "'
		,'" . $marca . "'
		,'" . $pago . "'
		,'" . $viaTransporte . "'
		,'" . $trafico . "'
		,'" . $paisOrigen . "'
		,'" . $puertoEmbarque . "'
		,'" . $puertoDescarga . "'
		,'" . $aduana . "'
		,'" . $estado . "'
		,'" . $chkimportadorRut . "'
		,'" . $chkimportadorNombre . "'
		,'" . $chknaviera . "'
		,'" . $chknave . "'
		,'" . $chktipoNave . "'
		,'" . $chktipoVehiculo . "'
		,'" . $chkproducto . "'
		,'" . $chkmarca . "'
		,'" . $chkpago . "'
		,'" . $chkviaTransporte . "'
		,'" . $chktrafico . "'
		,'" . $chkpaisOrigen . "'
		,'" . $chkpuertoEmbarque . "'
		,'" . $chkpuertoDescarga . "'
		,'" . $chkaduana . "'
		,'" . $chkestado . "'
		,'" . $nombreArchivo . "'
		,'" . $usuarioId . "'";        
        		//echo $query;
		
                        
        $stmt = mssql_init("vehiculosExtracciones");

		mssql_bind($stmt, '@mercadoId', $mercadoId, SQLVARCHAR);
        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);        
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        //mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);        
        mssql_bind($stmt, '@importadorRut', $importadorRut, SQLVARCHAR);
        mssql_bind($stmt, '@importadorNombre', $importadorNombre, SQLVARCHAR);
        mssql_bind($stmt, '@naviera', $naviera, SQLVARCHAR);
        mssql_bind($stmt, '@nave', $nave, SQLVARCHAR);
        mssql_bind($stmt, '@tipoNave', $tipoNave, SQLVARCHAR);
        mssql_bind($stmt, '@tipoVehiculo', $tipoVehiculo, SQLVARCHAR);
        mssql_bind($stmt, '@producto', $producto, SQLVARCHAR);
        mssql_bind($stmt, '@marca', $marca, SQLVARCHAR);
        mssql_bind($stmt, '@pago', $pago, SQLVARCHAR);
        mssql_bind($stmt, '@viaTransporte', $viaTransporte, SQLVARCHAR);        
        mssql_bind($stmt, '@trafico', $trafico, SQLVARCHAR);
        mssql_bind($stmt, '@paisOrigen', $paisOrigen, SQLVARCHAR);
        mssql_bind($stmt, '@puertoEmbarque', $puertoEmbarque, SQLVARCHAR);
        mssql_bind($stmt, '@puertoDescarga', $puertoDescarga, SQLVARCHAR);
        mssql_bind($stmt, '@aduana', $aduana, SQLVARCHAR);
		mssql_bind($stmt, '@estado', $estado, SQLVARCHAR);
                
        mssql_bind($stmt, '@chkimportadorRut', $chkimportadorRut, SQLVARCHAR);
        mssql_bind($stmt, '@chkimportadorNombre', $chkimportadorNombre, SQLVARCHAR);
        mssql_bind($stmt, '@chknaviera', $chknaviera, SQLVARCHAR);
        mssql_bind($stmt, '@chknave', $chknave, SQLVARCHAR);
        mssql_bind($stmt, '@chktipoNave', $chktipoNave, SQLVARCHAR);
        mssql_bind($stmt, '@chktipoVehiculo', $chktipoVehiculo, SQLVARCHAR);
        mssql_bind($stmt, '@chkproducto', $chkproducto, SQLVARCHAR);
        mssql_bind($stmt, '@chkmarca', $chkmarca, SQLVARCHAR);
        mssql_bind($stmt, '@chkpago', $chkpago, SQLVARCHAR);
        mssql_bind($stmt, '@chkviaTransporte', $chkviaTransporte, SQLVARCHAR);        
        mssql_bind($stmt, '@chktrafico', $chktrafico, SQLVARCHAR);
        mssql_bind($stmt, '@chkpaisOrigen', $chkpaisOrigen, SQLVARCHAR);
        mssql_bind($stmt, '@chkpuertoEmbarque', $chkpuertoEmbarque, SQLVARCHAR);
        mssql_bind($stmt, '@chkpuertoDescarga', $chkpuertoDescarga, SQLVARCHAR);
        mssql_bind($stmt, '@chkaduana', $chkaduana, SQLVARCHAR);
		mssql_bind($stmt, '@chkestado', $chkestado, SQLVARCHAR);
        
        mssql_bind($stmt, '@nombreArchivo', $nombreArchivo, SQLVARCHAR);                                    
        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);
        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    function vehiculosExtraccionesArchivos($usuarioId, $moduloId) {
        $query = "Select * From Extraccion_Reportes (nolock) Where usuarioId = '" . $usuarioId . "' and modulo = '" . $moduloId . "' Order by fecha_reporte desc";
        //echo $query;
        $result = sql_db::sql_query($query);
        sql_db::sql_close();        
        return $result;
    }
    
    function vehiculosExtraccionesSeleccionarArchivo($usuarioId, $moduloId, $arhivoId) {
        $query = "Select nombre_archivo From Extraccion_Reportes (nolock) Where usuarioId = '" . $usuarioId . "' and modulo = '" . $moduloId . "' and id_archivo = '" . $arhivoId . "'";                
        $result = sql_db::sql_query($query);
        $row = sql_db::sql_fetch_assoc($result);
        sql_db::sql_close();        
        return $row;
    }
    
    function vehiculosExtraccionesEliminarArchivo($usuarioId, $moduloId, $arhivoId) {        
        $query = "Delete e From Extraccion_Reportes e Where usuarioId = '" . $usuarioId . "' and modulo = '" . $moduloId . "' and id_archivo = '" . $arhivoId . "'";        
        sql_db::sql_query($query);        
        sql_db::sql_close();        
        return true;
    }
    
    
}

?>


