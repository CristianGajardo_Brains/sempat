


function buscarExtracciones(){
    
    var nombreArchivo = $("#inputNombreArchivo").val();
    var carpeta = $("#carpeta").val();

    var fechaDesde = $("#fechaDesde").val();
    var fechaHasta = $("#fechaHasta").val();
    
    
    if(fechaDesde == "" && fechaHasta == ""){
	
		if($("#btnPeru").prop('checked')){
			fechasDefecto("VEHICULOS_PE");
		}
		else{
			fechasDefecto("VEHICULOS");
		}
        
        fechaDesde = $("#fechaDesde").val();
        fechaHasta = $("#fechaHasta").val();
    }
    
    if(validarFormulario()){

        $("#divResultado").html("");  

        $("#imgCargando").css("display","block");
        //$("#divPanelFiltros").css("display","none");            
        //$("#divMensaje").css("display","none");

        var rut = $("#rut").val();
        var importador = $("#importador").val(); 
        var naviera = $("#naviera").val();
        var nave = $("#nave").val();
        var tipoNave = $("#tipoNave").val();                
        var tipo = $("#tipo").val();
        var producto = $("#producto").val();
        var marca = $("#marca").val();
        var pago = $("#pago").val();
        var via = $("#via").val();        
        var trafico = $("#trafico").val();
        var paisOrigen = $("#paisOrigen").val();
        var puertoEmbarque = $("#puertoEmbarque").val();
        var puertoDescarga = $("#puertoDescarga").val();
        var aduana = $("#aduana").val();
		var estado = $("#estado").val();
	/*
		alert("rut"+rut);
		alert("importador"+importador);
		alert("naviera"+naviera);
		alert("nave"+nave);
		alert("tipoNave"+tipoNave);
		alert("tipo"+tipo);
		alert("producto"+producto);
		alert("marca"+marca);
		alert("pago"+pago);
		alert("via"+via);
		alert("trafico"+trafico);
		alert("paisOrigen"+paisOrigen);
		alert("puertoEmbarque"+puertoEmbarque);
		alert("puertoDescarga"+puertoDescarga);
		alert("aduana"+aduana);
		alert("estado"+estado);
*/

        var chkrut = $("#chkrut").attr('checked');
        var chkimportador = $("#chkimportador").attr('checked');
        var chknaviera = $("#chknaviera").attr('checked');
        var chknave = $("#chknave").attr('checked');
        var chktipoNave = $("#chktipoNave").attr('checked');
        
        var chktipo = $("#chktipo").attr('checked');
        var chkproducto = $("#chkproducto").attr('checked');
        var chkmarca = $("#chkmarca").attr('checked');
        var chkpago = $("#chkpago").attr('checked');
        var chkvia = $("#chkvia").attr('checked');
        
        var chktrafico = $("#chktrafico").attr('checked');
        var chkpaisOrigen = $("#chkpaisOrigen").attr('checked');
        var chkpuertoEmbarque = $("#chkpuertoEmbarque").attr('checked');
        var chkpuertoDescarga = $("#chkpuertoDescarga").attr('checked');
        var chkaduana = $("#chkaduana").attr('checked');
		var chkestado = $("#chkestado").attr('checked');
                
        var tipoDato = "";
        
        if($("#btnToneladas").prop('checked')){
            tipoDato = tipoDato + "toneladas," 
        }        
        
        if($("#btnCantidad").prop('checked')){
            tipoDato = tipoDato + "cantidad," 
        } 
		
		if($("#btnFletes").prop('checked')){
            tipoDato = tipoDato + "fletes," 
        } 
		
		var mercadoId = "0";
        
        if($("#btnChile").prop('checked')){
            mercadoId = mercadoId + ",1";
        }        
        
        if($("#btnPeru").prop('checked')){
            mercadoId = mercadoId + ",2";
        }
		
		if(mercadoId == "0"){
			mercadoId = "1,2";
		}
		
		

        var etapa = $("input[name=btnEtapa]:checked").val();
                
        $.ajax({
            type: "POST",
            url: "vehiculosExtraccionesData.php",
            data: { mercadoId: mercadoId, fechaDesde: fechaDesde, fechaHasta: fechaHasta, rut: rut, importador: importador, nave: nave, tipoNave: tipoNave, naviera: naviera,
                tipo: tipo, producto: producto, marca: marca, pago: pago, via: via,trafico: trafico, paisOrigen: paisOrigen, puertoEmbarque: puertoEmbarque, puertoDescarga: puertoDescarga, aduana: aduana, estado: estado,
                chkrut: chkrut, chkimportador: chkimportador, chknave: chknave, chktipoNave: chktipoNave, chknaviera: chknaviera,
                chktipo: chktipo, chkproducto: chkproducto, chkmarca: chkmarca, chkpago: chkpago, chkvia: chkvia,
                chktrafico: chktrafico, chkpaisOrigen: chkpaisOrigen, chkpuertoEmbarque: chkpuertoEmbarque, chkpuertoDescarga: chkpuertoDescarga, chkaduana: chkaduana,
				chkestado: chkestado,nombreArchivo: nombreArchivo,tipoDato:tipoDato, carpeta: carpeta}
            })
            .done(function( msg ) {
                buscarExtraccionesArchivos($("#moduloId").val());
                $("#imgCargando").css("display","none");                                           

        }); 

    }                                                                    

}


function validarFormulario(){

    var mensaje = "";

    var nombreArchivo = $("#inputNombreArchivo").val();
    var fechaDesde = $("#fechaDesde").val();
    var fechaHasta = $("#fechaHasta").val();                                 
            
    if(nombreArchivo == ""){
        mensaje = "You must enter a name for the file.<br>";
    }
    
    if(fechaDesde == ""){
        mensaje += "You haven't entered the start date.<br>";
    }
    
    if(fechaHasta == ""){
        mensaje += "You haven't entered the end date.<br>";
    }
    
    if(fechaDesde != "" && fechaHasta != "" && validarFechaMenor(fechaDesde, fechaHasta) == false){
        mensaje += "The initial date may not exceed the final date.<br>";
    }
    
    if(fechaDesde != "" && fechaHasta != "" && fechaDiferenciasDias(fechaDesde, fechaHasta) > 365){
        mensaje += "The difference between the dates can not be longer than one year.<br>";
    }
            
    if(mensaje != ""){
        $("#divMensaje").html(mensaje);        
        return false;
    }
    else{
        $("#divMensaje").html("");        
        return true;
    }

}
    
    
    