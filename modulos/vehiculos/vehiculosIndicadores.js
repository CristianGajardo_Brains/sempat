var count = 0;


function filtros(){

    var filtros = '';
    var mensaje = '';
    count = 0;

    filtros += filtrosValores($("#rut"), "Importer Id");
    filtros += filtrosValores($("#importador"), "Importer");
    filtros += filtrosValores($("#naviera"), "Shipping Company");
    filtros += filtrosValores($("#nave"), "Vessel");
    filtros += filtrosValores($("#tipoNave"), "Vessel Type");
    
    filtros += filtrosValores($("#tipo"), "Vehicle Type");
    filtros += filtrosValores($("#producto"), "Product");
    filtros += filtrosValores($("#marca"), "Brand");
    filtros += filtrosValores($("#pago"), "Pay");
    filtros += filtrosValores($("#via"), "Mode of Transport");
    
    filtros += filtrosValores($("#trafico"), "Traffic");
    filtros += filtrosValores($("#paisOrigen"), "Country Of Origin");
    filtros += filtrosValores($("#puertoEmbarque"), "Port of Loading");
    filtros += filtrosValores($("#puertoDescarga"), "Port of Discharge");
    filtros += filtrosValores($("#aduana"), "Customs");
    filtros += filtrosValores($("#estado"), "New/Used");
        
        
    if(count > 0){
        mensaje = "<font color='#3D85FE' size='2'>Filters:</font><br>"                
    }
                    
    if(count > 4){
        mensaje += "<div style='height:60px; overflow-y: scroll'>" + filtros + "</div>";
        
    }
    else{
        mensaje += "<div style='height:60px'>" + filtros + "</div>";
    }

    return mensaje;

}


function buscarIndicadorNivel1(t){

    var nivel1 = $("#selectNivel1").val();
    var nivel2 = $("#selectNivel2").val();
    var nivel3 = $("#selectNivel3").val();
    var nivel4 = $("#selectNivel4").val();
    var nivel5 = $("#selectNivel5").val();
	
	var mercadoId = $("input[name=mercado]:checked").val();
	
	if(mercadoId != "1"){
		fechasDefecto("VEHICULOS_PE"); 
	}
	else{
		fechasDefecto("VEHICULOS"); 
	}

    
    var fechaDesde = $("#fechaDesde").val();
    var fechaHasta = $("#fechaHasta").val();


    if(nivel1 != ""){

        $("#divResultado").html("");  
        $("#tablaNivelesDesc").html("");   

        $("#imgCargando").css("display","block");
        $("#divPanelFiltros").css("display","none");
        $("#divOpciones").css("display","none");
        $("#divGraficos").css("display","none");
        $("#divGrafico").css("display","none");
        $("#divGraficoA").css("display","none");
        $("#divGraficoB").css("display","none");
        $("#divMensaje").css("display","none");
        
        var rut = $("#rut").val();
        var importador = $("#importador").val();        
        var naviera = $("#naviera").val();
        var nave = $("#nave").val();
        var tipoNave = $("#tipoNave").val();        
        
        var tipo = $("#tipo").val();
        var producto = $("#producto").val();
        var marca = $("#marca").val();
        var pago = $("#pago").val();
        var via = $("#via").val();
        
        
        if(t == 0){
            via = 1;
        }
        
        
        var trafico = $("#trafico").val();
        var paisOrigen = $("#paisOrigen").val();
        var puertoEmbarque = $("#puertoEmbarque").val();
        var puertoDescarga = $("#puertoDescarga").val();
        var aduana = $("#aduana").val();
		var estado = $("#estado").val();
                       
        var nivel1Titulo = $("#selectNivel1 option:selected").text();

        //$('#selectMkt').css("display", "none");

        $('#selectGrafico').val("line");
        $('#selectselectGrafico').html("Lines");

        $('#selectselectMkt').val("Acumulado del año");
        $('#selectMkt').val("mkt1");

        $('.tdMkt').css("display", "none");

        var tipoDato = $("input[name=tipoDato]:checked").val();			
		var mercadoId = $("input[name=mercado]:checked").val();
        
        var subtitulo = "";
		
		if(mercadoId == "1"){

			switch(tipoDato){
				case 'cantidad':
					$('#tituloGrafico').val("Chile Imports in Units");
					$('#tituloGraficoEjeY').val("Units");                
					$('.tdTipo').css("display", "");
					subtitulo = "in Units";
					break;
				
				case 'toneladas':
					$('#tituloGrafico').val("Chile Imports in Tons");
					$('#tituloGraficoEjeY').val("Tons");
					$('.tdTipo').css("display", "none");
					subtitulo = "in Tons";
					break;
					
				case 'fletes':
					$('#tituloGrafico').val("Chile Imports in Freight USD");
					$('#tituloGraficoEjeY').val("Freight USD");
					$('.tdTipo').css("display", "none");
					subtitulo = "in Freight USD";
					break;
			}
			
			$('#tdResumen').html("<center><font color='#3D85FE' size='3'>Chile Imports</font><br>" + subtitulo + "</center>" + filtros());
		}
		else if(mercadoId == "2"){

			switch(tipoDato){
				case 'cantidad':
					$('#tituloGrafico').val("Peru Imports in Units");
					$('#tituloGraficoEjeY').val("Units");                
					$('.tdTipo').css("display", "");
					subtitulo = "in Units";
					break;
				
				case 'toneladas':
					$('#tituloGrafico').val("Peru Imports in Tons");
					$('#tituloGraficoEjeY').val("Tons");
					$('.tdTipo').css("display", "none");
					subtitulo = "in Tons";
					break;
			}
			
			$('#tdResumen').html("<center><font color='#3D85FE' size='3'>Peru Imports</font><br>" + subtitulo + "</center>" + filtros());
		}		
		else {

			switch(tipoDato){
				case 'cantidad':
					$('#tituloGrafico').val("Chile - Peru Imports in Units");
					$('#tituloGraficoEjeY').val("Units");                
					$('.tdTipo').css("display", "");
					subtitulo = "in Units";
					break;
				
				case 'toneladas':
					$('#tituloGrafico').val("Chile - Peru Imports in Tons");
					$('#tituloGraficoEjeY').val("Tons");
					$('.tdTipo').css("display", "none");
					subtitulo = "in Tons";
					break;
			}
			
			$('#tdResumen').html("<center><font color='#3D85FE' size='3'>Chile - Peru Imports</font><br>" + subtitulo + "</center>" + filtros());
		}
                              
                                                
        $.ajax({
            type: "POST",
            url: "vehiculosIndicadoresNivel1.php",
            //async: false,
            data: { mercadoId: mercadoId, fechaDesde: fechaDesde, fechaHasta: fechaHasta, 
                rut: rut, importador: importador, nave: nave, tipoNave: tipoNave, naviera: naviera, 
                tipo: tipo, producto: producto, marca: marca, pago: pago, via: via, estado: estado,
                trafico: trafico, paisOrigen: paisOrigen, puertoEmbarque: puertoEmbarque, puertoDescarga: puertoDescarga, aduana: aduana,                 
                tipoDato:tipoDato, nivel1: nivel1, nivel2: nivel2, nivel3: nivel3, nivel4: nivel4, nivel5: nivel5, nivel1Titulo: nivel1Titulo}
            })
            .done(function( msg ) {

                if(msg.trim() != ""){

                    $('#divResultado').html(msg);

                    $("#divOpciones").css("display","block");
                    $("#tBody tr").each(function (index) {

                        if(index < 3){

                            $(this).children("td").each(function (index2, td) {

                                if(index2 == 0){

                                    $(td).children("input").each(function (index3, input) {
                                        input.click();
                                    });

                                    $(td).children("span ").each(function (index3, span) {
                                        $(span).removeClass('checkboxC');
                                        $(span).addClass('checkboxChecked');
                                    });

                                }                                                
                            });
                        }                    
                    });


                    var nivelesDesc = "";                

                    if(nivel1 != "")
                        nivelesDesc = nivelesDesc + "<tr><td><div class='nivel1'></div></td><td class='desc'>Level 1: " + nivel1Titulo + "</td></tr>"; 

                    if(nivel2.trim() != ""){
                        var nivel2Titulo = $("#selectNivel2 option:selected").text();
                        nivelesDesc = nivelesDesc + "<tr><td><div class='nivel2'></div></td><td class='desc'>Level 2: " + nivel2Titulo + "</td></tr>"; 
                    }

                    if(nivel3 != ""){
                        var nivel3Titulo = $("#selectNivel3 option:selected").text();
                        nivelesDesc = nivelesDesc + "<tr><td><div class='nivel3'></div></td><td class='desc'>Level 3: " + nivel3Titulo + "</td></tr>"; 
                    }

                    if(nivel4 != ""){
                        var nivel4Titulo = $("#selectNivel4 option:selected").text();
                        nivelesDesc = nivelesDesc + "<tr><td><div class='nivel4'></div></td><td class='desc'>Level 4: " + nivel4Titulo + "</td></tr>"; 
                    }

                    if(nivel5 != ""){
                        var nivel5Titulo = $("#selectNivel5 option:selected").text();
                        nivelesDesc = nivelesDesc + "<tr><td><div class='nivel5'></div></td><td class='desc'>Level 5: " + nivel5Titulo + "</td></tr>"; 
                    }
                    
//                    if(t != 0){
//                        Custom.initChkbox();
//                    }

                    Custom.initChkbox();
                    
                    if(t == 0){
                        
                        var url = '../../complementos/diccionarios?nivelId=122';
                        var series = [];
                        series[0] = {id: 1, name: 'MARITIME'};

                        $('#token-input-via').remove();
                        $('#via').tokenInput(url, { prePopulate: series,theme: 'facebook', queryParam: 'filtro', minChars: 3, placeHolder: 'Mode of Transport'});

                        $('#token-input-via').css({                
                            color: "#3D85FE"
                        });                    
                        $('#token-input-via').val("Mode of Transport: 1 selected");

                        $('#tdResumen').html("<center><font color='#3D85FE' size='3'>Chile Imports</font><br>" + subtitulo + "</center><font size='2' color='#3D85FE'>Filters:</font><br><div style='height:60px'>Mode of Transport:<font title='MARITIMA'>MARITIME</font><br></div>");
                        
                    }                                                            

                    //var obj = $('#token-input-via').siblings('tester');
                    //obj.html("EJEMPLO");
                    //alert(obj);

                    $("#divOpciones").css("display","block");
                    $("#tablaNivelesDesc").html(nivelesDesc);

                }
                else{
                    $('#divResultado').html("<div style='margin-left:30px'><label style='color:red'>Your search returns no results.</label></div>");
                }

                $("#imgCargando").css("display","none");   

        }); 

    }
    else{
        $("#divMensaje").css("display","block");
    }                                                                  

}


function buscarIndicadorNivel2(obj, fechaDesde, fechaHasta, nivel2, nivel3, nivel4, nivel5, tipoDato, nivel1Id, otros){

    var tr = obj.parentNode;  
    var className = $(tr).hasClass('OK');                

    if(className == false && nivel2 != ""){

        $.ajax({
            type: "POST",
            url: "vehiculosIndicadoresNivel2.php",
            async: false,
            data: {fechaDesde: fechaDesde, fechaHasta: fechaHasta, tipoDato:tipoDato, nivel2: nivel2, nivel3: nivel3, nivel4: nivel4, nivel5: nivel5, trId: $(tr).attr('id'), nivel1Id: nivel1Id, otros: otros}
            })
            .done(function(msg ) {
                $(tr).addClass('OK');            
                $(msg).insertAfter(tr);
        });                
    }
    else{                        
        desplegarNivel($(tr).attr('id'), 2);
    }
}


function buscarIndicadorNivel3(obj, fechaDesde, fechaHasta, nivel3, nivel4, nivel5, tipoDato, nivel1Id, nivel2Id, otros){

    var tr = obj.parentNode;  
    var className = $(tr).hasClass('OK');                

    if(className == false && nivel3 != ""){

        $.ajax({
            type: "POST",
            url: "vehiculosIndicadoresNivel3.php",
            async: false,
            data: {fechaDesde: fechaDesde, fechaHasta: fechaHasta, tipoDato:tipoDato, nivel3: nivel3, nivel4: nivel4, nivel5: nivel5, trId: $(tr).attr('id'), nivel1Id: nivel1Id, nivel2Id: nivel2Id, otros: otros}
            })
            .done(function(msg ) {
                $(tr).addClass('OK');            
                $(msg).insertAfter(tr);
        });                
    }
    else{                        
        desplegarNivel($(tr).attr('id'), 3);
    }
}


function buscarIndicadorNivel4(obj, fechaDesde, fechaHasta, nivel4, nivel5, tipoDato, nivel1Id, nivel2Id, nivel3Id, otros){

    var tr = obj.parentNode;  
    var className = $(tr).hasClass('OK');                

    if(className == false && nivel4 != ""){

        $.ajax({
            type: "POST",
            url: "vehiculosIndicadoresNivel4.php",
            async: false,
            data: {fechaDesde: fechaDesde, fechaHasta: fechaHasta, tipoDato:tipoDato, nivel4: nivel4, nivel5: nivel5, trId: $(tr).attr('id'), nivel1Id: nivel1Id, nivel2Id: nivel2Id, nivel3Id: nivel3Id, otros: otros}
            })
            .done(function(msg ) {
                $(tr).addClass('OK');            
                $(msg).insertAfter(tr);
        });                
    }
    else{                        
        desplegarNivel($(tr).attr('id'), 4);
    }
}


function buscarIndicadorNivel5(obj, fechaDesde, fechaHasta, nivel5, tipoDato, nivel1Id, nivel2Id, nivel3Id, nivel4Id, otros){

    var tr = obj.parentNode;  
    var className = $(tr).hasClass('OK');                

    if(className == false && nivel5 != ""){

        $.ajax({
            type: "POST",
            url: "vehiculosIndicadoresNivel5.php",
            async: false,
            data: {fechaDesde: fechaDesde, fechaHasta: fechaHasta, tipoDato:tipoDato, nivel5: nivel5, trId: $(tr).attr('id'), nivel1Id: nivel1Id, nivel2Id: nivel2Id, nivel3Id: nivel3Id, nivel4Id: nivel4Id, otros: otros}
            })
            .done(function(msg ) {
                $(tr).addClass('OK');            
                $(msg).insertAfter(tr);                                                            
        });                
    }
    else{                        
        desplegarNivel($(tr).attr('id'), 5);
    }
}
    
            
    
    
    
    
    