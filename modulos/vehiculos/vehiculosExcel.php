<?php

session_start();
/** Incluir la libreria PHPExcel */
include ('../../librerias/PHPExcel/Classes/PHPExcel.php');
include('../../librerias/conexion.php');

// Crea un nuevo objeto PHPExcel
$objPHPExcel = new PHPExcel();

$arreglo = explode("/", $_POST['datos']);
$usuario = $arreglo[0];
$tituloTabla = $arreglo[1]; 
$idNivel1 = $arreglo[2];
$idNivel2 = $arreglo[3];
$tipoDato = $arreglo[4];
$fDesde = $arreglo[5];
$fHasta = $arreglo[6];
$idMercado = $arreglo[7]; 

$tituloNombre = $_POST['tituloNombre'];
$nivel1Nombre = $_POST['nivel1Nombre'];
$nivel2Nombre = $_POST['nivel2Nombre'];


if($idNivel1 > 0 && $idNivel2 > 0){
    $contadorNiveles = 2;
}else if($idNivel1 > 0){
    $contadorNiveles = 1;
}
$filtroMeses = explode("/", $_POST['meses']);

$letras = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");

function cellColor($cells,$color){
        global $objPHPExcel;
        $objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()
        ->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array('rgb' => $color)
        ));
}

//se dibujan las columnas del nivel 1 y nivel 2 segun corresponda
if($contadorNiveles == 2){
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
    $numLetra = 4; 
}
if($contadorNiveles == 1){
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20); 
    $numLetra = 3;
}

//Se dibuja la cabezera de la tabla con todas las columnas respectivas(se controla el numero de columnas de acuerdo a los meses seleccionados)
$totalColumnas = count($filtroMeses)+3+$contadorNiveles;
    
for ($i = 0; $i <= $totalColumnas ; $i++) {         
    
    if($i < count($filtroMeses)){
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras[$i+$numLetra].'4', $filtroMeses[$i]);                
    }else{
        if($i == count($filtroMeses)){
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras[$i+$numLetra].'4', 'Total');
        }
        if($i == count($filtroMeses)+1){
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras[$i+$numLetra].'4', 'Total Anterior');
        }
        if($i == count($filtroMeses)+2){
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras[$i+$numLetra].'4', 'Diferencia');
            $ultimaLetra = $letras[$i+$numLetra];
        }
    }
    
    $objPHPExcel->setActiveSheetIndex(0)->getStyle($letras[$i+$numLetra].'4')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,));
}
     
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('C1:'.$ultimaLetra.'1');
$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(38);

//Se escribe el titulo de la tabla, combinando todas las columnas y luego se centra
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C2', $tituloNombre)->mergeCells('C2:'.$ultimaLetra.'2');
$objPHPExcel->setActiveSheetIndex(0)->getStyle('C2')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,));
$objPHPExcel->getActiveSheet()->getStyle("C2")->getFont()->setBold(true)->setName('ARIAL')->setSize(10)->getColor()->setRGB('FFFFFF');

$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C3', $tituloTabla)->mergeCells('C3:'.$ultimaLetra.'3');
$objPHPExcel->setActiveSheetIndex(0)->getStyle('C3')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,));

//Se establece el color/estilo del titulo de la tabla y los titulos de sus todas sus columnas
cellColor('C2', '72A8ED');
cellColor('C3', '72A8ED');
cellColor('C4:'.$ultimaLetra.'4', '72A8ED');
    
// Establecer propiedades
$objPHPExcel->getProperties();
$objPHPExcel->getProperties()->setCreator("Cattivo");
$objPHPExcel->getProperties()->setLastModifiedBy("Cattivo");
$objPHPExcel->getProperties()->setTitle("Documento Excel");
$objPHPExcel->getProperties()->setSubject("Documento Excel");
$objPHPExcel->getProperties()->setDescription("Archivo Excel desde SEMPAT.");
$objPHPExcel->getProperties()->setKeywords("Excel Office 2007 openxml php");
$objPHPExcel->getProperties()->setCategory("Excel");

$gdImage = imagecreatefromjpeg('../../imagenes/sempatJPG.jpg');
$objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
$objDrawing->setName('Sempat Logo');$objDrawing->setDescription('Sempat Logo');
$objDrawing->setImageResource($gdImage);
$objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_JPEG);
$objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
$objDrawing->setWidth(50);
$objDrawing->setHeight(50);
$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
$objDrawing-> setCoordinates('C1');
//$objPHPExcel->setActiveSheetIndex(0)->getStyle('C1')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,));
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');$objWriter->save(str_replace('.php', '.xlsx', __FILE__));

//PROPIEDADES DEL LA CELDA
$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
$objPHPExcel->getDefaultStyle()->getFont()->setSize(8);

//alto de la fila numero 1..
//$objPHPExcel->getActiveSheet()->getRowDimension('4')->setRowHeight(20);

//Se estable el ancho de todas las columnas de la tabla 
for($i = $numLetra;$i < count($filtroMeses)+3+$numLetra;$i++){
    if($i > count($filtroMeses)+3){
        $objPHPExcel->getActiveSheet()->getColumnDimension($letras[$i])->setWidth(15);
    }else{
        $objPHPExcel->getActiveSheet()->getColumnDimension($letras[$i])->setWidth(10);
    }
}

$objPHPExcel->getActiveSheet()->getStyle("C4:".$ultimaLetra."4")->getFont()->setBold(true)->setName('ARIAL')->setSize(8)->getColor()->setRGB('FFFFFF');

// Agregar Informacion
$objPHPExcel->setActiveSheetIndex(0);
if($contadorNiveles == 2){
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue("C4", $nivel1Nombre);
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue("D4", $nivel2Nombre);
}
if($contadorNiveles == 1){
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue("C4", $nivel1Nombre);    
}

$borders = array(
'borders' => array(
'allborders' => array(
'style' => PHPExcel_Style_Border::BORDER_THIN,
'color' => array('argb' => '72A8ED'),
)
),
);

$objPHPExcel->getActiveSheet()->getStyle('C4:'.$ultimaLetra.'4')->applyFromArray($borders);

$y=4;

$stmt = mssql_init("bunkeringReporteExcel");  
mssql_bind($stmt, '@fechaDesde', $fDesde, SQLVARCHAR);
mssql_bind($stmt, '@fechaHasta', $fHasta, SQLVARCHAR);
mssql_bind($stmt, '@var1', $idNivel1, SQLVARCHAR);
mssql_bind($stmt, '@var2', $idNivel2, SQLVARCHAR);
mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
mssql_bind($stmt, '@idMercado', $idMercado, SQLINT4);
mssql_bind($stmt, '@usuarioId', $usuario, SQLINT4);
$tablaDatos = mssql_execute($stmt);

$niveles =  array();
$nivel1 = 0;
$nivel2 = 0;

$totalFilas = mssql_num_rows($tablaDatos);

while($row = mssql_fetch_array($tablaDatos)){
    $y++;
    //BORDE DE LA CELDA
    $objPHPExcel->setActiveSheetIndex(0)->getStyle('C'.$y.":".$ultimaLetra.$y)->applyFromArray($borders);

    //Se verifica si el dato corresponde a primer nivel y se le asigna un color distintivo
    if($row['nivel1'] == 1){               
        $objPHPExcel->getActiveSheet()->getStyle('C'.$y.":".$ultimaLetra.$y)->getFont()->setBold(true)->setName('ARIAL')->setSize(8)->getColor()->setRGB('000000');
        
        if($row['campo1'] == "TOTAL" && $totalFilas = $y - 4){
            cellColor('C'.$y.":".$ultimaLetra.$y, 'B8CFEB');    //color nivel 1 TOTAL
        }
        else{
            cellColor('C'.$y.":".$ultimaLetra.$y, 'FFFFFF');    //color nivel 1
        }
        
        $nivel1++;
        $nivel2 = 0;
        
        $filasNivel1[$y-5] = $y; 
    }
    else{
        cellColor('C'.$y.":".$ultimaLetra.$y, 'F5F5F5');    //color nivel 2
        $nivel2++;
        $niveles[$nivel1] = $nivel2;
    }

    //Se verifica los niveles que fueron seleccionados para mostrar las columnas correspondientes
    if($contadorNiveles == 2){
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue("C".$y, mb_convert_encoding($row['campo1'],"UTF-8", "ISO-8859-1"));
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue("D".$y, mb_convert_encoding($row['campo2'],"UTF-8", "ISO-8859-1"));
        $next = 4;
    }else{
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue("C".$y, mb_convert_encoding($row['campo1'],"UTF-8", "ISO-8859-1"));
        $next = 3;
    }
    
    //Se muestran los datos de todos los meses seleccionados por cada fila  
    for($j = 0;$j < count($filtroMeses);$j++){
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras[$next].$y, number_format($row[$j+7], 0, '.',''));      
        $next++;
    }
    
//    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras[$next].$y, '=sum('.$letras[$numLetra].''.$y.':'.$letras[$next].''.$y.')');
    if($row['nivel1'] == 1){
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras[$next].$y, number_format($row['total1'], 0, '.',''));
    }else{
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras[$next].$y, number_format($row['total2'], 0, '.',''));
    }
    
    //Se verifica si el valor es 0
    $totalAnteriorVerificado = $row['totalAnterior'];
    if($totalAnteriorVerificado > 0){
        $totalAnteriorVerificado = $row['totalAnterior'];
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras[$next+1].$y, number_format($totalAnteriorVerificado, 0, '.',''));
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras[$next+2].$y, '=(100*'.$letras[$next].''.$y.'/'.$letras[$next+1].''.$y.'-100)');
    }else{
        $totalAnteriorVerificado = 0;
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras[$next+1].$y, number_format($totalAnteriorVerificado, 0, '.',''));
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras[$next+2].$y, '100.000');
    }            
    
    //Se obtiene el resultado de la formula realizada, se corta el resultado para mostrarlo con dos decimales y el % 
    $valorCeldaDiferencia = $objPHPExcel->getActiveSheet()->getCell($letras[$next+2].$y)->getCalculatedValue();
    $separacion = explode('.', $valorCeldaDiferencia);
    if(strlen($separacion[1]) > 0){
        $decimales = substr($separacion[1], 0,2);
    }else{
        $decimales = '00';
    }
    if($valorCeldaDiferencia < 0){
        $objPHPExcel->getActiveSheet()->getStyle($letras[$next+2].$y)->getFont()->setBold(true)->setName('ARIAL')->setSize(8)->getColor()->setRGB('FF0000');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras[$next+2].$y, $separacion[0].'.'.$decimales.' %');
    }else{
        $objPHPExcel->getActiveSheet()->getStyle($letras[$next+2].$y)->getFont()->setBold(true)->setName('ARIAL')->setSize(8)->getColor()->setRGB('0000FF');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letras[$next+2].$y, $separacion[0].'.'.$decimales.' %');
    }
    //Se alinean los valores hacia la derecha de la columna Diferencia
    $objPHPExcel->setActiveSheetIndex(0)->getStyle($letras[$next+2].$y)->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
}

$indiceInicial = 0;
$indiceFinal = 4;

//Se realiza el agrupamiento de los datos de la tabla 
for ($i = 1; $i <= count($niveles); $i++) {
        
    $indiceInicial = $indiceFinal + 1;                  
    $indiceFinal = $indiceInicial + $niveles[$i];
        
    for ($x = $indiceInicial + 1; $x <= $indiceFinal; $x++) {
            $objPHPExcel->getActiveSheet()->getRowDimension($x)->setOutlineLevel(1);
            $objPHPExcel->getActiveSheet()->getRowDimension($x)->setVisible(false);
    }
    
    $objPHPExcel->getActiveSheet()->getRowDimension($indiceFinal)->setCollapsed(true);
}

// Renombrar Hoja
$objPHPExcel->getActiveSheet()->setTitle('Reporte Sempat');
 
// Establecer la hoja activa, para que cuando se abra el documento se muestre primero.
$objPHPExcel->setActiveSheetIndex(0);
 
// Se modifican los encabezados del HTTP para indicar que se envia un archivo de Excel.
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="ReporteBunkering.xlsx"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;

?>