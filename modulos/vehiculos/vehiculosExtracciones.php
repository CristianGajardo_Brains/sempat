<?
session_start();

setlocale(LC_TIME, "spanish");
$fechaActual = htmlentities(strftime("%A %d de %B del %Y"));
$fechaActual = ucfirst($fechaActual);


$usuarioLog = $_SESSION["SEMPAT_usuarioNombre"];
$usuarioId = $_SESSION["SEMPAT_usuarioId"];
$clienteId = $_SESSION["SEMPAT_clienteId"];


if ($_SESSION['SEMPAT_usuarioId'] == "") {
    echo "<script>window.location='../../index.php';</script>";
}

include("../../default.php");
$plantilla->setPath('../../plantillas/');
$plantilla->setTemplate("header");
$plantilla->setVars(array("USUARIO" => " $usuarioLog ",
    "FECHA" => "$fechaActual"
));
echo $plantilla->show();

require('../../clases/sempat.class.php');
$objSem = new sempat();

require('vehiculos.class.php');
$objVeh = new vehiculos();

$moduloId = 37;
$menuId = 88;
$carpeta = "Vehiculos";


$htmlMenu = $objSem->menuRetornarHtml($usuarioId, $menuId);


$moduloFiltros = $objSem->admBuscarFiltros($clienteId, $moduloId);
$scriptAC = "";


//$consultaFechas = $objSem->manifiestoFechasDefecto($clienteId);
//$fechaDesde = "01-10-2012";
//$fechaHasta = "30-09-2013";


// <editor-fold>
if ($moduloFiltros) {

    $filttrosNivel = "<option value='1'>Etapa</option>";
    $filtrosHtml = "<table id='tablaFiltrosCampos' style='margin-left:40px'>";
    $numFiltros = 0;   
    $arreglo = array();
    
    $tablaCamposSalida = "<tr>";

    while ($filtros= mssql_fetch_array($moduloFiltros)) {
                
        if($filtros["filtroVisible"] == "1"){                        
                      
            if(trim($filtros["filtroEtiqueta"]) != ""){
                
                $filttrosNivel .= "<option value='" . $filtros["nivelId"] . "'>" . htmlentities($filtros["filtroEtiquetaIngles"]) . "</option>";            
                
                //$filtrosHtml .= "<input id=" . $filtros["filtroHtml"] . " type='text' class='inputFiltro' placeHolder='" . htmlentities($filtros["filtroEtiqueta"]) . "'/><input type='text' class='inputFiltroRight' readonly disabled/>";
                //$filtrosHtml .= "<input id=" . $filtros["filtroHtml"] . " type='text'/>";
                
                $arreglo[$filtros["filtroOrden"]][$filtros["filtroColumna"] - 1] = "<td><input id=\""  . "chk" .  $filtros["filtroHtml"] . "\" class=\"styled\" type=\"checkbox\"></td><td width='340px'><input type='text' id=" . $filtros["filtroHtml"] . "></input></td>";                               
                
                
                if(($numFiltros % 6) == 0){
                    $tablaCamposSalida .= "</tr><tr>";
                }
                
                $tablaCamposSalida .= "";
                
                $scriptAC .= "$('#"  . $filtros["filtroHtml"] . "').tokenInput('../../complementos/diccionarios?nivelId=" . htmlentities($filtros["nivelId"]) . "', {
                                            theme: 'facebook',
                                            queryParam: 'filtro',
                                            minChars: 3,
                                            placeHolder: '" . mb_convert_encoding($filtros["filtroEtiquetaIngles"], "UTF-8", "ISO-8859-1")  . "'
                                        });\n\n";                                                            
                
                $numFiltros++;
                
            }
            else{
                $arreglo[$filtros["filtroOrden"]][$filtros["filtroColumna"] - 1] = "<td></td><td></td>";
            }
        }
        else{
            
            if(trim($filtros["filtroHtml"]) != ""){               
                $filtrosHtml .= "<input id=" . $filtros["filtroHtml"] . " type='text' style='display:none'/><input type='text' readonly disabled style='display:none'/>";
            }            
        }
        
//        if(($numFiltros % 5) == 0){
//            $filtrosHtml.= "<br><br>";
//        }
            }
    
    $tablaCamposSalida .= "</tr>";
            
    for($i = 1; $i <= count($arreglo); $i++){
        $filtrosHtml = $filtrosHtml . "<tr>";
        
        if($arreglo[$i][0] != ""){
            $filtrosHtml = $filtrosHtml . $arreglo[$i][0];
        }
        else{
            $filtrosHtml = $filtrosHtml . "<td></td><td></td>";
        }

        if($arreglo[$i][1] != ""){
            $filtrosHtml = $filtrosHtml . $arreglo[$i][1];
        }
        else{
            $filtrosHtml = $filtrosHtml . "<td></td><td></td>";
        }

        if($arreglo[$i][2] != ""){
            $filtrosHtml = $filtrosHtml . $arreglo[$i][2];
        }
        else{
            $filtrosHtml = $filtrosHtml . "<td></td><td></td>";
        }

        $filtrosHtml = $filtrosHtml . "</tr>";
    }   
    
    $filtrosHtml .= "</table>";
    

}
// </editor-fold>

if($clienteId == "18"){
	$listaArchivos = $objVeh->leerArchivosNyk();	
}
else{
	$listaArchivos = $objVeh->leerArchivos();	
}

?>

<script type="text/javascript" src="vehiculosExtracciones.js"></script>

<script type="text/javascript">


    $(document).ready(function(){
        
        <? echo $scriptAC; ?>    
                
        $("#fechaDesde").mask("99-99-9999");
        $("#fechaHasta").mask("99-99-9999");
        
        $('#fechaDesde').bind('blur', function() {
            validarFecha(this);
        });
        
        $('#fechaHasta').bind('blur', function() {
            validarFecha(this);
        });
        
                                
        $("#imgCargando").css("display","");
        buscarExtraccionesArchivos(<? echo $moduloId; ?>);                                                                            

    });


</script>
            
    <? echo $htmlMenu; ?>

    <div class="divMinHeight">


        <div id="divGuardarReporte" class="divPopUp">

            <label>SAVED REPORT</label>

            <br><br>

            <input type="text" class="inputFiltro" placeHolder="Nombre"/>
            <input type="text" class="inputFiltroRight" readonly disabled/>

            <br><br>

            <div style="text-align: left; width:200px; margin-left: 50px; min-height: 160px">

                Level 1: Port agent  <br>
                Level 2: Port <br>

            </div>

            <div style="margin-left:10px">
                <input type="button" class="btnPlomo" value="CANCEL" onclick="cerrarReporte()"/>                            
                <input type="button" class="btnAzul" value="SAVE"/>  
            </div>

        </div>

<? if($clienteId == "4" || $clienteId == "9" || $usuarioId == "157" || $usuarioId == "156"){ ?>
		<div id="divReportesGuardados" class="divPopUp">

			<label>REPORTS</label>

			<br><br>

			<div style="text-align: left; width:370px; margin-left: 20px; height: 220px; overflow: scroll">
			
				<br/>

				<table>
					<?php echo $listaArchivos; ?>			
					
				</table>

			</div>

			<div style="margin-left:10px">
				<input type="button" class="btnPlomo" value="CLOSE" onclick="cerrarReporte2()"/>                                        
			</div>

		</div>
	<? } ?>

        <div class="divSubContenedor">

            <div class="divPanelBusqueda">
              <img src="../../imagenes/lupa.png" alt="" title=""><label>SEARCH PANEL</label>
            </div>

            <div id="divPanelFiltros" class="divPanelFiltros">
                <div class="divPanelFiltros1" style="height: 200px" >
                    
                    <input id="moduloId" type="hidden" value="<? echo $moduloId; ?>">
                    <input id="carpeta" type="hidden" value="<? echo $carpeta; ?>"/>

                    <table>
                        <tr>
                            <td valign="top" width="500px">
                                <label>DATES</label>

                                <br><br>

                                <input id="fechaDesde" type="text" class="inputFecha" placeHolder="Start" value=""/><input id="btnFechaDesde" type="button" class="inputFechaBtn" />
                                <input id="fechaDesdeDef" type="hidden"  value="<? echo $fechaDesde; ?>"/>


                                <script type="text/javascript">//<![CDATA[
                                    var myCal1 = Calendar.setup({
                                        checkRange: false,
                                        inputField: "fechaDesde",
                                        trigger: "btnFechaDesde",
                                        //onSelect: function () { this.hide(); validarFechasReportes($('fechaDesde'), 'fechaDesde', 'fechaHasta');},
                                        dateFormat: "%d-%m-%Y"
                                    });
                                    myCal1.setLanguage('es');
                                    //]]></script>

                                <input id="fechaHasta" type="text" class="inputFecha" placeHolder="End" value="" style="margin-left:60px"/><input id="btnFechaHasta" type="button" class="inputFechaBtn" />
                                <input id="fechaHastaDef" type="hidden"  value="<? echo $fechaHasta; ?>"/>

                                <script type="text/javascript">//<![CDATA[
                                    var myCal1 = Calendar.setup({
                                        checkRange: false,
                                        inputField: "fechaHasta",
                                        trigger: "btnFechaHasta",
                                        //onSelect: function () { this.hide(); validarFechasReportes($('fechaDesde'), 'fechaDesde', 'fechaHasta');},
                                        dateFormat: "%d-%m-%Y"
                                    });
                                    myCal1.setLanguage('es');
                                    //]]></script>
                                
                                
                                <div id="divMensajeFechas">

                                </div>

                            </td>                            
                            <td valign="top">
                                <label class="niveles">DATA</label>
                                <table>
                                    <tr>
                                        <td>
                                            <input id="btnCantidad" type="checkbox" class="checkGrafico styled" value="cantidad">
                                        </td>
                                        <td valign="bottom">
                                            Units
                                        </td>                                    
                                    </tr>
                                    <tr>
                                        <td>
                                            <input id="btnToneladas" type="checkbox" class="checkGrafico styled" value="toneladas">
                                        </td>
                                        <td>
                                            Tons
                                        </td>                                    
                                    </tr>
									<? if($clienteId == 4){ ?>								
									<tr>
										<td>
											<input id="btnFletes" type="checkbox" class="checkGrafico styled" value="fletes">
										</td>
										<td>
											Freight USD
										</td>
									</tr>								
								<? }?>									
                                </table>
                            </td>
							
							<? if($clienteId == "4" || $usuarioId == "103"){ ?>
							 <td valign="top" style='padding-left: 120px'>
								<label class="niveles">MARKET</label>
								<table>
									<tr>
										<td>
											<input id="btnChile" type="checkbox" class="checkGrafico styled" value="1">
										</td>
										<td valign="bottom">
											Chile
										</td>                                    
									</tr>
									<tr>
										<td>
											<input id="btnPeru" type="checkbox" class="checkGrafico styled" value="2">
										</td>
										<td>
											Peru
										</td>                                    
									</tr>   		
								</table>
							</td>
							<? if($clienteId == "4" || $clienteId == "9"){ ?>
							<? //if($clienteId == "4" && $usuarioId == "103"){ ?>
							
							
							<td valign="top" style='padding-left: 250px'>
								<img src="imagenes/pdf.png" alt="" style="width: 25px;height: 25px;cursor: pointer" onclick="abrirReporte2()" title="Reports">							
							</td>
							
							<? } ?>
											
						<?}
						else{ ?>
							<td>
								<input id="btnChile" type="radio" name="mercado" value="1" checked style='display:none'/>
							</td>
						<?} if($usuarioId == "157" || $usuarioId == "156"){ ?>
							<? //if($clienteId == "4" && $usuarioId == "103"){ ?>
							
							
							<td valign="top" style='padding-left: 440px'>
								<img src="imagenes/pdf.png" alt="" style="width: 25px;height: 25px;cursor: pointer" onclick="abrirReporte2()" title="Reports">
							</td>
							
							<? } ?>
							
							
                        </tr>
                    </table>    
                    
                    <br><br>

                    <div style="width:100%; margin-top: 0px; margin-bottom: 8px;">                            
                        <label>FILE NAME</label>                                                    
                    </div>

                    <input id="inputNombreArchivo" type="text" class="inputFiltro">
                    <input type="text" class="inputFiltroRight" disabled readonly>



                    <div style="width:100%; margin-top: 40px; margin-bottom: 8px;">                            
                        <label>FILTERS AND FIELDS OF DEPARTURE</label>                                                    
                    </div>

                    <table class="tablaSalidas">
                        <tr>
                            <td>
                                <input id="chkSeleccionarTodo" type="checkbox" class="styled" onClick="seleccionarTodoExtracciones()">
                            </td>                                        
                            <td>
                                All
                            </td>
                        </tr>
                    </table>


                    <? echo $filtrosHtml; ?>

                    <div style="clear: both"></div>

                    
                    <div style="float:right;">

                    </div>                                                        

                </div>    

                <div style="clear: both"></div>

                <div class="divPanelFiltros2" style="height:auto">

                    <div style="width:96%;">

                        <br>
                        
                        <div id="divMensaje" style="margin-left:0px; display:block; width:450px">
                            
                        </div>

                        <div style="">
                            <input type="button" class="btnLogin" value="CREATE REPORT" onClick="buscarExtracciones()"/>
<!--                                    <input type="button" class="btnLogin" value="LIMPIAR"/>  -->
                        </div>   

                        <br>

                    </div>   

                </div> 

                <div class="divPanelFiltros2" style="display:none">

                    <div style="width:100%; margin-bottom: 8px;">                            
                        <label>STORED REPORTS</label>                            
                    </div>   

                    <input type="text" class="inputFecha" placeHolder="Nombre"/><input type="button" class="inputLupaBtn" onclick="abrirReporte2()"/>

                    <input type="button" class="btnLogin" value="SHOW"/>

                </div>                                        

            </div>

<!--                    <div class="divPanelBusquedaBottom" onClick="mostrarFiltrosBusqueda()">

            </div>-->

        </div>

        <br style="clear:both"/> 

        <div style="height:70px">

            <center id="imgCargando" style="display:none">
                <img src="../../imagenes/cargando.gif" height="42" width="42"><br/><i>Loading</i>
            </center>

        </div>




        <div class="divPanelBusqueda" style="margin: 0px auto;">
            <label>REPORTS</label>
        </div>

        <br>

        <table class="tablaExtracciones" cellspacing="0" cellspadding="0">
            <thead>
                <tr>            
                    <th class="thCenter" style="width:250px;">
                        <div class="divThLeft"></div>          
                        <div class="divThCenter">FILE NAME</div>                
                    </th>                                                
                    <th class="thCenter" style="width:150px;">                
                        <div class="divThCenter">DATE</div>                
                    </th>  
                    <th class="thCenter" colspan="2">                
                        <div class="divThCenter"></div>
                        <div class="divThRight"></div>          
                    </th>  
                </tr> 
            </thead>
            <tbody id="tBody">

            </tbody>

        </table>

    </div>

    <br>
            
<?
$plantilla->setPath('../../plantillas/');
$plantilla->setTemplate("footer");
echo $plantilla->show();
?>