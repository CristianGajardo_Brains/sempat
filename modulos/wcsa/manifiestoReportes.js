
var count = 0;


function filtros(){

    var filtros = '';
    var mensaje = '';
    count = 0;

    filtros += filtrosValores($("#manPuertoOrigen"), "Puerto Origen");
    filtros += filtrosValores($("#manPuertoEmbarque"), "Puerto Embarque");
    filtros += filtrosValores($("#manPuertoDescarga"), "Puerto Descarga");
    filtros += filtrosValores($("#manPuertoDestino"), "Puerto Destino");
    filtros += filtrosValores($("#manPaisOrigen"), "País Origen");
    filtros += filtrosValores($("#manPaisEmbarque"), "País Embarque");
    filtros += filtrosValores($("#manPaisDescarga"), "País Descarga");
    filtros += filtrosValores($("#manPaisDestino"), "País Destino");
    filtros += filtrosValores($("#manTrafico"), "Tráfico/Zona");
    filtros += filtrosValores($("#manNaviera"), "Cia. Naviera");
    filtros += filtrosValores($("#manAgencia"), "Agente Portuario");
    filtros += filtrosValores($("#manAgenteDoc"), "Agente Comercial");
    filtros += filtrosValores($("#manNave"), "Nave");
    filtros += filtrosValores($("#manTipoNave"), "Tipo Nave");
    filtros += filtrosValores($("#manShipper"), "Embarcador");
    filtros += filtrosValores($("#manConsignee"), "Consignatario");
    filtros += filtrosValores($("#manGranFamilia"), "Gran Familia");
    filtros += filtrosValores($("#manFamilia"), "Familia");
    filtros += filtrosValores($("#manSubFamilia"), "Sub Familia");
    filtros += filtrosValores($("#manCommodity"), "Mercadería");
    filtros += filtrosValores($("#manTipoCarga"), "Tipo de Carga");
    filtros += filtrosValores($("#manTipoServicio"), "Tipo de Servicio");
    filtros += filtrosValores($("#manTipoEmbalaje"), "Tipo de Embalaje");
    filtros += filtrosValores($("#manStatusContainer"), "Estado Contenedor");

    if(count > 0){
        mensaje = "<font color='#3D85FE' size='2'>Filtros:</font><br>"                
    }
                    
    if(count > 4){
        mensaje += "<div style='height:60px; overflow-y: scroll'>" + filtros + "</div>";
        
    }
    else{
        mensaje += "<div style='height:60px'>" + filtros + "</div>";
    }

    return  mensaje;


}


function buscarNivel1(){

    var nivel1 = $("#selectNivel1").val();
    var nivel2 = $("#selectNivel2").val();
    var nivel3 = $("#selectNivel3").val();
    var nivel4 = $("#selectNivel4").val();
    var nivel5 = $("#selectNivel5").val();

    var mercado = "";

    var chile = $("input[id=btnChile]:checked").val();
    var peru = $("input[id=btnPeru]:checked").val();
    var ecuador = $("input[id=btnEcuador]:checked").val();
    var colombia = $("input[id=btnColombia]:checked").val();


    mercado = chile + "," + peru + "," + ecuador + "," + colombia;                                                         
    mercado = replaceAll(mercado, ",undefined",""); 
    mercado = replaceAll(mercado, "undefined",""); 

    if(chile == undefined){
        mercado = mercado.substring(1, mercado.length);
    }


    var fechaDesde = $("#fechaDesde").val();
    var fechaHasta = $("#fechaHasta").val();


    if(fechaDesde == "" && fechaHasta == ""){

        fechasDefecto(mercado); 
        fechaDesde = $("#fechaDesde").val();
        fechaHasta = $("#fechaHasta").val();

        //fechaDesde = $("#fechaDesdeDef").val();
        //fechaHasta = $("#fechaHastaDef").val();
        //$("#fechaDesde").val(fechaDesde);
        //$("#fechaHasta").val(fechaHasta);

    }


    if(validarFormulario()){

        $("#divResultado").html("");  
        $("#tablaNivelesDesc").html("");   

        $("#imgCargando").css("display","block");
        $("#divPanelFiltros").css("display","none");
        $("#divOpciones").css("display","none");
        $("#divGraficos").css("display","none");
        $("#divGrafico").css("display","none");
        $("#divGraficoA").css("display","none");
        $("#divGraficoB").css("display","none");
        $("#divMensaje").css("display","none");

        var puertoOrigen = $("#manPuertoOrigen").val();
        var puertoEmbarque = $("#manPuertoEmbarque").val();
        var puertoDescarga = $("#manPuertoDescarga").val();
        var puertoDestino = $("#manPuertoDestino").val();
        var paisOrigen = $("#manPaisOrigen").val();
        var paisEmbarque = $("#manPaisEmbarque").val();
        var paisDescarga = $("#manPaisDescarga").val();
        var paisDestino = $("#manPaisDestino").val();
        var trafico = $("#manTrafico").val();
        var naviera = $("#manNaviera").val();
        var agencia = $("#manAgencia").val();
        var agenteDoc = $("#manAgenteDoc").val();
        var nave = $("#manNave").val();
        var tipoNave = $("#manTipoNave").val();
        var shipper = $("#manShipper").val();
        var consignee = $("#manConsignee").val();
        var granFamilia = $("#manGranFamilia").val();
        var familia = $("#manFamilia").val();
        var subFamilia = $("#manSubFamilia").val();
        var commodity = $("#manCommodity").val();
        var tipoCarga = $("#manTipoCarga").val();
        var tipoServicio = $("#manTipoServicio").val();
        var tipoEmbalaje = $("#manTipoEmbalaje").val();
        var statusContainer = $("#manStatusContainer").val();

        var nivel1Titulo = $("#selectNivel1 option:selected").text();

        $('#selectTipoReporte').val("");
        $('#selectselectTipoReporte').html("Ambos");

        $('#selectGraficoReporte').val("line");
        $('#selectselectGrafico').html("Líneas");

        var tipoDato = $("input[name=tipoDato]:checked").val();

        switch(tipoDato){
            case 'contenedores':
                $('#tituloGrafico2').val("N° de Contenedores (C20 + C40)");
                $('#tituloGraficoEjeY').val("N° de Contenedores (C20 + C40)");                
                $('.tdTipo').css("display", "");
                break;

            case 'teus':
                $('#tituloGrafico2').val("N° de TEUs");
                $('#tituloGraficoEjeY').val("N° de TEUs");
                $('.tdTipo').css("display", "none");
                break;
				
			case 'feus':
                $('#tituloGrafico2').val("N° de FEUs");
                $('#tituloGraficoEjeY').val("N° de FEUs");
                $('.tdTipo').css("display", "none");
                break;

            case 'toneladas':
                $('#tituloGrafico2').val("N° de Toneladas");
                $('#tituloGraficoEjeY').val("N° de Toneladas");
                $('.tdTipo').css("display", "none");
                break;
        }


        var etapa = $("input[name=btnEtapa]:checked").val();
        var contenedor = $("input[name=btnContenedor]:checked").val();
        var dryReefer = $("input[name=btnSecaRefrigerada]:checked").val();

        var tituloGrafico2 = "";

        switch(etapa){
            case undefined:
                tituloGrafico2 = 'Exportaciones e Importaciones'
                break;

            case '1':
                tituloGrafico2 = 'Exportaciones'
                break;

            case '2':
                tituloGrafico2 = 'Importaciones'
                break;                
        }



        if(mercado == '' || mercado == '1,2,3,4'){
            tituloGrafico2 += ' (WCSA)'
        }
        else{                
            tituloGrafico2 += ' (' + replaceAll(mercado, ",", " - ") + ')'
            tituloGrafico2 = tituloGrafico2.replace("1","Chile"); 
            tituloGrafico2 = tituloGrafico2.replace("2","Perú"); 
            tituloGrafico2 = tituloGrafico2.replace("3","Ecuador"); 
            tituloGrafico2 = tituloGrafico2.replace("4","Colombia"); 
			tituloGrafico2 = tituloGrafico2.replace("8","Perú");
            tituloGrafico2 = tituloGrafico2.replace("9","Ecuador");
        }

        $('#tituloGrafico').val(tituloGrafico2);

        $('#tdResumen').html("<center><font color='#3D85FE' size='3'>" + tituloGrafico2 + "</font><br>" + $('#tituloGrafico2').val() + "</center>" + filtros());


        $.ajax({
            type: "POST",
            url: "manifiestoReporteNivel1.php",
            //async: false,
            data: { fechaDesde: fechaDesde, fechaHasta: fechaHasta, puertoOrigen: puertoOrigen, puertoEmbarque: puertoEmbarque, puertoDescarga: puertoDescarga, puertoDestino: 
                    puertoDestino, paisOrigen: paisOrigen, paisEmbarque: paisEmbarque, paisDescarga: paisDescarga, paisDestino: paisDestino, trafico: trafico, naviera: naviera, 
                agencia: agencia, agenteDoc: agenteDoc, nave: nave, tipoNave: tipoNave, shipper: shipper, consignee: consignee, granFamilia: granFamilia, familia: familia, 
                subFamilia: subFamilia, commodity: commodity, tipoCarga: tipoCarga, tipoServicio: tipoServicio, tipoEmbalaje: tipoEmbalaje, statusContainer: statusContainer, 
                tipoDato:tipoDato, nivel1: nivel1, nivel2: nivel2, nivel3: nivel3, nivel4: nivel4, nivel5: nivel5, nivel1Titulo: nivel1Titulo, mercado: mercado, etapa: etapa, contenedor: contenedor, dryReefer: dryReefer}
            })
            .done(function( msg ) {
                
                if(msg.trim() != ""){
                                    
                    $("#divResultado").html(msg);                 
                    $("#divOpciones").css("display","block");

                    $("#tBody tr").each(function (index) {

                        if(index < 3){

                            $(this).children("td").each(function (index2, td) {

                                if(index2 == 0){

                                    $(td).children("input").each(function (index3, input) {
                                        input.click();
                                    });

                                    $(td).children("span ").each(function (index3, span) {
                                        $(span).removeClass('checkboxC');
                                        $(span).addClass('checkboxChecked');
                                    });

                                }                                                
                            });
                        }                    
                    });


                    var nivelesDesc = "";                

                    if(nivel1 != "")
                        nivelesDesc = nivelesDesc + "<tr><td><div class='nivel1'></div></td><td class='desc'>Nivel 1: " + nivel1Titulo + "</td></tr>"; 

                    if(nivel2.trim() != ""){
                        var nivel2Titulo = $("#selectNivel2 option:selected").text();
                        nivelesDesc = nivelesDesc + "<tr><td><div class='nivel2'></div></td><td class='desc'>Nivel 2: " + nivel2Titulo + "</td></tr>"; 
                    }

                    if(nivel3 != ""){
                        var nivel3Titulo = $("#selectNivel3 option:selected").text();
                        nivelesDesc = nivelesDesc + "<tr><td><div class='nivel3'></div></td><td class='desc'>Nivel 3: " + nivel3Titulo + "</td></tr>"; 
                    }

                    if(nivel4 != ""){
                        var nivel4Titulo = $("#selectNivel4 option:selected").text();
                        nivelesDesc = nivelesDesc + "<tr><td><div class='nivel4'></div></td><td class='desc'>Nivel 4: " + nivel4Titulo + "</td></tr>"; 
                    }

                    if(nivel5 != ""){
                        var nivel5Titulo = $("#selectNivel5 option:selected").text();
                        nivelesDesc = nivelesDesc + "<tr><td><div class='nivel5'></div></td><td class='desc'>Nivel 5: " + nivel5Titulo + "</td></tr>"; 
                    }                   

                    Custom.initChkbox();
                    
                    $("#divOpciones").css("display","block");
                    $("#tablaNivelesDesc").html(nivelesDesc);
                }
                else{
                    $('#divResultado').html("<div style='margin-left:30px'><label style='color:red'>Búsqueda sin resultados.</label></div>");
                }

                $("#imgCargando").css("display","none");

        }); 

    }    

}


function buscarNivel2(obj, fechaDesde, fechaHasta, nivel2, nivel3, nivel4, nivel5, tipoDato, nivel1Id, otros){

    var tr = obj.parentNode;  
    var className = $(tr).hasClass('OK');                

    if(className == false && nivel2 != ""){

        $.ajax({                
            type: "POST",
            url: "manifiestoReporteNivel2.php",
            async: false,
            data: {fechaDesde: fechaDesde, fechaHasta: fechaHasta, tipoDato:tipoDato, nivel2: nivel2, nivel3: nivel3, nivel4: nivel4, nivel5: nivel5, trId: $(tr).attr('id'), nivel1Id: nivel1Id, otros: otros}
            })
            .done(function(msg ) {
                $(tr).addClass('OK');            
                $(msg).insertAfter(tr);
        });                
    }
    else{                        
        desplegarNivel($(tr).attr('id'), 2);
    }
}


function buscarNivel3(obj, fechaDesde, fechaHasta, nivel3, nivel4, nivel5, tipoDato, nivel1Id, nivel2Id, otros){

    var tr = obj.parentNode;  
    var className = $(tr).hasClass('OK');                

    if(className == false && nivel3 != ""){

        $.ajax({
            type: "POST",
            url: "manifiestoReporteNivel3.php",
            async: false,
            data: {fechaDesde: fechaDesde, fechaHasta: fechaHasta, tipoDato:tipoDato, nivel3: nivel3, nivel4: nivel4, nivel5: nivel5, trId: $(tr).attr('id'), nivel1Id: nivel1Id, nivel2Id: nivel2Id, otros: otros}
            })
            .done(function(msg ) {
                $(tr).addClass('OK');            
                $(msg).insertAfter(tr);
        });                
    }
    else{                        
        desplegarNivel($(tr).attr('id'), 3);
    }
}


function buscarNivel4(obj, fechaDesde, fechaHasta, nivel4, nivel5, tipoDato, nivel1Id, nivel2Id, nivel3Id, otros){

    var tr = obj.parentNode;  
    var className = $(tr).hasClass('OK');                

    if(className == false && nivel4 != ""){

        $.ajax({
            type: "POST",
            url: "manifiestoReporteNivel4.php",
            async: false,
            data: {fechaDesde: fechaDesde, fechaHasta: fechaHasta, tipoDato:tipoDato, nivel4: nivel4, nivel5: nivel5, trId: $(tr).attr('id'), nivel1Id: nivel1Id, nivel2Id: nivel2Id, nivel3Id: nivel3Id, otros: otros}
            })
            .done(function(msg ) {
                $(tr).addClass('OK');            
                $(msg).insertAfter(tr);
        });                
    }
    else{                        
        desplegarNivel($(tr).attr('id'), 4);
    }
}


function buscarNivel5(obj, fechaDesde, fechaHasta, nivel5, tipoDato, nivel1Id, nivel2Id, nivel3Id, nivel4Id, otros){

    var tr = obj.parentNode;  
    var className = $(tr).hasClass('OK');                

    if(className == false && nivel5 != ""){

        $.ajax({
            type: "POST",
            url: "manifiestoReporteNivel5.php",
            async: false,
            data: {fechaDesde: fechaDesde, fechaHasta: fechaHasta, tipoDato:tipoDato, nivel5: nivel5, trId: $(tr).attr('id'), nivel1Id: nivel1Id, nivel2Id: nivel2Id, nivel3Id: nivel3Id, nivel4Id: nivel4Id, otros: otros}
            })
            .done(function(msg ) {
                $(tr).addClass('OK');            
                $(msg).insertAfter(tr);                                                            
        });                
    }
    else{                        
        desplegarNivel($(tr).attr('id'), 5);
    }
}

    
function validarFormulario(){

    var mensaje = "";

    var nivel1 = $("#selectNivel1").val();
    var fechaDesde = $("#fechaDesde").val();
    var fechaHasta = $("#fechaHasta").val();
            
    if(nivel1 == ""){
        mensaje = "Debe seleccionar el primer nivel.<br>";
    }
    
    if(fechaDesde == ""){
        mensaje += "No ha ingresado la fecha inicial.<br>";
    }
    
    if(fechaHasta == ""){
        mensaje += "No ha ingresado la fecha final.<br>";
    }
    
    if(fechaDesde != "" && fechaHasta != "" && validarFechaMenor(fechaDesde, fechaHasta) == false){
        mensaje += "La fecha incial no puede ser mayor a la fecha final.<br>";
    }
    
    if(fechaDesde != "" && fechaHasta != "" && fechaDiferenciasDias(fechaDesde, fechaHasta) > 365){
        mensaje += "La diferencia entre las fechas no puede ser superior a un año.<br>";
    }
            
    if(mensaje != ""){
        $("#divMensaje").html(mensaje);
        $("#divMensaje").css("display","block");
        return false;
    }
    else{
        $("#divMensaje").html("");
        $("#divMensaje").css("display","none");
        return true;
    }

}
    
    
    