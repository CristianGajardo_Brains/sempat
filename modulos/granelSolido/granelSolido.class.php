<?php

class granelSolido {
    
    
    function ultimaFechaManifiesto($clienteId) {
        $query = "Exec seleccionarFechasLimites '" . $clienteId . "','M'";
        $result = sql_db::sql_query($query);
        sql_db::sql_close();
        $row = sql_db::sql_fetch_assoc($result);
        return $row;
    }
    
    
    function manifiestoFechasDefecto($clienteId) {
        $query = "Exec seleccionarFechasDefecto '" . $clienteId . "','M'";
        $result = sql_db::sql_query($query);
        sql_db::sql_close();
        $row = sql_db::sql_fetch_assoc($result);
        return $row;
    }
    
    
    //INDICADORES
    
    function indicadoresCabeceraDatos($fecha) {
        $query = "Exec manifiestoIndicadoresCabecera '" . $fecha . "'";        
        //echo $query;
        $result = sql_db::sql_query($query);
        $row = sql_db::sql_fetch_assoc($result);
        sql_db::sql_close();
        return $row;
    }
    
    
    function indicadoresCabeceraHtml($valorNivel1, $fecha){
        
        $objSem = new granelSolido();
        $resultado = $objSem->indicadoresCabeceraDatos($fecha);
        
        $titulo1 = "ENERO";
        $titulo2 = "";
        $titulo3 = "";
        $titulo2b = "";
        $titulo3b = "";
        
        if(strtoupper($resultado["mmActual"]) != "ENERO"){
            $titulo1 = "ENERO - " . strtoupper($resultado["mmActual"]);
        }

        if($resultado["mmAnterior"] != "" && $resultado["mmActual"] != ""){
            $titulo2 = $resultado["mmAnterior"] . " - " . $resultado["mmActual"];
        }
        
        if($resultado["mmActual"] != ""){
            $titulo3 = $resultado["mmActual"];
        }
        
        if($resultado["mmActual"] == "DICIEMBRE"){
            $titulo2b = $resultado["yyyyActual"];
        }
        else{
            if($resultado["yyyyAnterior"] != ""){
                $titulo2b = $resultado["yyyyAnterior"] . " - " . ($resultado["yyyyActual"]);
            }            
        }
        
        if($resultado["mmActual"] == "DICIEMBRE") {
            $titulo3b = $resultado["yyyyAnterior"];            
        }
        else {            
            if($resultado["yyyyAnterior"] != ""){
                $titulo3b = ($resultado["yyyyAnterior"] - 1) . " - " . ($resultado["yyyyAnterior"]);                
            }                        
        }
        
        
        $htmlThead = "<tr>
                        <th class=\"thCenter\" colspan=\"6\">
                            <div class=\"divThLeft\"></div>
                            <div class=\"divThCenter\">" . $titulo1 . "</div>
                            <div class=\"divThRight\"></div>          
                        </th>                        
                        <th style=\"min-width: 10px;\"></th>
                        <th class=\"thCenter\" colspan=\"4\">
                            <div class=\"divThLeft\"></div>          
                            <div class=\"divThCenter\">" . $titulo2 . "</div>
                            <div class=\"divThRight\"></div>          
                        </th> 
                        <th style=\"min-width: 10px;\"></th>
                        <th class=\"thCenter\" colspan=\"4\">
                            <div class=\"divThLeft\"></div>          
                            <div class=\"divThCenter\">" . $titulo3 . "</div>
                            <div class=\"divThRight\"></div>          
                        </th> 
                    </tr>   
                    <tr>
                        <td colspan=\"2\" class=\"left\" style=\"width:185px;\">
                                " . mb_convert_encoding(trim($valorNivel1), "UTF-8", "ISO-8859-1") .  "
                        </td>
                        <td>
                            " . $resultado["yyyyActual"] . "
                        </td>
                        <td>
                            " . $resultado["yyyyAnterior"] . "
                        </td>
                        <td>
                            DIF.
                        </td>
                        <td>
                            Mkt. SHARE
                        </td>
                        <td class=\"clear\">

                        </td>
                        <td class=\"left\">
                            " . $titulo2b .  "
                        </td>
                        <td>
                            " . $titulo3b .  "
                        </td>
                        <td>
                            DIF.
                        </td>
                        <td>
                            Mkt. SHARE
                        </td>
                        <td class=\"clear\">

                        </td>
                        <td class=\"left\">
                            " . $resultado["yyyyActual"] . "
                        </td>
                        <td>
                            " . $resultado["yyyyAnterior"] . "
                        </td>
                        <td>
                            DIF.
                        </td>
                        <td>
                            Mkt. SHARE
                        </td>                        
                    </tr>";

        return $htmlThead;       

    }
    
    
    function granelesIndicadoresNivel1($fechaDesde, $fechaHasta, $nivel1, $nivel2, $nivel3, $nivel4, $nivel5, $tipoDato, $clienteId, $mercado, $etapa, $naviera, $agencia, $trafico, 
            $puertoOrigen, $puertoEmbarque, $nave, $puertoDestino, $puertoDescarga, $tipoNave, $tipoServicio, $tipoCarga, $tipoEmbalaje, $paisOrigen, $paisEmbarque,
            $agenteDoc, $paisDestino, $paisDescarga, $statusContainer, $shipper, $consignee, $commodity, $granFamilia, $familia, $subFamilia, $dryReefer, $contenedor, $usuarioId){
    
        
        /*$query = "Exec granelesIndicadoresNivel1 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel1 . "','" . $nivel2 . "','" . $nivel3 . "','" . $nivel4 . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $mercado . "','" . $etapa . "','" . $naviera . "','" . $agencia . "','" . $trafico . "','" . $puertoOrigen . "','" . $puertoEmbarque . "','" . $nave . "','" . $puertoDestino . "','" . 
                $puertoDescarga . "','" . $tipoNave . "','" . $tipoServicio . "','" . $tipoCarga . "','" . $tipoEmbalaje . "','" . $paisOrigen . "','" . $paisEmbarque . "','" . $agenteDoc . "','" . 
                $paisDestino . "','" . $paisDescarga . "','" . $statusContainer . "','" . $shipper . "','" . $consignee . "','" . $commodity . "','" . $granFamilia . "','" . $familia . "','" . 
                $subFamilia . "','" . $dryReefer . "','" .$contenedor . "','" . $usuarioId . "'";        
        echo $query;*/
        
        
        $stmt = mssql_init("granelesIndicadoresNivel1");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);
        mssql_bind($stmt, '@var1', $nivel1, SQLVARCHAR);
        mssql_bind($stmt, '@var2', $nivel2, SQLVARCHAR);
        mssql_bind($stmt, '@var3', $nivel3, SQLVARCHAR);
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@mercado', $mercado, SQLVARCHAR);
        mssql_bind($stmt, '@etapa', $etapa, SQLVARCHAR);
        mssql_bind($stmt, '@naviera', $naviera, SQLVARCHAR);
        mssql_bind($stmt, '@agencia', $agencia, SQLVARCHAR);
        mssql_bind($stmt, '@trafico', $trafico, SQLVARCHAR);
        mssql_bind($stmt, '@puertoOrigen', $puertoOrigen, SQLVARCHAR);
        mssql_bind($stmt, '@puertoEmbarque', $puertoEmbarque, SQLVARCHAR);
        mssql_bind($stmt, '@nave', $nave, SQLVARCHAR);
        mssql_bind($stmt, '@puertoDestino', $puertoDestino, SQLVARCHAR);
        mssql_bind($stmt, '@puertoDescarga', $puertoDescarga, SQLVARCHAR);
        mssql_bind($stmt, '@tipoNave', $tipoNave, SQLVARCHAR);
        mssql_bind($stmt, '@tipoServicio', $tipoServicio, SQLVARCHAR);
        mssql_bind($stmt, '@tipoCarga', $tipoCarga, SQLVARCHAR);
        mssql_bind($stmt, '@tipoEmbalaje', $tipoEmbalaje, SQLVARCHAR);
        mssql_bind($stmt, '@paisOrigen', $paisOrigen, SQLVARCHAR);
        mssql_bind($stmt, '@paisEmbarque', $paisEmbarque, SQLVARCHAR);
        mssql_bind($stmt, '@agenteDoc', $agenteDoc, SQLVARCHAR);
        mssql_bind($stmt, '@paisDestino', $paisDestino, SQLVARCHAR);
        mssql_bind($stmt, '@paisDescarga', $paisDescarga, SQLVARCHAR);
        mssql_bind($stmt, '@statusContainer', $statusContainer, SQLVARCHAR);
        mssql_bind($stmt, '@shipper', $shipper, SQLVARCHAR);
        mssql_bind($stmt, '@consignee', $consignee, SQLVARCHAR);
        mssql_bind($stmt, '@commodity', $commodity, SQLVARCHAR);
        mssql_bind($stmt, '@granFamilia', $granFamilia, SQLVARCHAR);
        mssql_bind($stmt, '@familia', $familia, SQLVARCHAR);
        mssql_bind($stmt, '@subFamilia', $subFamilia, SQLVARCHAR);
        mssql_bind($stmt, '@dryReefer', $dryReefer, SQLVARCHAR);        
        mssql_bind($stmt, '@containerVacios', $contenedor, SQLVARCHAR);                                    
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    function granelesIndicadoresNivel1Grafico($fechaDesde, $fechaHasta, $nivel1, $usuarioId){
            
        /*$query = "Exec granelesIndicadoresNivel1Grafico '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel1 . "','" . $usuarioId . "'";        
        echo $query;*/
               
        $stmt = mssql_init("granelesIndicadoresNivel1Grafico");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);
        mssql_bind($stmt, '@var1', $nivel1, SQLVARCHAR);                                            
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    
    function granelesIndicadoresNivel1Contenedores($fechaDesde, $fechaHasta, $nivel1, $tipoDato, $usuarioId){
    
        
        /*$query = "Exec granelesIndicadoresNivel1Contenedores '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel1 . "','" . $usuarioId . "','" . $tipoDato . "'";        
        echo $query;*/
                
        $stmt = mssql_init("granelesIndicadoresNivel1Contenedores");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);                
        mssql_bind($stmt, '@var1', $nivel1, SQLVARCHAR);        
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    function granelesIndicadoresNivel2($fechaDesde, $fechaHasta, $nivel2, $nivel3, $nivel4, $nivel5, $tipoDato, $clienteId, $nivel1Id, $otros, $usuarioId){
    
        
        /*$query = "Exec granelesIndicadoresNivel2 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel2 . "','" . $nivel3 . "','" . $nivel4 . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $nivel1Id . "','" . $otros . "','" . $usuarioId . "'";                
        echo $query;*/
                
        $stmt = mssql_init("granelesIndicadoresNivel2");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);        
        mssql_bind($stmt, '@var2', $nivel2, SQLVARCHAR);
        mssql_bind($stmt, '@var3', $nivel3, SQLVARCHAR);
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@nivel1Id', $nivel1Id, SQLVARCHAR);
        mssql_bind($stmt, '@otros', $otros, SQLVARCHAR);        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    function granelesIndicadoresNivel3($fechaDesde, $fechaHasta, $nivel3, $nivel4, $nivel5, $tipoDato, $clienteId, $nivel1Id, $nivel2Id, $otros, $usuarioId){
    
        
        /*$query = "Exec granelesIndicadoresNivel3 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel3 . "','" . $nivel4 . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $nivel1Id . "','" . $nivel2Id . "','" . $otros . "','" . $usuarioId . "'";        
        echo $query;*/
                
        $stmt = mssql_init("granelesIndicadoresNivel3");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);                
        mssql_bind($stmt, '@var3', $nivel3, SQLVARCHAR);
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@nivel1Id', $nivel1Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel2Id', $nivel2Id, SQLVARCHAR);
        mssql_bind($stmt, '@otros', $otros, SQLVARCHAR);        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    function granelesIndicadoresNivel4($fechaDesde, $fechaHasta, $nivel4, $nivel5, $tipoDato, $clienteId, $nivel1Id, $nivel2Id, $nivel3Id, $otros, $usuarioId){
            
        /*$query = "Exec granelesIndicadoresNivel4 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel4 . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $nivel1Id . "','" . $nivel2Id . "','" . $nivel3Id . "','" . $otros . "','" . $usuarioId . "'";      
        echo $query;*/
                
        $stmt = mssql_init("granelesIndicadoresNivel4");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);                        
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@nivel1Id', $nivel1Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel2Id', $nivel2Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel3Id', $nivel3Id, SQLVARCHAR);
        mssql_bind($stmt, '@otros', $otros, SQLVARCHAR);        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    function granelesIndicadoresNivel5($fechaDesde, $fechaHasta, $nivel5, $tipoDato, $clienteId, $nivel1Id, $nivel2Id, $nivel3Id, $nivel4Id, $otros, $usuarioId){
            
        /*$query = "Exec granelesIndicadoresNivel5 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $nivel1Id . "','" . $nivel2Id . "','" . $nivel3Id . "','" . $nivel4Id . "','" . $otros . "','" . $usuarioId . "'";        
        echo $query;*/
                
        $stmt = mssql_init("granelesIndicadoresNivel5");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);                                
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@nivel1Id', $nivel1Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel2Id', $nivel2Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel3Id', $nivel3Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel4Id', $nivel4Id, SQLVARCHAR);
        mssql_bind($stmt, '@otros', $otros, SQLVARCHAR);        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    
    //REPORTES
    
    function granelesReporteNivel1($fechaDesde, $fechaHasta, $nivel1, $nivel2, $nivel3, $nivel4, $nivel5, $tipoDato, $clienteId, $mercado, $etapa, $naviera, $agencia, $trafico, 
            $puertoOrigen, $puertoEmbarque, $nave, $puertoDestino, $puertoDescarga, $tipoNave, $tipoServicio, $tipoCarga, $tipoEmbalaje, $paisOrigen, $paisEmbarque,
            $agenteDoc, $paisDestino, $paisDescarga, $statusContainer, $shipper, $consignee, $commodity, $granFamilia, $familia, $subFamilia, $dryReefer, $contenedor, $usuarioId){
    
        
        /*$query = "Exec granelesReporteNivel1 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel1 . "','" . $nivel2 . "','" . $nivel3 . "','" . $nivel4 . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $mercado . "','" . $etapa . "','" . $naviera . "','" . $agencia . "','" . $trafico . "','" . $puertoOrigen . "','" . $puertoEmbarque . "','" . $nave . "','" . $puertoDestino . "','" . 
                $puertoDescarga . "','" . $tipoNave . "','" . $tipoServicio . "','" . $tipoCarga . "','" . $tipoEmbalaje . "','" . $paisOrigen . "','" . $paisEmbarque . "','" . $agenteDoc . "','" . 
                $paisDestino . "','" . $paisDescarga . "','" . $statusContainer . "','" . $shipper . "','" . $consignee . "','" . $commodity . "','" . $granFamilia . "','" . $familia . "','" . 
                $subFamilia . "','" . $dryReefer . "','" .$contenedor . "','" . $usuarioId . "'";        
        echo $query;*/
        
        
        $stmt = mssql_init("granelesReporteNivel1");

        mssql_bind($stmt, '@fechaDesdeA', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHastaA', $fechaHasta, SQLVARCHAR);
        mssql_bind($stmt, '@var1', $nivel1, SQLVARCHAR);
        mssql_bind($stmt, '@var2', $nivel2, SQLVARCHAR);
        mssql_bind($stmt, '@var3', $nivel3, SQLVARCHAR);
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@mercado', $mercado, SQLVARCHAR);
        mssql_bind($stmt, '@etapa', $etapa, SQLVARCHAR);
        mssql_bind($stmt, '@naviera', $naviera, SQLVARCHAR);
        mssql_bind($stmt, '@agencia', $agencia, SQLVARCHAR);
        mssql_bind($stmt, '@trafico', $trafico, SQLVARCHAR);
        mssql_bind($stmt, '@puertoOrigen', $puertoOrigen, SQLVARCHAR);
        mssql_bind($stmt, '@puertoEmbarque', $puertoEmbarque, SQLVARCHAR);
        mssql_bind($stmt, '@nave', $nave, SQLVARCHAR);
        mssql_bind($stmt, '@puertoDestino', $puertoDestino, SQLVARCHAR);
        mssql_bind($stmt, '@puertoDescarga', $puertoDescarga, SQLVARCHAR);
        mssql_bind($stmt, '@tipoNave', $tipoNave, SQLVARCHAR);
        mssql_bind($stmt, '@tipoServicio', $tipoServicio, SQLVARCHAR);
        mssql_bind($stmt, '@tipoCarga', $tipoCarga, SQLVARCHAR);
        mssql_bind($stmt, '@tipoEmbalaje', $tipoEmbalaje, SQLVARCHAR);
        mssql_bind($stmt, '@paisOrigen', $paisOrigen, SQLVARCHAR);
        mssql_bind($stmt, '@paisEmbarque', $paisEmbarque, SQLVARCHAR);
        mssql_bind($stmt, '@agenteDoc', $agenteDoc, SQLVARCHAR);
        mssql_bind($stmt, '@paisDestino', $paisDestino, SQLVARCHAR);
        mssql_bind($stmt, '@paisDescarga', $paisDescarga, SQLVARCHAR);
        mssql_bind($stmt, '@statusContainer', $statusContainer, SQLVARCHAR);
        mssql_bind($stmt, '@shipper', $shipper, SQLVARCHAR);
        mssql_bind($stmt, '@consignee', $consignee, SQLVARCHAR);
        mssql_bind($stmt, '@commodity', $commodity, SQLVARCHAR);
        mssql_bind($stmt, '@granFamilia', $granFamilia, SQLVARCHAR);
        mssql_bind($stmt, '@familia', $familia, SQLVARCHAR);
        mssql_bind($stmt, '@subFamilia', $subFamilia, SQLVARCHAR);
        mssql_bind($stmt, '@dryReefer', $dryReefer, SQLVARCHAR);        
        mssql_bind($stmt, '@containerVacios', $contenedor, SQLVARCHAR);                                    
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    function granelesReporteNivel1Contenedores($fechaDesde, $fechaHasta, $nivel1, $tipoDato, $usuarioId){
    
        
        /*$query = "Exec granelesReporteNivel1Contenedores '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel1 . "','" . $usuarioId . "','" . $tipoDato . "'";        
        echo $query;*/
                
        $stmt = mssql_init("granelesReporteNivel1Contenedores");

        mssql_bind($stmt, '@fechaDesdeA', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHastaA', $fechaHasta, SQLVARCHAR);                
        mssql_bind($stmt, '@var1', $nivel1, SQLVARCHAR);        
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    function granelesReporteNivel2($fechaDesde, $fechaHasta, $nivel2, $nivel3, $nivel4, $nivel5, $tipoDato, $clienteId, $nivel1Id, $otros, $usuarioId){
    
        
        /*$query = "Exec granelesReporteNivel2 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel2 . "','" . $nivel3 . "','" . $nivel4 . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $nivel1Id . "','" . $otros . "','" . $usuarioId . "'";        
        $query;*/
                
        $stmt = mssql_init("granelesReporteNivel2");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);        
        mssql_bind($stmt, '@var2', $nivel2, SQLVARCHAR);
        mssql_bind($stmt, '@var3', $nivel3, SQLVARCHAR);
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@nivel1Id', $nivel1Id, SQLVARCHAR);
        mssql_bind($stmt, '@otros', $otros, SQLVARCHAR);        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    function granelesReporteNivel3($fechaDesde, $fechaHasta, $nivel3, $nivel4, $nivel5, $tipoDato, $clienteId, $nivel1Id, $nivel2Id, $otros, $usuarioId){
    
        
        /*$query = "Exec granelesReporteNivel3 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel3 . "','" . $nivel4 . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $nivel1Id . "','" . $nivel2Id . "','" . $otros . "','" . $usuarioId . "'";        
        echo $query;*/
                
        $stmt = mssql_init("granelesReporteNivel3");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);                
        mssql_bind($stmt, '@var3', $nivel3, SQLVARCHAR);
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@nivel1Id', $nivel1Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel2Id', $nivel2Id, SQLVARCHAR);
        mssql_bind($stmt, '@otros', $otros, SQLVARCHAR);        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    function granelesReporteNivel4($fechaDesde, $fechaHasta, $nivel4, $nivel5, $tipoDato, $clienteId, $nivel1Id, $nivel2Id, $nivel3Id, $otros, $usuarioId){
            
        /*$query = "Exec granelesReporteNivel4 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel4 . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $nivel1Id . "','" . $nivel2Id . "','" . $nivel3Id . "','" . $otros . "','" . $usuarioId . "'";        
        echo $query;*/
                
        $stmt = mssql_init("granelesReporteNivel4");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);                        
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@nivel1Id', $nivel1Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel2Id', $nivel2Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel3Id', $nivel3Id, SQLVARCHAR);
        mssql_bind($stmt, '@otros', $otros, SQLVARCHAR);        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    function granelesReporteNivel5($fechaDesde, $fechaHasta, $nivel5, $tipoDato, $clienteId, $nivel1Id, $nivel2Id, $nivel3Id, $nivel4Id, $otros, $usuarioId){
            
        /*$query = "Exec granelesReporteNivel5 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $nivel1Id . "','" . $nivel2Id . "','" . $nivel3Id . "','" . $nivel4Id . "','" . $otros . "','" . $usuarioId . "'";        
        echo $query;*/
                
        $stmt = mssql_init("granelesReporteNivel5");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);                                
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@nivel1Id', $nivel1Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel2Id', $nivel2Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel3Id', $nivel3Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel4Id', $nivel4Id, SQLVARCHAR);
        mssql_bind($stmt, '@otros', $otros, SQLVARCHAR);        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    //EXTRACCIONES
    
    function granelesExtracciones($fechaDesde, $fechaHasta, $tipoDato, $clienteId, $mercado, $etapa, 
            $naviera, $agencia, $trafico, $puertoOrigen, $puertoEmbarque, $nave, $puertoDestino, $puertoDescarga, $tipoNave, $tipoServicio, $tipoCarga, $tipoEmbalaje, $paisOrigen, 
            $paisEmbarque, $agenteDoc, $paisDestino, $paisDescarga, $statusContainer, $shipper, $consignee, $commodity, $granFamilia, $familia, $subFamilia, $dryReefer, $contenedor, 
            $chknaviera, $chkagencia, $chktrafico, $chkpuertoOrigen, $chkpuertoEmbarque, $chknave, $chkpuertoDestino, $chkpuertoDescarga, $chktipoNave, $chktipoServicio, $chktipoCarga, $chktipoEmbalaje, $chkpaisOrigen, 
            $chkpaisEmbarque, $chkagenteDoc, $chkpaisDestino, $chkpaisDescarga, $chkstatusContainer, $chkshipper, $chkconsignee, $chkcommodity, $chkgranFamilia, $chkfamilia, $chksubFamilia, $chkdryReefer, $chkcontenedor, $nombreArchivo,
            $usuarioId){
    
        
        $query = "Exec granelSolidoExtracciones '" . $fechaDesde . "','" . $fechaHasta . "','" . $tipoDato . "','" . $mercado . "','" . $etapa . "','" . 
                $naviera . "','" . $agencia . "','" . $trafico . "','" . $puertoOrigen . "','" . $puertoEmbarque . "','" . $nave . "','" . $puertoDestino . "','" . 
                $puertoDescarga . "','" . $tipoNave . "','" . $tipoServicio . "','" . $tipoCarga . "','" . $tipoEmbalaje . "','" . $paisOrigen . "','" . $paisEmbarque . "','" . $agenteDoc . "','" . 
                $paisDestino . "','" . $paisDescarga . "','" . $statusContainer . "','" . $shipper . "','" . $consignee . "','" . $commodity . "','" . $granFamilia . "','" . $familia . "','" . 
                $subFamilia . "','" . $dryReefer . "','" .$contenedor . "','" . 
                
                $chknaviera . "','" . $chkagencia . "','" . $chktrafico . "','" . $chkpuertoOrigen . "','" . $chkpuertoEmbarque . "','" . $chknave . "','" . $chkpuertoDestino . "','" . 
                $chkpuertoDescarga . "','" . $chktipoNave . "','" . $chktipoServicio . "','" . $chktipoCarga . "','" . $chktipoEmbalaje . "','" . $chkpaisOrigen . "','" . $chkpaisEmbarque . "','" . $chkagenteDoc . "','" . 
                $chkpaisDestino . "','" . $chkpaisDescarga . "','" . $chkstatusContainer . "','" . $chkshipper . "','" . $chkconsignee . "','" . $chkcommodity . "','" . $chkgranFamilia . "','" . $chkfamilia . "','" . 
                $chksubFamilia . "','" . $chkdryReefer . "','" .$chkcontenedor . "','" . $nombreArchivo . "','" . $usuarioId . "'";        
        echo $query;
        
        
        $stmt = mssql_init("granelSolidoExtracciones");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);        
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        //mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@mercado', $mercado, SQLVARCHAR);
        mssql_bind($stmt, '@etapa', $etapa, SQLVARCHAR);
        mssql_bind($stmt, '@naviera', $naviera, SQLVARCHAR);
        mssql_bind($stmt, '@agencia', $agencia, SQLVARCHAR);
        mssql_bind($stmt, '@trafico', $trafico, SQLVARCHAR);
        mssql_bind($stmt, '@puertoOrigen', $puertoOrigen, SQLVARCHAR);
        mssql_bind($stmt, '@puertoEmbarque', $puertoEmbarque, SQLVARCHAR);
        mssql_bind($stmt, '@nave', $nave, SQLVARCHAR);
        mssql_bind($stmt, '@puertoDestino', $puertoDestino, SQLVARCHAR);
        mssql_bind($stmt, '@puertoDescarga', $puertoDescarga, SQLVARCHAR);
        mssql_bind($stmt, '@tipoNave', $tipoNave, SQLVARCHAR);
        mssql_bind($stmt, '@tipoServicio', $tipoServicio, SQLVARCHAR);
        mssql_bind($stmt, '@tipoCarga', $tipoCarga, SQLVARCHAR);
        mssql_bind($stmt, '@tipoEmbalaje', $tipoEmbalaje, SQLVARCHAR);
        mssql_bind($stmt, '@paisOrigen', $paisOrigen, SQLVARCHAR);
        mssql_bind($stmt, '@paisEmbarque', $paisEmbarque, SQLVARCHAR);
        mssql_bind($stmt, '@agenteDoc', $agenteDoc, SQLVARCHAR);
        mssql_bind($stmt, '@paisDestino', $paisDestino, SQLVARCHAR);
        mssql_bind($stmt, '@paisDescarga', $paisDescarga, SQLVARCHAR);
        mssql_bind($stmt, '@statusContainer', $statusContainer, SQLVARCHAR);
        mssql_bind($stmt, '@shipper', $shipper, SQLVARCHAR);
        mssql_bind($stmt, '@consignee', $consignee, SQLVARCHAR);
        mssql_bind($stmt, '@commodity', $commodity, SQLVARCHAR);
        mssql_bind($stmt, '@granFamilia', $granFamilia, SQLVARCHAR);
        mssql_bind($stmt, '@familia', $familia, SQLVARCHAR);
        mssql_bind($stmt, '@subFamilia', $subFamilia, SQLVARCHAR);
        mssql_bind($stmt, '@dryReefer', $dryReefer, SQLVARCHAR);        
        mssql_bind($stmt, '@containerVacios', $contenedor, SQLVARCHAR);                                    
        
        mssql_bind($stmt, '@chknaviera', $chknaviera, SQLVARCHAR);
        mssql_bind($stmt, '@chkagencia', $chkagencia, SQLVARCHAR);
        mssql_bind($stmt, '@chktrafico', $chktrafico, SQLVARCHAR);
        mssql_bind($stmt, '@chkpuertoOrigen', $chkpuertoOrigen, SQLVARCHAR);
        mssql_bind($stmt, '@chkpuertoEmbarque', $chkpuertoEmbarque, SQLVARCHAR);
        mssql_bind($stmt, '@chknave', $chknave, SQLVARCHAR);
        mssql_bind($stmt, '@chkpuertoDestino', $chkpuertoDestino, SQLVARCHAR);
        mssql_bind($stmt, '@chkpuertoDescarga', $chkpuertoDescarga, SQLVARCHAR);
        mssql_bind($stmt, '@chktipoNave', $chktipoNave, SQLVARCHAR);
        mssql_bind($stmt, '@chktipoServicio', $chktipoServicio, SQLVARCHAR);
        mssql_bind($stmt, '@chktipoCarga', $chktipoCarga, SQLVARCHAR);
        mssql_bind($stmt, '@chktipoEmbalaje', $chktipoEmbalaje, SQLVARCHAR);
        mssql_bind($stmt, '@chkpaisOrigen', $chkpaisOrigen, SQLVARCHAR);
        mssql_bind($stmt, '@chkpaisEmbarque', $chkpaisEmbarque, SQLVARCHAR);
        mssql_bind($stmt, '@chkagenteDoc', $chkagenteDoc, SQLVARCHAR);
        mssql_bind($stmt, '@chkpaisDestino', $chkpaisDestino, SQLVARCHAR);
        mssql_bind($stmt, '@chkpaisDescarga', $chkpaisDescarga, SQLVARCHAR);
        mssql_bind($stmt, '@chkstatusContainer', $chkstatusContainer, SQLVARCHAR);
        mssql_bind($stmt, '@chkshipper', $chkshipper, SQLVARCHAR);
        mssql_bind($stmt, '@chkconsignee', $chkconsignee, SQLVARCHAR);
        mssql_bind($stmt, '@chkcommodity', $chkcommodity, SQLVARCHAR);
        mssql_bind($stmt, '@chkgranFamilia', $chkgranFamilia, SQLVARCHAR);
        mssql_bind($stmt, '@chkfamilia', $chkfamilia, SQLVARCHAR);
        mssql_bind($stmt, '@chksubFamilia', $chksubFamilia, SQLVARCHAR);
        mssql_bind($stmt, '@chkdryReefer', $chkdryReefer, SQLVARCHAR);        
        mssql_bind($stmt, '@chkcontainerVacios', $chkcontenedor, SQLVARCHAR);                                    
        
        mssql_bind($stmt, '@nombreArchivo', $nombreArchivo, SQLVARCHAR);                                    
        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    function granelesExtraccionesArchivos($usuarioId, $moduloId) {
        $query = "Select * From Extraccion_Reportes (nolock) Where usuarioId = '" . $usuarioId . "' and modulo = '" . $moduloId . "' Order by fecha_reporte desc";
        //echo $query;
        $result = sql_db::sql_query($query);
        sql_db::sql_close();        
        return $result;
    }
    
    function granelesExtraccionesSeleccionarArchivo($usuarioId, $moduloId, $arhivoId) {
        $query = "Select nombre_archivo From Extraccion_Reportes (nolock) Where usuarioId = '" . $usuarioId . "' and modulo = '" . $moduloId . "' and id_archivo = '" . $arhivoId . "'";                
        $result = sql_db::sql_query($query);
        $row = sql_db::sql_fetch_assoc($result);
        sql_db::sql_close();        
        return $row;
    }
    
    function granelesExtraccionesEliminarArchivo($usuarioId, $moduloId, $arhivoId) {        
        $query = "Delete e From Extraccion_Reportes e Where usuarioId = '" . $usuarioId . "' and modulo = '" . $moduloId . "' and id_archivo = '" . $arhivoId . "'";        
        sql_db::sql_query($query);        
        sql_db::sql_close();        
        return true;
    }
    
    
}

?>


