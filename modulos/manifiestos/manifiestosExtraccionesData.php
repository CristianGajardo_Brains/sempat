<?PHP

session_start();
include ("../../librerias/conexion.php");
require('manifiestos.class.php');
$objManifiesto = new declaracion();


$usuarioId = $_SESSION['SEMPAT_usuarioId'];
$clienteId = $_SESSION['SEMPAT_clienteId'];

$fechaDesde = mb_convert_encoding(trim($_POST['fechaDesde']), "ISO-8859-1", "UTF-8");
$fechaHasta = mb_convert_encoding(trim($_POST['fechaHasta']), "ISO-8859-1", "UTF-8");
$carpeta = mb_convert_encoding(trim($_POST['carpeta']), "ISO-8859-1", "UTF-8");


$etapa = mb_convert_encoding(trim($_POST['etapa']), "ISO-8859-1", "UTF-8");
$mercado = mb_convert_encoding(trim($_POST['mercado']), "ISO-8859-1", "UTF-8");
$contenedor = mb_convert_encoding(trim($_POST['contenedor']), "ISO-8859-1", "UTF-8");
$dryReefer = mb_convert_encoding(trim($_POST['dryReefer']), "ISO-8859-1", "UTF-8");

$nombreArchivo = mb_convert_encoding(trim($_POST['nombreArchivo']), "ISO-8859-1", "UTF-8");

$tipoEmbalaje = mb_convert_encoding(trim($_POST['tipoEmbalaje']), "ISO-8859-1", "UTF-8");
$statusContainer = mb_convert_encoding(trim($_POST['statusContainer']), "ISO-8859-1", "UTF-8");
$tipoDato = mb_convert_encoding(trim($_POST['tipoDato']), "ISO-8859-1", "UTF-8");

$puertoOrigen = mb_convert_encoding(trim($_POST['puertoOrigen']), "ISO-8859-1", "UTF-8");
$puertoEmbarque = mb_convert_encoding(trim($_POST['puertoEmbarque']), "ISO-8859-1", "UTF-8");
$puertoDescarga = mb_convert_encoding(trim($_POST['puertoDescarga']), "ISO-8859-1", "UTF-8");
$puertoDestino = mb_convert_encoding(trim($_POST['puertoDestino']), "ISO-8859-1", "UTF-8");

$paisOrigen = mb_convert_encoding(trim($_POST['paisOrigen']), "ISO-8859-1", "UTF-8");
$paisEmbarque = mb_convert_encoding(trim($_POST['paisEmbarque']), "ISO-8859-1", "UTF-8");
$paisDescarga = mb_convert_encoding(trim($_POST['paisDescarga']), "ISO-8859-1", "UTF-8");
$paisDestino = mb_convert_encoding(trim($_POST['paisDestino']), "ISO-8859-1", "UTF-8");

$trafico = mb_convert_encoding(trim($_POST['trafico']), "ISO-8859-1", "UTF-8");
$naviera = mb_convert_encoding(trim($_POST['naviera']), "ISO-8859-1", "UTF-8");
$agencia = mb_convert_encoding(trim($_POST['agencia']), "ISO-8859-1", "UTF-8");
$agenteDoc = mb_convert_encoding(trim($_POST['agenteDoc']), "ISO-8859-1", "UTF-8");
$nave = mb_convert_encoding(trim($_POST['nave']), "ISO-8859-1", "UTF-8");
$tipoNave = mb_convert_encoding(trim($_POST['tipoNave']), "ISO-8859-1", "UTF-8");
$shipper = mb_convert_encoding(trim($_POST['shipper']), "ISO-8859-1", "UTF-8");
$consignee = mb_convert_encoding(trim($_POST['consignee']), "ISO-8859-1", "UTF-8");

$granFamilia = mb_convert_encoding(trim($_POST['granFamilia']), "ISO-8859-1", "UTF-8");
$familia = mb_convert_encoding(trim($_POST['familia']), "ISO-8859-1", "UTF-8");
$subFamilia = mb_convert_encoding(trim($_POST['subFamilia']), "ISO-8859-1", "UTF-8");
$commodity = mb_convert_encoding(trim($_POST['commodity']), "ISO-8859-1", "UTF-8");

$tipoCarga = mb_convert_encoding(trim($_POST['tipoCarga']), "ISO-8859-1", "UTF-8");
$tipoServicio = mb_convert_encoding(trim($_POST['tipoServicio']), "ISO-8859-1", "UTF-8");




$chkpuertoOrigen = mb_convert_encoding(trim($_POST['chkpuertoOrigen']), "ISO-8859-1", "UTF-8");
$chkpuertoEmbarque = mb_convert_encoding(trim($_POST['chkpuertoEmbarque']), "ISO-8859-1", "UTF-8");
$chkpuertoDescarga = mb_convert_encoding(trim($_POST['chkpuertoDescarga']), "ISO-8859-1", "UTF-8");
$chkpuertoDestino = mb_convert_encoding(trim($_POST['chkpuertoDestino']), "ISO-8859-1", "UTF-8");

$chkpaisOrigen = mb_convert_encoding(trim($_POST['chkpaisOrigen']), "ISO-8859-1", "UTF-8");
$chkpaisEmbarque = mb_convert_encoding(trim($_POST['chkpaisEmbarque']), "ISO-8859-1", "UTF-8");
$chkpaisDescarga = mb_convert_encoding(trim($_POST['chkpaisDescarga']), "ISO-8859-1", "UTF-8");
$chkpaisDestino = mb_convert_encoding(trim($_POST['chkpaisDestino']), "ISO-8859-1", "UTF-8");

$chktrafico = mb_convert_encoding(trim($_POST['chktrafico']), "ISO-8859-1", "UTF-8");
$chknaviera = mb_convert_encoding(trim($_POST['chknaviera']), "ISO-8859-1", "UTF-8");
$chkagencia = mb_convert_encoding(trim($_POST['chkagencia']), "ISO-8859-1", "UTF-8");
$chkagenteDoc = mb_convert_encoding(trim($_POST['chkagenteDoc']), "ISO-8859-1", "UTF-8");
$chknave = mb_convert_encoding(trim($_POST['chknave']), "ISO-8859-1", "UTF-8");
$chktipoNave = mb_convert_encoding(trim($_POST['chktipoNave']), "ISO-8859-1", "UTF-8");
$chkshipper = mb_convert_encoding(trim($_POST['chkshipper']), "ISO-8859-1", "UTF-8");
$chkconsignee = mb_convert_encoding(trim($_POST['chkconsignee']), "ISO-8859-1", "UTF-8");

$chkgranFamilia = mb_convert_encoding(trim($_POST['chkgranFamilia']), "ISO-8859-1", "UTF-8");
$chkfamilia = mb_convert_encoding(trim($_POST['chkfamilia']), "ISO-8859-1", "UTF-8");
$chksubFamilia = mb_convert_encoding(trim($_POST['chksubFamilia']), "ISO-8859-1", "UTF-8");
$chkcommodity = mb_convert_encoding(trim($_POST['chkcommodity']), "ISO-8859-1", "UTF-8");

$chktipoCarga = mb_convert_encoding(trim($_POST['chktipoCarga']), "ISO-8859-1", "UTF-8");
$chktipoServicio = mb_convert_encoding(trim($_POST['chktipoServicio']), "ISO-8859-1", "UTF-8");


$uploaddir = "../../Reportes/" . $carpeta . "/";
			
if (file_exists($uploaddir)==false) {
    mkdir($uploaddir, 0700, true);
}

$uploaddir = $uploaddir . "/" . $usuarioId . "/";

if (file_exists($uploaddir)==false) {
    mkdir($uploaddir, 0700, true);
}

$objManifiesto->declaracionExtracciones($fechaDesde, $fechaHasta, $tipoDato, $clienteId, $mercado, $etapa, $naviera, $agencia, $trafico, $puertoOrigen, $puertoEmbarque, $nave, $puertoDestino, $puertoDescarga, $tipoNave, $tipoServicio, $tipoCarga, $tipoEmbalaje, $paisOrigen, $paisEmbarque, $agenteDoc, $paisDestino, $paisDescarga, $statusContainer, $shipper, $consignee, $commodity, $granFamilia, $familia, $subFamilia, $dryReefer, $contenedor, $chknaviera, $chkagencia, $chktrafico, $chkpuertoOrigen, $chkpuertoEmbarque, $chknave, $chkpuertoDestino, $chkpuertoDescarga, $chktipoNave, $chktipoServicio, $chktipoCarga, $chktipoEmbalaje, $chkpaisOrigen, $chkpaisEmbarque, $chkagenteDoc, $chkpaisDestino, $chkpaisDescarga, $chkstatusContainer, $chkshipper, $chkconsignee, $chkcommodity, $chkgranFamilia, $chkfamilia, $chksubFamilia, $chkdryReefer, $carpeta, $nombreArchivo, $usuarioId);

?>
