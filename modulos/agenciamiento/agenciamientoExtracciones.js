

function buscarExtracciones(){
    
    var nombreArchivo = $("#inputNombreArchivo").val();
    var carpeta = $("#carpeta").val();

    var fechaDesde = $("#fechaDesde").val();
    var fechaHasta = $("#fechaHasta").val();
    
    var mercado = "";

    var chile = $("input[id=btnChile]:checked").val();
    var peru = $("input[id=btnPeru]:checked").val();
    var ecuador = $("input[id=btnEcuador]:checked").val();
    var colombia = $("input[id=btnColombia]:checked").val();


    mercado = chile + "," + peru + "," + ecuador + "," + colombia;                                                         
    mercado = replaceAll(mercado, ",undefined",""); 
    mercado = replaceAll(mercado, "undefined",""); 

    if(chile == undefined){
        mercado = mercado.substring(1, mercado.length);
    }

    if(fechaDesde == "" && fechaHasta == ""){

        fechasDefecto('PRACTICAJE'); 
        fechaDesde = $("#fechaDesde").val();
        fechaHasta = $("#fechaHasta").val();

        //fechaDesde = $("#fechaDesdeDef").val();
        //fechaHasta = $("#fechaHastaDef").val();
        //$("#fechaDesde").val(fechaDesde);
        //$("#fechaHasta").val(fechaHasta);

    }
    
    if(validarFormulario()){

        $("#divResultado").html("");  

        $("#imgCargando").css("display","block");
        //$("#divPanelFiltros").css("display","none");            
        //$("#divMensaje").css("display","none");

        var agentePortuario = $("#agentePortuario").val();
        var puerto = $("#puerto").val();
        var tipoNave = $("#tipoNave").val();
        var tipoFaena = $("#tipoFaena").val();
        var tipoServicio = $("#tipoServicio").val();
        var naviera = $("#naviera").val();
        var shipper = $("#shipper").val();
        var agenteComercial = $("#agenteComercial").val();
        var sitio = $("#sitio").val();
        var sucursal = $("#sucursal").val();
        var nave = $("#nave").val();
        
        var chkagentePortuario = $("#chkagentePortuario").attr('checked');
        var chkpuerto = $("#chkpuerto").attr('checked');
        var chktipoNave = $("#chktipoNave").attr('checked');
        var chktipoFaena = $("#chktipoFaena").attr('checked');
        var chktipoServicio = $("#chktipoServicio").attr('checked');
        var chknaviera = $("#chknaviera").attr('checked');
        var chkshipper = $("#chkshipper").attr('checked');
        var chkagenteComercial = $("#chkagenteComercial").attr('checked');
        var chksitio = $("#chksitio").attr('checked');
        var chksucursal = $("#chksucursal").attr('checked');
        var chknave = $("#chknave").attr('checked');
        
        
        var tipoDato = $("input[name=tipoDato]:checked").val();               

        $.ajax({
            type: "POST",
            url: "agenciamientoExtraccionesData.php",
            data: { fechaDesde: fechaDesde, fechaHasta: fechaHasta, agentePortuario: agentePortuario, puerto: puerto, tipoNave: tipoNave, tipoFaena: tipoFaena, 
                tipoServicio: tipoServicio, naviera: naviera, shipper: shipper, agenteComercial: agenteComercial, sitio: sitio, sucursal: sucursal, nave: nave,
                chkagentePortuario: chkagentePortuario, chkpuerto: chkpuerto, chktipoNave: chktipoNave, chktipoFaena: chktipoFaena, 
                chktipoServicio: chktipoServicio, chknaviera: chknaviera, chkshipper: chkshipper, chkagenteComercial: chkagenteComercial, chksitio: chksitio, 
                chksucursal: chksucursal, chknave: chknave, nombreArchivo: nombreArchivo, tipoDato:tipoDato, carpeta: carpeta}
            })
            .done(function( msg ) {
                buscarExtraccionesArchivos($("#moduloId").val());
                $("#imgCargando").css("display","none");                                           

        }); 

    }                                                                    

}  


function validarFormulario(){

    var mensaje = "";

    var nombreArchivo = $("#inputNombreArchivo").val();
    var fechaDesde = $("#fechaDesde").val();
    var fechaHasta = $("#fechaHasta").val();                                 
            
    if(nombreArchivo == ""){
        mensaje = "Debe ingresar un nombre para el archivo.<br>";
    }
    
    if(fechaDesde == ""){
        mensaje += "No ha ingresado la fecha inicial.<br>";
    }
    
    if(fechaHasta == ""){
        mensaje += "No ha ingresado la fecha final.<br>";
    }
    
    if(fechaDesde != "" && fechaHasta != "" && validarFechaMenor(fechaDesde, fechaHasta) == false){
        mensaje += "La fecha incial no puede ser mayor a la fecha final.<br>";
    }
    
    if(fechaDesde != "" && fechaHasta != "" && fechaDiferenciasDias(fechaDesde, fechaHasta) > 365){
        mensaje += "La diferencia entre las fechas no puede ser superior a un año.<br>";
    }
            
    if(mensaje != ""){
        $("#divMensaje").html(mensaje);        
        return false;
    }
    else{
        $("#divMensaje").html("");        
        return true;
    }

}
    
    
    