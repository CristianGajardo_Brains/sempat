<?php

header('P3P: CP="IDC DSP COR CURa ADMa OUR IND PHY ONL COM STA"');
session_start();

include("../../default.php");

require('../../clases/sempat.class.php');
$objSem = new sempat();

$clienteId = $_SESSION['SEMPAT_clienteId'];
$usuarioId = $_SESSION['SEMPAT_usuarioId'];

$htmlTablaCabecera = $objSem->indicadoresCabeceraHtml('AGENCIA', $clienteId, 'AGENTE PORTUARIO');

require('agenciamiento.class.php');
$objAge= new agenciamiento();

$dataTable = $objAge->agenciamientoIndicadores(1, 'PA', $clienteId);
$dataTable4 = $objAge->agenciamientoIndicadores(2, 'PA', $clienteId);

$dataTable2 = $objAge->agenciamientoIndicadores(1, 'AP', $clienteId);
$dataTable3 = $objAge->agenciamientoIndicadores(2, 'AP', $clienteId);


$tbodyAgenciaPuertoGeo = "<tbody id=\"tbodyAgenciaPuertoGeo\" class=\"indicadores\">";
$tbodyPuertoAgenciaGeo = "<tbody id=\"tbodyPuertoAgenciaGeo\" class=\"indicadores\" style='display:none'>";
$tbodyPuertoAgencia = "<tbody id=\"tbodyPuertoAgencia\" class=\"indicadores\" style='display:none'>";
$tbodyAgenciaPuerto = "<tbody id=\"tbodyAgenciaPuerto\" class=\"indicadores\" style='display:none'>";

$fila = 1;
$id = 0;

// <editor-fold>
if ($dataTable) {
    
    $col_1 = 0;
    $col_2 = 0;
    $col_3 = 0;
    $col_4 = 0;
    $col_5 = 0;
    $col_6 = 0;
    
    while ($rowdatos = mssql_fetch_array($dataTable)) {
        $fila++;
                
        if (trim($rowdatos[1]) == "") {
            $id++;
            
            $tbodyAgenciaPuertoGeo .= "<tr class=\"nivel1\">";
            $tbodyAgenciaPuertoGeo .= "<td class=\"check\"><input type=\"checkbox\" onClick=\"graficoCheckboxIndicador(this)\" class=\"checkGrafico styled\" /></td>";
            $tbodyAgenciaPuertoGeo .= "<td class=\"nivel1\" onClick=\"graficoAbrirNivel('tr-" . $id . "')\" style='cursor:pointer'>" . htmlentities($rowdatos[0]) . "</td>";
            $tbodyAgenciaPuertoGeo .= "<td>" . number_format($rowdatos[2], 0, '', '.') . "</td>";
            $tbodyAgenciaPuertoGeo .= "<td>" . number_format($rowdatos[3], 0, '', '.') . "</td>";
            $tbodyAgenciaPuertoGeo .= $objSem->calculoDiferencia($rowdatos[2], $rowdatos[3]);
            $tbodyAgenciaPuertoGeo .= $objSem->calculoMkt($rowdatos[2], $rowdatos[8]);
            
            $tbodyAgenciaPuertoGeo .= "<td class=\"clear\"></td>";
            $tbodyAgenciaPuertoGeo .= "<td class=\"left\">" . number_format($rowdatos[4], 0, '', '.') . "</td>";
            $tbodyAgenciaPuertoGeo .= "<td>" . number_format($rowdatos[5], 0, '', '.') . "</td>";
            $tbodyAgenciaPuertoGeo .= $objSem->calculoDiferencia($rowdatos[4], $rowdatos[5]);
            $tbodyAgenciaPuertoGeo .= $objSem->calculoMkt($rowdatos[4], $rowdatos[9]);
            
            $tbodyAgenciaPuertoGeo .= "<td class=\"clear\"></td>";
            $tbodyAgenciaPuertoGeo .= "<td class=\"left\">" . number_format($rowdatos[6], 0, '', '.') . "</td>";
            $tbodyAgenciaPuertoGeo .= "<td>" . number_format($rowdatos[7], 0, '', '.') . "</td>";
            $tbodyAgenciaPuertoGeo .= $objSem->calculoDiferencia($rowdatos[6], $rowdatos[7]);
            $tbodyAgenciaPuertoGeo .= $objSem->calculoMkt($rowdatos[6], $rowdatos[10]);
            $tbodyAgenciaPuertoGeo .= "</tr>";

            $col_1 = $col_1 + $rowdatos[2];
            $col_2 = $col_2 + $rowdatos[3];
            $col_3 = $col_3 + $rowdatos[4];
            $col_4 = $col_4 + $rowdatos[5];
            $col_5 = $col_5 + $rowdatos[6];
            $col_6 = $col_6 + $rowdatos[7];

        }
        else{
            
            $tbodyAgenciaPuertoGeo .= "<tr class=\"tr-" . $id . " nivel2\" style=\"display:none\">";
            $tbodyAgenciaPuertoGeo .= "<td class=\"check\"></td>";
            $tbodyAgenciaPuertoGeo .= "<td class=\"nivel2\">" . htmlentities($rowdatos[1]) . "</td>";
            $tbodyAgenciaPuertoGeo .= "<td>" . number_format($rowdatos[2], 0, '', '.') . "</td>";
            $tbodyAgenciaPuertoGeo .= "<td>" . number_format($rowdatos[3], 0, '', '.') . "</td>";
            $tbodyAgenciaPuertoGeo .= $objSem->calculoDiferencia($rowdatos[2], $rowdatos[3]);
            $tbodyAgenciaPuertoGeo .= $objSem->calculoMkt($rowdatos[2], $rowdatos[8]);
            
            $tbodyAgenciaPuertoGeo .= "<td class=\"clear\"></td>";
            $tbodyAgenciaPuertoGeo .= "<td class=\"left\">" . number_format($rowdatos[4], 0, '', '.') . "</td>";
            $tbodyAgenciaPuertoGeo .= "<td>" . number_format($rowdatos[5], 0, '', '.') . "</td>";
            $tbodyAgenciaPuertoGeo .= $objSem->calculoDiferencia($rowdatos[4], $rowdatos[5]);
            $tbodyAgenciaPuertoGeo .= $objSem->calculoMkt($rowdatos[4], $rowdatos[9]);
            
            $tbodyAgenciaPuertoGeo .= "<td class=\"clear\"></td>";
            $tbodyAgenciaPuertoGeo .= "<td class=\"left\">" . number_format($rowdatos[6], 0, '', '.') . "</td>";
            $tbodyAgenciaPuertoGeo .= "<td>" . number_format($rowdatos[7], 0, '', '.') . "</td>";
            $tbodyAgenciaPuertoGeo .= $objSem->calculoDiferencia($rowdatos[6], $rowdatos[7]);
            $tbodyAgenciaPuertoGeo .= $objSem->calculoMkt($rowdatos[6], $rowdatos[10]);
            $tbodyAgenciaPuertoGeo .= "</tr>";
                       
        }                                
    }
    
    $tbodyAgenciaPuertoGeo .= "<tr class=\"nivelTotal\">";
    $tbodyAgenciaPuertoGeo .= "<td class=\"check\"></td>";
    $tbodyAgenciaPuertoGeo .= "<td class=\"nivelTotal\">Total</td>";
    $tbodyAgenciaPuertoGeo .= "<td>" . number_format($col_1, 0, '', '.') . "</td>";
    $tbodyAgenciaPuertoGeo .= "<td>" . number_format($col_2, 0, '', '.') . "</td>";
    $tbodyAgenciaPuertoGeo .= $objSem->calculoDiferencia($col_1, $col_2);
    $tbodyAgenciaPuertoGeo .= "<td style=\"text-align:center\">100%</td>";

    $tbodyAgenciaPuertoGeo .= "<td class=\"clear\"></td>";
    $tbodyAgenciaPuertoGeo .= "<td class=\"left\">" . number_format($col_3, 0, '', '.') . "</td>";
    $tbodyAgenciaPuertoGeo .= "<td>" . number_format($col_4, 0, '', '.') . "</td>";
    $tbodyAgenciaPuertoGeo .= $objSem->calculoDiferencia($col_3, $col_4);
    $tbodyAgenciaPuertoGeo .= "<td style=\"text-align:center\">100%</td>";

    $tbodyAgenciaPuertoGeo .= "<td class=\"clear\"></td>";
    $tbodyAgenciaPuertoGeo .= "<td class=\"left\">" . number_format($col_5, 0, '', '.') . "</td>";
    $tbodyAgenciaPuertoGeo .= "<td>" . number_format($col_6, 0, '', '.') . "</td>";
    $tbodyAgenciaPuertoGeo .= $objSem->calculoDiferencia($col_5, $col_6);
    $tbodyAgenciaPuertoGeo .= "<td style=\"text-align:center\">100%</td>";
    $tbodyAgenciaPuertoGeo .= "</tr>";
    $fila++;    
    $tbodyAgenciaPuertoGeo .= "</tbody>";
            
}

// </editor-fold>
 

// <editor-fold>
if ($dataTable2) {

    $col_1 = 0;
    $col_2 = 0;
    $col_3 = 0;
    $col_4 = 0;
    $col_5 = 0;
    $col_6 = 0;
    
    while ($rowdatos = mssql_fetch_array($dataTable2)) {
        $fila++;
                
        if (trim($rowdatos[1]) == "") {            
            $id++;
            
            $tbodyPuertoAgenciaGeo .= "<tr class=\"nivel1\">";
            $tbodyPuertoAgenciaGeo .= "<td class=\"check\"><input type=\"checkbox\" onClick=\"graficoCheckboxIndicador(this)\" class=\"checkGrafico styled\"/></td>";
            $tbodyPuertoAgenciaGeo .= "<td class=\"nivel1\" onClick=\"graficoAbrirNivel('tr-" . $id . "')\" style='cursor:pointer'>" . htmlentities($rowdatos[0]) . "</td>";
            $tbodyPuertoAgenciaGeo .= "<td>" . number_format($rowdatos[2], 0, '', '.') . "</td>";
            $tbodyPuertoAgenciaGeo .= "<td>" . number_format($rowdatos[3], 0, '', '.') . "</td>";
            $tbodyPuertoAgenciaGeo .= $objSem->calculoDiferencia($rowdatos[2], $rowdatos[3]);
            $tbodyPuertoAgenciaGeo .= $objSem->calculoMkt($rowdatos[2], $rowdatos[8]);
            
            $tbodyPuertoAgenciaGeo .= "<td class=\"clear\"></td>";
            $tbodyPuertoAgenciaGeo .= "<td class=\"left\">" . number_format($rowdatos[4], 0, '', '.') . "</td>";
            $tbodyPuertoAgenciaGeo .= "<td>" . number_format($rowdatos[5], 0, '', '.') . "</td>";
            $tbodyPuertoAgenciaGeo .= $objSem->calculoDiferencia($rowdatos[4], $rowdatos[5]);
            $tbodyPuertoAgenciaGeo .= $objSem->calculoMkt($rowdatos[4], $rowdatos[9]);
            
            $tbodyPuertoAgenciaGeo .= "<td class=\"clear\"></td>";
            $tbodyPuertoAgenciaGeo .= "<td class=\"left\">" . number_format($rowdatos[6], 0, '', '.') . "</td>";
            $tbodyPuertoAgenciaGeo .= "<td>" . number_format($rowdatos[7], 0, '', '.') . "</td>";
            $tbodyPuertoAgenciaGeo .= $objSem->calculoDiferencia($rowdatos[6], $rowdatos[7]);
            $tbodyPuertoAgenciaGeo .= $objSem->calculoMkt($rowdatos[6], $rowdatos[10]);
            $tbodyPuertoAgenciaGeo .= "</tr>";

            $col_1 = $col_1 + $rowdatos[2];
            $col_2 = $col_2 + $rowdatos[3];
            $col_3 = $col_3 + $rowdatos[4];
            $col_4 = $col_4 + $rowdatos[5];
            $col_5 = $col_5 + $rowdatos[6];
            $col_6 = $col_6 + $rowdatos[7];

        }
        else{
            
            $tbodyPuertoAgenciaGeo .= "<tr class=\"tr-" . $id . " nivel2\" style=\"display:none\">";
            $tbodyPuertoAgenciaGeo .= "<td class=\"check\"></td>";
            $tbodyPuertoAgenciaGeo .= "<td class=\"nivel2\">" . htmlentities($rowdatos[1]) . "</td>";
            $tbodyPuertoAgenciaGeo .= "<td>" . number_format($rowdatos[2], 0, '', '.') . "</td>";
            $tbodyPuertoAgenciaGeo .= "<td>" . number_format($rowdatos[3], 0, '', '.') . "</td>";
            $tbodyPuertoAgenciaGeo .= $objSem->calculoDiferencia($rowdatos[2], $rowdatos[3]);
            $tbodyPuertoAgenciaGeo .= $objSem->calculoMkt($rowdatos[2], $rowdatos[8]);
            
            $tbodyPuertoAgenciaGeo .= "<td class=\"clear\"></td>";
            $tbodyPuertoAgenciaGeo .= "<td class=\"left\">" . number_format($rowdatos[4], 0, '', '.') . "</td>";
            $tbodyPuertoAgenciaGeo .= "<td>" . number_format($rowdatos[5], 0, '', '.') . "</td>";
            $tbodyPuertoAgenciaGeo .= $objSem->calculoDiferencia($rowdatos[4], $rowdatos[5]);
            $tbodyPuertoAgenciaGeo .= $objSem->calculoMkt($rowdatos[4], $rowdatos[9]);
            
            $tbodyPuertoAgenciaGeo .= "<td class=\"clear\"></td>";
            $tbodyPuertoAgenciaGeo .= "<td class=\"left\">" . number_format($rowdatos[6], 0, '', '.') . "</td>";
            $tbodyPuertoAgenciaGeo .= "<td>" . number_format($rowdatos[7], 0, '', '.') . "</td>";
            $tbodyPuertoAgenciaGeo .= $objSem->calculoDiferencia($rowdatos[6], $rowdatos[7]);
            $tbodyPuertoAgenciaGeo .= $objSem->calculoMkt($rowdatos[6], $rowdatos[10]);
            $tbodyPuertoAgenciaGeo .= "</tr>";           
            
        }                                
    }
    
    $tbodyPuertoAgenciaGeo .= "<tr class=\"nivelTotal\">";
    $tbodyPuertoAgenciaGeo .= "<td class=\"check\"></td>";
    $tbodyPuertoAgenciaGeo .= "<td class=\"nivelTotal\">Total</td>";
    $tbodyPuertoAgenciaGeo .= "<td>" . number_format($col_1, 0, '', '.') . "</td>";
    $tbodyPuertoAgenciaGeo .= "<td>" . number_format($col_2, 0, '', '.') . "</td>";
    $tbodyPuertoAgenciaGeo .= $objSem->calculoDiferencia($col_1, $col_2);
    $tbodyPuertoAgenciaGeo .= "<td style=\"text-align:center\">100%</td>";

    $tbodyPuertoAgenciaGeo .= "<td class=\"clear\"></td>";
    $tbodyPuertoAgenciaGeo .= "<td class=\"left\">" . number_format($col_3, 0, '', '.') . "</td>";
    $tbodyPuertoAgenciaGeo .= "<td>" . number_format($col_4, 0, '', '.') . "</td>";
    $tbodyPuertoAgenciaGeo .= $objSem->calculoDiferencia($col_3, $col_4);
    $tbodyPuertoAgenciaGeo .= "<td style=\"text-align:center\">100%</td>";

    $tbodyPuertoAgenciaGeo .= "<td class=\"clear\"></td>";
    $tbodyPuertoAgenciaGeo .= "<td class=\"left\">" . number_format($col_5, 0, '', '.') . "</td>";
    $tbodyPuertoAgenciaGeo .= "<td>" . number_format($col_6, 0, '', '.') . "</td>";
    $tbodyPuertoAgenciaGeo .= $objSem->calculoDiferencia($col_5, $col_6);
    $tbodyPuertoAgenciaGeo .= "<td style=\"text-align:center\">100%</td>";
    $tbodyPuertoAgenciaGeo .= "</tr>";
    $fila++;
                
    $tbodyPuertoAgenciaGeo .= "</tbody>";
            
}
// </editor-fold>


// <editor-fold>
if ($dataTable3) {

    $col_1 = 0;
    $col_2 = 0;
    $col_3 = 0;
    $col_4 = 0;
    $col_5 = 0;
    $col_6 = 0;
    
    while ($rowdatos = mssql_fetch_array($dataTable3)) {
        $fila++;
                
        if (trim($rowdatos[1]) == "") {
            $id++;
            
            $tbodyPuertoAgencia .= "<tr class=\"nivel1\">";
            $tbodyPuertoAgencia .= "<td class=\"check\"><input type=\"checkbox\" onClick=\"graficoCheckboxIndicador(this)\" class=\"checkGrafico styled\"/></td>";
            $tbodyPuertoAgencia .= "<td class=\"nivel1\" onClick=\"graficoAbrirNivel('tr-" . $id . "')\" style='cursor:pointer'>" . htmlentities($rowdatos[0]) . "</td>";
            $tbodyPuertoAgencia .= "<td>" . number_format($rowdatos[2], 0, '', '.') . "</td>";
            $tbodyPuertoAgencia .= "<td>" . number_format($rowdatos[3], 0, '', '.') . "</td>";
            $tbodyPuertoAgencia .= $objSem->calculoDiferencia($rowdatos[2], $rowdatos[3]);
            $tbodyPuertoAgencia .= $objSem->calculoMkt($rowdatos[2], $rowdatos[8]);
            
            $tbodyPuertoAgencia .= "<td class=\"clear\"></td>";
            $tbodyPuertoAgencia .= "<td class=\"left\">" . number_format($rowdatos[4], 0, '', '.') . "</td>";
            $tbodyPuertoAgencia .= "<td>" . number_format($rowdatos[5], 0, '', '.') . "</td>";
            $tbodyPuertoAgencia .= $objSem->calculoDiferencia($rowdatos[4], $rowdatos[5]);
            $tbodyPuertoAgencia .= $objSem->calculoMkt($rowdatos[4], $rowdatos[9]);
            
            $tbodyPuertoAgencia .= "<td class=\"clear\"></td>";
            $tbodyPuertoAgencia .= "<td class=\"left\">" . number_format($rowdatos[6], 0, '', '.') . "</td>";
            $tbodyPuertoAgencia .= "<td>" . number_format($rowdatos[7], 0, '', '.') . "</td>";
            $tbodyPuertoAgencia .= $objSem->calculoDiferencia($rowdatos[6], $rowdatos[7]);
            $tbodyPuertoAgencia .= $objSem->calculoMkt($rowdatos[6], $rowdatos[10]);
            $tbodyPuertoAgencia .= "</tr>";

            $col_1 = $col_1 + $rowdatos[2];
            $col_2 = $col_2 + $rowdatos[3];
            $col_3 = $col_3 + $rowdatos[4];
            $col_4 = $col_4 + $rowdatos[5];
            $col_5 = $col_5 + $rowdatos[6];
            $col_6 = $col_6 + $rowdatos[7];

        }
        else{
            
            $tbodyPuertoAgencia .= "<tr class=\"tr-" . $id . " nivel2\" style=\"display:none\">";
            $tbodyPuertoAgencia .= "<td class=\"check\"></td>";
            $tbodyPuertoAgencia .= "<td class=\"nivel2\">" . htmlentities($rowdatos[1]) . "</td>";
            $tbodyPuertoAgencia .= "<td>" . number_format($rowdatos[2], 0, '', '.') . "</td>";
            $tbodyPuertoAgencia .= "<td>" . number_format($rowdatos[3], 0, '', '.') . "</td>";
            $tbodyPuertoAgencia .= $objSem->calculoDiferencia($rowdatos[2], $rowdatos[3]);
            $tbodyPuertoAgencia .= $objSem->calculoMkt($rowdatos[2], $rowdatos[8]);
            
            $tbodyPuertoAgencia .= "<td class=\"clear\"></td>";
            $tbodyPuertoAgencia .= "<td class=\"left\">" . number_format($rowdatos[4], 0, '', '.') . "</td>";
            $tbodyPuertoAgencia .= "<td>" . number_format($rowdatos[5], 0, '', '.') . "</td>";
            $tbodyPuertoAgencia .= $objSem->calculoDiferencia($rowdatos[4], $rowdatos[5]);
            $tbodyPuertoAgencia .= $objSem->calculoMkt($rowdatos[4], $rowdatos[9]);
            
            $tbodyPuertoAgencia .= "<td class=\"clear\"></td>";
            $tbodyPuertoAgencia .= "<td class=\"left\">" . number_format($rowdatos[6], 0, '', '.') . "</td>";
            $tbodyPuertoAgencia .= "<td>" . number_format($rowdatos[7], 0, '', '.') . "</td>";
            $tbodyPuertoAgencia .= $objSem->calculoDiferencia($rowdatos[6], $rowdatos[7]);
            $tbodyPuertoAgencia .= $objSem->calculoMkt($rowdatos[6], $rowdatos[10]);
            $tbodyPuertoAgencia .= "</tr>";           
            
        }                                
    }
    
    $tbodyPuertoAgencia .= "<tr class=\"nivelTotal\">";
    $tbodyPuertoAgencia .= "<td class=\"check\"></td>";
    $tbodyPuertoAgencia .= "<td class=\"nivelTotal\">Total</td>";
    $tbodyPuertoAgencia .= "<td>" . number_format($col_1, 0, '', '.') . "</td>";
    $tbodyPuertoAgencia .= "<td>" . number_format($col_2, 0, '', '.') . "</td>";
    $tbodyPuertoAgencia .= $objSem->calculoDiferencia($col_1, $col_2);
    $tbodyPuertoAgencia .= "<td style=\"text-align:center\">100%</td>";

    $tbodyPuertoAgencia .= "<td class=\"clear\"></td>";
    $tbodyPuertoAgencia .= "<td class=\"left\">" . number_format($col_3, 0, '', '.') . "</td>";
    $tbodyPuertoAgencia .= "<td>" . number_format($col_4, 0, '', '.') . "</td>";
    $tbodyPuertoAgencia .= $objSem->calculoDiferencia($col_3, $col_4);
    $tbodyPuertoAgencia .= "<td style=\"text-align:center\">100%</td>";

    $tbodyPuertoAgencia .= "<td class=\"clear\"></td>";
    $tbodyPuertoAgencia .= "<td class=\"left\">" . number_format($col_5, 0, '', '.') . "</td>";
    $tbodyPuertoAgencia .= "<td>" . number_format($col_6, 0, '', '.') . "</td>";
    $tbodyPuertoAgencia .= $objSem->calculoDiferencia($col_5, $col_6);
    $tbodyPuertoAgencia .= "<td style=\"text-align:center\">100%</td>";
    $tbodyPuertoAgencia .= "</tr>";
    $fila++;
                
    $tbodyPuertoAgencia .= "</tbody>";
            
}

// </editor-fold>


// <editor-fold>
if ($dataTable4) {
      
    $col_1 = 0;
    $col_2 = 0;
    $col_3 = 0;
    $col_4 = 0;
    $col_5 = 0;
    $col_6 = 0;
    
    while ($rowdatos = mssql_fetch_array($dataTable4)) {
        $fila++;
                
        if (trim($rowdatos[1]) == "") {
            $id++;
            
            $tbodyAgenciaPuerto .= "<tr class=\"nivel1\">";
            $tbodyAgenciaPuerto .= "<td class=\"check\"><input type=\"checkbox\" onClick=\"graficoCheckboxIndicador(this)\" class=\"checkGrafico styled\" /></td>";
            $tbodyAgenciaPuerto .= "<td class=\"nivel1\" onClick=\"graficoAbrirNivel('tr-" . $id . "')\" style='cursor:pointer'>" . htmlentities($rowdatos[0]) . "</td>";
            $tbodyAgenciaPuerto .= "<td>" . number_format($rowdatos[2], 0, '', '.') . "</td>";
            $tbodyAgenciaPuerto .= "<td>" . number_format($rowdatos[3], 0, '', '.') . "</td>";
            $tbodyAgenciaPuerto .= $objSem->calculoDiferencia($rowdatos[2], $rowdatos[3]);
            $tbodyAgenciaPuerto .= $objSem->calculoMkt($rowdatos[2], $rowdatos[8]);
            
            $tbodyAgenciaPuerto .= "<td class=\"clear\"></td>";
            $tbodyAgenciaPuerto .= "<td class=\"left\">" . number_format($rowdatos[4], 0, '', '.') . "</td>";
            $tbodyAgenciaPuerto .= "<td>" . number_format($rowdatos[5], 0, '', '.') . "</td>";
            $tbodyAgenciaPuerto .= $objSem->calculoDiferencia($rowdatos[4], $rowdatos[5]);
            $tbodyAgenciaPuerto .= $objSem->calculoMkt($rowdatos[4], $rowdatos[9]);
            
            $tbodyAgenciaPuerto .= "<td class=\"clear\"></td>";
            $tbodyAgenciaPuerto .= "<td class=\"left\">" . number_format($rowdatos[6], 0, '', '.') . "</td>";
            $tbodyAgenciaPuerto .= "<td>" . number_format($rowdatos[7], 0, '', '.') . "</td>";
            $tbodyAgenciaPuerto .= $objSem->calculoDiferencia($rowdatos[6], $rowdatos[7]);
            $tbodyAgenciaPuerto .= $objSem->calculoMkt($rowdatos[6], $rowdatos[10]);
            $tbodyAgenciaPuerto .= "</tr>";

            $col_1 = $col_1 + $rowdatos[2];
            $col_2 = $col_2 + $rowdatos[3];
            $col_3 = $col_3 + $rowdatos[4];
            $col_4 = $col_4 + $rowdatos[5];
            $col_5 = $col_5 + $rowdatos[6];
            $col_6 = $col_6 + $rowdatos[7];

        }
        else{
            
            $tbodyAgenciaPuerto .= "<tr class=\"tr-" . $id . " nivel2\" style=\"display:none\">";
            $tbodyAgenciaPuerto .= "<td class=\"check\"></td>";
            $tbodyAgenciaPuerto .= "<td class=\"nivel2\">" . htmlentities($rowdatos[1]) . "</td>";
            $tbodyAgenciaPuerto .= "<td>" . number_format($rowdatos[2], 0, '', '.') . "</td>";
            $tbodyAgenciaPuerto .= "<td>" . number_format($rowdatos[3], 0, '', '.') . "</td>";
            $tbodyAgenciaPuerto .= $objSem->calculoDiferencia($rowdatos[2], $rowdatos[3]);
            $tbodyAgenciaPuerto .= $objSem->calculoMkt($rowdatos[2], $rowdatos[8]);
            
            $tbodyAgenciaPuerto .= "<td class=\"clear\"></td>";
            $tbodyAgenciaPuerto .= "<td class=\"left\">" . number_format($rowdatos[4], 0, '', '.') . "</td>";
            $tbodyAgenciaPuerto .= "<td>" . number_format($rowdatos[5], 0, '', '.') . "</td>";
            $tbodyAgenciaPuerto .= $objSem->calculoDiferencia($rowdatos[4], $rowdatos[5]);
            $tbodyAgenciaPuerto .= $objSem->calculoMkt($rowdatos[4], $rowdatos[9]);
            
            $tbodyAgenciaPuerto .= "<td class=\"clear\"></td>";
            $tbodyAgenciaPuerto .= "<td class=\"left\">" . number_format($rowdatos[6], 0, '', '.') . "</td>";
            $tbodyAgenciaPuerto .= "<td>" . number_format($rowdatos[7], 0, '', '.') . "</td>";
            $tbodyAgenciaPuerto .= $objSem->calculoDiferencia($rowdatos[6], $rowdatos[7]);
            $tbodyAgenciaPuerto .= $objSem->calculoMkt($rowdatos[6], $rowdatos[10]);
            $tbodyAgenciaPuerto .= "</tr>";           
            
        }                                
    }
    
    $tbodyAgenciaPuerto .= "<tr class=\"nivelTotal\">";
    $tbodyAgenciaPuerto .= "<td class=\"check\"></td>";
    $tbodyAgenciaPuerto .= "<td class=\"nivelTotal\">Total</td>";
    $tbodyAgenciaPuerto .= "<td>" . number_format($col_1, 0, '', '.') . "</td>";
    $tbodyAgenciaPuerto .= "<td>" . number_format($col_2, 0, '', '.') . "</td>";
    $tbodyAgenciaPuerto .= $objSem->calculoDiferencia($col_1, $col_2);
    $tbodyAgenciaPuerto .= "<td style=\"text-align:center\">100%</td>";

    $tbodyAgenciaPuerto .= "<td class=\"clear\"></td>";
    $tbodyAgenciaPuerto .= "<td class=\"left\">" . number_format($col_3, 0, '', '.') . "</td>";
    $tbodyAgenciaPuerto .= "<td>" . number_format($col_4, 0, '', '.') . "</td>";
    $tbodyAgenciaPuerto .= $objSem->calculoDiferencia($col_3, $col_4);
    $tbodyAgenciaPuerto .= "<td style=\"text-align:center\">100%</td>";

    $tbodyAgenciaPuerto .= "<td class=\"clear\"></td>";
    $tbodyAgenciaPuerto .= "<td class=\"left\">" . number_format($col_5, 0, '', '.') . "</td>";
    $tbodyAgenciaPuerto .= "<td>" . number_format($col_6, 0, '', '.') . "</td>";
    $tbodyAgenciaPuerto .= $objSem->calculoDiferencia($col_5, $col_6);
    $tbodyAgenciaPuerto .= "<td style=\"text-align:center\">100%</td>";
    $tbodyAgenciaPuerto .= "</tr>";
    $fila++;            
    
    $tbodyAgenciaPuertoGeo .= "</tbody>";
            
}

// </editor-fold>

echo "<thead id=\"tablaValoresTHead\">" . $htmlTablaCabecera . "</thead>";
echo $tbodyAgenciaPuertoGeo;
echo $tbodyPuertoAgenciaGeo;
echo $tbodyPuertoAgencia;
echo $tbodyAgenciaPuerto;


mssql_free_result($dataTable);
mssql_free_result($dataTable2);
mssql_free_result($dataTable3);
mssql_free_result($dataTable4);


?>
