var count = 0;


    
function agenciamientoIndicadoresData(tipo, nivel1){

    $.ajax({
        url:'agenciamientoIndicadoresDatos.php',
        type:'POST',
        data:{
            tipo: encodeURIComponent(tipo),
            nivel1: encodeURIComponent(nivel1)
        },
        //cache:false,
        //async: false,
        dataType: "html"
    }).done(function(data) {
        
        $('#tdResumen').html("<center><font color='#3D85FE' size='3'>Cantidad de Recaladas</font>");
        
        $("#tablaValores").html(data)
        //$(data).insertAfter("#tablaValoresTHead");
        
        agenciamientoIndicadoresDataGrafico();

    });

}


function agenciamientoIndicadoresDataGrafico(){

    $.ajax({
        url:'agenciamientoIndicadoresDatosGrafico.php',
        type:'POST',
        data:{
        //tipo: encodeURIComponent(tipo),
        //nivel1: encodeURIComponent(nivel1)
        },
        //cache:false,
        //async: false,
        dataType: "html"
    }).done(function(data) {
        $("#divDatosGrafico").html(data);            
        $('#divOpciones').css('display', '');
        $('#divGraficos').css('display', '');
        $('#divGrafico').css('display', '');
        $('#tablaValores').css('display', '');
        $("#imgCargando").css("display","none");
        
        var checks = 0;
        
        $("#tbodyAgenciaPuertoGeo tr").each(function (index, elemento) {
                                                                        
            if(checks < 3 && $(elemento).hasClass("nivel1")){
                
                $(this).children("td").each(function (index2, td) {

                    if(index2 == 0){

                        $(td).children("input").each(function (index3, input) {
                            input.click();
                        });

                        $(td).children("span ").each(function (index3, span) {
                            $(span).removeClass('checkboxC');
                            $(span).addClass('checkboxChecked');
                        });
                        
                        checks++;

                    }                                                
                });
            }                    
        });
        
        Custom.initChkbox();
        
        
    });

}        
    
    
    
    
    