<?php

    class agenciamiento {

        //constructor

        /******************************************************/
        /********* Agenciamiento Tipo I Agencia - Puerto  *****/
        /******************************************************/

        function agenciamientoIndicadores($tipoOrden, $tipoBusqueda, $clienteId) {
            $query = "Exec agenciamientoIndicadores " . $tipoOrden . ",'" . $tipoBusqueda . "'," . $clienteId . ", '', ''";
            //echo $query . "<br>";
            $result = sql_db::sql_query($query);
            sql_db::sql_close();
            return $result;
        }

        function agenciamientoIndicadoresGrafico($tipo, $clienteId) {
            $query = "Exec agenciamientoIndicadoresGrafico " . $tipo . "," . $clienteId;
            //echo $query;
            $result = sql_db::sql_query($query);
            sql_db::sql_close();
            return $result;
        }

        function agenciamientoListaPuertos($clienteId) {
            $query = "  Select distinct i.puerto, i.yyyy, i.mm
                        From IndicadoresAgenciamientoTipoI i (nolock)
                        Where   i.yyyy = (Select MAX(CAST(yyyy as int)) From IndicadoresAgenciamientoTipoI)
                        and     i.mm = (Select MAX(CAST(mm as int)) From IndicadoresAgenciamientoTipoI i2 Where i2.yyyy = i.yyyy)
                        and     i.puerto <> ''
                        and     i.clienteId = " . $clienteId;
            $result = sql_db::sql_query($query);
            sql_db::sql_close();
            return $result;
        }

        function agenciamientoListaAgencias($clienteId) {
            $query = "  Select distinct agencia
                        From IndicadoresAgenciamientoTipoI i
                        Where   i.yyyy = (Select MAX(CAST(yyyy as int)) From IndicadoresAgenciamientoTipoI)
                        and     i.mm = (Select MAX(CAST(mm as int)) From IndicadoresAgenciamientoTipoI i2 Where i2.yyyy = i.yyyy)
                        and     i.agencia <> ''
                        and     i.clienteId = " . $clienteId;
            $result = sql_db::sql_query($query);
            sql_db::sql_close();
            return $result;
        }

        /******************************************************/
        /************* Agenciamiento Tipo II ******************/
        /******************************************************/

        function agenciamientoTipoIIData($nombreSP, $fechaDesde, $fechaHasta, $campo1, $campo2, $campo3, $periodos1, $periodos2, $clienteId, $agenciaNombre, $puertoNombre, $tipoNaveNombre, $tipoFaenaNombre, $tipoServicioNombre, $navieraNombre, $shipperNombre, $agenteComercialNombre, $sitioNombre, $sucursalNombre, $naveNombre, $usuarioId) {
            /*$query = "Exec " . $nombreSP . " '" . $fechaDesde . "','" . $fechaHasta . "','" . $campo1 . "','" . $campo2 . "','" . $campo3 . "','" . $periodos1 . "','" . $periodos2 . "'," . $clienteId . ", '" . trim($agenciaNombre) . "', '" . trim($puertoNombre) . "', '" . trim($tipoNaveNombre) . "', '" . trim($tipoFaenaNombre) . "', '" . trim($tipoServicioNombre) . "', '" . trim($navieraNombre) . "', '" . trim($shipperNombre) . "', '" . trim($agenteComercialNombre) . "', '" . trim($sitioNombre) . "', '" . trim($sucursalNombre) . "','" . trim($naveNombre) . "'," . $usuarioId . "";
            echo $query;*/

            $stmt = mssql_init("agenciamientoReportesRecaladas");

            mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
            mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);
            mssql_bind($stmt, '@var1', $campo1, SQLVARCHAR);
            mssql_bind($stmt, '@var2', $campo2, SQLVARCHAR);
            mssql_bind($stmt, '@var3', $campo3, SQLVARCHAR);        
            mssql_bind($stmt, '@mes1', $periodos1, SQLTEXT);
            mssql_bind($stmt, '@mes2', $periodos2, SQLVARCHAR);
            mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
            mssql_bind($stmt, '@agencia', $agenciaNombre, SQLVARCHAR);
            mssql_bind($stmt, '@puerto', $puertoNombre, SQLVARCHAR);
            mssql_bind($stmt, '@tipoNave', $tipoNaveNombre, SQLVARCHAR);
            mssql_bind($stmt, '@tipoFaena', $tipoFaenaNombre, SQLVARCHAR);
            mssql_bind($stmt, '@tipoServicio', $tipoServicioNombre, SQLVARCHAR);
            mssql_bind($stmt, '@naviera', $navieraNombre, SQLVARCHAR);
            mssql_bind($stmt, '@shipper', $shipperNombre, SQLVARCHAR);
            mssql_bind($stmt, '@agenteComercial', $agenteComercialNombre, SQLVARCHAR);
            mssql_bind($stmt, '@sitio', $sitioNombre, SQLVARCHAR);
            mssql_bind($stmt, '@sucursal', $sucursalNombre, SQLVARCHAR);
            mssql_bind($stmt, '@nave', $naveNombre, SQLVARCHAR);        
            mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

            $result = sql_db::sql_ejecutar_sp($stmt);
            sql_db::sql_close();
            return $result;







            $result = sql_db::sql_query($query);
            sql_db::sql_close();
            return $result;
        }

        function agenciamientoFechasDefecto($clienteId) {
            $query = "Exec seleccionarFechasDefecto '" . $clienteId . "','P'";
            //echo $query;
            $result = sql_db::sql_query($query);        
            $row = sql_db::sql_fetch_assoc($result);
            sql_db::sql_close();
            return $row;
        }

        function agenciamientoMostrarNivelII($campo1, $usuarioId) {
            $query = "  Select *
                        From AgenciamientoReporte fr (nolock)
                        Where   fr.nivel2 = 2
                        and     fr.campo1 = '" . $campo1 . "'
                        and     fr.usuarioId = '" . $usuarioId . "'
                        Order by 6 desc, 9 desc";
            //echo $query;
            $result = sql_db::sql_query($query);
            sql_db::sql_close();
            return $result;
        }

        function agenciamientoMostrarNivelIII($campo1, $campo2, $usuarioId) {
            $query = "  Select *
                        From AgenciamientoReporte fr (nolock)
                        Where   fr.nivel3 = 3
                        and     fr.campo1 = '" . $campo1 . "'
                        and     fr.campo2 = '" . $campo2 . "'
                        and     fr.usuarioId = '" . $usuarioId . "'
                        Order by 7 desc, 10 desc";
            echo $query;
            $result = sql_db::sql_query($query);
            sql_db::sql_close();
            return $result;
        }

        /******************************************************/
        /************** Agenciamiento Extracciones ************/
        /******************************************************/



        function agenciamientoExtracciones($fechaDesde, $fechaHasta, $tipoDato, $agentePortuario, $puerto, $tipoNave, $tipoFaena, $tipoServicio, $naviera, $shipper, $agenteComercial, $sitio, 
                $sucursal, $nave, $chkagentePortuario, $chkpuerto, $chktipoNave, $chktipoFaena, $chktipoServicio, $chknaviera, $chkshipper, $chkagenteComercial, $chksitio, $chksucursal, $chknave, $nombreArchivo, $usuarioId){


            /*$query = "Exec agenciamientoExtracciones '" . $fechaDesde . "','" . $fechaHasta . "','" . $tipoDato . "','" . $agentePortuario . "','" . $puerto . "','" . 
                    $tipoNave . "','" . $tipoFaena . "','" . $tipoServicio . "','" . $naviera . "','" . $shipper . "','" . $agenteComercial . "','" . $sitio . "','" .                 
                    $sucursal . "','" . $nave . "','" . 

                    $chkagentePortuario . "','" . $chkpuerto . "','" . $chktipoNave . "','" . $chktipoFaena . "','" . $chktipoServicio . "','" . $chknaviera . "','" . $chkshipper . "','" . 
                    $chkagenteComercial . "','" . $chksitio . "','" . $chksucursal . "','" . $chknave . "','" . $nombreArchivo . "','" . $usuarioId . "'";        
            echo $query;*/


            $stmt = mssql_init("agenciamientoExtracciones");

            mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
            mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);        
            mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
            //mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
            mssql_bind($stmt, '@agentePortuario', $agentePortuario, SQLVARCHAR);
            mssql_bind($stmt, '@puerto', $puerto, SQLVARCHAR);
            mssql_bind($stmt, '@tipoNave', $tipoNave, SQLVARCHAR);
            mssql_bind($stmt, '@tipoFaena', $tipoFaena, SQLVARCHAR);
            mssql_bind($stmt, '@tipoServicio', $tipoServicio, SQLVARCHAR);
            mssql_bind($stmt, '@naviera', $naviera, SQLVARCHAR);
            mssql_bind($stmt, '@shipper', $shipper, SQLVARCHAR);
            mssql_bind($stmt, '@agenteComercial', $agenteComercial, SQLVARCHAR);
            mssql_bind($stmt, '@sitio', $sitio, SQLVARCHAR);
            mssql_bind($stmt, '@sucursal', $sucursal, SQLVARCHAR);
            mssql_bind($stmt, '@nave', $nave, SQLVARCHAR);
            mssql_bind($stmt, '@chkagentePortuario', $chkagentePortuario, SQLVARCHAR);
            mssql_bind($stmt, '@chkpuerto', $chkpuerto, SQLVARCHAR);
            mssql_bind($stmt, '@chktipoNave', $chktipoNave, SQLVARCHAR);
            mssql_bind($stmt, '@chktipoFaena', $chktipoFaena, SQLVARCHAR);
            mssql_bind($stmt, '@chktipoServicio', $chktipoServicio, SQLVARCHAR);
            mssql_bind($stmt, '@chknaviera', $chknaviera, SQLVARCHAR);
            mssql_bind($stmt, '@chkshipper', $chkshipper, SQLVARCHAR);
            mssql_bind($stmt, '@chkagenteComercial', $chkagenteComercial, SQLVARCHAR);
            mssql_bind($stmt, '@chksitio', $chksitio, SQLVARCHAR);
            mssql_bind($stmt, '@chksucursal', $chksucursal, SQLVARCHAR);
            mssql_bind($stmt, '@chknave', $chknave, SQLVARCHAR);
            mssql_bind($stmt, '@nombreArchivo', $nombreArchivo, SQLVARCHAR);
            mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

            $result = sql_db::sql_ejecutar_sp($stmt);
            sql_db::sql_close();
            return $result;         

        }



        function agenciamientoExtraccionesArchivos($usuarioId, $moduloId) {
            $query = "Select * From Extraccion_Reportes (nolock) Where usuarioId = '" . $usuarioId . "' and modulo = '" . $moduloId . "' Order by fecha_reporte desc";
            //echo $query;
            $result = sql_db::sql_query($query);
            sql_db::sql_close();        
            return $result;
        }

        function agenciamientoExtraccionesSeleccionarArchivo($usuarioId, $moduloId, $arhivoId) {
            $query = "Select nombre_archivo From Extraccion_Reportes (nolock) Where usuarioId = '" . $usuarioId . "' and modulo = '" . $moduloId . "' and id_archivo = '" . $arhivoId . "'";                
            $result = sql_db::sql_query($query);
            $row = sql_db::sql_fetch_assoc($result);
            sql_db::sql_close();        
            return $row;
        }

        function agenciamientoExtraccionesEliminarArchivo($usuarioId, $moduloId, $arhivoId) {        
            $query = "Delete e From Extraccion_Reportes e Where usuarioId = '" . $usuarioId . "' and modulo = '" . $moduloId . "' and id_archivo = '" . $arhivoId . "'";        
            sql_db::sql_query($query);        
            sql_db::sql_close();        
            return true;
        }



}

?>


