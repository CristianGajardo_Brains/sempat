<?PHP

session_start();
include ("../../librerias/conexion.php");
require('agenciamiento.class.php');
$objAgencia = new agenciamiento();


$usuarioId = $_SESSION['SEMPAT_usuarioId'];
$clienteId = $_SESSION['SEMPAT_clienteId'];

$fechaDesde = mb_convert_encoding(trim($_POST['fechaDesde']), "ISO-8859-1", "UTF-8");
$fechaHasta = mb_convert_encoding(trim($_POST['fechaHasta']), "ISO-8859-1", "UTF-8");
$carpeta = mb_convert_encoding(trim($_POST['carpeta']), "ISO-8859-1", "UTF-8");


$nombreArchivo = mb_convert_encoding(trim($_POST['nombreArchivo']), "ISO-8859-1", "UTF-8");

$agentePortuario = mb_convert_encoding(trim($_POST['agentePortuario']), "ISO-8859-1", "UTF-8");
$puerto = mb_convert_encoding(trim($_POST['puerto']), "ISO-8859-1", "UTF-8");
$tipoNave = mb_convert_encoding(trim($_POST['tipoNave']), "ISO-8859-1", "UTF-8");
$tipoFaena = mb_convert_encoding(trim($_POST['tipoFaena']), "ISO-8859-1", "UTF-8");
$tipoServicio = mb_convert_encoding(trim($_POST['tipoServicio']), "ISO-8859-1", "UTF-8");
$naviera = mb_convert_encoding(trim($_POST['naviera']), "ISO-8859-1", "UTF-8");
$shipper = mb_convert_encoding(trim($_POST['shipper']), "ISO-8859-1", "UTF-8");
$agenteComercial = mb_convert_encoding(trim($_POST['agenteComercial']), "ISO-8859-1", "UTF-8");
$sitio = mb_convert_encoding(trim($_POST['sitio']), "ISO-8859-1", "UTF-8");
$sucursal = mb_convert_encoding(trim($_POST['sucursal']), "ISO-8859-1", "UTF-8");
$nave = mb_convert_encoding(trim($_POST['nave']), "ISO-8859-1", "UTF-8");

$chkagentePortuario = mb_convert_encoding(trim($_POST['chkagentePortuario']), "ISO-8859-1", "UTF-8");
$chkpuerto = mb_convert_encoding(trim($_POST['chkpuerto']), "ISO-8859-1", "UTF-8");
$chktipoNave = mb_convert_encoding(trim($_POST['chktipoNave']), "ISO-8859-1", "UTF-8");
$chktipoFaena = mb_convert_encoding(trim($_POST['chktipoFaena']), "ISO-8859-1", "UTF-8");
$chktipoServicio = mb_convert_encoding(trim($_POST['chktipoServicio']), "ISO-8859-1", "UTF-8");
$chknaviera = mb_convert_encoding(trim($_POST['chknaviera']), "ISO-8859-1", "UTF-8");
$chkshipper = mb_convert_encoding(trim($_POST['chkshipper']), "ISO-8859-1", "UTF-8");
$chkagenteComercial = mb_convert_encoding(trim($_POST['chkagenteComercial']), "ISO-8859-1", "UTF-8");
$chksitio = mb_convert_encoding(trim($_POST['chksitio']), "ISO-8859-1", "UTF-8");
$chksucursal = mb_convert_encoding(trim($_POST['chksucursal']), "ISO-8859-1", "UTF-8");
$chknave = mb_convert_encoding(trim($_POST['chknave']), "ISO-8859-1", "UTF-8");

$agentePortuario = str_replace("\'", "'", $agentePortuario);
$puerto = str_replace("\'", "'", $puerto);
$tipoNave = str_replace("\'", "'", $tipoNave);
$tipoFaena = str_replace("\'", "'", $tipoFaena);
$tipoServicio = str_replace("\'", "'", $tipoServicio);
$naviera = str_replace("\'", "'", $naviera);
$shipper = str_replace("\'", "'", $shipper);
$agenteComercial = str_replace("\'", "'", $agenteComercial);
$sitio = str_replace("\'", "'", $sitio);
$sucursal = str_replace("\'", "'", $sucursal);
$nave = str_replace("\'", "'", $nave);


$tipoDato = "recaladas";

$uploaddir = "../../Reportes/" . $carpeta . "/";
			
if (file_exists($uploaddir)==false) {
    mkdir($uploaddir, 0700, true);
}

$uploaddir = $uploaddir . "/" . $usuarioId . "/";

if (file_exists($uploaddir)==false) {
    mkdir($uploaddir, 0700, true);
}

$objAgencia->agenciamientoExtracciones($fechaDesde, $fechaHasta, $tipoDato, $agentePortuario, $puerto, $tipoNave, $tipoFaena, $tipoServicio, $naviera, $shipper, $agenteComercial, $sitio, $sucursal, $nave, $chkagentePortuario, $chkpuerto, $chktipoNave, $chktipoFaena, $chktipoServicio, $chknaviera, $chkshipper, $chkagenteComercial, $chksitio, $chksucursal, $chknave, $nombreArchivo, $usuarioId);

?>
