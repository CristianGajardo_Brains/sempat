<?PHP

session_start();
include ("../../librerias/conexion.php");
require('agenciamiento.class.php');
require('../../clases/sempat.class.php');
$objAgencia = new agenciamiento();
$objSem = new sempat();


$usuarioId = $_SESSION['SEMPAT_usuarioId'];
$clienteId = $_SESSION['SEMPAT_clienteId'];

$nivel3 = mb_convert_encoding(trim($_POST['nivel3']), "ISO-8859-1", "UTF-8");
$campo1 = mb_convert_encoding(trim($_POST['campo1']), "ISO-8859-1", "UTF-8");
$tipoDato = mb_convert_encoding(trim($_POST['tipoDato']), "ISO-8859-1", "UTF-8");
$nMeses = mb_convert_encoding(trim($_POST['nMeses']), "ISO-8859-1", "UTF-8");
$trId = mb_convert_encoding(trim($_POST['trId']), "ISO-8859-1", "UTF-8");

$consultaTabla = $objAgencia->agenciamientoMostrarNivelII($campo1, $usuarioId);


$campo2 = "";
$valorCampo2 = "";
$salida = "";
$salidaOtros = "";
$total = 0;
$totalAnterior = 0;

$fila = 0;

$cursor = ""; 

if($nivel3 != ""){
    $cursor = " style='cursor:pointer'";
}


if ($consultaTabla) {
    
    $numFilas = mssql_num_rows($consultaTabla);

    while ($manifiesto = mssql_fetch_array($consultaTabla)) {
        
        $fila++;

        $campo2 = htmlentities($manifiesto[8]);

        if(strlen($campo1) > 22){
            $valorCampo2 = substr($campo2, 0, 20) . "...";
        }
        else{
            $valorCampo2 = $campo2;
        }

        if($campo2 != "OTROS"){

            $salida .= "<tr id='" . $trId . "-". $fila ."' class='nivel2'><td class='check'><input class='styled' type='checkbox' onClick='graficoCheckboxReporte(this);'></td><td class='nivel2' " . $cursor . " title =\"" . $campo2 . "\" onClick=\"buscarNivel3(this, '" . $nivel3 . "', '" . $campo1 . "','" . $campo2 . "', '" . $datos . "', '" . $nMeses . "');\">" . $valorCampo2 . "</td>";        

            for ($i = 0; $i < $nMeses; $i++) {

                $salida .= "<td>" . number_format($manifiesto[10 + $i], 0, '', '.') . "</td>";
                $meses[$i][$fila] .= $manifiesto[10 + $i];
            }

            $salida .= "<td><b>" . number_format($manifiesto[5], 0, '', '.') . "</b></td><td><b>" . number_format($manifiesto[22], 0, '', '.') . "</b></td>";                                    
            $salida .= $objSem->calculoDiferencia($manifiesto[5], $manifiesto[22]) . "</tr>";
            
            $total = $total + $manifiesto[5];
            $totalAnterior = $totalAnterior + $manifiesto[22];
                        
        }
        else{
            
            if($manifiesto[22] != ""){
                $salidaOtros .= "<tr id='" . $trId . "-". $fila ."' class='nivel2'><td class='check'><input type='checkbox' class='styled' onClick='graficoAgregarReporte(this);'/></td><td class='nivel2' title =\"" . $campo2 . "\">" . $valorCampo2 . "</td>";        

                for ($i = 0; $i < $nMeses; $i++) {

                    $salidaOtros .= "<td>" . number_format($manifiesto[10 + $i], 0, '', '.') . "</td>";
                    $meses[$i][$numFilas] .= $manifiesto[10 + $i];
                }

                $salidaOtros .= "<td><b>" . number_format($manifiesto[5], 0, '', '.') . "</b></td><td><b>" . number_format($manifiesto[22], 0, '', '.') . "</b></td>";
                $salidaOtros .= $objSem->calculoDiferencia(number_format($manifiesto[5], 0, '', '.'), number_format($manifiesto[22], 0, '', '.')) . "</tr>";
                
                $total = $total + $manifiesto[5];
                $totalAnterior = $totalAnterior + $manifiesto[22];
            
            }
        }
    }    
        
    echo $salida . $salidaOtros;
    
}
else{
    echo "";
}


?>
