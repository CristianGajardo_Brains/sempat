<?PHP

session_start();
include ("../../librerias/conexion.php");
require('agenciamiento.class.php');
require('../../clases/sempat.class.php');
$objAgencia = new agenciamiento();
$objSem = new sempat();


$usuarioId = $_SESSION['SEMPAT_usuarioId'];
$clienteId = $_SESSION['SEMPAT_clienteId'];

$campo1 = mb_convert_encoding(trim($_POST['campo1']), "ISO-8859-1", "UTF-8");
$campo2 = mb_convert_encoding(trim($_POST['campo2']), "ISO-8859-1", "UTF-8");
$tipoDato = mb_convert_encoding(trim($_POST['tipoDato']), "ISO-8859-1", "UTF-8");
$nMeses = mb_convert_encoding(trim($_POST['nMeses']), "ISO-8859-1", "UTF-8");
$trId = mb_convert_encoding(trim($_POST['trId']), "ISO-8859-1", "UTF-8");


$consultaTabla = $objAgencia->agenciamientoMostrarNivelIII($campo1, $campo2, $usuarioId);


$campo3 = "";
$valorCampo3 = "";
$salida = "";
$salidaOtros = "";
$total = 0;
$totalAnterior = 0;

$fila = 0;


if ($consultaTabla) {
    
    $numFilas = mssql_num_rows($consultaTabla);

    while ($manifiesto = mssql_fetch_array($consultaTabla)) {
        
        $fila++;

        $campo3 = htmlentities($manifiesto[9]);

        if(strlen($campo3) > 20){
            $valorCampo3 = substr($campo3, 0, 18) . "...";
        }
        else{
            $valorCampo3 = $campo3;
        }

        if($campo3 != "OTROS"){

            $salida .= "<tr id='" . $trId . "-". $fila ."' class='nivel3'><td class='check'><input class='styled' type='checkbox' onClick='graficoAgregarReporte(this);'></td><td class='nivel3' title =\"" . $campo3 . "\">" . $valorCampo3 . "</td>";        

            for ($i = 0; $i < $nMeses; $i++) {

                $salida .= "<td>" . number_format($manifiesto[10 + $i], 0, '', '.') . "</td>";
                $meses[$i][$fila] .= $manifiesto[10 + $i];
            }

            $salida .= "<td><b>" . number_format($manifiesto[6], 0, '', '.') . "</b></td><td><b>" . number_format($manifiesto[22], 0, '', '.') . "</b></td>";                                    
            $salida .= $objSem->calculoDiferencia($manifiesto[6], $manifiesto[22]) . "</tr>";
            
            $total = $total + $manifiesto[6];
            $totalAnterior = $totalAnterior + $manifiesto[22];
                        
        }
        else{
            
            if($manifiesto[22] != ""){
                $salidaOtros .= "<tr id='" . $trId . "-". $fila ."' class='nivel3'><td class='check'><input type='checkbox' class='styled' onClick='graficoAgregarReporte(this);'/></td><td class='nivel3' title =\"" . $campo3 . "\">" . $valorCampo3 . "</td>";        

                for ($i = 0; $i < $nMeses; $i++) {

                    $salidaOtros .= "<td>" . number_format($manifiesto[10 + $i], 0, '', '.') . "</td>";
                    $meses[$i][$numFilas] .= $manifiesto[10 + $i];
                }

                $salidaOtros .= "<td><b>" . number_format($manifiesto[6], 0, '', '.') . "</b></td><td><b>" . number_format($manifiesto[22], 0, '', '.') . "</b></td>";
                $salidaOtros .= $objSem->calculoDiferencia(number_format($manifiesto[6], 0, '', '.'), number_format($manifiesto[22], 0, '', '.')) . "</tr>";
                
                $total = $total + $manifiesto[6];
                $totalAnterior = $totalAnterior + $manifiesto[22];
            
            }
        }
    }    
        
    echo $salida . $salidaOtros;
    
}
else{
    echo "";
}


?>
