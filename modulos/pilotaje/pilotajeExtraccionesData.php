<?PHP

session_start();
include ("../../librerias/conexion.php");
require('pilotaje.class.php');
$objPilotaje = new pilotaje();


$usuarioId = $_SESSION['SEMPAT_usuarioId'];
$clienteId = $_SESSION['SEMPAT_clienteId'];

$fechaDesde = mb_convert_encoding(trim($_POST['fechaDesde']), "ISO-8859-1", "UTF-8");
$fechaHasta = mb_convert_encoding(trim($_POST['fechaHasta']), "ISO-8859-1", "UTF-8");
$carpeta = mb_convert_encoding(trim($_POST['carpeta']), "ISO-8859-1", "UTF-8");


$nombreArchivo = mb_convert_encoding(trim($_POST['nombreArchivo']), "ISO-8859-1", "UTF-8");

$tipoDato = mb_convert_encoding(trim($_POST['tipoDato']), "ISO-8859-1", "UTF-8");

$puertoAsignado = mb_convert_encoding(trim($_POST['puertoAsignado']), "ISO-8859-1", "UTF-8");
$puertoProcedencia = mb_convert_encoding(trim($_POST['puertoProcedencia']), "ISO-8859-1", "UTF-8");
$puertoDestino = mb_convert_encoding(trim($_POST['puertoDestino']), "ISO-8859-1", "UTF-8");
$puertoDesde = mb_convert_encoding(trim($_POST['puertoDesde']), "ISO-8859-1", "UTF-8");
$puertoHasta = mb_convert_encoding(trim($_POST['puertoHasta']), "ISO-8859-1", "UTF-8");
$nave = mb_convert_encoding(trim($_POST['nave']), "ISO-8859-1", "UTF-8");
$tipoNave = mb_convert_encoding(trim($_POST['tipoNave']), "ISO-8859-1", "UTF-8");
$tipoPilotaje = mb_convert_encoding(trim($_POST['tipoPilotaje']), "ISO-8859-1", "UTF-8");
$agentePortuario = mb_convert_encoding(trim($_POST['agentePortuario']), "ISO-8859-1", "UTF-8");
$naviera = mb_convert_encoding(trim($_POST['naviera']), "ISO-8859-1", "UTF-8");
$tramo = mb_convert_encoding(trim($_POST['tramo']), "ISO-8859-1", "UTF-8");

$chkpuertoAsignado = mb_convert_encoding(trim($_POST['chkpuertoAsignado']), "ISO-8859-1", "UTF-8");
$chkpuertoProcedencia = mb_convert_encoding(trim($_POST['chkpuertoProcedencia']), "ISO-8859-1", "UTF-8");
$chkpuertoDestino = mb_convert_encoding(trim($_POST['chkpuertoDestino']), "ISO-8859-1", "UTF-8");
$chkpuertoDesde = mb_convert_encoding(trim($_POST['chkpuertoDesde']), "ISO-8859-1", "UTF-8");
$chkpuertoHasta = mb_convert_encoding(trim($_POST['chkpuertoHasta']), "ISO-8859-1", "UTF-8");
$chknave = mb_convert_encoding(trim($_POST['chknave']), "ISO-8859-1", "UTF-8");
$chktipoNave = mb_convert_encoding(trim($_POST['chktipoNave']), "ISO-8859-1", "UTF-8");
$chktipoPilotaje = mb_convert_encoding(trim($_POST['chktipoPilotaje']), "ISO-8859-1", "UTF-8");
$chkagentePortuario = mb_convert_encoding(trim($_POST['chkagentePortuario']), "ISO-8859-1", "UTF-8");
$chknaviera = mb_convert_encoding(trim($_POST['chknaviera']), "ISO-8859-1", "UTF-8");
$chktramo = mb_convert_encoding(trim($_POST['chktramo']), "ISO-8859-1", "UTF-8");


$puertoAsignado = str_replace("\'", "'", $puertoAsignado);
$puertoProcedencia = str_replace("\'", "'", $puertoProcedencia);
$puertoDestino = str_replace("\'", "'", $puertoDestino);
$puertoDesde = str_replace("\'", "'", $puertoDesde);
$puertoHasta = str_replace("\'", "'", $puertoHasta);
$nave = str_replace("\'", "'", $nave);
$tipoNave = str_replace("\'", "'", $tipoNave);
$tipoPilotaje = str_replace("\'", "'", $tipoPilotaje);
$agentePortuario = str_replace("\'", "'", $agentePortuario);
$naviera = str_replace("\'", "'", $naviera);
$tramo = str_replace("\'", "'", $tramo);


$uploaddir = "../../Reportes/" . $carpeta . "/";
			
if (file_exists($uploaddir)==false) {
    mkdir($uploaddir, 0700, true);
}

$uploaddir = $uploaddir . "/" . $usuarioId . "/";

if (file_exists($uploaddir)==false) {
    mkdir($uploaddir, 0700, true);
}

$objPilotaje->pilotajeExtracciones($fechaDesde, $fechaHasta, $tipoDato, $clienteId, $puertoAsignado, $puertoProcedencia, $puertoDestino, $puertoDesde, $puertoHasta, $nave, $tipoNave, $tipoPilotaje, $agentePortuario, $naviera, $tramo, $chkpuertoAsignado, $chkpuertoProcedencia, $chkpuertoDestino, $chkpuertoDesde, $chkpuertoHasta, $chknave, $chktipoNave, $chktipoPilotaje, $chkagentePortuario, $chknaviera, $chktramo, $nombreArchivo, $usuarioId);

?>
