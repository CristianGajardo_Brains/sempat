<?php

class pilotaje {
    
    
    function ultimaFechaManifiesto($clienteId) {
        $query = "Exec seleccionarFechasLimites '" . $clienteId . "','M'";
        $result = sql_db::sql_query($query);
        sql_db::sql_close();
        $row = sql_db::sql_fetch_assoc($result);
        return $row;
    }
    
    
    function manifiestoFechasDefecto($clienteId) {
        $query = "Exec seleccionarFechasDefecto '" . $clienteId . "','M'";
        $result = sql_db::sql_query($query);
        sql_db::sql_close();
        $row = sql_db::sql_fetch_assoc($result);
        return $row;
    }
    
    
    //INDICADORES
    
    function indicadoresCabeceraDatos($fecha) {
        $query = "Exec manifiestoIndicadoresCabecera '" . $fecha . "'";        
        //echo $query;
        $result = sql_db::sql_query($query);
        $row = sql_db::sql_fetch_assoc($result);
        sql_db::sql_close();
        return $row;
    }
    
    
    function indicadoresCabeceraHtml($valorNivel1, $fecha){
        
        $objSem = new pilotaje();
        $resultado = $objSem->indicadoresCabeceraDatos($fecha);
        
        $titulo1 = "ENERO";
        $titulo2 = "";
        $titulo3 = "";
        $titulo2b = "";
        $titulo3b = "";
        
        if(strtoupper($resultado["mmActual"]) != "ENERO"){
            $titulo1 = "ENERO - " . strtoupper($resultado["mmActual"]);
        }

        if($resultado["mmAnterior"] != "" && $resultado["mmActual"] != ""){
            $titulo2 = $resultado["mmAnterior"] . " - " . $resultado["mmActual"];
        }
        
        if($resultado["mmActual"] != ""){
            $titulo3 = $resultado["mmActual"];
        }
        
        if($resultado["mmActual"] == "DICIEMBRE"){
            $titulo2b = $resultado["yyyyActual"];
        }
        else{
            if($resultado["yyyyAnterior"] != ""){
                $titulo2b = $resultado["yyyyAnterior"] . " - " . ($resultado["yyyyActual"]);
            }            
        }
        
        if($resultado["mmActual"] == "DICIEMBRE") {
            $titulo3b = $resultado["yyyyAnterior"];            
        }
        else {            
            if($resultado["yyyyAnterior"] != ""){
                $titulo3b = ($resultado["yyyyAnterior"] - 1) . " - " . ($resultado["yyyyAnterior"]);                
            }                        
        }
        
        
        $htmlThead = "<tr>
                        <th class=\"thCenter\" colspan=\"6\">
                            <div class=\"divThLeft\"></div>
                            <div class=\"divThCenter\">" . $titulo1 . "</div>
                            <div class=\"divThRight\"></div>          
                        </th>                        
                        <th style=\"min-width: 10px;\"></th>
                        <th class=\"thCenter\" colspan=\"4\">
                            <div class=\"divThLeft\"></div>          
                            <div class=\"divThCenter\">" . $titulo2 . "</div>
                            <div class=\"divThRight\"></div>          
                        </th> 
                        <th style=\"min-width: 10px;\"></th>
                        <th class=\"thCenter\" colspan=\"4\">
                            <div class=\"divThLeft\"></div>          
                            <div class=\"divThCenter\">" . $titulo3 . "</div>
                            <div class=\"divThRight\"></div>          
                        </th> 
                    </tr>   
                    <tr>
                        <td colspan=\"2\" class=\"left\" style=\"width:185px;\">
                                " . mb_convert_encoding(trim($valorNivel1), "UTF-8", "ISO-8859-1") .  "
                        </td>
                        <td>
                            " . $resultado["yyyyActual"] . "
                        </td>
                        <td>
                            " . $resultado["yyyyAnterior"] . "
                        </td>
                        <td>
                            DIF.
                        </td>
                        <td>
                            Mkt. SHARE
                        </td>
                        <td class=\"clear\">

                        </td>
                        <td class=\"left\">
                            " . $titulo2b .  "
                        </td>
                        <td>
                            " . $titulo3b .  "
                        </td>
                        <td>
                            DIF.
                        </td>
                        <td>
                            Mkt. SHARE
                        </td>
                        <td class=\"clear\">

                        </td>
                        <td class=\"left\">
                            " . $resultado["yyyyActual"] . "
                        </td>
                        <td>
                            " . $resultado["yyyyAnterior"] . "
                        </td>
                        <td>
                            DIF.
                        </td>
                        <td>
                            Mkt. SHARE
                        </td>                        
                    </tr>";

        return $htmlThead;       

    }
    
    
    function pilotajeIndicadoresNivel1($fechaDesde, $fechaHasta, $nivel1, $nivel2, $nivel3, $nivel4, $nivel5, $tipoDato, $clienteId, $puertoAsignado, $puertoProcedencia, $puertoDestino, 
            $puertoDesde, $puertoHasta, $nave, $tipoNave, $tipoPilotaje, $agentePortuario, $naviera, $tramo, $usuarioId){
    
        
        /*$query = "Exec pilotajeIndicadoresNivel1 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel1 . "','" . $nivel2 . "','" . $nivel3 . "','" . $nivel4 . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $puertoAsignado . "','" . $puertoProcedencia . "','" . $puertoDestino . "','" . $puertoDesde . "','" . $puertoHasta . "','" . 
                $nave . "','" . $tipoNave . "','" . $tipoPilotaje . "','" . $agentePortuario . "','" . $naviera . "','" . $naviera . "','"  . $usuarioId . "'";        
        echo $query;*/
        
        
        $stmt = mssql_init("pilotajeIndicadoresNivel1");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);
        mssql_bind($stmt, '@var1', $nivel1, SQLVARCHAR);
        mssql_bind($stmt, '@var2', $nivel2, SQLVARCHAR);
        mssql_bind($stmt, '@var3', $nivel3, SQLVARCHAR);
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);        
        mssql_bind($stmt, '@puertoAsignado', $puertoAsignado, SQLVARCHAR);
        mssql_bind($stmt, '@puertoProcedencia', $puertoProcedencia, SQLVARCHAR);
        mssql_bind($stmt, '@puertoDestino', $puertoDestino, SQLVARCHAR);
        mssql_bind($stmt, '@puertoDesde', $puertoDesde, SQLVARCHAR);
        mssql_bind($stmt, '@puertoHasta', $puertoHasta, SQLVARCHAR);
        mssql_bind($stmt, '@nave', $nave, SQLVARCHAR);
        mssql_bind($stmt, '@tipoNave', $tipoNave, SQLVARCHAR);
        mssql_bind($stmt, '@tipoPilotaje', $tipoPilotaje, SQLVARCHAR);
        mssql_bind($stmt, '@agentePortuario', $agentePortuario, SQLVARCHAR);
        mssql_bind($stmt, '@naviera', $naviera, SQLVARCHAR);        
		mssql_bind($stmt, '@tramo', $tramo, SQLVARCHAR);  
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    function pilotajeIndicadoresNivel1Grafico($fechaDesde, $fechaHasta, $nivel1, $usuarioId){
            
        /*$query = "Exec pilotajeIndicadoresNivel1Grafico '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel1 . "','" . $usuarioId . "'";        
        echo $query;*/
               
        $stmt = mssql_init("pilotajeIndicadoresNivel1Grafico");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);
        mssql_bind($stmt, '@var1', $nivel1, SQLVARCHAR);                                            
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
               
    
    function pilotajeIndicadoresNivel2($fechaDesde, $fechaHasta, $nivel2, $nivel3, $nivel4, $nivel5, $tipoDato, $clienteId, $nivel1Id, $otros, $usuarioId){
    
        
        /*$query = "Exec pilotajeIndicadoresNivel2 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel2 . "','" . $nivel3 . "','" . $nivel4 . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $nivel1Id . "','" . $otros . "','" . $usuarioId . "'";                
        echo $query;*/
                
        $stmt = mssql_init("pilotajeIndicadoresNivel2");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);        
        mssql_bind($stmt, '@var2', $nivel2, SQLVARCHAR);
        mssql_bind($stmt, '@var3', $nivel3, SQLVARCHAR);
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@nivel1Id', $nivel1Id, SQLVARCHAR);
        mssql_bind($stmt, '@otros', $otros, SQLVARCHAR);        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    function pilotajeIndicadoresNivel3($fechaDesde, $fechaHasta, $nivel3, $nivel4, $nivel5, $tipoDato, $clienteId, $nivel1Id, $nivel2Id, $otros, $usuarioId){
    
        
        /*$query = "Exec pilotajeIndicadoresNivel3 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel3 . "','" . $nivel4 . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $nivel1Id . "','" . $nivel2Id . "','" . $otros . "','" . $usuarioId . "'";        
        echo $query;*/
                
        $stmt = mssql_init("pilotajeIndicadoresNivel3");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);                
        mssql_bind($stmt, '@var3', $nivel3, SQLVARCHAR);
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@nivel1Id', $nivel1Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel2Id', $nivel2Id, SQLVARCHAR);
        mssql_bind($stmt, '@otros', $otros, SQLVARCHAR);        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    function pilotajeIndicadoresNivel4($fechaDesde, $fechaHasta, $nivel4, $nivel5, $tipoDato, $clienteId, $nivel1Id, $nivel2Id, $nivel3Id, $otros, $usuarioId){
            
        /*$query = "Exec pilotajeIndicadoresNivel4 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel4 . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $nivel1Id . "','" . $nivel2Id . "','" . $nivel3Id . "','" . $otros . "','" . $usuarioId . "'";      
        echo $query;*/
                
        $stmt = mssql_init("pilotajeIndicadoresNivel4");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);                        
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@nivel1Id', $nivel1Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel2Id', $nivel2Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel3Id', $nivel3Id, SQLVARCHAR);
        mssql_bind($stmt, '@otros', $otros, SQLVARCHAR);        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    function pilotajeIndicadoresNivel5($fechaDesde, $fechaHasta, $nivel5, $tipoDato, $clienteId, $nivel1Id, $nivel2Id, $nivel3Id, $nivel4Id, $otros, $usuarioId){
            
        /*$query = "Exec pilotajeIndicadoresNivel5 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $nivel1Id . "','" . $nivel2Id . "','" . $nivel3Id . "','" . $nivel4Id . "','" . $otros . "','" . $usuarioId . "'";        
        echo $query;*/
                
        $stmt = mssql_init("pilotajeIndicadoresNivel5");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);                                
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@nivel1Id', $nivel1Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel2Id', $nivel2Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel3Id', $nivel3Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel4Id', $nivel4Id, SQLVARCHAR);
        mssql_bind($stmt, '@otros', $otros, SQLVARCHAR);        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    
    //REPORTES
    
    function pilotajeReporteNivel1($fechaDesde, $fechaHasta, $nivel1, $nivel2, $nivel3, $nivel4, $nivel5, $tipoDato, $clienteId, $puertoAsignado, $puertoProcedencia, $puertoDestino, 
            $puertoDesde, $puertoHasta, $nave, $tipoNave, $tipoPilotaje, $agentePortuario, $naviera, $tramo, $usuarioId){
    
        /*$query = "Exec pilotajeReporteNivel1 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel1 . "','" . $nivel2 . "','" . $nivel3 . "','" . $nivel4 . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $puertoAsignado . "','" . $puertoProcedencia . "','" . $puertoDestino . "','" . $puertoDesde . "','" . $puertoHasta . "','" . 
                $nave . "','" . $tipoNave . "','" . $tipoPilotaje . "','" . $agentePortuario . "','" . $naviera . "','"  . $usuarioId . "'";        
        echo $query;*/
                        
        
        $stmt = mssql_init("pilotajeReporteNivel1");

        mssql_bind($stmt, '@fechaDesdeA', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHastaA', $fechaHasta, SQLVARCHAR);
        mssql_bind($stmt, '@var1', $nivel1, SQLVARCHAR);
        mssql_bind($stmt, '@var2', $nivel2, SQLVARCHAR);
        mssql_bind($stmt, '@var3', $nivel3, SQLVARCHAR);
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@puertoAsignado', $puertoAsignado, SQLVARCHAR);
        mssql_bind($stmt, '@puertoProcedencia', $puertoProcedencia, SQLVARCHAR);
        mssql_bind($stmt, '@puertoDestino', $puertoDestino, SQLVARCHAR);
        mssql_bind($stmt, '@puertoDesde', $puertoDesde, SQLVARCHAR);
        mssql_bind($stmt, '@puertoHasta', $puertoHasta, SQLVARCHAR);
        mssql_bind($stmt, '@nave', $nave, SQLVARCHAR);
        mssql_bind($stmt, '@tipoNave', $tipoNave, SQLVARCHAR);
        mssql_bind($stmt, '@tipoPilotaje', $tipoPilotaje, SQLVARCHAR);
        mssql_bind($stmt, '@agentePortuario', $agentePortuario, SQLVARCHAR);
        mssql_bind($stmt, '@naviera', $naviera, SQLVARCHAR);                                      
		mssql_bind($stmt, '@tramo', $tramo, SQLVARCHAR);                                      
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
          
    
    function pilotajeReporteNivel2($fechaDesde, $fechaHasta, $nivel2, $nivel3, $nivel4, $nivel5, $tipoDato, $clienteId, $nivel1Id, $otros, $usuarioId){
    
        
        $query = "Exec pilotajeReporteNivel2 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel2 . "','" . $nivel3 . "','" . $nivel4 . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $nivel1Id . "','" . $otros . "','" . $usuarioId . "'";        
        echo $query;
                
        $stmt = mssql_init("pilotajeReporteNivel2");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);        
        mssql_bind($stmt, '@var2', $nivel2, SQLVARCHAR);
        mssql_bind($stmt, '@var3', $nivel3, SQLVARCHAR);
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@nivel1Id', $nivel1Id, SQLVARCHAR);
        mssql_bind($stmt, '@otros', $otros, SQLVARCHAR);        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    function pilotajeReporteNivel3($fechaDesde, $fechaHasta, $nivel3, $nivel4, $nivel5, $tipoDato, $clienteId, $nivel1Id, $nivel2Id, $otros, $usuarioId){
    
        
        /*$query = "Exec pilotajeReporteNivel3 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel3 . "','" . $nivel4 . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $nivel1Id . "','" . $nivel2Id . "','" . $otros . "','" . $usuarioId . "'";        
        echo $query;*/
                
        $stmt = mssql_init("pilotajeReporteNivel3");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);                
        mssql_bind($stmt, '@var3', $nivel3, SQLVARCHAR);
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@nivel1Id', $nivel1Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel2Id', $nivel2Id, SQLVARCHAR);
        mssql_bind($stmt, '@otros', $otros, SQLVARCHAR);        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    function pilotajeReporteNivel4($fechaDesde, $fechaHasta, $nivel4, $nivel5, $tipoDato, $clienteId, $nivel1Id, $nivel2Id, $nivel3Id, $otros, $usuarioId){
            
        /*$query = "Exec pilotajeReporteNivel4 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel4 . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $nivel1Id . "','" . $nivel2Id . "','" . $nivel3Id . "','" . $otros . "','" . $usuarioId . "'";        
        echo $query;*/
                
        $stmt = mssql_init("pilotajeReporteNivel4");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);                        
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@nivel1Id', $nivel1Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel2Id', $nivel2Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel3Id', $nivel3Id, SQLVARCHAR);
        mssql_bind($stmt, '@otros', $otros, SQLVARCHAR);        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    function pilotajeReporteNivel5($fechaDesde, $fechaHasta, $nivel5, $tipoDato, $clienteId, $nivel1Id, $nivel2Id, $nivel3Id, $nivel4Id, $otros, $usuarioId){
            
        /*$query = "Exec pilotajeReporteNivel5 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel5 . "','" . 
                $tipoDato . "','" . $clienteId . "','" . $nivel1Id . "','" . $nivel2Id . "','" . $nivel3Id . "','" . $nivel4Id . "','" . $otros . "','" . $usuarioId . "'";        
        echo $query;*/
                
        $stmt = mssql_init("pilotajeReporteNivel5");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);                                
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@nivel1Id', $nivel1Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel2Id', $nivel2Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel3Id', $nivel3Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel4Id', $nivel4Id, SQLVARCHAR);
        mssql_bind($stmt, '@otros', $otros, SQLVARCHAR);        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    //EXTRACCIONES
    
    function pilotajeExtracciones($fechaDesde, $fechaHasta, $tipoDato, $clienteId, $puertoAsignado, $puertoProcedencia, $puertoDestino, 
            $puertoDesde, $puertoHasta, $nave, $tipoNave, $tipoPilotaje, $agentePortuario, $naviera, $tramo,            
            $chkpuertoAsignado, $chkpuertoProcedencia, $chkpuertoDestino, $chkpuertoDesde, $chkpuertoHasta, $chknave, $chktipoNave, $chktipoPilotaje, $chkagentePortuario, 
            $chknaviera, $chktramo, $nombreArchivo, $usuarioId){
                   
        /*$query = "Exec pilotajeExtracciones '" . $fechaDesde . "','" . $fechaHasta . "','" . $tipoDato . "','" . $clienteId . "','" . $puertoAsignado . "','" . 
				$puertoProcedencia . "','" . $puertoDestino . "','" . $puertoDesde . "','" . $puertoHasta . "','" . 
                $nave . "','" . $tipoNave . "','" . $tipoPilotaje . "','" . $agentePortuario . "','" . $naviera . "','"  .                
                $chkpuertoAsignado . "','" . $chkpuertoProcedencia . "','" . $chkpuertoDestino . "','" . $chkpuertoDesde . "','" . $chkpuertoHasta . "','" . 
                $chknave . "','" . $chktipoNave . "','" . $chktipoPilotaje . "','" . $chkagentePortuario . "','" . $chknaviera . "','"  . $usuarioId . "'";        
        echo $query;*/

        $stmt = mssql_init("pilotajeExtracciones");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);        
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        //mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);        
        mssql_bind($stmt, '@puertoAsignado', $puertoAsignado, SQLVARCHAR);
        mssql_bind($stmt, '@puertoProcedencia', $puertoProcedencia, SQLVARCHAR);
        mssql_bind($stmt, '@puertoDestino', $puertoDestino, SQLVARCHAR);
        mssql_bind($stmt, '@puertoDesde', $puertoDesde, SQLVARCHAR);
        mssql_bind($stmt, '@puertoHasta', $puertoHasta, SQLVARCHAR);
        mssql_bind($stmt, '@nave', $nave, SQLVARCHAR);
        mssql_bind($stmt, '@tipoNave', $tipoNave, SQLVARCHAR);
        mssql_bind($stmt, '@tipoPilotaje', $tipoPilotaje, SQLVARCHAR);
        mssql_bind($stmt, '@agentePortuario', $agentePortuario, SQLVARCHAR);
        mssql_bind($stmt, '@naviera', $naviera, SQLVARCHAR);                                                   
		mssql_bind($stmt, '@tramo', $tramo, SQLVARCHAR);                                                   
                
        mssql_bind($stmt, '@chkpuertoAsignado', $chkpuertoAsignado, SQLVARCHAR);
        mssql_bind($stmt, '@chkpuertoProcedencia', $chkpuertoProcedencia, SQLVARCHAR);
        mssql_bind($stmt, '@chkpuertoDestino', $chkpuertoDestino, SQLVARCHAR);
        mssql_bind($stmt, '@chkpuertoDesde', $chkpuertoDesde, SQLVARCHAR);
        mssql_bind($stmt, '@chkpuertoHasta', $chkpuertoHasta, SQLVARCHAR);
        mssql_bind($stmt, '@chknave', $chknave, SQLVARCHAR);
        mssql_bind($stmt, '@chktipoNave', $chktipoNave, SQLVARCHAR);
        mssql_bind($stmt, '@chktipoPilotaje', $chktipoPilotaje, SQLVARCHAR);
        mssql_bind($stmt, '@chkagentePortuario', $chkagentePortuario, SQLVARCHAR);
        mssql_bind($stmt, '@chknaviera', $chknaviera, SQLVARCHAR);                                
		mssql_bind($stmt, '@chktramo', $chktramo, SQLVARCHAR);                                
        
        mssql_bind($stmt, '@nombreArchivo', $nombreArchivo, SQLVARCHAR);                                    
        
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;         
    
    }
    
    
    function pilotajeExtraccionesArchivos($usuarioId, $moduloId) {
        $query = "Select * From Extraccion_Reportes (nolock) Where usuarioId = '" . $usuarioId . "' and modulo = '" . $moduloId . "' Order by fecha_reporte desc";
        //echo $query;
        $result = sql_db::sql_query($query);
        sql_db::sql_close();        
        return $result;
    }
    
    function pilotajeExtraccionesSeleccionarArchivo($usuarioId, $moduloId, $arhivoId) {
        $query = "Select nombre_archivo From Extraccion_Reportes (nolock) Where usuarioId = '" . $usuarioId . "' and modulo = '" . $moduloId . "' and id_archivo = '" . $arhivoId . "'";                
        $result = sql_db::sql_query($query);
        $row = sql_db::sql_fetch_assoc($result);
        sql_db::sql_close();        
        return $row;
    }
    
    function pilotajeExtraccionesEliminarArchivo($usuarioId, $moduloId, $arhivoId) {        
        $query = "Delete e From Extraccion_Reportes e Where usuarioId = '" . $usuarioId . "' and modulo = '" . $moduloId . "' and id_archivo = '" . $arhivoId . "'";        
        sql_db::sql_query($query);        
        sql_db::sql_close();        
        return true;
    }
    
    
}

?>


