

function buscarExtracciones(){
    
    var nombreArchivo = $("#inputNombreArchivo").val();
    var carpeta = $("#carpeta").val();

    var fechaDesde = $("#fechaDesde").val();
    var fechaHasta = $("#fechaHasta").val();
    
    
    if(fechaDesde == "" && fechaHasta == ""){

        fechasDefecto("PILOTAJE");
        fechaDesde = $("#fechaDesde").val();
        fechaHasta = $("#fechaHasta").val();
    }
    
    if(validarFormulario()){

        $("#divResultado").html("");  

        $("#imgCargando").css("display","block");
        //$("#divPanelFiltros").css("display","none");            
        //$("#divMensaje").css("display","none");

        var puertoAsignado = $("#puertoAsignado").val();
        var puertoProcedencia = $("#puertoProcedencia").val();
        var puertoDestino = $("#puertoDestino").val();
        var puertoDesde = $("#puertoDesde").val();
        var puertoHasta = $("#puertoHasta").val();
        var nave = $("#nave").val();
        var tipoNave = $("#tipoNave").val();
        var tipoPilotaje = $("#tipoPilotaje").val();
        var agentePortuario = $("#agentePortuario").val();
        var naviera = $("#naviera").val();    
		var tramo = $("#tramo").val();    

        var chkpuertoAsignado = $("#chkpuertoAsignado").attr('checked');
        var chkpuertoProcedencia = $("#chkpuertoProcedencia").attr('checked');
        var chkpuertoDestino = $("#chkpuertoDestino").attr('checked');
        var chkpuertoDesde = $("#chkpuertoDesde").attr('checked');
        var chkpuertoHasta = $("#chkpuertoHasta").attr('checked');
        var chknave = $("#chknave").attr('checked');
        var chktipoNave = $("#chktipoNave").attr('checked');
        var chktipoPilotaje = $("#chktipoPilotaje").attr('checked');
        var chkagentePortuario = $("#chkagentePortuario").attr('checked');
        var chknaviera = $("#chknaviera").attr('checked');
		var chktramo = $("#chktramo").attr('checked');
                        

        var tipoDato = $("input[name=tipoDato]:checked").val();

        var etapa = $("input[name=btnEtapa]:checked").val();
                
        $.ajax({
            type: "POST",
            url: "pilotajeExtraccionesData.php",
            data: { fechaDesde: fechaDesde, fechaHasta: fechaHasta, puertoAsignado: puertoAsignado, puertoProcedencia: puertoProcedencia, puertoDestino: puertoDestino, 
                puertoDesde: puertoDesde, puertoHasta: puertoHasta, nave: nave, tipoNave: tipoNave, tipoPilotaje: tipoPilotaje, agentePortuario: agentePortuario, naviera: naviera, tramo: tramo, 

                chkpuertoAsignado: chkpuertoAsignado, chkpuertoProcedencia: chkpuertoProcedencia, chkpuertoDestino: chkpuertoDestino, 
                chkpuertoDesde: chkpuertoDesde, chkpuertoHasta: chkpuertoHasta, chknave: chknave, chktipoNave: chktipoNave, chktipoPilotaje: chktipoPilotaje, chktramo: chktramo,
                chkagentePortuario: chkagentePortuario, chknaviera: chknaviera, 

                nombreArchivo: nombreArchivo,tipoDato:tipoDato, carpeta:carpeta}
            })
            .done(function( msg ) {
                buscarExtraccionesArchivos($("#moduloId").val());
                $("#imgCargando").css("display","none");                                           

        }); 

    }                                                                    

}  
  

function validarFormulario(){

    var mensaje = "";

    var nombreArchivo = $("#inputNombreArchivo").val();
    var fechaDesde = $("#fechaDesde").val();
    var fechaHasta = $("#fechaHasta").val();                                 
            
    if(nombreArchivo == ""){
        mensaje = "Debe ingresar un nombre para el archivo.<br>";
    }
    
    if(fechaDesde == ""){
        mensaje += "No ha ingresado la fecha inicial.<br>";
    }
    
    if(fechaHasta == ""){
        mensaje += "No ha ingresado la fecha final.<br>";
    }
    
    if(fechaDesde != "" && fechaHasta != "" && validarFechaMenor(fechaDesde, fechaHasta) == false){
        mensaje += "La fecha incial no puede ser mayor a la fecha final.<br>";
    }
    
    if(fechaDesde != "" && fechaHasta != "" && fechaDiferenciasDias(fechaDesde, fechaHasta) > 365){
        mensaje += "La diferencia entre las fechas no puede ser superior a un año.<br>";
    }
            
    if(mensaje != ""){
        $("#divMensaje").html(mensaje);        
        return false;
    }
    else{
        $("#divMensaje").html("");        
        return true;
    }

}
    
    
    