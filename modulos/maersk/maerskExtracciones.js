

function buscarExtracciones(){
    
    var nombreArchivo = $("#inputNombreArchivo").val();
    var carpeta = $("#carpeta").val();
    
    var fechaDesde = $("#fechaDesde").val();
    var fechaHasta = $("#fechaHasta").val();
    
    var mercado = "";

    var chile = $("input[id=btnChile]:checked").val();
    var peru = $("input[id=btnPeru]:checked").val();
    var ecuador = $("input[id=btnEcuador]:checked").val();
    var colombia = $("input[id=btnColombia]:checked").val();


    mercado = chile + "," + peru + "," + ecuador + "," + colombia;                                                         
    mercado = replaceAll(mercado, ",undefined",""); 
    mercado = replaceAll(mercado, "undefined",""); 

    if(chile == undefined){
        mercado = mercado.substring(1, mercado.length);
    }

    if(fechaDesde == "" && fechaHasta == ""){

        if(mercado == "1"){
			fechasDefecto("MAERSK_CHILE");
		}
		else if(mercado == "2"){
			fechasDefecto("MAERSK_PERU");
		}
		else if(mercado == "3"){
			fechasDefecto("MAERSK_ECUADOR");
		}
		else {
			fechasDefecto("MAERSK");
		}
	
        fechaDesde = $("#fechaDesde").val();
        fechaHasta = $("#fechaHasta").val();    
    }
    
    if(validarFormulario()){

        $("#divResultado").html("");  

        $("#imgCargando").css("display","block");
        //$("#divPanelFiltros").css("display","none");            
        //$("#divMensaje").css("display","none");

        var portCluster = $("#portCluster").val();
        var porCountry = $("#porCountry").val();
        var porCity = $("#porCity").val();
        var lopfiCluster = $("#lopfiCluster").val();
        var lopfiCountry = $("#lopfiCountry").val();
        var lopCity = $("#lopCity").val();
        var diplaCountry = $("#diplaCountry").val();
        var diplaCity = $("#diplaCity").val();
        var podCountry = $("#podCountry").val();
        var podCity = $("#podCity").val();
				
        var naviera = $("#naviera").val();
        var nave = $("#nave").val();
        var shipper = $("#shipper").val();
        var consignee = $("#consignee").val();
        var ffww = $("#ffww").val();
        var notify = $("#notify").val();
        var customerCity = $("#customerCity").val();
		
        var ruta = $("#ruta").val();
        var trade = $("#trade").val();
        var subTrade = $("#subTrade").val();
        var item = $("#item").val();
        var itemCode = $("#itemCode").val();
        var btn = $("#btn").val();
        var commodity = $("#commodity").val();
		var cargoType = $("#cargoType").val();
		
		var ruta3 = $("#ruta3").val();
		var grupoNaviera = $("#grupoNaviera").val();

        var chkportCluster = $("#chkportCluster").attr('checked');
        var chkporCountry = $("#chkporCountry").attr('checked');
        var chkporCity = $("#chkporCity").attr('checked');
        var chklopfiCluster = $("#chklopfiCluster").attr('checked');
        var chklopfiCountry = $("#chklopfiCountry").attr('checked');
        var chklopCity = $("#chklopCity").attr('checked');
        var chkdiplaCountry = $("#chkdiplaCountry").attr('checked');
        var chkdiplaCity = $("#chkdiplaCity").attr('checked');
        var chkpodCountry = $("#chkpodCountry").attr('checked');
        var chkpodCity = $("#chkpodCity").attr('checked');
				
        var chknaviera = $("#chknaviera").attr('checked');
        var chknave = $("#chknave").attr('checked');
        var chkshipper = $("#chkshipper").attr('checked');
        var chkconsignee = $("#chkconsignee").attr('checked');
        var chkffww = $("#chkffww").attr('checked');
        var chknotify = $("#chknotify").attr('checked');
        var chkcustomerCity = $("#chkcustomerCity").attr('checked');
		
        var chkruta = $("#chkruta").attr('checked');
        var chktrade = $("#chktrade").attr('checked');
        var chksubTrade = $("#chksubTrade").attr('checked');
        var chkitem = $("#chkitem").attr('checked');
        var chkitemCode = $("#chkitemCode").attr('checked');
        var chkbtn = $("#chkbtn").attr('checked');
        var chkcommodity = $("#chkcommodity").attr('checked');
		var chkcargoType = $("#chkcargoType").attr('checked');
		
		var chkruta3 = $("#chkruta3").attr('checked');
		var chkgrupoNaviera = $("#chkgrupoNaviera").attr('checked');

        var tipoDato = "";
        
        if($("#btnContenedores").prop('checked')){
            tipoDato = tipoDato + "contenedores," 
        }        
        
        if($("#btnTeus").prop('checked')){
            tipoDato = tipoDato + "teus," 
        } 
        
        if($("#btnToneladas").prop('checked')){
            tipoDato = tipoDato + "toneladas," 
        }                                        
		
		if($("#btnFob").prop('checked')){
            tipoDato = tipoDato + "fob," 
        } 
		
		if($("#btnCif").prop('checked')){
            tipoDato = tipoDato + "cif," 
        } 
		
		if($("#btnCif").prop('checked')){
            tipoDato = tipoDato + "cif," 
        } 
		
		if($("#btnFeus").prop('checked')){
            tipoDato = tipoDato + "feus," 
        } 
		

        var etapa = $("input[name=btnEtapa]:checked").val();
        var contenedor = $("input[name=btnContenedor]:checked").val();
        var dryReefer = $("input[name=btnSecaRefrigerada]:checked").val();
        

        $.ajax({
            type: "POST",
            url: "maerskExtraccionesData.php",
            data: { fechaDesde: fechaDesde, fechaHasta: fechaHasta, 
					portCluster: portCluster, porCountry: porCountry, porCity: porCity, lopfiCluster: lopfiCluster, lopfiCountry: lopfiCountry,
					lopCity: lopCity, diplaCountry: diplaCountry, diplaCity: diplaCity, podCountry: podCountry, podCity: podCity,
					naviera: naviera, nave: nave, shipper: shipper, consignee: consignee, ffww: ffww, notify: notify, customerCity: customerCity,
					ruta: ruta, trade: trade, subTrade: subTrade, item: item, itemCode: itemCode, btn: btn, commodity: commodity, cargoType: cargoType,
					ruta3: ruta3, grupoNaviera: grupoNaviera, 

					chkportCluster: chkportCluster, chkporCountry: chkporCountry, chkporCity: chkporCity, chklopfiCluster: chklopfiCluster, chklopfiCountry: chklopfiCountry,
					chklopCity: chklopCity, chkdiplaCountry: chkdiplaCountry, chkdiplaCity: chkdiplaCity, chkpodCountry: chkpodCountry, chkpodCity: chkpodCity,
					chknaviera: chknaviera, chknave: chknave, chkshipper: chkshipper, chkconsignee: chkconsignee, chkffww: chkffww, chknotify: chknotify, chkcustomerCity: chkcustomerCity,
					chkruta: chkruta, chktrade: chktrade, chksubTrade: chksubTrade, chkitem: chkitem, chkitemCode: chkitemCode, chkbtn: chkbtn, chkcommodity: chkcommodity, chkcargoType: chkcargoType,
					chkruta3: chkruta3, chkgrupoNaviera: chkgrupoNaviera,

					nombreArchivo: nombreArchivo,tipoDato:tipoDato, mercado: mercado, etapa: etapa, contenedor: contenedor, dryReefer: dryReefer, carpeta:carpeta}
            })
            .done(function( msg ) {
                buscarExtraccionesArchivos($("#moduloId").val());
                $("#imgCargando").css("display","none");                                           

        }); 

    }                                                                    

}  


function validarFormulario(){

    var mensaje = "";

    var nombreArchivo = $("#inputNombreArchivo").val();
    var fechaDesde = $("#fechaDesde").val();
    var fechaHasta = $("#fechaHasta").val();                                 
            
    if(nombreArchivo == ""){
        mensaje = "Debe ingresar un nombre para el archivo.<br>";
    }
    
    if(fechaDesde == ""){
        mensaje += "No ha ingresado la fecha inicial.<br>";
    }
    
    if(fechaHasta == ""){
        mensaje += "No ha ingresado la fecha final.<br>";
    }
    
    if(fechaDesde != "" && fechaHasta != "" && validarFechaMenor(fechaDesde, fechaHasta) == false){
        mensaje += "La fecha incial no puede ser mayor a la fecha final.<br>";
    }
    
    if(fechaDesde != "" && fechaHasta != "" && fechaDiferenciasDias(fechaDesde, fechaHasta) > 365){
        mensaje += "La diferencia entre las fechas no puede ser superior a un año.<br>";
    }
            
    if(mensaje != ""){
        $("#divMensaje").html(mensaje);        
        return false;
    }
    else{
        $("#divMensaje").html("");        
        return true;
    }

}
    
    
    