
var count = 0;


function filtros() {

    var filtros = '';
    var mensaje = '';
    count = 0;

    filtros += filtrosValores($("#portCluster"), "Port Cluster");
    filtros += filtrosValores($("#porCountry"), "Port Country");
    filtros += filtrosValores($("#porCity"), "Port City");
    filtros += filtrosValores($("#lopfiCluster"), "Lopfi Cluster");
    filtros += filtrosValores($("#lopfiCountry"), "Lopfi Country");
    filtros += filtrosValores($("#lopCity"), "Lop City");
    filtros += filtrosValores($("#diplaCountry"), "Dipla Country");
    filtros += filtrosValores($("#diplaCity"), "Dipla City");
    filtros += filtrosValores($("#podCountry"), "Pod Country");
    filtros += filtrosValores($("#podCity"), "podCity");

    filtros += filtrosValores($("#naviera"), "Cía. Naviera");
    filtros += filtrosValores($("#nave"), "Nave");
    filtros += filtrosValores($("#shipper"), "Shipper");
    filtros += filtrosValores($("#consignee"), "Consignee");
    filtros += filtrosValores($("#ffww"), "FFWW");
    filtros += filtrosValores($("#notify"), "Notify");
    filtros += filtrosValores($("#customerCity"), "Customer City");

    filtros += filtrosValores($("#ruta"), "Ruta");
    filtros += filtrosValores($("#trade"), "Trade");
    filtros += filtrosValores($("#subTrade"), "Sub Trade");
    filtros += filtrosValores($("#item"), "Item Nombre");
    filtros += filtrosValores($("#itemCode"), "Item Code");
    filtros += filtrosValores($("#btn"), "Btn");
    filtros += filtrosValores($("#commodity"), "commodity");
    filtros += filtrosValores($("#cargoType"), "Cargo Type");

    filtros += filtrosValores($("#route3"), "Route 3");
    filtros += filtrosValores($("#grupoNaviera"), "Carrier Group");

    if (count > 0) {
        mensaje = "<font color='#3D85FE' size='2'>Filtros:</font><br>"
    }

    if (count > 4) {
        mensaje += "<div style='height:60px; overflow-y: scroll'>" + filtros + "</div>";

    } else {
        mensaje += "<div style='height:60px'>" + filtros + "</div>";
    }

    return  mensaje;


}


function buscarNivel1() {

    var nivel1 = $("#selectNivel1").val();
    var nivel2 = $("#selectNivel2").val();
    var nivel3 = $("#selectNivel3").val();
    var nivel4 = $("#selectNivel4").val();
    var nivel5 = $("#selectNivel5").val();

    var mercado = "";

    var chile = $("input[id=btnChile]:checked").val();
    var peru = $("input[id=btnPeru]:checked").val();
    var ecuador = $("input[id=btnEcuador]:checked").val();
    var colombia = $("input[id=btnColombia]:checked").val();


    mercado = chile + "," + peru + "," + ecuador + "," + colombia;
    mercado = replaceAll(mercado, ",undefined", "");
    mercado = replaceAll(mercado, "undefined", "");

    if (chile == undefined) {
        mercado = mercado.substring(1, mercado.length);
    }


    var fechaDesde = $("#fechaDesde").val();
    var fechaHasta = $("#fechaHasta").val();


    if (fechaDesde == "" && fechaHasta == "") {

        if (mercado == "1") {
            fechasDefecto("MAERSK_CHILE");
        } else if (mercado == "2") {
            fechasDefecto("MAERSK_PERU");
        } else if (mercado == "3") {
            fechasDefecto("MAERSK_ECUADOR");
        } else {
            fechasDefecto("MAERSK");
        }

        fechaDesde = $("#fechaDesde").val();
        fechaHasta = $("#fechaHasta").val();

        //fechaDesde = $("#fechaDesdeDef").val();
        //fechaHasta = $("#fechaHastaDef").val();
        //$("#fechaDesde").val(fechaDesde);
        //$("#fechaHasta").val(fechaHasta);

    }


    if (validarFormulario()) {

        $("#divResultado").html("");
        $("#tablaNivelesDesc").html("");

        $("#imgCargando").css("display", "block");
        $("#divPanelFiltros").css("display", "none");
        $("#divOpciones").css("display", "none");
        $("#divGraficos").css("display", "none");
        $("#divGrafico").css("display", "none");
        $("#divGraficoA").css("display", "none");
        $("#divGraficoB").css("display", "none");
        $("#divMensaje").css("display", "none");

        var portCluster = $("#portCluster").val();
        var porCountry = $("#porCountry").val();
        var porCity = $("#porCity").val();
        var lopfiCluster = $("#lopfiCluster").val();
        var lopfiCountry = $("#lopfiCountry").val();
        var lopCity = $("#lopCity").val();
        var diplaCountry = $("#diplaCountry").val();
        var diplaCity = $("#diplaCity").val();
        var podCountry = $("#podCountry").val();
        var podCity = $("#podCity").val();

        var naviera = $("#naviera").val();
        var nave = $("#nave").val();
        var shipper = $("#shipper").val();
        var consignee = $("#consignee").val();
        var ffww = $("#ffww").val();
        var notify = $("#notify").val();
        var customerCity = $("#customerCity").val();

        var ruta = $("#ruta").val();
        var trade = $("#trade").val();
        var subTrade = $("#subTrade").val();
        var item = $("#item").val();
        var itemCode = $("#itemCode").val();
        var btn = $("#btn").val();
        var commodity = $("#commodity").val();
        var cargoType = $("#cargoType").val();

        var ruta3 = $("#ruta3").val();
        var grupoNaviera = $("#grupoNaviera").val();

        var nivel1Titulo = $("#selectNivel1 option:selected").text();

        $('#selectTipoReporte').val("");
        $('#selectselectTipoReporte').html("Ambos");

        $('#selectGraficoReporte').val("line");
        $('#selectselectGrafico').html("Líneas");

        var tipoDato = $("input[name=tipoDato]:checked").val();

        switch (tipoDato) {
            case 'contenedores':
                $('#tituloGrafico2').val("N° de Contenedores (C20 + C40)");
                $('#tituloGraficoEjeY').val("N° de Contenedores (C20 + C40)");
                $('.tdTipo').css("display", "");
                break;

            case 'teus':
                $('#tituloGrafico2').val("N° de TEUs");
                $('#tituloGraficoEjeY').val("N° de TEUs");
                $('.tdTipo').css("display", "none");
                break;

            case 'toneladas':
                $('#tituloGrafico2').val("N° de Toneladas");
                $('#tituloGraficoEjeY').val("N° de Toneladas");
                $('.tdTipo').css("display", "none");
                break;

            case 'fob':
                $('#tituloGrafico2').val("FOB (en miles de USD)");
                $('#tituloGraficoEjeY').val("USD");
                $('.tdTipo').css("display", "none");
                break;

            case 'cif':
                $('#tituloGrafico2').val("CIF (en miles de USD)");
                $('#tituloGraficoEjeY').val("USD");
                $('.tdTipo').css("display", "none");
                break;

            case 'feus':
                $('#tituloGrafico2').val("N° de FFEs");
                $('#tituloGraficoEjeY').val("N° de FFEs");
                $('.tdTipo').css("display", "none");
                break;
        }


        var etapa = $("input[name=btnEtapa]:checked").val();
        var contenedor = $("input[name=btnContenedor]:checked").val();
        var dryReefer = $("input[name=btnSecaRefrigerada]:checked").val();

        var tituloGrafico2 = "";

        switch (etapa) {
            case undefined:
                tituloGrafico2 = 'Exportaciones e Importaciones'
                break;

            case '1':
                tituloGrafico2 = 'Exportaciones'
                break;

            case '2':
                tituloGrafico2 = 'Importaciones'
                break;
        }



        if (mercado == '' || mercado == '1,2,3,4') {
            tituloGrafico2 += ' (WCSA)'
        } else {
            tituloGrafico2 += ' (' + replaceAll(mercado, ",", " - ") + ')'
            tituloGrafico2 = tituloGrafico2.replace("1", "Chile");
            tituloGrafico2 = tituloGrafico2.replace("2", "Perú");
            tituloGrafico2 = tituloGrafico2.replace("3", "Ecuador");
            tituloGrafico2 = tituloGrafico2.replace("4", "Colombia");
        }

        $('#tituloGrafico').val(tituloGrafico2);

        $('#tdResumen').html("<center><font color='#3D85FE' size='3'>" + tituloGrafico2 + "</font><br>" + $('#tituloGrafico2').val() + "</center>" + filtros());


        $.ajax({
            type: "POST",
            url: "maerskReporteNivel1.php",
            //async: false,
            data: {fechaDesde: fechaDesde, fechaHasta: fechaHasta, contenedor: contenedor,
                portCluster: portCluster, porCountry: porCountry, porCity: porCity, lopfiCluster: lopfiCluster, lopfiCountry: lopfiCountry,
                lopCity: lopCity, diplaCountry: diplaCountry, diplaCity: diplaCity, podCountry: podCountry, podCity: podCity,
                naviera: naviera, nave: nave, shipper: shipper, consignee: consignee, ffww: ffww, notify: notify, customerCity: customerCity,
                ruta: ruta, trade: trade, subTrade: subTrade, item: item, itemCode: itemCode, btn: btn, commodity: commodity, cargoType: cargoType,
                ruta3: ruta3, grupoNaviera: grupoNaviera,
                nivel1: nivel1, nivel2: nivel2, nivel3: nivel3, nivel4: nivel4, nivel5: nivel5, nivel1Titulo: nivel1Titulo,
                tipoDato: tipoDato, nivel1: nivel1, nivel2: nivel2, nivel3: nivel3, nivel4: nivel4, nivel5: nivel5, nivel1Titulo: nivel1Titulo, mercado: mercado, etapa: etapa, contenedor: contenedor, dryReefer: dryReefer}
        })
                .done(function (msg) {

                    if (msg.trim() != "") {

                        $("#divResultado").html(msg);
                        $("#divOpciones").css("display", "block");

                        $("#tBody tr").each(function (index) {

                            if (index < 3) {

                                $(this).children("td").each(function (index2, td) {

                                    if (index2 == 0) {

                                        $(td).children("input").each(function (index3, input) {
                                            input.click();
                                        });

                                        $(td).children("span ").each(function (index3, span) {
                                            $(span).removeClass('checkboxC');
                                            $(span).addClass('checkboxChecked');
                                        });

                                    }
                                });
                            }
                        });


                        var nivelesDesc = "";

                        if (nivel1 != "")
                            nivelesDesc = nivelesDesc + "<tr><td><div class='nivel1'></div></td><td class='desc'>Nivel 1: " + nivel1Titulo + "</td></tr>";

                        if (nivel2.trim() != "") {
                            var nivel2Titulo = $("#selectNivel2 option:selected").text();
                            nivelesDesc = nivelesDesc + "<tr><td><div class='nivel2'></div></td><td class='desc'>Nivel 2: " + nivel2Titulo + "</td></tr>";
                        }

                        if (nivel3 != "") {
                            var nivel3Titulo = $("#selectNivel3 option:selected").text();
                            nivelesDesc = nivelesDesc + "<tr><td><div class='nivel3'></div></td><td class='desc'>Nivel 3: " + nivel3Titulo + "</td></tr>";
                        }

                        if (nivel4 != "") {
                            var nivel4Titulo = $("#selectNivel4 option:selected").text();
                            nivelesDesc = nivelesDesc + "<tr><td><div class='nivel4'></div></td><td class='desc'>Nivel 4: " + nivel4Titulo + "</td></tr>";
                        }

                        if (nivel5 != "") {
                            var nivel5Titulo = $("#selectNivel5 option:selected").text();
                            nivelesDesc = nivelesDesc + "<tr><td><div class='nivel5'></div></td><td class='desc'>Nivel 5: " + nivel5Titulo + "</td></tr>";
                        }

                        Custom.initChkbox();

                        $("#divOpciones").css("display", "block");
                        $("#tablaNivelesDesc").html(nivelesDesc);
                    } else {
                        $('#divResultado').html("<div style='margin-left:30px'><label style='color:red'>Búsqueda sin resultados.</label></div>");
                    }

                    $("#imgCargando").css("display", "none");

                });

    }

}


function buscarNivel1MA() {

    var nivel1 = $("#selectNivel1").val();
    var nivel2 = $("#selectNivel2").val();
    var nivel3 = $("#selectNivel3").val();
    var nivel4 = $("#selectNivel4").val();
    var nivel5 = $("#selectNivel5").val();


    var tipoFiltro = $('input[name=btnFiltroIndicador]:checked').val();
    var baremoTOP = $('#btnBaremosTOP').val();
    var baremoBTM = $('#btnBaremosBTM').val();
    var participacionMIN = $('#btnParticipacionMIN').val();
    var participacionMAX = $('#btnParticipacionMAX').val();
    var top = $('#btnTopval').val();
    
    var mercado = "";

    var chile = $("input[id=btnChile]:checked").val();
    var peru = $("input[id=btnPeru]:checked").val();
    var ecuador = $("input[id=btnEcuador]:checked").val();
    var colombia = $("input[id=btnColombia]:checked").val();


    mercado = chile + "," + peru + "," + ecuador + "," + colombia;
    mercado = replaceAll(mercado, ",undefined", "");
    mercado = replaceAll(mercado, "undefined", "");

    if (chile == undefined) {
        mercado = mercado.substring(1, mercado.length);
    }


    var fechaDesde = $("#fechaDesde").val();
    var fechaHasta = $("#fechaHasta").val();


    if (fechaDesde == "" && fechaHasta == "") {

        if (mercado == "1") {
            fechasDefecto("MAERSK_CHILE");
        } else if (mercado == "2") {
            fechasDefecto("MAERSK_PERU");
        } else if (mercado == "3") {
            fechasDefecto("MAERSK_ECUADOR");
        } else {
            fechasDefecto("MAERSK");
        }

        fechaDesde = $("#fechaDesde").val();
        fechaHasta = $("#fechaHasta").val();

        //fechaDesde = $("#fechaDesdeDef").val();
        //fechaHasta = $("#fechaHastaDef").val();
        //$("#fechaDesde").val(fechaDesde);
        //$("#fechaHasta").val(fechaHasta);

    }


    if (validarFormulario()) {

        $("#divResultado").html("");
        $("#tablaNivelesDesc").html("");

        $("#imgCargando").css("display", "block");
        $("#divPanelFiltros").css("display", "none");
        $("#divOpciones").css("display", "none");
        $("#divGraficos").css("display", "none");
        $("#divGrafico").css("display", "none");
        $("#divGraficoA").css("display", "none");
        $("#divGraficoB").css("display", "none");
        $("#divMensaje").css("display", "none");

        var portCluster = $("#portCluster").val();
        var porCountry = $("#porCountry").val();
        var porCity = $("#porCity").val();
        var lopfiCluster = $("#lopfiCluster").val();
        var lopfiCountry = $("#lopfiCountry").val();
        var lopCity = $("#lopCity").val();
        var diplaCountry = $("#diplaCountry").val();
        var diplaCity = $("#diplaCity").val();
        var podCountry = $("#podCountry").val();
        var podCity = $("#podCity").val();

        var naviera = $("#naviera").val();
        var nave = $("#nave").val();
        var shipper = $("#shipper").val();
        var consignee = $("#consignee").val();
        var ffww = $("#ffww").val();
        var notify = $("#notify").val();
        var customerCity = $("#customerCity").val();

        var ruta = $("#ruta").val();
        var trade = $("#trade").val();
        var subTrade = $("#subTrade").val();
        var item = $("#item").val();
        var itemCode = $("#itemCode").val();
        var btn = $("#btn").val();
        var commodity = $("#commodity").val();
        var cargoType = $("#cargoType").val();

        var ruta3 = $("#ruta3").val();
        var grupoNaviera = $("#grupoNaviera").val();

        var nivel1Titulo = $("#selectNivel1 option:selected").text();

        $('#selectTipoReporte').val("");
        $('#selectselectTipoReporte').html("Ambos");

        $('#selectGraficoReporte').val("line");
        $('#selectselectGrafico').html("Líneas");

        var tipoDato = $("input[name=tipoDato]:checked").val();

        switch (tipoDato) {
            case 'contenedores':
                $('#tituloGrafico2').val("N° de Contenedores (C20 + C40)");
                $('#tituloGraficoEjeY').val("N° de Contenedores (C20 + C40)");
                $('.tdTipo').css("display", "");
                break;

            case 'teus':
                $('#tituloGrafico2').val("N° de TEUs");
                $('#tituloGraficoEjeY').val("N° de TEUs");
                $('.tdTipo').css("display", "none");
                break;

            case 'toneladas':
                $('#tituloGrafico2').val("N° de Toneladas");
                $('#tituloGraficoEjeY').val("N° de Toneladas");
                $('.tdTipo').css("display", "none");
                break;

            case 'fob':
                $('#tituloGrafico2').val("FOB (en miles de USD)");
                $('#tituloGraficoEjeY').val("USD");
                $('.tdTipo').css("display", "none");
                break;

            case 'cif':
                $('#tituloGrafico2').val("CIF (en miles de USD)");
                $('#tituloGraficoEjeY').val("USD");
                $('.tdTipo').css("display", "none");
                break;

            case 'feus':
                $('#tituloGrafico2').val("N° de FFEs");
                $('#tituloGraficoEjeY').val("N° de FFEs");
                $('.tdTipo').css("display", "none");
                break;
        }


        var etapa = $("input[name=btnEtapa]:checked").val();
        var contenedor = $("input[name=btnContenedor]:checked").val();
        var dryReefer = $("input[name=btnSecaRefrigerada]:checked").val();

        var tituloGrafico2 = "";

        switch (etapa) {
            case undefined:
                tituloGrafico2 = 'Exportaciones e Importaciones'
                break;

            case '1':
                tituloGrafico2 = 'Exportaciones'
                break;

            case '2':
                tituloGrafico2 = 'Importaciones'
                break;
        }



        if (mercado == '' || mercado == '1,2,3,4') {
            tituloGrafico2 += ' (WCSA)'
        } else {
            tituloGrafico2 += ' (' + replaceAll(mercado, ",", " - ") + ')'
            tituloGrafico2 = tituloGrafico2.replace("1", "Chile");
            tituloGrafico2 = tituloGrafico2.replace("2", "Perú");
            tituloGrafico2 = tituloGrafico2.replace("3", "Ecuador");
            tituloGrafico2 = tituloGrafico2.replace("4", "Colombia");
        }

        $('#tituloGrafico').val(tituloGrafico2);

        $('#tdResumen').html("<center><font color='#3D85FE' size='3'>" + tituloGrafico2 + "</font><br>" + $('#tituloGrafico2').val() + "</center>" + filtros());


        $.ajax({
            type: "POST",
            url: "maerskMissingAccountNivel1.php",
            //async: false,
            data: {fechaDesde: fechaDesde, fechaHasta: fechaHasta, contenedor: contenedor,
                portCluster: portCluster, porCountry: porCountry, porCity: porCity, lopfiCluster: lopfiCluster, lopfiCountry: lopfiCountry,
                lopCity: lopCity, diplaCountry: diplaCountry, diplaCity: diplaCity, podCountry: podCountry, podCity: podCity,
                naviera: naviera, nave: nave, shipper: shipper, consignee: consignee, ffww: ffww, notify: notify, customerCity: customerCity,
                ruta: ruta, trade: trade, subTrade: subTrade, item: item, itemCode: itemCode, btn: btn, commodity: commodity, cargoType: cargoType,
                ruta3: ruta3, grupoNaviera: grupoNaviera,
                nivel1: nivel1, nivel2: nivel2, nivel3: nivel3, nivel4: nivel4, nivel5: nivel5, nivel1Titulo: nivel1Titulo,
                tipoDato: tipoDato, nivel1: nivel1, nivel2: nivel2, nivel3: nivel3, nivel4: nivel4, nivel5: nivel5, nivel1Titulo: nivel1Titulo, mercado: mercado,
                etapa: etapa, contenedor: contenedor, dryReefer: dryReefer
                , tipoFiltro: tipoFiltro, baremoTOP: baremoTOP, baremoBTM: baremoBTM, participacionMIN: participacionMIN, participacionMAX: participacionMAX, top: top}
        })
                .done(function (msg) {

                    if (msg.trim() != "") {

                        $("#divResultado").html(msg);
                        $("#divOpciones").css("display", "block");

                        $("#tBody tr").each(function (index) {

                            if (index < 3) {

                                $(this).children("td").each(function (index2, td) {

                                    if (index2 == 0) {

                                        $(td).children("input").each(function (index3, input) {
                                            input.click();
                                        });

                                        $(td).children("span ").each(function (index3, span) {
                                            $(span).removeClass('checkboxC');
                                            $(span).addClass('checkboxChecked');
                                        });

                                    }
                                });
                            }
                        });


                        var nivelesDesc = "";

                        if (nivel1 != "")
                            nivelesDesc = nivelesDesc + "<tr><td><div class='nivel1'></div></td><td class='desc'>Nivel 1: " + nivel1Titulo + "</td></tr>";

                        if (nivel2.trim() != "") {
                            var nivel2Titulo = $("#selectNivel2 option:selected").text();
                            nivelesDesc = nivelesDesc + "<tr><td><div class='nivel2'></div></td><td class='desc'>Nivel 2: " + nivel2Titulo + "</td></tr>";
                        }

                        if (nivel3 != "") {
                            var nivel3Titulo = $("#selectNivel3 option:selected").text();
                            nivelesDesc = nivelesDesc + "<tr><td><div class='nivel3'></div></td><td class='desc'>Nivel 3: " + nivel3Titulo + "</td></tr>";
                        }

                        if (nivel4 != "") {
                            var nivel4Titulo = $("#selectNivel4 option:selected").text();
                            nivelesDesc = nivelesDesc + "<tr><td><div class='nivel4'></div></td><td class='desc'>Nivel 4: " + nivel4Titulo + "</td></tr>";
                        }

                        if (nivel5 != "") {
                            var nivel5Titulo = $("#selectNivel5 option:selected").text();
                            nivelesDesc = nivelesDesc + "<tr><td><div class='nivel5'></div></td><td class='desc'>Nivel 5: " + nivel5Titulo + "</td></tr>";
                        }

                        Custom.initChkbox();

                        $("#divOpciones").css("display", "block");
                        $("#tablaNivelesDesc").html(nivelesDesc);
                    } else {
                        $('#divResultado').html("<div style='margin-left:30px'><label style='color:red'>Búsqueda sin resultados.</label></div>");
                    }

                    $("#imgCargando").css("display", "none");

                });

    }

}



function buscarNivel2(obj, fechaDesde, fechaHasta, nivel2, nivel3, nivel4, nivel5, tipoDato, nivel1Id, otros) {

    var tr = obj.parentNode;
    var className = $(tr).hasClass('OK');

    if (className == false && nivel2 != "") {

        $.ajax({
            type: "POST",
            url: "maerskReporteNivel2.php",
            async: false,
            data: {fechaDesde: fechaDesde, fechaHasta: fechaHasta, tipoDato: tipoDato, nivel2: nivel2, nivel3: nivel3, nivel4: nivel4, nivel5: nivel5, trId: $(tr).attr('id'), nivel1Id: nivel1Id, otros: otros}
        })
                .done(function (msg) {
                    $(tr).addClass('OK');
                    $(msg).insertAfter(tr);
                });
    } else {
        desplegarNivel($(tr).attr('id'), 2);
    }
}


function buscarNivel2MA(obj, fechaDesde, fechaHasta, nivel2, nivel3, nivel4, nivel5, tipoDato, nivel1Id, otros, total) {

    var tr = obj.parentNode;
    var className = $(tr).hasClass('OK');

    if (className == false && nivel2 != "") {

        $.ajax({
            type: "POST",
            url: "maerskMissingAccountNivel2.php",
            async: false,
            data: {fechaDesde: fechaDesde, fechaHasta: fechaHasta, tipoDato: tipoDato, nivel2: nivel2, nivel3: nivel3, nivel4: nivel4, nivel5: nivel5, trId: $(tr).attr('id'), nivel1Id: nivel1Id, otros: otros, total: total}
        })
                .done(function (msg) {
                    $(tr).addClass('OK');
                    $(msg).insertAfter(tr);
                });
    } else {
        desplegarNivel($(tr).attr('id'), 2);
    }
}


function buscarNivel3(obj, fechaDesde, fechaHasta, nivel3, nivel4, nivel5, tipoDato, nivel1Id, nivel2Id, otros) {

    var tr = obj.parentNode;
    var className = $(tr).hasClass('OK');

    if (className == false && nivel3 != "") {

        $.ajax({
            type: "POST",
            url: "maerskReporteNivel3.php",
            async: false,
            data: {fechaDesde: fechaDesde, fechaHasta: fechaHasta, tipoDato: tipoDato, nivel3: nivel3, nivel4: nivel4, nivel5: nivel5, trId: $(tr).attr('id'), nivel1Id: nivel1Id, nivel2Id: nivel2Id, otros: otros}
        })
                .done(function (msg) {
                    $(tr).addClass('OK');
                    $(msg).insertAfter(tr);
                });
    } else {
        desplegarNivel($(tr).attr('id'), 3);
    }
}


function buscarNivel4(obj, fechaDesde, fechaHasta, nivel4, nivel5, tipoDato, nivel1Id, nivel2Id, nivel3Id, otros) {

    var tr = obj.parentNode;
    var className = $(tr).hasClass('OK');

    if (className == false && nivel4 != "") {

        $.ajax({
            type: "POST",
            url: "maerskReporteNivel4.php",
            async: false,
            data: {fechaDesde: fechaDesde, fechaHasta: fechaHasta, tipoDato: tipoDato, nivel4: nivel4, nivel5: nivel5, trId: $(tr).attr('id'), nivel1Id: nivel1Id, nivel2Id: nivel2Id, nivel3Id: nivel3Id, otros: otros}
        })
                .done(function (msg) {
                    $(tr).addClass('OK');
                    $(msg).insertAfter(tr);
                });
    } else {
        desplegarNivel($(tr).attr('id'), 4);
    }
}


function buscarNivel5(obj, fechaDesde, fechaHasta, nivel5, tipoDato, nivel1Id, nivel2Id, nivel3Id, nivel4Id, otros) {

    var tr = obj.parentNode;
    var className = $(tr).hasClass('OK');

    if (className == false && nivel5 != "") {

        $.ajax({
            type: "POST",
            url: "maerskReporteNivel5.php",
            async: false,
            data: {fechaDesde: fechaDesde, fechaHasta: fechaHasta, tipoDato: tipoDato, nivel5: nivel5, trId: $(tr).attr('id'), nivel1Id: nivel1Id, nivel2Id: nivel2Id, nivel3Id: nivel3Id, nivel4Id: nivel4Id, otros: otros}
        })
                .done(function (msg) {
                    $(tr).addClass('OK');
                    $(msg).insertAfter(tr);
                });
    } else {
        desplegarNivel($(tr).attr('id'), 5);
    }
}


function validarFormulario() {

    var mensaje = "";

    var nivel1 = $("#selectNivel1").val();
    var fechaDesde = $("#fechaDesde").val();
    var fechaHasta = $("#fechaHasta").val();

    if (nivel1 == "") {
        mensaje = "Debe seleccionar el primer nivel.<br>";
    }

    if (fechaDesde == "") {
        mensaje += "No ha ingresado la fecha inicial.<br>";
    }

    if (fechaHasta == "") {
        mensaje += "No ha ingresado la fecha final.<br>";
    }

    if (fechaDesde != "" && fechaHasta != "" && validarFechaMenor(fechaDesde, fechaHasta) == false) {
        mensaje += "La fecha incial no puede ser mayor a la fecha final.<br>";
    }

    if (fechaDesde != "" && fechaHasta != "" && fechaDiferenciasDias(fechaDesde, fechaHasta) > 365) {
        mensaje += "La diferencia entre las fechas no puede ser superior a un año.<br>";
    }

    if (mensaje != "") {
        $("#divMensaje").html(mensaje);
        $("#divMensaje").css("display", "block");
        return false;
    } else {
        $("#divMensaje").html("");
        $("#divMensaje").css("display", "none");
        return true;
    }

}


function buscarNivel1SoW() {

    var nivel1 = $("#selectNivel1").val();
    var nivel2 = $("#selectNivel2").val();
    var nivel3 = $("#selectNivel3").val();
    var nivel4 = $("#selectNivel4").val();
    var nivel5 = $("#selectNivel5").val();

    var mercado = "";

    var chile = $("input[id=btnChile]:checked").val();
    var peru = $("input[id=btnPeru]:checked").val();
    var ecuador = $("input[id=btnEcuador]:checked").val();
    var colombia = $("input[id=btnColombia]:checked").val();

    var participacionTOP = $('#participacionTOP').val();
    var participacionBTM = $('#participacionBTM').val();
    var top = $('#btnTopval').val();

    mercado = chile + "," + peru + "," + ecuador + "," + colombia;
    mercado = replaceAll(mercado, ",undefined", "");
    mercado = replaceAll(mercado, "undefined", "");

    if (chile == undefined) {
        mercado = mercado.substring(1, mercado.length);
    }


    var fechaDesde = $("#fechaDesde").val();
    var fechaHasta = $("#fechaHasta").val();


    if (fechaDesde == "" && fechaHasta == "") {

        if (mercado == "1") {
            fechasDefecto("MAERSK_CHILE");
        } else if (mercado == "2") {
            fechasDefecto("MAERSK_PERU");
        } else if (mercado == "3") {
            fechasDefecto("MAERSK_ECUADOR");
        } else {
            fechasDefecto("MAERSK");
        }

        fechaDesde = $("#fechaDesde").val();
        fechaHasta = $("#fechaHasta").val();

        //fechaDesde = $("#fechaDesdeDef").val();
        //fechaHasta = $("#fechaHastaDef").val();
        //$("#fechaDesde").val(fechaDesde);
        //$("#fechaHasta").val(fechaHasta);

    }


    if (validarFormulario()) {

        $("#divResultado").html("");
        $("#tablaNivelesDesc").html("");

        $("#imgCargando").css("display", "block");
        $("#divPanelFiltros").css("display", "none");
        $("#divOpciones").css("display", "none");
        $("#divGraficos").css("display", "none");
        $("#divGrafico").css("display", "none");
        $("#divGraficoA").css("display", "none");
        $("#divGraficoB").css("display", "none");
        $("#divMensaje").css("display", "none");

        var portCluster = $("#portCluster").val();
        var porCountry = $("#porCountry").val();
        var porCity = $("#porCity").val();
        var lopfiCluster = $("#lopfiCluster").val();
        var lopfiCountry = $("#lopfiCountry").val();
        var lopCity = $("#lopCity").val();
        var diplaCountry = $("#diplaCountry").val();
        var diplaCity = $("#diplaCity").val();
        var podCountry = $("#podCountry").val();
        var podCity = $("#podCity").val();

        var naviera = $("#naviera").val();
        var nave = $("#nave").val();
        var shipper = $("#shipper").val();
        var consignee = $("#consignee").val();
        var ffww = $("#ffww").val();
        var notify = $("#notify").val();
        var customerCity = $("#customerCity").val();

        var ruta = $("#ruta").val();
        var trade = $("#trade").val();
        var subTrade = $("#subTrade").val();
        var item = $("#item").val();
        var itemCode = $("#itemCode").val();
        var btn = $("#btn").val();
        var commodity = $("#commodity").val();
        var cargoType = $("#cargoType").val();

        var ruta3 = $("#ruta3").val();
        var grupoNaviera = $("#grupoNaviera").val();

        var nivel1Titulo = $("#selectNivel1 option:selected").text();

        $('#selectTipoReporte').val("");
        $('#selectselectTipoReporte').html("Ambos");

        $('#selectGraficoReporte').val("line");
        $('#selectselectGrafico').html("Líneas");

        var tipoDato = $("input[name=tipoDato]:checked").val();

        switch (tipoDato) {
            case 'contenedores':
                $('#tituloGrafico2').val("N° de Contenedores (C20 + C40)");
                $('#tituloGraficoEjeY').val("N° de Contenedores (C20 + C40)");
                $('.tdTipo').css("display", "");
                break;

            case 'teus':
                $('#tituloGrafico2').val("N° de TEUs");
                $('#tituloGraficoEjeY').val("N° de TEUs");
                $('.tdTipo').css("display", "none");
                break;

            case 'toneladas':
                $('#tituloGrafico2').val("N° de Toneladas");
                $('#tituloGraficoEjeY').val("N° de Toneladas");
                $('.tdTipo').css("display", "none");
                break;

            case 'fob':
                $('#tituloGrafico2').val("FOB (en miles de USD)");
                $('#tituloGraficoEjeY').val("USD");
                $('.tdTipo').css("display", "none");
                break;

            case 'cif':
                $('#tituloGrafico2').val("CIF (en miles de USD)");
                $('#tituloGraficoEjeY').val("USD");
                $('.tdTipo').css("display", "none");
                break;

            case 'feus':
                $('#tituloGrafico2').val("N° de FFEs");
                $('#tituloGraficoEjeY').val("N° de FFEs");
                $('.tdTipo').css("display", "none");
                break;
        }


        var etapa = $("input[name=btnEtapa]:checked").val();
        var contenedor = $("input[name=btnContenedor]:checked").val();
        var dryReefer = $("input[name=btnSecaRefrigerada]:checked").val();

        var tituloGrafico2 = "";

        switch (etapa) {
            case undefined:
                tituloGrafico2 = 'Exportaciones e Importaciones'
                break;

            case '1':
                tituloGrafico2 = 'Exportaciones'
                break;

            case '2':
                tituloGrafico2 = 'Importaciones'
                break;
        }



        if (mercado == '' || mercado == '1,2,3,4') {
            tituloGrafico2 += ' (WCSA)'
        } else {
            tituloGrafico2 += ' (' + replaceAll(mercado, ",", " - ") + ')'
            tituloGrafico2 = tituloGrafico2.replace("1", "Chile");
            tituloGrafico2 = tituloGrafico2.replace("2", "Perú");
            tituloGrafico2 = tituloGrafico2.replace("3", "Ecuador");
            tituloGrafico2 = tituloGrafico2.replace("4", "Colombia");
        }

        $('#tituloGrafico').val(tituloGrafico2);

        $('#tdResumen').html("<center><font color='#3D85FE' size='3'>" + tituloGrafico2 + "</font><br>" + $('#tituloGrafico2').val() + "</center>" + filtros());


        $.ajax({
            type: "POST",
            url: "maerskSoWNivel1.php",
            //async: false,
            data: {fechaDesde: fechaDesde, fechaHasta: fechaHasta, contenedor: contenedor,
                portCluster: portCluster, porCountry: porCountry, porCity: porCity, lopfiCluster: lopfiCluster, lopfiCountry: lopfiCountry,
                lopCity: lopCity, diplaCountry: diplaCountry, diplaCity: diplaCity, podCountry: podCountry, podCity: podCity,
                naviera: naviera, nave: nave, shipper: shipper, consignee: consignee, ffww: ffww, notify: notify, customerCity: customerCity,
                ruta: ruta, trade: trade, subTrade: subTrade, item: item, itemCode: itemCode, btn: btn, commodity: commodity, cargoType: cargoType,
                ruta3: ruta3, grupoNaviera: grupoNaviera,
                nivel1: nivel1, nivel2: nivel2, nivel3: nivel3, nivel4: nivel4, nivel5: nivel5, nivel1Titulo: nivel1Titulo,
                tipoDato: tipoDato, nivel1: nivel1, nivel2: nivel2, nivel3: nivel3, nivel4: nivel4, nivel5: nivel5, nivel1Titulo: nivel1Titulo, mercado: mercado, etapa: etapa, contenedor: contenedor, dryReefer: dryReefer,
                participacionTOP: 100, participacionBTM: 0, top: top
            }
        })
                .done(function (msg) {

                    if (msg.trim() != "") {

                        $("#divResultado").html(msg);
                        $("#divOpciones").css("display", "block");

                        $("#tBody tr").each(function (index) {

                            if (index < 3) {

                                $(this).children("td").each(function (index2, td) {

                                    if (index2 == 0) {

                                        $(td).children("input").each(function (index3, input) {
                                            input.click();
                                        });

                                        $(td).children("span ").each(function (index3, span) {
                                            $(span).removeClass('checkboxC');
                                            $(span).addClass('checkboxChecked');
                                        });

                                    }
                                });
                            }
                        });


                        var nivelesDesc = "";

                        if (nivel1 != "")
                            nivelesDesc = nivelesDesc + "<tr><td><div class='nivel1'></div></td><td class='desc'>Nivel 1: " + nivel1Titulo + "</td></tr>";

                        if (nivel2.trim() != "") {
                            var nivel2Titulo = $("#selectNivel2 option:selected").text();
                            nivelesDesc = nivelesDesc + "<tr><td><div class='nivel2'></div></td><td class='desc'>Nivel 2: " + nivel2Titulo + "</td></tr>";
                        }

                        if (nivel3 != "") {
                            var nivel3Titulo = $("#selectNivel3 option:selected").text();
                            nivelesDesc = nivelesDesc + "<tr><td><div class='nivel3'></div></td><td class='desc'>Nivel 3: " + nivel3Titulo + "</td></tr>";
                        }

                        if (nivel4 != "") {
                            var nivel4Titulo = $("#selectNivel4 option:selected").text();
                            nivelesDesc = nivelesDesc + "<tr><td><div class='nivel4'></div></td><td class='desc'>Nivel 4: " + nivel4Titulo + "</td></tr>";
                        }

                        if (nivel5 != "") {
                            var nivel5Titulo = $("#selectNivel5 option:selected").text();
                            nivelesDesc = nivelesDesc + "<tr><td><div class='nivel5'></div></td><td class='desc'>Nivel 5: " + nivel5Titulo + "</td></tr>";
                        }

                        Custom.initChkbox();

                        $("#divOpciones").css("display", "block");
                        $("#tablaNivelesDesc").html(nivelesDesc);
                    } else {
                        $('#divResultado').html("<div style='margin-left:30px'><label style='color:red'>Búsqueda sin resultados.</label></div>");
                    }

                    $("#imgCargando").css("display", "none");

                });

    }

}
    
    
    
function buscarNivel2sow(obj, fechaDesde, fechaHasta, nivel2, nivel3, nivel4, nivel5, tipoDato, nivel1Id, otros, total) {

    var tr = obj.parentNode;
    var className = $(tr).hasClass('OK');

    if (className == false && nivel2 != "") {

        $.ajax({
            type: "POST",
            url: "maerskSoWNivel2.php",
            async: false,
            data: {fechaDesde: fechaDesde, fechaHasta: fechaHasta, tipoDato: tipoDato, nivel2: nivel2, nivel3: nivel3, nivel4: nivel4, nivel5: nivel5, trId: $(tr).attr('id'), nivel1Id: nivel1Id, otros: otros, total: total}
        })
                .done(function (msg) {
                    $(tr).addClass('OK');
                    $(msg).insertAfter(tr);
                });
    } else {
        desplegarNivel($(tr).attr('id'), 2);
    }
}








function rotator(img) {
    var jqimg = $(img);

    if (jqimg.attr('class') == 'c-on') {
        jqimg.attr('src', '../../imagenes/off.png');
    } else {
        jqimg.attr('src', '../../imagenes/on.png');
    }
    jqimg.toggleClass("c-on");
    jqimg.toggleClass("c-off");


    findDataForGrafic(jqimg[0]);
}

function findDataForGrafic(p) {



    var parentTR = $(p).parents('tr');
    var pId = parentTR.prop('id');



    if (p.nodeName === 'TD') {
        pId = pId + '-0';
    }

    gselected = p;

    if (countDashes(pId) == 2) {

        var papi = $('#' + removeLastDash(pId));
        //var alert(papi.find('td').eq(1).html());

        var nopt = {};
        var categories = [];
        var series = [];

        $('#tablaReporte').find('tr').eq(1).find('td').each(function (indice, element) {
            if (indice > 0 && $(element).html().indexOf("-") >= 0) {
                console.log($(element).html());
                categories.push($(element).html());
            }
        });

        console.log('==========');

        nopt['categories'] = categories;
        nopt['title'] = papi.find('td').eq(1).html();
        gtotal = parseInt(papi.find('td').eq(categories.length + 2).text().replace(/\./g, ''));
        console.log('GTOTAL: ' + gtotal);
        nopt['ind'] = $('input[name=tipoDato]:checked').val();
        nopt['totals'] = [];
        papi.nextAll('tr').each(function (indice, element) {
            var e = $(element);

            if (countDashes(e.prop('id')) == 1) {
                return false;
            } else {

                var i = e.find('img').eq(0);
                if (i.attr('class') == 'c-on') {
                    console.log(e.find('td').eq(1).html());
                    var narr = {};
                    var data = [];
                    narr['name'] = e.find('td').eq(1).html().replace(/&amp;/g, '&');
                    for (var j = 2; j <= categories.length + 1; j++) {
                        data.push(parseInt(e.find('td').eq(j).html().replace(/\./g, '')));
                        //console.log('===>' + e.find('td').eq(j).html());
                    }
                    narr['data'] = data;
                    console.log('Total: ' + e.find('td').eq(categories.length + 2).text());
                    nopt['totals'].push(parseInt(e.find('td').eq(categories.length + 2).text().replace(/\./g, '')));
                    series.push(narr);
                }
            }
        });

        nopt['series'] = series;

        console.log(JSON.stringify(nopt));

        	
		if(papi.find('td').eq(1).html() == null){
			 
			 //ALERT("PASO");
			 console.log('=====NO PASO=====');
		}else{
			crearGraficosO(nopt);
			console.log('=====PASO=====');
		}
    }

}


function countDashes(haystack) {
    return haystack.length - haystack.replace(/\-/g, '').length;
}

function removeLastDash(str) {
    var pos = str.lastIndexOf("-");
    return str.substring(0, pos);
}


function cambiarTypoGrafico() {

    if (typeof gselected !== "undefined") {
        findDataForGrafic(gselected);
    }

}

var gselected;
var gtotal = 0;

function crearGraficosO(nopt) {

    $('#divGraficos').css('display', '');
    $('#divGrafico').css('display', '');
    var typog = $('#selectGraficoReporte1').find('option:selected').val();


    var opt = {
        "chart": {
            "renderTo": "divGrafico",
            "defaultSeriesType": typog
        },
        "title": {
            "text": "Exportaciones e Importaciones (Chile)<br>N° de Contenedores (C20 + C40)"
        },
        "subtitle": {
            "text": " ",
            "y": 50
        },
        "xAxis": {
            "categories": ["", "JUL-2016", "AGO-2016", "SEP-2016", "OCT-2016", "NOV-2016", "DIC-2016", "ENE-2017", "FEB-2017", "MAR-2017", "ABR-2017", "MAY-2017"]
        },
        "yAxis": {
            "min": 0,
            "labels": {},
            "title": {
                "text": "N° de Contenedores (C20 + C40)"
            }
        },
        "legend": {
            "backgroundColor": "#FFFFFF"
        },
        "tooltip": {},
        "plotOptions": {
            "column": {
                "pointPadding": 0.2,
                "borderWidth": 0
            }
        },
        "series": [{
                "name": "EXPO",
                "data": [64670, 63772, 63279, 53302, 65285, 54837, 70694, 57699, 56786, 76609, 64314, 65427]
            }, {
                "name": "IMPO",
                "data": [48340, 51756, 54444, 50474, 57323, 50021, 52570, 52700, 46393, 48761, 49679, 51133]
            }]
    }



    opt['xAxis']['categories'] = nopt['categories'];
    if ($('#btnImpo').prop('checked')) {
        opt['title']['text'] = 'Importaciones ' + nopt['title'];
    } else {
        opt['title']['text'] = 'Exportaciones ' + nopt['title'];
    }
    opt['yAxis']['title']['text'] = nopt['ind'].toUpperCase();




    if (typog === 'pie') {
        opt['chart']["plotBackgroundColor"] = null,
                opt['chart']["plotBorderWidth"] = null,
                opt['chart']["plotShadow"] = false

        var ndata = [];
        var tsum = 0;
        $.each(nopt['series'], function (index, value) {
            var dt = nopt['series'][index];
            /*var sum = 0;
             $.each(dt['data'], function( index, value ) {
             sum = sum + value;
             });
             */
            var sum = nopt['totals'][index];
            var name = dt['name'];
            tsum = tsum + sum;
            var por = parseFloat(((sum * 100) / gtotal).toFixed(2));
            ndata.push({name: name + ' <b>' + por + '%</b>', y: por});
        });

        console.log('*----> ' + gtotal + ' - ' + tsum)
        if (gtotal - tsum > 0) {
            var por = parseFloat((((gtotal - tsum) * 100) / gtotal).toFixed(2));
            ndata.push({name: 'OTROS <b>' + por + '%</b>', y: por});
        }

        opt['plotOptions'] = {
            "pie": {
                "allowPointSelect": true,
                "cursor": "pointer",
                "dataLabels": {
                    "enabled": true,
                    "color": "#808080",
                    "connectorColor": "#6D869F"
                },
                "showInLegend": true
            }

        };
        delete(opt['xAxis']);
        delete(opt['yAxis']);
        opt['tooltip'] = {formatter: function () {
                var leg = this.point.name;
                var leg2 = replaceAll(leg, "&amp;", "&");

                //return '<b>'+ leg2 +'</b>: '+ this.percentage +' %';
            }};
        opt['series'] = [{
                name: opt['title']['text'],
                data: ndata
            }];
    } else {
        opt['series'] = nopt['series'];
    }



    console.log('----------------------');
   // console.log(JSON.stringify(opt));
    console.log('----------------------');
    var chart1 = new Highcharts.Chart(opt);
}




function crearGraficos() {
    var opt = {
        "chart": {
            "renderTo": "divGrafico",
            "defaultSeriesType": "line"
        },
        "title": {
            "text": "Exportaciones e Importaciones (Chile)<br>N° de Contenedores (C20 + C40)"
        },
        "subtitle": {
            "text": " ",
            "y": 50
        },
        "xAxis": {
            "categories": ["", "JUL-2016", "AGO-2016", "SEP-2016", "OCT-2016", "NOV-2016", "DIC-2016", "ENE-2017", "FEB-2017", "MAR-2017", "ABR-2017", "MAY-2017"]
        },
        "yAxis": {
            "min": 0,
            "labels": {},
            "title": {
                "text": "N° de Contenedores (C20 + C40)"
            }
        },
        "legend": {
            "backgroundColor": "#FFFFFF"
        },
        "tooltip": {},
        "plotOptions": {
            "column": {
                "pointPadding": 0.2,
                "borderWidth": 0
            }
        },
        "series": [{
                "name": "EXPO",
                "data": [64670, 63772, 63279, 53302, 65285, 54837, 70694, 57699, 56786, 76609, 64314, 65427]
            }, {
                "name": "IMPO",
                "data": [48340, 51756, 54444, 50474, 57323, 50021, 52570, 52700, 46393, 48761, 49679, 51133]
            }]
    }


    var chart1 = new Highcharts.Chart(opt);
}




function validate(e){
    tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite
    if (tecla==8){
        return true;
    }
	if (tecla==37){
        return true;
    }
	if (tecla==39){
        return true;
    }

    // Patron de entrada, en este caso solo acepta numeros
    patron =/[0-9]/;
	
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
}

 

