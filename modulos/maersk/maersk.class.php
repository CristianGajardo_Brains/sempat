<?php

class maersk {

    function ultimaFechaManifiesto($clienteId) {
        $query = "Exec seleccionarFechasLimites '" . $clienteId . "','M'";
        $result = sql_db::sql_query($query);
        sql_db::sql_close();
        $row = sql_db::sql_fetch_assoc($result);
        return $row;
    }

    function manifiestoFechasDefecto($clienteId) {
        $query = "Exec seleccionarFechasDefecto '" . $clienteId . "','M'";
        $result = sql_db::sql_query($query);
        sql_db::sql_close();
        $row = sql_db::sql_fetch_assoc($result);
        return $row;
    }

    function mercadosDisponibles($mercados, $ancho) {

        $tdHtml = "";

        if ($mercados == "") {
            $tdHtml = "<td valign='top' width='" . $ancho . "'><label class='niveles'>MERCADO</label><table>";
        } else {
            $tdHtml = "<td valign='top' width='" . $ancho . "' style='display:none'><label class='niveles'>MERCADO</label><table>";
        }

        switch ($mercados) {

            case "":
                $tdHtml .= "<tr>
                                <td>
                                    <input id='btnChile' type='radio' value='1'>
                                </td> 
                                <td>
                                    Chile
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input id='btnPeru' type='radio' value='2'>
                                </td>                                        
                                <td>
                                    Perú
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input id='btnEcuador' type='radio' value='3'>
                                </td>                                        
                                <td>
                                    Ecuador
                                </td>
                            </tr>";
                break;

            case "1":
                $tdHtml .= "<tr>
                                <td>
                                    <input id='btnChile' type='radio' value='1' checked='checked'>
                                </td> 
                                <td>
                                    Chile
                                </td>
                            </tr>";
                break;

            case "2":
                $tdHtml .= "<tr>
                                <td>
                                    <input id='btnPeru' type='radio' value='2' checked='checked'>
                                </td> 
                                <td>
                                    Perú
                                </td>
                            </tr>";
                break;

            case "3":
                $tdHtml .= "<tr>
                                <td>
                                    <input id='btnEcuador' type='radio' value='3' checked='checked'>
                                </td> 
                                <td>
                                    Ecuador
                                </td>
                            </tr>";
                break;

            case "4":
                $tdHtml .= "<tr>
                                <td>
                                    <input id='btnColombia' type='radio' value='4' checked='checked'>
                                </td> 
                                <td>
                                    Colombia
                                </td>
                            </tr>";
                break;
        }

        $tdHtml .= "</table></td>";

        return $tdHtml;
    }

    //INDICADORES

    function indicadoresCabeceraDatos($fecha) {
        $query = "Exec manifiestoIndicadoresCabecera '" . $fecha . "'";
        //echo $query;
        $result = sql_db::sql_query($query);
        $row = sql_db::sql_fetch_assoc($result);
        sql_db::sql_close();
        return $row;
    }

    function indicadoresCabeceraHtml($valorNivel1, $fecha) {

        $objSem = new maersk();
        $resultado = $objSem->indicadoresCabeceraDatos($fecha);

        $titulo1 = "ENERO";
        $titulo2 = "";
        $titulo3 = "";
        $titulo2b = "";
        $titulo3b = "";

        if (strtoupper($resultado["mmActual"]) != "ENERO") {
            $titulo1 = "ENERO - " . strtoupper($resultado["mmActual"]);
        }

        if ($resultado["mmAnterior"] != "" && $resultado["mmActual"] != "") {
            $titulo2 = $resultado["mmAnterior"] . " - " . $resultado["mmActual"];
        }

        if ($resultado["mmActual"] != "") {
            $titulo3 = $resultado["mmActual"];
        }

        if ($resultado["mmActual"] == "DICIEMBRE") {
            $titulo2b = $resultado["yyyyActual"];
        } else {
            if ($resultado["yyyyAnterior"] != "") {
                $titulo2b = $resultado["yyyyAnterior"] . " - " . ($resultado["yyyyActual"]);
            }
        }

        if ($resultado["mmActual"] == "DICIEMBRE") {
            $titulo3b = $resultado["yyyyAnterior"];
        } else {
            if ($resultado["yyyyAnterior"] != "") {
                $titulo3b = ($resultado["yyyyAnterior"] - 1) . " - " . ($resultado["yyyyAnterior"]);
            }
        }


        $htmlThead = "<tr>
                        <th class=\"thCenter\" colspan=\"6\">
                            <div class=\"divThLeft\"></div>
                            <div class=\"divThCenter\">" . $titulo1 . "</div>
                            <div class=\"divThRight\"></div>          
                        </th>                        
                        <th style=\"min-width: 10px;\"></th>
                        <th class=\"thCenter\" colspan=\"4\">
                            <div class=\"divThLeft\"></div>          
                            <div class=\"divThCenter\">" . $titulo2 . "</div>
                            <div class=\"divThRight\"></div>          
                        </th> 
                        <th style=\"min-width: 10px;\"></th>
                        <th class=\"thCenter\" colspan=\"4\">
                            <div class=\"divThLeft\"></div>          
                            <div class=\"divThCenter\">" . $titulo3 . "</div>
                            <div class=\"divThRight\"></div>          
                        </th> 
                    </tr>   
                    <tr>
                        <td colspan=\"2\" class=\"left\" style=\"width:185px;\">
                                " . mb_convert_encoding(trim($valorNivel1), "UTF-8", "ISO-8859-1") . "
                        </td>
                        <td>
                            " . $resultado["yyyyActual"] . "
                        </td>
                        <td>
                            " . $resultado["yyyyAnterior"] . "
                        </td>
                        <td>
                            DIF.
                        </td>
                        <td>
                            Mkt. SHARE
                        </td>
                        <td class=\"clear\">

                        </td>
                        <td class=\"left\">
                            " . $titulo2b . "
                        </td>
                        <td>
                            " . $titulo3b . "
                        </td>
                        <td>
                            DIF.
                        </td>
                        <td>
                            Mkt. SHARE
                        </td>
                        <td class=\"clear\">

                        </td>
                        <td class=\"left\">
                            " . $resultado["yyyyActual"] . "
                        </td>
                        <td>
                            " . $resultado["yyyyAnterior"] . "
                        </td>
                        <td>
                            DIF.
                        </td>
                        <td>
                            Mkt. SHARE
                        </td>                        
                    </tr>";

        return $htmlThead;
    }

    function maerskIndicadoresNivel1($fechaDesde, $fechaHasta, $nivel1, $nivel2, $nivel3, $nivel4, $nivel5, $tipoDato, $clienteId, $etapa, $mercado, $nave, $naviera, $ruta, $trade, $subTrade, $portCluster, $porCountry, $porCity, $lopfiCountry, $lopfiCode, $lopCityId, $lopfiClusterId, $diplaCountryId, $diplaCodeId, $diplaCityId, $podCountryId, $podCityId, $itemId, $itemCodeId, $btnId, $commodityId, $cargoTypeId, $shipperId, $consigneeId, $ffwwId, $notifyId, $customerCityId, $ruta3, $grupoNaviera, $containerVacios, $usuarioId) {


        $stmt = mssql_init("maerskIndicadoresNivel1");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);
        mssql_bind($stmt, '@var1', $nivel1, SQLVARCHAR);
        mssql_bind($stmt, '@var2', $nivel2, SQLVARCHAR);
        mssql_bind($stmt, '@var3', $nivel3, SQLVARCHAR);
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@etapa', $etapa, SQLVARCHAR);
        mssql_bind($stmt, '@mercado', $mercado, SQLVARCHAR);
        mssql_bind($stmt, '@nave', $nave, SQLVARCHAR);
        mssql_bind($stmt, '@naviera', $naviera, SQLVARCHAR);
        mssql_bind($stmt, '@ruta', $ruta, SQLVARCHAR);
        mssql_bind($stmt, '@trade', $trade, SQLVARCHAR);
        mssql_bind($stmt, '@subTrade', $subTrade, SQLVARCHAR);
        mssql_bind($stmt, '@portCluster', $portCluster, SQLVARCHAR);
        mssql_bind($stmt, '@porCountry', $porCountry, SQLVARCHAR);
        mssql_bind($stmt, '@porCity', $porCity, SQLVARCHAR);
        mssql_bind($stmt, '@lopfiCountry', $lopfiCountry, SQLVARCHAR);
        mssql_bind($stmt, '@lopfiCode', $lopfiCode, SQLVARCHAR);
        mssql_bind($stmt, '@lopCityId', $lopCityId, SQLVARCHAR);

        mssql_bind($stmt, '@lopfiClusterId', $lopfiClusterId, SQLVARCHAR);
        mssql_bind($stmt, '@diplaCountryId', $diplaCountryId, SQLVARCHAR);
        mssql_bind($stmt, '@diplaCodeId', $diplaCodeId, SQLVARCHAR);
        mssql_bind($stmt, '@diplaCityId', $diplaCityId, SQLVARCHAR);
        mssql_bind($stmt, '@podCountryId', $podCountryId, SQLVARCHAR);
        mssql_bind($stmt, '@podCityId', $podCityId, SQLVARCHAR);
        mssql_bind($stmt, '@itemId', $itemId, SQLVARCHAR);
        mssql_bind($stmt, '@itemCodeId', $itemCodeId, SQLVARCHAR);
        mssql_bind($stmt, '@btnId', $btnId, SQLVARCHAR);
        mssql_bind($stmt, '@commodityId', $commodityId, SQLVARCHAR);
        mssql_bind($stmt, '@cargoTypeId', $cargoTypeId, SQLVARCHAR);
        mssql_bind($stmt, '@shipperId', $shipperId, SQLVARCHAR);
        mssql_bind($stmt, '@consigneeId', $consigneeId, SQLVARCHAR);
        mssql_bind($stmt, '@ffwwId', $ffwwId, SQLVARCHAR);
        mssql_bind($stmt, '@notifyId', $notifyId, SQLVARCHAR);
        mssql_bind($stmt, '@customerCityId', $customerCityId, SQLVARCHAR);

        mssql_bind($stmt, '@ruta3Id', $ruta3, SQLVARCHAR);
        mssql_bind($stmt, '@grupoNavieraId', $grupoNaviera, SQLVARCHAR);

        mssql_bind($stmt, '@containerVacios', $containerVacios, SQLVARCHAR);
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;
    }

    function maerskIndicadoresNivel1Grafico($fechaDesde, $fechaHasta, $nivel1, $usuarioId) {

        //$query = "Exec maerskIndicadoresNivel1Grafico '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel1 . "','" . $usuarioId . "'";        
        //echo $query;

        $stmt = mssql_init("maerskIndicadoresNivel1Grafico");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);
        mssql_bind($stmt, '@var1', $nivel1, SQLVARCHAR);
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;
    }

    function maerskIndicadoresNivel1Contenedores($fechaDesde, $fechaHasta, $nivel1, $tipoDato, $usuarioId) {


        /* $query = "Exec manifiestoIndicadoresNivel1Contenedores '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel1 . "','" . $usuarioId . "','" . $tipoDato . "'";        
          echo $query; */

        $stmt = mssql_init("maerskIndicadoresNivel1Contenedores");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);
        mssql_bind($stmt, '@var1', $nivel1, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;
    }

    function maerskIndicadoresNivel2($fechaDesde, $fechaHasta, $nivel2, $nivel3, $nivel4, $nivel5, $tipoDato, $clienteId, $nivel1Id, $otros, $usuarioId) {


        /* $query = "Exec manifiestoIndicadoresNivel2 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel2 . "','" . $nivel3 . "','" . $nivel4 . "','" . $nivel5 . "','" . 
          $tipoDato . "','" . $clienteId . "','" . $nivel1Id . "','" . $otros . "','" . $usuarioId . "'";
          echo $query; */

        $stmt = mssql_init("maerskIndicadoresNivel2");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);
        mssql_bind($stmt, '@var2', $nivel2, SQLVARCHAR);
        mssql_bind($stmt, '@var3', $nivel3, SQLVARCHAR);
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@nivel1Id', $nivel1Id, SQLVARCHAR);
        mssql_bind($stmt, '@otros', $otros, SQLVARCHAR);
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;
    }

    function maerskIndicadoresNivel3($fechaDesde, $fechaHasta, $nivel3, $nivel4, $nivel5, $tipoDato, $clienteId, $nivel1Id, $nivel2Id, $otros, $usuarioId) {


        /* $query = "Exec manifiestoIndicadoresNivel3 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel3 . "','" . $nivel4 . "','" . $nivel5 . "','" . 
          $tipoDato . "','" . $clienteId . "','" . $nivel1Id . "','" . $nivel2Id . "','" . $otros . "','" . $usuarioId . "'";
          echo $query; */

        $stmt = mssql_init("maerskIndicadoresNivel3");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);
        mssql_bind($stmt, '@var3', $nivel3, SQLVARCHAR);
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@nivel1Id', $nivel1Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel2Id', $nivel2Id, SQLVARCHAR);
        mssql_bind($stmt, '@otros', $otros, SQLVARCHAR);
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;
    }

    function maerskIndicadoresNivel4($fechaDesde, $fechaHasta, $nivel4, $nivel5, $tipoDato, $clienteId, $nivel1Id, $nivel2Id, $nivel3Id, $otros, $usuarioId) {

        /* $query = "Exec manifiestoIndicadoresNivel4 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel4 . "','" . $nivel5 . "','" . 
          $tipoDato . "','" . $clienteId . "','" . $nivel1Id . "','" . $nivel2Id . "','" . $nivel3Id . "','" . $otros . "','" . $usuarioId . "'";
          echo $query; */

        $stmt = mssql_init("maerskIndicadoresNivel4");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@nivel1Id', $nivel1Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel2Id', $nivel2Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel3Id', $nivel3Id, SQLVARCHAR);
        mssql_bind($stmt, '@otros', $otros, SQLVARCHAR);
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;
    }

    function maerskIndicadoresNivel5($fechaDesde, $fechaHasta, $nivel5, $tipoDato, $clienteId, $nivel1Id, $nivel2Id, $nivel3Id, $nivel4Id, $otros, $usuarioId) {

        /* $query = "Exec manifiestoIndicadoresNivel5 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel5 . "','" . 
          $tipoDato . "','" . $clienteId . "','" . $nivel1Id . "','" . $nivel2Id . "','" . $nivel3Id . "','" . $nivel4Id . "','" . $otros . "','" . $usuarioId . "'";
          echo $query; */

        $stmt = mssql_init("maerskIndicadoresNivel5");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@nivel1Id', $nivel1Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel2Id', $nivel2Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel3Id', $nivel3Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel4Id', $nivel4Id, SQLVARCHAR);
        mssql_bind($stmt, '@otros', $otros, SQLVARCHAR);
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;
    }

    //REPORTES

    function maerskReporteNivel1($fechaDesde, $fechaHasta, $nivel1, $nivel2, $nivel3, $nivel4, $nivel5, $tipoDato, $clienteId, $etapa, $mercado, $nave, $naviera, $ruta, $trade, $subTrade, $portCluster, $porCountry, $porCity, $lopfiCountry, $lopfiCode, $lopCityId, $lopfiClusterId, $diplaCountryId, $diplaCodeId, $diplaCityId, $podCountryId, $podCityId, $itemId, $itemCodeId, $btnId, $commodityId, $cargoTypeId, $shipperId, $consigneeId, $ffwwId, $notifyId, $customerCityId, $ruta3, $grupoNaviera, $containerVacios, $usuarioId) {


        /* $query = "Exec manifiestoReporteNivel1 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel1 . "','" . $nivel2 . "','" . $nivel3 . "','" . $nivel4 . "','" . $nivel5 . "','" . 
          $tipoDato . "','" . $clienteId . "','" . $mercado . "','" . $etapa . "','" . $naviera . "','" . $agencia . "','" . $trafico . "','" . $puertoOrigen . "','" . $puertoEmbarque . "','" . $nave . "','" . $puertoDestino . "','" .
          $puertoDescarga . "','" . $tipoNave . "','" . $tipoServicio . "','" . $tipoCarga . "','" . $tipoEmbalaje . "','" . $paisOrigen . "','" . $paisEmbarque . "','" . $agenteDoc . "','" .
          $paisDestino . "','" . $paisDescarga . "','" . $statusContainer . "','" . $shipper . "','" . $consignee . "','" . $commodity . "','" . $granFamilia . "','" . $familia . "','" .
          $subFamilia . "','" . $dryReefer . "','" .$contenedor . "','" . $usuarioId . "'";
          echo $query; */


        $stmt = mssql_init("maerskReporteNivel1");

        mssql_bind($stmt, '@fechaDesdeA', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHastaA', $fechaHasta, SQLVARCHAR);
        mssql_bind($stmt, '@var1', $nivel1, SQLVARCHAR);
        mssql_bind($stmt, '@var2', $nivel2, SQLVARCHAR);
        mssql_bind($stmt, '@var3', $nivel3, SQLVARCHAR);
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@etapa', $etapa, SQLVARCHAR);
        mssql_bind($stmt, '@mercado', $mercado, SQLVARCHAR);
        mssql_bind($stmt, '@nave', $nave, SQLVARCHAR);
        mssql_bind($stmt, '@naviera', $naviera, SQLVARCHAR);
        mssql_bind($stmt, '@ruta', $ruta, SQLVARCHAR);
        mssql_bind($stmt, '@trade', $trade, SQLVARCHAR);
        mssql_bind($stmt, '@subTrade', $subTrade, SQLVARCHAR);
        mssql_bind($stmt, '@portCluster', $portCluster, SQLVARCHAR);
        mssql_bind($stmt, '@porCountry', $porCountry, SQLVARCHAR);
        mssql_bind($stmt, '@porCity', $porCity, SQLVARCHAR);
        mssql_bind($stmt, '@lopfiCountry', $lopfiCountry, SQLVARCHAR);
        mssql_bind($stmt, '@lopfiCode', $lopfiCode, SQLVARCHAR);
        mssql_bind($stmt, '@lopCityId', $lopCityId, SQLVARCHAR);

        mssql_bind($stmt, '@lopfiClusterId', $lopfiClusterId, SQLVARCHAR);
        mssql_bind($stmt, '@diplaCountryId', $diplaCountryId, SQLVARCHAR);
        mssql_bind($stmt, '@diplaCodeId', $diplaCodeId, SQLVARCHAR);
        mssql_bind($stmt, '@diplaCityId', $diplaCityId, SQLVARCHAR);
        mssql_bind($stmt, '@podCountryId', $podCountryId, SQLVARCHAR);
        mssql_bind($stmt, '@podCityId', $podCityId, SQLVARCHAR);
        mssql_bind($stmt, '@itemId', $itemId, SQLVARCHAR);
        mssql_bind($stmt, '@itemCodeId', $itemCodeId, SQLVARCHAR);
        mssql_bind($stmt, '@btnId', $btnId, SQLVARCHAR);
        mssql_bind($stmt, '@commodityId', $commodityId, SQLVARCHAR);
        mssql_bind($stmt, '@cargoTypeId', $cargoTypeId, SQLVARCHAR);
        mssql_bind($stmt, '@shipperId', $shipperId, SQLVARCHAR);
        mssql_bind($stmt, '@consigneeId', $consigneeId, SQLVARCHAR);
        mssql_bind($stmt, '@ffwwId', $ffwwId, SQLVARCHAR);
        mssql_bind($stmt, '@notifyId', $notifyId, SQLVARCHAR);
        mssql_bind($stmt, '@customerCityId', $customerCityId, SQLVARCHAR);

        mssql_bind($stmt, '@ruta3Id', $ruta3, SQLVARCHAR);
        mssql_bind($stmt, '@grupoNavieraId', $grupoNaviera, SQLVARCHAR);

        mssql_bind($stmt, '@containerVacios', $containerVacios, SQLVARCHAR);
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;
    }

    function maerskReporteNivel1MA($fechaDesde, $fechaHasta, $nivel1, $nivel2, $nivel3, $nivel4, $nivel5, $tipoDato, $clienteId, $etapa, $mercado, $nave, $naviera, $ruta, $trade, $subTrade, $portCluster, $porCountry, $porCity, $lopfiCountry, $lopfiCode, $lopCityId, $lopfiClusterId, $diplaCountryId, $diplaCodeId, $diplaCityId, $podCountryId, $podCityId, $itemId, $itemCodeId, $btnId, $commodityId, $cargoTypeId, $shipperId, $consigneeId, $ffwwId, $notifyId, $customerCityId, $ruta3, $grupoNaviera, $containerVacios, $usuarioId, $tipoFiltro, $baremoTOP, $baremoBTM, $participacionMIN, $participacionMAX, $top) {


        /* $query = "Exec manifiestoReporteNivel1 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel1 . "','" . $nivel2 . "','" . $nivel3 . "','" . $nivel4 . "','" . $nivel5 . "','" . 
          $tipoDato . "','" . $clienteId . "','" . $mercado . "','" . $etapa . "','" . $naviera . "','" . $agencia . "','" . $trafico . "','" . $puertoOrigen . "','" . $puertoEmbarque . "','" . $nave . "','" . $puertoDestino . "','" .
          $puertoDescarga . "','" . $tipoNave . "','" . $tipoServicio . "','" . $tipoCarga . "','" . $tipoEmbalaje . "','" . $paisOrigen . "','" . $paisEmbarque . "','" . $agenteDoc . "','" .
          $paisDestino . "','" . $paisDescarga . "','" . $statusContainer . "','" . $shipper . "','" . $consignee . "','" . $commodity . "','" . $granFamilia . "','" . $familia . "','" .
          $subFamilia . "','" . $dryReefer . "','" .$contenedor . "','" . $usuarioId . "'";
          echo $query; */

        //echo "maerskReporteNivel1MissingAccount '$fechaDesde', '$fechaHasta', '$nivel1', '$nivel2', '$nivel3', '$nivel4', '$nivel5', '$tipoDato', '$clienteId', '$etapa', '$mercado', '$nave', '$naviera', '$ruta', '$trade', '$subTrade', '$portCluster', '$porCountry', '$porCity', '$lopfiCountry', '$lopfiCode', '$lopCityId', '$lopfiClusterId', '$diplaCountryId', '$diplaCodeId', '$diplaCityId', '$podCountryId', '$podCityId', '$itemId', '$itemCodeId', '$btnId', '$commodityId', '$cargoTypeId', '$shipperId', '$consigneeId', '$ffwwId', '$notifyId', '$customerCityId', '$ruta3', '$grupoNaviera', '$containerVacios', '$usuarioId', '$tipoFiltro', '$baremoBTM', '$baremoTOP', '$participacionMIN', '$participacionMAX', '$top'";

        $stmt = mssql_init("maerskReporteNivel1MissingAccount");

        mssql_bind($stmt, '@fechaDesdeA', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHastaA', $fechaHasta, SQLVARCHAR);
        mssql_bind($stmt, '@var1', $nivel1, SQLVARCHAR);
        mssql_bind($stmt, '@var2', $nivel2, SQLVARCHAR);
        mssql_bind($stmt, '@var3', $nivel3, SQLVARCHAR);
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@etapa', $etapa, SQLVARCHAR);
        mssql_bind($stmt, '@mercado', $mercado, SQLVARCHAR);
        mssql_bind($stmt, '@nave', $nave, SQLVARCHAR);
        mssql_bind($stmt, '@naviera', $naviera, SQLVARCHAR);
        mssql_bind($stmt, '@ruta', $ruta, SQLVARCHAR);
        mssql_bind($stmt, '@trade', $trade, SQLVARCHAR);
        mssql_bind($stmt, '@subTrade', $subTrade, SQLVARCHAR);
        mssql_bind($stmt, '@portCluster', $portCluster, SQLVARCHAR);
        mssql_bind($stmt, '@porCountry', $porCountry, SQLVARCHAR);
        mssql_bind($stmt, '@porCity', $porCity, SQLVARCHAR);
        mssql_bind($stmt, '@lopfiCountry', $lopfiCountry, SQLVARCHAR);
        mssql_bind($stmt, '@lopfiCode', $lopfiCode, SQLVARCHAR);
        mssql_bind($stmt, '@lopCityId', $lopCityId, SQLVARCHAR);

        mssql_bind($stmt, '@lopfiClusterId', $lopfiClusterId, SQLVARCHAR);
        mssql_bind($stmt, '@diplaCountryId', $diplaCountryId, SQLVARCHAR);
        mssql_bind($stmt, '@diplaCodeId', $diplaCodeId, SQLVARCHAR);
        mssql_bind($stmt, '@diplaCityId', $diplaCityId, SQLVARCHAR);
        mssql_bind($stmt, '@podCountryId', $podCountryId, SQLVARCHAR);
        mssql_bind($stmt, '@podCityId', $podCityId, SQLVARCHAR);
        mssql_bind($stmt, '@itemId', $itemId, SQLVARCHAR);
        mssql_bind($stmt, '@itemCodeId', $itemCodeId, SQLVARCHAR);
        mssql_bind($stmt, '@btnId', $btnId, SQLVARCHAR);
        mssql_bind($stmt, '@commodityId', $commodityId, SQLVARCHAR);
        mssql_bind($stmt, '@cargoTypeId', $cargoTypeId, SQLVARCHAR);
        mssql_bind($stmt, '@shipperId', $shipperId, SQLVARCHAR);
        mssql_bind($stmt, '@consigneeId', $consigneeId, SQLVARCHAR);
        mssql_bind($stmt, '@ffwwId', $ffwwId, SQLVARCHAR);
        mssql_bind($stmt, '@notifyId', $notifyId, SQLVARCHAR);
        mssql_bind($stmt, '@customerCityId', $customerCityId, SQLVARCHAR);

        mssql_bind($stmt, '@ruta3Id', $ruta3, SQLVARCHAR);
        mssql_bind($stmt, '@grupoNavieraId', $grupoNaviera, SQLVARCHAR);

        mssql_bind($stmt, '@containerVacios', $containerVacios, SQLVARCHAR);
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        mssql_bind($stmt, '@tipoIndicador', $tipoFiltro, SQLCHAR);
        mssql_bind($stmt, '@baremoBTM', $baremoBTM, SQLVARCHAR);
        mssql_bind($stmt, '@baremoTOP', $baremoTOP, SQLVARCHAR);
        mssql_bind($stmt, '@participacionMIN', $participacionMIN, SQLVARCHAR);
        mssql_bind($stmt, '@participacionMAX', $participacionMAX, SQLVARCHAR);
        mssql_bind($stmt, '@topval', $top, SQLVARCHAR);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;
    }

    function maerskReporteSoWNivel1($fechaDesde, $fechaHasta, $nivel1, $nivel2, $nivel3, $nivel4, $nivel5, $tipoDato, $clienteId, $etapa, $mercado, $nave, $naviera, $ruta, $trade, $subTrade, $portCluster, $porCountry, $porCity, $lopfiCountry, $lopfiCode, $lopCityId, $lopfiClusterId, $diplaCountryId, $diplaCodeId, $diplaCityId, $podCountryId, $podCityId, $itemId, $itemCodeId, $btnId, $commodityId, $cargoTypeId, $shipperId, $consigneeId, $ffwwId, $notifyId, $customerCityId, $ruta3, $grupoNaviera, $containerVacios, $usuarioId, $participacionTOP, $participacionBTM, $top) {


        /* $query = "Exec manifiestoReporteNivel1 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel1 . "','" . $nivel2 . "','" . $nivel3 . "','" . $nivel4 . "','" . $nivel5 . "','" . 
          $tipoDato . "','" . $clienteId . "','" . $mercado . "','" . $etapa . "','" . $naviera . "','" . $agencia . "','" . $trafico . "','" . $puertoOrigen . "','" . $puertoEmbarque . "','" . $nave . "','" . $puertoDestino . "','" .
          $puertoDescarga . "','" . $tipoNave . "','" . $tipoServicio . "','" . $tipoCarga . "','" . $tipoEmbalaje . "','" . $paisOrigen . "','" . $paisEmbarque . "','" . $agenteDoc . "','" .
          $paisDestino . "','" . $paisDescarga . "','" . $statusContainer . "','" . $shipper . "','" . $consignee . "','" . $commodity . "','" . $granFamilia . "','" . $familia . "','" .
          $subFamilia . "','" . $dryReefer . "','" .$contenedor . "','" . $usuarioId . "'";
          echo $query; */

        //echo "EXEC maerskReporteNivel1ShareOfWallet '$fechaDesde', '$fechaHasta', '$nivel1', '$nivel2', '$nivel3', '$nivel4', '$nivel5', '$tipoDato', '$clienteId', '$etapa', '$mercado', '$nave', '$naviera', '$ruta', '$trade', '$subTrade', '$portCluster', '$porCountry', '$porCity', '$lopfiCountry', '$lopfiCode', '$lopCityId', '$lopfiClusterId', '$diplaCountryId', '$diplaCodeId', '$diplaCityId', '$podCountryId', '$podCityId', '$itemId', '$itemCodeId', '$btnId', '$commodityId', '$cargoTypeId', '$shipperId', '$consigneeId', '$ffwwId', '$notifyId', '$customerCityId', '$ruta3', '$grupoNaviera', '$containerVacios', '$participacionTOP', '$participacionBTM', '$top', '$usuarioId'";

        $stmt = mssql_init("maerskReporteNivel1ShareOfWallet");

        mssql_bind($stmt, '@fechaDesdeA', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHastaA', $fechaHasta, SQLVARCHAR);
        mssql_bind($stmt, '@var1', $nivel1, SQLVARCHAR);
        mssql_bind($stmt, '@var2', $nivel2, SQLVARCHAR);
        mssql_bind($stmt, '@var3', $nivel3, SQLVARCHAR);
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@etapa', $etapa, SQLVARCHAR);
        mssql_bind($stmt, '@mercado', $mercado, SQLVARCHAR);
        mssql_bind($stmt, '@nave', $nave, SQLVARCHAR);
        mssql_bind($stmt, '@naviera', $naviera, SQLVARCHAR);
        mssql_bind($stmt, '@ruta', $ruta, SQLVARCHAR);
        mssql_bind($stmt, '@trade', $trade, SQLVARCHAR);
        mssql_bind($stmt, '@subTrade', $subTrade, SQLVARCHAR);
        mssql_bind($stmt, '@portCluster', $portCluster, SQLVARCHAR);
        mssql_bind($stmt, '@porCountry', $porCountry, SQLVARCHAR);
        mssql_bind($stmt, '@porCity', $porCity, SQLVARCHAR);
        mssql_bind($stmt, '@lopfiCountry', $lopfiCountry, SQLVARCHAR);
        mssql_bind($stmt, '@lopfiCode', $lopfiCode, SQLVARCHAR);
        mssql_bind($stmt, '@lopCityId', $lopCityId, SQLVARCHAR);

        mssql_bind($stmt, '@lopfiClusterId', $lopfiClusterId, SQLVARCHAR);
        mssql_bind($stmt, '@diplaCountryId', $diplaCountryId, SQLVARCHAR);
        mssql_bind($stmt, '@diplaCodeId', $diplaCodeId, SQLVARCHAR);
        mssql_bind($stmt, '@diplaCityId', $diplaCityId, SQLVARCHAR);
        mssql_bind($stmt, '@podCountryId', $podCountryId, SQLVARCHAR);
        mssql_bind($stmt, '@podCityId', $podCityId, SQLVARCHAR);
        mssql_bind($stmt, '@itemId', $itemId, SQLVARCHAR);
        mssql_bind($stmt, '@itemCodeId', $itemCodeId, SQLVARCHAR);
        mssql_bind($stmt, '@btnId', $btnId, SQLVARCHAR);
        mssql_bind($stmt, '@commodityId', $commodityId, SQLVARCHAR);
        mssql_bind($stmt, '@cargoTypeId', $cargoTypeId, SQLVARCHAR);
        mssql_bind($stmt, '@shipperId', $shipperId, SQLVARCHAR);
        mssql_bind($stmt, '@consigneeId', $consigneeId, SQLVARCHAR);
        mssql_bind($stmt, '@ffwwId', $ffwwId, SQLVARCHAR);
        mssql_bind($stmt, '@notifyId', $notifyId, SQLVARCHAR);
        mssql_bind($stmt, '@customerCityId', $customerCityId, SQLVARCHAR);

        mssql_bind($stmt, '@ruta3Id', $ruta3, SQLVARCHAR);
        mssql_bind($stmt, '@grupoNavieraId', $grupoNaviera, SQLVARCHAR);

        mssql_bind($stmt, '@containerVacios', $containerVacios, SQLVARCHAR);


        mssql_bind($stmt, '@participacionTOP', $participacionTOP, SQLVARCHAR);
        mssql_bind($stmt, '@participacionBTM', $participacionBTM, SQLVARCHAR);
        mssql_bind($stmt, '@top', $top, SQLVARCHAR);


        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;
    }

    function maerskReporteNivel1Contenedores($fechaDesde, $fechaHasta, $nivel1, $tipoDato, $usuarioId) {
        /* $query = "Exec manifiestoReporteNivel1Contenedores '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel1 . "','" . $usuarioId . "','" . $tipoDato . "'";        
          echo $query; */

        $stmt = mssql_init("maerskReporteNivel1Contenedores");

        mssql_bind($stmt, '@fechaDesdeA', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHastaA', $fechaHasta, SQLVARCHAR);
        mssql_bind($stmt, '@var1', $nivel1, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;
    }

    function maerskReporteNivel2($fechaDesde, $fechaHasta, $nivel2, $nivel3, $nivel4, $nivel5, $tipoDato, $clienteId, $nivel1Id, $otros, $usuarioId) {


        /* $query = "Exec manifiestoReporteNivel2 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel2 . "','" . $nivel3 . "','" . $nivel4 . "','" . $nivel5 . "','" . 
          $tipoDato . "','" . $clienteId . "','" . $nivel1Id . "','" . $otros . "','" . $usuarioId . "'";
          echo $query; */

        $stmt = mssql_init("maerskReporteNivel2");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);
        mssql_bind($stmt, '@var2', $nivel2, SQLVARCHAR);
        mssql_bind($stmt, '@var3', $nivel3, SQLVARCHAR);
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@nivel1Id', $nivel1Id, SQLVARCHAR);
        mssql_bind($stmt, '@otros', $otros, SQLVARCHAR);
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;
    }

    function maerskReporteNivel3($fechaDesde, $fechaHasta, $nivel3, $nivel4, $nivel5, $tipoDato, $clienteId, $nivel1Id, $nivel2Id, $otros, $usuarioId) {


        /* $query = "Exec manifiestoReporteNivel3 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel3 . "','" . $nivel4 . "','" . $nivel5 . "','" . 
          $tipoDato . "','" . $clienteId . "','" . $nivel1Id . "','" . $nivel2Id . "','" . $otros . "','" . $usuarioId . "'";
          echo $query; */

        $stmt = mssql_init("maerskReporteNivel3");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);
        mssql_bind($stmt, '@var3', $nivel3, SQLVARCHAR);
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@nivel1Id', $nivel1Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel2Id', $nivel2Id, SQLVARCHAR);
        mssql_bind($stmt, '@otros', $otros, SQLVARCHAR);
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;
    }

    function maerskReporteNivel4($fechaDesde, $fechaHasta, $nivel4, $nivel5, $tipoDato, $clienteId, $nivel1Id, $nivel2Id, $nivel3Id, $otros, $usuarioId) {

        /* $query = "Exec manifiestoReporteNivel4 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel4 . "','" . $nivel5 . "','" . 
          $tipoDato . "','" . $clienteId . "','" . $nivel1Id . "','" . $nivel2Id . "','" . $nivel3Id . "','" . $otros . "','" . $usuarioId . "'";
          echo $query; */

        $stmt = mssql_init("maerskReporteNivel4");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);
        mssql_bind($stmt, '@var4', $nivel4, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@nivel1Id', $nivel1Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel2Id', $nivel2Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel3Id', $nivel3Id, SQLVARCHAR);
        mssql_bind($stmt, '@otros', $otros, SQLVARCHAR);
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;
    }

    function maerskReporteNivel5($fechaDesde, $fechaHasta, $nivel5, $tipoDato, $clienteId, $nivel1Id, $nivel2Id, $nivel3Id, $nivel4Id, $otros, $usuarioId) {

        /* $query = "Exec manifiestoReporteNivel5 '" . $fechaDesde . "','" . $fechaHasta . "','" . $nivel5 . "','" . 
          $tipoDato . "','" . $clienteId . "','" . $nivel1Id . "','" . $nivel2Id . "','" . $nivel3Id . "','" . $nivel4Id . "','" . $otros . "','" . $usuarioId . "'";
          echo $query; */

        $stmt = mssql_init("maerskReporteNivel5");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);
        mssql_bind($stmt, '@var5', $nivel5, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);
        mssql_bind($stmt, '@clienteId', $clienteId, SQLINT4);
        mssql_bind($stmt, '@nivel1Id', $nivel1Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel2Id', $nivel2Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel3Id', $nivel3Id, SQLVARCHAR);
        mssql_bind($stmt, '@nivel4Id', $nivel4Id, SQLVARCHAR);
        mssql_bind($stmt, '@otros', $otros, SQLVARCHAR);
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;
    }

    //EXTRACCIONES

    function maerskExtracciones($fechaDesde, $fechaHasta, $tipoDato, $clienteId, $mercado, $etapa, $contenedor, $nave, $naviera, $ruta, $trade, $subTrade, $portCluster, $porCountry, $porCity, $lopfiCountry, $lopfiCode, $lopCityId, $lopfiClusterId, $diplaCountryId, $diplaCodeId, $diplaCityId, $podCountryId, $podCityId, $itemId, $itemCodeId, $btnId, $commodityId, $cargoTypeId, $shipperId, $consigneeId, $ffwwId, $notifyId, $customerCityId, $ruta3Id, $grupoNavieraId, $chknave, $chknaviera, $chkruta, $chktrade, $chksubTrade, $chkportCluster, $chkporCountry, $chkporCity, $chklopfiCountry, $chklopfiCode, $chklopCityId, $chklopfiClusterId, $chkdiplaCountryId, $chkdiplaCodeId, $chkdiplaCityId, $chkpodCountryId, $chkpodCityId, $chkitemId, $chkitemCodeId, $chkbtnId, $chkcommodityId, $chkcargoTypeId, $chkshipperId, $chkconsigneeId, $chkffwwId, $chknotifyId, $chkcustomerCityId, $chkruta3Id, $chkgrupoNavieraId, $carpeta, $nombreArchivo, $usuarioId) {


        /* $query = "Exec manifiestoExtracciones '" . $fechaDesde . "','" . $fechaHasta . "','" . $tipoDato . "','" . $mercado . "','" . $etapa . "','" . 
          $naviera . "','" . $agencia . "','" . $trafico . "','" . $puertoOrigen . "','" . $puertoEmbarque . "','" . $nave . "','" . $puertoDestino . "','" .
          $puertoDescarga . "','" . $tipoNave . "','" . $tipoServicio . "','" . $tipoCarga . "','" . $tipoEmbalaje . "','" . $paisOrigen . "','" . $paisEmbarque . "','" . $agenteDoc . "','" .
          $paisDestino . "','" . $paisDescarga . "','" . $statusContainer . "','" . $shipper . "','" . $consignee . "','" . $commodity . "','" . $granFamilia . "','" . $familia . "','" .
          $subFamilia . "','" . $dryReefer . "','" .$contenedor . "','" .

          $chknaviera . "','" . $chkagencia . "','" . $chktrafico . "','" . $chkpuertoOrigen . "','" . $chkpuertoEmbarque . "','" . $chknave . "','" . $chkpuertoDestino . "','" .
          $chkpuertoDescarga . "','" . $chktipoNave . "','" . $chktipoServicio . "','" . $chktipoCarga . "','" . $chktipoEmbalaje . "','" . $chkpaisOrigen . "','" . $chkpaisEmbarque . "','" . $chkagenteDoc . "','" .
          $chkpaisDestino . "','" . $chkpaisDescarga . "','" . $chkstatusContainer . "','" . $chkshipper . "','" . $chkconsignee . "','" . $chkcommodity . "','" . $chkgranFamilia . "','" . $chkfamilia . "','" .
          $chksubFamilia . "','" . $chkdryReefer . "','" .$chkcontenedor . "','" . $nombreArchivo . "','" . $usuarioId . "'";
          echo $query; */


        $stmt = mssql_init("maerskExtracciones");

        mssql_bind($stmt, '@fechaDesde', $fechaDesde, SQLVARCHAR);
        mssql_bind($stmt, '@fechaHasta', $fechaHasta, SQLVARCHAR);
        mssql_bind($stmt, '@tipoDato', $tipoDato, SQLVARCHAR);


        mssql_bind($stmt, '@etapa', $etapa, SQLVARCHAR);
        mssql_bind($stmt, '@mercado', $mercado, SQLVARCHAR);
        mssql_bind($stmt, '@nave', $nave, SQLVARCHAR);
        mssql_bind($stmt, '@naviera', $naviera, SQLVARCHAR);
        mssql_bind($stmt, '@ruta', $ruta, SQLVARCHAR);
        mssql_bind($stmt, '@trade', $trade, SQLVARCHAR);
        mssql_bind($stmt, '@subTrade', $subTrade, SQLVARCHAR);
        mssql_bind($stmt, '@portCluster', $portCluster, SQLVARCHAR);
        mssql_bind($stmt, '@porCountry', $porCountry, SQLVARCHAR);
        mssql_bind($stmt, '@porCity', $porCity, SQLVARCHAR);
        mssql_bind($stmt, '@lopfiCountry', $lopfiCountry, SQLVARCHAR);
        mssql_bind($stmt, '@lopfiCode', $lopfiCode, SQLVARCHAR);
        mssql_bind($stmt, '@lopCityId', $lopCityId, SQLVARCHAR);

        mssql_bind($stmt, '@lopfiClusterId', $lopfiClusterId, SQLVARCHAR);
        mssql_bind($stmt, '@diplaCountryId', $diplaCountryId, SQLVARCHAR);
        mssql_bind($stmt, '@diplaCodeId', $diplaCodeId, SQLVARCHAR);
        mssql_bind($stmt, '@diplaCityId', $diplaCityId, SQLVARCHAR);
        mssql_bind($stmt, '@podCountryId', $podCountryId, SQLVARCHAR);
        mssql_bind($stmt, '@podCityId', $podCityId, SQLVARCHAR);
        mssql_bind($stmt, '@itemId', $itemId, SQLVARCHAR);
        mssql_bind($stmt, '@itemCodeId', $itemCodeId, SQLVARCHAR);
        mssql_bind($stmt, '@btnId', $btnId, SQLVARCHAR);
        mssql_bind($stmt, '@commodityId', $commodityId, SQLVARCHAR);
        mssql_bind($stmt, '@cargoTypeId', $cargoTypeId, SQLVARCHAR);
        mssql_bind($stmt, '@shipperId', $shipperId, SQLVARCHAR);
        mssql_bind($stmt, '@consigneeId', $consigneeId, SQLVARCHAR);
        mssql_bind($stmt, '@ffwwId', $ffwwId, SQLVARCHAR);
        mssql_bind($stmt, '@notifyId', $notifyId, SQLVARCHAR);
        mssql_bind($stmt, '@customerCityId', $customerCityId, SQLVARCHAR);

        mssql_bind($stmt, '@ruta3Id', $ruta3Id, SQLVARCHAR);
        mssql_bind($stmt, '@grupoNavieraId', $grupoNavieraId, SQLVARCHAR);

        mssql_bind($stmt, '@containerVacios', $contenedor, SQLVARCHAR);

        mssql_bind($stmt, '@chknave', $chknave, SQLVARCHAR);
        mssql_bind($stmt, '@chknaviera', $chknaviera, SQLVARCHAR);
        mssql_bind($stmt, '@chkruta', $chkruta, SQLVARCHAR);
        mssql_bind($stmt, '@chktrade', $chktrade, SQLVARCHAR);
        mssql_bind($stmt, '@chksubTrade', $chksubTrade, SQLVARCHAR);
        mssql_bind($stmt, '@chkportCluster', $chkportCluster, SQLVARCHAR);
        mssql_bind($stmt, '@chkporCountry', $chkporCountry, SQLVARCHAR);
        mssql_bind($stmt, '@chkporCity', $chkporCity, SQLVARCHAR);
        mssql_bind($stmt, '@chklopfiCountry', $chklopfiCountry, SQLVARCHAR);
        mssql_bind($stmt, '@chklopfiCode', $chklopfiCode, SQLVARCHAR);
        mssql_bind($stmt, '@chklopCityId', $chklopCityId, SQLVARCHAR);

        mssql_bind($stmt, '@chklopfiClusterId', $chklopfiClusterId, SQLVARCHAR);
        mssql_bind($stmt, '@chkdiplaCountryId', $chkdiplaCountryId, SQLVARCHAR);
        mssql_bind($stmt, '@chkdiplaCodeId', $chkdiplaCodeId, SQLVARCHAR);
        mssql_bind($stmt, '@chkdiplaCityId', $chkdiplaCityId, SQLVARCHAR);
        mssql_bind($stmt, '@chkpodCountryId', $chkpodCountryId, SQLVARCHAR);
        mssql_bind($stmt, '@chkpodCityId', $chkpodCityId, SQLVARCHAR);
        mssql_bind($stmt, '@chkitemId', $chkitemId, SQLVARCHAR);
        mssql_bind($stmt, '@chkitemCodeId', $chkitemCodeId, SQLVARCHAR);
        mssql_bind($stmt, '@chkbtnId', $chkbtnId, SQLVARCHAR);
        mssql_bind($stmt, '@chkcommodityId', $chkcommodityId, SQLVARCHAR);
        mssql_bind($stmt, '@chkcargoTypeId', $chkcargoTypeId, SQLVARCHAR);
        mssql_bind($stmt, '@chkshipperId', $chkshipperId, SQLVARCHAR);
        mssql_bind($stmt, '@chkconsigneeId', $chkconsigneeId, SQLVARCHAR);
        mssql_bind($stmt, '@chkffwwId', $chkffwwId, SQLVARCHAR);
        mssql_bind($stmt, '@chknotifyId', $chknotifyId, SQLVARCHAR);
        mssql_bind($stmt, '@chkcustomerCityId', $chkcustomerCityId, SQLVARCHAR);

        mssql_bind($stmt, '@chkruta3Id', $chkruta3Id, SQLVARCHAR);
        mssql_bind($stmt, '@chkgrupoNavieraId', $chkgrupoNavieraId, SQLVARCHAR);

        mssql_bind($stmt, '@carpeta', $carpeta, SQLVARCHAR);
        mssql_bind($stmt, '@nombreArchivo', $nombreArchivo, SQLVARCHAR);
        mssql_bind($stmt, '@usuarioId', $usuarioId, SQLINT4);

        $result = sql_db::sql_ejecutar_sp($stmt);
        sql_db::sql_close();
        return $result;
    }

    function manifiestoExtraccionesArchivos($usuarioId, $moduloId) {
        $query = "Select * From Extraccion_Reportes (nolock) Where usuarioId = '" . $usuarioId . "' and modulo = '" . $moduloId . "' Order by fecha_reporte desc";
        //echo $query;
        $result = sql_db::sql_query($query);
        sql_db::sql_close();
        return $result;
    }

    function manifiestoExtraccionesSeleccionarArchivo($usuarioId, $moduloId, $arhivoId) {
        $query = "Select nombre_archivo From Extraccion_Reportes (nolock) Where usuarioId = '" . $usuarioId . "' and modulo = '" . $moduloId . "' and id_archivo = '" . $arhivoId . "'";
        $result = sql_db::sql_query($query);
        $row = sql_db::sql_fetch_assoc($result);
        sql_db::sql_close();
        return $row;
    }

    function manifiestoExtraccionesEliminarArchivo($usuarioId, $moduloId, $arhivoId) {
        $query = "Delete e From Extraccion_Reportes e Where usuarioId = '" . $usuarioId . "' and modulo = '" . $moduloId . "' and id_archivo = '" . $arhivoId . "'";
        sql_db::sql_query($query);
        sql_db::sql_close();
        return true;
    }

}
?>


