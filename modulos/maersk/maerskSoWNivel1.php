<?PHP

session_start();
include ("../../librerias/conexion.php");
require('maersk.class.php');
require('../../clases/sempat.class.php');
$objManifiesto = new maersk();
$objSempat = new sempat();


$maximoStringLength = 30;

$usuarioId = $_SESSION['SEMPAT_usuarioId'];
$clienteId = $_SESSION['SEMPAT_clienteId'];

$fechaDesde = mb_convert_encoding(trim($_POST['fechaDesde']), "ISO-8859-1", "UTF-8");
$fechaHasta = mb_convert_encoding(trim($_POST['fechaHasta']), "ISO-8859-1", "UTF-8");


$participacionTOP = mb_convert_encoding(trim($_POST['participacionTOP']), "ISO-8859-1", "UTF-8");
$participacionBTM = mb_convert_encoding(trim($_POST['participacionBTM']), "ISO-8859-1", "UTF-8");
$top = mb_convert_encoding(trim($_POST['top']), "ISO-8859-1", "UTF-8");


$nivel1 = mb_convert_encoding(trim($_POST['nivel1']), "ISO-8859-1", "UTF-8");
$nivel2 = mb_convert_encoding(trim($_POST['nivel2']), "ISO-8859-1", "UTF-8");
$nivel3 = mb_convert_encoding(trim($_POST['nivel3']), "ISO-8859-1", "UTF-8");
$nivel4 = mb_convert_encoding(trim($_POST['nivel4']), "ISO-8859-1", "UTF-8");
$nivel5 = mb_convert_encoding(trim($_POST['nivel5']), "ISO-8859-1", "UTF-8");

$nivel1Titulo = mb_convert_encoding(trim($_POST['nivel1Titulo']), "ISO-8859-1", "UTF-8");
$contenedor = mb_convert_encoding(trim($_POST['contenedor']), "ISO-8859-1", "UTF-8");


$etapa = mb_convert_encoding(trim($_POST['etapa']), "ISO-8859-1", "UTF-8");
$mercado = mb_convert_encoding(trim($_POST['mercado']), "ISO-8859-1", "UTF-8");
$nave = mb_convert_encoding(trim($_POST['nave']), "ISO-8859-1", "UTF-8");
$naviera = mb_convert_encoding(trim($_POST['naviera']), "ISO-8859-1", "UTF-8");

$ruta = mb_convert_encoding(trim($_POST['ruta']), "ISO-8859-1", "UTF-8");
$trade = mb_convert_encoding(trim($_POST['trade']), "ISO-8859-1", "UTF-8");
$subTrade = mb_convert_encoding(trim($_POST['subTrade']), "ISO-8859-1", "UTF-8");
$portCluster = mb_convert_encoding(trim($_POST['portCluster']), "ISO-8859-1", "UTF-8");
$porCountry = mb_convert_encoding(trim($_POST['porCountry']), "ISO-8859-1", "UTF-8");

$porCity = mb_convert_encoding(trim($_POST['porCity']), "ISO-8859-1", "UTF-8");
$lopfiCountry = mb_convert_encoding(trim($_POST['lopfiCountry']), "ISO-8859-1", "UTF-8");
$lopfiCode = mb_convert_encoding(trim($_POST['lopfiCode']), "ISO-8859-1", "UTF-8");
$lopCityId = mb_convert_encoding(trim($_POST['lopCity']), "ISO-8859-1", "UTF-8");
$lopfiClusterId = mb_convert_encoding(trim($_POST['lopfiCluster']), "ISO-8859-1", "UTF-8");
$diplaCountryId = mb_convert_encoding(trim($_POST['diplaCountry']), "ISO-8859-1", "UTF-8");
$diplaCodeId = mb_convert_encoding(trim($_POST['diplaCode']), "ISO-8859-1", "UTF-8");
$diplaCityId = mb_convert_encoding(trim($_POST['diplaCity']), "ISO-8859-1", "UTF-8");
$podCountryId = mb_convert_encoding(trim($_POST['podCountry']), "ISO-8859-1", "UTF-8");
$podCityId = mb_convert_encoding(trim($_POST['podCity']), "ISO-8859-1", "UTF-8");

$itemId = mb_convert_encoding(trim($_POST['item']), "ISO-8859-1", "UTF-8");
$itemCodeId = mb_convert_encoding(trim($_POST['itemCode']), "ISO-8859-1", "UTF-8");
$btnId = mb_convert_encoding(trim($_POST['btn']), "ISO-8859-1", "UTF-8");
$commodityId = mb_convert_encoding(trim($_POST['commodity']), "ISO-8859-1", "UTF-8");
$cargoTypeId = mb_convert_encoding(trim($_POST['cargoType']), "ISO-8859-1", "UTF-8");

$shipperId = mb_convert_encoding(trim($_POST['shipper']), "ISO-8859-1", "UTF-8");
$consigneeId = mb_convert_encoding(trim($_POST['consignee']), "ISO-8859-1", "UTF-8");
$ffwwId = mb_convert_encoding(trim($_POST['ffww']), "ISO-8859-1", "UTF-8");
$notifyId = mb_convert_encoding(trim($_POST['notify']), "ISO-8859-1", "UTF-8");
$customerCityId = mb_convert_encoding(trim($_POST['customerCity']), "ISO-8859-1", "UTF-8");

$ruta3 = mb_convert_encoding(trim($_POST['ruta3']), "ISO-8859-1", "UTF-8");
$grupoNaviera = mb_convert_encoding(trim($_POST['grupoNaviera']), "ISO-8859-1", "UTF-8");

$consultaTabla = $objManifiesto->maerskReporteSoWNivel1(
        $fechaDesde, $fechaHasta, $nivel1, $nivel2, $nivel3, $nivel4, $nivel5, $tipoDato, $clienteId, $etapa, $mercado, $nave, $naviera, $ruta, $trade, $subTrade, $portCluster, $porCountry, $porCity, $lopfiCountry, $lopfiCode, $lopCityId, $lopfiClusterId, $diplaCountryId, $diplaCodeId, $diplaCityId, $podCountryId, $podCityId, $itemId, $itemCodeId, $btnId, $commodityId, $cargoTypeId, $shipperId, $consigneeId, $ffwwId, $notifyId, $customerCityId, $ruta3, $grupoNaviera, $contenedor, $usuarioId, $participacionTOP, $participacionBTM, $top);


$meses = array();

$mesInicial = substr($fechaDesde, 3, 2);
$añoInicial = substr($fechaDesde, 6, 4);

$mesFinal = substr($fechaHasta, 3, 2);
$añoFinal = substr($fechaHasta, 6, 4);

for ($a = 0; $a < 12; $a++) {

    $mes = "00" . ($mesInicial);
    $mes = substr($mes, strlen($mes) - 2, strlen($mes));

    $meses[$a][0] = $añoInicial . "-" . $mes;

    if ($mes >= $mesFinal && $añoInicial >= $añoFinal) {
        $a = 12;
    }

    if ($mes == 12) {
        $añoInicial++;
        $mesInicial = "01";
    } else {
        $mesInicial++;
    }
}


$cabeceraHTML = "";
//$cabeceraHTML2 = "<tr><td colspan='2' class='left' style='width:185px;'>" . htmlentities(strtoupper($nivel1Titulo)) . "</td>";
$cabeceraHTML2 = "<tr><td colspan='2' class='left' style='width:225px; background-image: url(../../imagenes/theader.png);background-size: 100%; background-position: center;'>" . htmlentities(strtoupper($nivel1Titulo)) . "</td>";
$colspan = 5;

for ($i = 0; $i < count($meses); $i++) {
    $cabeceraHTML2 .= "<td>" . $meses[$i][0] . "</td>";
    $colspan++;
}

/*
$cabeceraHTML = "<table id='tablaReporte' class='tablaValores' cellspacing='0' cellspadding='0'>
                    <thead>
                        <tr>            
                            <th class='thCenter' colspan='" . $colspan . "'>
                                <div class='divThLeft'></div>          
                                <div class='divThCenter'>" . tituloCabecera($fechaDesde, $fechaHasta, 2) . "</div>
                                <div class='divThRight'></div>          
                            </th>                                                
                        </tr>" . $cabeceraHTML2 . "<td>TOTAL</td><td>TOTAL ANT.</td><td>DIF.</td> </thead>";
*/
$cabeceraHTML = "<table id='tablaReporte' class='tablaValores' cellspacing='0' cellspadding='0'>
                    <thead>
                        <tr>            
                            <th class='thCenter' colspan='" . $colspan . "'>
                                <div class='divThLeft'></div>          
                                <div class='divThCenter'>" . tituloCabecera($fechaDesde, $fechaHasta, 2) . "</div>
                                <div class='divThRight'></div>          
                            </th>                                                
                        </tr>" . $cabeceraHTML2 . "<td>TOTAL</td><td>Mkt&nbsp;Share</td> </thead>";


$campo1 = "";
$valorCampo1 = "";
$salida = "";
$salidaOtros = "";
$total = 0;
$totalAnterior = 0;

$fila = 0;

$cursor = "";

if ($nivel2 != "") {
    $cursor = " style='cursor:pointer'";
}

$stotal = 0;

if ($consultaTabla) {

    if (mssql_num_fields($consultaTabla) > 1) {

        $numFilas = mssql_num_rows($consultaTabla);

        while ($manifiesto = mssql_fetch_array($consultaTabla)) {

            $fila++;

            $campo1 = htmlentities($manifiesto[1]);

            if (strlen($campo1) > 24) {
                $valorCampo1 = substr($campo1, 0, 19) . "...";
            } else {
                $valorCampo1 = $campo1;
            }

            if ($campo1 != "OTROS") {
                $salida .= "<tr id='tr-" . $fila . "' class='nivel1'><td class='check'></td><td class='nivel1' " . $cursor . " title =\"" . $campo1 . "\" onClick='buscarNivel2sow(this, \"" . $fechaDesde . "\", \"" . $fechaHasta . "\", \"" . $nivel2 . "\", \"" . $nivel3 . "\", \"" . $nivel4 . "\", \"" . $nivel5 . "\", \"" . $tipoDato . "\", \"" . $manifiesto[2] . "\", \"" . round($manifiesto[16]) . "\", \"" . round($manifiesto[16]) . "\"); findDataForGrafic(this)  '>" . $valorCampo1 . "</td>";
                //$salida .= "<tr id='tr-" . $fila . "' class='nivel1'><td class='check'><input class='styled' type='checkbox' onClick='graficoCheckboxReporte(this);'></td><td class='nivel1' " . $cursor . " title =\"" . $campo1 . "\" onClick='buscarNivel2(this, \"" . $fechaDesde . "\", \"" . $fechaHasta . "\", \"" . $nivel2 . "\", \"" . $nivel3 . "\", \"" . $nivel4 . "\", \"" . $nivel5 . "\", \"" . $tipoDato . "\", \"" . $manifiesto[2] . "\");'>" . $valorCampo1 . "</td>";

                for ($i = 0; $i < count($meses); $i++) {

                    $salida .= "<td>" . number_format($manifiesto[3 + $i], 0, '', '.') . "</td>";
                    $meses[$i][$fila] .= number_format($manifiesto[3 + $i], 0, '', '');
                }

                /*
                $salida .= "<td><b>" . number_format($manifiesto[16], 0, '', '.') . "</b></td><td><b>" . number_format($manifiesto[15], 0, '', '.') . "</b></td>";
                $salida .= $objSempat->calculoDiferencia($manifiesto[16], $manifiesto[15]) . "</tr>";
*/
                $salida .= "<td><b>" . number_format($manifiesto[16], 0, '', '.') . "</b></td><td>{" . round($manifiesto[16]) . "}</td>";
                $salida .= "</tr>";
                
                $total = $total + number_format($manifiesto[16], 0, '', '');
                $stotal += round($manifiesto[16]);
                $totalAnterior = $totalAnterior + number_format($manifiesto[15], 0, '', '');
            } else {

                if ($manifiesto[16] != "0" && $manifiesto[16] != "") {
                    $salidaOtros .= "<tr id='tr-" . ($numFilas + 1) . "' class='nivel1'><td class='check'><input type='checkbox' class='styled' onClick='graficoCheckboxReporte(this);'/></td><td class='nivel1' title =\"" . $campo1 . "\">" . $valorCampo1 . "</td>";

                    for ($i = 0; $i < count($meses); $i++) {

                        $salidaOtros .= "<td>" . number_format($manifiesto[3 + $i], 0, '', '.') . "</td>";
                        $meses[$i][$numFilas + 1] .= number_format($manifiesto[3 + $i], 0, '', '');
                    }
                    /*
                    $salidaOtros .= "<td><b>" . number_format($manifiesto[16], 0, '', '.') . "</b></td><td><b>" . number_format($manifiesto[15], 0, '', '.') . "</b></td>";
                    $salidaOtros .= $objSempat->calculoDiferencia($manifiesto[16], $manifiesto[15]) . "</tr>";
                    */
                    
                    $salidaOtros .= "<td><b>" . number_format($manifiesto[16], 0, '', '.') . "</b></td><td>{" . round($manifiesto[16]) . "}</td>";
                    $salidaOtros .= "</tr>";
                    
                    $total = $total + number_format($manifiesto[16], 0, '', '');
                    $stotal += round($manifiesto[16]);
                    $totalAnterior = $totalAnterior + number_format($manifiesto[15], 0, '', '');
                }
            }
        }

        $salidaTotal = "<tr class='nivelTotal'><td class='check'></td><td class='nivelTotal'>TOTAL</td>";

        $htmlTablaGrafico = "<table id='tablaGrafico'><thead><tr><th></th></tr></thead><tbody>";

        for ($x = 0; $x < count($meses); $x++) {

            $htmlTablaGrafico .= "<tr><th>" . retornarPeriodo($meses[$x][0]) . "</th>";

            $valor = "0";

            for ($i = 1; $i <= ($numFilas + 1); $i++) {
                $valor = $valor + number_format($meses[$x][$i], 0, '', '');
            }

            $salidaTotal .= "<td>" . number_format($valor, 0, '', '.') . "</td>";
        }

        //$salidaTotal .= "<td>" . number_format($total, 0, '', '.') . "</td><td>" . number_format($totalAnterior, 0, '', '.') . "</td>" . $objSempat->calculoDiferencia($total, $totalAnterior) . "</tr>";
        $salidaTotal .= "<td>" . number_format($total, 0, '', '.') . "</td><td>100%</td></tr>";

        $bodyHTML = "<tbody id='tBody'>" . $salida . $salidaOtros . $salidaTotal . "</tbody>";


        $htmlTablaGrafico .= "</tr></tbody></table>";

        $htmlTablaGrafico .= "<table id=\"tablaActual\">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>

                            <table id=\"tablaAnterior\">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>";



        $bodyHTMLC20 = "";
        $bodyHTMLC40 = "";

        //<editor-fold>
        if ($tipoDato == "contenedores") {

            $consultaTabla = $objManifiesto->maerskReporteNivel1Contenedores($fechaDesde, $fechaHasta, $nivel1, "totalC20", $usuarioId);

            //<editor-fold>
            if ($consultaTabla) {

                $numFilas = mssql_num_rows($consultaTabla);
                $fila = 0;
                $salidaC20 = "";
                $salidaOtrosC20 = "";
                $total = 0;
                $totalAnterior = 0;
                $mesesC20 = array();

                while ($manifiesto = mssql_fetch_array($consultaTabla)) {

                    $fila++;

                    $campo1 = htmlentities($manifiesto[1]);

                    if (strlen($campo1) > 24) {
                        $valorCampo1 = substr($campo1, 0, 19) . "...";
                    } else {
                        $valorCampo1 = $campo1;
                    }

                    if ($campo1 != "OTROS") {

                        $salidaC20 .= "<tr id='trC20-" . $fila . "' class='nivel1'><td class='check'><input class='styled' type='checkbox' onClick='graficoCheckboxReporte(this);'></td><td class='nivel1' " . $cursor . " title =\"" . $campo1 . "\" onClick='buscarNivel2(this, \"" . $fechaDesde . "\", \"" . $fechaHasta . "\", \"" . $nivel2 . "\", \"" . $nivel3 . "\", \"" . $nivel4 . "\", \"" . $nivel5 . "\", \"" . $tipoDato . "C20\", \"" . $manifiesto[2] . "\");'>" . $valorCampo1 . "</td>";

                        for ($i = 0; $i < count($meses); $i++) {

                            $salidaC20 .= "<td>" . number_format($manifiesto[3 + $i], 0, '', '.') . "</td>";
                            $mesesC20[$i][$fila] .= number_format($manifiesto[3 + $i], 0, '', '');
                        }

                        $salidaC20 .= "<td><b>" . number_format($manifiesto[16], 0, '', '.') . "</b></td><td><b>" . number_format($manifiesto[15], 0, '', '.') . "</b></td>";
                        $salidaC20 .= $objSempat->calculoDiferencia($manifiesto[16], $manifiesto[15]) . "</tr>";

                        $total = $total + number_format($manifiesto[16], 0, '', '');
                        $totalAnterior = $totalAnterior + number_format($manifiesto[15], 0, '', '');
                    } else {

                        if ($manifiesto[16] != "0" && $manifiesto[16] != "") {
                            $salidaOtrosC20 .= "<tr id='trC20-" . ($numFilas + 1) . "' class='nivel1'><td class='check'><input type='checkbox' class='styled' onClick='graficoCheckboxReporte(this);'/></td><td class='nivel1' title =\"" . $campo1 . "\">" . $valorCampo1 . "</td>";

                            for ($i = 0; $i < count($meses); $i++) {

                                $salidaOtrosC20 .= "<td>" . number_format($manifiesto[3 + $i], 0, '', '.') . "</td>";
                                $mesesC20[$i][$numFilas + 1] .= number_format($manifiesto[3 + $i], 0, '', '');
                            }

                            $salidaOtrosC20 .= "<td><b>" . number_format($manifiesto[16], 0, '', '.') . "</b></td><td><b>" . number_format($manifiesto[15], 0, '', '.') . "</b></td>";
                            $salidaOtrosC20 .= $objSempat->calculoDiferencia($manifiesto[16], $manifiesto[15]) . "</tr>";

                            $total = $total + number_format($manifiesto[16], 0, '', '');
                            $totalAnterior = $totalAnterior + number_format($manifiesto[15], 0, '', '');
                        }
                    }
                }

                $salidaTotal = "<tr class='nivelTotal'><td class='check'></td><td class='nivelTotal'>TOTAL</td>";


                for ($x = 0; $x < count($meses); $x++) {

                    $valor = "0";

                    for ($i = 0; $i <= ($numFilas + 1); $i++) {
                        $valor = $valor + number_format($mesesC20[$x][$i], 0, '', '');
                    }

                    $salidaTotal .= "<td>" . number_format($valor, 0, '', '.') . "</td>";
                }

                $salidaTotal .= "<td>" . number_format($total, 0, '', '.') . "</td><td>" . number_format($totalAnterior, 0, '', '.') . "</td>" . $objSempat->calculoDiferencia($total, $totalAnterior) . "</tr>";

                $bodyHTMLC20 = "<tbody id='tBodyC20' style='display:none'>" . $salidaC20 . $salidaOtrosC20 . $salidaTotal . "</tbody>";
            }
            //</editor-fold>
            //<editor-fold>
            $consultaTabla = $objManifiesto->maerskReporteNivel1Contenedores($fechaDesde, $fechaHasta, $nivel1, "totalC40", $usuarioId);

            if ($consultaTabla) {

                $numFilas = mssql_num_rows($consultaTabla);
                $fila = 0;
                $salidaC40 = "";
                $salidaOtrosC40 = "";
                $total = 0;
                $totalAnterior = 0;
                $mesesC40 = array();

                while ($manifiesto = mssql_fetch_array($consultaTabla)) {

                    $fila++;

                    $campo1 = htmlentities($manifiesto[1]);

                    if (strlen($campo1) > 24) {
                        $valorCampo1 = substr($campo1, 0, 19) . "...";
                    } else {
                        $valorCampo1 = $campo1;
                    }

                    if ($campo1 != "OTROS") {

                        $salidaC40 .= "<tr id='trC40-" . $fila . "' class='nivel1'><td class='check'><input class='styled' type='checkbox' onClick='graficoCheckboxReporte(this);'></td><td class='nivel1' " . $cursor . " title =\"" . $campo1 . "\" onClick='buscarNivel2(this, \"" . $fechaDesde . "\", \"" . $fechaHasta . "\", \"" . $nivel2 . "\", \"" . $nivel3 . "\", \"" . $nivel4 . "\", \"" . $nivel5 . "\", \"" . $tipoDato . "C40\", \"" . $manifiesto[2] . "\");'>" . $valorCampo1 . "</td>";

                        for ($i = 0; $i < count($meses); $i++) {

                            $salidaC40 .= "<td>" . number_format($manifiesto[3 + $i], 0, '', '.') . "</td>";
                            $mesesC40[$i][$fila] .= number_format($manifiesto[3 + $i], 0, '', '');
                        }

                        $salidaC40 .= "<td><b>" . number_format($manifiesto[16], 0, '', '.') . "</b></td><td><b>" . number_format($manifiesto[15], 0, '', '.') . "</b></td>";
                        $salidaC40 .= $objSempat->calculoDiferencia($manifiesto[16], $manifiesto[15]) . "</tr>";

                        $total = $total + number_format($manifiesto[16], 0, '', '');
                        $totalAnterior = $totalAnterior + number_format($manifiesto[15], 0, '', '');
                    } else {

                        if ($manifiesto[16] != "0" && $manifiesto[16] != "") {
                            $salidaOtrosC40 .= "<tr id='trC40-" . ($numFilas + 1) . "' class='nivel1'><td class='check'><input type='checkbox' class='styled' onClick='graficoCheckboxReporte(this);'/></td><td class='nivel1' title =\"" . $campo1 . "\">" . $valorCampo1 . "</td>";

                            for ($i = 0; $i < count($meses); $i++) {

                                $salidaOtrosC40 .= "<td>" . number_format($manifiesto[3 + $i], 0, '', '.') . "</td>";
                                $mesesC40[$i][$numFilas + 1] .= number_format($manifiesto[3 + $i], 0, '', '');
                            }

                            $salidaOtrosC40 .= "<td><b>" . number_format($manifiesto[16], 0, '', '.') . "</b></td><td><b>" . number_format($manifiesto[15], 0, '', '.') . "</b></td>";
                            $salidaOtrosC40 .= $objSempat->calculoDiferencia($manifiesto[16], $manifiesto[15]) . "</tr>";

                            $total = $total + number_format($manifiesto[16], 0, '', '');
                            $totalAnterior = $totalAnterior + number_format($manifiesto[15], 0, '', '');
                        }
                    }
                }

                $salidaTotal = "<tr class='nivelTotal'><td class='check'></td><td class='nivelTotal'>TOTAL</td>";


                for ($x = 0; $x < count($meses); $x++) {

                    $valor = "0";

                    for ($i = 0; $i <= ($numFilas + 1); $i++) {
                        $valor = $valor + number_format($mesesC40[$x][$i], 0, '', '');
                    }

                    $salidaTotal .= "<td>" . number_format($valor, 0, '', '.') . "</td>";
                }

                $salidaTotal .= "<td>" . number_format($total, 0, '', '.') . "</td><td>" . number_format($totalAnterior, 0, '', '.') . "</td>" . $objSempat->calculoDiferencia($total, $totalAnterior) . "</tr>";

                $bodyHTMLC40 = "<tbody id='tBodyC40' style='display:none'>" . $salidaC40 . $salidaOtrosC40 . $salidaTotal . "</tbody>";
            }
            //</editor-fold>
        }
        //</editor-fold>

        //=================================================================

        $stotal = round($stotal);

        //$bodyHTML = str_replace('{', '[', $bodyHTML);
        while (strpos($bodyHTML, '{') !== false) {
            $x1 = strpos($bodyHTML, '{');
            $x2 = strpos($bodyHTML, '}');
            $left = substr($bodyHTML, 0, $x1);
            $right = substr($bodyHTML, $x2 + 1);

            $middle = substr($bodyHTML, $x1 + 1, $x2 - $x1 - 1);
            $cporc = round((100 * $middle) / $stotal, 2);

            $bodyHTML = $left . '' . $cporc . '%' . $right;
        }
//        
        //=================================================================
        
        echo "<div id='divDatosGrafico' style='display:none'>" . $htmlTablaGrafico . "</div>" . $cabeceraHTML . $bodyHTML . $bodyHTMLC20 . $bodyHTMLC40 . "</table>";
    }
} else {
    echo "Sin resultado";
}

function tituloCabecera($fechaDesde, $fechaHasta, $tipo) {

    $mesesA = array("", "ENE", "FEB", "MAR", "ABR", "MAY", "JUN", "JUL", "AGO", "SEP", "OCT", "NOV", "DIC");
    $mesesB = array("", "ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE");

    $mDesde = date("m", strtotime($fechaDesde));
    $yDesde = date("Y", strtotime($fechaDesde));

    $mHasta = date("m", strtotime($fechaHasta));
    $yHasta = date("Y", strtotime($fechaHasta));

    $titulo = "";

    if ($tipo == 1) {

        if ($mDesde == $mHasta && $yDesde == $yHasta) {
            $titulo = $mesesA[(int) $mDesde] . " " . $yDesde;
        } else {
            $titulo = $mesesA[(int) $mDesde] . " " . $yDesde . " - " . $mesesA[(int) $mHasta] . " " . $yHasta;
        }
    }

    if ($tipo == 2) {
        if ($mDesde == $mHasta && $yDesde == $yHasta) {
            $titulo = $mesesB[(int) $mDesde] . " " . $yDesde;
        } else {
            $titulo = $mesesB[(int) $mDesde] . " " . $yDesde . " - " . $mesesB[(int) $mHasta] . " " . $yHasta;
        }
    }

    return $titulo;
}

function retornarPeriodo($valor) {
    $meses = array("", "ENE", "FEB", "MAR", "ABR", "MAY", "JUN", "JUL", "AGO", "SEP", "OCT", "NOV", "DIC");

    $datos = split("-", $valor);

    return $meses[$datos[1] * 1] . "-" . $datos[0];
}

//echo $salida;
?>
