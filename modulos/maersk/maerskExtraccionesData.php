<?PHP

session_start();
include ("../../librerias/conexion.php");
require('maersk.class.php');
$objManifiesto = new maersk();


$usuarioId = $_SESSION['SEMPAT_usuarioId'];
$clienteId = $_SESSION['SEMPAT_clienteId'];

$fechaDesde = mb_convert_encoding(trim($_POST['fechaDesde']), "ISO-8859-1", "UTF-8");
$fechaHasta = mb_convert_encoding(trim($_POST['fechaHasta']), "ISO-8859-1", "UTF-8");
$carpeta = mb_convert_encoding(trim($_POST['carpeta']), "ISO-8859-1", "UTF-8");

$contenedor = mb_convert_encoding(trim($_POST['contenedor']), "ISO-8859-1", "UTF-8");
$dryReefer = mb_convert_encoding(trim($_POST['dryReefer']), "ISO-8859-1", "UTF-8");

$nombreArchivo = mb_convert_encoding(trim($_POST['nombreArchivo']), "ISO-8859-1", "UTF-8");
$tipoDato = mb_convert_encoding(trim($_POST['tipoDato']), "ISO-8859-1", "UTF-8");



$etapa = mb_convert_encoding(trim($_POST['etapa']), "ISO-8859-1", "UTF-8");
$mercado = mb_convert_encoding(trim($_POST['mercado']), "ISO-8859-1", "UTF-8");
$nave = mb_convert_encoding(trim($_POST['nave']), "ISO-8859-1", "UTF-8");
$naviera = mb_convert_encoding(trim($_POST['naviera']), "ISO-8859-1", "UTF-8");

$ruta = mb_convert_encoding(trim($_POST['ruta']), "ISO-8859-1", "UTF-8");
$trade = mb_convert_encoding(trim($_POST['trade']), "ISO-8859-1", "UTF-8");
$subTrade = mb_convert_encoding(trim($_POST['subTrade']), "ISO-8859-1", "UTF-8");
$portCluster = mb_convert_encoding(trim($_POST['portCluster']), "ISO-8859-1", "UTF-8");
$porCountry = mb_convert_encoding(trim($_POST['porCountry']), "ISO-8859-1", "UTF-8");

$porCity = mb_convert_encoding(trim($_POST['porCity']), "ISO-8859-1", "UTF-8");
$lopfiCountry = mb_convert_encoding(trim($_POST['lopfiCountry']), "ISO-8859-1", "UTF-8");
$lopfiCode = mb_convert_encoding(trim($_POST['lopfiCode']), "ISO-8859-1", "UTF-8");
$lopCityId = mb_convert_encoding(trim($_POST['lopCity']), "ISO-8859-1", "UTF-8");
$lopfiClusterId = mb_convert_encoding(trim($_POST['lopfiCluster']), "ISO-8859-1", "UTF-8");
$diplaCountryId = mb_convert_encoding(trim($_POST['diplaCountry']), "ISO-8859-1", "UTF-8");
$diplaCodeId = mb_convert_encoding(trim($_POST['diplaCode']), "ISO-8859-1", "UTF-8");
$diplaCityId = mb_convert_encoding(trim($_POST['diplaCity']), "ISO-8859-1", "UTF-8");
$podCountryId = mb_convert_encoding(trim($_POST['podCountry']), "ISO-8859-1", "UTF-8");
$podCityId = mb_convert_encoding(trim($_POST['podCity']), "ISO-8859-1", "UTF-8");

$itemId = mb_convert_encoding(trim($_POST['item']), "ISO-8859-1", "UTF-8");
$itemCodeId = mb_convert_encoding(trim($_POST['itemCode']), "ISO-8859-1", "UTF-8");
$btnId = mb_convert_encoding(trim($_POST['btn']), "ISO-8859-1", "UTF-8");
$commodityId = mb_convert_encoding(trim($_POST['commodity']), "ISO-8859-1", "UTF-8");
$cargoTypeId = mb_convert_encoding(trim($_POST['cargoType']), "ISO-8859-1", "UTF-8");

$shipperId = mb_convert_encoding(trim($_POST['shipper']), "ISO-8859-1", "UTF-8");
$consigneeId = mb_convert_encoding(trim($_POST['consignee']), "ISO-8859-1", "UTF-8");
$ffwwId = mb_convert_encoding(trim($_POST['ffww']), "ISO-8859-1", "UTF-8");
$notifyId = mb_convert_encoding(trim($_POST['notify']), "ISO-8859-1", "UTF-8");
$customerCityId = mb_convert_encoding(trim($_POST['customerCity']), "ISO-8859-1", "UTF-8");

$ruta3Id = mb_convert_encoding(trim($_POST['ruta3']), "ISO-8859-1", "UTF-8");
$grupoNavieraId = mb_convert_encoding(trim($_POST['grupoNaviera']), "ISO-8859-1", "UTF-8");




$chknave = mb_convert_encoding(trim($_POST['chknave']), "ISO-8859-1", "UTF-8");
$chknaviera = mb_convert_encoding(trim($_POST['chknaviera']), "ISO-8859-1", "UTF-8");

$chkruta = mb_convert_encoding(trim($_POST['chkruta']), "ISO-8859-1", "UTF-8");
$chktrade = mb_convert_encoding(trim($_POST['chktrade']), "ISO-8859-1", "UTF-8");
$chksubTrade = mb_convert_encoding(trim($_POST['chksubTrade']), "ISO-8859-1", "UTF-8");
$chkportCluster = mb_convert_encoding(trim($_POST['chkportCluster']), "ISO-8859-1", "UTF-8");
$chkporCountry = mb_convert_encoding(trim($_POST['chkporCountry']), "ISO-8859-1", "UTF-8");

$chkporCity = mb_convert_encoding(trim($_POST['chkporCity']), "ISO-8859-1", "UTF-8");
$chklopfiCountry = mb_convert_encoding(trim($_POST['chklopfiCountry']), "ISO-8859-1", "UTF-8");
$chklopfiCode = mb_convert_encoding(trim($_POST['chklopfiCode']), "ISO-8859-1", "UTF-8");
$chklopCityId = mb_convert_encoding(trim($_POST['chklopCity']), "ISO-8859-1", "UTF-8");
$chklopfiClusterId = mb_convert_encoding(trim($_POST['chklopfiCluster']), "ISO-8859-1", "UTF-8");
$chkdiplaCountryId = mb_convert_encoding(trim($_POST['chkdiplaCountry']), "ISO-8859-1", "UTF-8");
$chkdiplaCodeId = mb_convert_encoding(trim($_POST['chkdiplaCode']), "ISO-8859-1", "UTF-8");
$chkdiplaCityId = mb_convert_encoding(trim($_POST['chkdiplaCity']), "ISO-8859-1", "UTF-8");
$chkpodCountryId = mb_convert_encoding(trim($_POST['chkpodCountry']), "ISO-8859-1", "UTF-8");
$chkpodCityId = mb_convert_encoding(trim($_POST['chkpodCity']), "ISO-8859-1", "UTF-8");

$chkitemId = mb_convert_encoding(trim($_POST['chkitem']), "ISO-8859-1", "UTF-8");
$chkitemCodeId = mb_convert_encoding(trim($_POST['chkitemCode']), "ISO-8859-1", "UTF-8");
$chkbtnId = mb_convert_encoding(trim($_POST['chkbtn']), "ISO-8859-1", "UTF-8");
$chkcommodityId = mb_convert_encoding(trim($_POST['chkcommodity']), "ISO-8859-1", "UTF-8");
$chkcargoTypeId = mb_convert_encoding(trim($_POST['chkcargoType']), "ISO-8859-1", "UTF-8");

$chkshipperId = mb_convert_encoding(trim($_POST['chkshipper']), "ISO-8859-1", "UTF-8");
$chkconsigneeId = mb_convert_encoding(trim($_POST['chkconsignee']), "ISO-8859-1", "UTF-8");
$chkffwwId = mb_convert_encoding(trim($_POST['chkffww']), "ISO-8859-1", "UTF-8");
$chknotifyId = mb_convert_encoding(trim($_POST['chknotify']), "ISO-8859-1", "UTF-8");
$chkcustomerCityId = mb_convert_encoding(trim($_POST['chkcustomerCity']), "ISO-8859-1", "UTF-8");

$chkruta3Id = mb_convert_encoding(trim($_POST['chkruta3']), "ISO-8859-1", "UTF-8");
$chkgrupoNavieraId = mb_convert_encoding(trim($_POST['chkgrupoNaviera']), "ISO-8859-1", "UTF-8");

$uploaddir = "../../Reportes/" . $carpeta . "/";
			
if (file_exists($uploaddir)==false) {
    mkdir($uploaddir, 0700, true);
}

$uploaddir = $uploaddir . "/" . $usuarioId . "/";

if (file_exists($uploaddir)==false) {
    mkdir($uploaddir, 0700, true);
}

$objManifiesto->maerskExtracciones($fechaDesde, $fechaHasta, $tipoDato, $clienteId, $mercado, $etapa, $contenedor,
            $nave, $naviera, $ruta, $trade, $subTrade, $portCluster, $porCountry, $porCity, $lopfiCountry, $lopfiCode, $lopCityId,
			$lopfiClusterId, $diplaCountryId, $diplaCodeId, $diplaCityId, $podCountryId, $podCityId, $itemId, $itemCodeId, $btnId, $commodityId, 
			$cargoTypeId, $shipperId, $consigneeId, $ffwwId, $notifyId, $customerCityId, $ruta3Id, $grupoNavieraId,
			
			$chknave, $chknaviera, $chkruta, $chktrade, $chksubTrade, $chkportCluster, $chkporCountry, $chkporCity, $chklopfiCountry, $chklopfiCode, $chklopCityId,
			$chklopfiClusterId, $chkdiplaCountryId, $chkdiplaCodeId, $chkdiplaCityId, $chkpodCountryId, $chkpodCityId, $chkitemId, $chkitemCodeId, $chkbtnId, $chkcommodityId, 
			$chkcargoTypeId, $chkshipperId, $chkconsigneeId, $chkffwwId, $chknotifyId, $chkcustomerCityId, $chkruta3Id, $chkgrupoNavieraId,
						
			$carpeta, $nombreArchivo, $usuarioId);

?>
