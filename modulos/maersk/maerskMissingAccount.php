<?
session_start();



setlocale(LC_TIME, "spanish");
$fechaActual = htmlentities(strftime("%A %d de %B del %Y"));
$fechaActual = ucfirst($fechaActual);


$usuarioLog = $_SESSION["SEMPAT_usuarioNombre"];
$usuarioId = $_SESSION["SEMPAT_usuarioId"];
$clienteId = $_SESSION["SEMPAT_clienteId"];
$clienteNombre = $_SESSION["SEMPAT_clienteNombre"];
$grupoEmisorId = $_SESSION["SEMPAT_grupoEmisorId"];

if ($clienteId == 19) {


    if ($_SESSION['SEMPAT_usuarioId'] == "") {
        echo "<script>window.location='../../index.php';</script>";
    }

    include("../../default.php");
    $plantilla->setPath('../../plantillas/');
    $plantilla->setTemplate("header");
    $plantilla->setVars(array("USUARIO" => " $usuarioLog ",
        "FECHA" => "$fechaActual"
    ));
    echo $plantilla->show();

    require('../../clases/sempat.class.php');
    require('maersk.class.php');
    $objSem = new sempat();
    $objMaersk = new maersk();

    $moduloId = 56;
    $menuId = 144;
    $mercados = "";


    $htmlMenu = $objSem->menuRetornarHtml($usuarioId, $menuId);


    $moduloFiltros = $objSem->admBuscarFiltros($clienteId, $moduloId);
    $scriptAC = "";


//$consultaFechas = $objSem->manifiestoFechasDefecto($clienteId);
// <editor-fold>
    if ($moduloFiltros) {

        if ($mercados == "") {
            $filttrosNivel = "<option value='1'>Etapa</option><option value='28'>Mercado</option>";
        } else {
            $filttrosNivel = "<option value='1'>Etapa</option>";
        }

        $filtrosHtml = "<table style='margin-left:40px'>";
        $numFiltros = 0;
        $arreglo = array();

        while ($filtros = mssql_fetch_array($moduloFiltros)) {

            if ($filtros["filtroVisible"] == "1") {

                $numFiltros++;

                if (trim($filtros["filtroEtiqueta"]) != "") {

                    $filttrosNivel .= "<option value='" . $filtros["nivelId"] . "'>" . htmlentities($filtros["filtroEtiqueta"]) . "</option>";

                    //$filtrosHtml .= "<input id=" . $filtros["filtroHtml"] . " type='text' class='inputFiltro' placeHolder='" . htmlentities($filtros["filtroEtiqueta"]) . "'/><input type='text' class='inputFiltroRight' readonly disabled/>";
                    //$filtrosHtml .= "<input id=" . $filtros["filtroHtml"] . " type='text'/>";

                    $finputhtml = "<td width='340px'><input type='text' id=" . $filtros["filtroHtml"] . "";
                    $finputhtml .= "></input></td>";

                    $arreglo[$filtros["filtroOrden"]][$filtros["filtroColumna"] - 1] = "<td width='340px'><input type='text' id=" . $filtros["filtroHtml"] . "></input></td>";

                    $scriptAC .= "$('#" . $filtros["filtroHtml"] . "').tokenInput('../../complementos/diccionarios?nivelId=" . htmlentities($filtros["nivelId"]) . "', {
                                            theme: 'facebook',
                                            queryParam: 'filtro',
                                            minChars: 3,
                                            placeHolder: '" . mb_convert_encoding($filtros["filtroEtiqueta"], "UTF-8", "ISO-8859-1") . "'
                                        });\n\n";
                } else {
                    $arreglo[$filtros["filtroOrden"]][$filtros["filtroColumna"] - 1] = "<td></td>";
                }
            } else {

                if (trim($filtros["filtroHtml"]) != "") {
                    $filtrosHtml .= "<input id=" . $filtros["filtroHtml"] . " type='text' style='display:none'/><input type='text' readonly disabled style='display:none'/>";
                }
            }

//        if(($numFiltros % 5) == 0){
//            $filtrosHtml.= "<br><br>";
//        }
        }

        $filttrosNivel .= "<option value='18'>Seca/Refrigerada</option>";

        for ($i = 1; $i <= count($arreglo); $i++) {
            $filtrosHtml = $filtrosHtml . "<tr>";

            if ($arreglo[$i][0] != "") {
                $filtrosHtml = $filtrosHtml . $arreglo[$i][0];
            } else {
                $filtrosHtml = $filtrosHtml . "<td></td>";
            }

            if ($arreglo[$i][1] != "") {
                $filtrosHtml = $filtrosHtml . $arreglo[$i][1];
            } else {
                $filtrosHtml = $filtrosHtml . "<td></td>";
            }

            if ($arreglo[$i][2] != "") {
                $filtrosHtml = $filtrosHtml . $arreglo[$i][2];
            } else {
                $filtrosHtml = $filtrosHtml . "<td></td>";
            }

            $filtrosHtml = $filtrosHtml . "</tr>";
        }

        $filtrosHtml .= "</table>";
    }
// </editor-fold>
// <editor-fold>
    $tablaMercados = $objMaersk->mercadosDisponibles($mercados, "210px");
// </editor-fold>
    ?>
    <style>
        .nivel2.pinta > td {
            background-color: #d8e7ff !important;
        }
    </style>
    <script type="text/javascript" src="maerskReportes.js"></script>
    <script type="text/javascript" src="maerskFiltros.js"></script>

    <script type="text/javascript">

        $(document).ready(function () {

    <? echo $scriptAC; ?>


    <? if ($clienteId == 1) { ?>
                //new Messi('Ya se encuentra disponible la información de los Manifiestos Marítimos desde Octubre de 2013.', {center: false, align: 'right', titleClass: 'info', title: 'Aviso importante',  width: '320px', viewport: {top: '180px'}});
    <? } ?>

            var url = '../../complementos/diccionarios?nivelId=';


            $('#token-input-grupoNaviera').remove();
            $('#grupoNaviera').tokenInput(url + '179', {prePopulate: [{id: <?php echo $grupoEmisorId; ?>, name: '<?php echo $clienteNombre; ?>'}], theme: 'facebook', queryParam: 'filtro', minChars: 3, placeHolder: 'Carrier Group'});

            $('#token-input-grupoNaviera').css({
                color: "#3D85FE"
            });
            $('#token-input-grupoNaviera').val("Carrier Group: 1 seleccionado(s)");

            //$('#selectNivel1').val('133');
            //$('#selectNivel1 option').filter(function () { return $(this).html() == "Importador"; }).val();

            $('#selectNivel1').find('option').each(function () {
                //console.log("======>" + $(this)[0].innerText + "|" + $(this)[0].value + ")");
                if ($(this)[0].innerText == 'Consignee') {
                    $('#selectNivel1').val($(this)[0].value);
                }
            });
            $('#selectNivel2').find('option').each(function () {
                //console.log("======>" + $(this)[0].innerText + "|" + $(this)[0].value + ")");
                if ($(this)[0].innerText == 'Carrier Group') {
                    $('#selectNivel2').val($(this)[0].value);
                }
            });

            //token-input-manEmisor
            /*
             var url = '../../complementos/diccionarios?nivelId=';
                 
                 
             $('#token-input-manEmisor').remove();
             $('#manEmisor').tokenInput(url + '131', {prePopulate: [{id: <?php echo $grupoEmisorId; ?>, name: '<?php echo $clienteNombre; ?>'}], theme: 'facebook', queryParam: 'filtro', minChars: 3, placeHolder: 'Emisor de BL'});
                 
             $('#token-input-manEmisor').css({
             color: "#3D85FE"
             });
             $('#token-input-manEmisor').val("Emisor de BL: 1 seleccionado(s)");
                 
             //$('#selectNivel1').val('133');
             //$('#selectNivel1 option').filter(function () { return $(this).html() == "Importador"; }).val();
                 
             $('#selectNivel1').find('option').each(function () {
             //console.log("======>" + $(this)[0].innerText + "|" + $(this)[0].value + ")");
             if ($(this)[0].innerText == 'Importador') {
             $('#selectNivel1').val($(this)[0].value);
             }
             });
             $('#selectNivel2').find('option').each(function () {
             //console.log("======>" + $(this)[0].innerText + "|" + $(this)[0].value + ")");
             if ($(this)[0].innerText == 'Emisor de BL') {
             $('#selectNivel2').val($(this)[0].value);
             }
             });
             */
            //console.log("======>" + $('#selectNivel1').find('option[text="Importador"]')[0].value);
            //$('#selectNivel2').val('131');
            //$('#token-input-manImportador').val('asd');

            $('#token-input-manExportador').css('visibility', 'hidden');
            $('#token-input-manImportador').css('visibility', 'hidden');
            $('#token-input-manEmisor').css('visibility', 'hidden');
            //        $('#btnFFW').parent().css('visibility', 'hidden');
            //        $('#btnFFW').parent().next().css('visibility', 'hidden');

            $('#btnTeus').prop('checked', true);
            $('#btnImpo').prop('checked', true);
            $('#btnFFW').prop('checked', false);

            //mostrarFiltrosBusqueda()
            //buscarNivel1sow();

            //$('td.tdSubMenuLeft').html('asd');
            //        $('td.tdSubMenuLeft').css('height', '40px');
            //        $('td.tdSubMenuLeft').css('background-size', '100% 120%');
            //        $('td.tdSubMenuLeft').css('background-position', 'center');
            //        $('td.tdSubMenuLeft').css('background-repeat', 'no-repeat');

            var clienteNombre = '<?php echo $clienteNombre; ?>';

            //        $('td.check').css('width', '0px');
            //        $('td.check').css('text-align', 'left');
            //.tablaValores tbody tr td.check
            //$('td.tdSubMenuLeft').css('border', 'solid 1px #00f');
        });

    </script>
    <input type="hidden" id="clienteNombre" value="<?php echo $clienteNombre; ?>">
    <? echo $htmlMenu; ?>



    <style>
        .tablaValores tbody tr td.check {
            width: 0% !important;
            text-align: left !important;
        }
        .tablaValores tbody tr.nivel1 td.nivel1 {
            width: 225px !important;
            text-align: left !important;
        }
    </style>


    <div class="divMinHeight">


        <div id="divGuardarReporte" class="divPopUp">

            <label>GUARDAR REPORTE</label>

            <br><br>

            <input type="text" class="inputFiltro" placeHolder="Nombre"/>
            <input type="text" class="inputFiltroRight" readonly disabled/>

            <br><br>

            <div style="text-align: left; width:200px; margin-left: 50px; min-height: 160px">

                Nivel 1: Agente Portuario <br>
                Nivel 2: Puerto <br>

            </div>

            <div style="margin-left:10px">
                <input type="button" class="btnPlomo" value="CANCELAR" onclick="cerrarReporte()"/>                            
                <input type="button" class="btnAzul" value="GUARDAR"/>  
            </div>

        </div>

        <div id="divReportesGuardados" class="divPopUp">

            <label>REPORTES GUARDADOS</label>

            <br><br>

            <div style="text-align: left; width:200px; margin-left: 50px; min-height: 210px;">

                <table>
                    <tr>
                        <td>
                            <input type="checkbox" class="styled">
                        </td>
                        <td>
                            Reporte 1
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="checkbox" class="styled">
                        </td>
                        <td>
                            Reporte 2
                        </td>
                    </tr>
                </table>

            </div>

            <div style="margin-left:10px">
                <input type="button" class="btnPlomo" value="CANCELAR" onclick="cerrarReporte2()"/>                            
                <input type="button" class="btnAzul" value="GUARDAR"/>  
            </div>

        </div>



        <div class="divSubContenedor">

            <div class="divPanelBusqueda" onClick="mostrarFiltrosBusqueda()">
                <img src="../../imagenes/lupa.png" alt="" title=""><label>PANEL DE BUSQUEDA - MISSING ACCOUNT</label>
            </div>

            <div id="divPanelFiltros" class="divPanelFiltros">

                <div class="divPanelFiltros1" style="height: 200px" >



                    <table>
                        <tr>
                            <td valign="top" width="400px">
                                <label>FECHAS</label>

                                <br><br>

                                <input id="fechaDesde" type="text" class="inputFecha" placeHolder="Inicio" value=""/><input id="btnFechaDesde" type="button" class="inputFechaBtn" />
    <!--                            <input id="fechaDesdeDef" type="hidden"  value="<? //echo $fechaDesde;        ?>"/>-->


                                <script type="text/javascript">//<![CDATA[
                                    var myCal1 = Calendar.setup({
                                        checkRange: false,
                                        inputField: "fechaDesde",
                                        trigger: "btnFechaDesde",
                                        //onSelect: function () { this.hide(); validarFechasReportes($('fechaDesde'), 'fechaDesde', 'fechaHasta');},
                                        dateFormat: "%d-%m-%Y"
                                    });
                                    myCal1.setLanguage('es');
                                    //]]></script>

                                <input id="fechaHasta" type="text" class="inputFecha" placeHolder="Fin" value="" style="margin-left:60px"/><input id="btnFechaHasta" type="button" class="inputFechaBtn" />
    <!--                            <input id="fechaHastaDef" type="hidden"  value="<? //echo $fechaHasta;         ?>"/>-->


                                <script type="text/javascript">//<![CDATA[
                                    var myCal1 = Calendar.setup({
                                        checkRange: false,
                                        inputField: "fechaHasta",
                                        trigger: "btnFechaHasta",
                                        //onSelect: function () { this.hide(); validarFechasReportes($('fechaDesde'), 'fechaDesde', 'fechaHasta');},
                                        dateFormat: "%d-%m-%Y"
                                    });
                                    myCal1.setLanguage('es');
                                    //]]></script>


                                <div id="divMensajeFechas">

                                </div>

                            </td> 
                            <? echo $tablaMercados; ?>
                            <td valign="top">

                                <label class="niveles" style="padding-left: 8px;">DATOS</label>
                                <table>
                                    <tr>
                                        <td>
                                            <input id="btnContenedores" type="radio" name="tipoDato" value="contenedores">
                                        </td>
                                        <td>
                                            N° de Contenedores
                                        </td>
                                        <td width="80px">

                                        </td>
                                        <td>
                                            <!--input id="btnImpo" type="radio" name="btnEtapa" value="2"-->
                                        </td>
                                        <td>
                                            <!--Impo-->
                                        </td>
                                        <td>
                                            <!--input id="btnExpo" type="radio" name="btnEtapa" value="1"-->
                                        </td>
                                        <td>
                                            <!--Expo-->
                                        </td>
                                    </tr>                                    
                                    <tr>
                                        <td>
                                            <input id="btnFeus" type="radio" name="tipoDato" value="feus" checked>
                                        </td>
                                        <td valign="bottom">
                                            FFEs
                                        </td>
                                        <td>

                                        </td>
                                        <td>
                                            <!--input id="btnContenedor" type="radio" value="C" name="btnContenedor"-->
                                        </td>
                                        <td>
                                            <!--Contenedor-->
                                        </td>
                                        <td>
                                            <!--input id="btnNoContenedor" type="radio" value ="NC" name="btnContenedor"-->
                                        </td>
                                        <td>
                                            <!--No Contenedor-->
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input id="btnToneladas" type="radio" name="tipoDato" value="toneladas">
                                        </td>
                                        <td valign="bottom">
                                            Toneladas
                                        </td>
                                        <td>

                                        </td>
                                    </tr>								
                                </table>
                            </td>
                        </tr>
                    </table>



                    <style>
                        .inputR{
                            background-image: url(../../imagenes/inputFiltroR.png); 
                            background-repeat: no-repeat; 
                            background-size: 100% 100% ;
                            background-color: transparent;
                            background-position: center;
                            text-align: right;
                        }

                        .fakefbi{
                            outline: medium none;
                            font-size: 11px;
                            color: rgb(169, 169, 169);
                            background: url("../../imagenes/inputFiltro_sm.png") no-repeat;
                            background-position: -2px -0px;
                            padding-right: 5px;
                            height: 30px;
                            padding-left: 10px;
                            line-height: 28px;
                            border: none;
                            list-style-type: none;
                            /*font-style: italic;*/
                            text-align: right;
                            cursor: text;
                            font-family: "Lucida Grande", "Lucida Sans Unicode", Verdana,Arial,Helvetica,sans-serif;
                        }

                    </style>



                    <div style="border: none; height: 0px; overflow: hidden; position: relative;">
                        <div style="width:100%; margin-top: 35px; margin-bottom: 8px;">                            
                            <label>NIVELES</label>                                                    
                        </div>

                        <select id="selectNivel1" name="selectNivel1" class="styled filtro">
                            <option value="" class="placeHolder">Nivel 1</option>
                            <? echo $filttrosNivel; ?>
                        </select>

                        <select id="selectNivel2" name="selectNivel2" class="styled filtro">
                            <option value=""  class="placeHolder">Nivel 2</option>
                            <? echo $filttrosNivel; ?>
                        </select>

                        <select id="selectNivel3" name="selectNivel3" class="styled filtro">
                            <option value=""  class="placeHolder">Nivel 3</option>
                            <? echo $filttrosNivel; ?>
                        </select>

                        <select id="selectNivel4" name="selectNivel4" class="styled filtro">
                            <option value=""  class="placeHolder">Nivel 4</option>
                            <? echo $filttrosNivel; ?>
                        </select>

                        <select id="selectNivel5" name="selectNivel5" class="styled filtro">
                            <option value="" class="placeHolder">Nivel 5</option>
                            <? echo $filttrosNivel; ?>
                        </select>
                    </div>




                    <style>
                        .tablealigner td {
                            position: relative;
                            clear:both;

                        }
                        .tablealigner td:after {
                            clear:both;

                        }
                        .tablealigner input {

                        }
                        .tablealigner span{
                            display: inline-block;
                            position: absolute;
                            padding-top: 2px;
                            padding-left: 6px;
                            white-space: nowrap;
                        }
                        .canmarker {

                        }
                        .alpadder {
                            display: inline-block;
                            padding-top: 8px !important;

                            padding-left: 2px !important;
                        }

                    </style>

                    <div style="width:100%; margin-top: 40px; margin-bottom: 8px;">                            
                        <label>FILTROS</label>                                                    
                    </div>

                    <div style="padding-left: 37px;">

                        <table>
                            <tr style="vertical-align: top;">
                                <td style="width: 335px;">
                                    <table class="tablealigner">
                                        <tr>
                                            <td style="width: 115px;"><input id="btnImpo" type="radio" name="btnEtapa" value="2"><span>Impo</span></td>
                                            <td><input id="btnExpo" type="radio" name="btnEtapa" value="1"><span>Expo</span></td>
                                        </tr>
                                        <tr>
                                            <td><input id="btnContenedor" type="radio" value="C" name="btnContenedor" checked onchange="actualizarBaremos()"><span>Contenedor</span></td>
                                            <td><input id="btnNoContenedor" type="radio" value ="NC" name="btnContenedor" disabled><span>No Contenedor</span></td>
                                        </tr>
                                        <tr>
                                            <td><input id="btnSeca" type="radio" value="1" name="btnSecaRefrigerada"><span>Seca</span></td>
                                            <td><input id="btnRefrigerada" type="radio" value="2" name="btnSecaRefrigerada"><span>Refrigerada</span></td>
                                        </tr>
                                        <tr><td>&nbsp;</td><td></td></tr>
                                        <tr><td><input id="btnFFW" type="radio" name="btnFww" value="1"><span>Solo carga de Forwarders</span></td><td></td></tr>
                                    </table>
                                <td>

                                </td>
                                <td style="width: 350px;">
                                    VOLUMEN DE CLIENTES <br>
                                    <table class="tablealigner">
                                        <tr>
                                            <td style="width: 140px;"><input id="btnFiTotal" type="radio" name="btnFiltroIndicador"  value="T" CHECKED onchange="actualizarBaremos()"><span>Total Per&iacute;odo</span></td>
                                            <td><span class="alpadder">Desde</span><input id="btnBaremosBTM" class="fakefbi" type="text" name="baremosBTM" onkeypress="return validate(event);"  value="0" style='width: 40px; margin-left: 40px;'><span class="alpadder canmarker">TEUs</span></td>
                                        </tr>
                                        <tr>
                                            <td><input id="btnFiProm" type="radio" name="btnFiltroIndicador" value="P" onkeypress="return validate(event);" onchange="actualizarBaremos()"><span>Promedio Mensual</span></td>
                                            <td><span class="alpadder">Hasta</span><input id="btnBaremosTOP" class="fakefbi" type="text" name="baremosTOP"  onkeypress="return validate(event);" value="0" style='width: 40px; margin-left: 40px;'><span class="alpadder canmarker">TEUs</span></td>
                                        </tr>
                                    </table>


                                </td>
                                <td >
                                    MARKET SHARE DE CLIENTES <?php echo $clienteNombre; ?> <br>
                                    <table class="tablealigner">
                                        <tr>
                                            <td><span class="alpadder">Entre</span>
                                                <input id="btnParticipacionMIN" class="fakefbi" type="text" name="participacionMIN" onkeypress="return validate(event);" value="0" style='width: 40px; margin-left: 40px;'>
                                                <span class="alpadder">%&nbsp; - </span>
                                                <input id="btnParticipacionMAX" class="fakefbi" type="text" name="participacionMAX" onkeypress="return validate(event);" value="0" style='width: 40px; margin-left: 30px;'>
                                                <span class="alpadder">%</span>
                                            </td>
                                        </tr>
                                    </table>

                                    LISTADO DE CLIENTES<br>
                                    <table class="tablealigner">
                                        <tr>
                                            <td><span class="alpadder">TOP</span><input id="btnTopval" class="fakefbi" type="text" onkeypress="return validate(event);" name="topval" value="100" style='width: 40px; margin-left: 40px;'></td>
                                        </tr>
                                    </table>


                                </td>
                            </tr>
                        </table>

                    </div>
                    <br>



                    <? echo $filtrosHtml; ?>

                    <div style="clear: both"></div>

                    <div id="divMensaje">
                        Debe seleccionar el primer nivel.
                    </div>

                    <div style="float:right; margin-right: 90px; margin-top: 15px; margin-bottom: 15px;">
                        <input type="button" class="btnLogin" value="BUSCAR" onclick="buscarNivel1MA()"/>                            
    <!--                                <input type="button" class="btnLogin" value="GUARDAR" onclick="abrirReporte()"/>  -->
                    </div>                                                        

                </div>    

                <div style="clear: both"></div>

                <div class="divPanelFiltros2" style="display:none">

                    <div style="width:100%; margin-bottom: 8px;">                            
                        <label>REPORTES GUARDADOS</label>                            
                    </div>   

                    <input type="text" class="inputFecha" placeHolder="Nombre"/><input type="button" class="inputLupaBtn" onclick="abrirReporte2()"/>

                    <input type="button" class="btnLogin" value="VER"/>

                    <br/><br/>

                </div>                                        

            </div>

            <div class="divPanelBusquedaBottom" onClick="mostrarFiltrosBusqueda()">

            </div>

        </div>




        <div id="divGraficos" style="height: 405px; display:none;">

            <div id="divGrafico" class="divGrafico" style="width:1122px; height:400px; display:none;">

            </div>                        

            <div id="divGraficoA" class="divGraficoLeft" style="width:540px; height:390px; display:none;">

            </div>

            <div id="divGraficoB" class="divGraficoRight" style="width:540px; height:390px; display:none;">

            </div> 

        </div>

        <br style="clear:both"/>   


        <div id="divOpciones" class="divOpciones" style="display:none">  

            <table>
                <tr>
                    <td width="300px" valign="top">
                        <label>NIVELES</label>
                        <table id="tablaNivelesDesc" class="tablaNiveles">

                        </table>
                    </td>
                    <td id="tdResumen" width="475px" valign="top">

                    </td>
                    <td width="300px" rowspan="2" valign="top">
                        <table>
                            <tr style="height:30px; visibility: visible;">
                                <td width="100px" valign="middle">
                                    <label>GRÁFICO</label>
                                </td>
                                <td width="230px" valign="middle">

                                    <script>
                                        $(document).ready(function () {
                                            checkSelect();
                                            /*
                                             $('#selectselectGrafico').bind("DOMSubtreeModified", function () {
                                             alert('changed');
                                             });
                                             */
                                        });

                                        function checkSelect() {

                                            if ($("#selectselectGrafico").length) {
                                                console.log('existe');
                                                $('#selectselectGrafico').bind("DOMSubtreeModified", function () {
                                                    cambiarTypoGrafico();
                                                });
                                            } else {
                                                console.log('no existe');
                                                setTimeout(function () {
                                                    checkSelect();
                                                }, 1000);
                                            }

                                        }

                                    </script>

                                    <select id="selectGraficoReporte1" name="selectGrafico" class="styled" style="height:26px;">
                                        <option value="line">Líneas</option>
                                        <option value="column">Barras</option>
                                        <option value="pie">Porcentaje</option>
                                    </select>

                                    <input id="tituloGrafico" type="hidden" value="">
                                    <input id="tituloGrafico2" type="hidden" value="">
                                    <input id="tituloGraficoEjeY" type="hidden" value="">
                                </td>
                            </tr>
                            <tr style="height:30px;">
                                <td valign="middle" class="tdTipo">
                                    <label>TIPO CNT.</label>
                                </td>
                                <td class="tdTipo">
                                    <select id="selectTipoReporte" name="selectTipoReporte" class="styled" style="height:26px;">
                                        <option value="">Ambos</option>
                                        <option value="C20">C20</option>
                                        <option value="C40">C40</option>
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

        </div>


        <center id="imgCargando" style="display:none">
            <img src="../../imagenes/cargando.gif" height="42" width="42"><br/><i>Cargando</i>
        </center>

        <div id="divResultado">


        </div>

    </div>

    <br>

    <?php
    $plantilla->setPath('../../plantillas/');
    $plantilla->setTemplate("footer");
    echo $plantilla->show();
}
