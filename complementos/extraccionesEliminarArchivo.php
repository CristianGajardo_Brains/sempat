<?PHP

session_start();
include ("../librerias/conexion.php");
require('../clases/sempat.class.php');
$objSempat = new sempat();


$usuarioId = $_SESSION['SEMPAT_usuarioId'];
$clienteId = $_SESSION['SEMPAT_clienteId'];

$moduloId = mb_convert_encoding(trim($_POST['modulo']), "ISO-8859-1", "UTF-8");
$archivoId = mb_convert_encoding(trim($_POST['archivoId']), "ISO-8859-1", "UTF-8");
$carpeta = mb_convert_encoding(trim($_POST['carpeta']), "ISO-8859-1", "UTF-8");

$resultadoFila = $objSempat->extraccionesEliminarArchivo($usuarioId, $moduloId, $archivoId);

$ruta = "../Reportes/" . htmlentities($carpeta) . "/" . $usuarioId . "/" . $resultadoFila . "";


if (file_exists($ruta)) {
    unlink($ruta);            
}

$resultadoFila = $objSempat->extraccionesEliminarArchivo($usuarioId, $moduloId, $archivoId);


?>
