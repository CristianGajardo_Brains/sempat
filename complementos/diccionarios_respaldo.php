<?

session_start();
include ("../librerias/conexion.php");

$nivelId = $_GET["nivelId"];
$filtro = $_GET["filtro"];
$query = "";

$clienteId = $_SESSION["SEMPAT_clienteId"];

/** EXPO/IMPO **/

if($nivelId == 2){//naviera
    $query = "Select idGrupoNaviera as id, (nombreGrupoNaviera) as name From Mercado..gruposNavieras (nolock) Where nombreGrupoNaviera like '%" . $filtro . "%' and estadoGrupoNaviera = 1";
}

if($nivelId == 3 || $nivelId == 4){//embarcador - consignatario
    $query = "Select idCliente as id, (nombreCliente) as name From Mercado..clientes (nolock) Where nombreCliente like '%" . $filtro . "%' and estadoCliente = 1";
}

if($nivelId == 5){//nave
    $query = "Select idNave as id, (nombreNave) as name From Mercado..naves (nolock) Where nombreNave like '%" . $filtro . "%' and estadoNave = 1";
}

if($nivelId == 6){//tipo nave
    $query = "Select idTipoNave as id, (nombreTipoNave) as name From Mercado..tiposNaves (nolock) Where nombreTipoNave like '%" . $filtro . "%' and estadoTipoNave = 1";
}

if($nivelId == 7 || $nivelId == 9 || $nivelId == 11 || $nivelId == 13){ //puertos
    $query = "Select idPuerto as id, (nombrePuerto + '-' + codigoPuerto) as name From Mercado..puertos (nolock) Where nombrePuerto like '%" . $filtro . "%' and estadoPuerto = 1";
}

if($nivelId == 8 || $nivelId == 10 || $nivelId == 12 || $nivelId == 14){//paises
    $query = "Select idPais as id, (nombrePais) as name From Mercado..paises (nolock) Where nombrePais like '%" . $filtro . "%' and estadoPais = 1";
}

if($nivelId == 15){//tráfico
    $query = "Select idZona as id, (nombreZona) as name From Mercado..zonas (nolock) Where nombreZona like '%" . $filtro . "%' and estadoZona = 1";
}

if($nivelId == 16){//tipo servicio
    $query = "Select idTipoServicio as id, (descripcionTipoServicio) as name From Mercado..tiposServicios (nolock) Where descripcionTipoServicio like '%" . $filtro . "%' and estadoTipoServicio = 1";
}

if($nivelId == 17){//tipo carga
    $query = "Select idTipoCarga as id, (nombreTipoCargaEsp) as name From Mercado..tiposCargas (nolock) Where nombreTipoCargaEsp like '%" . $filtro . "%' and estadoTipoCarga = 1";
}

if($nivelId == 19){//tipo embalaje
    $query = "Select idTipoEmbalaje as id, (nombreEmbalaje) as name From Mercado..tiposEmbalajes (nolock) Where nombreEmbalaje like '%" . $filtro . "%' and estadoTipoEmbalaje = 1";
}

if($nivelId == 20){//mercaderia
    $query = "Select idCommodity as id, (nombreCommodityEsp) as name From Mercado..commodities (nolock) Where nombreCommodityEsp like '%" . $filtro . "%' and estadoCommodity = 1";
}

if($nivelId == 21){//gran familia
    $query = "Select idGranFamilia as id, (nombreGranFamiliaEsp) as name From Mercado..granfamilias (nolock) Where nombreGranFamiliaEsp like '%" . $filtro . "%' and estadoGranFamilia = 1";
}

if($nivelId == 22){//familia
    $query = "Select idFamilia as id, (nombreFamiliaEsp) as name From Mercado..familias (nolock) Where nombreFamiliaEsp like '%" . $filtro . "%' and estadoFamilia = 1";
}

if($nivelId == 23){//sub familia
    $query = "Select idSubFamilia as id, (nombreSubFamiliaEsp) as name From Mercado..subFamilia (nolock) Where nombreSubFamiliaEsp like '%" . $filtro . "%' and estadoSubFamilia = 1";
}

if($nivelId == 24 || $nivelId == 30){//agente portuario
    $query = "Select idAgencia as id, (nombreAgencia) as name From maeAgenciasSempat (nolock) Where nombreAgencia like '%" . $filtro . "%' and estadoAgencias = 1";
}

if($nivelId == 26){//agente comercial
    $query = "Select idAgente as id, (nombreAgente) as name From Mercado..agentes (nolock) Where nombreAgente like '%" . $filtro . "%' and estadoAgente = 1";
}

if($nivelId == 27){//estado contenedor
    $query = "Select idEstadoContenedor as id, (nombreEstadoContenedor) as name From maeEstadosContenedor (nolock) Where nombreEstadoContenedor like '%" . $filtro . "%' and estadoContenedor = 1";
}


if($nivelId == 29){//SUCURSAL TRAMP
    $query = "Select distinct sucursalId as id, (sucursal) as name From maeSucursalesUltramar (nolock) Where sucursal like '%" . $filtro . "%' ";
}

if($nivelId == 31){ //PUERTO TRAMP
    $query = "Select idPuerto as id, (nombrePuerto) as name From Mercado..puertos (nolock) Where nombrePuerto like '%" . $filtro . "%' and estadoPuerto = 1";
}

/** AGENCIAMIENTO - FLOTA **/


if($nivelId == 32){//AGENCIA
    $query = "Select '''' + AGENCIA + '''' as id, (AGENCIA) as name From pracAgencias (nolock) Where AGENCIA like '%" . $filtro . "%' ";
}

if($nivelId == 33){//PUERTO
    $query = "Select '''' + PUERTO + '''' as id, (PUERTO) as name From pracPuertos (nolock) Where PUERTO like '%" . $filtro . "%' ";
}

if($nivelId == 34){//TIPO NAVE
    $query = "Select '''' + TIPO_NAVE + '''' as id, (TIPO_NAVE) as name From pracTiposNave (nolock) Where TIPO_NAVE like '%" . $filtro . "%' ";
}

if($nivelId == 35){//TIPO FAENA
    $query = "Select '''' + TIPO_FAENA + '''' as id, (TIPO_FAENA) as name From pracTiposFaena (nolock) Where TIPO_FAENA like '%" . $filtro . "%' ";
}

if($nivelId == 36){//TIPO SERVICIO
    $query = "Select '''' + TIPO_SERVICIO + '''' as id, (TIPO_SERVICIO) as name From pracTipoServicio (nolock) Where TIPO_SERVICIO like '%" . $filtro . "%' ";
}

if($nivelId == 36){//TIPO SERVICIO
    $query = "Select '''' + TIPO_SERVICIO + '''' as id, (TIPO_SERVICIO) as name From pracTipoServicio (nolock) Where TIPO_SERVICIO like '%" . $filtro . "%' ";
}

if($nivelId == 37){//NAVIERA
    $query = "";
}

if($nivelId == 38){//SHIPPER
    $query = "";
}

if($nivelId == 39){//AGENTE COMERCIAL
    $query = "";
}

if($nivelId == 40){//NAVE
    $query = "Select '''' + nave + '''' as id, (nave) as name From pracNaves (nolock) Where nave like '%" . $filtro . "%' ";
}

if($nivelId == 41){//SITIO
    $query = "Select '''' + sitio + '''' as id, (sitio) as name From pracSitios (nolock) Where sitio like '%" . $filtro . "%' ";
}

if($nivelId == 42){//SUCURSAL
    $query = "Select distinct '''' + sucursal + '''' as id, (sucursal) as name From maeSucursalesUltramar (nolock) Where sucursal like '%" . $filtro . "%' ";
}


/** TRAMP **/


if($nivelId == 43){//CARGO GROUP
    $query = "Select idCargoGroup as id, (nombreCargoGroupEsp) as name From Mercado..cargoGroup (nolock) Where nombreCargoGroupEsp like '%" . $filtro . "%' and estadoCargoGroup = 1";
}

if($nivelId == 44){ //puertos
    $query = "Select idPuerto as id, (nombrePuerto + '-' + codigoPuerto) as name From Mercado..puertos (nolock) Where nombrePuerto like '%" . $filtro . "%' and estadoPuerto = 1";
}

if($nivelId == 45){//paises
    $query = "Select idPais as id, (nombrePais) as name From Mercado..paises (nolock) Where nombrePais like '%" . $filtro . "%' and estadoPais = 1";
}


//** PILOTAJES **/

if($nivelId == 46 || $nivelId == 47 || $nivelId == 48 || $nivelId == 49 || $nivelId == 50){//puertos
    $query = "Select distinct '''' + puerto + '''' as id, (puerto) as name From (Select distinct puerto From pilotajePuertos (nolock)) as t Where puerto like '%" . $filtro . "%' ";    
}

if($nivelId == 51){//tipos pilotaje
    $query = "Select distinct '''' + tipoPilotaje + '''' as id, (tipoPilotaje) as name From pilotajeTiposPilotaje (nolock) Where tipoPilotaje like '%" . $filtro . "%' ";
}

if($nivelId == 60){//agente portuario
    $query = "Select distinct '''' + agentePortuario + '''' as id, (agentePortuario) as name From pilotajeAgentePortuario (nolock) Where agentePortuario like '%" . $filtro . "%' ";
}

if($nivelId == 61){//nave
    $query = "Select distinct '''' + nave + '''' as id, (nave) as name From pilotajeNaves (nolock) Where nave like '%" . $filtro . "%' ";
}

if($nivelId == 62){//navieras
    $query = "Select distinct '''' + naviera + '''' as id, (naviera) as name From pilotajeNavieras (nolock) Where naviera like '%" . $filtro . "%' ";
}

if($nivelId == 63){//tipo nave
    $query = "Select distinct '''' + tipoNave + '''' as id, (tipoNave) as name From pilotajeTiposNave (nolock) Where tipoNave like '%" . $filtro . "%' ";
}

if($nivelId == 130){//tramo
    $query = "Select distinct '''' + tramo + '''' as id, (tramo) as name From pilotajeTramos (nolock) Where tramo like '%" . $filtro . "%' ";
}


//** BUNKERING **/

if($nivelId == 52){//embarcador
    $query = "Select distinct '''' + embarcador + '''' as id, (embarcador) as name From bunkeringEmbarcadores (nolock) Where embarcador like '%" . $filtro . "%' ";
}

if($nivelId == 53){//naviera
    $query = "Select distinct '''' + naviera + '''' as id, (naviera) as name From bunkeringNavieras (nolock) Where naviera like '%" . $filtro . "%' ";
}

if($nivelId == 54){//nave
    $query = "Select distinct '''' + nave + '''' as id, (nave) as name From bunkeringNaves (nolock) Where nave like '%" . $filtro . "%' ";
}

if($nivelId == 55){//tipoNave
    $query = "Select distinct '''' + tipoNave + '''' as id, (tipoNave) as name From bunkeringTiposNave (nolock) Where tipoNave like '%" . $filtro . "%' ";
}

if($nivelId == 56){//tipoNave
    $query = "Select distinct '''' + puertoOrigen + '''' as id, (puertoOrigen) as name From bunkeringPuertos (nolock) Where puertoOrigen like '%" . $filtro . "%' ";
}

if($nivelId == 57){//proveedor
    $query = "Select distinct '''' + proveedor + '''' as id, (proveedor) as name From bunkeringProveedores (nolock) Where proveedor like '%" . $filtro . "%' ";
}

if($nivelId == 58){//producto
    $query = "Select distinct '''' + producto + '''' as id, (producto) as name From bunkeringProductos (nolock) Where producto like '%" . $filtro . "%' ";
}

if($nivelId == 59){//detalle
    $query = "Select distinct '''' + detalle + '''' as id, (detalle) as name From bunkeringDetalles ingProductos (nolock) Where detalle like '%" . $filtro . "%' ";
}


//** FLEXITANK **/

if($nivelId == 64){//aduana
    $query = "Select distinct '''' + aduana + '''' as id, (aduana) as name From flexiAduanas (nolock) Where aduana like '%" . $filtro . "%' ";
}

if($nivelId == 65){//clausula
    $query = "Select distinct '''' + clausula + '''' as id, (clausula) as name From flexiClausulas (nolock) Where clausula like '%" . $filtro . "%' ";
}

if($nivelId == 66){//exportador
    $query = "Select distinct '''' + exportador + '''' as id, (exportador) as name From flexiExportadores (nolock) Where exportador like '%" . $filtro . "%' ";
}

if($nivelId == 67){//mercaderia
    $query = "Select distinct '''' + mercaderia + '''' as id, (mercaderia) as name From flexiMercaderias (nolock) Where mercaderia like '%" . $filtro . "%' ";
}

if($nivelId == 68){//moneda
    $query = "Select distinct '''' + moneda + '''' as id, (moneda) as name From flexiMonedas (nolock) Where moneda like '%" . $filtro . "%' ";
}

if($nivelId == 69){//nave 
    $query = "Select distinct '''' + nave + '''' as id, (nave) as name From flexiNaves (nolock) Where nave like '%" . $filtro . "%' ";
}

if($nivelId == 70){//naviera
    $query = "Select distinct '''' + naviera + '''' as id, (naviera) as name From flexiNavieras (nolock) Where naviera like '%" . $filtro . "%' ";
}

if($nivelId == 71){//paisDestino
    $query = "Select distinct '''' + paisDestino + '''' as id, (paisDestino) as name From flexiPaisesDestino (nolock) Where paisDestino like '%" . $filtro . "%' ";
}

if($nivelId == 72){//puertoDescarga
    $query = "Select distinct '''' + puertoDescarga + '''' as id, (puertoDescarga) as name From flexiPuertosDescarga (nolock) Where puertoDescarga like '%" . $filtro . "%' ";
}

if($nivelId == 73){//puertoEmbarque
    $query = "Select distinct '''' + puertoEmbarque + '''' as id, (puertoEmbarque) as name From flexiPuertosEmbarque (nolock) Where puertoEmbarque like '%" . $filtro . "%' ";
}

if($nivelId == 74){//tipoBulto
    $query = "Select distinct '''' + tipoBulto + '''' as id, (tipoBulto) as name From flexiTiposBulto (nolock) Where tipoBulto like '%" . $filtro . "%' ";
}

if($nivelId == 75){//tipoCarga
    $query = "Select distinct '''' + tipoCarga + '''' as id, (tipoCarga) as name From flexiTiposCarga (nolock) Where tipoCarga like '%" . $filtro . "%' ";
}

if($nivelId == 76){//tipoOperacion
    $query = "Select distinct '''' + tipoOperacion + '''' as id, (tipoOperacion) as name From flexiTiposOperaciones (nolock) Where tipoOperacion like '%" . $filtro . "%' ";
}

if($nivelId == 77){//trafico
    $query = "Select distinct '''' + trafico + '''' as id, (trafico) as name From flexiTraficos (nolock) Where trafico like '%" . $filtro . "%' ";
}

if($nivelId == 78){//viaTransporte
    $query = "Select distinct '''' + viaTransporte + '''' as id, (viaTransporte) as name From flexiViasTransporte (nolock) Where viaTransporte like '%" . $filtro . "%' ";
}





//** ADUANA **/

if($nivelId == 79){//aduana
    $query = "Select distinct '''' + aduana + '''' as id, (aduana) as name From aduanaExpoAduanas (nolock) Where aduana like '%" . $filtro . "%' ";
}

if($nivelId == 80){//clausula
    $query = "Select distinct '''' + clausula + '''' as id, (clausula) as name From aduanaExpoClausulas (nolock) Where clausula like '%" . $filtro . "%' ";
}

if($nivelId == 81){//exportador
    $query = "Select distinct '''' + exportador + '''' as id, (exportador) as name From aduanaExpoExportadores (nolock) Where exportador like '%" . $filtro . "%' ";
}

if($nivelId == 82){//forma pago
    $query = "Select distinct '''' + formaPago + '''' as id, (formaPago) as name From aduanaExpoFormasDePago (nolock) Where formaPago like '%" . $filtro . "%' ";
}

if($nivelId == 83){//naviera
    $query = "Select distinct '''' + naviera + '''' as id, (naviera) as name From aduanaExpoNavieras (nolock) Where naviera like '%" . $filtro . "%' ";
}

if($nivelId == 84){//pais destino 
    $query = "Select distinct '''' + paisDestino + '''' as id, (paisDestino) as name From aduanaExpoPaisDestino (nolock) Where paisDestino like '%" . $filtro . "%' ";
}

if($nivelId == 85){//producto
    $query = "Select distinct '''' + producto + '''' as id, (producto) as name From aduanaExpoProductos (nolock) Where producto like '%" . $filtro . "%' ";
}

if($nivelId == 86){//puerto descarga
    $query = "Select distinct '''' + puertoDescarga + '''' as id, (puertoDescarga) as name From aduanaExpoPuertoDescarga (nolock) Where puertoDescarga like '%" . $filtro . "%' ";
}

if($nivelId == 87){//puertoEmbarque
    $query = "Select distinct '''' + puertoEmbarque + '''' as id, (puertoEmbarque) as name From aduanaExpoPuertoEmbarque (nolock) Where puertoEmbarque like '%" . $filtro . "%' ";
}

if($nivelId == 88){//tipo bulto
    $query = "Select distinct '''' + tipoBulto + '''' as id, (tipoBulto) as name From aduanaExpoTipoBulto (nolock) Where tipoBulto like '%" . $filtro . "%' ";
}

if($nivelId == 89){//tipoCarga
    $query = "Select distinct '''' + tipoCarga + '''' as id, (tipoCarga) as name From aduanaExpoTipoCarga (nolock) Where tipoCarga like '%" . $filtro . "%' ";
}

if($nivelId == 90){//unidad medida
    $query = "Select distinct '''' + unidadMedida + '''' as id, (unidadMedida) as name From aduanaExpoUnidadMedida (nolock) Where unidadMedida like '%" . $filtro . "%' ";
}

if($nivelId == 91){//via transporte
    $query = "Select distinct '''' + viaTransporte + '''' as id, (viaTransporte) as name From aduanaExpoViaTransporte (nolock) Where viaTransporte like '%" . $filtro . "%' ";
}

if($nivelId == 92){//exportador Rut
    $query = "Select distinct '''' + exportadorRut + '''' as id, (exportadorRut) as name From aduanaExpoExportadoresRut (nolock) Where exportadorRut like '%" . $filtro . "%' ";
}

if($nivelId == 93){//marca
    $query = "Select distinct '''' + marca + '''' as id, (marca) as name From aduanaExpoMarcas (nolock) Where marca like '%" . $filtro . "%' ";
}

if($nivelId == 94){//variedad
    $query = "Select distinct '''' + variedad + '''' as id, (variedad) as name From aduanaExpoVariedad (nolock) Where variedad like '%" . $filtro . "%' ";
}


if($nivelId == 95){//aduana    
    $query = "Select distinct '''' + aduana + '''' as id, (aduana) as name From aduanaImpoAduanas (nolock) Where aduana like '%" . $filtro . "%' ";
}

if($nivelId == 96){//clausula
    $query = "Select distinct '''' + clausula + '''' as id, (clausula) as name From aduanaImpoClausulas (nolock) Where clausula like '%" . $filtro . "%' ";
}

if($nivelId == 97){//Importador
    $query = "Select distinct '''' + Importador + '''' as id, (Importador) as name From aduanaImpoImportadores (nolock) Where Importador like '%" . $filtro . "%' ";
}

if($nivelId == 98){//forma pago
    $query = "Select distinct '''' + formaPago + '''' as id, (formaPago) as name From aduanaImpoFormasDePago (nolock) Where formaPago like '%" . $filtro . "%' ";
}

if($nivelId == 99){//naviera
    $query = "Select distinct '''' + naviera + '''' as id, (naviera) as name From aduanaImpoNavieras (nolock) Where naviera like '%" . $filtro . "%' ";
}

if($nivelId == 100){//pais destino 
    $query = "Select distinct '''' + paisOrigen + '''' as id, (paisOrigen) as name From aduanaImpoPaisOrigen (nolock) Where paisOrigen like '%" . $filtro . "%' ";
}

if($nivelId == 101){//producto
    $query = "Select distinct '''' + producto + '''' as id, (producto) as name From aduanaImpoProductos (nolock) Where producto like '%" . $filtro . "%' ";
}

if($nivelId == 102){//puerto descarga
    $query = "Select distinct '''' + puertoDescarga + '''' as id, (puertoDescarga) as name From aduanaImpoPuertoDescarga (nolock) Where puertoDescarga like '%" . $filtro . "%' ";
}

if($nivelId == 103){//puertoEmbarque
    $query = "Select distinct '''' + puertoEmbarque + '''' as id, (puertoEmbarque) as name From aduanaImpoPuertoEmbarque (nolock) Where puertoEmbarque like '%" . $filtro . "%' ";
}

if($nivelId == 104){//tipo bulto
    $query = "Select distinct '''' + tipoBulto + '''' as id, (tipoBulto) as name From aduanaImpoTipoBulto (nolock) Where tipoBulto like '%" . $filtro . "%' ";
}

if($nivelId == 105){//tipoCarga
    $query = "Select distinct '''' + tipoCarga + '''' as id, (tipoCarga) as name From aduanaImpoTipoCarga (nolock) Where tipoCarga like '%" . $filtro . "%' ";
}

if($nivelId == 106){//unidad medida
    $query = "Select distinct '''' + unidadMedida + '''' as id, (unidadMedida) as name From aduanaImpoUnidadMedida (nolock) Where unidadMedida like '%" . $filtro . "%' ";
}

if($nivelId == 107){//via transporte
    $query = "Select distinct '''' + viaTransporte + '''' as id, (viaTransporte) as name From aduanaImpoViaTransporte (nolock) Where viaTransporte like '%" . $filtro . "%' ";
}

if($nivelId == 108){//Importador Rut
    $query = "Select distinct '''' + ImportadorRut + '''' as id, (ImportadorRut) as name From aduanaImpoImportadoresRut (nolock) Where ImportadorRut like '%" . $filtro . "%' ";
}

if($nivelId == 109){//marca
    $query = "Select distinct '''' + marca + '''' as id, (marca) as name From aduanaImpoMarcas (nolock) Where marca like '%" . $filtro . "%' ";
}

if($nivelId == 110){//variedad
    $query = "Select distinct '''' + variedad + '''' as id, (variedad) as name From aduanaImpoVariedades (nolock) Where variedad like '%" . $filtro . "%' ";
}

/** VEHICULOS **/


if($nivelId == 111){//rut
    $query = "Select distinct clienteId as id, (clienteRut) as name From Mercado..maeAdClientesVeh (nolock) Where clienteRut like '%" . $filtro . "%' ";
}

if($nivelId == 112){//nombre importador
    $query = "Select distinct clienteId as id, (clienteNombre) as name From Mercado..maeAdClientesVeh (nolock) Where clienteNombre like '%" . $filtro . "%' ";
}

if($nivelId == 121){//naviera
    $query = "Select distinct navieraId as id, (navieraNombre) as name From Mercado..maeAdNavieras (nolock) Where navieraNombre like '%" . $filtro . "%' ";
}

if($nivelId == 113){//tipo
    $query = "Select distinct tipoid as id, (tipoNombreIngles) as name From Mercado..maeAdTipos (nolock) Where tipoNombreIngles like '%" . $filtro . "%' ";
}

if($nivelId == 120){//productos
    $query = "Select distinct productoId as id, (productoNombreIngles) as name From Mercado..maeAdProductos (nolock) Where productoNombreIngles like '%" . $filtro . "%' and estado = 1";
}

if($nivelId == 114){//marcas
    $query = "Select distinct marcaId as id, (marcaNombre) as name From Mercado..maeAdMarcas (nolock) Where marcaNombre like '%" . $filtro . "%' ";
}

if($nivelId == 115){//paisOrigen
    $query = "Select distinct paisId as id, (paisNombre) as name From Mercado..maeAdPaises (nolock) Where paisNombre like '%" . $filtro . "%' ";
}

if($nivelId == 116){//puertoEmbarque
    $query = "Select distinct puertoId as id, (puertoNombre) as name From Mercado..maeAdPuertos (nolock) Where puertoNombre like '%" . $filtro . "%' ";
}

if($nivelId == 117){//puertoDescarga
    $query = "Select distinct puertoId as id, (puertoNombre) as name From Mercado..maeAdPuertos (nolock) Where puertoNombre like '%" . $filtro . "%' ";
}

if($nivelId == 118){//aduana
    $query = "Select distinct aduanaId as id, (aduanaNombre) as name From Mercado..maeAdAduanas (nolock) Where aduanaNombre like '%" . $filtro . "%' ";
}

if($nivelId == 119){//pago
    $query = "Select distinct pagoId as id, (pagoNombre) as name From Mercado..maeAdPagos (nolock) Where pagoNombre like '%" . $filtro . "%' ";
}

if($nivelId == 122){//via de transporte
    $query = "Select distinct viaId as id, (viaNombreIngles) as name From Mercado..maeAdVias (nolock) Where viaNombreIngles like '%" . $filtro . "%' ";
	
}

if($nivelId == 123){//estado
    $query = "Select distinct estadoId as id, (estadoNombre) as name From Mercado..maeAdestados (nolock) Where estadoNombre like '%" . $filtro . "%' ";
}

if($nivelId == 124){//naves Tramp
    $query = "Select idNave as id, (nombreNave) as name From Mercado..navesTramp (nolock) Where nombreNave like '%" . $filtro . "%' and estadoNave = 1";
}

if($nivelId == 125 || $nivelId == 126){//embarcador - consignatario NYK
    $query = "Select idCliente as id, (nombreCliente) as name From Mercado..clientesNyk (nolock) Where nombreCliente like '%" . $filtro . "%' and estadoCliente = 1";
}

if($nivelId == 127){//embarcador - consignatario NYK
    $query = "Select tipoId as id, (nombreContenedor) as name From Mercado..tiposContenedoresTam (nolock) Where nombreContenedor like '%" . $filtro . "%' and estado = 1";
}

if($nivelId == 128 || $nivelId == 129){//embarcador - consignatario NEW
    $query = "Select idCliente as id, (nombreCliente) as name From Mercado..clientesNew (nolock) Where nombreCliente like '%" . $filtro . "%' ";
}



/** NEW DECLARACIONES **/

if($nivelId == 131){//grupoEmisores
    $query = "Select grupoEmisorId as id, (grupoEmisorNombre) as name From Mercado..maeAdGrupoEmisores (nolock) Where grupoEmisorNombre like '%" . $filtro . "%' ";
}

if($nivelId == 132){//almacen
    $query = "Select almacenId as id, (almacenNombre) as name From Mercado..maeAdAlmacenes (nolock) Where almacenNombre like '%" . $filtro . "%' ";
}


if($nivelId == 133 || $nivelId == 134){//clientes aduana
    $query = "Select clienteId as id, (clienteNombre) as name From Mercado..maeAdClientes (nolock) Where clienteNombre like '%" . $filtro . "%' ";
}

if($nivelId == 139 || $nivelId == 140){//clientes aduana
    $query = "Select clienteId as id, (clienteNombre) as name From Mercado..maeAdClientesDelfin (nolock) Where clienteNombre like '%" . $filtro . "%' ";
}

if($nivelId == 141){//clausula
    $query = "Select clausulaId as id, (clausulaNombre) as name From Mercado..maeAdClausulas (nolock) Where clausulaNombre like '%" . $filtro . "%' ";
}


/** NEW DECLARACIONES AEREO **/

if($nivelId == 135){//grupoEmisores aereo
    $query = "Select grupoEmisorId as id, (grupoEmisorNombre) as name From Mercado..maeAdGrupoEmisoresAereo (nolock) Where grupoEmisorNombre like '%" . $filtro . "%' ";
}

if($nivelId == 136){//cia aerea
    $query = "Select grupoNavieraId as id, (grupoNavieraNombre) as name From Mercado..maeAdGrupoNavierasAereo (nolock) Where grupoNavieraNombre like '%" . $filtro . "%' ";
}


/** NEW DECLARACIONES TERRESTRE **/

if($nivelId == 137){//cia terrestre
    $query = "Select navieraId as id, (navieraNombre) as name From Mercado..maeAdNavierasTerrestre (nolock) Where navieraNombre like '%" . $filtro . "%' ";
}

if($nivelId == 138){//grupoEmisores terrestre
    $query = "Select grupoEmisorId as id, (grupoEmisorNombre) as name From Mercado..maeAdGrupoEmisoresTerrestre (nolock) Where grupoEmisorNombre like '%" . $filtro . "%' ";
}

/** DECLARACIONES COMPLETO **/

if($nivelId == 143){//cia transporte
    $query = "Select ciaTransporteId as id, (ciaTransporteNombre) as name From sempat..declaracionesCompletoCiaTransporte (nolock) Where ciaTransporteNombre like '%" . $filtro . "%' ";
}




/** GLENCORE **/

if($clienteId == 22 && $nivelId == 20){//mercaderia
	$query = "Select idCommodity as id, (nombreCommodityEsp) as name From Mercado..commodities (nolock) Where nombreCommodityEsp like '%" . $filtro . "%' and estadoCommodity = 1 and idCommodity in (379,380,381,382,383,384,386,396,397,407,436,1115,1116,1117,1118,1119,1120,1121,1122,1123,1124, 458,474,1101,1112,1113,1114,1125,1126,1127,1128,1129,1130,1131,1132,1133,1134,1136,1137,1138,1139,1140)";	
}

if($clienteId == 22 && $nivelId == 21){//gran familia
    $query = "Select idGranFamilia as id, (nombreGranFamiliaEsp) as name From Mercado..granfamilias (nolock) Where nombreGranFamiliaEsp like '%" . $filtro . "%' and estadoGranFamilia = 1 and idGranFamilia in (3,8,9)";
}

if($clienteId == 22 && $nivelId == 22){//familia
    $query = "Select idFamilia as id, (nombreFamiliaEsp) as name From Mercado..familias (nolock) Where nombreFamiliaEsp like '%" . $filtro . "%' and estadoFamilia = 1 and idFamilia in (18,35,46,58)";
}

if($clienteId == 22 && $nivelId == 23){//sub familia
    $query = "Select idSubFamilia as id, (nombreSubFamiliaEsp) as name From Mercado..subFamilia (nolock) Where nombreSubFamiliaEsp like '%" . $filtro . "%' and estadoSubFamilia = 1 and idSubFamilia in (237,274,532,674,909,1013,1110)";
}



/** VERTICALES **/

if($nivelId == 144){//mercaderia
	$query = "Select verticalId as id, (verticalNombre) as name From Mercado..verticales (nolock) Where verticalNombre like '%" . $filtro . "%' ";
}

if($nivelId == 145){//gran familia
    $query = "Select subVerticalId as id, (subVerticalNombre) as name From Mercado..verticalesSub (nolock) Where subVerticalNombre like '%" . $filtro . "%' ";
}

if($nivelId == 146){//familia
    $query = "Select categoriaId as id, (categoriaNombre) as name From Mercado..verticalesCategoria (nolock) Where categoriaNombre like '%" . $filtro . "%' ";
}

/** VINOS **/

if($nivelId == 147){//tipoVino
    $query = "Select tipoId as id, (tipoNombre) as name From Mercado..vinosTipos (nolock) Where tipoNombre like '%" . $filtro . "%' ";
}
	
if($nivelId == 148){//categoria
    $query = "Select categoriaId as id, (categoriaNombre) as name From Mercado..vinosCategoria (nolock) Where categoriaNombre like '%" . $filtro . "%' ";
}

if($nivelId == 149){//marca
    $query = "Select marcaId as id, (marcaNombre) as name From Mercado..vinosMarcas (nolock) Where marcaNombre like '%" . $filtro . "%' ";
}
	
	

$rs = $base_datos->sql_query($query);

$arr = array();
$rs = $base_datos->sql_query($query);

# Collect the results
//while($obj = mssql_fetch_object($rs)) {
//    $arr[] = $obj;
//}

while($row = mssql_fetch_assoc($rs)){
    $arr[] = array_map('utf8_encode', $row);
}

# JSON-encode the response
$json_response = json_encode($arr);

# Optionally: Wrap the response in a callback function for JSONP cross-domain support
if($_GET["callback"]) {
    $json_response = $_GET["callback"] . "(" . $json_response . ")";
}

# Return the response
echo $json_response;

?>
