<?PHP

session_start();
include ("../librerias/conexion.php");
require('../clases/sempat.class.php');
$objSempat = new sempat();

$usuarioId = $_SESSION['SEMPAT_usuarioId'];
$clienteId = $_SESSION['SEMPAT_clienteId'];

$mercado = mb_convert_encoding(trim($_POST['mercado']), "ISO-8859-1", "UTF-8");
$consultaTabla = $objSempat->manifiestoFechaDefecto($mercado, $clienteId);

if($clienteId == "4" && ($mercado == "1" || $mercado == "2" || $mercado == "3")){
	//echo '01-08-2015,31-07-2016';
	echo date('d-m-Y', strtotime($consultaTabla["fechaDesde"])) . "," .  date('d-m-Y', strtotime($consultaTabla["fechaHasta"]));
}

/** Descomentar cuando llegua quincena de practicaje **/
//else if($clienteId == "1" && $mercado == "PRACTICAJE"){
//	echo '01-03-2017,28-02-2018';
//}
/******************************************************/
else if($clienteId == "18" && $mercado == "1"){
	echo '01-08-2015,31-07-2016';
	//echo date('d-m-Y', strtotime($consultaTabla["fechaDesde"])) . "," .  date('d-m-Y', strtotime($consultaTabla["fechaHasta"]));
}
else if($clienteId == "15" && $mercado == "1"){
	echo '01-01-2016,31-12-2016';
	//echo date('d-m-Y', strtotime($consultaTabla["fechaDesde"])) . "," .  date('d-m-Y', strtotime($consultaTabla["fechaHasta"]));
}
else if($clienteId == "4" && $mercado == "TRAMP"){
	//echo '01-01-2016, 31-12-2016';
	echo date('d-m-Y', strtotime($consultaTabla["fechaDesde"])) . "," .  date('d-m-Y', strtotime($consultaTabla["fechaHasta"]));
}
else if($clienteId == "15" && $mercado == "PRACTICAJE"){
	echo '01-01-2015, 31-12-2015';
	//echo date('d-m-Y', strtotime($consultaTabla["fechaDesde"])) . "," .  date('d-m-Y', strtotime($consultaTabla["fechaHasta"]));
}
else if($clienteId == "16" && $mercado == "DECLARACIONES"){	
	echo date('d-m-Y', strtotime($consultaTabla["fechaDesde"])) . "," .  date('d-m-Y', strtotime($consultaTabla["fechaHasta"]));
}
else if($clienteId == "22" && $mercado == "DECLARACIONES"){	 // GLENCORE
	echo '01-01-2017, 31-12-2017';
	//echo date('d-m-Y', strtotime($consultaTabla["fechaDesde"])) . "," .  date('d-m-Y', strtotime($consultaTabla["fechaHasta"]));
}
else if($clienteId == "39" && $mercado == "DECLARACIONES"){	 // GLENCORE
	echo '01-10-2016, 30-09-2017';
	//echo date('d-m-Y', strtotime($consultaTabla["fechaDesde"])) . "," .  date('d-m-Y', strtotime($consultaTabla["fechaHasta"]));
}
else{
	echo date('d-m-Y', strtotime($consultaTabla["fechaDesde"])) . "," .  date('d-m-Y', strtotime($consultaTabla["fechaHasta"]));
}


?>
