<?PHP

session_start();
include ("../librerias/conexion.php");
require('../clases/sempat.class.php');
$objSempat = new sempat();


$usuarioId = $_SESSION['SEMPAT_usuarioId'];
$clienteId = $_SESSION['SEMPAT_clienteId'];

$moduloId = mb_convert_encoding(trim($_POST['modulo']), "ISO-8859-1", "UTF-8");


$carpeta = mb_convert_encoding(trim($_POST['carpeta']), "ISO-8859-1", "UTF-8");

$consultaTabla = $objSempat->extraccionesArchivos($usuarioId, $moduloId);

$archivosHTML = "";

if ($consultaTabla) {                
    
    while ($rowdatos = mssql_fetch_array($consultaTabla)) {
	
	
	    if($moduloId == 37 || $clienteId == 41){
		    $archivosHTML .= "<tr>
                            <td class=\"left\">
                                " . htmlentities($rowdatos["nombre_archivo"]) . "
                            </td>
                            <td>
                                " . date("d-m-Y   H:i", strtotime(htmlentities($rowdatos["fecha_reporte"]))) . "
                            </td>
                            <td>
                            <a target=\"_blank\" title=\"Descargar\" href=\"../../Reportes/" . htmlentities($carpeta) . "/" . $usuarioId . "/" . htmlentities($rowdatos["nombre_archivo"]) . "\">
                                <img title=\"Descargar\" alt=\"Descargar\" src=\"../../imagenes/excel.png\" style=\"width:20px; height:20px;\"/> 
                                    </a>
                            </td> 
                            <td>
                                <a href=\"#\" onClick=\"eliminarArchivo('" . htmlentities($rowdatos["id_archivo"]) . "', '" . $moduloId ."', '" . htmlentities($carpeta) . "'); return false;\">Delete</a>
                            </td>
                        </tr>";
		}
		else{
		        $archivosHTML .= "<tr>
                            <td class=\"left\">
                                " . htmlentities($rowdatos["nombre_archivo"]) . "
                            </td>
                            <td>
                                " . date("d-m-Y   H:i", strtotime(htmlentities($rowdatos["fecha_reporte"]))) . "
                            </td>
                            <td>
                            <a target=\"_blank\" title=\"Descargar\" href=\"../../Reportes/" . htmlentities($carpeta) . "/" . $usuarioId . "/" . htmlentities($rowdatos["nombre_archivo"]) . "\">
                                <img title=\"Descargar\" alt=\"Descargar\" src=\"../../imagenes/excel.png\" style=\"width:20px; height:20px;\"/> 
                                    </a>
                            </td> 
                            <td>
                                <a href=\"#\" onClick=\"eliminarArchivo('" . htmlentities($rowdatos["id_archivo"]) . "', '" . $moduloId ."', '" . htmlentities($carpeta) . "'); return false;\">Eliminar</a>
                            </td>
                        </tr>";
		}
        
    }
}

echo $archivosHTML;






?>
