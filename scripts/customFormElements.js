/*

CUSTOM FORM ELEMENTS

Created by Ryan Fait
www.ryanfait.com

The only things you may need to change in this file are the following
variables: checkboxHeight, radioHeight and selectWidth (lines 24, 25, 26)

The numbers you set for checkboxHeight and radioHeight should be one quarter
of the total height of the image want to use for checkboxes and radio
buttons. Both images should contain the four stages of both inputs stacked
on top of each other in this order: unchecked, unchecked-clicked,
checked, checked-clicked.

You may need to adjust your images a bit if there is a slight vertical
movement during the different stages of the button activation.

The value of selectWidth should be the width of your select list image.

Visit http://ryanfait.com/ for more information.

*/

var checkboxHeight = "25";
var radioHeight = "25";
var selectWidth = "190";


/* No need to change anything after this */


document.write('<style type="text/css">input.styled { display: none; } select.styled { position: relative; width: ' + selectWidth + 'px; opacity: 0; filter: alpha(opacity=0); z-index: 5; } .disabled { opacity: 0.5; filter: alpha(opacity=50); }</style>');

var Custom = {
    init: function() {
        var inputs = document.getElementsByTagName("input"), span = Array(), textnode, option, active;
        for(a = 0; a < inputs.length; a++) {
            if((inputs[a].type == "checkbox" || inputs[a].type == "radio") && inputs[a].className.indexOf("styled") > -1) {
                span[a] = document.createElement("span");

                if(inputs[a].checked == true){
                    span[a].className = "checkboxChecked";
                }
                else{
                    span[a].className = inputs[a].type;
                }                               
                

                //				if(inputs[a].checked == true) {
                //					if(inputs[a].type == "checkbox") {
                //						position = "0 -" + (checkboxHeight*2) + "px";
                //						span[a].style.backgroundPosition = position;
                //					} else {
                //						position = "0 -" + (radioHeight*2) + "px";
                //						span[a].style.backgroundPosition = position;
                //					}
                //				}
                inputs[a].parentNode.insertBefore(span[a], inputs[a]);
                inputs[a].onchange = Custom.clear;
                if(!inputs[a].getAttribute("disabled")) {
                    span[a].onmousedown = Custom.pushed;
                    span[a].onmouseup = Custom.check;
                } else {
                    span[a].className = span[a].className += " disabled";
                }
            }
        }
        inputs = document.getElementsByTagName("select");
        for(a = 0; a < inputs.length; a++) {
            if(inputs[a].className.indexOf("styled") > -1) {
                                                                
                option = inputs[a].getElementsByTagName("option");
                active = option[0].childNodes[0].nodeValue;
                textnode = document.createTextNode(active);
                for(b = 0; b < option.length; b++) {
                    if(option[b].selected == true) {
                        textnode = document.createTextNode(option[b].childNodes[0].nodeValue);
                    }
                }
                span[a] = document.createElement("span");

                if(inputs[a].className.indexOf("filtro") > -1 && inputs[a].value == ""){
                    span[a].className = "selectPlaceHolder";
                }
                else{
                    if(inputs[a].className.indexOf("filtro") > -1){
                        span[a].className = "selectFiltro";
                    }
                    else{
                        span[a].className = "select";
                    }
                }

                span[a].id = "select" + inputs[a].name;
                span[a].appendChild(textnode);
                inputs[a].parentNode.insertBefore(span[a], inputs[a]);
                if(!inputs[a].getAttribute("disabled")) {
                    inputs[a].onchange = Custom.choose;
                } else {
                    inputs[a].previousSibling.className = inputs[a].previousSibling.className += " disabled";
                }
            }
        }
        document.onmouseup = Custom.clear;
    },
    pushed: function() {
        element = this.nextSibling;
//        if(element.checked == true && element.type == "checkbox") {
//            this.style.backgroundPosition = "0 -" + checkboxHeight*3 + "px";
//        } else if(element.checked == true && element.type == "radio") {
//            this.style.backgroundPosition = "0 -" + radioHeight*3 + "px";
//        } else if(element.checked != true && element.type == "checkbox") {
//            this.style.backgroundPosition = "0 -" + checkboxHeight + "px";
//        } else {
//            this.style.backgroundPosition = "0 -" + radioHeight + "px";
//        }
    },
    check: function() {

        element = this.nextSibling;
        if(element.checked == true && element.type == "checkbox") {
            /*this.style.backgroundPosition = "0 0";*/
            this.className  = "checkbox";
            element.click();
            element.checked = false;
        } else {
            if(element.type == "checkbox") {
                /*this.style.backgroundPosition = "0 -" + checkboxHeight*2 + "px";*/
                this.className  = "checkboxChecked";
                element.click();
                element.checked = true;
            } else {
                this.style.backgroundPosition = "0 -" + radioHeight*2 + "px";
                group = this.nextSibling.name;
                inputs = document.getElementsByTagName("input");
                for(a = 0; a < inputs.length; a++) {
                    if(inputs[a].name == group && inputs[a] != this.nextSibling) {
                        inputs[a].previousSibling.style.backgroundPosition = "0 0";
                    }
                }
            }
            element.checked = true;
        }
    },
    clear: function() {
        inputs = document.getElementsByTagName("input");
//        for(var b = 0; b < inputs.length; b++) {
//            if(inputs[b].type == "checkbox" && inputs[b].checked == true && inputs[b].className.indexOf("styled") > -1) {
//                inputs[b].previousSibling.style.backgroundPosition = "0 -" + checkboxHeight*2 + "px";
//            } else if(inputs[b].type == "checkbox" && inputs[b].className.indexOf("styled") > -1) {
//                inputs[b].previousSibling.style.backgroundPosition = "0 0";
//            } else if(inputs[b].type == "radio" && inputs[b].checked == true && inputs[b].className.indexOf("styled") > -1) {
//                inputs[b].previousSibling.style.backgroundPosition = "0 -" + radioHeight*2 + "px";
//            } else if(inputs[b].type == "radio" && inputs[b].className.indexOf("styled") > -1) {
//                inputs[b].previousSibling.style.backgroundPosition = "0 0";
//            }
//        }
    },
    choose: function() {    
        
        if(this.id == "selectGraficoReporte" || this.id  == "selectGrafico"){
            
            var nombreTabla = "tBody";
            
            if($('#selectTipo').val() == "C20"){
                nombreTabla = "tBodyC20";
            }
            
            if($('#selectTipo').val() == "C40"){
                nombreTabla = "tBodyC40";
            }
                                    
            if($("#selectOrdenA").val() == "AP"){                
                
                if($("#selectOrdenB").val() == "og"){                    
                    nombreTabla = "tbodyAgenciaPuertoGeo";
                }
                else{                    
                    nombreTabla = "tbodyAgenciaPuerto";
                }
            }

            if($("#selectOrdenA").val() == "PA"){

                if($("#selectOrdenB").val() == "og"){                    
                    nombreTabla = "tbodyPuertoAgenciaGeo";
                }
                else{                
                    nombreTabla = "tbodyPuertoAgencia";
                }
            }
            
                                                                        
            if(this.id == "selectGraficoReporte"){
                graficoCargaReporte($('#tablaGrafico')[0], nombreTabla);
            }
            else{   
                
                if($('#selectGrafico').val() == "pie"){
                    $('.tdMkt').css("display", "");
                }
                else{
                    $('.tdMkt').css("display", "none");
                }                                
                
                graficoCargaIndicador($('#tablaGrafico')[0], nombreTabla, null);
            }
            
        }                    
        
        
        if(this.id == "selectTipo"){
                        
            if($('#selectGrafico').val() == "pie"){
                $('.tdMkt').css("display", "");
            }
            else{
                $('.tdMkt').css("display", "none");
            }
            
            graficoTipoIndicador();            
        }
        
        
        if(this.id == "selectTipoReporte"){
            graficoTipoReporte();
        }
        
        
        
        if(this.id == "selectMkt"){
            
            var nombreTabla = "tBody";
            
            if($("#selectOrdenA").val() == "AP"){
                
                if($("#selectOrdenB").val() == "og"){                    
                    nombreTabla = "tbodyAgenciaPuertoGeo";
                }
                else{                    
                    nombreTabla = "tbodyAgenciaPuerto";
                }                
                
            }
            else if($("#selectOrdenA").val() == "PA"){

                if($("#selectOrdenB").val() == "og"){                    
                    nombreTabla = "tbodyPuertoAgenciaGeo";
                }
                else{                
                    nombreTabla = "tbodyPuertoAgencia";
                }               
            }
            else{                
                                            
                if($('#selectTipo').val() == "C20"){
                    nombreTabla = "tBodyC20";
                }

                if($('#selectTipo').val() == "C40"){
                    nombreTabla = "tBodyC40";
                }                                
            }
            
            graficoCargaIndicador($('#tablaGrafico')[0], nombreTabla, '');
                                    
        }
                              
        
        if(this.id == "selectOrdenA" || this.id == "selectOrdenB"){                                    
             graficoOrden("AGENTE PORTUARIO", "PUERTO")                        
        }
        
        
        
        
        option = this.getElementsByTagName("option");
        placeHolder = this.className.indexOf("filtro");        
        for(d = 0; d < option.length; d++) {
            if(option[d].selected == true) {                                

                document.getElementById("select" + this.name).childNodes[0].nodeValue = option[d].childNodes[0].nodeValue;

                if(d == 0 && placeHolder > -1){
                    document.getElementById("select" + this.name).className = "selectPlaceHolder";
                }
                else{
                    if(placeHolder > -1){
                        document.getElementById("select" + this.name).className = "selectFiltro";
                    }
                    else{
                        document.getElementById("select" + this.name).className = "select";
                    }
                }
            }
        }
    },    
    initChkbox: function() {
        var inputs = document.getElementsByTagName("input"), span = Array(), textnode, option, active;
        for(a = 0; a < inputs.length; a++) {
            if((inputs[a].type == "checkbox") && inputs[a].className.indexOf("styled") > -1) {
                span[a] = document.createElement("span");

                if(inputs[a].checked == true){
                    span[a].className = "checkboxChecked";
                }
                else{
                    span[a].className = inputs[a].type;
                }                               
                
                inputs[a].parentNode.insertBefore(span[a], inputs[a]);
                inputs[a].onchange = Custom.clear;
                if(!inputs[a].getAttribute("disabled")) {
                    span[a].onmousedown = Custom.pushed;
                    span[a].onmouseup = Custom.check;
                } else {
                    span[a].className = span[a].className += " disabled";
                }
            }
        }
                
    }
    
}
window.onload = Custom.init;



