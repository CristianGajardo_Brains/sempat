/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var opt = {
    "chart": {
        "renderTo": "divGrafico2",
        "defaultSeriesType": "column"
    },
    "title": {
        "text": "Exportaciones e Importaciones (Chile)<br>N° de Contenedores (C20 + C40)"
    },
    "subtitle": {
        "text": " ",
        "y": 50
    },
    "xAxis": {
        "categories": ["JUN-2016", "JUL-2016", "AGO-2016", "SEP-2016", "OCT-2016", "NOV-2016", "DIC-2016", "ENE-2017", "FEB-2017", "MAR-2017", "ABR-2017", "MAY-2017"]
    },
    "yAxis": {
        "min": 0,
        "labels": {},
        "title": {
            "text": "N° de Contenedores (C20 + C40)"
        }
    },
    "legend": {
        "backgroundColor": "#FFFFFF"
    },
    "tooltip": {},
    "plotOptions": {
        "column": {
            "pointPadding": 0.2,
            "borderWidth": 0
        }
    },
    "series": [{
        "name": "EXPO",
        "data": [64670, 63772, 63279, 53302, 65285, 54837, 70694, 57699, 56786, 76609, 64314, 65427]
    }, {
        "name": "IMPO",
        "data": [48340, 51756, 54444, 50474, 57323, 50021, 52570, 52700, 46393, 48761, 49679, 51133]
    }]
}
