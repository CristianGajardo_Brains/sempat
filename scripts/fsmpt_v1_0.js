

function replaceAll(text, busca, reemplaza){
    while (text.toString().indexOf(busca) != -1)
        text = text.toString().replace(busca,reemplaza);
    return text;
}


var era;
var previo = null;

function deschequear(rbutton) {

    if(previo && previo!= rbutton){
        previo.era=false;
    }

    if(rbutton.checked==true && rbutton.era==true){
        rbutton.checked=false;
    }

    rbutton.era=rbutton.checked;
    previo=rbutton;

}


function retornarNombreMes(nombreMes){
	
	var idioma = $("#idioma").val();
	
	if(idioma == "EN"){

		switch(nombreMes){

			case 'JAN':
				nombreMes = "January";
				break;

			case "FEB":
				nombreMes = "February";
				break;

			case "MAR":
				nombreMes = "March";
				break;

			case "APR":
				nombreMes = "April";
				break;

			case "MAY":
				nombreMes = "May";
				break;

			case "JUN":
				nombreMes = "June";
				break;

			case "JUL":
				nombreMes = "July";
				break;

			case "AUG":
				nombreMes = "August";
				break;

			case "SEP":
				nombreMes = "September";
				break;

			case "OCT":
				nombreMes = "October";
				break;

			case "NOV":
				nombreMes = "November";
				break;

			case "DEC":
				nombreMes = "December";
				break;
		}
    }	
	else{
		
		switch(nombreMes){

			case 'ENE':
				nombreMes = "Enero";
				break;

			case "FEB":
				nombreMes = "Febrero";
				break;

			case "MAR":
				nombreMes = "Marzo";
				break;

			case "ABR":
				nombreMes = "Abril";
				break;

			case "MAY":
				nombreMes = "Mayo";
				break;

			case "JUN":
				nombreMes = "Junio";
				break;

			case "JUL":
				nombreMes = "Julio";
				break;

			case "AGO":
				nombreMes = "Agosto";
				break;

			case "SEP":
				nombreMes = "Septiembre";
				break;

			case "OCT":
				nombreMes = "Octubre";
				break;

			case "NOV":
				nombreMes = "Noviembre";
				break;

			case "DIC":
				nombreMes = "Diciembre";
				break;
		}		
		
	}

    return nombreMes;

}


function toLowerCaseFirst(string){
    return string.chartAt(0).toLowerCase() + string.slice(1);
}


function mostrarFiltrosBusqueda(){
    $( "#divPanelFiltros" ).slideToggle( "slow" );
}


function abrirReporte(){
    $('#divGuardarReporte').fadeIn();    
}


function cerrarReporte(){
    $('#divGuardarReporte').fadeOut();    
}


function abrirReporte2(){
    $('#divReportesGuardados').fadeIn();    
}


function cerrarReporte2(){
    $('#divReportesGuardados').fadeOut();    
}


function desplegarNivel(selector, nivel){
    console.log("desplegarNivel fsmpt v1");
    var trs = $("tr[id^='" + selector + "-']");
    var estado = "";

    for(var i = 0; i < trs.length; i++){                        

        //if($(trs[i]).css('display') == 'none' && (trs[i].className == ("nivel" + nivel) || trs[i].className == ("nivel" + nivel) + " OK")){
        if($(trs[i]).css('display') == 'none' && $(trs[i]).hasClass("nivel" + nivel)){
            trs[i].style.display = "";                
            estado = 1;
        }
        else{
            trs[i].style.display = "none";                
        }
    }

    var trs;

    if(estado == 1 && nivel == 2){
        trs = $("tr[id^='" + selector + "-']");
        for(var i = 0; i < trs.length; i++){                 
            if(trs[i].className == 'nivel3' || trs[i].className == 'nivel4' || trs[i].className == 'nivel5'){
                trs[i].style.display = "none";
            }                                
        }                                    
    }

    if(estado == 1 && nivel == 3){
        trs = $("tr[id^='" + selector + "-']");
        for(var i = 0; i < trs.length; i++){                 
            if(trs[i].className == 'nivel4' || trs[i].className == 'nivel5'){
                trs[i].style.display = "none";
            }                                
        }                                    
    }                      
}


function fechasDefecto(mercado){
        
    $.ajax({
        type: "POST",
        url: "../../complementos/fechasDefecto.php",
        async: false,
        data: {
            mercado: mercado
        }
    })
    .done(function(msg) {                                
        var vfechas = msg.trim().split(','); 
        var fechaDesde = vfechas[0].toString();
        var fechaHasta = vfechas[1].toString();

        $("#fechaDesde").attr("value", fechaDesde);
        $("#fechaHasta").attr("value", fechaHasta);

    });                              
}


function filtrosValores(obj, nombre){

    var filtro = '';
    var temp = '';

    if(obj.val() != "" && obj.val() != undefined){

        count++;    
        filtro += '' + nombre + ':  ';

        obj.prev('div').find('li p').each(function(){
            temp += $(this).text()+ ", ";
        });

        temp = temp.substring(0, temp.length - 2);

        if(temp.length > 65){
            filtro += "<font title='" + temp + "'>" + temp.substring(0, 65) + "...</font><br>";
        }
        else{
            filtro += "<font title='" + temp + "'>" + temp + "</font><br>";
        }
        
        

    }

    return filtro;
}

/** FECHAS **/


function validarFormatoFecha(campo) {
    var RegExPattern = /^\d{1,2}\-\d{1,2}\-\d{2,4}$/;
    if ((campo.match(RegExPattern)) && (campo!='')) {
        return true;
    } else {
        return false;
    }
}


function existeFecha(fecha){
    var dtCh= "-";
    var minYear=1900;
    var maxYear=2100;
    
    function isInteger(s){
        var i;
        for (i = 0; i < s.length; i++){
            var c = s.charAt(i);
            if (((c < "0") || (c > "9"))) return false;
        }
        return true;
    }
    function stripCharsInBag(s, bag){
        var i;
        var returnString = "";
        for (i = 0; i < s.length; i++){
            var c = s.charAt(i);
            if (bag.indexOf(c) == -1) returnString += c;
        }
        return returnString;
    }
    function daysInFebruary (year){
        return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
    }
    function DaysArray(n) {
        for (var i = 1; i <= n; i++) {
            this[i] = 31
            if (i==4 || i==6 || i==9 || i==11) {
                this[i] = 30
            }
            if (i==2) {
                this[i] = 29
            }
        }
        return this
    }
    
    function isDate(dtStr){
        var daysInMonth = DaysArray(12)
        var pos1=dtStr.indexOf(dtCh)
        var pos2=dtStr.indexOf(dtCh,pos1+1)
        var strDay=dtStr.substring(0,pos1)
        var strMonth=dtStr.substring(pos1+1,pos2)
        var strYear=dtStr.substring(pos2+1)
        strYr=strYear
        if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
        if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
        for (var i = 1; i <= 3; i++) {
            if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
        }
        month=parseInt(strMonth)
        day=parseInt(strDay)
        year=parseInt(strYr)
        if (pos1==-1 || pos2==-1){
            return false
        }
        if (strMonth.length<1 || month<1 || month>12){
            return false
        }
        if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
            return false
        }
        if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
            return false
        }
        if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
            return false
        }
        return true
    }
    
    if(isDate(fecha)){
        return true;
    }else{
        return false;
    }
    
}


function validarFechaMenor(fechaDesde, fechaHasta){
        
    var fechaf = fechaDesde.split("-");
    var day = fechaf[0];
    var month = fechaf[1];
    var year = fechaf[2];
    
    var fd = new Date(year, month, day);
    
    fechaf = fechaHasta.split("-");
    day = fechaf[0];
    month = fechaf[1];
    year = fechaf[2];            
            
    var fh = new Date(year, month, day);

    if (fd > fh){        
        return false;
    }
    else{        
        return true;
    }
    
}


function validarFecha(obj){
    
    var fecha = $(obj).val();        
    
    if(fecha != ""){
    
        if(validarFormatoFecha(fecha)){
            if(!existeFecha(fecha)){
                $("#divMensajeFechas").html("La fecha introducida no existe.")
                $("#divMensajeFechas").css("display", "block");                
            }
            else{
                $("#divMensajeFechas").html("")
                $("#divMensajeFechas").css("display", "none");
            }
        }else{
            $("#divMensajeFechas").html("El formato de la fecha es incorrecto.")
            $("#divMensajeFechas").css("display", "block");
        }        
    }
    else{
        $("#divMensajeFechas").css("display", "none");
    }
    
}


function fechaDiferenciasDias(fechaDesde, fechaHasta){
    var d1 = fechaDesde.split("-");
    var dat1 = new Date(d1[2], parseFloat(d1[1])-1, parseFloat(d1[0]));
    var d2 = fechaHasta.split("-");
    var dat2 = new Date(d2[2], parseFloat(d2[1])-1, parseFloat(d2[0]));
     
    var fin = dat2.getTime() - dat1.getTime();
    var dias = Math.floor(fin / (1000 * 60 * 60 * 24))
             
    return dias;         
}


function seleccionarTodoExtracciones(){

    var estado = $("#chkSeleccionarTodo").attr('checked');

    $("#tablaFiltrosCampos tr").each(function (index) {

        $(this).children("td").each(function (index2, td) {

            $(td).children("input").each(function (index3, input) {                    

                if(estado == "checked"){
                    input.checked = true;
                }
                else{
                    input.checked = false;
                }                                        
            });

            $(td).children("span ").each(function (index3, span) {

                if(estado == "checked"){
                    $(span).removeClass('checkbox');
                    $(span).addClass('checkboxChecked');
                }
                else{
                    $(span).removeClass('checkboxChecked');
                    $(span).addClass('checkbox');
                }                                                                                
            });                                              
        });                    
    });               

}


/** GRAFICO **/

/** Carga datos en la tabla con la estructura asignada para los graficos de lineas y columnas **/
function graficoBarra(table, options) {

    options.xAxis.categories = [];

    $('tbody th', table).each( function(i) {
        options.xAxis.categories.push(this.innerHTML);
    });

    // the data series
    options.series = [];

    $('tr', table).each( function(i) {
        var tr = this;
        $('th, td', tr).each( function(j) {
            if (j > 0) { // skip first column

                if (i == 0) { // get the name and init the series
                    options.series[j - 1] = {
                        name: this.innerHTML,
                        data: []
                    };
                } else { // add values
                    options.series[j - 1].data.push(parseFloat(this.innerHTML));
                }
            }
        });
    });

    var chart = new Highcharts.Chart(options);

}

/** Carga datos en la tabla con la estructura asignada para los graficos de torta **/
function graficoTorta(table, options) {

    // the data series
    options.series = [];
    var l= options.series.length;

    options.series[l] = {
        name: $('thead th')[l+1].innerHTML,
        data: []
    };

    //table.getElements('tbody tr').each( function(tr) {
    $('tbody tr', table).each( function(i, tr) {
        var th = tr.cells[0].innerHTML;
        var td = parseFloat(tr.cells[1].innerHTML.replace(".",""));
        options.series[0].data.push({
            name:th,
            y:td
        });
    });

    var chart = new Highcharts.Chart(options);

}

/** Carga el gráfico de barras o líneas **/
function graficoInicializarBarra(nombreTabla, divGrafico, graficoSelect, subTituloGrafico){

    var tituloGrafico = "";
    var tituloGrafico2 = "";
    var tituloGraficoEjeY = "";
    var decimales = 0;
    var y = 30;

    if($('#tituloGrafico').val() != undefined){
        tituloGrafico = $('#tituloGrafico').val();
        tituloGrafico += "<br>";                                
    }
    
    if($('#tituloGrafico2').val() != undefined){
        tituloGrafico2 = $('#tituloGrafico2').val();                        
        
        if(tituloGrafico2.trim() != ""){
            y = 50;
        }
    }

    if($('#tituloGraficoEjeY').val() != undefined){
        tituloGraficoEjeY = $('#tituloGraficoEjeY').val();
    }

    if($('#decimales') != undefined){
        if(!isNaN($('#decimales').val())){
            decimales = $('#decimales').val();
        }
    }

    var table = $("#" + nombreTabla);

    var options = {
        chart: {
            renderTo: divGrafico,
            defaultSeriesType: graficoSelect
        //borderRadius: 0
        },
        title: {
            text: tituloGrafico + tituloGrafico2
        },
        subtitle: {
            text: subTituloGrafico,
            y: y
        },
        xAxis: {
        },
        yAxis: {
            min:0,
            labels:{
                formatter: function(){
                    return Highcharts.numberFormat(this.value, 0);
                }
            },
            title: {
                text: tituloGraficoEjeY
            }
        },
        legend: {				
            backgroundColor: '#FFFFFF',
            labelFormatter : function(){
                var leg = this.name;
                var leg2 = replaceAll(leg, "&amp;","&");
                
                if(leg2.length > 25){
                    leg2 = leg2.substring(0, 25) + "..."; 
                }
                
                return leg2;
            }

        },
        tooltip: {
            formatter: function() {

                var leg = this.series.name;
                var leg2 = replaceAll(leg, "&amp;","&");

                return '<b>'+ leg2 +'</b><br/>'+
                this.x +': '+ Highcharts.numberFormat(this.y, decimales);
            }
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        }
    };

    graficoBarra(table, options);

}

/** Carga el gráfico de torta **/
function graficoInicializarTorta(nombreTabla, divGrafico, subTituloGrafico, total){

    var tituloGrafico = "";
    var tituloGrafico2 = "";
    var y = 30;

    if($('#tituloGrafico').val() != undefined){
        tituloGrafico = $('#tituloGrafico').val();
        tituloGrafico += "<br>";                        
    }
    
    if($('#tituloGrafico2').val() != undefined){
        tituloGrafico2 = $('#tituloGrafico2').val();
        
        if(tituloGrafico2.trim() != ""){
            y = 50;
        }
    }

    var table = $("#" + nombreTabla);

    var options = {
        chart: {
            renderTo: divGrafico,
            defaultSeriesType: 'pie',
            plotBackgroundColor: null,
            plotBorderWidth: null,
            //borderRadius: 0,
            plotShadow: false
        },
        title: {
            text: tituloGrafico + tituloGrafico2
        },
        subtitle: {
            text: subTituloGrafico,
            y: y
        },
        tooltip: {
            formatter: function() {
                var leg = this.point.name ;
                var leg2 = replaceAll(leg, "&amp;","&");
                
                //return '<b>'+ leg2 +'</b>: '+ this.percentage +' %';
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    color: '#808080',
					//color: '#FE2E2E',
                    connectorColor: '#6D869F',
                    formatter: function() {
                        var leg = this.point.name ;
                        var leg2 = replaceAll(leg, "&amp;","&");
                        
                        if(leg2.length > 18){
                            leg2 = leg2.substring(0, 18) + "..."; 
                        }
                        
                        return '<b>'+ leg2 +'</b>: '+ this.percentage +' %';
                    }
                },
                showInLegend: true
            }
        },
        legend: {			
            labelFormatter: function() {
                var leg = this.name;
                var leg2 = leg.replace("&amp;","&");
                
                if(leg2.length > 18){
                    leg2 = leg2.substring(0, 18) + "..."; 
                }
                
                this.y = replaceAll(this.y, ".", "");
                if(total != 0){
                    return leg2 +': ' + ((this.y/total)*100).toFixed(0) + ' %';
                }
                else{
                    return leg2 +': 0 %';
                }
            }
        }
    };

    graficoTorta(table, options);

}



/** GRAFICO INDICADORES **/

/** Inserta un nuevo dato a las tablas de los graficos **/
function graficoDatoIndicador(nombreObjeto, tablaYtd2, tablaYtd1, tabla12M2, tabla12M1, tablaM2, tablaM1, mktYtd2, mktYtd1, mkt12M2, mkt12M1, mktM2, mktM1){

    /*** YTD 2 ***/

    var thYtd2 = document.createElement('th');
    var tdYtd2 = document.createElement('td');
    var trYtd2 = document.createElement('tr');

    thYtd2.innerHTML = nombreObjeto.replace(/^\s*|\s*$/g,"");
    tdYtd2.innerHTML = replaceAll(mktYtd2, ".","");

    $(trYtd2).append(thYtd2);
    $(trYtd2).append(tdYtd2);
    $(tablaYtd2).append(trYtd2);

    /*** YTD 1 ***/

    var thYtd1 = document.createElement('th');
    var tdYtd1 = document.createElement('td');
    var trYtd1 = document.createElement('tr');

    thYtd1.innerHTML = nombreObjeto.replace(/^\s*|\s*$/g,"");
    tdYtd1.innerHTML = replaceAll(mktYtd1, ".","");

    $(trYtd1).append(thYtd1);
    $(trYtd1).append(tdYtd1);
    $(tablaYtd1).append(trYtd1);

    /*** 12M 2 ***/

    var th12M2 = document.createElement('th');
    var td12M2 = document.createElement('td');
    var tr12M2 = document.createElement('tr');

    th12M2.innerHTML = nombreObjeto.replace(/^\s*|\s*$/g,"");
    td12M2.innerHTML = replaceAll(mkt12M2, ".","");

    $(tr12M2).append(th12M2);
    $(tr12M2).append(td12M2);
    $(tabla12M2).append(tr12M2);

    /*** 12M 1 ***/

    var th12M1 = document.createElement('th');
    var td12M1 = document.createElement('td');
    var tr12M1 = document.createElement('tr');

    th12M1.innerHTML = nombreObjeto.replace(/^\s*|\s*$/g,"");
    td12M1.innerHTML = replaceAll(mkt12M1, ".","");

    $(tr12M1).append(th12M1);
    $(tr12M1).append(td12M1);
    $(tabla12M1).append(tr12M1);

    /*** M 2 ***/

    var thM2 = document.createElement('th');
    var tdM2 = document.createElement('td');
    var trM2 = document.createElement('tr');

    thM2.innerHTML = nombreObjeto.replace(/^\s*|\s*$/g,"");
    tdM2.innerHTML = replaceAll(mktM2, ".","");

    $(trM2).append(thM2);
    $(trM2).append(tdM2);
    $(tablaM2).append(trM2);

    /*** M 1 ***/

    var thM1 = document.createElement('th');
    var tdM1 = document.createElement('td');
    var trM1 = document.createElement('tr');

    thM1.innerHTML = nombreObjeto.replace(/^\s*|\s*$/g,"");
    tdM1.innerHTML = replaceAll(mktM1, ".","");

    $(trM1).append(thM1);
    $(trM1).append(tdM1);
    $(tablaM1).append(trM1);
}


/** Agrega un nuevo dato a la tabla según el checkbox seleccionado **/
function graficoAgregarIndicador(objTR, tablaDatos, nombreObjeto){
    
    var tablaGrafico = $('#tablaGrafico')[0];
        
    var tablaYtd2 = $('#mktYtd2')[0].tBodies[0];
    var tablaYtd1 = $('#mktYtd1')[0].tBodies[0];

    var tabla12M2 = $('#mkt12M2')[0].tBodies[0];
    var tabla12M1 = $('#mkt12M1')[0].tBodies[0];

    var tablaM2 = $('#mktM2')[0].tBodies[0];
    var tablaM1 = $('#mktM1')[0].tBodies[0];
    
    var tablaD = $("#" + tablaDatos)[0];
    var filasD = tablaD.rows.length;
    var columD = tablaD.rows[0].cells.length;

    for(i = 1; i < columD; i++){

        nombreTH = (tablaD.rows[0].cells[i].innerText != undefined) ? tablaD.rows[0].cells[i].innerText : tablaD.rows[0].cells[i].textContent;
        nombreTH = nombreTH.replace(/^\s*|\s*$/g,"");

        if(nombreTH == nombreObjeto){

            for(j = 0; j < filasD; j++){

                td = document.createElement('td');

                tr = tablaGrafico.rows[j];
                td.innerHTML = (tablaD.rows[j].cells[i].innerText != undefined) ? tablaD.rows[j].cells[i].innerText : tablaD.rows[j].cells[i].textContent;
                $(tr).append(td);
            }
        }
    }
    
    if(nombreObjeto != "TOTAL GENERAL"){
    
        var mktYtd2 = (objTR.cells[2].innerText != undefined) ? objTR.cells[2].innerText : objTR.cells[2].textContent;
        var mktYtd1 = (objTR.cells[3].innerText != undefined) ? objTR.cells[3].innerText : objTR.cells[3].textContent;

        var mkt12M2 = (objTR.cells[7].innerText != undefined) ? objTR.cells[7].innerText : objTR.cells[7].textContent;
        var mkt12M1 = (objTR.cells[8].innerText != undefined) ? objTR.cells[8].innerText : objTR.cells[8].textContent;

        var mktM2 = (objTR.cells[12].innerText != undefined) ? objTR.cells[12].innerText : objTR.cells[12].textContent;
        var mktM1 = (objTR.cells[13].innerText != undefined) ? objTR.cells[13].innerText : objTR.cells[13].textContent;
        
        graficoDatoIndicador(nombreObjeto, tablaYtd2, tablaYtd1, tabla12M2, tabla12M1, tablaM2, tablaM1, mktYtd2, mktYtd1, mkt12M2, mkt12M1, mktM2, mktM1);                    
    }

    
    
}


/** Eliminar un dato a la tabla según el checkbox seleccionado **/
function graficoEliminarIndicador(nombreObjeto){
    
    var tablaGrafico = $('#tablaGrafico')[0];
    var filasG = tablaGrafico.rows.length;
    var columG = tablaGrafico.rows[0].cells.length;
    
    var tablaYtd2 = $('#mktYtd2')[0].tBodies[0];
    var tablaYtd1 = $('#mktYtd1')[0].tBodies[0];

    var tabla12M2 = $('#mkt12M2')[0].tBodies[0];
    var tabla12M1 = $('#mkt12M1')[0].tBodies[0];

    var tablaM2 = $('#mktM2')[0].tBodies[0];
    var tablaM1 = $('#mktM1')[0].tBodies[0];
        

    for(i = 1; i < columG; i++){

        nombreTH = (tablaGrafico.rows[0].cells[i].innerText != undefined) ? tablaGrafico.rows[0].cells[i].innerText : tablaGrafico.rows[0].cells[i].textContent;
        nombreTH = nombreTH.replace(/^\s*|\s*$/g,"");

        if(nombreTH == nombreObjeto){

            for(j = 0; j < filasG; j++){

                td = tablaGrafico.rows[j].cells[i];
                tr = td.parentNode;
                tr.removeChild(td);

            }
            i--;
            columG--;
        }
    }


    for(i = 0; i < tablaYtd2.rows.length; i++){

        nombreTH = (tablaYtd2.rows[i].cells[0].innerText != undefined) ? tablaYtd2.rows[i].cells[0].innerText : tablaYtd2.rows[i].cells[0].textContent;
        nombreTH = nombreTH.replace(/^\s*|\s*$/g,"");

        if(nombreTH == nombreObjeto){

            tablaYtd2.deleteRow(i);
            tablaYtd1.deleteRow(i);

            tabla12M2.deleteRow(i);
            tabla12M1.deleteRow(i);

            tablaM2.deleteRow(i);
            tablaM1.deleteRow(i);

            i = tablaYtd2.rows.length;
        }
    }
    
}


/** Evento al checkear **/
function graficoCheckboxIndicador(objCheckBox){

    var objTR = objCheckBox.parentNode.parentNode;
    var objTBody= objTR.parentNode;

    var nTabla = objTBody.id;        

    var objNombre = objTR.cells[1];
    var nombreObjeto = (objNombre.title != undefined) ? objNombre.title : "";

    if(nombreObjeto.trim() == ""){
        nombreObjeto = (objNombre.innerText != undefined) ? objNombre.innerText : objNombre.textContent;
    }

    nombreObjeto = nombreObjeto.replace(/^\s*|\s*$/g,"");

    var tablaDatos = "tablaGeneral";   
            
    if(nTabla == "tbodyAgenciaPuerto" || nTabla == "tbodyAgenciaPuertoGeo"){
        tablaDatos = "tablaAgenciaPuerto";
    }
    else if(nTabla == "tbodyPuertoAgencia" || nTabla == "tbodyPuertoAgenciaGeo"){
        tablaDatos = "tablaPuertoAgencia";
    }
    else if(nTabla == "tBodyC20"){
        tablaDatos = "tablaGeneralC20";
    }
    else if(nTabla == "tBodyC40"){
        tablaDatos = "tablaGeneralC40";
    }

    if(objCheckBox.checked == true){
        graficoAgregarIndicador(objTR, tablaDatos, nombreObjeto);
    }
    else{
        graficoEliminarIndicador(nombreObjeto);
    }

    graficoCargaIndicador($('#tablaGrafico')[0], nTabla, objTR);    //tabla con datos para grafico, nombre de la tabla visible

}


/** Selecciona el tipo de gráfico gráfico y lo carga **/
function graficoCargaIndicador(tablaGrafico, nombreTablaDatos, objTR){

    var graficoSelect = $('#selectGrafico').val();
    var nombreTabla = "tablaGrafico";

    $('#divGraficos').css("display", "");
	
	/** 28-02-2018 **/
	
	var idioma = $("#idioma").val();
	var TOTAL_GENERAL = "";
	var OTROS = "";
	var CONECTOR = "";
	var ENERO = "";
	
	if(idioma == "EN"){		
		TOTAL_GENERAL = "TOTAL";	
		OTROS = 'OTHERS';
		CONECTOR = 'to';
		ENERO = 'JANUARY';
	}
	else{
		TOTAL_GENERAL = "TOTAL GENERAL";
		OTROS = 'OTROS';
		CONECTOR = 'a';
		ENERO = 'ENERO';
	}
	
	/****************/
	
	

    var columG = tablaGrafico.rows[0].cells.length;

    if(columG == 1){

        var tipo = $("#selectTipo").val();

        var tablaDatos = "tablaGeneral";

        if(tipo == "C20"){
            tablaDatos = "tablaGeneralC20";
        }
        else if(tipo == "C40"){
            tablaDatos = "tablaGeneralC40";
        }
        
        graficoAgregarIndicador(objTR, tablaDatos, TOTAL_GENERAL);
               
    }
    else{

        var celda = tablaGrafico.rows[0].cells[1];
        var valorCelda = (celda.innerText != undefined) ? celda.innerText : celda.textContent;

        if(valorCelda == TOTAL_GENERAL && columG > 2){
            graficoEliminarIndicador(TOTAL_GENERAL);
        }
    }

    if(graficoSelect == "line" || graficoSelect == "column"){

        $('#divGrafico').css("display", "");
        $('#divGraficoA').css("display", "none");
        $('#divGraficoB').css("display", "none");
        
        graficoInicializarBarra(nombreTabla, 'divGrafico', graficoSelect, ' ');

    }
    else if (graficoSelect == "pie"){

        $('#divGrafico').css("display", "none");
        $('#divGraficoA').css("display", "");
        $('#divGraficoB').css("display", "");

        var meses = ($('#tablaValoresTHead')[0].rows[0].cells[2].innerText != undefined) ? $('#tablaValoresTHead')[0].rows[0].cells[2].innerText : $('#tablaValoresTHead')[0].rows[0].cells[2].textContent;
        var mesAnterior = (meses.split("-")[0]).trim();
        var mesActual = (meses.split("-")[1]).trim();

        var tdyyyyActual = ($('#tablaValoresTHead')[0].rows[1].cells[1].innerText != undefined) ? $('#tablaValoresTHead')[0].rows[1].cells[1].innerText : $('#tablaValoresTHead')[0].rows[1].cells[1].textContent;
        var tdyyyyAnterior = ($('#tablaValoresTHead')[0].rows[1].cells[2].innerText != undefined) ? $('#tablaValoresTHead')[0].rows[1].cells[2].innerText : $('#tablaValoresTHead')[0].rows[1].cells[2].textContent;

        var yyyyActual = tdyyyyActual.trim();
        var yyyyAnterior = tdyyyyAnterior.trim();

        var graficoMktSelect = $('#selectMkt').val();

        var tabla2;
        var tabla1;

        var subtituloMKT2 = "";
        var subtituloMKT1 = "";

        var indiceCelda2 = 0;
        var indiceCelda1 = 0;

        switch(graficoMktSelect){

            case 'mkt1':
                tabla2 = $("#mktYtd2")[0].tBodies[0];
                tabla1 = $("#mktYtd1")[0].tBodies[0];
                if(mesActual == ENERO){
                    subtituloMKT2 = ENERO + " " + yyyyActual;
                    subtituloMKT1 = ENERO + " " + yyyyAnterior;
                }
                else{
                    subtituloMKT2 = ENERO + " - " + mesActual + " " + yyyyActual;
                    subtituloMKT1 = ENERO + " - " + mesActual + " " + yyyyAnterior;
                }

                indiceCelda2 = 2;
                indiceCelda1 = 3;

                break;

            case 'mkt2':
                tabla2 = $("#mkt12M2")[0].tBodies[0];
                tabla1 = $("#mkt12M1")[0].tBodies[0];
                var ultimos12A = ($('#tablaValoresTHead')[0].rows[1].cells[6].innerText != undefined) ? $('#tablaValoresTHead')[0].rows[1].cells[6].innerText : $('#tablaValoresTHead')[0].rows[1].cells[6].textContent;
                var ultimos12B = ($('#tablaValoresTHead')[0].rows[1].cells[7].innerText != undefined) ? $('#tablaValoresTHead')[0].rows[1].cells[7].innerText : $('#tablaValoresTHead')[0].rows[1].cells[7].textContent;

                if(ultimos12A.split("-")[1] != undefined){
                    subtituloMKT2 = mesAnterior + " " + ultimos12A.split("-")[0].trim() + " - " + mesActual + " " + ultimos12A.split("-")[1].trim();
                    subtituloMKT1 = mesAnterior + " " + ultimos12B.split("-")[0].trim() + " - " + mesActual + " " + ultimos12B.split("-")[1].trim();
                }
                else{
                    subtituloMKT2 = mesAnterior + " - " + mesActual + " " + ultimos12A.split("-")[0].trim();
                    subtituloMKT1 = mesAnterior + " - " + mesActual + " " + ultimos12B.split("-")[0].trim();
                }

                indiceCelda2 = 7;
                indiceCelda1 = 8;

                break;

            case 'mkt3':
                tabla2 = $("#mktM2")[0].tBodies[0];
                tabla1 = $("#mktM1")[0].tBodies[0];
                subtituloMKT2 = mesActual + " " + yyyyActual;
                subtituloMKT1 = mesActual + " " + yyyyAnterior;
                indiceCelda2 = 12;
                indiceCelda1 = 13;

                break;
        }

        var total2 = 0;
        var total1 = 0;

        var valorTotal = 0;
        var valorTexto = 0;

        for(var i = 0; i < $("#" + nombreTablaDatos)[0].rows.length; i++){

            valorTexto = ($("#" + nombreTablaDatos)[0].rows[i].cells[1].innerText != undefined) ? $("#" + nombreTablaDatos)[0].rows[i].cells[1].innerText : $("#" + nombreTablaDatos)[0].rows[i].cells[1].textContent;

            if(valorTexto.trim() == "Total" || valorTexto.trim() == "TOTAL"){
                valorTotal = ($("#" + nombreTablaDatos)[0].rows[i].cells[indiceCelda2].innerText != undefined) ? $("#" + nombreTablaDatos)[0].rows[i].cells[indiceCelda2].innerText : $("#" + nombreTablaDatos)[0].rows[i].cells[indiceCelda2].textContent;
                valorTotal = replaceAll(valorTotal, ".", "");
                total2 = total2 + parseInt(valorTotal);

                valorTotal = ($("#" + nombreTablaDatos)[0].rows[i].cells[indiceCelda1].innerText != undefined) ? $("#" + nombreTablaDatos)[0].rows[i].cells[indiceCelda1].innerText : $("#" + nombreTablaDatos)[0].rows[i].cells[indiceCelda1].textContent;
                valorTotal = replaceAll(valorTotal, ".", "");
                total1 = total1 + parseInt(valorTotal);
            }
        }                       

        var nombreTH = "";

        var valorTotalTabla2 = 0;
        var valorTotalTabla1 = 0;

        for(i = 0; i < tabla2.rows.length; i++){

            nombreTH = (tabla2.rows[i].cells[0].innerText != undefined) ? tabla2.rows[i].cells[0].innerText : tabla2.rows[i].cells[0].textContent;

            if(nombreTH == OTROS){

                tabla2.deleteRow(i);
                i--;
            }
            else{
                valorTotal = (tabla2.rows[i].cells[1].innerText != undefined) ? tabla2.rows[i].cells[1].innerText : tabla2.rows[i].cells[1].textContent;
                valorTotal = replaceAll(valorTotal, ".","");
                valorTotalTabla2 = valorTotalTabla2 + parseInt(valorTotal);
            }
        }


        for(i = 0; i < tabla1.rows.length; i++){

            nombreTH = (tabla1.rows[i].cells[0].innerText != undefined) ? tabla1.rows[i].cells[0].innerText : tabla1.rows[i].cells[0].textContent;

            if(nombreTH == OTROS){

                tabla1.deleteRow(i);
                i--;
            }
            else{
                valorTotal = (tabla1.rows[i].cells[1].innerText != undefined) ? tabla1.rows[i].cells[1].innerText : tabla1.rows[i].cells[1].textContent;
                valorTotal = replaceAll(valorTotal, ".","");
                valorTotalTabla1 = valorTotalTabla1 + parseInt(valorTotal);
            }
        }

        
        /*** TABLA 2 ***/

        if(total2 - valorTotalTabla2 > 0){
            var thYtd2 = document.createElement('th');
            var tdYtd2 = document.createElement('td');
            var trYtd2 = document.createElement('tr');
            
            if(valorTotalTabla2 == "0"){
                thYtd2.innerHTML = TOTAL_GENERAL;
            }
            else{
                thYtd2.innerHTML = OTROS;
            }

            
            tdYtd2.innerHTML = total2 - valorTotalTabla2;

            $(trYtd2).append(thYtd2);
            $(trYtd2).append(tdYtd2);
            $(tabla2).append(trYtd2);

        }
        //en caso contrario, insertar todas las filas que no han sido insertadas en la tabla, cuyo porcentaje sumado sea muy pequeño.


        /*** TABLA 1 ***/

        if(total1 - valorTotalTabla1 > 0){
            var thYtd1 = document.createElement('th');
            var tdYtd1 = document.createElement('td');
            var trYtd1 = document.createElement('tr');

            if(valorTotalTabla1 == "0"){
                thYtd1.innerHTML = TOTAL_GENERAL;
            }
            else{
                thYtd1.innerHTML = OTROS;
            }
            
            tdYtd1.innerHTML = total1 - valorTotalTabla1;

            $(trYtd1).append(thYtd1);
            $(trYtd1).append(tdYtd1);
            $(tabla1).append(trYtd1);

        }
                

        graficoInicializarTorta($(tabla2).parent().attr('id'), 'divGraficoA', subtituloMKT2, total2);
        graficoInicializarTorta($(tabla1).parent().attr('id'), 'divGraficoB', subtituloMKT1, total1);

    }
}


/** Carga gráfico según C20 y C40**/
function graficoTipoIndicador(){

    var tipo = $("#selectTipo").val();
    var tbodyId = "tBody" + tipo;
    tbodyId = tbodyId.trim();
	
	/** 28-02-2018 **/
	
	var idioma = $("#idioma").val();
	var TITULO = "";
	
	
	if(idioma == "EN"){		
		TITULO = "of Containers";	
	}
	else{
		TITULO = "de Contenedores";
	}
	
	/****************/


    if(tipo == "C20"){
        $("#tBody").css("display","none");
        $("#tBodyC20").css("display","");
        $("#tBodyC40").css("display","none");

        $('#tituloGrafico2').val("N° " + TITULO + " (C20)");
        $('#tituloGraficoEjeY').val("N° " + TITULO + " (C20)");
    }
    else if(tipo == "C40"){
        $("#tBody").css("display","none");
        $("#tBodyC20").css("display","none");
        $("#tBodyC40").css("display","");

        $('#tituloGrafico2').val("N° " + TITULO + " (C40)");
        $('#tituloGraficoEjeY').val("N° " + TITULO + " (C40)");
    }
    else{
        $("#tBody").css("display","");
        $("#tBodyC20").css("display","none");
        $("#tBodyC40").css("display","none");

        $('#tituloGrafico2').val("N° " + TITULO + " (C20 + C40)");
        $('#tituloGraficoEjeY').val("N° " + TITULO + " (C20 + C40)");
    }

    var tablaGrafico = $('#tablaGrafico')[0];
    var filasG = tablaGrafico.rows.length;
    var columG = tablaGrafico.rows[0].cells.length;

    var tabla1 = $('#mktYtd2')[0].tBodies[0];
    var tabla2 = $('#mktYtd1')[0].tBodies[0];

    var tabla3 = $('#mkt12M2')[0].tBodies[0];
    var tabla4 = $('#mkt12M1')[0].tBodies[0];

    var tabla5 = $('#mktM2')[0].tBodies[0];
    var tabla6 = $('#mktM1')[0].tBodies[0];

    for(i = 1; i < columG; i++){

        for(j = 0; j < filasG; j++){

            td = tablaGrafico.rows[j].cells[i];
            tr = td.parentNode;
            tr.removeChild(td);

        }
        i--;
        columG--;
    }


    for(i = 0; i < tabla1.rows.length; i++){

        tabla1.deleteRow(i);
        tabla2.deleteRow(i);

        i--;
    }

    for(i = 0; i < tabla3.rows.length; i++){

        tabla3.deleteRow(i);
        tabla4.deleteRow(i);

        i--;
    }

    for(i = 0; i < tabla5.rows.length; i++){

        tabla5.deleteRow(i);
        tabla6.deleteRow(i);

        i--;
    }

    var checkeados = 0;

    $("#" + tbodyId + " tr").each(function (index) {
        
        $(this).children("td").each(function (index2, td) {

            if(index2 == 0){

                if(checkeados < 3){

                    $(td).children("input").each(function (index3, input) {

                        if(input.checked == true){
                            input.click();
                        }

                        input.click();
                        checkeados++;
                    });

                    $(td).children("span ").each(function (index3, span) {
                        $(span).removeClass('checkboxC');
                        $(span).addClass('checkboxChecked');
                    });

                }
                else{
                    
                    $(td).children("input").each(function (index3, input) {
                        input.checked = false;
                    });

                    $(td).children("span ").each(function (index3, span) {
                        $(span).removeClass('checkboxChecked');
                        $(span).addClass('checkbox');
                    });                    
                    
                }
            }
        });
    });


}



/** GRAFICO REPORTES **/


function graficoDatoReporte(nombreObjeto, tabla2, tabla1, valor2, valor1){

    /*** YTD 2 ***/

    var thYtd2 = document.createElement('th');
    var tdYtd2 = document.createElement('td');
    var trYtd2 = document.createElement('tr');
    var valor = "";

    thYtd2.innerHTML = nombreObjeto.replace(/^\s*|\s*$/g,"");
    valor = replaceAll(valor2, ".","");
    tdYtd2.innerHTML = parseInt(valor);

    $(trYtd2).append(thYtd2);
    $(trYtd2).append(tdYtd2);
    $(tabla2).append(trYtd2);

    /*** YTD 1 ***/

    var thYtd1 = document.createElement('th');
    var tdYtd1 = document.createElement('td');
    var trYtd1 = document.createElement('tr');

    thYtd1.innerHTML = nombreObjeto.replace(/^\s*|\s*$/g,"");
    valor = replaceAll(valor1, ".","");
    tdYtd1.innerHTML = parseInt(valor);

    $(trYtd1).append(thYtd1);
    $(trYtd1).append(tdYtd1);
    $(tabla1).append(trYtd1);

}


/** Agrega un nuevo dato a la tabla según el checkbox seleccionado **/
function graficoAgregarReporte(objTR, nombreObjeto){
    
    var tablaGrafico = $('#tablaGrafico')[0];
    var filasG = tablaGrafico.rows.length;
            
    var tabla2 = $('#tablaActual')[0].tBodies[0];
    var tabla1 = $('#tablaAnterior')[0].tBodies[0];
	
	/** 28-02-2018 **/
	
	var idioma = $("#idioma").val();
	var TOTAL_GENERAL = "";
	
	if(idioma == "EN"){		
		TOTAL_GENERAL = "TOTAL";		
	}
	else{
		TOTAL_GENERAL = "TOTAL GENERAL";
	}
	
	   
    if(nombreObjeto != TOTAL_GENERAL){
                        
        var valor1 = (objTR.cells[filasG + 2].innerText != undefined) ? objTR.cells[filasG + 2].innerText : objTR.cells[filasG + 2].textContent;
        var valor2 = (objTR.cells[filasG + 1].innerText != undefined) ? objTR.cells[filasG + 1].innerText : objTR.cells[filasG + 1].textContent;

        graficoDatoReporte(nombreObjeto, tabla2, tabla1, valor2, valor1);

    }
    else{
        
        var objTBody = objTR.parentNode;        
        objTR = $("#" + objTBody.id + " tr").last()[0];                
        
    }
    
    $("#tablaGrafico tr:first").append("<th>" + nombreObjeto + "</th>");

    $('#tablaGrafico tr:not(:first)').each(function(index){
        var valor = (objTR.cells[index + 2].innerText != undefined) ? objTR.cells[index + 2].innerText : objTR.cells[index + 2].textContent;
        valor = replaceAll(valor, ".","");
        valor = replaceAll(valor, ",",".");

        $(this).append("<td>" + valor + "</td>");
    });
                    
}


/** Eliminar un dato a la tabla según el checkbox seleccionado **/
function graficoEliminarReporte(nombreObjeto){
    
    var tablaGrafico = $('#tablaGrafico')[0];
    var filasG = tablaGrafico.rows.length;
    var columG = tablaGrafico.rows[0].cells.length;
    
    var tabla2 = $('#tablaActual')[0].tBodies[0];
    var tabla1 = $('#tablaAnterior')[0].tBodies[0];

            
    for(i = 1; i < columG; i++){

        var nombreTH = (tablaGrafico.rows[0].cells[i].innerText != undefined) ? tablaGrafico.rows[0].cells[i].innerText : tablaGrafico.rows[0].cells[i].textContent;
        nombreTH = nombreTH.replace(/^\s*|\s*$/g,"");

        if(nombreTH == nombreObjeto){

            for(j = 0; j < filasG; j++){

                td = tablaGrafico.rows[j].cells[i];
                tr = td.parentNode;
                tr.removeChild(td);

            }
            i--;
            columG--;
        }
    }


    for(var i = 0; i < tabla2.rows.length; i++){

        nombreTH = (tabla2.rows[i].cells[0].innerText != undefined) ? tabla2.rows[i].cells[0].innerText : tabla2.rows[i].cells[0].textContent;
        nombreTH = nombreTH.replace(/^\s*|\s*$/g,"");

        if(nombreTH == nombreObjeto){

            tabla2.deleteRow(i);
            tabla1.deleteRow(i);
            
            i = tabla2.rows.length;
        }
    }
    
}



function graficoCheckboxReporte(objCheckBox){

    var objTR = objCheckBox.parentNode.parentNode;
    var objTBody= objTR.parentNode;

    var nTabla = objTBody.id;

    var tablaGrafico = $('#tablaGrafico')[0];
    
    
    var objNombre = objTR.cells[1];
    var nombreObjeto = "";

    nombreObjeto = (objNombre.title != undefined) ? objNombre.title : "";

    if(nombreObjeto.trim() == ""){
        nombreObjeto = (objNombre.innerText != undefined) ? objNombre.innerText : objNombre.textContent;
    }

    nombreObjeto = nombreObjeto.replace(/^\s*|\s*$/g,"");
    
    if(objCheckBox.checked == true){
        graficoAgregarReporte(objTR, nombreObjeto);

    }
    else{
        graficoEliminarReporte(nombreObjeto);
    }

    graficoCargaReporte(tablaGrafico, nTabla, objTR);    //tabla con datos para grafico, nombre de la tabla visible

}



function graficoCargaReporte(tablaGrafico, nombreTablaDatos, objTR){

    var graficoSelect = $('#selectGraficoReporte').val();
    var nombreTabla = "tablaGrafico";
	
	/** 28-02-2018 **/
	
	var idioma = $("#idioma").val();
	var TOTAL_GENERAL = "";
	var OTROS = "";
	var CONECTOR = "";
	
	if(idioma == "EN"){		
		TOTAL_GENERAL = "TOTAL";	
		OTROS = 'OTHERS';
		CONECTOR = 'to';
	}
	else{
		TOTAL_GENERAL = "TOTAL GENERAL";
		OTROS = 'OTROS';
		CONECTOR = 'a';
	}
	
	/****************/
	

    $('#divGraficos').css("display", "");
    
    var columG = tablaGrafico.rows[0].cells.length;

    if(columG == 1){                       
        graficoAgregarReporte(objTR, TOTAL_GENERAL);
    }
    else{

        var celda = tablaGrafico.rows[0].cells[1];
        var valorCelda = (celda.innerText != undefined) ? celda.innerText : celda.textContent;

        if(valorCelda == TOTAL_GENERAL && columG > 2){
            graficoEliminarReporte(TOTAL_GENERAL);
        }
    }

    if(graficoSelect == "line" || graficoSelect == "column"){

        $('#divGrafico').css("display", "block");
        $('#divGraficoA').css("display", "none");
        $('#divGraficoB').css("display", "none");
       
        graficoInicializarBarra(nombreTabla, 'divGrafico', graficoSelect, ' ');

    }
    else if (graficoSelect == "pie"){

        $('#divGrafico').css("display", "none");
        $('#divGraficoA').css("display", "");
        $('#divGraficoB').css("display", "");

        var filasG = tablaGrafico.rows.length;

        var periodo1 = (tablaGrafico.rows[1].cells[0].innerText != undefined) ? tablaGrafico.rows[1].cells[0].innerText : tablaGrafico.rows[1].cells[0].textContent;
        var periodo2 = (tablaGrafico.rows[filasG - 1].cells[0].innerText != undefined) ? tablaGrafico.rows[filasG - 1].cells[0].innerText : tablaGrafico.rows[filasG - 1].cells[0].textContent;

        var mesAnterior = (periodo1.split("-")[0]).trim();
        var mesActual = (periodo2.split("-")[0]).trim();

        var yyyyAnterior = (periodo1.split("-")[1]).trim();
        var yyyyActual = (periodo2.split("-")[1]).trim();

        var tabla1 = $("#tablaAnterior")[0].tBodies[0];
        var tabla2 = $("#tablaActual")[0].tBodies[0];

        var subtituloTabla2 = "";
        var subtituloTabla1 = "";

        if(periodo1 == periodo2){
            subtituloTabla2 = retornarNombreMes(mesActual) + " " + yyyyActual;
            subtituloTabla1 = retornarNombreMes(mesActual) + " " + (yyyyActual - 1);
        }
        else{
            subtituloTabla2 = retornarNombreMes(mesAnterior) + " " + yyyyAnterior + " " + CONECTOR + " " + retornarNombreMes(mesActual) + " " + yyyyActual;
            subtituloTabla1 = retornarNombreMes(mesAnterior) + " " + (yyyyAnterior - 1) + " " + CONECTOR + " " + retornarNombreMes(mesActual) + " " + (yyyyActual - 1);
        }

        var total2 = 0;
        var total1 = 0;

        var valorTotal = 0;

        var numCeldas = $("#" + nombreTablaDatos + " tr:last")[0].cells.length;

        valorTotal = ($("#" + nombreTablaDatos + " tr:last")[0].cells[numCeldas - 3].innerText != undefined) ? $("#" + nombreTablaDatos + " tr:last")[0].cells[numCeldas - 3].innerText : $("#" + nombreTablaDatos + " tr:last")[0].cells[numCeldas - 3].textContent;
        valorTotal = replaceAll(valorTotal, ".", "");
        total2 = total2 + parseInt(valorTotal);

        valorTotal = ($("#" + nombreTablaDatos + " tr:last")[0].cells[numCeldas - 2].innerText != undefined) ? $("#" + nombreTablaDatos + " tr:last")[0].cells[numCeldas - 2].innerText : $("#" + nombreTablaDatos + " tr:last")[0].cells[numCeldas - 2].textContent;
        valorTotal = replaceAll(valorTotal, ".", "");
        total1 = total1 + parseInt(valorTotal);

        var nombreTH = "";

        var valorTotalTabla2 = 0;
        var valorTotalTabla1 = 0;

        for(i = 0; i < tabla2.rows.length; i++){

            nombreTH = (tabla2.rows[i].cells[0].innerText != undefined) ? tabla2.rows[i].cells[0].innerText : tabla2.rows[i].cells[0].textContent;

            if(nombreTH == OTROS){

                tabla2.deleteRow(i);
                i--;
            }
            else{
                valorTotal = (tabla2.rows[i].cells[1].innerText != undefined) ? tabla2.rows[i].cells[1].innerText : tabla2.rows[i].cells[1].textContent;
                valorTotal = replaceAll(valorTotal, ".","");
                valorTotalTabla2 = valorTotalTabla2 + parseInt(valorTotal);
            }
        }


        for(i = 0; i < tabla1.rows.length; i++){

            nombreTH = (tabla1.rows[i].cells[0].innerText != undefined) ? tabla1.rows[i].cells[0].innerText : tabla1.rows[i].cells[0].textContent;

            if(nombreTH == OTROS){

                tabla1.deleteRow(i);
                i--;
            }
            else{
                valorTotal = (tabla1.rows[i].cells[1].innerText != undefined) ? tabla1.rows[i].cells[1].innerText : tabla1.rows[i].cells[1].textContent;
                valorTotal = replaceAll(valorTotal, ".","");
                valorTotalTabla1 = valorTotalTabla1 + parseInt(valorTotal);
            }
        }


        /*** TABLA 2 ***/

        if(total2 - valorTotalTabla2 > 0){
            var thYtd2 = document.createElement('th');
            var tdYtd2 = document.createElement('td');
            var trYtd2 = document.createElement('tr');
            
            if(valorTotalTabla2 == "0"){
                thYtd2.innerHTML = TOTAL_GENERAL;
            }
            else{
                thYtd2.innerHTML = OTROS;
            }
            
            tdYtd2.innerHTML = total2 - valorTotalTabla2;

            $(trYtd2).append(thYtd2);
            $(trYtd2).append(tdYtd2);
            $(tabla2).append(trYtd2);

        }
        //en caso contrario, insertar todas las filas que no han sido insertadas en la tabla, cuyo porcentaje sumado sea muy pequeño.


        /*** TABLA 1 ***/

        if(total1 - valorTotalTabla1 > 0){
            var thYtd1 = document.createElement('th');
            var tdYtd1 = document.createElement('td');
            var trYtd1 = document.createElement('tr');

            if(valorTotalTabla1 == "0"){
                thYtd1.innerHTML = TOTAL_GENERAL;
            }
            else{
                thYtd1.innerHTML = OTROS;
            }
            
            tdYtd1.innerHTML = total1 - valorTotalTabla1;

            $(trYtd1).append(thYtd1);
            $(trYtd1).append(tdYtd1);
            $(tabla1).append(trYtd1);

        }

        graficoInicializarTorta($(tabla2).parent().attr('id'), 'divGraficoA', subtituloTabla2, total2);
        graficoInicializarTorta($(tabla1).parent().attr('id'), 'divGraficoB', subtituloTabla1, total1);

    }
}



function graficoTipoReporte(){

    var tipo = $("#selectTipoReporte").val();
    var tbodyId = "tBody" + tipo;
    tbodyId = tbodyId.trim();
	
	/** 28-02-2018 **/
	
	var idioma = $("#idioma").val();
	var TITULO = "";	
	
	if(idioma == "EN"){		
		TITULO = "of Containers";			
	}
	else{
		TITULO = "de Contenedores";		
	}
	
	/****************/
	
	

    if(tipo == "C20"){
        $("#tBody").css("display","none");
        $("#tBodyC20").css("display","");
        $("#tBodyC40").css("display","none");

        //        $('#tBody tr.nivel2, #tBody tr.nivel3, #tBody tr.nivel4, #tBody tr.nivel5').each(function(index){
        //            $(this).css("display", "none");
        //        });
        //
        //        $('#tBodyC40 tr.nivel2, #tBodyC40 tr.nivel3, #tBodyC40 tr.nivel4, #tBodyC40 tr.nivel5').each(function(index){
        //            $(this).css("display", "none");
        //        });

        $('#tituloGrafico2').val("N° " + TITULO + " (C20)");
        $('#tituloGraficoEjeY').val("N° " + TITULO + " (C20)");


    }
    else if(tipo == "C40"){
        $("#tBody").css("display","none");
        $("#tBodyC20").css("display","none");
        $("#tBodyC40").css("display","");

        //        $('#tBody tr.nivel2, #tBody tr.nivel3, #tBody tr.nivel4, #tBody tr.nivel5').each(function(index){
        //            $(this).css("display", "none");
        //        });
        //
        //        $('#tBodyC20 tr.nivel2, #tBodyC20 tr.nivel3, #tBodyC20 tr.nivel4, #tBodyC20 tr.nivel5').each(function(index){
        //            $(this).css("display", "none");
        //        });

        $('#tituloGrafico2').val("N° " + TITULO + " (C40)");
        $('#tituloGraficoEjeY').val("N° " + TITULO + " (C40)");
    }
    else{
        $("#tBody").css("display","");
        $("#tBodyC20").css("display","none");
        $("#tBodyC40").css("display","none");

        //        $('#tBodyC40 tr.nivel2, #tBodyC40 tr.nivel3, #tBodyC40 tr.nivel4, #tBodyC40 tr.nivel5').each(function(index){
        //            $(this).css("display", "none");
        //        });
        //
        //        $('#tBodyC20 tr.nivel2, #tBodyC20 tr.nivel3, #tBodyC20 tr.nivel4, #tBodyC20 tr.nivel5').each(function(index){
        //            $(this).css("display", "none");
        //        });

        $('#tituloGrafico2').val("N° " + TITULO + " (C20 + C40)");
        $('#tituloGraficoEjeY').val("N° " + TITULO + " (C20 + C40)");
    }

    var tablaGrafico = $('#tablaGrafico')[0];
    var filasG = tablaGrafico.rows.length;
    var columG = tablaGrafico.rows[0].cells.length;

    var tabla1 = $('#tablaAnterior')[0].tBodies[0];
    var tabla2 = $('#tablaActual')[0].tBodies[0];

    for(i = 1; i < columG; i++){

        for(j = 0; j < filasG; j++){

            td = tablaGrafico.rows[j].cells[i];
            tr = td.parentNode;
            tr.removeChild(td);

        }
        i--;
        columG--;
    }


    for(i = 0; i < tabla1.rows.length; i++){

        tabla1.deleteRow(i);
        tabla2.deleteRow(i);

        i--;
    }

    var checkeados = 0;

    $("#" + tbodyId + " tr").each(function (index) {

        if(checkeados < 3){

            $(this).children("td").each(function (index2, td) {

                if(index2 == 0){

                    $(td).children("input").each(function (index3, input) {

                        if(input.checked == true){
                            input.click();
                        }

                        input.click();
                        checkeados++;
                    });

                    $(td).children("span ").each(function (index3, span) {
                        $(span).removeClass('checkboxC');
                        $(span).addClass('checkboxChecked');
                    });

                }
            });
        }
    });


}

/*********************************************/






/** GRAFICO INDICADORES PRACTICAJE **/

/** Aplica el cambio de orden, Agencia <-> Puerto, Mayor a menor <-> Geográfico  **/

function graficoAbrirNivel(idTr){

    var objetos = $("." + idTr);

    for(var i = 0; i < objetos.length; i++){
        if(objetos[i].style.display == ''){
            $(objetos[i]).css('display','none');
        }
        else{
            $(objetos[i]).css('display','');
        }
    }
}


function graficoOrden(nombre1, nombre2){

    var busquedaA =  $("#selectOrdenA").val();
    var busquedaB =  $("#selectOrdenB").val();
    var nombreTbody = "";
    var nombreTData = "";
    
    if(busquedaA == "AP"){
        $("#tablaValoresTHead")[0].rows[1].cells[0].innerHTML = nombre1;
        //$('nombreTabla').value = "tablaAgenciaPuerto";
        //nombreTData = "tablaAgenciaPuerto";

        if(busquedaB == "og"){
            $("#tbodyAgenciaPuertoGeo").css("display", "");
            $("#tbodyAgenciaPuerto").css("display", "none");
            $("#tbodyPuertoAgenciaGeo").css("display", "none");
            $("#tbodyPuertoAgencia").css("display", "none");
            nombreTbody = "tbodyAgenciaPuertoGeo";
        }
        else{
            $("#tbodyAgenciaPuertoGeo").css("display", "none");
            $("#tbodyAgenciaPuerto").css("display", "");
            $("#tbodyPuertoAgenciaGeo").css("display", "none");
            $("#tbodyPuertoAgencia").css("display", "none");
            nombreTbody = "tbodyAgenciaPuerto";
        }
    }

    if(busquedaA == "PA"){
        $("#tablaValoresTHead")[0].rows[1].cells[0].innerHTML = nombre2;

        //$('nombreTabla').value = "tablaPuertoAgencia";
        //nombreTData = "tablaPuertoAgencia";

        if(busquedaB == "og"){
            $("#tbodyAgenciaPuertoGeo").css("display", "none");
            $("#tbodyAgenciaPuerto").css("display", "none");
            $("#tbodyPuertoAgenciaGeo").css("display", "");
            $("#tbodyPuertoAgencia").css("display", "none");
            nombreTbody = "tbodyPuertoAgenciaGeo";
        }
        else{
            $("#tbodyAgenciaPuertoGeo").css("display", "none");
            $("#tbodyAgenciaPuerto").css("display", "none");
            $("#tbodyPuertoAgenciaGeo").css("display", "none");
            $("#tbodyPuertoAgencia").css("display", "");
            nombreTbody = "tbodyPuertoAgencia";
        }
    }
    
    
    
    var objetos = $("[class^='tr-']");   

    for(var i = 0; i < objetos.length; i++){
        $(objetos[i]).css('display','none');        
    }
    

    var tablaGrafico = $('#tablaGrafico')[0];
    var filasG = tablaGrafico.rows.length;
    var columG = tablaGrafico.rows[0].cells.length;

    for(i = 1; i < columG; i++){

        for(j = 0; j < filasG; j++){

            td = tablaGrafico.rows[j].cells[i];
            tr = td.parentNode;
            tr.removeChild(td);

        }
        i--;
        columG--;
    }
    
    var tablaYtd2 = $('#mktYtd2')[0].tBodies[0];
    var tablaYtd1 = $('#mktYtd1')[0].tBodies[0];

    var tabla12M2 = $('#mkt12M2')[0].tBodies[0];
    var tabla12M1 = $('#mkt12M1')[0].tBodies[0];

    var tablaM2 = $('#mktM2')[0].tBodies[0];
    var tablaM1 = $('#mktM1')[0].tBodies[0];


    for(j = 0; j < tablaYtd2.rows.length; j++){
        tr = tablaYtd2.rows[j];
        tablaYtd2.removeChild(tr);
        j--;
    }

    for(j = 0; j < tablaYtd1.rows.length; j++){
        tr = tablaYtd1.rows[j];
        tablaYtd1.removeChild(tr);
        j--;
    }

    for(j = 0; j < tabla12M2.rows.length; j++){
        tr = tabla12M2.rows[j];
        tabla12M2.removeChild(tr);
        j--;
    }

    for(j = 0; j < tabla12M1.rows.length; j++){
        tr = tabla12M1.rows[j];
        tabla12M1.removeChild(tr);
        j--;
    }

    for(j = 0; j < tablaM2.rows.length; j++){
        tr = tablaM2.rows[j];
        tablaM2.removeChild(tr);
        j--;
    }

    for(j = 0; j < tablaM1.rows.length; j++){
        tr = tablaM1.rows[j];
        tablaM1.removeChild(tr);
        j--;
    }

    var checks = 0;
    
    
    $("#tablaValores tr").each(function (index) {
                                                                                
        $(this).children("td").each(function (index2, td) {

            if(index2 == 0){
                                
                $(td).children("input").each(function (index3, input) {                                                            
                    input.checked = false;
                });

                $(td).children("span ").each(function (index3, span) {
                    $(span).removeClass('checkboxChecked');
                    $(span).addClass('checkbox');                                        
                });               

            }                                                
        });       
    });
    
    $("#" + nombreTbody + " tr").each(function (index, elemento) {
                                                                        
        if(checks < 3 && $(elemento).hasClass("nivel1")){

            $(this).children("td").each(function (index2, td) {

                if(index2 == 0){

                    $(td).children("input").each(function (index3, input) {
                        input.click();
                    });

                    $(td).children("span ").each(function (index3, span) {
                        $(span).removeClass('checkboxC');
                        $(span).addClass('checkboxChecked');
                    });

                    checks++;

                }                                                
            });
        }                    
    });   
}







/** LOGIN **/

function validarUsuario(){

    var usuarioNombre = $('#usuarioNombre').attr('value');
    var usuarioPassword = $('#usuarioPassword').attr('value');
    
    $('#lbLoginError').html('');
    $('#lbLoginError').css('color', '#DF1D1E');

    $('#divPass').css('display', 'none');

    $.ajax({
        url:'indexValidacion.php',
        type:'POST',
        data:{
            usuarioNombre: usuarioNombre,
            usuarioPassword: usuarioPassword,
            estado: "login"
        },
        cache:false,
        async: false,
        dataType: "json"
    }).done(function(data) {
        
        var estado = "";
        var url = "";
        
        if(data != null){
            estado = data[0].estado;
            url = data[0].url;
        }      
                        
        if(estado == "pass"){
            $('#divLogin').css('display', 'none');
            $('#divPass').css('display', '');
            
            usuarioNombre = $('#usuarioNombre').attr('value');
            $('#usuarioNombrePass').val(usuarioNombre);
        }
        else if(estado == "A"){
            $('#lbLoginError').html("El usuario no existe");
        }
        else if(estado == "B"){
            $('#lbLoginError').html("La contraseña es incorrecta");
        }
        else if(estado == "N"){
            $('#lbLoginError').html("Complete los datos solicitados");
        }
        else if(estado == "OK"){
            window.location = url;
        }
        else{
            //window.location = data.trim();
            $('#lbLoginError').html("Se ha producido un error, favor inténtelo más tarde");
        }

    });

}


function validarUsuario2(){

    var usuarioNombre = $('#usuarioNombrePass').attr('value');
    var usuarioPassword = $('#usuarioPassword').attr('value');
    var usuarioPasswordB = $('#usuarioPasswordB').attr('value');
    var usuarioPasswordB2 = $('#usuarioPasswordB2').attr('value');
    
    $('#lbLoginError2').html('');
    
    $.ajax({
        url:'indexValidacion.php',
        type:'POST',
        data:{
            usuarioNombre: usuarioNombre,
            usuarioPassword: usuarioPassword,
            usuarioPasswordB: usuarioPasswordB,
            usuarioPasswordB2: usuarioPasswordB2,
            estado: "pass"
        },
        cache:false,
        async: false,
        dataType: "json"
    }).done(function(data) { 
                       
        var estado = "";      
        var url = "";
        
        if(data != null){
            estado = data[0].estado;  
            url = data[0].url;
        }      
                                
        if(estado == "NC"){
            $('#lbLoginError2').html("Los valores de las contraseñas no coinciden");            
        }
        else if(estado == "A"){
            $('#lbLoginError2').html("La contraseña ingresada no es válida");
        }
        else if(estado == "B"){
            $('#lbLoginError2').html("La contraseña no puede ser igual al R.U.T");
        }
        else if(estado == "E"){
            $('#lbLoginError2').html("Se ha producido un error, favor inténtelo más tarde");
        }
        else if(estado == "N"){
            $('#lbLoginError2').html("Complete los datos solicitados");
        }
        else if(estado == "5"){
            $('#lbLoginError2').html("La contraseña debe tener al menos 5 caracteres");
        }
        else if(estado == "OK"){
            window.location = url;
        }
        else{
            $('#lbLoginError2').html("Se ha producido un error, favor inténtelo más tarde");
        }
        
    });

}


function recuperarPass(){

    var usuarioNombre = $('#usuarioNombre').attr('value');
    
    $('#lbLoginError').html('');
    $('#lbLoginError').css('color', '#DF1D1E');

    $.ajax({
        url:'indexValidacion.php',
        type:'POST',
        data:{
            usuarioNombre: usuarioNombre,
            estado: "recuperar"
        },
        cache:false,
        async: false,
        dataType: "json"
    }).done(function(data) {
        
        var estado = "";        
        
        if(data != null){
            estado = data[0].estado;            
        }      
                                
        if(estado == "S"){
            $('#lbLoginError').html("Se han enviado los datos de acceso a su E-mail");
            $('#lbLoginError').css('color', '#3E576F');
        }
        else if(estado == "NS"){
            $('#lbLoginError').html("Se ha producido un error, favor inténtelo más tarde");
        }
        else if(estado == "E"){
            $('#lbLoginError').html("El usuario no existe");
        }
        else if(estado == "N"){
            $('#lbLoginError').html("Ingrese el nombre de usuario");
        }
        else{
            $('#lbLoginError').html("Se ha producido un error, favor inténtelo más tarde");
        }

    });

}



/** EXTRACCIONES **/

function buscarExtraccionesArchivos(modulo){

    var carpeta = $("#carpeta").val();

    $.ajax({
        type: "POST",
        url: "../../complementos/extraccionesArchivos.php",
        data: {
            modulo: modulo, 
            carpeta: carpeta
        }
    })
    .done(function( msg ) {
        $("#tBody").html(msg);                 
        $("#imgCargando").css("display","none");
    });                                                                  

} 


function eliminarArchivo(archivoId, modulo, carpeta){

    $.ajax({
        type: "POST",
        url: "../../complementos/extraccionesEliminarArchivo.php",
        data: {
            modulo: modulo, 
            archivoId: archivoId, 
            carpeta:carpeta
        }
    })
    .done(function( msg ) {
        buscarExtraccionesArchivos(modulo);
    });                  

} 


/** 09-09-2016 **/

function reloadMenu(modId){        

    $.ajax({
        type: "POST",
        url: "../../complementos/menu.php",
        data: {
            modId: modId           
        }
    })
    .done(function(html) {        
        $("#divMenuS").html(html);        
    });                  

} 


