<?
header('P3P: CP="IDC DSP COR CURa ADMa OUR IND PHY ONL COM STA"');
session_start();

include("sempatConfig.php");

$plantilla = new Template();
$plantilla->setPath('plantillas/');
$plantilla->setTemplate("headerLogin");
echo $plantilla->show();
?>

<script type="text/javascript">
                
    $(document).keypress(function(e) {
        if(e.which == 13) {
            $("#btnLogin").click();
        }
    });

</script>

<div>

    <img id="background" title="" alt="" src="imagenes/login/fondo.png" style="z-index:-10"> 

    <div class="divLoginFranja">

        <center>

            <div style="width:1024px;">

                <div class="divLoginLogo"></div>
                <div id="divLogin" class="divLoginPanel">

                    <table class="tablaForm">
                        <thead>
                            <tr>
                                <th class="welcome">BIENVENIDO</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><input id="usuarioNombre" type="text" class="inputLogin" placeHolder="Usuario" /></td>
                            </tr>
                            <tr>
                                <td><input id="usuarioPassword" type="password" class="inputLogin" placeHolder="Contraseña" /></td>
                            </tr>
                            <tr>                                
                                <td style="height:10px"><a href="#" onClick="recuperarPass();">Recuperar contraseña</a></td>
                            </tr>
                        </tbody>
                    </table>

                    <input id="btnLogin" type="button" class="btnLogin" value="ENTRAR" style="float:right;" onClick="validarUsuario();" />

                    <br style="clear:both; margin-top: 30px;">

                    <label id="lbLoginError" class="error"></label>

                </div>
                
                <div id="divPass" class="divLoginPanel" style="display:none">

                    <table class="tablaForm">
                        <thead>
                            <tr>
                                <th class="welcome">Cambie su contraseña</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><input id="usuarioNombrePass" type="text" class="inputLogin" placeHolder="Usuario" readonly /></td>
                            </tr>
                            <tr>
                                <td><input id="usuarioPasswordB" type="password" class="inputLogin" placeHolder="Contraseña" /></td>
                            </tr>
                            <tr>
                                <td><input id="usuarioPasswordB2" type="password" class="inputLogin" placeHolder="Repita la contraseña" /></td>
                            </tr>
                        </tbody>
                    </table>

                    <input id="btnLogin2" type="button" class="btnLogin" value="GUARDAR" style="float:right;" onClick="validarUsuario2();" />

                    <br style="clear:both; margin-top: 30px;">

                    <label id="lbLoginError2" class="error"></label>

                </div>

            </div>                               

        </center>

    </div>

</div>



